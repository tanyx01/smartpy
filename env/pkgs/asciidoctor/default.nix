{ bundlerApp, makeWrapper }:

bundlerApp {
  pname = "asciidoctor";
  gemdir = ./.;
  exes = [ "asciidoctor" ];
  buildInputs = [ makeWrapper ];
}
