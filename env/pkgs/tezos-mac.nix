{ stdenv, pkgs }:

with pkgs;

stdenv.mkDerivation {
  name = "tezos-mac";
  tezos_binaries = builtins.fetchurl
    { url = "https://gitlab.com/SmartPy/tezos-binaries/-/jobs/3131591845/artifacts/download";
      sha256 = "11aphg6p4fdib8wwccfl6r8amxsvyhqxylv41saykjm1383c54xp"; };
  phases = [ "installPhase" ];
  buildInputs = [ unzip ];
  installPhase = ''
      mkdir -p $out/bin
      ${unzip}/bin/unzip -jd $out/bin $tezos_binaries
    '';
}
