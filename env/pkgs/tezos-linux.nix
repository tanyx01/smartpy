{ stdenv, pkgs }:

with pkgs;

stdenv.mkDerivation {
  name = "tezos-linux";
  tezos_binaries = builtins.fetchurl
    {
      url = "https://gitlab.com/tezos/tezos/-/package_files/61895291/download";
      sha256 = "1kk2zhwqkv7wk9za867g6m0hffz8vn53lsnmq5y7swnq1acqd6ay";
    };
  phases = [ "installPhase" ];
  buildInputs = [ ];
  installPhase = ''
      mkdir -p $out/bin
      tar xzf $tezos_binaries --strip-components=1 -C $out/bin
    '';
}
