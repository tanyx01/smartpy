# Price Feed documentation

The Oracle nodes submit a new value if one of these conditions are fulfilled:
- Hearbeat Threshold
- Deviation Threshold: the price changed by more than a threshold (5% in general)

## Round rules

Note that for in-progress rounds (i.e. rounds that haven't yet received maxSubmissions) answer and updatedAt may change between queries.

When an Oracle submits a price, he indicates in which round he wants to submit it.

3 possibilities:

|     Submit in round     |             Validity condition            |
|:-----------------------:|:---------------------------------------:|
| Previous                | NOT (updated(current))                  |
| Current                 | timedout(previous) OR updated(previous) |
| Next                    | timedout(current) OR updated(current)   |

**1) Submit in previous round**

- [ ] The Oracle did not submit in current round.
- [ ] The Oracle did not submit in previous round.
- [ ] The previous round hasn't yet received `maxSubmissions` submissions
- [ ] The current round did not have the quorum yet (i.e. `answeredInRound` < `currentRoundId`)
- [ ] The submitted value isn't extreme
- [ ] Oracle is activated for previous round

+ Add of submission in the previous round
+ If previous round has the quorum: update `latestRoundData` with the previous round infos.

**2) Submit in current round**

- [ ] The Oracle did not submit in current round.
- [ ] The current round hasn't yet received `maxSubmissions` submissions
- [ ] The submitted value isn't extreme
- [ ] Oracle is activated for current round

+ Add of submission in the current round
+ If current round has the quorum: update `latestRoundData` with the current round infos.

**3) Submit in next round**

- [ ] Oracle did not ask for a new round since `roundDelay` rounds
- [ ] The current round has the quorum or timedout. Other way to say it: the round is superseadable
- [ ] The submitted value isn't extreme
- [ ] Oracle is activated for next round

+ The previous round infos are filled with those of the current round
+ `currentRoundId` is incremented
+ A new current round is initialised with the submission
+ The `toActivate` and `toDeactivate` map is updated.
+ The `lastStartedRound` of the Oracle is updated with the `currentRoundId`

**Other cases**

+ Submission is refused

### Update of `latestRoundData`

`roundId` = `currentRoundId`<br/>
`answer` = median of submissions of last round that obtained the quorum<br/>
`startedAt` = timestamp of the last round that obtained the quorum<br/>
`answeredInRound` = id of the last round that obtained the quorum<br/>
`updatedAt` = timestamp of the last submission that updated the value.

> **Note:** `answeredInRound` may be smaller than `roundId` when the round timed out.

### Usage of timeout

The `timeout` gives the opportunity to:

- update the current round even if the last didn't obtained the quorum.
- create a next round even if the current didn't obtained the quorum.

### Example with previous, current and next

This example is simplified. We don't take into account which Oracle submit, if he has the right to do so...

We only represent the contract's state if it changed and with a lot of simplification.

States keywords: `timeout`: *The round started since more than the threshold*. `update`: *the round has obtained the quorum*.<br/>
Commands keywords: `Previous`, `Submit` and `Current` represents a call to the submit entry point with a roundId equals to respectivaly the previous, the current, the next roundId.

\>\>\> `Origination`

    Previous:  id = 0 ; timeout ; update
    Current:   id = 1 ; no timeout ; no update

\>\>\> `Next`: ERR.CurrentNonSuperseadable

Time passed

    Previous:  id = 0 ; timeout ; update
    Current:   id = 1 ; timeout ; no update

\>\>\> `Next`: Ok

    Previous:  id = 1 ; timeout ; no update
    Current:   id = 2 ; no timeout ; no update

\>\>\> `Previous`: Ok

\>\>\> `Current`: Ok

\>\>\> `Previous`: Ok

    Previous:  id = 1 ; timeout ; update
    Current:   id = 2 ; no timeout ; no update

\>\>\> `Previous`: Ok

\>\>\> `Current`: Ok

    Previous:  id = 1 ; timeout ; update
    Current:   id = 2 ; no timeout ; update

\>\>\> `Previous`: ERR.CurrentHasValue

\>\>\> `Current`: Ok

\>\>\> `Next`: Ok

    Previous:  id = 2 ; no timeout ; update
    Current:   id = 3 ; no timeout ; no update

\>\>\> `Next`: ERR.RoundNotOver

\>\>\> `Current`: Ok

\>\>\> `Previous`: Ok

Le temps passe

    Previous:  id = 2 ; timeout ; update
    Current:   id = 3 ; timeout ; no update

\>\>\> `Previous`: Ok


## Goals:

**Trust / Solitidy**

- [ ] insensitive to small fluctuations -> consensus
- [ ] auditable process
- [ ] sustainability

**Moderate cost**

- [ ] moderate submit cost
- [ ] moderate view cost

**Accessibility**

- [ ] Make the answer accessible by all available and future means
    + view (current version)
    + ticket
    + off-chain view
    + view (future version)
    + off-chain events
    + on-chain events

## Technical choices

Choices:
+ Prefer GET on Push for answers
+ Don't save an onchain historic
+ proxy contract
+ no fallback addresses system

Hypothesis: We want to limitate submit cost<br/>
Consequence: Answer is retrieve by a GET and not pushed by *Aggregator*

Hypothesis: We want *Aggregator* to be immuable to maximise trust<br/>
            + be able to add new functionnalities to *Aggregator* (like events)<br/>
            + reduce efforts on client side<br/>
            + limitate submit cost (no dynamic load of submit function)<br/>
Consequence: create a *Proxy* contrat

Dependency: *Proxy*<br/>
Hypothesis: We want that *Proxy* can work nearly forever<br/>
            + be able to add new functionnalities to *Proxies*<br/>
Consequence: originate new *Proxy* when needed new functionnality<br/>
             + add an entry point to change the aggregator's address<br/>
Alternative: dynamic load of functionnalities -> indisponibility risk in case of error + increased cost

Dependency: *Proxy* entry point to change *Aggregator* address<br/>
Hypothesis: *Proxy* should still be able to answer in cas of an error after a target *Aggregator* change<br/>
Consequence: Accept the risk to avoid adding risk by increasing complexity<br/>
Alternative: Double target system (current and fallback)<br/>
             + add a fallback mode on proxy requets<br/>
             + more complexity on client side, on proxy side and on requests

Hypothesis: Clients want on-chain historic of rounds (we don't have info that it is a real nead)<br/>
Consequence: Don't satisfy this demand<br/>
Alternative: Save historic on-chain<br/>
             + Storage cost will be important (how much?)

## Process

**How to update the aggregator contract?**
1) Originate a new contract for each N assets
2) Test it on the mainnet by pushing values both on the old and new one
3) Using the administration contract:
   + update *Proxy*'s target with the new *Aggregator* address
   + consider updating legagy proxies as well if they are still used or inactivate them

**How to update the proxy view contract?**
1) Create a new proxy view for each N assets
2) Document and publicly announce changement of the new view address for each view
3) The old proxy should still work

**As a client, how to change the proxy view address?**
1) Save the one you were using as a fallback
2) Register the new address
