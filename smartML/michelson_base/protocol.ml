type protocol =
  | Jakarta
  | Kathmandu
  | Lima
[@@deriving eq, ord, show {with_path = false}]
