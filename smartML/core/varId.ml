(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Utils_pure

module T = struct
  type t = {var_id : int} [@@deriving eq, ord, show {with_path = false}]
end

include T

let counter = ref 0

let mk () =
  incr counter;
  {var_id = !counter}

module Set = Set.Make (T)
module Map = Map.Make (T)
