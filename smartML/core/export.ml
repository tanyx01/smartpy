(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Utils_pure
open Basics

type sexpr =
  | Atom of string
  | List of sexpr list

let rec display_sexpr = function
  | Atom s -> s
  | List es -> "(" ^ String.concat " " (List.map display_sexpr es) ^ ")"

let export_bool = function
  | true -> Atom "True"
  | false -> Atom "False"

let export_option f = function
  | None -> Atom "None"
  | Some x -> f x

let export_layout layout =
  match Hole.get layout with
  | None -> "None"
  | Some layout ->
      let leaf Layout.{source; target} =
        if source = target
        then Printf.sprintf "(\"%s\")" source
        else Printf.sprintf "(\"%s as %s\")" source target
      in
      let node l1 l2 = Printf.sprintf "(%s %s)" l1 l2 in
      Printf.sprintf "(Some %s)" (Binary_tree.cata leaf node layout)

let export_type =
  let rec export_type t =
    match Type.unF t with
    | T0 t ->
        let t, memo = Michelson_base.Type.string_of_type0 t in
        assert (memo = None);
        t
    | TInt {isNat} -> (
        match Hole.get isNat with
        | None -> "intOrNat"
        | Some true -> "nat"
        | Some false -> "int")
    | TBounded {base; cases; var} ->
        Printf.sprintf "(bounded %s %s %s)" (export_type base)
          (if var = None then "True" else "False")
          (String.concat " " (List.map (fun _ -> "bounded not handled") cases))
    | TRecord {row; layout; var} ->
        assert (var = None);
        Printf.sprintf "(record (%s) %s (\"\" -1))"
          (String.concat " "
             (List.map
                (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
                row))
          (export_layout layout)
    | T1 (T_option, t) -> Printf.sprintf "(option %s)" (export_type t)
    | TVariant {layout; row} ->
        Printf.sprintf "(variant (%s) %s (\"\" -1))"
          (String.concat " "
             (List.map
                (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
                row))
          (export_layout layout)
    | T1 (T_set, telement) -> Printf.sprintf "(set %s)" (export_type telement)
    | T2 ((T_map | T_big_map), tkey, tvalue) ->
        Printf.sprintf "(map %s %s)" (export_type tkey) (export_type tvalue)
        (* TODO big ? *)
    | T1 (T_contract, t) -> Printf.sprintf "(contract %s)" (export_type t)
    | TSecretKey -> "secret_key"
    | TUnknown (_, id) -> Printf.sprintf "(unknown %s)" id
    | TTuple (_, Some _) -> assert false
    | TTuple (ts, None) ->
        let ts = List.map (fun (_, t) -> export_type t) ts in
        Printf.sprintf "(tuple %s)" (String.concat " " ts)
    | T1 (T_list, t) -> Printf.sprintf "(list %s)" (export_type t)
    | TLambda ({with_storage; with_operations}, t1, t2) ->
        let f x =
          match Hole.get x with
          | None -> "None"
          | Some b -> if b then "True" else "False"
        in
        let with_storage, tstorage =
          match Hole.get with_storage with
          | None | Some None -> ("None", "None")
          | Some (Some (Read_only, tstorage)) ->
              ("read-only", export_type tstorage)
          | Some (Some (Read_write, tstorage)) ->
              ("read-write", export_type tstorage)
        in
        Printf.sprintf "(lambda %s %s %s %s %s)" with_storage
          (f with_operations) tstorage (export_type t1) (export_type t2)
    | T2 (T_lambda, _, _) -> assert false
    | TSaplingState {memo} ->
        let memo =
          match Hole.get memo with
          | None -> "unknown"
          | Some i -> string_of_int i
        in
        Printf.sprintf "(sapling_state %s)" memo
    | TSaplingTransaction {memo} ->
        let memo =
          match Hole.get memo with
          | None -> "unknown"
          | Some i -> string_of_int i
        in
        Printf.sprintf "(sapling_transaction %s)" memo
    | T1 (T_ticket, t) -> Printf.sprintf "(ticket %s)" (export_type t)
    | T2 ((T_pair _ | T_or _), _, _) -> assert false
  in
  export_type

let no_loc = Atom "None"

let loc = function
  | [] -> no_loc
  | (file, no) :: _ ->
      List [Atom (Printf.sprintf "%S" file); Atom (string_of_int no)]

let rec unbind r (c : Untyped.command) =
  match c.c with
  | CBind (None, c1, c2) -> unbind (c1 :: r) c2
  | _ -> List.rev (c :: r)

let export_contract_id = function
  | C_static {static_id} ->
      List [no_loc; Atom "static_id"; Atom (string_of_int static_id)]
  | C_dynamic {dynamic_id} ->
      List [no_loc; Atom "dynamic_id"; Atom (string_of_int dynamic_id)]

let export_literal : Literal.t -> _ =
  let stringy kind x = List [Atom kind; Atom (Printf.sprintf "%S" x)] in
  let integery kind x = List [Atom kind; Atom (Bigint.string_of_big_int x)] in
  function
  | Unit -> List [Atom "unit"]
  | Bool true -> List [Atom "bool"; Atom "True"]
  | Bool false -> List [Atom "bool"; Atom "False"]
  | Int {i; is_nat} ->
      let c =
        match Hole.get is_nat with
        | Some true -> "nat"
        | Some false -> "int"
        | None -> "intOrNat"
      in
      List [Atom c; Atom (Bigint.string_of_big_int i)]
  | Mutez x -> integery "mutez" x
  | String x -> stringy "string" x
  | Bytes x -> stringy "bytes" x
  | Chain_id x -> stringy "chain_id_cst" x
  | Timestamp x -> integery "timestamp" x
  | Address {address; entry_point} when entry_point = None ->
      stringy "address" address
  | Address _ -> assert false
  | Key x -> stringy "key" x
  | Secret_key x -> stringy "secret_key" x
  | Key_hash x -> stringy "key_hash" x
  | Signature x -> stringy "signature" x
  | Sapling_test_transaction {source; target; amount; memo} ->
      List
        [
          Atom "sapling_test_transaction"
        ; Atom (string_of_int memo)
        ; Atom (Option.default "" source)
        ; Atom (Option.default "" target)
        ; Atom (Big_int.string_of_big_int amount)
        ; Atom "None"
        ]
  | Bls12_381_g1 x -> stringy "bls12_381_g1" x
  | Bls12_381_g2 x -> stringy "bls12_381_g2" x
  | Bls12_381_fr x -> stringy "bls12_381_fr" x
  | Chest_key x -> stringy "chest_key" x
  | Chest x -> stringy "chest" x
  | Sapling_test_state _ -> assert false

let rec export_expr (e : Untyped.expr) =
  let l = loc e.line_no in
  match e.e with
  | EVar ("__storage__", _) -> List [l; Atom "data"]
  | EVar (name, Local) -> List [l; Atom "get_local"; Atom name]
  | EVar (name, Simple) -> List [l; Atom "iter"; Atom name]
  | EVar (_, Scenario) -> assert false
  | EPrivate x -> List [l; Atom "private"; Atom x]
  | ERecord entries ->
      let entries =
        List.map (fun (n, e) -> List [Atom n; export_expr e]) entries
      in
      List (l :: Atom "record" :: entries)
  | EMPrim0 p -> export_mprim0 l p
  | EMPrim1 (p, e) -> export_mprim1 l p e
  | EMPrim2 (p, e1, e2) -> export_mprim2 l p e1 e2
  | EMPrim3 (p, e1, e2, e3) -> export_mprim3 l p e1 e2 e3
  | EPrim0 p -> export_prim0 l p
  | EPrim1 (p, e) -> export_prim1 l p e
  | EPrim2 (p, e1, e2) -> export_prim2 l p e1 e2
  | EPrim3 (p, e1, e2, e3) -> export_prim3 l p e1 e2 e3
  | EItem {items; key; default_value = Some default; missing_message} ->
      assert (missing_message = None);
      List
        [
          l
        ; Atom "get_item_default"
        ; export_expr items
        ; export_expr key
        ; export_expr default
        ]
  | EItem {items; key; default_value; missing_message} ->
      assert (default_value = None);
      assert (missing_message = None);
      List [l; Atom "get_item"; export_expr items; export_expr key]
  | EMap (big, entries) ->
      let entries =
        List.map (fun (k, v) -> List [export_expr k; export_expr v]) entries
      in
      List (l :: Atom (if big then "big_map" else "map") :: entries)
  | ELambda {name; body} ->
      List [l; Atom "lambda"; Atom "-1"; Atom name; List [export_command body]]
  | ETuple xs -> List (l :: Atom "tuple" :: List.map export_expr xs)
  | EList xs -> List (l :: Atom "list" :: List.map export_expr xs)
  | ESet xs -> List (l :: Atom "set" :: List.map export_expr xs)
  | ECreate_contract {contract_template; baker; balance; storage} ->
      List
        [
          l
        ; Atom "create_contract"
        ; List [Atom "contract"; export_contract contract_template]
        ; List [Atom "storage"; export_expr storage]
        ; List [Atom "baker"; export_expr baker]
        ; List [Atom "amount"; export_expr balance]
        ]
  | ETransfer {arg; amount; destination} ->
      List
        [
          l
        ; Atom "transfer"
        ; export_expr arg
        ; export_expr amount
        ; export_expr destination
        ]
  | EOpen_variant (constructor, arg, missing_message) ->
      let missing_message =
        Option.cata (Atom "None") export_expr missing_message
      in
      List
        [
          l
        ; Atom "open_variant"
        ; export_expr arg
        ; Atom constructor
        ; missing_message
        ]
  | ESapling_verify_update {state; transaction} ->
      List
        [
          l
        ; Atom "sapling_verify_update"
        ; export_expr state
        ; export_expr transaction
        ]
  | EMichelson ({name = _; parsed = _; typesIn = _; typesOut = _}, args) ->
      let instr = assert false in
      List ([l; Atom "call_michelson"; instr] @ List.map export_expr args)
  | EMap_function {f; l = l'} ->
      List [l; Atom "map_function"; export_expr l'; export_expr f]
  | EContract {entry_point; arg_type; address} ->
      List
        [
          l
        ; Atom "contract"
        ; Atom (Option.default "\"\"" entry_point)
        ; Atom (export_type arg_type)
        ; export_expr address
        ]
  | ESlice {offset; length; buffer} ->
      List
        [
          l
        ; Atom "slice"
        ; export_expr offset
        ; export_expr length
        ; export_expr buffer
        ]
  | EMake_signature {secret_key; message; message_format} ->
      let message_format =
        match message_format with
        | `Raw -> "raw"
        | `Hex -> "hex"
      in
      List
        [
          l
        ; Atom "make_signature"
        ; export_expr secret_key
        ; export_expr message
        ; Atom message_format
        ]
  | EMatch (scrutinee, clauses) ->
      let export_clause (constructor, e) =
        List [Atom constructor; export_expr e]
      in
      List
        (l :: Atom "ematch" :: export_expr scrutinee
        :: List.map export_clause clauses)
  | EMPrim1_fail _ -> assert false
  | EIf (e1, e2, e3) ->
      List [l; Atom "eif"; export_expr e1; export_expr e2; export_expr e3]
  | EIs_failing e -> List [l; Atom "is_failing"; export_expr e]
  | ECatch_exception (t, e) ->
      List [l; Atom "catch_exception"; export_expr e; Atom (export_type t)]

and export_mprim0 l p =
  let open Michelson_base.Primitive in
  let open Michelson_base.Type in
  let _f name = List [Atom name; l] in
  match (p : mtype prim0) with
  | Amount -> List [l; Atom "amount"]
  | Balance -> List [l; Atom "balance"]
  | Chain_id -> List [l; Atom "chain_id"]
  | Level -> List [l; Atom "level"]
  | Now -> List [l; Atom "now"]
  | Self None -> List [l; Atom "self"]
  | Self (Some s) -> List [l; Atom "self"; Atom s]
  | Self_address -> List [l; Atom "self_address"]
  | Sender -> List [l; Atom "sender"]
  | Source -> List [l; Atom "source"]
  | Total_voting_power -> List [l; Atom "total_voting_power"]
  | Empty_bigmap _
  | Empty_map _
  | Empty_set _
  | Nil _
  | None_ _
  | Sapling_empty_state _
  | Unit_ -> assert false

and export_mprim1 l p e =
  let open Michelson_base.Primitive in
  let open Michelson_base.Type in
  let f name = List [l; Atom name; export_expr e] in
  match (p : mtype prim1) with
  | Abs -> f "abs"
  | Not -> f "not"
  | IsNat -> f "is_nat"
  | Blake2b -> f "blake2b"
  | Sha256 -> f "sha256"
  | Sha512 -> f "sha512"
  | Keccak -> f "keccak"
  | Sha3 -> f "sha3"
  | Hash_key -> f "hash_key"
  | Car
  | Cdr
  | Some_
  | Eq
  | Neg
  | Int
  | Neq
  | Le
  | Lt
  | Ge
  | Gt
  | Concat1
  | Size
  | Address
  | Implicit_account
  | Pack
  | Set_delegate
  | Read_ticket
  | Join_tickets
  | Pairing_check
  | Voting_power
  | Left _
  | Right _
  | Contract _
  | Unpack _
  | Getn _
  | Cast _
  | Rename _
  | Emit _ -> assert false

and export_mprim2 l p e1 e2 =
  let open Michelson_base.Primitive in
  let f name = List [l; Atom name; export_expr e1; export_expr e2] in
  match (p : _ prim2) with
  | Lsl -> f "lsl"
  | Lsr -> f "lsr"
  | Mul -> f "MUL"
  | Add -> f "ADD"
  | Compare -> f "COMPARE"
  | Sub
  | Sub_mutez
  | Xor
  | Ediv
  | And
  | Or
  | Cons
  | Concat2
  | Get
  | Mem
  | Exec
  | Apply
  | Sapling_verify_update
  | Ticket
  | Ticket_deprecated
  | Split_ticket
  | Pair _
  | Updaten _
  | View _ -> assert false

and export_mprim3 l p e1 e2 e3 =
  let open Michelson_base.Primitive in
  let f name =
    List [l; Atom name; export_expr e1; export_expr e2; export_expr e3]
  in
  match (p : prim3) with
  | Check_signature -> f "check_signature"
  | Slice | Update | Get_and_update | Transfer_tokens | Open_chest ->
      assert false

and export_prim0 l (p : _ prim0) =
  match p with
  | ELiteral x -> List [l; Atom "literal"; export_literal x]
  | EContract_data id -> List [l; Atom "contract_data"; export_contract_id id]
  | ECst_contract _ -> assert false
  | EBounded x -> List [l; Atom "bounded"; export_literal x]
  | EMeta_local x -> List [l; Atom "get_meta_local"; Atom x]
  | EMatch_cons x -> List [l; Atom "match_cons"; Atom x]
  | EAccount_of_seed {seed} -> List [l; Atom "account_of_seed"; Atom seed]
  | EContract_address (x, ep) ->
      let ep = Option.default "\"\"" ep in
      List [l; Atom "contract_address"; export_contract_id x; Atom ep]
  | EContract_balance x ->
      List [l; Atom "contract_balance"; export_contract_id x]
  | EContract_baker x -> List [l; Atom "contract_baker"; export_contract_id x]
  | EContract_typed (x, ep) ->
      let ep = Option.default "" ep in
      List [l; Atom "contract_typed"; export_contract_id x; Atom ep]
  | EContract_entrypoint_map x ->
      List [l; Atom "contract_entrypoint_map"; export_contract_id x]
  | EContract_entrypoint_id (x, ep_name) ->
      List
        [l; Atom "contract_entrypoint_id"; export_contract_id x; Atom ep_name]
  | EConstant (e, t) -> List [l; Atom "constant"; Atom e; Atom (export_type t)]
  | EConstant_var e -> List [l; Atom "constant_scenario_var"; Atom e]
  | EEntrypoint_map -> List [l; Atom "entrypoint_map"]
  | EEntrypoint_id ep -> List [l; Atom "entrypoint_id"; Atom ep]

and export_prim1 l (p : _ prim1) e =
  let f name = List [l; Atom name; export_expr e] in
  match p with
  | EAttr name -> List [l; Atom "attr"; export_expr e; Atom name]
  | EType_annotation t ->
      List [l; Atom "type_annotation"; export_expr e; Atom (export_type t)]
  | EProject 0 -> f "first"
  | EProject 1 -> f "second"
  | ESum -> f "sum"
  | ENeg -> f "neg"
  | ESign -> f "sign"
  | ESize -> f "size"
  | ETo_int -> f "to_int"
  | EPack -> f "pack"
  | EVariant cons -> List [l; Atom "variant"; Atom cons; export_expr e]
  | EConcat_list -> f "concat"
  | EResolve -> f "resolve"
  | EList_rev -> f "rev"
  | EList_items rev -> f (if rev then "rev_items" else "items")
  | EList_keys rev -> f (if rev then "rev_keys" else "keys")
  | EList_values rev -> f (if rev then "rev_values" else "values")
  | EList_elements rev -> f (if rev then "rev_elements" else "elements")
  | ERead_ticket -> f "read_ticket"
  | EJoin_tickets -> f "join_tickets"
  | ESet_delegate -> f "set_delegate"
  | EProject _ -> assert false
  | EAddress -> f "to_address"
  | EImplicit_account -> f "implicit_account"
  | EPairing_check -> f "pairing_check"
  | EVoting_power -> f "voting_power"
  | EUnbounded -> f "unbounded"
  | EUnpack t -> List [l; Atom "unpack"; export_expr e; Atom (export_type t)]
  | EIs_variant c -> List [l; Atom "is_variant"; export_expr e; Atom c]
  | EConvert -> f "convert"
  | EStatic_view (static_id, view) ->
      List
        [
          l
        ; Atom "static_view"
        ; export_contract_id (C_static static_id)
        ; Atom view
        ]
  | EEmit _ -> assert false

and export_prim2 l (p : _ prim2) e1 e2 =
  let f name = List [l; Atom name; export_expr e1; export_expr e2] in
  let f_swapped name = List [l; Atom name; export_expr e2; export_expr e1] in
  match p with
  | EApply_lambda -> f_swapped "apply_lambda"
  | ECall_lambda -> f_swapped "call_lambda"
  | ENeq -> f "neq"
  | EEq -> f "eq"
  | EAnd -> f "and"
  | EOr -> f "or"
  | EAdd -> f "add"
  | ESub -> f "sub"
  | EDiv -> f "div"
  | EEDiv -> f "ediv"
  | EMod -> f "mod"
  | EMul_homo -> f "mul_homo"
  | ELt -> f "lt"
  | ELe -> f "le"
  | EGt -> f "gt"
  | EGe -> f "ge"
  | EXor -> f "xor"
  | EMin -> f "min"
  | EMax -> f "max"
  | EContains -> f_swapped "contains"
  | EGet_opt -> List [l; Atom "get_opt"; export_expr e2; export_expr e1]
  | EAdd_seconds -> f "add_seconds"
  | ECons -> f "cons"
  | ETicket -> f "ticket"
  | ESplit_ticket -> f "split_ticket"
  | EView (name, return_type) ->
      List
        [
          l
        ; Atom "view"
        ; Atom name
        ; export_expr e1
        ; export_expr e2
        ; Atom (export_type return_type)
        ]

and export_prim3 l (p : prim3) e1 e2 e3 =
  let f name =
    List [l; Atom name; export_expr e1; export_expr e2; export_expr e3]
  in
  match p with
  | ERange -> f "range"
  | ESplit_tokens -> f "split_tokens"
  | EUpdate_map ->
      List
        [l; Atom "update_map"; export_expr e3; export_expr e1; export_expr e2]
  | EGet_and_update ->
      List
        [
          l
        ; Atom "get_and_update"
        ; export_expr e3
        ; export_expr e1
        ; export_expr e2
        ]
  | ETest_ticket -> f "test_ticket"

and export_command (c : Untyped.command) =
  let l = loc c.line_no in
  match c.c with
  | CDefine_local {var; rhs; is_mutable} ->
      if is_mutable
      then List [l; Atom "define_local"; Atom var; export_expr rhs]
      else
        List [l; Atom "define_local"; Atom var; export_expr rhs; Atom "False"]
  | CVerify (e, None) -> List [l; Atom "verify"; export_expr e]
  | CVerify (e, Some msg) ->
      List [l; Atom "verify"; export_expr e; export_expr msg]
  | CSet_var (lhs, rhs) ->
      List [l; Atom "set"; export_expr lhs; export_expr rhs]
  | CBind (None, _, _) -> List (List.map export_command (unbind [] c))
  | CBind (Some x, c1, c2) ->
      List [List [Atom "bind"; Atom x; export_command c1]; export_command c2]
  | CSet_type (e, t) ->
      List [l; Atom "set_type"; export_expr e; Atom (export_type t)]
  | CResult e -> List [l; Atom "result"; export_expr e]
  | CFailwith e -> List [l; Atom "failwith"; export_expr e]
  | CNever e -> List [l; Atom "never"; export_expr e]
  | CIf (cond, t, f) ->
      List
        [
          List [l; Atom "if_block"; export_expr cond; List [export_command t]]
        ; List [Atom "else_block"; List [export_command f]]
        ]
  | CWhile (cond, body) ->
      List [l; Atom "while_block"; export_expr cond; List [export_command body]]
  | CFor (name, e, body) ->
      List
        [
          l
        ; Atom "for_group"
        ; Atom name
        ; export_expr e
        ; List [export_command body]
        ]
  | CMatch (scrutinee, cases) ->
      let f (constructor, arg_name, body) =
        List
          [
            l
          ; Atom "match"
          ; Atom "None"
          ; Atom constructor
          ; Atom arg_name
          ; List [export_command body]
          ]
      in
      List
        [
          l
        ; Atom "match_cases"
        ; export_expr scrutinee
        ; Atom "None"
        ; List (List.map f cases)
        ]
  | CTrace e -> List [l; Atom "trace"; export_expr e]
  | CMatch_product _ | CModify_product _ | CMatch_cons _ -> assert false
  | CDel_item (e, k) -> List [l; Atom "del_item"; export_expr e; export_expr k]
  | CUpdate_set (e, k, b) ->
      List [l; Atom "update_set"; export_expr e; export_expr k; export_bool b]
  | CComment _ -> assert false
  | CSet_result_type (c, t) ->
      List
        [
          l
        ; Atom "set_result_type"
        ; List [export_command c]
        ; Atom (export_type t)
        ]

and export_entry_point
    {
      channel
    ; tparameter_ep
    ; originate
    ; lazify
    ; lazy_no_code
    ; check_no_incoming_transfer
    ; line_no
    ; body
    } =
  let has_param, ep_type =
    match tparameter_ep with
    | `Present -> (true, Atom "None")
    | `Annotated t -> (true, Atom (export_type t))
    | `Absent -> (false, Atom "None")
  in
  List
    [
      Atom channel
    ; export_bool originate
    ; export_option export_bool lazify
    ; export_option export_bool lazy_no_code
    ; export_option export_bool check_no_incoming_transfer
    ; export_bool has_param
    ; ep_type
    ; loc line_no
    ; List [export_command body]
    ]

and export_contract
    ({
       template_id
     ; balance
     ; storage
     ; baker = _ (* TODO handle baker in import.ml? *)
     ; tstorage_explicit
     ; entrypoints
     ; entrypoints_layout = _
     ; unknown_parts = _ (* TODO move this into "derived"? *)
     ; flags
     ; private_variables
     ; metadata = _
     ; views = _
     } :
      _ contract_f) =
  let id =
    match template_id with
    | None -> Atom "(None static_id -1)" (* TODO *)
    | Some id -> export_contract_id (C_static id)
  in
  let balance =
    match balance with
    | None -> List []
    | Some x -> export_expr x
  in
  let tstorage_explicit =
    match tstorage_explicit with
    | None -> List []
    | Some t -> Atom (export_type t)
  in
  List
    [
      Atom "template_id"
    ; id
    ; Atom "storage"
    ; Option.cata (List []) export_expr storage
    ; Atom "storage_type"
    ; List [tstorage_explicit]
    ; Atom "entry_points"
    ; List (List.map export_entry_point entrypoints)
    ; Atom "flags"
    ; List
        (List.map
           (fun x ->
             List (List.map (fun x -> Atom x) (Config.string_of_flag x)))
           flags)
    ; Atom "privates"
    ; List
        (List.map
           (fun (x, e) -> List [Atom x; export_expr e])
           private_variables)
    ; Atom "views"
    ; List []
    ; Atom "entry_points_layout"
    ; List []
    ; Atom "initial_metadata"
    ; List []
    ; Atom "balance"
    ; balance
    ]

let export_action = function
  | New_contract
      {id; contract; line_no; accept_unknown_types; show; address = _} ->
      `Assoc
        [
          ("action", `String "newContract")
        ; ("accept_unknown_types", `Bool accept_unknown_types)
        ; ("export", `String (display_sexpr (export_contract contract)))
        ; ("id", `String (display_sexpr (export_contract_id id)))
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ; ("show", `Bool show)
        ]
  | Message
      {
        id
      ; valid
      ; exception_ = _
      ; params
      ; line_no
      ; title = _
      ; messageClass = _
      ; amount = _
      ; context = _
      ; message
      ; show = _
      ; export = _
      } ->
      `Assoc
        [
          ("action", `String "message")
        ; ("id", `String (display_sexpr (export_contract_id id)))
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ; ("message", `String message)
        ; ("params", `String (display_sexpr (export_expr params)))
        ; ("valid", `String (display_sexpr (export_expr valid)))
        ]
  | Verify {condition; line_no} ->
      `Assoc
        [
          ("action", `String "verify")
        ; ("condition", `String (display_sexpr (export_expr condition)))
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ]
  | Html {tag; inner; line_no} ->
      `Assoc
        [
          ("action", `String "html")
        ; ("inner", `String inner)
        ; ("tag", `String tag)
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ]
  | Show {expression; html; stripStrings; compile; line_no} ->
      `Assoc
        [
          ("action", `String "show")
        ; ("expression", `String (display_sexpr (export_expr expression)))
        ; ("html", `Bool html)
        ; ("stripStrings", `Bool stripStrings)
        ; ("compile", `Bool compile)
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ]
  | Compute {var; expression; line_no} ->
      `Assoc
        [
          ("action", `String "compute")
        ; ("id", `String var)
        ; ("expression", `String (display_sexpr (export_expr expression)))
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ]
  | Simulation {id; line_no} ->
      `Assoc
        [
          ("action", `String "simulation")
        ; ("id", `String (display_sexpr (export_contract_id id)))
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ]
  | ScenarioError _ -> `Assoc [("action", `String "error")]
  | Exception _ -> assert false
  | Set_delegate _ -> assert false
  | DynamicContract {id; model_id; line_no} ->
      `Assoc
        [
          ("action", `String "dynamic_contract")
        ; ("id", `String (display_sexpr (export_contract_id (C_dynamic id))))
        ; ("model_id", `String (display_sexpr (export_contract_id model_id)))
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ]
  | Add_flag {flag; line_no} ->
      `Assoc
        [
          ("action", `String "flag")
        ; ( "flag"
          , `List (List.map (fun x -> `String x) (Config.string_of_flag flag))
          )
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ]
  | Prepare_constant_value {var; hash; expression; line_no} ->
      let hash =
        match hash with
        | None -> "None"
        | Some x -> x
      in
      `Assoc
        [
          ("action", `String "value")
        ; ("var", `String var)
        ; ("hash", `String hash)
        ; ("expression", `String (display_sexpr (export_expr expression)))
        ; ("line_no", `String (display_sexpr (loc line_no)))
        ]
  | Mutation_test _ -> assert false

let export_scenario ({shortname; longname; kind; actions} : Basics.scenario) =
  `Assoc
    [
      ("shortname", `String shortname)
    ; ("longname", `String longname)
    ; ("scenario", `List (List.map export_action (List.map snd actions)))
    ; ("kind", `String kind.kind)
    ]

let export_scenarios scenarios =
  let scenarios = List.map export_scenario scenarios in
  let scenarios = `List scenarios in
  Yojson.Safe.pretty_to_string scenarios
