(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Expr
open Michelson_base.Protocol

type state = {var_counter : int ref}

val fresh : state -> string -> string

val freshen : state -> string -> string list -> string list

val simplify :
  protocol:protocol -> state -> expr precontract -> expr precontract

val smartMLify :
  protocol:protocol -> state -> expr precontract -> expr precontract

val michelsonify :
  protocol:protocol -> state -> expr precontract -> expr precontract
