(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Ppxlib

let () =
  Ppx_smartml_lib.Plumbing.register_via_ppxlib ();
  Driver.run_as_ppx_rewriter ()
