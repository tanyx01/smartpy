(* Copyright 2019-2022 Smart Chain Arena LLC. *)

val ast_mapper : Ast_mapper.mapper

val register_via_ast_mapper : unit -> unit

val register_via_ppxlib : unit -> unit
