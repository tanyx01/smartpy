val parse_expr : Ppxlib.expression -> Ppxlib.expression

val parse_command : Ppxlib.expression -> Ppxlib.expression

val parse_type : Ppxlib.expression -> Ppxlib.expression

val expand_entry_point :
  Ppxlib.rec_flag -> Ppxlib.value_binding list -> Ppxlib.structure_item

module Scenario : sig
  val expand_scenario :
       scenario_kind:string
    -> Ppxlib.rec_flag
    -> Ppxlib.value_binding list
    -> Ppxlib.structure_item

  val parse_actions : Ppxlib.expression -> Ppxlib.expression
end
