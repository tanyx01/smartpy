(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Ppxlib

let ast_mapper =
  let open Ppxlib_ast.Selected_ast in
  let expr e =
    let get = function
      | PStr [{pstr_desc = Pstr_eval (e, []); _}] -> e
      | _ -> assert false
    in
    match e with
    | {pexp_desc = Pexp_extension ({txt = "expr"; _}, pl); _} ->
        Transformer.parse_expr (get pl)
    | {pexp_desc = Pexp_extension ({txt = "typ"; _}, pl); _} ->
        Transformer.parse_type (get pl)
    | {pexp_desc = Pexp_extension ({txt = "command"; _}, pl); _} ->
        Transformer.parse_command (get pl)
    | {pexp_desc = Pexp_extension ({txt = "actions"; _}, pl); _} ->
        Transformer.Scenario.parse_actions (get pl)
    | e -> e
  in
  let expr mapper e =
    let e = Ocaml_common.Ast_mapper.default_mapper.expr mapper e in
    let e = Of_ocaml.copy_expression e in
    let e = expr e in
    To_ocaml.copy_expression e
  in
  let structure_item = function
    | {pstr_desc = Pstr_extension (({txt = "entry_point"; _}, pl), _); _} ->
        let get_bindings = function
          | PStr [{pstr_desc = Pstr_value (recflag, bindings); _}] ->
              (recflag, bindings)
          | _ -> assert false
        in
        let recflag, bindings = get_bindings pl in
        Transformer.expand_entry_point recflag bindings
    | si -> si
  in
  let structure mapper s =
    let open Ppxlib_ast.Selected_ast in
    let s = Ocaml_common.Ast_mapper.default_mapper.structure mapper s in
    let s = Of_ocaml.copy_structure s in
    let s = List.map structure_item s in
    To_ocaml.copy_structure s
  in
  {Ocaml_common.Ast_mapper.default_mapper with expr; structure}

(** Doesn't work under #use in js_of_ocaml-toplevel. *)
let register_via_ast_mapper () =
  Ocaml_common.Ast_mapper.register "ppx_smartml" (fun _ -> ast_mapper)

let register_via_ppxlib () =
  let open Extension.Context in
  let open Ast_pattern in
  let register name ctxt pat expand =
    let extensions = [Extension.V3.declare name ctxt pat expand] in
    Driver.register_transformation ~extensions name
  in
  register "expr" expression (single_expr_payload __) (fun ~ctxt:_ ->
      Transformer.parse_expr);
  register "typ" expression (single_expr_payload __) (fun ~ctxt:_ ->
      Transformer.parse_type);
  register "actions" expression (single_expr_payload __) (fun ~ctxt:_ ->
      Transformer.Scenario.parse_actions);
  register "command" expression (single_expr_payload __) (fun ~ctxt:_ ->
      Transformer.parse_command);
  register "entry_point" structure_item
    (pstr (pstr_value __ __ ^:: nil))
    (fun ~ctxt:_ -> Transformer.expand_entry_point);
  register "smartml_test" structure_item
    (pstr (pstr_value __ __ ^:: nil))
    (fun ~ctxt:_ -> Transformer.Scenario.expand_scenario ~scenario_kind:"test")
