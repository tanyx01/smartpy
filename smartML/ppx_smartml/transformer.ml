(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Ppxlib

exception Err of location * string

let () =
  let open Location.Error in
  register_error_of_exn (function
    | Err (loc, msg) -> Some (make ~loc msg ~sub:[])
    | _ -> None)

let err loc = Printf.ksprintf (fun msg -> raise (Err (loc, msg)))

let err_unknown_identifier loc x = err loc "Unknown identifier: %s" x

let err_arity loc ident expected got =
  let numeral = function
    | 0 -> "zero"
    | 1 -> "one"
    | 2 -> "two"
    | 3 -> "three"
    | n -> Printf.sprintf "%d" n
  in
  let args = function
    | 1 -> "argument"
    | _ -> "arguments"
  in
  err loc "%S expects %s %s, but got %s" ident (numeral expected)
    (args expected) (numeral got)

let err_expected loc expected = err loc "Expected: %s" expected

let lift_int ?loc x =
  Ast_helper.Exp.constant ?loc (Pconst_integer (string_of_int x, None))

let lift_bool loc = function
  | true -> [%expr true]
  | false -> [%expr false]

let lift_string ~loc x =
  Ast_helper.Exp.constant ~loc (Pconst_string (x, loc, None))

let rec lift_list loc = function
  | [] -> [%expr []]
  | a :: l -> [%expr [%e a] :: [%e lift_list loc l]]

let parse_cons_chain =
  let rec parse r = function
    | [%expr [%e? x] :: [%e? y]] -> parse (x :: r) y
    | y -> (List.rev r, y)
  in
  parse []

let parse_list loc xs =
  match parse_cons_chain xs with
  | r, [%expr []] -> r
  | _ -> err_expected loc "list"

let expr_of_lid lid =
  match lid.txt with
  | Lident s -> lift_string s
  | _ -> assert false

let show_pexpdesc pexp_desc =
  match pexp_desc with
  | Pexp_record _ -> "Pexp_record"
  | Pexp_field _ -> "Pexp_field"
  | Pexp_ident _ -> "Pexp_ident"
  | Pexp_setfield _ -> "Pexp_setfield"
  | Pexp_apply _ -> "Pexp_apply"
  | Pexp_sequence _ -> "Pexp_sequence"
  | Pexp_constant _ -> "Pexp_constant"
  | Pexp_let _ -> "Pexp_let"
  | Pexp_function _ -> "Pexp_function"
  | Pexp_fun _ -> "Pexp_fun"
  | Pexp_match _ -> "Pexp_match"
  | Pexp_try _ -> "Pexp_try"
  | Pexp_tuple _ -> "Pexp_tuple"
  | Pexp_construct _ -> "Pexp_construct"
  | Pexp_variant _ -> "Pexp_variant"
  | Pexp_array _ -> "Pexp_array"
  | Pexp_ifthenelse _ -> "Pexp_ifthenelse"
  | Pexp_while _ -> "Pexp_while"
  | Pexp_for _ -> "Pexp_for"
  | Pexp_constraint _ -> "Pexp_constraint"
  | Pexp_coerce _ -> "Pexp_coerce"
  | Pexp_send _ -> "Pexp_send"
  | Pexp_new _ -> "Pexp_new"
  | Pexp_setinstvar _ -> "Pexp_setinstvar"
  | Pexp_override _ -> "Pexp_override"
  | Pexp_letmodule _ -> "Pexp_letmodule"
  | Pexp_letexception _ -> "Pexp_letexception"
  | Pexp_assert _ -> "Pexp_assert"
  | Pexp_lazy _ -> "Pexp_lazy"
  | Pexp_poly _ -> "Pexp_poly"
  | Pexp_object _ -> "Pexp_object"
  | Pexp_newtype _ -> "Pexp_newtype"
  | Pexp_pack _ -> "Pexp_pack"
  | Pexp_open _ -> "Pexp_open"
  | Pexp_letop _ -> "Pexp_letop"
  | Pexp_extension _ -> "Pexp_extension"
  | Pexp_unreachable -> "Pexp_unreachable"

let failtdesc = function
  | Ptyp_any -> failwith "failtdesc Ptyp_any"
  | Ptyp_var _ -> failwith "failtdesc Ptyp_var"
  | Ptyp_arrow _ -> failwith "failtdesc Ptyp_arrow"
  | Ptyp_tuple _ -> failwith "failtdesc Ptyp_tuple"
  | Ptyp_constr _ -> failwith "failtdesc Ptyp_constr"
  | Ptyp_object _ -> failwith "failtdesc Ptyp_object"
  | Ptyp_class _ -> failwith "failtdesc Ptyp_class"
  | Ptyp_alias _ -> failwith "failtdesc Ptyp_alias"
  | Ptyp_variant _ -> failwith "failtdesc Ptyp_variant"
  | Ptyp_poly _ -> failwith "failtdesc Ptyp_poly"
  | Ptyp_package _ -> failwith "failtdesc Ptyp_package"
  | Ptyp_extension _ -> failwith "failtdesc Ptyp_extension"

(* This must be global or else the result will not be shared between
   invocations of %expr. Obviously this is less than ideal. TODO Make
   static contract ids into strings and treat them uniformly with
   other variables? *)

let contract_static_ids : (string, int) Hashtbl.t = Hashtbl.create 5

type env = {
    actions : expression list ref
  ; next_id : unit -> int
}

let line_no_of_loc loc =
  lift_list loc [[%expr "", [%e lift_int loc.loc_start.pos_lnum]]]

let lookup_prim loc = function
  | "Map.update" -> Some (3, [%expr Expr.update_map])
  | "Map.values" -> Some (1, [%expr fun l -> Expr.list_values l false])
  | "Map.keys" ->
      Some (1, [%expr fun ~line_no x -> Expr.list_keys ~line_no x false])
  | "Map.items" ->
      Some (1, [%expr fun ~line_no x -> Expr.list_items ~line_no x false])
  | "Set.elements" ->
      Some (1, [%expr fun ~line_no x -> Expr.list_elements ~line_no x false])
  | "fst" -> Some (1, [%expr Expr.first])
  | "snd" -> Some (1, [%expr Expr.second])
  | "~-" -> Some (1, [%expr Expr.negE])
  | "not" -> Some (1, [%expr Expr.notE])
  | "abs" -> Some (1, [%expr Expr.absE])
  | "implicit_account" -> Some (1, [%expr Expr.implicit_account])
  | "is_nat" -> Some (1, [%expr Expr.is_nat])
  | "left" -> Some (1, [%expr Expr.left])
  | "right" -> Some (1, [%expr Expr.right])
  | "sign" -> Some (1, [%expr Expr.signE])
  | "some" -> Some (1, [%expr Expr.some])
  | "sum" -> Some (1, [%expr Expr.sum])
  | "to_int" -> Some (1, [%expr Expr.to_int])
  | "voting_power" -> Some (1, [%expr Expr.voting_power])
  | "concat_list" -> Some (1, [%expr Expr.concat_list])
  | "size" -> Some (1, [%expr Expr.size])
  | "unbounded" -> Some (1, [%expr Expr.unbounded])
  | "pack" -> Some (1, [%expr Expr.pack])
  | "resolve" -> Some (1, [%expr Expr.resolve])
  | "to_address" -> Some (1, [%expr Expr.to_address])
  | "list_rev" -> Some (1, [%expr Expr.list_rev])
  | "set_delegate_operation" -> Some (1, [%expr Expr.set_delegate])
  | "read_ticket" -> Some (1, [%expr Expr.read_ticket])
  | "join_tickets" -> Some (1, [%expr Expr.join_tickets])
  | "pairing_check" -> Some (1, [%expr Expr.pairing_check])
  | "hash_key" -> Some (1, [%expr Expr.hash_key])
  | "blake2b" -> Some (1, [%expr Expr.blake2b])
  | "sha256" -> Some (1, [%expr Expr.sha256])
  | "sha512" -> Some (1, [%expr Expr.sha512])
  | "keccak" -> Some (1, [%expr Expr.keccak])
  | "sha3" -> Some (1, [%expr Expr.sha3])
  | "open_some" -> Some (1, [%expr fun x -> Expr.open_variant "Some" x None])
  | "convert" -> Some (1, [%expr Expr.convert])
  | "=" -> Some (2, [%expr Expr.eq])
  | "<>" -> Some (2, [%expr Expr.neq])
  | "*" -> Some (2, [%expr Expr.mul_homo])
  | ">" -> Some (2, [%expr Expr.gt])
  | ">=" -> Some (2, [%expr Expr.ge])
  | "<" -> Some (2, [%expr Expr.lt])
  | "<=" -> Some (2, [%expr Expr.le])
  | "/" -> Some (2, [%expr Expr.div])
  | "%" -> Some (2, [%expr Expr.mod_])
  | "ediv" -> Some (2, [%expr Expr.ediv])
  | "+" -> Some (2, [%expr Expr.add])
  | "-" -> Some (2, [%expr Expr.sub])
  | "||" -> Some (2, [%expr Expr.or_])
  | "&&" -> Some (2, [%expr Expr.and_])
  | "mul" -> Some (2, [%expr Expr.mul_prim])
  | "add" -> Some (2, [%expr Expr.add_prim])
  | "sub_mutez" -> Some (2, [%expr Expr.sub_mutez])
  | "min" -> Some (2, [%expr Expr.min])
  | "max" -> Some (2, [%expr Expr.max])
  | "xor" -> Some (2, [%expr Expr.xor])
  | "contains" -> Some (2, [%expr Expr.contains])
  | "cons" -> Some (2, [%expr Expr.cons])
  | "add_seconds" -> Some (2, [%expr Expr.add_seconds])
  | "apply_lambda" -> Some (2, [%expr Expr.apply_lambda])
  | "ticket" -> Some (2, [%expr Expr.ticket])
  | "split_ticket" -> Some (2, [%expr Expr.split_ticket])
  | "get_opt" -> Some (2, [%expr Expr.get_opt])
  | "lsl" -> Some (2, [%expr Expr.lsl_])
  | "lsr" -> Some (2, [%expr Expr.lsr_])
  | "map" -> Some (2, [%expr fun f xs -> Library.map_function_full_stack xs f])
  | "check_signature" -> Some (3, [%expr Expr.check_signature])
  | "range" -> Some (3, [%expr Expr.range])
  | "split_tokens" -> Some (3, [%expr Expr.split_tokens])
  | "test_ticket" -> Some (3, [%expr Expr.test_ticket])
  | "get_and_update" -> Some (3, [%expr Expr.get_and_update])
  | "transfer_operation" ->
      Some
        ( 3
        , [%expr
            fun arg amount destination ->
              Expr.transfer ~arg ~amount ~destination] )
  | _ -> None

let parse_constant loc =
  let line_no = line_no_of_loc loc in
  function
  | Pconst_integer (i, _) ->
      [%expr
        Expr.literal ~line_no:[%e line_no]
          (Literal.intOrNat (Hole.mk ())
             (Big_int.big_int_of_string [%e lift_string ~loc i]))]
  | Pconst_string (x, _, None) ->
      [%expr
        Expr.literal ~line_no:[%e line_no]
          (Literal.string [%e lift_string ~loc x])]
  | _ -> err_expected loc "constant"

let label_of_longindent = function
  | Lident ident -> ident
  | Ldot (Lident mod_, ident) -> Printf.sprintf "%s.%s" mod_ ident
  | _ -> assert false

let stringy_literal check loc kind f =
  let line_no = line_no_of_loc loc in
  function
  | {pexp_desc = Pexp_constant (Pconst_string (x, _, None)); _} ->
      if not (check x) then err_expected loc (kind ^ " literal");
      [%expr
        Expr.literal ~line_no:[%e line_no] ([%e f] [%e lift_string ~loc x])]
  | _ -> err_expected loc (kind ^ " literal")

let string_bytes s =
  String.length s mod 2 = 0
  && Base.String.is_prefix s ~prefix:"0x"
  && Base.String.for_all (Base.String.chop_prefix_exn s ~prefix:"0x")
       ~f:(fun x -> Base.String.contains "0123456789abcdef" x)

let has_prefix prefixes s =
  List.exists (fun prefix -> Base.String.is_prefix s ~prefix) prefixes

let string_key_hash = has_prefix ["tz1"; "tz2"; "tz3"]

let string_address = has_prefix ["KT1"; "tz1"; "tz2"; "tz3"]

let string_key = has_prefix ["edpk"]

let string_secret_key = has_prefix ["edsk"; "spsk"; "p2esk"]

let string_signature = has_prefix ["sig"; "edsig"; "spsig1"; "p2sig"]

let integery_literal loc kind f =
  let line_no = line_no_of_loc loc in
  function
  | {pexp_desc = Pexp_constant (Pconst_integer (x, None)); _} ->
      [%expr
        Expr.literal ~line_no:[%e line_no]
          ([%e f] (Big_int.big_int_of_string [%e lift_string ~loc x]))]
  | _ -> err_expected loc (kind ^ " literal")

let rec parse_expr ~env =
  let contract_id var = Hashtbl.find_opt contract_static_ids var in
  let rec parse_expr expr =
    let loc = expr.pexp_loc in
    let line_no = line_no_of_loc loc in
    match expr with
    | [%expr ()] -> [%expr Library.unit ~line_no:[%e line_no]]
    | [%expr true] ->
        [%expr Expr.literal ~line_no:[%e line_no] (Literal.bool true)]
    | [%expr false] ->
        [%expr Expr.literal ~line_no:[%e line_no] (Literal.bool false)]
    | [%expr None] -> [%expr Expr.none ~line_no:[%e line_no]]
    | [%expr Some [%e? x]] ->
        [%expr Expr.some ~line_no:[%e line_no] [%e parse_expr x]]
    | [%expr private_ [%e? x]] -> (
        match x with
        | {pexp_desc = Pexp_ident {txt = Lident s; _}; _} ->
            [%expr Expr.private_ ~line_no:[%e line_no] [%e lift_string ~loc s]]
        | _ -> err_expected loc "identifier")
    | [%expr fun [%p? x] -> [%e? body]] -> (
        match x.ppat_desc with
        | Ppat_var {txt; loc} ->
            [%expr
              Expr.lambda ~line_no:[%e line_no] [%e lift_string ~loc txt]
                [%e parse_command ~env body] ~with_storage:None
                ~with_operations:false ~recursive:None]
        | _ -> err_expected x.ppat_loc "variable")
    | {pexp_desc = Pexp_constant c; _} -> parse_constant loc c
    | [%expr bytes [%e? x]] ->
        stringy_literal string_bytes loc "bytes" [%expr Literal.bytes] x
    | [%expr chain_id_cst [%e? x]] ->
        stringy_literal string_bytes loc "chain id" [%expr Literal.chain_id] x
    | [%expr nat [%e? x]] -> integery_literal loc "nat" [%expr Literal.nat] x
    | [%expr int [%e? x]] -> integery_literal loc "int" [%expr Literal.int] x
    | [%expr tez [%e? x]] -> integery_literal loc "tez" [%expr Library.tez] x
    | [%expr mutez [%e? x]] ->
        integery_literal loc "mutez" [%expr Literal.mutez] x
    | [%expr timestamp [%e? x]] ->
        integery_literal loc "timestamp" [%expr Literal.timestamp] x
    | [%expr address [%e? x]] ->
        stringy_literal string_address loc "address" [%expr Literal.address] x
    | [%expr key [%e? x]] ->
        stringy_literal string_key loc "key" [%expr Literal.key] x
    | [%expr secret_key [%e? x]] ->
        stringy_literal string_secret_key loc "secret_key"
          [%expr Literal.secret_key] x
    | [%expr key_hash [%e? x]] ->
        stringy_literal string_key_hash loc "key_hash" [%expr Literal.key_hash]
          x
    | [%expr signature [%e? x]] ->
        stringy_literal string_signature loc "signature"
          [%expr Literal.signature] x
    | [%expr bls12_381_g1 [%e? x]] ->
        stringy_literal string_bytes loc "bls12_381_g1"
          [%expr Literal.bls12_381_g1] x
    | [%expr bls12_381_g2 [%e? x]] ->
        stringy_literal string_bytes loc "bls12_381_g2"
          [%expr Literal.bls12_381_g2] x
    | [%expr bls12_381_fr [%e? x]] ->
        stringy_literal string_bytes loc "bls12_381_fr"
          [%expr Literal.bls12_381_fr] x
    | [%expr Map.get [%e? m] [%e? x]] ->
        [%expr
          Expr.item ~line_no:[%e line_no] [%e parse_expr m] [%e parse_expr x]
            None None]
    | [%expr Map.get ~message:[%e? message] [%e? m] [%e? x]] -> (
        match message with
        | {pexp_desc = Pexp_constant (Pconst_string (message, _, None)); _} ->
            [%expr
              Expr.item ~line_no:[%e line_no] [%e parse_expr m]
                [%e parse_expr x] None
                (Some [%e lift_string ~loc message])]
        | _ -> err_expected loc "Map.get: ~message expects a literal string")
    | [%expr Map.get ~default_value:[%e? default_value] [%e? m] [%e? x]] ->
        [%expr
          Expr.item ~line_no:[%e line_no] [%e parse_expr m] [%e parse_expr x]
            (Some [%e parse_expr default_value])
            None]
    | [%expr open_some [%e? e] ~message:[%e? message]]
    | [%expr open_some ~message:[%e? message] [%e? e]] ->
        [%expr
          Expr.open_variant ~line_no:[%e line_no] "Some" [%e parse_expr e]
            (Some [%e parse_expr message])]
    | [%expr is_variant [%e? c] [%e? e]] -> (
        let line_no = line_no_of_loc loc in
        match c with
        | {pexp_desc = Pexp_constant (Pconst_string (x, _, None)); _} ->
            [%expr
              Expr.is_variant ~line_no:[%e line_no]
                ~name:[%e lift_string ~loc x] [%e parse_expr e]]
        | _ -> err_expected loc "is_variant")
    | [%expr is_none [%e? e]] ->
        let line_no = line_no_of_loc loc in
        [%expr
          Expr.is_variant ~line_no:[%e line_no] ~name:"None" [%e parse_expr e]]
    | [%expr is_some [%e? e]] ->
        let line_no = line_no_of_loc loc in
        [%expr
          Expr.is_variant ~line_no:[%e line_no] ~name:"Some" [%e parse_expr e]]
    | [%expr self_entry_point [%e? ep]] -> (
        let line_no = line_no_of_loc loc in
        match ep with
        | {pexp_desc = Pexp_constant (Pconst_string (x, _, None)); _} ->
            [%expr
              Expr.self_entry_point ~line_no:[%e line_no]
                [%e lift_string ~loc x]]
        | _ -> err_expected loc "self_entry_point")
    | [%expr set_type_expr [%e? e] [%e? t]] ->
        let line_no = line_no_of_loc loc in
        [%expr
          Expr.type_annotation ~line_no:[%e line_no] [%e parse_expr e]
            ~t:[%e parse_type t]]
    | [%expr unpack [%e? e] [%e? t]] ->
        let line_no = line_no_of_loc loc in
        [%expr
          Expr.unpack ~line_no:[%e line_no] [%e parse_expr e] [%e parse_type t]]
    | [%expr contract [%e? t] [%e? e]] ->
        let line_no = line_no_of_loc loc in
        [%expr
          Expr.contract ~line_no:[%e line_no] None [%e parse_type t]
            [%e parse_expr e]]
    | [%expr Set.make [%e? elements]] ->
        let elements = List.map parse_expr (parse_list loc elements) in
        [%expr
          Expr.set ~line_no:[%e line_no] ~entries:[%e lift_list loc elements]]
    | [%expr Map.make [%e? entries]] -> map_make ~env loc line_no false entries
    | [%expr BigMap.make [%e? entries]] ->
        map_make ~env loc line_no true entries
    | [%expr
        create_contract_operation
          [%e? baker]
          [%e? balance]
          [%e? storage]
          [%e? c]] ->
        [%expr
          Expr.create_contract ~line_no:[%e line_no]
            ~baker:[%e parse_expr baker] ~balance:[%e parse_expr balance]
            ~storage:[%e parse_expr storage] [%e c]]
    | [%expr test_account [%e? seed]] -> (
        match seed with
        | {pexp_desc = Pexp_constant (Pconst_string (x, _, None)); _} ->
            [%expr
              Expr.account_of_seed ~line_no:[%e line_no]
                ~seed:[%e lift_string ~loc x]]
        | _ -> err_expected loc "seed")
    | [%expr [%e? e1] [%e? e2]] -> (
        match parse_app ~env loc expr with
        | Some r -> r
        | None ->
            [%expr
              Expr.call_lambda ~line_no:[%e line_no] [%e parse_expr e2]
                [%e parse_expr e1]])
    | {pexp_desc = Pexp_apply ({pexp_desc = Pexp_ident {txt; _}; _}, _); _} -> (
        match parse_app ~env loc expr with
        | Some r -> r
        | None ->
            let ident = label_of_longindent txt in
            err_unknown_identifier loc ident)
    | [%expr data] -> [%expr Expr.storage ~line_no:[%e line_no]]
    | {pexp_desc = Pexp_field (expr, attribute); _} -> (
        let contract_id =
          match expr.pexp_desc with
          | Pexp_ident {txt = Lident s; _} -> contract_id s
          | _ -> None
        in
        match contract_id with
        | None ->
            [%expr
              Expr.attr ~line_no:[%e line_no] [%e parse_expr expr]
                ~name:[%e expr_of_lid ~loc attribute]]
        | Some id -> (
            match attribute.txt with
            | Lident "data" ->
                [%expr
                  Expr.contract_data ~line_no:[%e line_no]
                    (Ids.C_static {static_id = [%e lift_int ~loc id]})]
            | Lident "address" ->
                [%expr
                  Expr.contract_address ~line_no:[%e line_no] None
                    (Ids.C_static {static_id = [%e lift_int ~loc id]})]
            | _ -> assert false))
    | {pexp_desc = Pexp_record (fields, None (*record with*)); _} ->
        let fields =
          List.map
            (fun (name, field) ->
              [%expr [%e expr_of_lid ~loc name], [%e parse_expr field]])
            fields
        in
        let fields = lift_list loc fields in
        [%expr Expr.record ~line_no:[%e line_no] [%e fields]]
    | {pexp_desc = Pexp_ident lid; _} ->
        let expr_or_lid = function
          | Lident "sender" -> [%expr Expr.sender ~line_no:[%e line_no]]
          | Lident "source" -> [%expr Expr.source ~line_no:[%e line_no]]
          | Lident "sp_balance" -> [%expr Expr.balance ~line_no:[%e line_no]]
          | Lident "amount" -> [%expr Expr.amount ~line_no:[%e line_no]]
          | Lident "level" -> [%expr Expr.level ~line_no:[%e line_no]]
          | Lident "now" -> [%expr Expr.now ~line_no:[%e line_no]]
          | Lident "chain_id" -> [%expr Expr.chain_id ~line_no:[%e line_no]]
          | Lident "params" -> [%expr Expr.params ~line_no:[%e line_no]]
          | Lident "operations" -> [%expr Expr.operations ~line_no:[%e line_no]]
          | Lident "total_voting_power" ->
              [%expr Expr.total_voting_power ~line_no:[%e line_no]]
          | Lident "self" -> [%expr Expr.self ~line_no:[%e line_no]]
          | Lident "self_address" ->
              [%expr Expr.self_address ~line_no:[%e line_no]]
          | Lident s -> (
              match contract_id s with
              | None ->
                  [%expr
                    Expr.local ~line_no:[%e line_no] [%e lift_string ~loc s]]
              | Some var ->
                  [%expr
                    Expr.local ~line_no:[%e line_no]
                      [%e lift_string ~loc (s ^ string_of_int var)]])
          | Ldot _ | Lapply _ -> err_expected loc "identifier"
        in
        expr_or_lid lid.txt
    (*  | [%expr data <- [%e? _]] -> failwith "set data"*)
    | {pexp_desc = Pexp_extension ({txt = "e"; _}, PStr [{pstr_desc; _}]); _}
      -> (
        match pstr_desc with
        | Pstr_eval (e, []) -> e
        | _ -> assert false)
    | [%expr []] -> [%expr Expr.list ~line_no:[%e line_no] ~elems:[]]
    | [%expr [%e? x] :: [%e? xs]] as xxs -> (
        match parse_cons_chain xxs with
        | xxs, [%expr []] ->
            let xxs = lift_list loc (List.map parse_expr xxs) in
            [%expr Expr.list ~line_no:[%e line_no] ~elems:[%e xxs]]
        | _ ->
            [%expr
              Expr.cons ~line_no:[%e line_no] [%e parse_expr x]
                [%e parse_expr xs]])
    | {pexp_desc = Pexp_variant (c, Some x); _} ->
        [%expr
          Expr.variant ~line_no:[%e line_no] ~name:[%e lift_string ~loc c]
            [%e parse_expr x]]
    | {pexp_desc = Pexp_tuple xs; _} ->
        let xs = lift_list loc (List.map parse_expr xs) in
        [%expr Expr.tuple ~line_no:[%e line_no] [%e xs]]
    | {pexp_desc; _} ->
        err_expected loc ("expression, got: " ^ show_pexpdesc pexp_desc)
  in
  parse_expr

and parse_app ~env loc =
  let parse_expr = parse_expr ~env in
  function
  | {
      pexp_desc =
        Pexp_apply ({pexp_desc = Pexp_ident {txt; _}; pexp_loc; _}, args)
    ; _
    } -> (
      let ident = label_of_longindent txt in
      let line_no = line_no_of_loc loc in
      match (lookup_prim pexp_loc ident, args) with
      | None, _ -> None
      | Some (1, f), [(Nolabel, e1)] ->
          Some [%expr [%e f] ~line_no:[%e line_no] [%e parse_expr e1]]
      | Some (2, f), [(Nolabel, e1); (Nolabel, e2)] ->
          Some
            [%expr
              [%e f] ~line_no:[%e line_no] [%e parse_expr e1] [%e parse_expr e2]]
      | Some (3, f), [(Nolabel, e1); (Nolabel, e2); (Nolabel, e3)] ->
          Some
            [%expr
              [%e f] ~line_no:[%e line_no] [%e parse_expr e1] [%e parse_expr e2]
                [%e parse_expr e3]]
      | ( Some (4, f)
        , [(Nolabel, e1); (Nolabel, e2); (Nolabel, e3); (Nolabel, e4)] ) ->
          Some
            [%expr
              [%e f] ~line_no:[%e line_no] [%e parse_expr e1] [%e parse_expr e2]
                [%e parse_expr e3] [%e parse_expr e4]]
      | Some (a, _), args -> err_arity pexp_loc ident a (List.length args))
  | _ -> None

and map_make ~env loc line_no big entries =
  let entries =
    List.map
      (function
        | [%expr [%e? k], [%e? v]] ->
            [%expr [%e parse_expr ~env k], [%e parse_expr ~env v]]
        | _ -> err_expected loc "map entry")
      (parse_list loc entries)
  in
  let entries = lift_list loc entries in
  [%expr
    Expr.map ~line_no:[%e line_no] ~big:[%e lift_bool loc big]
      ~entries:[%e entries]]

and parse_type =
  let parse_type (typ : expression) =
    let loc = typ.pexp_loc in
    (*    let _line_no = line_no_of_loc loc in*)
    match typ with
    | [%expr [[%e? x]]] -> parse_type x
    | [%expr unknown [%e? id]] -> (
        match id.pexp_desc with
        | Pexp_constant (Pconst_string (id, _, None)) ->
            [%expr Type.(unknown_raw [%e lift_string ~loc id])]
        | _ -> err_expected loc "unknown id")
    | [%expr unit] -> [%expr Type.unit]
    | [%expr bool] -> [%expr Type.bool]
    | [%expr int] -> [%expr Type.int]
    | [%expr nat] -> [%expr Type.nat]
    | [%expr intOrNat] -> [%expr Type.intOrNat ()]
    | [%expr string] -> [%expr Type.string]
    | [%expr bytes] -> [%expr Type.bytes]
    | [%expr chain_id] -> [%expr Type.chain_id]
    | [%expr mutez] -> [%expr Type.mutez]
    | [%expr timestamp] -> [%expr Type.timestamp]
    | [%expr address] -> [%expr Type.address]
    | [%expr key_hash] -> [%expr Type.key_hash]
    | [%expr key] -> [%expr Type.key]
    | [%expr signature] -> [%expr Type.signature]
    | [%expr operation] -> [%expr Type.operation]
    | [%expr bls12_381_g1] -> [%expr Type.bls12_381_g1]
    | [%expr bls12_381_g2] -> [%expr Type.bls12_381_g2]
    | [%expr bls12_381_fr] -> [%expr Type.bls12_381_fr]
    | [%expr never] -> [%expr Type.never]
    | [%expr option [%e? t]] -> [%expr Type.option [%e parse_type t]]
    | [%expr list [%e? t]] -> [%expr Type.list [%e parse_type t]]
    | [%expr set [%e? t]] -> [%expr Type.set ~telement:[%e parse_type t]]
    | [%expr contract [%e? t]] -> [%expr Type.contract [%e parse_type t]]
    | [%expr ticket [%e? t]] -> [%expr Type.ticket [%e parse_type t]]
    | [%expr lambda [%e? t1] [%e? t2]] ->
        [%expr Type.(lambda no_effects) [%e parse_type t1] [%e parse_type t2]]
    | [%expr pair [%e? t1] [%e? t2]] ->
        [%expr Type.pair [%e parse_type t1] [%e parse_type t2]]
    | [%expr or_ [%e? t1] [%e? t2]] ->
        [%expr Type.tor [%e parse_type t1] [%e parse_type t2]]
    | [%expr map [%e? t1] [%e? t2]] ->
        [%expr
          Type.map ~big:false ~tkey:[%e parse_type t1]
            ~tvalue:[%e parse_type t2]]
    | [%expr big_map [%e? t1] [%e? t2]] ->
        [%expr
          Type.map ~big:true ~tkey:[%e parse_type t1] ~tvalue:[%e parse_type t2]]
    | {pexp_desc = Pexp_record (entries, None); _} ->
        let parse_entry = function
          | {txt = Lident fld; _}, x ->
              [%expr [%e lift_string ~loc fld], [%e parse_type x]]
          | {txt = _; loc}, _ -> err_expected loc "<record label> = <type>"
        in
        let entries = lift_list loc (List.map parse_entry entries) in
        [%expr Type.record (Hole.mk ()) [%e entries]]
    | [%expr [%e? _] + [%e? _]] as cases ->
        let rec uncases = function
          | [%expr [%e? x] + [%e? y]] -> y :: uncases x
          | x -> [x]
        in
        let cases = List.rev (uncases cases) in
        let parse_case = function
          | {pexp_desc = Pexp_variant (cons, Some targ); _} ->
              [%expr [%e lift_string ~loc cons], [%e parse_type targ]]
          | {pexp_desc = Pexp_variant (cons, None); _} ->
              [%expr [%e lift_string ~loc cons], Type.unit ()]
          | {pexp_loc = loc; _} -> err_expected loc "<variant case> [<type>]"
        in
        let cases = lift_list loc (List.map parse_case cases) in
        [%expr Type.variant (Hole.mk ()) [%e cases]]
    | [%expr [%e? _] :: [%e? _]] as ts ->
        let ts = parse_list loc ts in
        let ts = lift_list loc (List.map parse_type ts) in
        [%expr Type.tuple [%e ts]]
    | {pexp_desc = Pexp_extension ({txt = "e"; loc = _}, x); _} -> (
        match x with
        | PStr [{pstr_desc = Pstr_eval (e, []); _}] -> e
        | _ -> assert false)
    | _ -> err_expected loc "type"
  in
  parse_type

and parse_command ~env =
  let parse_expr = parse_expr ~env in
  let rec parse_command command =
    let loc = command.pexp_loc in
    let line_no = line_no_of_loc loc in
    match command with
    | {pexp_desc = Pexp_extension ({txt = "e"; loc = _}, x); _} -> (
        match x with
        | PStr [{pstr_desc = Pstr_eval (e, []); _}] -> e
        | _ -> assert false)
    | [%expr verify [%e? e]] ->
        [%expr Command.verify ~line_no:[%e line_no] [%e parse_expr e] None]
    | [%expr verify [%e? e] ~msg:[%e? msg]]
    | [%expr verify ~msg:[%e? msg] [%e? e]] ->
        [%expr
          Command.verify ~line_no:[%e line_no] [%e parse_expr e]
            (Some [%e parse_expr msg])]
    | [%expr ()] -> [%expr Command.seq ~line_no:[%e line_no] []]
    | [%expr
        [%e? e1];
        [%e? e2]] ->
        let e1 = parse_command e1 in
        let e2 = parse_command e2 in
        [%expr Command.seq ~line_no:[%e line_no] [[%e e1]; [%e e2]]]
    | [%expr result [%e? e]] ->
        [%expr Command.result ~line_no:[%e line_no] [%e parse_expr e]]
    | [%expr data <- [%e? e]] ->
        [%expr
          Command.set ~line_no:[%e line_no]
            (Expr.storage ~line_no:[%e line_no])
            [%e parse_expr e]]
    | [%expr Map.delete [%e? e1] [%e? e2]] ->
        [%expr
          Command.del_item ~line_no:[%e line_no] [%e parse_expr e1]
            [%e parse_expr e2]]
    | [%expr Set.add [%e? e1] [%e? e2]] ->
        [%expr
          Command.update_set ~line_no:[%e line_no] [%e parse_expr e1]
            [%e parse_expr e2] true]
    | [%expr Set.remove [%e? e1] [%e? e2]] ->
        [%expr
          Command.update_set ~line_no:[%e line_no] [%e parse_expr e1]
            [%e parse_expr e2] false]
    | [%expr
        let* [%p? x] = [%e? c1] in
        [%e? c2]] -> (
        match x.ppat_desc with
        | Ppat_var {txt; loc} ->
            [%expr
              Command.bind ~line_no:[%e line_no]
                (Some [%e lift_string ~loc txt])
                [%e parse_command c1] [%e parse_command c2]]
        | _ -> err_expected x.ppat_loc "variable")
    | [%expr
        let [%p? x] = [%e? e] in
        [%e? c]] -> (
        match x.ppat_desc with
        | Ppat_var {txt; loc} ->
            [%expr
              Command.seq ~line_no:[%e line_no]
                [
                  Command.define_local ~line_no:[%e line_no]
                    [%e lift_string ~loc txt] [%e parse_expr e] false
                ; [%e parse_command c]
                ]]
        | _ -> err_expected x.ppat_loc "variable")
    | [%expr
        let%mutable [%p? x] = [%e? e] in
        [%e? c]] -> (
        match x.ppat_desc with
        | Ppat_var {txt; loc} ->
            [%expr
              Command.seq ~line_no:[%e line_no]
                [
                  Command.define_local ~line_no:[%e line_no]
                    [%e lift_string ~loc txt] [%e parse_expr e] true
                ; [%e parse_command c]
                ]]
        | _ -> err_expected x.ppat_loc "variable")
    | [%expr Map.set [%e? m] [%e? k] [%e? v]] ->
        [%expr
          Command.set ~line_no:[%e line_no]
            (Expr.item ~line_no:[%e line_no] [%e parse_expr m] [%e parse_expr k]
               None None)
            [%e parse_expr v]]
    | {pexp_desc = Pexp_setinstvar ({txt; loc = _}, e); _} ->
        [%expr
          Command.set ~line_no:[%e line_no]
            (Expr.local ~line_no:[%e line_no] [%e lift_string ~loc txt])
            [%e parse_expr e]]
    | {pexp_desc = Pexp_setfield (e1, attribute, e2); _} ->
        [%expr
          Command.set ~line_no:[%e line_no]
            (Expr.attr ~line_no:[%e line_no] [%e parse_expr e1]
               ~name:[%e expr_of_lid ~loc attribute])
            [%e parse_expr e2]]
    | [%expr
        while [%e? cond] do
          [%e? command]
        done] ->
        [%expr
          Command.while_loop ~line_no:[%e line_no] [%e parse_expr cond]
            [%e parse_command command]]
    | [%expr List.iter (fun [%p? p] -> [%e? command]) [%e? e]] ->
        let p =
          match p.ppat_desc with
          | Ppat_var {txt = x; _} -> x
          | Ppat_any -> "_"
          | _ -> assert false
        in
        [%expr
          Command.for_loop ~line_no:[%e line_no] [%e lift_string ~loc p]
            [%e parse_expr e] [%e parse_command command]]
    | [%expr List.iter [%e? command] [%e? e]] ->
        [%expr
          Command.for_loop ~line_no:[%e line_no] "a" [%e parse_expr e]
            [%e parse_command command]]
    | [%expr failwith [%e? e]] ->
        [%expr Command.sp_failwith ~line_no:[%e line_no] [%e parse_expr e]]
    | [%expr if [%e? cond] then [%e? then_]] ->
        [%expr
          Command.ifte ~line_no:[%e line_no] [%e parse_expr cond]
            [%e parse_command then_]
            (Command.seq ~line_no:[%e line_no] [])]
    | [%expr if [%e? cond] then [%e? then_] else [%e? else_]] ->
        [%expr
          Command.ifte ~line_no:[%e line_no] [%e parse_expr cond]
            [%e parse_command then_] [%e parse_command else_]]
    | [%expr set_type [%e? e] [%e? t]] ->
        [%expr
          Command.set_type ~line_no:[%e line_no] [%e parse_expr e]
            [%e parse_type t]]
    | [%expr create_contract [%e? baker] [%e? balance] [%e? storage] [%e? c]] ->
        [%expr
          Library.create_contract ~line_no:[%e line_no] [%e parse_expr baker]
            [%e parse_expr balance] [%e parse_expr storage] [%e c]]
    | {pexp_desc = Pexp_ident {txt = Lident ident; _}; _} -> apply ~loc ident []
    | {
      pexp_desc =
        Pexp_apply ({pexp_desc = Pexp_ident {txt = Lident ident; _}; _}, args)
    ; _
    } -> apply ~loc ident args
    | {pexp_desc = Pexp_match (e, cases); _} ->
        let parse_case = function
          | {
              pc_lhs = {ppat_desc = Ppat_variant (v, arg); _}
            ; pc_guard = None
            ; pc_rhs
            } ->
              let command = parse_command pc_rhs in
              let arg =
                match arg with
                | Some {ppat_desc = Ppat_any; _} | None -> "_no_arg"
                | Some {ppat_desc = Ppat_var {txt = arg; _}; _} -> arg
                | _ -> err_expected loc "match clause argument"
              in
              [%expr
                [%e lift_string ~loc v], [%e lift_string ~loc arg], [%e command]]
          | _ -> err_expected loc "match clause"
        in
        let cases = List.map parse_case cases in
        [%expr
          Command.mk_match ~line_no:[%e line_no] [%e parse_expr e]
            [%e lift_list loc cases]]
    | _ -> err_expected loc "command"
  and apply ~loc ident (args : _ list) =
    let single_commands =
      let mono (f : _ -> _) ident args =
        match args with
        | [(Nolabel, e)] -> fun loc -> f loc e
        | _ -> fun loc -> err loc "%S expects one argument" ident
      in
      let bin (f : _ -> _) ident args =
        match args with
        | [(Nolabel, e1); (Nolabel, e2)] -> fun loc -> f loc e1 e2
        | _ -> fun loc -> err loc "%S expects two arguments" ident
      in
      let ternary (f : _ -> _) ident args =
        match args with
        | [(Nolabel, e1); (Nolabel, e2); (Nolabel, e3)] ->
            fun loc -> f loc e1 e2 e3
        | _ -> fun loc -> err loc "%S expects three arguments" ident
      in
      [
        ( "never"
        , mono (fun loc e ->
              let line_no = line_no_of_loc loc in
              [%expr Command.never ~line_no:[%e line_no] [%e parse_expr e]]) )
      ; ( "trace"
        , mono (fun loc e ->
              let line_no = line_no_of_loc loc in
              [%expr Command.trace ~line_no:[%e line_no] [%e parse_expr e]]) )
      ; ( "add_operations"
        , mono (fun loc e ->
              let line_no = line_no_of_loc loc in
              [%expr
                Library.add_operations ~line_no:[%e line_no] [%e parse_expr e]])
        )
      ; ( "send"
        , bin (fun loc e1 e2 ->
              let line_no = line_no_of_loc loc in
              [%expr
                Library.send ~line_no:[%e line_no] [%e parse_expr e1]
                  [%e parse_expr e2]]) )
      ; ( "transfer"
        , ternary (fun loc e1 e2 e3 ->
              let line_no = line_no_of_loc loc in
              [%expr
                Library.transfer ~line_no:[%e line_no] [%e parse_expr e1]
                  [%e parse_expr e2] [%e parse_expr e3]]) )
      ; ( "set_delegate"
        , mono (fun loc e ->
              let line_no = line_no_of_loc loc in
              [%expr
                Library.set_delegate ~line_no:[%e line_no] [%e parse_expr e]])
        )
      ]
    in
    match List.assoc_opt ident single_commands with
    | Some command -> command ident args loc
    | _ ->
        err loc "Unknown command %s\nSupported commands are: %s." ident
          (String.concat ", " (List.map fst single_commands))
  in
  parse_command

let expand_entry_point recflag value_bindings =
  let on_value_binding binding =
    let loc = binding.pvb_loc in
    match binding.pvb_pat.ppat_desc with
    | Ppat_var name ->
        let code =
          let t, e =
            match binding.pvb_expr with
            | [%expr fun params -> [%e? e]] -> (`Present, e)
            | [%expr fun () -> [%e? e]] -> (`Absent, e)
            | [%expr fun (params : [%t? t]) -> [%e? e]] -> (`Annotated t, e)
            | _ -> err binding.pvb_loc "Unexpected parameters to entry point"
          in
          let tparameter_ep_explicit =
            match t with
            | `Present -> [%expr `Present]
            | `Absent -> [%expr `Absent]
            | `Annotated {ptyp_desc = Ptyp_constr ({txt = Lident t; _}, _); _}
              ->
                [%expr
                  `Annotated
                    [%e Ast_helper.Exp.ident {txt = Lident ("gen_t_" ^ t); loc}]]
            | `Annotated {ptyp_desc = t; _} -> failtdesc t
          in
          let line_no = line_no_of_loc loc in
          [%expr
            Basics.
              {
                channel = [%e lift_string ~loc name.txt]
              ; tparameter_ep = [%e tparameter_ep_explicit]
              ; originate = true
              ; lazify = Some false
              ; lazy_no_code = Some false
              ; check_no_incoming_transfer = None
              ; line_no = [%e line_no]
              ; body = [%e parse_command ~env:None e]
              ; derived = U
              }]
        in
        let code =
          {
            binding with
            (* pvb_pat =
             *   { binding.pvb_pat with
             *     ppat_desc = Ppat_var {name with txt = "gen_" ^ name.txt} } *)
            pvb_expr = code
          }
        in
        [code]
    | _ -> assert false
  in
  let value_bindings =
    List.map on_value_binding value_bindings |> List.concat
  in
  Ast_helper.Str.value recflag value_bindings

module Scenario = struct
  let new_contract ?name ~loc ~env e =
    let id = env.next_id () in
    Option.iter (fun name -> Hashtbl.replace contract_static_ids name id) name;
    let line_no = line_no_of_loc loc in
    [%expr
      let id = Ids.C_static {static_id = [%e lift_int ~loc id]} in
      Basics.New_contract
        {
          id
        ; contract = [%e e].contract
        ; line_no = [%e line_no]
        ; accept_unknown_types = false
        ; show = true
        ; address = Basics.address_of_contract_id id
        }]

  let add_html ~loc ~env:_ tag e =
    let line_no = line_no_of_loc loc in
    [%expr
      Basics.Html
        {
          tag = [%e lift_string ~loc tag]
        ; inner = [%e e]
        ; line_no = [%e line_no]
        }]

  let verify ~loc ~env condition =
    let line_no = line_no_of_loc loc in
    [%expr
      Basics.Verify
        {
          condition = [%e parse_expr ~env:(Some env) condition]
        ; line_no = [%e line_no]
        }]

  let message ~loc ~env ~options e1 params =
    let supported_options = ["valid"] in
    let check_supported loc k =
      if not (List.mem k supported_options)
      then
        err loc
          "Unsupported option ~%s in contract all.\nSupported options: %s." k
          (String.concat ", " supported_options)
    in
    let options =
      List.map
        (function
          | (Labelled k : arg_label), v ->
              check_supported loc k;
              (k, v)
          | (Optional k : arg_label), _v ->
              check_supported loc k;
              err loc
                "Unsupported option usage ?%s in contract call; please use ~%s."
                k k
          | _ -> assert false)
        options
    in
    let line_no = line_no_of_loc loc in
    let id, message =
      match e1 with
      | {
        pexp_desc =
          Pexp_field
            ( {pexp_desc = Pexp_ident {txt = Lident id; _}; _}
            , {txt = Lident message; _} )
      ; _
      } -> (id, message)
      | _ -> assert false
    in
    let id =
      match Hashtbl.find_opt contract_static_ids id with
      | None -> err loc "Missing contract id %S" id
      | Some s -> s
    in
    let valid =
      match List.assoc_opt "valid" options with
      | None -> [%expr Expr.literal ~line_no:[%e line_no] (Literal.bool true)]
      | Some valid -> parse_expr ~env:(Some env) valid
    in
    [%expr
      Basics.Message
        {
          id = Ids.C_static {static_id = [%e lift_int ~loc id]}
        ; valid = [%e valid]
        ; exception_ = None
        ; params = [%e parse_expr ~env:(Some env) params]
        ; line_no = [%e line_no]
        ; title = ""
        ; messageClass = ""
        ; context =
            {
              sender = None
            ; source = None
            ; chain_id = None
            ; time = None
            ; level = None
            ; voting_powers = None
            }
        ; amount =
            Expr.literal ~line_no:[%e line_no]
              (Literal.mutez Big_int.zero_big_int)
        ; message = [%e lift_string ~loc message]
        ; show = true
        ; export = true
        }]

  let show ~loc ~env expr =
    let line_no = line_no_of_loc loc in
    [%expr
      Basics.Show
        {
          expression = [%e parse_expr ~env:(Some env) expr]
        ; html = true
        ; stripStrings = false
        ; compile = true
        ; line_no = [%e line_no]
        }]

  let single_actions =
    let add_action action ~env = env.actions := action :: !(env.actions) in
    let mono (f : loc:_ -> env:_ -> _ -> _) ident args =
      match args with
      | [(Nolabel, e)] -> fun loc env : unit -> add_action ~env (f e ~loc ~env)
      | _ ->
          fun loc ->
            err loc "Scenario syntax error %s ...\nUsage: %s <expression>" ident
              ident
    in
    let call _ args =
      let args, options =
        List.partition
          (function
            | Nolabel, _ -> true
            | _ -> false)
          args
      in
      let args = List.map snd args in
      match args with
      | [e1; params] ->
          fun loc env -> add_action ~env (message ~loc ~env ~options e1 params)
      | _ -> assert false
    in
    [
      ("h1", mono (add_html "h1"))
    ; ("h2", mono (add_html "h2"))
    ; ("h3", mono (add_html "h3"))
    ; ("h4", mono (add_html "h4"))
    ; ("p", mono (add_html "p"))
    ; ("show", mono show)
    ; ("verify", mono verify)
    ; ("register_contract", mono (new_contract ?name:None))
    ; ("call", call)
    ]

  let parse_actions env expr =
    let rec parse_actions expr =
      let loc = expr.pexp_loc in
      let add_action action =
        env.actions := action ~loc ~env :: !(env.actions)
      in
      match expr with
      | {pexp_desc = Pexp_ident {txt = Lident ident; _}; _} ->
          apply ~loc ident []
      | {
        pexp_desc =
          Pexp_apply ({pexp_desc = Pexp_ident {txt = Lident ident; _}; _}, args)
      ; _
      } -> apply ~loc ident args
      | [%expr
          let [%p? p] = register_contract [%e? e1] in
          [%e? e]] ->
          let name =
            match p.ppat_desc with
            | Ppat_var {txt = v; _} -> v
            | _ -> assert false
          in
          add_action (new_contract ~name e1);
          parse_actions e
      | [%expr ()] -> ()
      | [%expr
          [%e? e1];
          [%e? e2]] ->
          parse_actions e1;
          parse_actions e2
      | _ -> err_expected loc "action"
    and apply ~loc ident (args : _ list) =
      match List.assoc_opt ident single_actions with
      | Some action -> action ident args loc env
      | _ ->
          err loc "Unknown action %s\nSupported actions are: %s." ident
            (String.concat ", " (List.map fst single_actions))
    in
    parse_actions expr

  let parse_actions expression =
    let ids = ref (-1) in
    let next_id () =
      incr ids;
      !ids
    in
    let env = {actions = ref []; next_id} in
    parse_actions env expression;
    let loc = expression.pexp_loc in
    let actions = lift_list loc (List.rev !(env.actions)) in
    [%expr lazy [%e actions]]

  let expand_scenario ~scenario_kind recflag value_bindings =
    let on_value_binding binding =
      let loc = binding.pvb_loc in
      match binding.pvb_pat.ppat_desc with
      | Ppat_var name ->
          let code =
            let actions = parse_actions binding.pvb_expr in
            [%expr
              Basics.
                {
                  shortname = [%e lift_string ~loc name.txt]
                ; longname = [%e lift_string ~loc name.txt]
                ; actions = [%e actions]
                ; flags = []
                ; kind = {kind = [%e lift_string ~loc scenario_kind]}
                }]
          in
          let code = {binding with pvb_expr = code} in
          [code]
      | _ -> assert false
    in
    let value_bindings =
      List.map on_value_binding value_bindings |> List.concat
    in
    Ast_helper.Str.value recflag value_bindings
end

let parse_expr = parse_expr ~env:None

let parse_command = parse_command ~env:None
