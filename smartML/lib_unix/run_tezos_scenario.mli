type connection =
  | Node of {
        address : string option
      ; port : int
      ; tls : bool
    }
  | Mockup

module Make (Primitives : SmartML.Primitives.Primitives) : sig
  val run :
       config:SmartML.Config.t
    -> funder:string
    -> confirmation:[ `Auto | `Bake | `Wait of int ]
    -> connection:connection
    -> root_dir:string
    -> scenarios:(string * Tools.Scenario.loaded_scenario) list
    -> unit
end
