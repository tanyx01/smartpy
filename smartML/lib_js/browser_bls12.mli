(* Copyright 2019-2022 Smart Chain Arena LLC. *)

(* G1 Point *)

val addG1 : string -> string -> string
  [@@js.global "smartpyContext.Bls12.addG1"]

val negateG1 : string -> string [@@js.global "smartpyContext.Bls12.negateG1"]

val multiplyG1ByFr : string -> string -> string
  [@@js.global "smartpyContext.Bls12.multiplyG1ByFr"]

(* G2 Point *)

val addG2 : string -> string -> string
  [@@js.global "smartpyContext.Bls12.addG2"]

val negateG2 : string -> string [@@js.global "smartpyContext.Bls12.negateG2"]

val multiplyG2ByFr : string -> string -> string
  [@@js.global "smartpyContext.Bls12.multiplyG2ByFr"]

(* Fr *)

val addFr : string -> string -> string
  [@@js.global "smartpyContext.Bls12.addFr"]

val negateFr : string -> string [@@js.global "smartpyContext.Bls12.negateFr"]

val multiplyFrByFr : string -> string -> string
  [@@js.global "smartpyContext.Bls12.multiplyFrByFr"]

val multiplyFrByInt : string -> string -> string
  [@@js.global "smartpyContext.Bls12.multiplyFrByInt"]

val convertFrToInt : string -> string
  [@@js.global "smartpyContext.Bls12.convertFrToInt"]

(* Pairing Check *)

val pairingCheck : (string * string) list -> bool
  [@@js.global "smartpyContext.Bls12.pairingCheck"]
