(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Browser_sodium
open Utils

let dbgf fmt =
  Format.kasprintf
    (fun s ->
      Js_of_ocaml.Firebug.console##log (Js_of_ocaml.Js.string s) |> ignore)
    fmt

module Timelock = struct
  open Browser_timelock

  type opening_result =
    | Correct of string
    | Bogus_cipher
    | Bogus_opening

  let open_chest chest chest_key time =
    try
      let result = open_chest chest chest_key time in
      let result = Yojson.Safe.from_string result in
      let kind = Yojson.Safe.Util.member "kind" result in
      match kind with
      | `String "Correct" -> (
          let bytes = Yojson.Safe.Util.member "bytes" result in
          match bytes with
          | `String bytes -> Correct bytes
          | _ -> failwith "Expected bytes after successfully opening a chest.")
      | `String "Bogus_cipher" -> Bogus_cipher
      | `String "Bogus_opening" -> Bogus_opening
      | _ -> failwith "Got an unexpected kind when opening the timelock chest."
    with _ -> failwith "Something went wrong when opening the chest."
end

module Bls12 = struct
  open Browser_bls12

  (* G1 Point *)
  let negateG1 b = negateG1 b

  let addG1 b1 b2 = addG1 b1 b2

  let multiplyG1ByFr b1 b2 = multiplyG1ByFr b1 b2

  (* G2 Point *)
  let negateG2 b = negateG2 b

  let addG2 b1 b2 = addG2 b1 b2

  let multiplyG2ByFr b1 b2 = multiplyG2ByFr b1 b2

  (* Fr *)
  let negateFr b = negateFr b

  let addFr b1 b2 = addFr b1 b2

  let multiplyFrByFr b1 b2 = multiplyFrByFr b1 b2

  let multiplyFrByInt b i = multiplyFrByInt b i

  let convertFrToInt b = convertFrToInt b

  (* Pairing Check *)
  let pairingCheck l = pairingCheck l
end

let to_b58 prefix x =
  let x = String.implode x in
  let (`Base58 r) = Base58.of_bytes (prefix ^ x) in
  r

let of_b58 prefix x =
  let x = Base58.to_bytes_exn (`Base58 x) in
  let l = String.length prefix in
  if String.take l x <> prefix then failwith "invalid base58 prefix";
  let x = String.drop l x in
  String.explode x

module Crypto = struct
  let ojs_error_protect msg f =
    let m = Printf.sprintf "Crypto-primitives.%s" msg in
    try f () with
    | Ojs_exn.Error e ->
        Format.kasprintf failwith "%s: %s" m (Ojs_exn.to_string e)
    | other -> Printf.ksprintf failwith "%s: %s" m (Base.Exn.to_string other)

  let check_signature ~public_key ~signature msg =
    ojs_error_protect "check_signature" (fun () ->
        let hex_msg = Hex.(show (of_string msg)) in
        if false
        then
          dbgf
            "check_signature.js:\n\
            \   public_key: %S,\n\
            \   signature: %S,\n\
            \   msg: %S\n\
            \   hex-msg: %s\n"
            public_key signature msg hex_msg;
        Ed25519.verify_signature
          ~message:(Crypto_bytes.of_hex hex_msg |> crypto_hash_blake_2b ~size:32)
          ~public_key:(public_key |> of_b58 Bs58.Prefix.ed25519_public_key)
          (signature |> of_b58 Bs58.Prefix.ed25519_signature))

  let sign ~secret_key message =
    ojs_error_protect "sign" (fun () ->
        let message =
          Crypto_bytes.of_hex Hex.(show (of_string message))
          |> crypto_hash_blake_2b ~size:32
        in
        let secret_key = secret_key |> of_b58 Bs58.Prefix.ed25519_secret_key in
        Ed25519.sign ~message ~secret_key
        |> to_b58 Bs58.Prefix.ed25519_signature)

  let account_of_seed seed =
    let real_seed =
      (* The seed is expected to be a 32-byte “C-string” (no '\x00'
         characters alloweed). *)
      let hashed =
        Hex.(show (of_string (Utils.Crypto.sha256 seed))) ^ String.make 32 'B'
      in
      String.sub hashed 0 32
    in
    let kp =
      ojs_error_protect "keypair_of_seed" (fun () ->
          Ed25519.keypair_of_seed real_seed)
    in
    let public_key = to_b58 Bs58.Prefix.ed25519_public_key kp.publicKey in
    let public_key_hash =
      to_b58 Bs58.Prefix.ed25519_public_key_hash
        (crypto_hash_blake_2b ~size:20 kp.publicKey)
    in
    let full_secret_key = to_b58 Bs58.Prefix.ed25519_secret_key kp.privateKey in
    if false
    then
      dbgf "real_seed: %S\npkh: %S\npk: %S\nsk: %s" real_seed public_key_hash
        public_key full_secret_key;
    SmartML.Primitives.
      {pkh = public_key_hash; pk = public_key; sk = full_secret_key}

  let hash_key s =
    let pkh =
      s
      |> of_b58 Bs58.Prefix.ed25519_public_key
      |> crypto_hash_blake_2b ~size:20
      |> to_b58 Bs58.Prefix.ed25519_public_key_hash
    in
    if false then dbgf "pk: %s → hashed: %s" s pkh;
    pkh
end

let test primitives : unit =
  let public_key = "edpku5ZuqYibUQrAhove2B1bZDYCQyDRTGGa1ZwtZYiJ6nvJh4GXcE" in
  let secret_key = "edsk3ZAU1vr8z3Laj6kGqjsgL9a1kQA9Zs7QeEk2vBsVR5RGbb6UQX" in
  let message_hex = "050a00000003424146" in
  let signature =
    "edsigtbqJKYNhTVAYaY1sjFKTxS7awXwFrXv2fPBV1BzcYv4PvKzzjBYZNJmZeBpd1ec7J2VVAdqPCnMXoS7qu3KrdZZC5faCUZ"
  in
  let wrong_signature =
    "edsigtjoEiJAEdbeWqmhwc1RJE1AgdnBStaECLykBDvzWQ6bbVsMMEdz21iJqBrgXwYmmWKLCFp7tMuY7sfAubY75ThLxNwSVuG"
  in
  let open Browser_sodium in
  dbgf "-------------------- SELF TEST ------------------";
  let should_true b = if b then "Ok" else "#### ERROR ####" in
  let should_false b = should_true (not b) in
  let b58_inverse prefix v =
    let i = v |> of_b58 prefix |> to_b58 prefix in
    dbgf "B58-inverse: %S -> %S -> %S -> %s" v
      (v |> of_b58 prefix |> Crypto_bytes.to_hex)
      i
      (String.equal v i |> should_true)
  in
  b58_inverse Bs58.Prefix.ed25519_public_key public_key;
  b58_inverse Bs58.Prefix.ed25519_secret_key secret_key;
  b58_inverse Bs58.Prefix.ed25519_signature wrong_signature;
  let message =
    Crypto_bytes.of_hex message_hex |> crypto_hash_blake_2b ~size:32
  in
  let public_key = public_key |> of_b58 Bs58.Prefix.ed25519_public_key in

  dbgf "Ed25519.verify GOOD signature: %s"
    (Ed25519.verify_signature ~public_key ~message
       (signature |> of_b58 Bs58.Prefix.ed25519_signature)
    |> should_true);
  dbgf "Ed25519.verify WRONG signature: %s"
    (Ed25519.verify_signature ~public_key ~message
       (wrong_signature |> of_b58 Bs58.Prefix.ed25519_signature)
    |> should_false);
  let full_secret_key =
    Crypto_bytes.append
      (secret_key |> of_b58 Bs58.Prefix.ed25519_seed)
      public_key
  in
  let new_signature =
    Ed25519.sign ~message ~secret_key:full_secret_key
    |> to_b58 Bs58.Prefix.ed25519_signature
  in
  dbgf "Re-sign with Ed25519 (full sk: %s): new-sig: %s -> %s"
    (to_b58 Bs58.Prefix.ed25519_secret_key full_secret_key)
    new_signature
    (String.equal new_signature signature |> should_true);
  dbgf "account_of_seed";
  let kp = Ed25519.keypair_of_seed "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" in
  dbgf "pk: %s ; sk: %s ; keyType: %s"
    (to_b58 Bs58.Prefix.ed25519_public_key kp.publicKey)
    (to_b58 Bs58.Prefix.ed25519_secret_key kp.privateKey)
    kp.keyType;
  dbgf "Contract.Primitive_implementations.self_test";
  (try
     List.iteri
       (fun i -> function
         | Ok s -> dbgf "self-test-%d-OK: %s" i s
         | Error s -> dbgf "self-test-%d-ERROR: %s" i s)
       (SmartML.Primitives.test_primitives primitives)
   with e ->
     dbgf "Contract.Primitive_implementations.self_test: error: %s"
       (Printexc.to_string e));
  dbgf "Contract.Primitive_implementations.self_test done";
  ()
