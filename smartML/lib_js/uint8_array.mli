type t = char list

val t_to_js : t -> Ojs.t

val t_of_js : Ojs.t -> t
