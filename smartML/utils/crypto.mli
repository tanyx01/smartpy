val sha512 : string -> string

val sha256 : string -> string

val blake2b : string -> string

val sha3 : string -> string

val keccak : string -> string
