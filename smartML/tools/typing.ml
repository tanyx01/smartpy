(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Utils
open Michelson

let intType isNat =
  match Hole.get isNat with
  | None -> `Unknown
  | Some true -> `Nat
  | Some false -> `Int

let tree_of_layout row =
  Binary_tree.map (fun Layout.{source; target} ->
      match List.assoc_opt source row with
      | Some t -> (source, target, t)
      | None ->
          Printf.ksprintf failwith "Missing layout field %S in [%s]" source
            (String.concat "; " (List.map fst row)))

let get_layout row layout =
  match Hole.get layout with
  | None ->
      Printf.ksprintf failwith "Missing layout for record %s"
        (String.concat " " (List.map fst row))
  | Some layout -> layout

let rec compile_tuple f = function
  | [] | [_] -> failwith "compile_tuple"
  | [x; y] -> f x y
  | x :: xs -> f x (compile_tuple f xs)

let rec mtype_of_type ~with_annots t =
  snd (mtype_of_type_with_single ~with_annots t)

and mtype_of_type_with_single ~with_annots t =
  let mtype_of_type = mtype_of_type ~with_annots in
  let row_of_tree build tr =
    let l (_source, target, t) = (Some target, mtype_of_type t) in
    let n (annot1, t1) (annot2, t2) =
      if with_annots
      then (None, build annot1 annot2 t1 t2)
      else (None, build None None t1 t2)
    in
    Binary_tree.cata l n tr
  in
  let mtype_of_layout build row layout =
    match row with
    | [] -> (None, mt_unit)
    | row ->
        let layout = get_layout row layout in
        row_of_tree build (tree_of_layout row layout)
  in
  match Type.unF t with
  | T0 t -> (None, mt0 t)
  | T1 (t, t1) -> (None, mt1 t (mtype_of_type t1))
  | T2 ((T_pair _ | T_or _), _, _) -> assert false
  | T2 (t, t1, t2) -> (None, mt2 t (mtype_of_type t1) (mtype_of_type t2))
  | TLambda
      ( {
          with_storage = Value with_storage
        ; with_operations = Value with_operations
        }
      , t1
      , t2 ) ->
      let with_storage = Option.map snd with_storage in
      let t1, t2 = Type.rawify_lambda ~with_storage ~with_operations t1 t2 in
      (None, mt_lambda (mtype_of_type t1) (mtype_of_type t2))
  | TLambda _ -> assert false
  | TBounded {base} -> (None, mtype_of_type base)
  | TInt {isNat} -> (
      ( None
      , match intType isNat with
        | `Unknown -> mt_int
        | `Nat -> mt_nat
        | `Int -> mt_int ))
  | TRecord {row; layout; var = None} ->
      let mt_pair annot_fst annot_snd = mt_pair ?annot_fst ?annot_snd in
      mtype_of_layout mt_pair row layout
  | TVariant {row; layout} ->
      let mt_or annot_left annot_right = mt_or ?annot_left ?annot_right in
      mtype_of_layout mt_or row layout
  | TSecretKey -> (None, mt_var "Secret keys are forbidden in contracts")
  | TUnknown (_, x) -> (None, mt_var x)
  | TRecord {var = Some _} | TTuple (_, Some _) -> (None, mt_var "Unknown Type")
  | TTuple (ts, None) ->
      (None, compile_tuple mt_pair (List.map (fun (_, t) -> mtype_of_type t) ts))
  | TSaplingState {memo} ->
      ( None
      , Option.fold
          ~none:(mt_var "sapling state with no explicit memo")
          ~some:(fun memo -> mt_sapling_state memo)
          (Hole.get memo) )
  | TSaplingTransaction {memo} ->
      ( None
      , Option.fold
          ~none:(mt_var "sapling transaction with no explicit memo")
          ~some:(fun memo -> mt_sapling_transaction memo)
          (Hole.get memo) )

(* TODO go through Michel types *)
let type_of_mtype ?wrap =
  let open Type in
  let f ?annot_type:_ ?annot_variable:_ mt ~wrap =
    let t =
      match (mt : _ Michelson.mtype_f) with
      | MT0 T_unit -> unit
      | MT0 T_bool -> bool
      | MT0 T_nat -> nat
      | MT0 T_int -> int
      | MT0 T_mutez -> mutez
      | MT0 T_string -> string
      | MT0 T_bytes -> bytes
      | MT0 T_timestamp -> timestamp
      | MT0 T_address -> address
      | MT0 T_key -> key
      | MT0 T_key_hash -> key_hash
      | MT0 T_signature -> signature
      | MT0 T_operation -> operation
      | MT0 (T_sapling_state {memo}) -> sapling_state (Some memo)
      | MT0 (T_sapling_transaction {memo}) -> sapling_transaction (Some memo)
      | MT0 T_never -> never
      | MT0 T_bls12_381_g1 -> bls12_381_g1
      | MT0 T_bls12_381_g2 -> bls12_381_g2
      | MT0 T_bls12_381_fr -> bls12_381_fr
      | MT0 T_chest_key -> chest_key
      | MT0 T_chest -> chest
      | MT1 (T_option, t) -> option (t ~wrap:None)
      | MT1 (T_list, t) -> list (t ~wrap:None)
      | MT1 (T_ticket, t) -> ticket (t ~wrap:None)
      | MT1 (T_set, telement) -> set ~telement:(telement ~wrap:None)
      | MT1 (T_contract, t) -> contract (t ~wrap:None)
      | MT2 (T_pair {annot_fst; annot_snd}, fst, snd) -> (
          let fst = fst ~wrap:(Option.map (Control.pair `Record) annot_fst) in
          let snd = snd ~wrap:(Option.map (Control.pair `Record) annot_snd) in
          let mk xl yl xs ys =
            record (Hole.value (Binary_tree.node xl yl)) (xs @ ys)
          in
          match (Type.unF fst, Type.unF snd) with
          | TRecord {layout = xl; row = xs}, TRecord {layout = yl; row = ys}
            -> (
              (* TODO If names clash, stick to a pair/or. *)
              match (Hole.get xl, Hole.get yl) with
              | Some xl, Some yl -> mk xl yl xs ys
              | _ -> pair fst snd)
          | _ -> pair fst snd)
      | MT2 (T_or {annot_left; annot_right}, left, right) -> (
          let left =
            left ~wrap:(Option.map (Control.pair `Variant) annot_left)
          in
          let right =
            right ~wrap:(Option.map (Control.pair `Variant) annot_right)
          in
          match (Type.unF left, Type.unF right) with
          | TVariant {layout = xl; row = xs}, TVariant {layout = yl; row = ys}
            -> (
              match (Hole.get xl, Hole.get yl) with
              | Some xl, Some yl ->
                  variant (Hole.value (Binary_tree.node xl yl)) (xs @ ys)
              | _ -> tor left right)
          | _ -> tor left right)
      | MT2 (T_lambda, t, u) -> lambda no_effects (t ~wrap:None) (u ~wrap:None)
      | MT2 (T_map, tkey, tvalue) ->
          let tkey = tkey ~wrap:None in
          let tvalue = tvalue ~wrap:None in
          map ~big:false ~tkey ~tvalue
      | MT2 (T_big_map, tkey, tvalue) ->
          let tkey = tkey ~wrap:None in
          let tvalue = tvalue ~wrap:None in
          map ~big:true ~tkey ~tvalue
      | MT0 T_chain_id -> chain_id
      | MT_var s -> failwith ("type_of_mtype: MT_var " ^ s)
    in
    match wrap with
    | Some (`Record, a) -> record (Hole.value (Layout.leaf a a)) [(a, t)]
    | Some (`Variant, a) -> variant (Hole.value (Layout.leaf a a)) [(a, t)]
    | None -> t
  in
  Michelson.cata_mtype ~wrap f
