(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Basics
open Typed
open Utils
open Ternary

let pp_constraint ppf =
  let open Type in
  function
  | HasAdd (_e, e1, e2) ->
      Format.fprintf ppf "@[<v>HasAdd@;<0 2>%a@;<0 2>%a@]" pp e1.et pp e2.et
  | HasMul (_e, e1, e2) -> Format.fprintf ppf "HasMul %a, %a" pp e1.et pp e2.et
  | HasSub (_e, e1, e2) -> Format.fprintf ppf "HasSub %a, %a" pp e1.et pp e2.et
  | HasDiv (_e, e1, e2) -> Format.fprintf ppf "HasDiv %a, %a" pp e1.et pp e2.et
  | HasBitArithmetic (_e, e1, e2) ->
      Format.fprintf ppf "HasBitArithmetic %a, %a" pp e1.et pp e2.et
  | HasMap (_e, e1, e2) -> Format.fprintf ppf "HasMap %a, %a" pp e1.et pp e2.et
  | IsComparable e -> Format.fprintf ppf "IsComparable %a" pp e.et
  | IsPackable t -> Format.fprintf ppf "IsPackable %a" pp t
  | HasGetItem (e1, e2, _) ->
      Format.fprintf ppf "HasGetItem %a, %a" pp e1.et pp e2.et
  | HasContains (e1, e2, _line_no) ->
      Format.fprintf ppf "HasContains %a, %a" pp e1.et pp e2.et
  | HasSize e -> Format.fprintf ppf "HasSize %a" pp e.et
  | HasSlice e -> Format.fprintf ppf "HasSlice %a" pp e.et
  | AssertEqual (t1, t2, _pp) ->
      Format.fprintf ppf "@[<v>AssertEqual@;<0 2>%a@;<0 2>%a@]" pp t1 pp t2
  | IsInt (t, _pp) -> Format.fprintf ppf "IsInt %a" pp t
  | SaplingVerify (state, transaction) ->
      Format.fprintf ppf "SaplingVerify %a %a" pp state.et pp transaction.et
  | HasNeg (e, t) -> Format.fprintf ppf "HasNeg %a %a" pp e.et pp t
  | HasInt e -> Format.fprintf ppf "HasInt %a" pp e.et
  | IsNotHot (n, t) -> Format.fprintf ppf "IsNotHot %s %a" n pp t
  | IsAnyMap (tk, tv, e) ->
      Format.fprintf ppf "IsAnyMap %a, %a, %a" pp tk pp tv pp e.et
  | IsConvertible (t1, t2) ->
      Format.fprintf ppf "IsConvertible %a, %a" pp t1 pp t2
  | IsInstance2 (cls, _, (t1, t2), _) ->
      Format.fprintf ppf "IsInstance2 %s, %a, %a" cls pp t1 pp t2

let show_constraint = Format.asprintf "%a" pp_constraint

let equal_constraint (l1, c1) (l2, c2) =
  l1 = l2 && equal_typing_constraint c1 c2

let progress cs cs' =
  List.length cs <> List.length cs'
  || not (List.for_all2 equal_constraint cs cs')

let unresolved_error ~line_no = function
  | AssertEqual _ -> assert false
  | HasSub (e, e1, e2) ->
      [
        `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be subtracted in "
      ; `Expr e
      ; `Line line_no
      ]
  | HasDiv (e, e1, e2) ->
      [
        `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be divided in "
      ; `Expr e
      ; `Br
      ; `Text "Allowed types are (int|nat, int|nat) and (tez, tez|nat)"
      ; `Line line_no
      ]
  | HasMap (e, e1, e2) ->
      [
        `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be map in "
      ; `Expr e
      ; `Line line_no
      ]
  | HasBitArithmetic (e, e1, e2) ->
      [
        `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be xor-ed in "
      ; `Expr e
      ; `Line line_no
      ]
  | IsComparable e ->
      [
        `Text "Type error"
      ; `Expr e
      ; `Text "doesn't have a comparable type"
      ; `Line line_no
      ]
  | IsPackable t ->
      [`Text "Type error"; `Type t; `Text "is not packable"; `Line line_no]
  | HasGetItem (l, pos, _t) ->
      [`Text "Type Error"; `Expr l; `Text "cannot get item"; `Expr pos]
  | HasContains (items, member, line_no) ->
      [
        `Text "Type Error"
      ; `Expr items
      ; `Text "cannot contains"
      ; `Expr member
      ; `Line line_no
      ]
  | HasSize e ->
      [
        `Text "Type Error"; `Expr e; `Text "has no length or size"; `Line line_no
      ]
  | HasSlice e ->
      [`Text "Type Error"; `Expr e; `Text "cannot be sliced"; `Line line_no]
  | HasAdd (e, e1, e2) ->
      [
        `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be added in "
      ; `Expr e
      ; `Line line_no
      ]
  | HasMul (e, e1, e2) ->
      [
        `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be multiplied "
      ; `Expr e
      ; `Line line_no
      ]
  | IsInt (_, pp) -> pp ()
  | SaplingVerify (state, transaction) ->
      [`Text "memo_size error"; `Exprs [state; transaction]; `Line line_no]
  | HasNeg (e, _) ->
      [`Text "Type Error"; `Expr e; `Text "cannot be negated"; `Line line_no]
  | HasInt e ->
      [
        `Text "Type Error"; `Expr e; `Text "cannot be cast to Int"; `Line line_no
      ]
  | IsNotHot (n, t) ->
      [
        `Text "Variable"
      ; `Text n
      ; `Text "of type"
      ; `Type t
      ; `Text "cannot be used twice because it contains a ticket."
      ; `Line line_no
      ]
  | IsAnyMap (tk, tv, e) ->
      [
        `Expr e
      ; `Text "of type"
      ; `Type e.et
      ; `Text "is not a map from"
      ; `Type tk
      ; `Text "to"
      ; `Type tv
      ; `Line line_no
      ]
  | IsConvertible (t1, t2) ->
      [
        `Type t1
      ; `Text "does not have the same Michelson type as"
      ; `Type t2
      ; `Line line_no
      ]
  | IsInstance2 (cls, _, (t1, t2), _) ->
      [
        `Text (Printf.sprintf "Type class %S" cls)
      ; `Text "does not have instance"
      ; `Text (Type.show t1)
      ; `Text ", "
      ; `Text (Type.show t2)
      ; `Line line_no
      ]

let is_comparable =
  Type.cata (function
    | TInt _ | TBounded _
    | T0
        ( T_unit
        | T_never
        | T_bool
        | T_string
        | T_chain_id
        | T_bytes
        | T_mutez
        | T_key_hash
        | T_key
        | T_signature
        | T_timestamp
        | T_address ) -> Yes
    | T0
        ( T_operation
        | T_bls12_381_g1
        | T_bls12_381_g2
        | T_bls12_381_fr
        | T_chest
        | T_chest_key )
    | T1 ((T_list | T_set | T_contract | T_ticket), _)
    | T2 ((T_map | T_big_map), _, _)
    | TLambda _ | TSaplingState _ | TSaplingTransaction _ | TSecretKey -> No
    | TUnknown _ -> Maybe
    | TRecord {var = Some _} as t -> Type.fold_f and_ Maybe t
    | TVariant {var = Some _} as t -> Type.fold_f and_ Maybe t
    | (TTuple (_, var) | TRecord {var} | TVariant {var}) as t ->
        Type.fold_f and_ (if var = None then Yes else Maybe) t
    | T1 (T_option, _) as t -> Type.fold_f and_ Yes t
    | T0 (T_nat | T_int | T_sapling_transaction _ | T_sapling_state _)
    | T2 ((T_pair _ | T_or _ | T_lambda), _, _) -> assert false)

let is_packable =
  let of_bool x = if x then Yes else No in
  let kto_bool x k =
    match x with
    | Yes -> k true
    | No -> k false
    | Maybe -> Maybe
  in
  Type.cata
    (let open Michelson_base.Type in
    let open Michelson_base.Typing in
    function
    | T0 t -> of_bool (is_packable_f (MT0 t))
    | T1 (t, p) -> kto_bool p (fun p -> of_bool (is_packable_f (MT1 (t, p))))
    | T2 (t, p1, p2) ->
        kto_bool p1 (fun p1 ->
            kto_bool p2 (fun p2 -> of_bool (is_packable_f (MT2 (t, p1, p2)))))
    | TInt _ | TLambda _ | TSaplingTransaction _ -> Yes
    | TSaplingState _ | TSecretKey -> No
    | TBounded {base} -> base
    | TUnknown _ -> Maybe
    | TRecord {var = None} as t -> Type.fold_f and_ Maybe t
    | (TTuple _ | TRecord _ | TVariant _) as t -> Type.fold_f and_ Yes t)

let reduce_constraint ~config rsubst (line_no, c) =
  let pp () = unresolved_error ~line_no c in
  let unresolved = (line_no, c) in
  let assert_equal ?(pp = pp) t1 t2 = (line_no, AssertEqual (t1, t2, pp)) in
  match c with
  | AssertEqual (t1, t2, pp) ->
      let cs =
        try Unifier.unify ~config pp rsubst t1 t2
        with Unifier.Unification_failed e ->
          raise
            (SmartExcept ((`Text "Type Error" :: `Br :: e) @ [`Br; `Rec (pp ())]))
      in
      List.map (fun c -> (line_no, c)) cs
  | HasSub (e, e1, _e2) -> (
      match Type.unF e1.et with
      | T0 T_mutez -> [assert_equal e1.et e.et]
      | TInt _ -> [assert_equal e.et Type.int]
      | T0 T_timestamp -> [assert_equal e.et Type.int]
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | HasDiv (e, e1, e2) -> (
      let fix_cst_intOrNat_as_nat x =
        match (x.e, Type.unF x.et) with
        | EPrim0 (ELiteral _), TInt {isNat} -> (
            match Hole.get isNat with
            | None -> (Some true, [assert_equal x.et Type.nat])
            | x -> (x, []))
        | _, TInt {isNat} -> (Hole.get isNat, [])
        | _ -> (None, [])
      in
      let a_b, cs =
        match (Type.unF e1.et, Type.unF e2.et) with
        | _, T0 T_mutez ->
            (`OK (Type.nat, Type.mutez), [assert_equal e1.et Type.mutez])
        | T0 T_mutez, TInt _ ->
            (`OK (Type.mutez, Type.mutez), [assert_equal e2.et Type.nat])
        | TInt _, TInt _ ->
            let r1, cs1 = fix_cst_intOrNat_as_nat e1 in
            let r2, cs2 = fix_cst_intOrNat_as_nat e2 in
            ( (match (r1, r2) with
              | Some true, Some true -> `OK (Type.nat, Type.nat)
              | Some false, Some true
              | Some true, Some false
              | Some false, Some false -> `OK (Type.int, Type.nat)
              | None, _ | _, None -> `Unknown)
            , cs1 @ cs2 )
        | TUnknown _, _ -> (`Unknown, [])
        | _, TUnknown _ -> (`Unknown, [])
        | _ -> raise (SmartExcept (pp ()))
      in
      match a_b with
      | `Unknown -> unresolved :: cs
      | `OK (a, b) ->
          let target_type = Type.option (Type.pair a b) in
          let pp () =
            [
              `Text "Type Error"
            ; `Expr e
            ; `Text "is not compatible with type"
            ; `Type target_type
            ; `Br
            ; `Line line_no
            ]
          in
          assert_equal ~pp e.et target_type :: cs)
  | HasMap (e, l, f) -> (
      match Type.unF f.et with
      | TLambda ({with_storage; with_operations}, s', t') -> (
          match (Hole.get with_storage, Hole.get with_operations) with
          | Some (Some _), _ | _, Some true ->
              raise
                (SmartExcept
                   [
                     `Text "Type Error"
                   ; `Text "Cannot map effectful lambda"
                   ; `Line line_no
                   ])
          | None, _ | _, None -> [unresolved]
          | _ -> (
              match Type.unF l.et with
              | T1 (T_list, t) ->
                  [assert_equal t s'; assert_equal e.et (Type.list t')]
              | T1 (T_option, t) ->
                  [assert_equal t s'; assert_equal e.et (Type.option t')]
              | T2 (T_map, tkey, tvalue) ->
                  [
                    assert_equal (Type.key_value tkey tvalue) s'
                  ; assert_equal e.et (Type.map ~big:false ~tkey ~tvalue:t')
                  ]
              | TUnknown _ -> [unresolved]
              | _ -> raise (SmartExcept (pp ()))))
      | T2 (T_lambda, _, _) -> assert false
      | _ -> raise (SmartExcept (pp ())))
  | HasBitArithmetic (e, e1, _e2) -> (
      match Type.unF e1.et with
      | T0 T_bool -> []
      | TInt _ -> [assert_equal e.et Type.nat]
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | IsComparable e -> (
      match is_comparable e.et with
      | Yes -> []
      | No -> raise (SmartExcept (pp ()))
      | Maybe -> [unresolved])
  | IsPackable t -> (
      match is_packable t with
      | Yes -> []
      | No -> raise (SmartExcept (pp ()))
      | Maybe -> [unresolved])
  | HasGetItem (l, pos, t) -> (
      match Type.unF l.et with
      | T2 ((T_map | T_big_map), tkey, tvalue) ->
          [assert_equal tkey pos.et; assert_equal tvalue t]
      | T1 (T_list, _) ->
          let pp () =
            [
              `Rec (pp ())
            ; `Br
            ; `Text "A list is not a map."
            ; `Br
            ; `Text
                (Format.sprintf
                   " You can use:\n\
                   \ - sp.utils.vector(..) to create a map from a list,\n\
                   \ - sp.utils.matrix(..) to create a map of maps from a list \
                    of lists,\n\
                   \ - sp.utils.cube(..) for a list of lists of lists.")
            ]
          in
          raise (SmartExcept (pp ()))
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | HasContains (items, member, _line_no) -> (
      match Type.unF items.et with
      | T1 (T_set, telement) -> [assert_equal telement member.et]
      | T2 ((T_map | T_big_map), tkey, _) -> [assert_equal tkey member.et]
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | HasSize e -> (
      match Type.unF e.et with
      | T0 (T_nat | T_int | T_sapling_transaction _ | T_sapling_state _) ->
          assert false
      | T1 (T_list, _) | T0 (T_string | T_bytes) | T1 (T_set, _) -> []
      | T2 (T_map, _, _) -> []
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | HasSlice e -> (
      match Type.unF e.et with
      | T0 (T_string | T_bytes) | TBounded _ -> []
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | HasAdd (_e, e1, _e2) -> (
      match Type.unF e1.et with
      | TInt _
      | T0
          ( T_mutez
          | T_string
          | T_bytes
          | T_bls12_381_g1
          | T_bls12_381_g2
          | T_bls12_381_fr ) -> []
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | HasMul (_e, e1, _e2) -> (
      match Type.unF e1.et with
      | TInt _ | T0 (T_mutez | T_bls12_381_g1 | T_bls12_381_g2 | T_bls12_381_fr)
        -> []
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | IsInt (t, pp) -> (
      match Type.unF t with
      | TInt _ -> []
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | SaplingVerify (state, transaction) -> (
      match (Type.unF state.et, Type.unF transaction.et) with
      | TSaplingState {memo = m1}, TSaplingTransaction {memo = m2} ->
          let err () = [`Text "memo_size mismatch"] in
          Unifier.equalize_holes ~err ~eq:( = )
            (fun i -> Substitution.SInt i)
            rsubst m1 m2;
          []
      | _ -> raise (SmartExcept (pp ())))
  | HasNeg (e, t) -> (
      match (Type.unF t, Type.unF e.et) with
      | T0 (T_bls12_381_g1 | T_bls12_381_g2 | T_bls12_381_fr), _
      | _, T0 (T_bls12_381_g1 | T_bls12_381_g2 | T_bls12_381_fr) ->
          [assert_equal t e.et]
      | TInt _, _ | _, TInt _ ->
          [assert_equal e.et (Type.intOrNat ()); assert_equal t Type.int]
      | _, TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | HasInt e -> (
      let pp () =
        [
          `Text "Type Error"
        ; `Expr e
        ; `Text "cannot be cast to Int"
        ; `Line e.line_no
        ]
      in
      match Type.unF e.et with
      | TInt _ -> [assert_equal e.et Type.nat]
      | T0 T_bls12_381_fr -> []
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | IsNotHot (_, t) -> (
      match Type.is_hot t with
      | No -> []
      | Yes -> raise (SmartExcept (pp ()))
      | Maybe -> [unresolved])
  | IsAnyMap (tk, tv, e) -> (
      match Type.unF e.et with
      | T2 ((T_map | T_big_map), tkey, tvalue) ->
          [assert_equal tk tkey; assert_equal tv tvalue]
      | TUnknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | IsConvertible (t1, t2) ->
      if Type.has_unknowns t1 || Type.has_unknowns t2
      then [unresolved]
      else
        let t1 = Typing.mtype_of_type ~with_annots:false t1 in
        let t2 = Typing.mtype_of_type ~with_annots:false t2 in
        if Michelson.equal_mtype t1 t2
           (* NB We could call Michelson.unifiable_types and propagate the
              resulting substitution here. *)
        then []
        else raise (SmartExcept (pp ()))
  | IsInstance2 (_cls, e, (t1, t2), instances) -> (
      if Type.has_holes t1 || Type.has_holes t2
      then [unresolved]
      else
        match List.assoc_opt (t1, t2) instances with
        | None -> raise (SmartExcept (pp ()))
        | Some t -> [assert_equal e.et t])

let default_constraint ~config:_ _rsubst (line_no, c) =
  let pp () = unresolved_error ~line_no c in
  let unresolved = (line_no, c) in
  let assert_equal t1 t2 = (line_no, AssertEqual (t1, t2, pp)) in
  match c with
  | IsAnyMap (tkey, tvalue, e) ->
      [assert_equal e.et (Type.map ~big:false ~tkey ~tvalue)]
  | _ -> [unresolved]

let pass ~config reduce =
  let rec pass subst r = function
    | [] ->
        let r =
          List.map (fun (ln, c) -> (ln, Substitution.on_constraint subst c)) r
        in
        (subst, List.rev r)
    | c :: cs ->
        let c = Control.map_snd (Substitution.on_constraint subst) c in
        let cs' = reduce ~config subst c in
        let p = progress [c] cs' in
        if p then pass subst r (cs' @ cs) else pass subst (cs' @ r) cs
  in
  fun subst cs -> pass subst [] cs

let run ~config constraints =
  let verbosity = 0 in
  let debug v f = if verbosity >= v then f () in
  debug 1 (fun () -> Format.(pp_set_margin std_formatter 300));
  let rec passes subst i = function
    | [] -> (subst, [])
    | cs when i > 100 ->
        ( subst
        , List.map (fun (ln, c) -> (ln, Substitution.on_constraint subst c)) cs
        )
    | cs ->
        debug 2 (fun () ->
            List.iter
              (fun (_, c) -> Format.printf "    %a@." pp_constraint c)
              cs);
        debug 1 (fun () -> Format.printf "  Pass #%d...@." i);
        let subst, cs' = pass ~config reduce_constraint subst cs in
        let p = progress cs cs' in
        debug 3 (fun () -> Format.printf "    Progress: %b@." p);
        if p
        then passes subst (i + 1) cs'
        else
          let subst, cs'' = pass ~config default_constraint subst cs in
          let p = progress cs' cs'' in
          debug 3 (fun () -> Format.printf "    Progress (defaulting): %b@." p);
          if p then passes subst (i + 1) cs'' else (subst, cs'')
  in
  debug 1 (fun () -> print_endline "Constraint resolution...");
  let subst, cs = passes (Substitution.empty ()) 1 (List.rev constraints) in
  let mandatory = function
    | _, IsPackable _ -> false
    | _, IsComparable _ -> false
    | _, IsNotHot _ -> false
    | _ -> true
  in
  (match List.find_opt mandatory cs with
  | None -> ()
  | Some (line_no, c) ->
      raise
        (SmartExcept
           (`Text "Missing typing information!" :: `Br
           :: `Text (show_constraint c)
           :: `Br
           :: unresolved_error ~line_no c)));
  debug 1 (fun () -> print_endline "All constraints resolved.");
  debug 2 (fun () -> Substitution.dump subst);
  subst
