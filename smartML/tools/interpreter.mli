(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Utils
open Basics
open Typed

type context
(** The initial state of the execution environment, a.k.a. this
    provides a “pseudo-blockchain.” *)

val context_sender : context -> string option

val context_time : context -> Bigint.t

val context_amount : context -> Bigint.t

val context_contract_id : context -> contract_id option

val context_line_no : context -> line_no

val context_debug : context -> bool

val build_context :
     ?sender:string
  -> ?source:string
  -> amount:Bigint.t
  -> scenario_state:scenario_state
  -> line_no:line_no
  -> debug:bool
  -> unit
  -> context
(** Build a {!context}. *)

val interpret_message :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> context
  -> contract_id
  -> tmessage
  -> tinstance option
     * toperation list
     * Execution.error option
     * Execution.step list
(** Evaluation of a contract call ({!tmessage}) within a {!context}. *)

val interpret_expr_external :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> no_env:Basics.smart_except list
  -> ?source:string
  -> ?sender:string
  -> scenario_state:scenario_state
  -> texpr
  -> value
(** Evaluation of an expression ({!texpr}) within a {!context}. *)

val interpret_contract :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> tcontract
  -> instance

val update_contract_address :
  ?address:string -> scenario_state -> contract_id -> unit

val update_contract :
  ?address:string -> scenario_state -> contract_id -> tinstance -> unit

val get_contract_address : scenario_state -> contract_id -> string
