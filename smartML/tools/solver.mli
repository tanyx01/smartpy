(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Basics

val run :
  config:Config.t -> (line_no * typing_constraint) list -> Substitution.t
