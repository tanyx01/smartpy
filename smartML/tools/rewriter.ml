(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Basics
open Untyped
open Utils
open Control

type step =
  | Proj of int
  | Attr of string
[@@deriving eq]

(* NB Stops at first map lookup *)
let rec path_of_lexpr ~config e =
  let module Printer = (val Printer.get config : Printer.Printer) in
  match e.e with
  | EVar (x, _) -> (x, [])
  | EPrim1 (EProject i, e) ->
      let root, steps = path_of_lexpr ~config e in
      (root, steps @ [Proj i])
  | EPrim1 (EAttr a, e) ->
      let root, steps = path_of_lexpr ~config e in
      (root, steps @ [Attr a])
  | EItem {items} -> path_of_lexpr ~config items
  | _ -> failwith ("path_of_lexpr: " ^ Printer.expr_to_string e)

(* Calculates all paths mentioned in the expression. *)
let all_accesses =
  let module W = Writer (struct
    type t = string * step list
  end) in
  let open W in
  let open Syntax (W) in
  let extend step (root, steps) = (root, steps @ [step]) in
  let close = Option.cata (return ()) write in
  let fm_expr _line_no = function
    | EVar (x, _) -> return (Some (x, []))
    | EPrim1 (EProject i, e) -> return (Option.map (extend (Proj i)) e)
    | EPrim1 (EAttr a, e) -> return (Option.map (extend (Attr a)) e)
    | e ->
        let* _ =
          sequence_expr_f (Basics.map_expr_f close close id elim_untyped e)
        in
        return None
  in
  let fm_command _line_no = assert false in
  let fm_type _line_no = assert false in
  let alg = {fm_expr; fm_command; fm_type} in
  fun e -> fst (run (cataM_expr alg e >>= close))

(* Check that neither the given path nor any of its prefixes is
   accessed. *)
let does_not_access (root, steps) e =
  let e = erase_types_expr e in
  let is_ok (root', steps') =
    root' <> root || not (List.is_prefix equal_step steps steps')
  in
  List.for_all is_ok (all_accesses e)

let embellish_alg ~config =
  let open Typed in
  let f_texpr line_no et e =
    let e =
      match e with
      | EPrim2
          ( ((EEq | ENeq) as op)
          , {e = EMPrim2 (Compare, x, y)}
          , {e = EPrim0 (ELiteral (Int {i}))} )
        when Bigint.(equal i zero_big_int) -> EPrim2 (op, x, y)
      | EMPrim2 (Mul, x, y) when Type.equal x.et y.et -> EPrim2 (EMul_homo, x, y)
      | EMPrim2 (Add, x, y) when Type.equal x.et y.et -> EPrim2 (EAdd, x, y)
      | e -> e
    in
    Typed.build_texpr ~line_no e et
  in
  let f_tcommand line_no ct c =
    let mke t e = Typed.build_texpr ~line_no e t in
    let mkc ct c = Typed.build_tcommand ~line_no c ct in
    let mkc_ e = Typed.build_tcommand ~line_no e Type.unit in
    let proj t i e = mke t (EPrim1 (EProject i, e)) in
    let attr t a e = mke t (EPrim1 (EAttr a, e)) in
    let set_proj i lhs rhs = mkc_ (CSet_var (proj rhs.et i lhs, rhs)) in
    let set_attr a lhs rhs = mkc_ (CSet_var (attr rhs.et a lhs, rhs)) in
    let path ~config e x =
      let r, steps = path_of_lexpr ~config (erase_types_expr e) in
      (r, steps @ [x])
    in
    let c =
      match c with
      | CIf (e, x, {c = CFailwith msg}) ->
          CBind (None, mkc_ (CVerify (e, Some msg)), x)
      | CSet_var (lhs, rhs) when equal_texpr lhs rhs ->
          CResult (mke Type.unit (EPrim0 (ELiteral Literal.unit)))
      | CSet_var (lhs, {e = ETuple [x; y]})
        when does_not_access (path ~config lhs (Proj 0)) y ->
          CBind (None, set_proj 0 lhs x, set_proj 1 lhs y)
      | CSet_var (lhs, {e = ETuple [x; y]})
        when does_not_access (path ~config lhs (Proj 1)) x ->
          CBind (None, set_proj 1 lhs y, set_proj 0 lhs x)
      | CSet_var (lhs, {e = ERecord [(a0, x0); (a1, x1)]})
        when does_not_access (path ~config lhs (Attr a0)) x1 ->
          CBind (None, set_attr a0 lhs x0, set_attr a1 lhs x1)
      | CBind (None, {c = CResult {e = EPrim0 (ELiteral Literal.Unit)}}, {c}) ->
          c
      | CBind (None, {c}, {c = CResult {e = EPrim0 (ELiteral Literal.Unit)}}) ->
          c
      | CSet_var
          ( lhs
          , {
              e =
                EPrim2
                  ( ((EAdd | EMul_homo) as prim2)
                  , ({e = EPrim0 (ELiteral _)} as x)
                  , y )
            ; et
            } ) -> CSet_var (lhs, mke et (EPrim2 (prim2, y, x)))
      | CBind
          ( _
          , {c = CDefine_local {var = x; rhs = e}}
          , {c = CSet_var (lhs, {e = EVar (x', _)})} )
        when x = x' && false ->
          CSet_var (lhs, e) (* invalid if x is used again later *)
      | CBind (Some x, {c = CResult e}, {c = CSet_var (lhs, {e = EVar (x', _)})})
        when x = x' && false ->
          CSet_var (lhs, e) (* invalid if x is used again later *)
      | CBind (x2, {c = CBind (x1, e1, e2)}, e3) ->
          CBind (x1, e1, mkc e3.ct (CBind (x2, e2, e3)))
      (*
    | CBind (Some x, {c = CMatch (s, clauses)}, c) when false ->
        let f (cons, arg, rhs) =
          (cons, arg, {c = CBind (Some x, rhs, c); line_no})
        in
        mk (CMatch (s, List.map f clauses))
        (* valid only if at most one branch does not fail *)
    *)
      | CBind (_, {c = CFailwith _ as fw}, _) -> fw
      | c -> c
    in
    Typed.build_tcommand ~line_no c ct
  in
  {f_texpr; f_tcommand; f_ttype = id}

let embellish_command ~config = cata_tcommand (embellish_alg ~config)

let rec fixpoint n f x =
  if n = 0 then failwith "rewriter: failed to converge";
  let y = f x in
  if equal_contract (erase_types_contract x) (erase_types_contract y)
  then x
  else fixpoint (n - 1) f y

let embellish_contract ~config =
  let step {tcontract} =
    let embellish_entrypoint (ep : _ entrypoint) =
      {ep with body = embellish_command ~config ep.body}
    in
    let embellish_view (v : _ view) =
      {v with body = embellish_command ~config v.body}
    in
    let tcontract =
      {
        tcontract with
        entrypoints = List.map embellish_entrypoint tcontract.entrypoints
      ; views = List.map embellish_view tcontract.views
      }
    in
    let c = erase_types_contract {tcontract} in
    Checker.check_contract config c
  in
  fixpoint 100 step
