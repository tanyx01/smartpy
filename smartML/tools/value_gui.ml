(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Utils
open Value

type inputGuiResult = {
    gui : string
  ; get : bool -> t
}

let textInputGui ?geometry path ?default nextId initialType cont =
  let id = nextId () in
  let gui =
    match geometry with
    | None -> Printf.sprintf "<input type='text' id='%s'>" id
    | Some (rows, columns) ->
        Printf.sprintf
          "<textarea type='text' class='resize_both' id='%s' rows=%i \
           cols=%i></textarea>"
          id rows columns
  in
  let get _withZeros =
    let s = SmartDom.getText id in
    try
      match (s, default) with
      | "", Some x -> cont x
      | x, _ -> cont x
    with exn ->
      let error =
        match exn with
        | Failure error -> Printf.sprintf " (%s)" error
        | _ -> ""
      in
      failwith
        (Printf.sprintf "Cannot convert '%s' into %s for path %s%s" s
           initialType
           (String.concat "." (List.rev path))
           error)
  in
  {gui; get}

let error s = string s

let parseIntOrNat isNat s =
  let i = Bigint.of_string ~msg:"value_gui" s in
  match Typing.intType isNat with
  | `Nat ->
      if Big_int.compare_big_int i Big_int.zero_big_int < 0
      then failwith (Printf.sprintf "%s is not a nat" s);
      nat i
  | `Unknown -> intOrNat (Hole.mk ()) i
  | `Int -> int i
  | `Error -> assert false

let enum_list t =
  Type.variant_default_layout Config.Comb
    [
      ("Zero", Type.unit)
    ; ("One", t)
    ; ("Two", Type.pair t t)
    ; ("Three", Type.tuple [t; t; t])
    ; ("Four", Type.tuple [t; t; t; t])
    ]

let fields_of_layout layout row =
  let module Printer = (val Printer.get_by_language ~config:Config.default
                              Config.SmartPy : Printer.Printer)
  in
  match Hole.get layout with
  | None -> List.map (fun (x, _) -> (x, x)) row
  | Some layout ->
      let leaf Layout.{source; target} = [(source, target)] in
      Binary_tree.cata leaf ( @ ) layout

let unhex_auto_chop s =
  let s = Base.(String.chop_prefix s ~prefix:"0x" |> Option.value ~default:s) in
  Hex.to_string (`Hex s)

let rec inputGuiR path nextId initialType =
  let module Printer = (val Printer.get_by_language ~config:Config.default
                              Config.SmartPy : Printer.Printer)
  in
  match Type.unF initialType with
  | T0 T_bool ->
      let id = nextId () in
      let gui = Printf.sprintf "<input type='checkbox' id='%s'>" id in
      let get _ = bool (SmartDom.isChecked id) in
      {gui; get}
  | TInt {isNat} -> textInputGui path nextId "int" (parseIntOrNat isNat)
  | TBounded {base = _} ->
      assert false (*textInputGui path nextId "string" string*)
  | T0 T_string -> textInputGui path nextId "string" string
  | T0 T_bytes ->
      textInputGui path nextId "bytes" (fun s -> bytes (unhex_auto_chop s))
  | T0 T_mutez ->
      let {gui; get} =
        inputGuiR path nextId
          (Type.variant_default_layout Config.Comb
             [("Tez", Type.nat); ("Mutez", Type.nat)])
      in
      let get context =
        match (get context).v with
        | Variant ("Tez", i) ->
            Value.mutez
              (Big_int.mult_big_int (Bigint.of_int 1000000)
                 (Value.unInt ~pp:(fun () -> []) i))
        | Variant ("Mutez", i) -> Value.mutez (Value.unInt ~pp:(fun () -> []) i)
        | _ -> assert false
      in
      {gui; get}
      (* textInputGui path nextId "token" (fun i ->
       *     mutez (Big_int.of_string ~msg:"value_gui" i)) *)
  | T0 T_key_hash -> textInputGui path nextId "key_hash" Value.key_hash
  | T0 T_timestamp ->
      textInputGui path nextId "timestamp" (fun i ->
          timestamp (Bigint.of_string ~msg:"value_gui" i))
  | T0 T_chain_id ->
      textInputGui path nextId "chainid" (fun s -> chain_id (unhex_auto_chop s))
  | TSecretKey -> textInputGui path nextId "secret_key" secret_key
  | T0 T_address ->
      textInputGui path nextId "address" (fun s ->
          if not
               (Base.String.is_prefix ~prefix:"tz" s
               || Base.String.is_prefix ~prefix:"KT" s)
          then failwith (Printf.sprintf "Badly formed address '%s'" s);
          address s)
  | T1 (T_contract, _) ->
      textInputGui path nextId "contract" (fun s ->
          if not (Base.String.is_prefix ~prefix:"KT" s)
          then failwith (Printf.sprintf "Badly formed contract '%s'" s);
          contract s)
  | T0 T_key -> textInputGui path nextId "key" key
  | T0 T_signature -> textInputGui path nextId "signature" signature
  | (T1 (T_option, _) | TVariant _) as t ->
      let as_option, fields, row =
        match t with
        | TVariant {layout; row} -> (None, fields_of_layout layout row, row)
        | T1 (T_option, t) ->
            let row = [("None", Type.unit); ("Some", t)] in
            let fields = List.map (fun (x, _) -> (x, x)) row in
            (Some t, fields, row)
        | _ -> assert false
      in
      let sub_variant_guis =
        List.mapi
          (fun i (name, target) ->
            let t = List.assoc name row in
            let input = inputGuiR (name :: path) nextId t in
            sub_variant_gui ~input ~nextId ~as_option i (name, target))
          fields
      in
      input_variant ~radio:false ~sub_variant_guis ~nextId ~initialType
  | TRecord {layout; row; var = None} ->
      let subs =
        List.map
          (fun (name, t) -> (name, inputGuiR (name :: path) nextId t))
          row
      in
      let gui =
        let fields = fields_of_layout layout row in
        Printer.render_record_list
          ( List.map snd fields
          , [List.map (fun (source, _) -> (List.assoc source subs).gui) fields]
          )
          (fun x -> x)
      in
      let get context =
        record (List.map (fun (name, input) -> (name, input.get context)) subs)
      in
      {gui; get}
  | T0 T_unit -> {gui = ""; get = (fun _ -> unit)}
  | TTuple (ts, None) ->
      let subs =
        List.map
          (fun (i, t) ->
            let name = string_of_int i in
            (name, inputGuiR (name :: path) nextId t))
          ts
      in
      let gui =
        Printer.render_record_list
          ([], [List.map (fun (_, input) -> input.gui) subs])
          (fun x -> x)
      in
      let get context =
        Value.tuple (List.map (fun (_name, input) -> input.get context) subs)
      in
      {gui; get}
  | TUnknown _ | TRecord {var = Some _} | TTuple (_, Some _) ->
      let notImplemented =
        Printf.sprintf "InputGui of partial types (%s) not implemented"
          (Printer.type_to_string initialType)
      in
      {gui = notImplemented; get = (fun _ -> error notImplemented)}
  | TLambda _ ->
      let config = Config.default in
      let parse_lambda s =
        let mich = Micheline_encoding.parse_node s in
        let pp_mich t i =
          Michelson.display_instr ~protocol:config.protocol
            (Typing.mtype_of_type ~with_annots:false t)
            (Michelson.Of_micheline.instruction i)
        in
        Value.of_micheline ~config ~pp_mich initialType mich
      in
      textInputGui ~geometry:(5, 80) path nextId "lambda" parse_lambda
  | T2 (T_lambda, _, _) -> assert false
  | T1 (T_list, t) -> input_list (fun l _ -> Value.build_list l) t path nextId
  | T1 (T_set, telement) ->
      input_list (fun l telement -> Value.set l ~telement) telement path nextId
  | T2 ((T_map | T_big_map), tkey, tvalue) ->
      input_list
        (fun l _ ->
          Value.map ~tkey
            (List.map
               (function
                 | ({v = Tuple [x; y]} : t) -> (x, y)
                 | _ -> assert false)
               l))
        (Type.pair tkey tvalue) path nextId
  | T0 T_operation ->
      let gui = "Type operation not handled" in
      {gui; get = (fun _ -> error gui)}
  | TSaplingState _ ->
      let gui = "Type sapling_state not handled" in
      {gui; get = (fun _ -> error gui)}
  | TSaplingTransaction _ ->
      let gui = "Type sapling_transaction not handled" in
      {gui; get = (fun _ -> error gui)}
  | T0 T_never ->
      let gui = "Type never has no value" in
      {gui; get = (fun _ -> error gui)}
  | T1 (T_ticket, _) ->
      let gui = "Type ticket cannot be forged" in
      {gui; get = (fun _ -> error gui)}
  | T0 T_bls12_381_g1 ->
      textInputGui path nextId "bls12_381_g1" (fun s ->
          bls12_381_g1 (unhex_auto_chop s))
  | T0 T_bls12_381_g2 ->
      textInputGui path nextId "bls12_381_g2" (fun s ->
          bls12_381_g2 (unhex_auto_chop s))
  | T0 T_bls12_381_fr ->
      textInputGui path nextId "bls12_381_fr" (fun s ->
          bls12_381_fr (unhex_auto_chop s))
  | T0 T_chest_key ->
      textInputGui path nextId "chest_key" (fun s ->
          chest_key (unhex_auto_chop s))
  | T0 T_chest ->
      textInputGui path nextId "chest" (fun s -> chest (unhex_auto_chop s))
  | ( T0 (T_nat | T_int | T_sapling_state _ | T_sapling_transaction _)
    | T2 ((T_pair _ | T_or _), _, _) ) as t -> failwith (Type.show (F t))

and input_list f t path nextId =
  let input = inputGuiR ("list" :: path) nextId (enum_list t) in
  let get context =
    match (input.get context).v with
    | Variant ("Zero", _) -> f [] t
    | Variant ("One", x) -> f [x] t
    | Variant ("Two", {v = Tuple [x; y]}) -> f [x; y] t
    | Variant ("Three", {v = Tuple [x1; x2; x3]}) -> f [x1; x2; x3] t
    | Variant ("Four", {v = Tuple [x1; x2; x3; x4]}) -> f [x1; x2; x3; x4] t
    | _ -> assert false
  in
  {input with get}

and sub_variant_gui ~input ~nextId ~as_option i (name, target) =
  let id = nextId () in
  let gui =
    Printf.sprintf "<div id='%s' class='%s'>%s</div>" id
      (if i = 0 then "" else "hidden")
      input.gui
  in
  let get x =
    match as_option with
    | Some _t -> if name = "None" then Value.none else Value.some (input.get x)
    | None -> variant name (input.get x)
  in
  (target, id, {gui; get})

and input_variant ~radio ~sub_variant_guis ~nextId ~initialType =
  let module Printer = (val Printer.get_by_language ~config:Config.default
                              Config.SmartPy : Printer.Printer)
  in
  let monoSelectionId = nextId () in
  let selection_gui =
    if radio
    then
      String.concat " &nbsp;&nbsp; "
        (List.mapi
           (fun i (name, _id, _) ->
             Printf.sprintf
               "<input type='radio' class='radio_selection' name='%s' \
                value='%s' onchange=\"setValueVisibility('%s',%s)\"%s>%s"
               monoSelectionId name name
               (String.concat ","
                  (List.map
                     (fun (name, id, _) -> Printf.sprintf "'%s','%s'" name id)
                     sub_variant_guis))
               (if i = 0 then " checked" else "")
               name)
           sub_variant_guis)
    else
      Printf.sprintf
        "<select class='selection' id='%s' \
         onchange=\"setSelectVisibility('%s',%s)\">%s</select>"
        monoSelectionId monoSelectionId
        (String.concat ","
           (List.map
              (fun (name, id, _) -> Printf.sprintf "'%s','%s'" name id)
              sub_variant_guis))
        (String.concat ""
           (List.map
              (fun (name, _, _) ->
                Printf.sprintf "<option value='%s'>%s</option>" name
                  (String.capitalize_ascii name))
              sub_variant_guis))
  in
  let gui =
    Printf.sprintf "<div class='subtype'>%s</div>%s" selection_gui
      (String.concat ""
         (List.map (fun (_, _, input) -> input.gui) sub_variant_guis))
  in
  let get context =
    let checked = if radio then Some () else None in
    let selected = SmartDom.getText ?checked monoSelectionId in
    match
      List.find_opt
        (fun (name, _id, _input) -> name = selected)
        sub_variant_guis
    with
    | Some (_, _, input) -> input.get context
    | None ->
        error
          ("Missing constructor " ^ selected ^ " in "
          ^ Printer.type_to_string initialType)
  in
  {gui; get}

let inputGuiR ?(path = []) ~nextId initialType =
  let input =
    let sub_variant_guis =
      let regular = inputGuiR path nextId initialType in
      let parse_micheline ~initialType syntax s =
        let mich =
          match syntax with
          | `JSON ->
              let open Micheline_encoding in
              let json = Ezjsonm.value_from_string s in
              let node = micheline_of_ezjsonm json in
              mich_of_node node
          | `Micheline -> Micheline_encoding.parse_node s
        in
        let mich =
          match mich with
          | Sequence [mich] -> mich
          | _ -> mich
        in
        let config = Config.default in
        let pp_mich t i =
          Michelson.display_instr ~protocol:config.protocol
            (Typing.mtype_of_type ~with_annots:false t)
            (Michelson.Of_micheline.instruction i)
        in
        Value.of_micheline ~config ~pp_mich initialType mich
      in
      let micheline_entry_points syntax =
        match Type.unF initialType with
        | TVariant {layout; row} ->
            let fields = fields_of_layout layout row in
            let sub_variant_guis =
              List.mapi
                (fun i (name, target) ->
                  let t = List.assoc name row in
                  let input =
                    textInputGui ~geometry:(5, 80) (name :: path) nextId name
                      (parse_micheline ~initialType:t syntax)
                  in
                  sub_variant_gui ~input ~nextId ~as_option:None i (name, target))
                fields
            in
            Some
              (input_variant ~radio:false ~sub_variant_guis ~nextId ~initialType)
        | _ -> None
      in
      let micheline_full syntax =
        textInputGui ~geometry:(5, 80) ("Micheline" :: path) nextId "micheline"
          (parse_micheline ~initialType syntax)
      in
      [
        sub_variant_gui ~input:regular ~nextId ~as_option:None 0
          ("Regular", "Regular")
      ]
      @ (match micheline_entry_points `Micheline with
        | None -> []
        | Some input ->
            [
              sub_variant_gui ~input ~nextId ~as_option:None 1
                ("MichelineEntryPoints", "Micheline")
            ])
      @ (match micheline_entry_points `JSON with
        | None -> []
        | Some input ->
            [
              sub_variant_gui ~input ~nextId ~as_option:None 2
                ("JSONEntryPoints", "JSON")
            ])
      @ [
          sub_variant_gui
            ~input:(micheline_full `Micheline)
            ~nextId ~as_option:None 3
            ("MichelineFull", "Micheline (Full Path)")
        ]
      @ [
          sub_variant_gui ~input:(micheline_full `JSON) ~nextId ~as_option:None 4
            ("JSONFull", "JSON (Full Path)")
        ]
    in
    input_variant ~radio:true ~sub_variant_guis ~nextId ~initialType
  in
  let get context =
    match (input.get context).v with
    | Variant ("Regular", x) -> x
    | Variant ("MichelineEntryPoints", x) -> x
    | Variant ("MichelineFull", x) -> x
    | Variant ("JSONEntryPoints", x) -> x
    | Variant ("JSONFull", x) -> x
    | _ -> assert false
  in
  {input with get}
