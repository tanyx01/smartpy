(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Utils
open Basics

type t = value [@@deriving show]

val equal : value -> value -> bool

val literal : Literal.t -> t

val bounded : Literal.t list -> Literal.t -> t

val int : Bigint.t -> t

val nat : Bigint.t -> t

val intOrNat : bool Hole.t -> Bigint.t -> t

val mutez : Bigint.t -> t

val int_of_value : t -> int

val bool_of_value : t -> bool

val lt : Type.t -> t -> t -> bool

val le : Type.t -> t -> t -> bool

val openV : t -> value value_f

val string : string -> t

val bytes : string -> t

val bls12_381_g1 : string -> t

val bls12_381_g2 : string -> t

val bls12_381_fr : string -> t

val chest_key : string -> t

val chest : string -> t

val chain_id : string -> t

val unString : pp:(unit -> smart_except list) -> t -> string

val unAddress : pp:(unit -> smart_except list) -> t -> string

val unKey_hash : pp:(unit -> smart_except list) -> t -> string

val un_baker : t -> string option

val build_list : t list -> t

val set : telement:Type.t -> t list -> t

val map : tkey:Type.t -> (t * t) list -> t

val closure : tlambda -> t list -> t

val unit : t

val bool : bool -> t

val unBool : pp:(unit -> smart_except list) -> t -> bool

val unMap : pp:(unit -> smart_except list) -> t -> (t * t) list

val unOption : t -> t option

val unInt : pp:(unit -> smart_except list) -> t -> Bigint.t

val unMutez : pp:(unit -> smart_except list) -> t -> Bigint.t

val unTimestamp : pp:(unit -> smart_except list) -> t -> Bigint.t

val unChain_id : pp:(unit -> smart_except list) -> t -> string

val plus : primitives:(module Primitives.Primitives) -> Type.t -> t -> t -> t

val mul : primitives:(module Primitives.Primitives) -> Type.t -> t -> t -> t

val add : primitives:(module Primitives.Primitives) -> Type.t -> t -> t -> t

val compare : Type.t -> t -> t -> int

val e_mod : Type.t -> t -> t -> t

val div_inner : t -> t -> t

val div : t -> t -> t

val minus : t -> t -> t

val key_hash : string -> t

val key : string -> t

val secret_key : string -> t

val signature : string -> t

val record : (string * t) list -> t

val variant : string -> value -> value

val tuple : t list -> t

val un_record : t -> (string * t) list

val none : t

val some : t -> t

val left : t -> t

val right : t -> t

val option : t option -> t

val ediv : Type.t -> Type.t -> t -> t -> t

val timestamp : Bigint.t -> t

val intXor : t -> t -> 'a

val sub_mutez : t -> t -> t

val address : ?entry_point:string -> string -> t

val contract : ?entry_point:string -> string -> t

val cons : t -> t -> t

val lens_list : (tvalue, tvalue list) Lens.t

val lens_list_nth : int -> (tvalue, tvalue option) Lens.t

val lens_map : (tvalue, (tvalue * tvalue) list) Lens.t

val lens_map_at : key:tvalue -> (tvalue, tvalue option) Lens.t

val lens_set : (tvalue, tvalue list) Lens.t

val lens_set_at : elem:tvalue -> (tvalue, bool) Lens.t

val lens_record : (tvalue, (string * tvalue) list) Lens.t

val lens_record_at : attr:string -> (tvalue, tvalue option) Lens.t

val zero_of_type : Type.t -> t

val nextId : string -> unit -> string

val shift_left : t -> t -> t

val shift_right : t -> t -> t

val xor : t -> t -> t

val closure_init : tlambda -> t

val closure_apply : t -> t -> t

val unclosure : pp:(unit -> smart_except list) -> t -> tlambda * t list

val extract_literals :
  config:SmartML.Config.t -> tvalue -> (Literal.t * string list) list

val get_field_opt : string -> tvalue -> tvalue option

val operation : operation -> t

val ticket : string -> t -> Bigint.t -> t

(*val error : string -> t -> value*)

val of_micheline :
     config:Config.t
  -> pp_mich:(Type.t -> Micheline.t -> string)
  -> Type.t
  -> Micheline.t
  -> t

val typecheck : Type.t -> value -> tvalue

val erase_types : tvalue -> value

val smartMLify : Type.t -> value -> value

val typecheck_instance : instance -> tinstance

module Typed : sig
  val mk : Type.t -> tvalue value_f -> tvalue

  val bool : bool -> tvalue

  val string : string -> tvalue

  val mutez : Bigint.t -> tvalue

  val address : string -> tvalue

  val tuple : tvalue list -> tvalue

  val list : Type.t -> tvalue list -> tvalue

  val record : (string * tvalue) list -> tvalue

  val variant : Type.t -> string -> tvalue -> tvalue

  val of_micheline :
       config:Config.t
    -> pp_mich:(Type.t -> micheline -> string)
    -> Type.t
    -> micheline
    -> tvalue
end
