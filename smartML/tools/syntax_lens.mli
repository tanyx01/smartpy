open SmartML
open Basics
open Typed

val entrypoints : (tcontract, tentrypoint list) Lens.t

val entrypoint : string -> (tcontract, tentrypoint) Lens.t

val entrypoint_body : (tentrypoint, tcommand) Lens.t

val views : (tcontract, tview list) Lens.t

val view : string -> (tcontract, tview) Lens.t

val view_body : (tview, tcommand) Lens.t

val seq_nth : int -> (tcommand, tcommand) Lens.t

val if_cond : (tcommand, texpr) Lens.t

val if_then : (tcommand, tcommand) Lens.t

val if_else : (tcommand, tcommand) Lens.t

val for_container : (tcommand, texpr) Lens.t

val for_body : (tcommand, tcommand) Lens.t

val arg1 : (texpr, texpr) Lens.t

val arg2 : (texpr, texpr) Lens.t

val arg3 : (texpr, texpr) Lens.t

val failwith_ : (tcommand, texpr) Lens.t
