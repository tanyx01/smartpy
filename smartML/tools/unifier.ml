(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Basics
open Printf
open Utils

exception Unification_failed of smart_except list

let fail e = raise (Unification_failed e)

let unify_holes ~unify_values wrap rsubst h1 h2 =
  let open Hole in
  match (h1, h2) with
  | Value x1, Value x2 -> unify_values x1 x2
  | Variable x1, _ ->
      Substitution.(insert [(x1, wrap h2)] rsubst);
      []
  | _, Variable x2 ->
      Substitution.(insert [(x2, wrap h1)] rsubst);
      []

(* Only checks equality of values, doesn't generate any
   constraints. *)
let equalize_holes ~err ~eq wrap rsubst h1 h2 =
  let unify_values v1 v2 = if not (eq v1 v2) then fail (err ()) else [] in
  let cs = unify_holes ~unify_values wrap rsubst h1 h2 in
  assert (cs = [])

let unify_layouts ~config ~err_msg rsubst t1 t2 l1 l2 =
  let open Substitution in
  let module Printer = (val Printer.get config : Printer.Printer) in
  let err () =
    [
      `Text "Non matching layouts"
    ; `Br
    ; `Text (Printer.layout_to_string l1)
    ; `Br
    ; `Text "and"
    ; `Br
    ; `Text (Printer.layout_to_string l2)
    ; `Br
    ; `Text err_msg
    ; `Br
    ; `Type t1
    ; `Br
    ; `Type t2
    ]
  in
  equalize_holes ~err ~eq:Layout.equal (fun x -> SLayout x) rsubst l1 l2

let unify_rows ~pp =
  let rec unify (cs, r1, r2) row1 row2 =
    match (row1, row2) with
    | (lbl1, t1) :: row1', (lbl2, t2) :: row2' -> (
        let c = compare lbl1 lbl2 in
        match c with
        | 0 -> unify (AssertEqual (t1, t2, pp) :: cs, r1, r2) row1' row2'
        | c when c < 0 -> unify (cs, (lbl1, t1) :: r1, r2) row1' row2
        | _ -> unify (cs, r1, (lbl2, t2) :: r2) row1 row2')
    | row1, row2 ->
        (cs, List.sort compare (r1 @ row1), List.sort compare (r2 @ row2))
  in
  fun row1 row2 ->
    unify ([], [], []) (List.sort compare row1) (List.sort compare row2)

let unify_boundeds =
  let rec unify (r1, r2) cases1 cases2 =
    match (cases1, cases2) with
    | lbl1 :: cases1', lbl2 :: cases2' -> (
        let c = Literal.compare lbl1 lbl2 in
        match c with
        | 0 -> unify (r1, r2) cases1' cases2'
        | c when c < 0 -> unify (lbl1 :: r1, r2) cases1' cases2
        | _ -> unify (r1, lbl2 :: r2) cases1 cases2')
    | cases1, cases2 ->
        ( List.sort Literal.compare (r1 @ cases1)
        , List.sort Literal.compare (r2 @ cases2) )
  in
  fun cases1 cases2 ->
    unify ([], [])
      (List.sort Literal.compare cases1)
      (List.sort Literal.compare cases2)

let unify ~config pp rsubst t1 t2 : typing_constraint list =
  let emit r = Substitution.(insert r rsubst) in
  match (Type.unF t1, Type.unF t2) with
  | T0 (T_nat | T_int | T_sapling_transaction _ | T_sapling_state _), _
  | _, T0 (T_nat | T_int | T_sapling_transaction _ | T_sapling_state _) ->
      assert false
  | T0 c, T0 c' when Michelson_base.Type.equal_type0 c c' -> []
  | T1 (c, t1), T1 (c', t1') when Michelson_base.Type.equal_type1 c c' ->
      [AssertEqual (t1, t1', pp)]
  | T2 (c, t1, t2), T2 (c', t1', t2') when Michelson_base.Type.equal_type2 c c'
    -> [AssertEqual (t1, t1', pp); AssertEqual (t2, t2', pp)]
  | ( TBounded {base = base1; cases = cases1; var = var1}
    , TBounded {base = base2; cases = cases2; var = var2} ) -> (
      if not (Type.equal base1 base2)
      then
        [
          AssertEqual (base1, base2, pp)
        ; AssertEqual
            ( Type.F (TBounded {base = base1; cases = cases1; var = var1})
            , Type.F (TBounded {base = base1; cases = cases2; var = var2})
            , pp )
        ]
      else
        let err () = fail [`Text "bounded mismatch"; `Type t1; `Type t2] in
        match (var1, var2, unify_boundeds cases1 cases2) with
        | None, None, ([], []) -> []
        | None, Some _, _ -> [AssertEqual (t2, t1, pp)]
        | Some i1, None, ([], r2') ->
            emit [(i1, SBounded (r2', None))];
            []
        | Some i1, Some i2, (r1', r2') ->
            let i3 = VarId.mk () in
            emit [(i1, SBounded (r2', Some i3)); (i2, SBounded (r1', Some i3))];
            []
        | _, None, (_lbl :: _, _) -> err ()
        | None, _, (_, _lbl :: _) -> err ())
  | TSecretKey, TSecretKey -> []
  | TSaplingTransaction {memo = m1}, TSaplingTransaction {memo = m2}
  | TSaplingState {memo = m1}, TSaplingState {memo = m2} ->
      let err () = [`Text "memo_size mismatch"] in
      equalize_holes ~err ~eq:( = ) (fun i -> Substitution.SInt i) rsubst m1 m2;
      []
  | TInt {isNat = isNat1}, TInt {isNat = isNat2} ->
      let err () = [`Text "Type sp.TInt / sp.TNat mismatch"] in
      equalize_holes ~err ~eq:( = )
        (fun i -> Substitution.SBool i)
        rsubst isNat1 isNat2;
      []
  | TUnknown (i1, _), TUnknown (i2, _) when i1 = i2 -> []
  | TTuple (r1, i1), TTuple (r2, i2) -> (
      let err_missing k t =
        fail
          [
            `Text (sprintf "Missing tuple component %d" k)
          ; `Text "in type"
          ; `Type t
          ]
      in
      match (i1, i2, unify_rows ~pp r1 r2) with
      | None, None, (cs, [], []) -> cs
      | None, Some _, _ -> [AssertEqual (t2, t1, pp)]
      | Some i1, None, (cs, [], r2') ->
          emit [(i1, STupleRow (r2', None))];
          cs
      | Some i1, Some i2, (cs, r1', r2') ->
          let i3 = VarId.mk () in
          emit [(i1, STupleRow (r2', Some i3)); (i2, STupleRow (r1', Some i3))];
          cs
      | _, None, (_, (lbl, _) :: _, _) -> err_missing lbl t2
      | None, _, (_, _, (lbl, _) :: _) -> err_missing lbl t1)
  | (T1 (T_set, _) | T2 ((T_map | T_big_map), _, _)), TUnknown _ ->
      [AssertEqual (t2, t1, pp)]
  | TUnknown (i, _), _ ->
      emit [(i, SType t2)];
      []
  | _, TUnknown (i, _) ->
      emit [(i, SType t1)];
      []
  | ( TVariant {row = r1; var = i1; layout = l1}
    , TVariant {row = r2; var = i2; layout = l2} )
  | ( TRecord {row = r1; var = i1; layout = l1}
    , TRecord {row = r2; var = i2; layout = l2} ) -> (
      let err_msg =
        match Type.unF t1 with
        | TVariant _ -> "Different layouts for variant types"
        | TRecord _ -> "Different layouts for record types"
        | _ -> assert false
      in
      let err_missing lbl t =
        let s =
          match Type.unF t1 with
          | TVariant _ -> "variant"
          | TRecord _ -> "field"
          | _ -> assert false
        in
        fail
          [
            `Text (sprintf "Missing %s" s)
          ; `Text (sprintf "'%s'" lbl)
          ; `Text "in type"
          ; `Type t
          ]
      in
      unify_layouts ~config ~err_msg rsubst t2 t1 l2 l1;
      match (i1, i2, unify_rows ~pp r1 r2) with
      | None, None, (cs, [], []) -> cs
      | None, Some i2, (cs, r1', []) ->
          emit [(i2, SRow (r1', None))];
          cs
      | Some i1, None, (cs, [], r2') ->
          emit [(i1, SRow (r2', None))];
          cs
      | Some i1, Some i2, (cs, r1', r2') ->
          let i3 = VarId.mk () in
          emit [(i1, SRow (r2', Some i3)); (i2, SRow (r1', Some i3))];
          cs
      | _, None, (_, (lbl, _) :: _, _) -> err_missing lbl t2
      | None, _, (_, _, (lbl, _) :: _) -> err_missing lbl t1)
  | TLambda (es1, t11, t12), TLambda (es2, t21, t22) ->
      let err () =
        fail [`Text "Lambdas have different effects"; `Type t1; `Type t2]
      in
      let with_storage_constraints =
        let unify_values ws1 ws2 =
          match (ws1, ws2) with
          | Some (a1, t1), Some (a2, t2) when Type.equal_access a1 a2 ->
              [AssertEqual (t1, t2, err)]
          | None, None -> []
          | _ -> err ()
        in
        unify_holes ~unify_values
          (fun i -> Substitution.SWithStorage i)
          rsubst es1.with_storage es2.with_storage
      in
      equalize_holes ~err ~eq:( = )
        (fun i -> Substitution.SBool i)
        rsubst es1.with_operations es2.with_operations;
      [AssertEqual (t11, t21, pp); AssertEqual (t12, t22, pp)]
      @ with_storage_constraints
  | _ -> fail [`Type t1; `Text "is not"; `Type t2]

let unify ~config pp rsubst t1 t2 =
  let cs = unify ~config pp rsubst t1 t2 in
  cs
