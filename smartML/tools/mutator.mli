open SmartML
open Basics

val mutate_loaded_scenarios :
     config:Config.t
  -> show_paths:bool
  -> (Scenario.loaded_scenario * int) list
  -> (string * tcontract * Scenario.loaded_scenario list) list
