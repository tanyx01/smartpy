(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Utils
open Control
open Basics
open Typed
open Primitives

let impossible fmt =
  let fail s = failwith ("Internal error. Please report.\n" ^ s) in
  Printf.ksprintf fail fmt

type in_contract =
  | No_contract of smart_except list
  | In_contract of contract_id * value

type context_ = {
    sender : string option
  ; source : string option
  ; chain_id : string
  ; time : Bigint.t
  ; amount : Bigint.t
  ; level : Bigint.t
  ; voting_powers : (string * Bigint.t) list
  ; line_no : line_no
  ; debug : bool
  ; contract_id : in_contract
}

type context = context_

let lambda_of_entrypoint ~config tps ({channel; body} : _ entrypoint) : tlambda
    =
  let line_no = [] in
  let ps = Expr.variable ~line_no "__parameter_storage__" in
  let ps = Expr.type_annotation ~line_no ~t:tps ps in
  let body =
    let open Command in
    let open Expr in
    seq ~line_no
      [
        (let param = open_variant ~line_no channel (first ~line_no ps) None in
         define_local ~line_no "__parameter__" param false)
      ; define_local ~line_no "__storage__" (second ~line_no ps) true
      ; (let ops =
           type_annotation ~line_no
             ~t:Type.(list operation)
             (list ~line_no ~elems:[])
         in
         define_local ~line_no "__operations__" ops true)
      ; erase_types_command body
      ; (let ops = variable ~line_no "__operations__" in
         let storage = variable ~line_no "__storage__" in
         result ~line_no (tuple ~line_no [ops; storage]))
      ]
  in
  let l =
    {
      name = "__parameter_storage__"
    ; body
    ; clean_stack = true
    ; with_storage = None
    ; with_operations = false
    ; recursive = None
    ; derived = U
    }
  in
  let l = Checker.check_lambda config [] l in
  {l with body = Closer.close_command ~config l.body}

let lazy_ep_type c =
  let {tparameter_lazy; tstorage} = get_extra c.tcontract.derived in
  match tparameter_lazy with
  | None -> impossible "lazy_ep_type"
  | Some tparameter_lazy ->
      Type.(pair tparameter_lazy tstorage, pair (list operation) tstorage)

let entrypoint_map_type c =
  let tkey = Type.nat in
  let a, b = lazy_ep_type c in
  Type.(map ~big:true ~tkey ~tvalue:(lambda no_effects a b))

let closure_of_entrypoint ~config c ep =
  let a, b = lazy_ep_type c in
  let l = lambda_of_entrypoint ~config a ep in
  Value.typecheck Type.(lambda no_effects a b) (Value.closure l [])

let build_context ?sender ?source ~amount ~(scenario_state : scenario_state)
    ~line_no ~debug () =
  {
    sender
  ; source
  ; chain_id = Option.default "" scenario_state.chain_id
  ; time = scenario_state.time
  ; amount
  ; level = scenario_state.level
  ; voting_powers = scenario_state.voting_powers
  ; line_no
  ; debug
  ; contract_id = No_contract []
  }

let context_sender c = c.sender

let context_time c = c.time

let context_amount c = c.amount

let context_contract_id c =
  match c.contract_id with
  | No_contract _ -> None
  | In_contract (id, _) -> Some id

let context_line_no c = c.line_no

let context_debug c = c.debug

(** Internal exception: raised by {!interpret_command} and caught by {!interpret_contract}. *)

type root =
  | R_storage
  | R_entrypoint_map
  | R_local of string

type step =
  | S_attr of string
  | S_item_map of tvalue
  | S_item_list of int

type path = {
    root : root
  ; steps : step list
}

let string_of_step config =
  let module Printer = (val Printer.get config : Printer.Printer) in
  function
  | S_attr attr -> "." ^ attr
  | S_item_list i -> Printf.sprintf "[%d]" i
  | S_item_map key -> Printf.sprintf "[%s]" (Printer.value_to_string key)

let _string_of_steps config steps =
  String.concat "" (List.map (string_of_step config) steps)

let extend_path {root; steps} steps' = {root; steps = steps @ steps'}

let ( @. ) = Lens.( @. )

type variable_value =
  | Iter of (tvalue * path option)
  | Heap_ref of tvalue ref
  | Constant of value

type env = {
    context : context_
  ; primitives : (module Primitives)
  ; variables : (string * variable_value) list ref
  ; steps : Execution.step list ref
  ; private_variables : (string * texpr) list
  ; current_locals : string list ref
  ; scenario_state : scenario_state
  ; config : Config.t
}

let get_current_contract_id env message : contract_id =
  match env.context.contract_id with
  | No_contract l ->
      raise
        (SmartExcept
           [`Text "Missing environment to compute"; `Text message; `Br; `Rec l])
  | In_contract (id, _) -> id

let get_current_parameters env message =
  match env.context.contract_id with
  | No_contract l ->
      raise
        (SmartExcept
           [`Text "Missing environment to compute"; `Text message; `Br; `Rec l])
  | In_contract (_, params) -> params

let get_current_instance env message : tinstance =
  let id = get_current_contract_id env message in
  match Hashtbl.find_opt env.scenario_state.contracts id with
  | None ->
      let module Printer = (val Printer.get env.config : Printer.Printer) in
      raise
        (SmartExcept
           [
             `Text
               (Printf.sprintf "Missing contract in scenario %s"
                  (Printer.string_of_contract_id id))
           ; `Line env.context.line_no
           ])
  | Some contract -> contract

let lens_current_instance env message : (unit, tinstance) Lens.t =
  let module Printer = (val Printer.get env.config : Printer.Printer) in
  let id = get_current_contract_id env message in
  Lens.hashtbl_at id env.scenario_state.contracts
  @. Lens.some
       ~err:
         (Printf.sprintf "Missing contract in scenario %s"
            (Printer.string_of_contract_id id))

let lens_heap_ref =
  Lens.bi
    (function
      | Heap_ref x -> x
      | _ -> impossible "not a heap reference")
    (fun x -> Heap_ref x)

let lens_of_root env =
  let l_state (x : tinstance) =
    Lens.{focus = x.state; zip = (fun state -> {x with state})}
  in
  let l_storage (x : tvalue contract_state_f) =
    Lens.{focus = x.storage; zip = (fun storage -> {x with storage})}
  in
  let l_entrypoint_map (x : tvalue contract_state_f) =
    Lens.
      {
        focus = x.entrypoint_map
      ; zip = (fun entrypoint_map -> {x with entrypoint_map})
      }
  in

  function
  | R_storage ->
      lens_current_instance env "variable"
      @. Lens.make l_state @. Lens.make l_storage
      @. Lens.some ~err:"no storage"
  | R_entrypoint_map ->
      lens_current_instance env "variable"
      @. Lens.make l_state @. Lens.make l_entrypoint_map
      @. Lens.some ~err:"no lazy entrypoints"
  | R_local name ->
      Lens.ref env.variables
      @. Lens.assoc_exn ~equal:( = ) ~key:name ~err:("local not found: " ^ name)
      @. lens_heap_ref @. Lens.unref

(** Convert a step into a lens that points to an optional value,
   accompanied by an error message for when the value is missing. *)
let lens_of_step ~config ~line_no =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let line_no = string_of_line_no line_no in
  function
  | S_item_list i ->
      ( Value.lens_list_nth i
      , Printf.sprintf "Line %s: Index '%d' out of range." line_no i )
  | S_item_map key ->
      ( Value.lens_map_at ~key
      , Printf.sprintf "Line %s: Key '%s' not found." line_no
          (Printer.value_to_string key) )
  | S_attr attr ->
      ( Value.lens_record_at ~attr
      , Printf.sprintf "Line %s: Impossible: attribute not found" line_no )

let rec lens_of_steps ~config ~line_no acc = function
  | [s] ->
      let l, err = lens_of_step ~config ~line_no s in
      (acc @. l, err)
  | s :: steps ->
      let l, err = lens_of_step ~config ~line_no s in
      lens_of_steps ~config ~line_no (acc @. l @. Lens.some ~err) steps
  | [] -> (Lens.option ~err:"lens_of_steps", "lens_of_steps")

let lens_of_path ~config ~line_no env {root; steps} =
  let l, err = lens_of_steps ~config ~line_no Lens.id steps in
  (lens_of_root env root @. l, err)

let add_step ?(sub_steps = ref []) upperSteps env c elements =
  let build_step () =
    let instance = get_current_instance env "not in contract" in
    let storage = Option.of_some ~msg:"no storage" instance.state.storage in
    let balance = instance.state.balance in
    {
      Execution.iters =
        List.filter_map
          (function
            | n, Iter (v, _) -> Some (n, (v, None))
            | _ -> None)
          !(env.variables)
    ; locals =
        List.filter_map
          (function
            | n, Heap_ref x -> Some (n, !x)
            | _ -> None)
          !(env.variables)
    ; storage
    ; balance
    ; operations = [] (*env.operations*)
    ; command = c
    ; substeps = sub_steps
    ; elements
    }
  in
  if env.context.debug then upperSteps := build_step () :: !upperSteps

let add_variable name value vars =
  match List.assoc_opt name vars with
  | Some _ -> impossible "Variable %S already defined" name
  | None -> (name, value) :: vars

let assert_unit c = function
  | {v = Literal Unit} -> ()
  | _ -> impossible "Command doesn't return unit" (show_tcommand c)

let mk_key_value ~layout k v =
  Value.Typed.mk
    (Type.record (Hole.value layout) [("key", k.t); ("value", v.t)])
    (Record [("key", k); ("value", v)])

let get_constant_value env hash =
  match Hashtbl.find_opt env.scenario_state.constants hash with
  | Some value -> value
  | None ->
      raise
        (SmartExcept
           [`Text "Could not find any constant with hash:"; `Text hash])

let update_contract_address ?address scenario_state id =
  let address =
    match address with
    | None -> Aux.address_of_contract_id ~html:false id None
    | Some address -> address
  in
  Hashtbl.replace scenario_state.addresses id address;
  Hashtbl.replace scenario_state.rev_addresses address id;
  address

let get_contract_address scenario_state id =
  match Hashtbl.find_opt scenario_state.addresses id with
  | Some addr -> addr
  | None -> update_contract_address scenario_state id

let update_contract_address ?address scenario_state id =
  let (_ : string) = update_contract_address ?address scenario_state id in
  ()

let update_contract ?address scenario_state id contract =
  Hashtbl.replace scenario_state.contracts id contract;
  update_contract_address ?address scenario_state id

let un_int err = function
  | {v = Literal (Int {i})} -> i
  | _ -> err ()

let un_bool err = function
  | {v = Literal (Bool x)} -> x
  | _ -> err ()

let un_tuple err = function
  | {v = Tuple vs} -> vs
  | _ -> err ()

let un_operation = function
  | {tv = Operation op} -> op
  | _ -> impossible "not an operation"

let interpret_mprim0 env e0 : _ Michelson_base.Primitive.prim0 -> _ = function
  | Sender -> (
      match env.context.sender with
      | Some address -> Value.literal (Literal.address address)
      | None -> raise (SmartExcept [`Text "Sender is undefined"; `Expr e0]))
  | Source -> (
      match env.context.source with
      | Some address -> Value.literal (Literal.address address)
      | None -> raise (SmartExcept [`Text "Source is undefined"; `Expr e0]))
  | Amount -> Value.mutez env.context.amount
  | Balance -> Value.mutez (get_current_instance env "balance").state.balance
  | Level -> Value.nat env.context.level
  | Now -> Value.timestamp env.context.time
  | Self None ->
      let id = get_current_contract_id env "Self" in
      Value.contract (get_contract_address env.scenario_state id)
  | Self (Some entry_point) ->
      let id = get_current_contract_id env "Self_entry_point" in
      Value.contract ~entry_point (get_contract_address env.scenario_state id)
  | Self_address ->
      let id = get_current_contract_id env "Self_address" in
      Value.literal
        (Literal.address (get_contract_address env.scenario_state id))
  | Chain_id -> Value.chain_id env.context.chain_id
  | Total_voting_power ->
      Value.nat
        (List.fold_left
           (fun acc (_, v) -> Big_int.add_big_int acc v)
           Big_int.zero_big_int env.context.voting_powers)
  | Sapling_empty_state {memo} ->
      Value.literal (Literal.sapling_test_state memo [])
  | Unit_ -> Value.literal Literal.unit
  | None_ _ -> Value.none
  | Nil _ -> Value.build_list []
  | Empty_set telement ->
      let telement = Type.of_mtype telement in
      Value.set ~telement []
  | Empty_map (tkey, _) ->
      let tkey = Type.of_mtype tkey in
      Value.map ~tkey []
  | Empty_bigmap (tkey, _) ->
      let tkey = Type.of_mtype tkey in
      Value.map ~tkey []

let interpret_mprim1 env p x =
  let primitives = env.primitives in
  let module P = (val primitives : Primitives) in
  let impossible () =
    impossible "interpret_mprim1 %s %s"
      Michelson_base.(Primitive.show_prim1 Type.pp_mtype p)
      (show_value x)
  in
  let un_int = un_int impossible in
  let un_bool = un_bool impossible in
  let hash_algo h x =
    match x.v with
    | Literal (Bytes b) -> Value.bytes (h b)
    | _ -> impossible ()
  in
  match p with
  | Abs -> Value.nat (Big_int.abs_big_int (un_int x))
  | IsNat ->
      let x = un_int x in
      if 0 <= Big_int.compare_big_int x Big_int.zero_big_int
      then Value.some (Value.nat x)
      else Value.none
  | Not -> Value.bool (not (un_bool x))
  | Hash_key ->
      let public_key =
        match x.v with
        | Literal (Key k) -> k
        | _ -> impossible ()
      in
      Value.key_hash (P.Crypto.hash_key public_key)
  | Blake2b -> hash_algo Utils.Crypto.blake2b x
  | Sha256 -> hash_algo Utils.Crypto.sha256 x
  | Sha512 -> hash_algo Utils.Crypto.sha512 x
  | Keccak -> hash_algo Utils.Crypto.keccak x
  | Sha3 -> hash_algo Utils.Crypto.sha3 x
  | _ -> impossible ()

let interpret_mprim2 ~primitives p (x1, tx1) (x2, _tx2) =
  let impossible () =
    impossible "interpret_mprim2 %s %s %s"
      Michelson_base.(Primitive.show_prim2 Type.pp_mtype p)
      (show_value x1) (show_value x2)
  in
  match (p : _ Michelson_base.Primitive.prim2) with
  | Lsl -> Value.shift_left x1 x2
  | Lsr -> Value.shift_right x1 x2
  | Sub_mutez -> Value.sub_mutez x1 x2
  | Mul -> Value.mul ~primitives tx1 x1 x2
  | Add -> Value.add ~primitives tx1 x1 x2
  | Compare -> Value.int (Big_int.big_int_of_int (Value.compare tx1 x1 x2))
  | _ -> impossible ()

let interpret_mprim3 env p x1 x2 x3 =
  let primitives = env.primitives in
  let module P = (val primitives : Primitives) in
  let impossible () =
    impossible "interpret_mprim3 %s %s %s %s"
      Michelson_base.(Primitive.show_prim3 p)
      (show_value x1) (show_value x2) (show_value x3)
  in
  match (p : Michelson_base.Primitive.prim3) with
  | Check_signature ->
      let pk_expr, sig_expr, msg_expr = (x1, x2, x3) in
      Misc.Dbg.on := false;
      let public_key =
        match pk_expr.v with
        | Literal (Key k) -> k
        | _ -> impossible ()
      in
      let signature =
        match sig_expr.v with
        | Literal (Signature s) -> s
        | _ -> impossible ()
      in
      let msg =
        match msg_expr.v with
        | Literal (Bytes b) -> b
        | _ -> impossible ()
      in
      Value.bool (P.Crypto.check_signature ~public_key ~signature msg)
  | Open_chest -> (
      let chest_key, chest, time = (x1, x2, x3) in
      let chest_key =
        match chest_key.v with
        | Literal (Chest_key b) -> b
        | _ -> impossible ()
      in
      let chest =
        chest.v |> function
        | Literal (Chest b) -> b
        | _ -> impossible ()
      in
      let time =
        time.v |> function
        | Literal (Int i) -> Bigint.int_of_big_int i.i
        | _ -> impossible ()
      in
      let result =
        P.Timelock.open_chest
          Hex.(show (of_string chest))
          Hex.(show (of_string chest_key))
          time
      in
      match result with
      | Correct x -> Value.(variant "Left" (bytes (Hex.to_string (`Hex x))))
      | Bogus_cipher -> Value.(variant "Right" (bool false))
      | Bogus_opening -> Value.(variant "Right" (bool true)))
  | _ -> impossible ()

let get_lazy_ix instance ep_name =
  let f (ep : _ entrypoint) = ep.channel = ep_name in
  match List.find_opt f instance.template.tcontract.entrypoints with
  | None -> impossible "get_lazy_ix"
  | Some ep -> (
      match (get_extra ep.derived).lazy_index with
      | None -> impossible "get_lazy_ix"
      | Some i -> Value.nat (Bigint.of_int i))

let interpret_prim0 ~config env e0 p =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let primitives = env.primitives in
  let module P = (val primitives : Primitives) in
  let impossible () = impossible "interpret_prim0 %s" (show_prim0 Type.pp p) in
  match p with
  | ELiteral x -> Value.literal x
  | EBounded x ->
      let cases =
        match Type.unF e0.et with
        | TBounded {cases} -> cases
        | _ -> impossible ()
      in
      Value.bounded cases x
  | ECst_contract {address; entry_point} -> Value.contract ?entry_point address
  | EConstant_var x -> (
      match Hashtbl.find_opt env.scenario_state.variables x with
      | Some {tv = Literal (String s)} -> get_constant_value env s
      | _ -> impossible ())
  | EConstant (hash, _) -> get_constant_value env hash
  | EMatch_cons name -> (
      match List.assoc_opt name !(env.variables) with
      | Some (Constant v) -> v
      | _ -> impossible ())
  | EMeta_local name -> (
      match List.assoc_opt name !(env.variables) with
      | Some (Heap_ref {contents = x}) -> Value.erase_types x
      | _ -> impossible ())
  | EContract_address (id, entry_point) ->
      Value.address ?entry_point (get_contract_address env.scenario_state id)
  | EContract_balance id -> (
      match Hashtbl.find_opt env.scenario_state.contracts id with
      | Some t -> Value.mutez t.state.balance
      | None ->
          raise
            (SmartExcept
               [
                 `Text "Missing contract balance for id: "
               ; `Text (Printer.string_of_contract_id id)
               ; `Br
               ; `Line e0.line_no
               ]))
  | EContract_baker id -> (
      match Hashtbl.find_opt env.scenario_state.contracts id with
      | Some {state = {baker = Some baker}} -> Value.some (Value.key_hash baker)
      | Some {state = {baker = None}} -> Value.none
      | None ->
          raise
            (SmartExcept
               [
                 `Text "Missing contract for id: "
               ; `Text (Printer.string_of_contract_id id)
               ; `Br
               ; `Line e0.line_no
               ]))
  | EContract_data id -> (
      match Hashtbl.find_opt env.scenario_state.contracts id with
      | Some t -> (
          match t.state.storage with
          | Some storage -> Value.erase_types storage
          | None ->
              raise
                (SmartExcept
                   [
                     `Text "Missing contract storage for id: "
                   ; `Text (Printer.string_of_contract_id id)
                   ; `Br
                   ; `Line e0.line_no
                   ]))
      | None ->
          raise
            (SmartExcept
               [
                 `Text "Missing contract for id: "
               ; `Text (Printer.string_of_contract_id id)
               ; `Br
               ; `Line e0.line_no
               ]))
  | EContract_entrypoint_map cid -> (
      match Hashtbl.find_opt env.scenario_state.contracts cid with
      | Some c -> (
          match c.state.entrypoint_map with
          | Some leps -> Value.erase_types leps
          | _ -> impossible ())
      | None ->
          raise
            (SmartExcept
               [
                 `Text "Missing contract for id: "
               ; `Text (Printer.string_of_contract_id cid)
               ; `Br
               ; `Line e0.line_no
               ]))
  | EContract_entrypoint_id (cid, ep_name) -> (
      match Hashtbl.find_opt env.scenario_state.contracts cid with
      | Some c -> get_lazy_ix c ep_name
      | None ->
          raise
            (SmartExcept
               [
                 `Text "Missing contract for id: "
               ; `Text (Printer.string_of_contract_id cid)
               ; `Br
               ; `Line e0.line_no
               ]))
  | EContract_typed (id, entry_point) ->
      Value.contract ?entry_point (get_contract_address env.scenario_state id)
  | EAccount_of_seed {seed} ->
      let account = P.Crypto.account_of_seed seed in
      let seed = Value.string seed in
      let pkh = Value.key_hash account.pkh in
      let address = Value.address account.pkh in
      let pk = Value.key account.pk in
      let sk = Value.secret_key account.sk in
      Value.record
        [
          ("seed", seed)
        ; ("address", address)
        ; ("public_key", pk)
        ; ("public_key_hash", pkh)
        ; ("secret_key", sk)
        ]
  | EEntrypoint_map -> (
      let instance = get_current_instance env "entrypoint map" in
      match instance.state.entrypoint_map with
      | Some leps -> Value.erase_types leps
      | None -> assert false)
  | EEntrypoint_id ep_name ->
      get_lazy_ix (get_current_instance env "entrypoint id") ep_name

let interpret_prim1 ~config env e0 p (x, tx) =
  let protocol = config.Config.protocol in
  let scenario_vars = String.Map.of_hashtbl env.scenario_state.variables in
  let primitives = env.primitives in
  let module P = (val primitives : Primitives) in
  let impossible () =
    impossible "interpret_prim1 %s %s" (show_prim1 Type.pp p) (show_value x)
  in
  let un_int = un_int impossible in
  match p with
  | ETo_int -> (
      match x.v with
      | Literal (Bls12_381_fr hex) ->
          Value.int
            (Bigint.of_string ~msg:"interpreter"
               (try P.Bls12.convertFrToInt Hex.(show (of_string hex))
                with _ -> impossible ()))
      | Literal (Int {i}) -> Value.int i
      | _ -> impossible ())
  | ENeg -> (
      match x.v with
      | Literal (Bls12_381_g1 hex) ->
          Value.bls12_381_g1
            (Hex.to_string
               (`Hex
                 (try P.Bls12.negateG1 Hex.(show (of_string hex))
                  with _ -> impossible ())))
      | Literal (Bls12_381_g2 hex) ->
          Value.bls12_381_g2
            (Hex.to_string
               (`Hex
                 (try P.Bls12.negateG2 Hex.(show (of_string hex))
                  with _ -> impossible ())))
      | Literal (Bls12_381_fr hex) ->
          Value.bls12_381_fr
            (Hex.to_string
               (`Hex
                 (try P.Bls12.negateFr Hex.(show (of_string hex))
                  with _ -> impossible ())))
      | Literal (Int {i}) -> Value.int (Big_int.minus_big_int i)
      | _ -> impossible ())
  | ESign ->
      let x = un_int x in
      let result = Big_int.compare_big_int x Big_int.zero_big_int in
      Value.int (Bigint.of_int result)
  | ESum -> (
      let t =
        match Type.unF tx with
        | T1 (T_list, t) -> t
        | _ -> impossible ()
      in
      match x.v with
      | List l | Set l ->
          List.fold_left
            (fun x y -> Value.plus ~primitives t x y)
            (Value.zero_of_type e0.et) l
      | Map l ->
          List.fold_left
            (fun x (_, y) -> Value.plus ~primitives t x y)
            (Value.zero_of_type e0.et) l
      | _ -> impossible ())
  | EList_rev -> (
      match x.v with
      | Basics.List l -> Value.build_list (List.rev l)
      | _ -> impossible ())
  | EList_items rev -> (
      match x.v with
      | Basics.Map l ->
          Value.build_list
            ((if rev then List.rev_map else List.map)
               (fun (k, v) -> Value.record [("key", k); ("value", v)])
               l)
      | _ -> impossible ())
  | EList_keys rev -> (
      match x.v with
      | Basics.Map l ->
          Value.build_list ((if rev then List.rev_map else List.map) fst l)
      | _ -> impossible ())
  | EList_values rev -> (
      match x.v with
      | Basics.Map l ->
          Value.build_list ((if rev then List.rev_map else List.map) snd l)
      | _ -> impossible ())
  | EList_elements rev -> (
      match x.v with
      | Basics.Set l -> Value.build_list (if rev then List.rev l else l)
      | _ -> impossible ())
  | EPack -> (
      try
        x |> Value.typecheck tx
        |> Compiler.pack_value ~config ~scenario_vars
        |> Value.bytes
      with Data_encoding.Binary.Write_error write_error ->
        raise
          (ExecFailure
             ( Value.Typed.string "Pack error"
             , [
                 `Text "Could not pack bytes due to invalid content"
               ; `Br
               ; `Expr e0
               ; `Text
                   (Format.asprintf "%a" Data_encoding.Binary.pp_write_error
                      write_error)
               ; `Line e0.line_no
               ] )))
  | EUnpack t -> (
      match x.v with
      | Literal (Bytes b) -> (
          try Compiler.unpack_value ~config t b |> Value.some
          with Data_encoding.Binary.Read_error read_error ->
            raise
              (ExecFailure
                 ( Value.Typed.string "Unpack error"
                 , [
                     `Text
                       (Format.asprintf
                          "Could not unpack bytes (%s) due to invalid content: \
                           %a."
                          Hex.(show (of_string b))
                          Data_encoding.Binary.pp_read_error read_error)
                   ; `Br
                   ; `Expr e0
                   ; `Line e0.line_no
                   ] )))
      | _ -> impossible ())
  | EConcat_list -> (
      let vals =
        match x.v with
        | List sl ->
            List.map
              (fun sb ->
                match sb.v with
                | Literal (String s) | Literal (Bytes s) -> s
                | _ -> impossible ())
              sl
        | _ -> impossible ()
      in
      match Type.unF e0.et with
      | T0 T_string -> Value.string (String.concat "" vals)
      | T0 T_bytes -> Value.bytes (String.concat "" vals)
      | _ -> impossible ())
  | ESize -> (
      let result length = Value.nat (Bigint.of_int length) in
      match x.v with
      | Literal (String s) | Literal (Bytes s) -> result (String.length s)
      | List l | Set l -> result (List.length l)
      | Map l -> result (List.length l)
      | _ -> impossible ())
  | EResolve -> x
  | EProject i ->
      let vs = un_tuple impossible x in
      if i < List.length vs then List.nth vs i else impossible ()
  | EAddress -> (
      match x.v with
      | Contract {address; entrypoint = entry_point} ->
          Value.literal (Literal.address ?entry_point address)
      | _ -> impossible ())
  | EImplicit_account -> (
      match x.v with
      | Literal (Key_hash key_hash) -> Value.contract key_hash
      | _ -> impossible ())
  | ESet_delegate ->
      let baker = Value.un_baker x in
      (* The delegate must be registered *)
      (match baker with
      | None -> ()
      | Some key_hash ->
          let registered =
            List.exists (fun (k, _) -> k = key_hash) env.context.voting_powers
          in
          if not registered
          then failwith (Printf.sprintf "Unregistered delegate (%s)" key_hash));
      Value.operation (SetDelegate baker)
  | EType_annotation _ -> x
  | EAttr name -> (
      match x.v with
      | Record l -> (
          match List.assoc_opt name l with
          | Some v -> v
          | None -> impossible ())
      | _ -> impossible ())
  | EIs_variant name -> (
      match x.v with
      | Variant (cons, _) -> Value.bool (cons = name)
      | _ -> impossible ())
  | EVariant name -> Value.variant name x
  | ERead_ticket -> (
      match x.v with
      | Ticket (address, content, amount) ->
          let address = Value.literal (Literal.address address) in
          let amount = Value.nat amount in
          Value.tuple [Value.tuple [address; content; amount]; x]
      | _ -> impossible ())
  | EJoin_tickets -> (
      match x.v with
      | Tuple
          [
            {v = Ticket (ticketer1, content1, amount1)}
          ; {v = Ticket (ticketer2, content2, amount2)}
          ] ->
          if ticketer1 = ticketer2 && Value.equal content1 content2
          then
            Value.some
              (Value.ticket ticketer1 content1
                 (Bigint.add_big_int amount1 amount2))
          else impossible ()
      | _ -> impossible ())
  | EPairing_check ->
      let vals =
        match x.v with
        | List sl ->
            Base.List.map sl ~f:(fun sb ->
                match sb.v with
                | Tuple
                    [
                      {v = Literal (Bls12_381_g1 i1)}
                    ; {v = Literal (Bls12_381_g2 i2)}
                    ] -> (Hex.(show (of_string i1)), Hex.(show (of_string i2)))
                | _ -> impossible ())
        | _ -> impossible ()
      in
      Value.bool (try P.Bls12.pairingCheck vals with _ -> impossible ())
  | EVoting_power ->
      let key_hash =
        match x.v with
        | Literal (Key_hash key_hash) -> key_hash
        | _ -> impossible ()
      in
      let res =
        List.find_opt
          (fun (k, _) ->
            Value.equal (Value.key_hash k) (Value.key_hash key_hash))
          env.context.voting_powers
      in
      Value.nat
        (match res with
        | Some (_, v) -> v
        | None -> Big_int.zero_big_int)
  | EUnbounded -> (
      match x.v with
      | Bounded (_, l) -> Value.literal l
      | _ -> impossible ())
  | EConvert ->
      let open Michelson_interpreter in
      let x = mvalue_of_value ~protocol tx x in
      value_of_mvalue config e0.et x
  | EStatic_view _ -> impossible ()
  | EEmit (tag, _with_type) -> Value.operation (Event (tag, tx, x))

let interpret_prim2 env (_e : texpr) p (x1, tx1) (x2, tx2) =
  let primitives = env.primitives in
  let module P = (val primitives : Primitives) in
  let impossible () =
    impossible "interpret_prim2 %s %s %s" (show_prim2 Type.pp p) (show_value x1)
      (show_value x2)
  in
  let un_int = un_int impossible in
  match p with
  | EGet_opt -> (
      match x2.v with
      | Map m -> (
          match List.assoc_opt ~equal:Value.equal x1 m with
          | Some v -> Value.some v
          | None -> Value.none)
      | _ -> impossible ())
  | EEq -> Value.bool (Value.equal x1 x2)
  | ENeq -> Value.bool (not (Value.equal x1 x2))
  | EAdd -> Value.plus ~primitives tx1 x1 x2
  | EMul_homo -> Value.mul ~primitives tx1 x1 x2
  | EMod -> Value.e_mod tx1 x1 x2
  | EDiv -> Value.div x1 x2
  | ESub -> Value.minus x1 x2
  | ELt -> Value.bool (Value.lt tx1 x1 x2)
  | ELe -> Value.bool (Value.le tx1 x1 x2)
  | EGt -> Value.bool (Value.lt tx1 x2 x1)
  | EGe -> Value.bool (Value.le tx1 x2 x1)
  | EAnd | EOr -> impossible ()
  | EXor -> Value.xor x1 x2
  | EEDiv -> Value.ediv tx1 tx2 x1 x2
  | ECons -> Value.cons x1 x2
  | EMax ->
      let isNat =
        match Type.unF tx1 with
        | TInt {isNat} -> isNat
        | _ -> impossible ()
      in
      Value.intOrNat isNat (Big_int.max_big_int (un_int x1) (un_int x2))
  | EMin ->
      let isNat =
        match Type.unF tx1 with
        | TInt {isNat} -> isNat
        | _ -> impossible ()
      in
      Value.intOrNat isNat (Big_int.min_big_int (un_int x1) (un_int x2))
  | EAdd_seconds -> (
      match (x1.v, x2.v) with
      | Literal (Timestamp t), Literal (Int {i = s}) ->
          Value.timestamp (Big_int.add_big_int t s)
      | _ -> impossible ())
  | EContains -> (
      match x2.v with
      | Map l -> Value.bool (List.exists (fun (x, _) -> Value.equal x x1) l)
      | Set l -> Value.bool (List.exists (fun x -> Value.equal x x1) l)
      | _ -> impossible ())
  | EApply_lambda -> Value.closure_apply x2 x1
  | ETicket -> (
      let ticketer =
        let id = get_current_contract_id env "ETicket" in
        get_contract_address env.scenario_state id
      in
      match x2.v with
      | Literal (Int {i}) -> Value.ticket ticketer x1 i
      | _ -> impossible ())
  | ESplit_ticket -> (
      match (x1.v, x2.v) with
      | ( Ticket (ticketer, content, amount)
        , Tuple [{v = Literal (Int {i = a1})}; {v = Literal (Int {i = a2})}] )
        ->
          if Bigint.equal (Bigint.add_big_int a1 a2) amount
          then
            let t1 = Value.ticket ticketer content a1 in
            let t2 = Value.ticket ticketer content a2 in
            Value.some (Value.tuple [t1; t2])
          else Value.none
      | _ -> impossible ())
  | ECall_lambda -> impossible ()
  | EView _ -> impossible ()

let interpret_prim3 env p (x1, tx1) (x2, _tx2) (x3, tx3) =
  let primitives = env.primitives in
  let module P = (val primitives : Primitives) in
  let impossible () =
    impossible "interpret_prim3 %s %s %s %s" (show_prim3 p) (show_value x1)
      (show_value x2) (show_value x3)
  in
  match p with
  | ESplit_tokens -> (
      match (x1.v, x2.v, x3.v) with
      | Literal (Mutez x1), Literal (Int {i = x2}), Literal (Int {i = x3}) ->
          Value.mutez (Big_int.div_big_int (Big_int.mult_big_int x1 x2) x3)
      | _ -> impossible ())
  | ERange -> (
      match (x1.v, x2.v, x3.v) with
      | Literal (Int {i = a}), Literal (Int {i = b}), Literal (Int {i = step})
        ->
          let a = Big_int.int_of_big_int a in
          let b = Big_int.int_of_big_int b in
          let step = Big_int.int_of_big_int step in
          if step = 0
          then failwith (Printf.sprintf "Range with 0 step")
          else if step * (b - a) < 0
          then Value.build_list []
          else
            let isNat =
              match Type.unF tx3 with
              | TInt {isNat} -> isNat
              | _ -> impossible ()
            in
            let rec aux a acc =
              if (b - a) * step <= 0
              then List.rev acc
              else aux (a + step) (Value.intOrNat isNat (Bigint.of_int a) :: acc)
            in
            Value.build_list (aux a [])
      | _ -> impossible ())
  | EUpdate_map -> (
      match (Type.unF tx3, x3.v) with
      | T2 ((T_map | T_big_map), tkey, _), Map xs ->
          let value = Value.unOption x2 in
          let m' = Lens.set (Lens.assoc ~equal:Value.equal ~key:x1) value xs in
          Value.map ~tkey m'
      | _ -> impossible ())
  | EGet_and_update -> (
      match (Type.unF tx3, x3.v) with
      | T2 ((T_map | T_big_map), _, _), Map xs ->
          let value' = Value.unOption x2 in
          let old, m' =
            Lens.get_and_set (Lens.assoc ~equal:Value.equal ~key:x1) value' xs
          in
          let map = Value.map ~tkey:tx1 m' in
          Value.tuple [Value.option old; map]
      | _ -> impossible ())
  | ETest_ticket -> (
      match (x1.v, x3.v) with
      | Literal (Address {address}), Literal (Int {i}) ->
          Value.ticket address x2 i
      | _ -> impossible ())

let rec interpret_expr ~config upper_steps env : texpr -> value =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let protocol = config.protocol in
  let rec interp e =
    let primitives = env.primitives in
    let module P = (val primitives : Primitives) in
    let impossible () = impossible "interpret_expr %S" (show_texpr e) in
    let un_bool = un_bool impossible in
    match e.e with
    | EVar ("__parameter__", _) -> get_current_parameters env "__parameter__"
    | EVar ("__storage__", _) ->
        let instance = get_current_instance env "__storage__" in
        Value.erase_types
          (Option.of_some ~msg:"no storage" instance.state.storage)
    | EVar (id, Scenario) -> (
        match Hashtbl.find_opt env.scenario_state.variables id with
        | Some x -> Value.erase_types x
        | None ->
            raise
              (SmartExcept
                 [
                   `Text "Missing scenario variable for id: "
                 ; `Text id
                 ; `Br
                 ; `Line e.line_no
                 ]))
    | EVar (name, _) -> (
        match List.assoc_opt name !(env.variables) with
        | Some (Heap_ref {contents = x}) -> Value.erase_types x
        | Some (Iter (v, _)) -> Value.erase_types v
        | Some (Constant v) -> v
        | None -> impossible ())
    | EPrivate id -> (
        match List.assoc_opt id env.private_variables with
        | Some e -> interp e
        | None -> impossible ())
    | EMPrim1 (Emit (tag, _ty), x) ->
        let x_t = x.et in
        let x = interp x in
        Value.operation (Event (tag, x_t, x))
    | EMPrim0 p -> interpret_mprim0 env e p
    | EMPrim1 (p, x) ->
        let x = interp x in
        interpret_mprim1 env p x
    | EMPrim1_fail _ -> impossible ()
    | EMPrim2 (p, x1, x2) ->
        let tx1 = x1.et in
        let tx2 = x2.et in
        let x2 = interp x2 in
        let x1 = interp x1 in
        interpret_mprim2 ~primitives p (x1, tx1) (x2, tx2)
    | EMPrim3 (p, x1, x2, x3) ->
        let x3 = interp x3 in
        let x2 = interp x2 in
        let x1 = interp x1 in
        interpret_mprim3 env p x1 x2 x3
    | EPrim0 p -> interpret_prim0 ~config env e p
    | EIs_failing x -> (
        try
          let (_ : value) = interp x in
          Value.bool false
        with _ -> Value.bool true)
    | ECatch_exception (_, x) -> (
        try
          let (_ : value) = interp x in
          Value.none
        with
        | Failure f -> Value.some (Value.string f)
        | ExecFailure (value, _) -> Value.some (Value.erase_types value)
        | SmartExcept _ -> Value.some (Value.string "Error")
        | _ -> impossible ())
    | EPrim1 (EStatic_view (static_id, name), param) ->
        let param_t = param.et in
        let param = interp param in
        let id = C_static static_id in
        let return_type =
          match Type.unF e.et with
          | T1 (T_option, t) -> t
          | _ -> impossible ()
        in
        eval_view ~name ~id ~config ~line_no:e.line_no ~e ~return_type
          ~types_ok:true env (param, param_t)
    | EPrim1 (p, x) ->
        let tx = x.et in
        let x = interp x in
        interpret_prim1 ~config env e p (x, tx)
    | EPrim2 (EOr, x, y) -> (
        let pp () = [] in
        match Type.unF x.et with
        | TInt _ ->
            Value.nat
              (Big_int.or_big_int
                 (Value.unInt ~pp (interp x))
                 (Value.unInt ~pp (interp y)))
        | T0 T_bool ->
            Value.bool
              (Value.unBool ~pp (interp x) || Value.unBool ~pp (interp y))
        | _ ->
            (* This should never happen (ensured by checker.ml) *) impossible ()
        )
    | EPrim2 (EAnd, x, y) -> (
        let pp () = [] in
        match Type.unF x.et with
        | TInt _ ->
            Value.nat
              (Big_int.and_big_int
                 (Value.unInt ~pp (interp x))
                 (Value.unInt ~pp (interp y)))
        | T0 T_bool ->
            Value.bool
              (Value.unBool ~pp (interp x) && Value.unBool ~pp (interp y))
        | _ ->
            (* This should never happen (ensured by checker.ml) *) impossible ()
        )
    | EPrim2 (ECall_lambda, parameter, lambda) -> (
        let lambda : tvalue =
          interpret_expr_and_check ~config upper_steps env lambda
        in
        let parameter : tvalue =
          interpret_expr_and_check ~config upper_steps env parameter
        in
        match lambda.tv with
        | Closure (l, args) -> (
            match l.recursive with
            | None ->
                Value.erase_types
                  (call_closure ~config upper_steps env (l, args) parameter)
            | Some fname ->
                Value.erase_types
                  (call_rec_closure ~config upper_steps env (l, args) parameter
                     (fname, lambda)))
        | _ -> impossible ())
    | EPrim2 (EView (name, return_type), param, y) -> (
        let param_t = param.et in
        let param = interp param in
        let y = interp y in
        let address =
          match y with
          | {v = Literal (Address {address})} -> address
          | _ -> raise (SmartExcept [`Text "View address is invalid"])
        in
        match Hashtbl.find_opt env.scenario_state.rev_addresses address with
        | None ->
            if config.view_check_exception
            then
              raise
                (ExecFailure
                   ( Value.Typed.string "Missing contract for view"
                   , [`Expr e; `Line e.line_no] ))
            else Value.none
        | Some id ->
            eval_view ~name ~id ~config ~line_no:e.line_no ~e ~return_type
              ~types_ok:false env (param, param_t))
    | EPrim2 (p, x1, x2) ->
        let tx1 = x1.et in
        let tx2 = x2.et in
        let x2 = interp x2 in
        let x1 = interp x1 in
        interpret_prim2 env e p (x1, tx1) (x2, tx2)
    | EIf (c, t, e) -> if un_bool (interp c) then interp t else interp e
    | EPrim3 (p, x1, x2, x3) ->
        let tx1 = x1.et in
        let tx2 = x2.et in
        let tx3 = x3.et in
        let x3 = interp x3 in
        let x2 = interp x2 in
        let x1 = interp x1 in
        interpret_prim3 env p (x1, tx1) (x2, tx2) (x3, tx3)
    | EOpen_variant (name, x, missing_message) -> (
        let x = interp x in
        match x.v with
        | Variant (cons, v) ->
            if cons <> name
            then
              let default_message =
                Printf.sprintf "Not the proper variant constructor [%s] != [%s]"
                  name cons
              in
              let missing_message, message =
                match missing_message with
                | Some e ->
                    let v =
                      interpret_expr_and_check ~config upper_steps env e
                    in
                    (v, [`Br; `Text "Message:"; `Value v])
                | None -> (Value.Typed.string default_message, [])
              in
              raise
                (ExecFailure
                   ( missing_message
                   , [
                       `Text default_message
                     ; `Br
                     ; `Expr e
                     ; `Rec message
                     ; `Line e.line_no
                     ] ))
            else v
        | _ -> impossible ())
    | EItem {items; key; default_value; missing_message} -> (
        let pp () =
          Printf.sprintf "%s[%s]"
            (Printer.texpr_to_string items)
            (Printer.texpr_to_string key)
        in
        let items' = interp items in
        let key' = interp key in
        let def =
          match default_value with
          | None -> None
          | Some def -> Some (lazy (interp def))
        in
        let missing_message =
          match missing_message with
          | None -> None
          | Some x ->
              Some (lazy (interpret_expr_and_check ~config upper_steps env x))
        in
        match items'.v with
        | Map map -> (
            match (List.assoc_opt ~equal:Value.equal key' map, def) with
            | Some v, _ -> v
            | None, Some v -> Lazy.force v
            | _ ->
                let default_message = "Missing item in map" in
                let missing_message =
                  match missing_message with
                  | None -> Value.Typed.string default_message
                  | Some missing_message -> Lazy.force missing_message
                in
                raise
                  (ExecFailure
                     ( missing_message
                     , [
                         `Text (default_message ^ ":")
                       ; `Value (Value.typecheck key.et key')
                       ; `Text "is not in"
                       ; `Value (Value.typecheck items.et items')
                       ; `Text "while evaluating"
                       ; `Text (pp ())
                       ] )))
        | _ ->
            raise
              (SmartExcept
                 [
                   `Text "Bad getItem"
                 ; `Value (Value.typecheck items.et items')
                 ; `Text "["
                 ; `Value (Value.typecheck key.et key')
                 ; `Text "]"
                 ]))
    | ERecord [] -> Value.unit
    | ERecord l -> Value.record (List.map (fun (s, e) -> (s, interp e)) l)
    | EMap_function {l; f} -> (
        let tf = f.et in
        let f = interpret_expr_and_check ~config upper_steps env f in
        let l = interpret_expr_and_check ~config upper_steps env l in
        match (l.tv, f.tv, Type.unF tf) with
        | Variant ("None", _), _, _ -> Value.erase_types l
        | Variant ("Some", x), Closure (f_code, f_args), _ ->
            Value.some
              (Value.erase_types
                 (call_closure ~config upper_steps env (f_code, f_args) x))
        | List l, Closure (f_code, f_args), TLambda (_effects, _, _) ->
            l
            |> List.map (fun x ->
                   Value.erase_types
                     (call_closure ~config upper_steps env (f_code, f_args) x))
            |> Value.build_list
        | ( Map l
          , Closure (f_code, f_args)
          , TLambda
              ( _effects
              , F
                  (TRecord
                    {layout = Value layout; row = [("key", tkey); ("value", _)]})
              , _ ) ) ->
            l
            |> List.map (fun (k, v) ->
                   ( Value.erase_types k
                   , Value.erase_types
                       (call_closure ~config upper_steps env (f_code, f_args)
                          (mk_key_value ~layout k v)) ))
            |> fun l -> Value.map ~tkey l
        | _ -> impossible ())
    | ESlice {offset; length; buffer} -> (
        let offset, length, buffer =
          (interp offset, interp length, interp buffer)
        in
        Basics.(
          match (offset.v, length.v, buffer.v) with
          | ( Literal (Int {i = ofs_bi})
            , Literal (Int {i = len_bi})
            , Literal (String s) ) -> (
              try
                Value.some
                  (Value.string
                     (String.sub s
                        (Big_int.int_of_big_int ofs_bi)
                        (Big_int.int_of_big_int len_bi)))
              with _ -> Value.none)
          | ( Literal (Int {i = ofs_bi})
            , Literal (Int {i = len_bi})
            , Literal (Bytes s) ) -> (
              try
                Value.some
                  (Value.bytes
                     (String.sub s
                        (Big_int.int_of_big_int ofs_bi)
                        (Big_int.int_of_big_int len_bi)))
              with _ -> Value.none)
          | _ -> impossible ()))
    | EMap (_, entries) -> (
        match Type.unF e.et with
        | T2 ((T_map | T_big_map), tkey, _) ->
            Value.map ~tkey
              (List.fold_right
                 (fun (key, value) entries ->
                   let value = interp value in
                   let key = interp key in
                   (key, value)
                   :: Base.List.Assoc.remove ~equal:Value.equal entries key)
                 entries [])
        | _ -> impossible ())
    | EList items -> Value.build_list (List.rev_map interp (List.rev items))
    | ESet l -> (
        match Type.unF e.et with
        | T1 (T_set, telement) ->
            Value.set ~telement (List.rev_map interp (List.rev l))
        | _ -> impossible ())
    | EContract {arg_type; address; entry_point} -> (
        match (entry_point, interp address) with
        | ( entry_point
          , {v = Literal (Address {address = addr; entry_point = None})} )
        | None, {v = Literal (Address {address = addr; entry_point})} -> (
            let ok () = Value.some (Value.contract ?entry_point addr) in
            let contract =
              match Hashtbl.find_opt env.scenario_state.rev_addresses addr with
              | None -> None
              | Some id ->
                  Option.map
                    (fun c -> (id, c))
                    (Hashtbl.find_opt env.scenario_state.contracts id)
            in
            match contract with
            | None -> ok ()
            | Some (id, contract) -> (
                let extra = get_extra contract.template.tcontract.derived in
                let raise_or_none exn =
                  if extra.config.contract_check_exception
                  then raise exn
                  else Value.none
                in
                let check_type t =
                  if Type.equal arg_type t
                  then ok ()
                  else if extra.config.contract_check_exception
                  then
                    raise_or_none
                      (ExecFailure
                         ( Value.Typed.string "Wrong type for contract"
                         , [
                             `Text "Wrong type for contract("
                           ; `Text (Printer.string_of_contract_id id)
                           ; `Text ")."
                           ; `Br
                           ; `Expr e
                           ; `Br
                           ; `Type t
                           ; `Br
                           ; `Text "instead of"
                           ; `Br
                           ; `Type arg_type
                           ; `Br
                           ; `Text "Please set a stable type."
                           ; `Br
                           ; `Line e.line_no
                           ] ))
                  else Value.none
                in
                match entry_point with
                | None -> (
                    match Type.unF extra.tparameter with
                    | TVariant {row = [(_, t)]} -> check_type t
                    | TVariant {row = []} | T0 T_unit -> check_type Type.unit
                    | TVariant {row} -> (
                        match List.assoc_opt "default" row with
                        | None ->
                            raise_or_none
                              (ExecFailure
                                 ( Value.Typed.string
                                     "Missing entry point target in contract"
                                 , [
                                     `Text
                                       "Missing entry point target in contract \
                                        ("
                                   ; `Text (Printer.string_of_contract_id id)
                                   ; `Text ")."
                                   ; `Br
                                   ; `Line e.line_no
                                   ] ))
                        | Some t -> check_type t)
                    | _ -> impossible ())
                | Some entry_point -> (
                    match Type.unF extra.tparameter with
                    | TVariant {row = [(ep_name, t)]} ->
                        if ep_name = entry_point
                           && extra.config.single_entry_point_annotation
                        then check_type t
                        else
                          raise_or_none
                            (ExecFailure
                               ( Value.Typed.string "No annotation in contract"
                               , [
                                   `Text "Entry point annotation ("
                                 ; `Text entry_point
                                 ; `Text ") for contract ("
                                 ; `Text (Printer.string_of_contract_id id)
                                 ; `Text
                                     ") with a single entry point and no \
                                      annotation."
                                 ; `Br
                                 ; `Line e.line_no
                                 ] ))
                    | TVariant {row} -> (
                        match List.assoc_opt entry_point row with
                        | None ->
                            raise_or_none
                              (ExecFailure
                                 ( Value.Typed.string
                                     "Missing entry point target in contract"
                                 , [
                                     `Text "Missing entry point target ("
                                   ; `Text entry_point
                                   ; `Text ") for contract ("
                                   ; `Text (Printer.string_of_contract_id id)
                                   ; `Text ")."
                                   ; `Br
                                   ; `Line e.line_no
                                   ] ))
                        | Some t -> check_type t)
                    | _ -> impossible ())))
        | _ -> impossible ())
    | ETuple es -> Value.tuple (List.rev_map interp (List.rev es))
    | ELambda l -> Value.closure_init l
    | EMake_signature {secret_key; message; message_format} ->
        let secret_key, message = (interp secret_key, interp message) in
        let secret_key =
          secret_key.v |> function
          | Literal (Secret_key k) -> k
          | _ -> impossible ()
        in
        let message =
          match message.v with
          | Literal (Bytes k) -> (
              match message_format with
              | `Raw -> k
              | `Hex ->
                  Hex.to_string
                    (`Hex
                      (Option.default k
                         (Base.String.chop_prefix k ~prefix:"0x"))))
          | _ -> impossible ()
        in
        P.Crypto.sign ~secret_key message |> Value.signature
    | EMichelson ({parsed; typesIn; typesOut}, es) -> (
        let open Michelson_interpreter in
        let code = Michelson.Of_micheline.instruction parsed in
        let ts =
          List.map (fun e -> Typing.mtype_of_type ~with_annots:true e.et) es
        in
        let code =
          Michelson.typecheck_instr ~protocol ~strict_dup:false
            ~tparameter:(Michelson.mt_unit, None)
            (* TODO: rely on the Checker and treat tparameter *)
            (Stack_ok ts) code
        in
        let stack =
          List.map2
            (fun t x -> mvalue_of_value ~protocol t (interp x))
            typesIn es
        in
        let module MI = M (P) in
        let context =
          let amount = env.context.amount in
          let balance = (get_current_instance env "balance").state.balance in
          ({config; balance; amount} : context)
        in
        let stack = MI.interpret context code stack in
        match (stack, typesOut) with
        | Ok [x], [t] ->
            let mt = Typing.mtype_of_type ~with_annots:false t in
            let x = typecheck_mvalue mt x in
            let x = value_of_tmvalue config x in
            let x = Value.smartMLify t x in
            let _ = Value.typecheck t x in
            x
        | Failed v, _ ->
            let t = Typing.type_of_mtype v.t in
            let v = value_of_tmvalue config v in
            let v = Value.typecheck t v in
            raise
              (ExecFailure
                 (v, [`Text "Failure:"; `Value v; `Br; `Line e.line_no]))
        | Error msg, _ -> failwith msg
        | _ -> impossible ())
    | ETransfer {destination; arg = params; amount} ->
        let params_t = params.et in
        let params, amount, destination =
          (interp params, interp amount, interp destination)
        in
        let destination =
          match destination with
          | {v = Contract {address; entrypoint}} ->
              {address; entry_point = entrypoint; type_ = params_t}
          | _ -> impossible ()
        in
        let amount =
          match amount with
          | {v = Literal (Mutez x)} -> x
          | _ -> impossible ()
        in
        Value.operation (Transfer {params; destination; amount})
    | ECreate_contract {contract_template; baker; balance; storage} ->
        let baker, balance, storage =
          (interp baker, interp balance, interp storage)
        in
        let baker = Value.un_baker baker in
        let balance =
          match balance with
          | {v = Literal (Mutez x)} -> x
          | _ -> impossible ()
        in
        let storage = storage in
        let {template; state} =
          interpret_contract ~config ~primitives
            ~scenario_state:env.scenario_state
            {tcontract = {contract_template with storage = None}}
        in
        let state = {state with baker; balance; storage = Some storage} in
        let dynamic_id = !(env.scenario_state.next_dynamic_address_id) in
        incr env.scenario_state.next_dynamic_address_id;
        let id = C_dynamic {dynamic_id} in
        let address = get_contract_address env.scenario_state id in
        Value.record
          [
            ( "operation"
            , Value.operation
                (CreateContract {id; instance = {template; state}}) )
          ; ("address", Value.literal (Literal.address address))
          ]
    | EMatch _ -> failwith "Interpreter TODO: ematch"
    | ESapling_verify_update {state; transaction} -> (
        let state = interp state in
        let transaction = interp transaction in
        match (state.v, transaction.v) with
        | ( Literal (Sapling_test_state {memo; elements})
          , Literal
              (Sapling_test_transaction {source; target; amount; boundData}) )
          -> (
            let output =
              match (source, target) with
              | None, None -> impossible ()
              | None, Some _ -> Bigint.minus_big_int amount
              | Some _, None -> amount
              | Some _, Some _ -> Bigint.zero_big_int
            in
            let state =
              match source with
              | None -> Some elements
              | Some source -> (
                  match List.assoc_opt source elements with
                  | Some x when Bigint.ge_big_int x amount ->
                      Some
                        ((source, Bigint.sub_big_int x amount)
                        :: List.remove_assoc source elements)
                  | _ -> None)
            in
            let state =
              match (state, target) with
              | None, _ -> None
              | Some state, None -> Some state
              | Some state, Some target -> (
                  match List.assoc_opt target state with
                  | Some x ->
                      Some
                        ((target, Bigint.add_big_int x amount)
                        :: List.remove_assoc target state)
                  | _ -> Some ((target, amount) :: state))
            in
            match state with
            | None -> Value.none
            | Some state ->
                Value.some
                  (Value.tuple
                     [
                       Value.bytes boundData
                     ; Value.int output
                     ; Value.literal (Literal.sapling_test_state memo state)
                     ]))
        | _ -> impossible ())
  in
  interp

and path_of_expr ~config upper_steps env =
  let rec of_expr acc e =
    match e.e with
    | EVar ("__storage__", _) -> Some {root = R_storage; steps = acc}
    | EPrim0 EEntrypoint_map -> Some {root = R_entrypoint_map; steps = acc}
    | EPrim0 (EMeta_local name) | EVar (name, Local) ->
        Some {root = R_local name; steps = acc}
    | EItem {items = cont; key; default_value = None; missing_message = None}
      -> (
        let key = interpret_expr_and_check ~config upper_steps env key in
        match Type.unF cont.et with
        | T2 ((T_map | T_big_map), _, _) -> of_expr (S_item_map key :: acc) cont
        | _ ->
            raise
              (SmartExcept
                 [
                   `Text "Interpreter Error"
                 ; `Br
                 ; `Text "GetItem"
                 ; `Expr e
                 ; `Expr cont
                 ; `Text "is not a map"
                 ; `Line e.line_no
                 ]))
    | EPrim1 (EAttr name, expr) -> of_expr (S_attr name :: acc) expr
    | EVar (name, Simple) -> (
        match List.assoc name !(env.variables) with
        | Iter (_, Some p) -> Some (extend_path p acc)
        | Iter (_, None) -> None (* Iteratee not an l-expression. *)
        | _ -> None)
    | _ -> None
  in
  of_expr []

and interpret_command ~config upper_steps env
    ({line_no} as initialCommand : tcommand) : value =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let interpret_command = interpret_command ~config in
  let interpret_expr = interpret_expr ~config in
  let path_of_expr = path_of_expr ~config in
  let impossible () =
    impossible "interpret_command %S" (show_tcommand initialCommand)
  in
  let un_bool = un_bool impossible in
  match initialCommand.c with
  | CNever message ->
      ignore (interpret_expr upper_steps env message : Value.t);
      assert false
  | CFailwith message ->
      let value = interpret_expr_and_check ~config upper_steps env message in
      raise
        (ExecFailure
           ( value
           , [`Text "Failure:"; `Value value]
             @ (match message.e with
               | EPrim0 (ELiteral _) -> []
               | _ -> [`Br; `Text "("; `Expr message; `Text ")"])
             @ [`Br; `Line line_no] ))
  | CIf (c, t, e) ->
      let sub_steps = ref [] in
      let condition = interpret_expr upper_steps env c in
      let r =
        if un_bool condition
        then interpret_command sub_steps env t
        else interpret_command sub_steps env e
      in
      add_step ~sub_steps upper_steps env initialCommand
        [("condition", Value.typecheck c.et condition)];
      r
  | CMatch_cons {expr; id; ok_match; ko_match} -> (
      match (interpret_expr upper_steps env expr).v with
      | List [] ->
          let sub_steps = ref [] in
          interpret_command sub_steps env ko_match
      | List (head :: tail) ->
          let sub_steps = ref [] in
          let env =
            {
              env with
              variables =
                ref
                  (add_variable id
                     (Constant
                        (Value.record
                           [("head", head); ("tail", Value.build_list tail)]))
                     !(env.variables))
            }
          in
          interpret_command sub_steps env ok_match
      | _ -> assert false)
  | CMatch_product (s, p, c) ->
      let sub_steps = ref [] in
      let variables = !(env.variables) in
      let vs =
        match p with
        | Pattern_single x ->
            let v = interpret_expr upper_steps env s in
            [(x, Constant v)]
        | Pattern_tuple ns ->
            let vs = un_tuple impossible (interpret_expr upper_steps env s) in
            List.map2 ~err:"match" (fun n v -> (n, Constant v)) ns vs
        | Pattern_record (_, bs) -> (
            let r = interpret_expr upper_steps env s in
            match r.v with
            | Record r ->
                let f {var; field} =
                  (var, Constant (List.assoc_exn ~msg:"match" field r))
                in
                List.map f bs
            | _ -> assert false)
      in
      let variables = List.fold_right (uncurry add_variable) vs variables in
      let env = {env with variables = ref variables} in
      interpret_command sub_steps env c
  | CModify_product (lhs, p, c) -> (
      match path_of_expr upper_steps env lhs with
      | None -> assert false
      | Some lhs_path -> (
          let lhs_lens, _err = lens_of_path ~config ~line_no env lhs_path in
          let sub_steps = ref [] in
          let variables = !(env.variables) in
          let v = interpret_expr upper_steps env lhs in
          let v = Value.typecheck lhs.et v in
          let vs =
            match p with
            | Pattern_single x -> [(x, Heap_ref (ref v))]
            | Pattern_tuple ns -> (
                match v.tv with
                | Tuple vs ->
                    List.map2 ~err:"match"
                      (fun n v -> (n, Heap_ref (ref v)))
                      ns vs
                | _ -> assert false)
            | Pattern_record (name, _bs) -> [(name, Heap_ref (ref v))]
            (* let r = Value.un_record v in
             * let f {var; field} =
             *   (var, Heap_ref (ref (List.assoc_exn ~msg:"match" field r)))
             * in
             * List.map f bs *)
          in
          let variables = List.fold_right (uncurry add_variable) vs variables in
          let env = {env with variables = ref variables} in
          let inner = interpret_command sub_steps env c in
          match p with
          | Pattern_single _ | Pattern_tuple _ ->
              let inner = Value.typecheck c.ct inner in
              let rhs = Some inner in
              Lens.set lhs_lens rhs ();
              Value.unit
          | Pattern_record (name, _bs) ->
              assert_unit initialCommand inner;
              let value =
                match List.assoc_opt name !(env.variables) with
                | Some (Heap_ref {contents = x}) -> x
                | _ -> assert false
              in
              Lens.set lhs_lens (Some value) ();
              Value.unit))
  | CMatch (scrutinee, cases) -> (
      let sub_steps = ref [] in
      let scrutinee' = interpret_expr upper_steps env scrutinee in
      match scrutinee'.v with
      | Variant (cons, arg) ->
          let res =
            match
              List.find_opt
                (fun (constructor, _, _) -> constructor = cons)
                cases
            with
            | None -> Value.unit
            | Some (_constructor, arg_name, body) ->
                interpret_command sub_steps
                  {
                    env with
                    variables =
                      ref
                        (add_variable arg_name (Constant arg) !(env.variables))
                  }
                  body
          in
          let scrutinee' = Value.typecheck scrutinee.et scrutinee' in
          add_step ~sub_steps upper_steps env initialCommand
            [("match", scrutinee')];
          res
      | _ -> assert false)
  | CBind (x, c1, c2) ->
      let outer_locals = !(env.current_locals) in
      env.current_locals := [];
      let y = interpret_command upper_steps env c1 in
      let y = Value.typecheck c1.ct y in
      (match x with
      | None -> ()
      | Some x ->
          env.variables := add_variable x (Heap_ref (ref y)) !(env.variables));
      let r = interpret_command upper_steps env c2 in
      env.variables :=
        List.filter
          (fun (n, _) ->
            (not (List.mem n !(env.current_locals)))
            && Option.cata true (( <> ) n) x)
          !(env.variables);
      env.current_locals := outer_locals;
      r
  | CSet_var (lhs, rhs) ->
      (match path_of_expr upper_steps env lhs with
      | None -> assert false
      | Some lhs ->
          let lhs, _err = lens_of_path ~config ~line_no env lhs in
          let rhs =
            Some (interpret_expr_and_check ~config upper_steps env rhs)
          in
          Lens.set lhs rhs ());
      add_step upper_steps env initialCommand [];
      Value.unit
  | CDel_item (map, key) ->
      (match path_of_expr upper_steps env map with
      | None -> assert false
      | Some path ->
          let key = interpret_expr_and_check ~config upper_steps env key in
          let l, _err =
            lens_of_path ~config ~line_no env
              (extend_path path [S_item_map key])
          in
          Lens.set l None ());
      add_step upper_steps env initialCommand [];
      Value.unit
  | CUpdate_set (set, elem, add) ->
      (match path_of_expr upper_steps env set with
      | None -> assert false
      | Some path ->
          let elem = interpret_expr_and_check ~config upper_steps env elem in
          let l, err = lens_of_path ~config ~line_no env path in
          Lens.set (l @. Lens.some ~err @. Value.lens_set_at ~elem) add ());
      add_step upper_steps env initialCommand [];
      Value.unit
  | CDefine_local {var; rhs} ->
      env.current_locals := var :: !(env.current_locals);
      env.variables :=
        add_variable var
          (Heap_ref (ref (interpret_expr_and_check ~config upper_steps env rhs)))
          !(env.variables);
      add_step upper_steps env initialCommand [];
      Value.unit
  | CFor (name, iteratee, body) ->
      let sub_steps = ref [] in
      let iteratee_v =
        interpret_expr_and_check ~config upper_steps env iteratee
      in
      let iteratee_l = path_of_expr upper_steps env iteratee in
      (match iteratee_v.tv with
      | List elems ->
          let step i v =
            let path =
              Option.map (fun p -> extend_path p [S_item_list i]) iteratee_l
            in
            assert_unit initialCommand
              (interpret_command sub_steps
                 {
                   env with
                   variables =
                     ref (add_variable name (Iter (v, path)) !(env.variables))
                 }
                 body)
          in
          List.iteri step elems
      | _ -> assert false);
      add_step ~sub_steps upper_steps env initialCommand [];
      Value.unit
  | CWhile (e, cmd) ->
      let sub_steps = ref [] in
      while un_bool (interpret_expr upper_steps env e) do
        assert_unit initialCommand (interpret_command sub_steps env cmd)
      done;
      add_step ~sub_steps upper_steps env initialCommand [];
      Value.unit
  | CVerify (x, message) -> (
      let v = interpret_expr upper_steps env x in
      add_step upper_steps env initialCommand [];
      match v.v with
      | Literal (Bool true) -> Value.unit
      | Literal (Bool false) ->
          let except, message =
            match message with
            | Some e ->
                let v = interpret_expr_and_check ~config upper_steps env e in
                (v, [`Br; `Text "Message:"; `Value v])
            | None -> (Value.Typed.string (Printer.wrong_condition_string x), [])
          in
          raise
            (ExecFailure
               ( except
               , [
                   `Text "Wrong condition:"; `Expr x; `Rec message; `Line line_no
                 ] ))
      | _ -> assert false)
  | CResult e -> interpret_expr upper_steps env e
  | CComment _ -> Value.unit
  | CSet_type _ -> Value.unit
  | CSet_result_type (c, _) -> interpret_command upper_steps env c
  | CTrace e ->
      let e = interpret_expr_and_check ~config upper_steps env e in
      print_endline (Printer.value_to_string e);
      Value.unit

and interpret_expr_and_check ~config upper_steps env e =
  Value.typecheck e.et (interpret_expr ~config upper_steps env e)

and interpret_command_and_check ~config upper_steps env c =
  Value.typecheck c.ct (interpret_command ~config upper_steps env c)

and call_closure ~config upper_steps env (l, (args : tvalue list))
    (parameter : tvalue) =
  let parameter =
    List.fold_left (fun acc v -> Value.Typed.tuple [v; acc]) parameter args
  in
  let steps = ref [] in
  let variables =
    if l.with_operations
    then List.filter (fun (n, _) -> n = "__operations__") !(env.variables)
    else []
  in
  let variables =
    ref ((l.name, Constant (Value.erase_types parameter)) :: variables)
  in
  let env = {env with variables} in
  let r = interpret_command_and_check ~config steps env l.body in
  add_step ~sub_steps:steps upper_steps env l.body
    [("lambda input", parameter); ("lambda output", r)];
  r

and call_rec_closure ~config upper_steps env (l, (args : tvalue list))
    (parameter : tvalue) (fname, f) =
  let parameter =
    List.fold_left (fun acc v -> Value.Typed.tuple [v; acc]) parameter args
  in
  let steps = ref [] in
  let vars = [(l.name, Constant (Value.erase_types parameter))] in
  let vars = (fname, Constant (Value.erase_types f)) :: vars in
  let env = {env with variables = ref vars} in
  let r = interpret_command_and_check ~config steps env l.body in
  add_step ~sub_steps:steps upper_steps env l.body
    [("lambda input", parameter); ("lambda output", r)];
  r

and eval_view ~name ~id ~config ~line_no ~e ~return_type ~types_ok env
    (parameters, param_t) =
  let optional_return_type = Type.option return_type in
  match Hashtbl.find_opt env.scenario_state.contracts id with
  | None ->
      Printf.ksprintf failwith "Missing contract %s for view %s"
        (address_of_contract_id id)
        name
  | Some contract -> (
      let view =
        List.find_opt
          (fun (view : _ view) -> view.name = name)
          contract.template.tcontract.views
      in
      match view with
      | None ->
          if config.view_check_exception
          then
            Printf.ksprintf failwith "Missing view %s in contract %s" name
              (address_of_contract_id id)
          else Value.none
      | Some view ->
          let tparameter =
            match get_extra view.tparameter_derived with
            | None -> Type.unit
            | Some parameter -> parameter
          in
          let input_ok, output_ok =
            ( types_ok || Type.equal tparameter param_t
            , types_ok
              || Type.equal (Type.option view.body.ct) optional_return_type )
          in
          if input_ok && output_ok
          then
            let env =
              let context =
                let sender =
                  match env.context.contract_id with
                  | No_contract _ -> env.context.sender
                  | In_contract (id, _) -> Some (address_of_contract_id id)
                in
                {
                  env.context with
                  contract_id = In_contract (id, parameters)
                ; line_no
                ; amount = Bigint.zero_big_int
                ; sender
                }
              in
              {env with context}
            in
            Value.some (interpret_command ~config env.steps env view.body)
          else if config.view_check_exception
          then
            let message =
              match (input_ok, output_ok) with
              | true, true -> assert false
              | true, false -> "Wrong output type"
              | false, true -> "Wrong input type"
              | false, false -> "Wrong input and output type"
            in
            raise
              (ExecFailure
                 ( Value.Typed.string "Type error in view"
                 , [
                     `Text message
                   ; `Expr e
                   ; `Br
                   ; `Text "Expected input type (in the view):"
                   ; `Br
                   ; `Type tparameter
                   ; `Br
                   ; `Text "Actual input type:"
                   ; `Br
                   ; `Type param_t
                   ; `Br
                   ; `Text "Actual output type (in the view):"
                   ; `Br
                   ; `Type view.body.ct
                   ; `Br
                   ; `Text "Expected output type:"
                   ; `Br
                   ; `Type optional_return_type
                   ; `Line line_no
                   ] ))
          else Value.none)

and interpret_message ~config ~primitives ~scenario_state context id
    {channel; params} =
  let context =
    {context with contract_id = In_contract (id, Value.erase_types params)}
  in
  let env =
    {
      context
    ; variables =
        ref
          [
            ( "__operations__"
            , Heap_ref (ref (Value.Typed.list Type.operation [])) )
          ]
    ; current_locals = ref []
    ; primitives
    ; steps = ref []
    ; private_variables = []
    ; scenario_state
    ; config
    }
  in
  let ({
         template =
           {tcontract = {entrypoints; private_variables; derived}} as template
       ; state
       } as instance) =
    get_current_instance env "message"
  in
  let env = {env with private_variables} in
  let interpret_command = interpret_command ~config in
  let tparameter = (get_extra derived).tparameter in
  let contract_config = (get_extra derived).config in
  let ep =
    List.find_opt
      (fun ({channel = x} : _ entrypoint) -> channel = x)
      entrypoints
  in
  let ep =
    match (ep, channel, entrypoints) with
    | None, "default", [ep] -> Some ep
    | _ -> ep
  in
  match ep with
  | None ->
      ( None
      , []
      , Some
          (Execution.Exec_failure
             ( Printf.ksprintf Value.Typed.string "Channel not found: %s" channel
             , [] ))
      , [] )
  | Some ({channel; lazify; body} as ep) -> (
      if Basics.check_no_incoming_transfer ~config:contract_config ep
         && not (Bigint.eq_big_int context.amount Bigint.zero_big_int)
      then
        let err = [`Text "Check-no-incoming-transfer-failure"] in
        ( None
        , []
        , Some
            (Execution.Exec_failure
               ( Value.Typed.string
                   "Incoming transfer in check-no-incoming-transfer mode"
               , err ))
        , !(env.steps) )
      else
        let lazify = Option.default config.lazy_entry_points lazify in
        let ep =
          if lazify
          then
            match state.entrypoint_map with
            | Some {tv = Map leps} -> (
                let ix = get_lazy_ix instance channel in
                let leps =
                  List.map (fun (i, l) -> (Value.erase_types i, l)) leps
                in
                match List.assoc_opt ~equal:Value.equal ix leps with
                | None -> Some `Absent
                | Some ep -> Some (`Lazy ep))
            | _ -> assert false
          else None
        in
        try
          match ep with
          | Some `Absent ->
              let err = [`Text "lazy entry point not found"] in
              ( None
              , []
              , Some
                  (Execution.Exec_failure
                     (Value.Typed.string "Missing entry point", err))
              , !(env.steps) )
          | None ->
              let (_ : value) = interpret_command env.steps env body in
              let operations =
                match List.assoc_opt "__operations__" !(env.variables) with
                | Some (Heap_ref {contents = {tv = List ops}}) ->
                    List.map un_operation ops
                | _ -> impossible "__operations__ not found"
              in
              let state = (get_current_instance env "result").state in
              (Some {template; state}, List.rev operations, None, !(env.steps))
          | Some (`Lazy l) -> (
              let storage =
                match state.storage with
                | Some storage -> storage
                | None -> failwith "no storage"
              in
              let params = Value.Typed.variant tparameter channel params in
              let r =
                match l.tv with
                | Closure (l, args) ->
                    call_closure ~config env.steps env (l, args)
                      (Value.Typed.tuple [params; storage])
                | _ -> impossible "entrypoint not a closure"
              in
              match r.tv with
              | Tuple [{tv = List ops}; storage] ->
                  let ops = List.map un_operation ops in
                  let state = (get_current_instance env "result").state in
                  let state = {state with storage = Some storage} in
                  (Some {template; state}, List.rev ops, None, !(env.steps))
              | _ -> impossible "lazy entry point returned non-pair")
        with
        | Failure f ->
            ( None
            , []
            , Some
                (Execution.Exec_failure
                   (Value.Typed.string f, [`Text "Failure:"; `Text f]))
            , !(env.steps) )
        | ExecFailure (value, message) ->
            ( None
            , []
            , Some (Execution.Exec_failure (value, message))
            , !(env.steps) )
        | SmartExcept l ->
            ( None
            , []
            , Some (Execution.Exec_failure (Value.Typed.string "Error", l))
            , !(env.steps) ))

and interpret_expr_external ~config ~primitives ~no_env ?source ?sender
    ~(scenario_state : scenario_state) =
  let context =
    build_context ?source ?sender ~scenario_state ~amount:Big_int.zero_big_int
      ~line_no:[] ~debug:false ()
  in
  let context = {context with contract_id = No_contract no_env} in
  let env =
    {
      context
    ; variables = ref []
    ; current_locals = ref []
    ; primitives
    ; steps = ref []
    ; private_variables = []
    ; scenario_state
    ; config
    }
  in
  interpret_expr ~config (ref []) env

and interpret_contract ~config ~primitives ~scenario_state
    ({tcontract = {balance; storage; baker; metadata}} as c : tcontract) =
  let conv name =
    interpret_expr_external ~config ~primitives
      ~no_env:[`Text ("Compute " ^ name)]
      ~scenario_state
  in
  let balance =
    match Option.map (conv "balance") balance with
    | None -> Big_int.zero_big_int
    | Some {v = Literal (Mutez x)} -> x
    | _ -> impossible "interpret_contract: balance not a mutez"
  in
  let storage = Option.map (conv "storage") storage in
  let baker = Option.bind baker (fun x -> Value.un_baker (conv "baker" x)) in
  let metadata = List.map (map_snd (Meta.map (conv "storage"))) metadata in
  let entrypoint_map =
    match (get_extra c.tcontract.derived).tparameter_lazy with
    | None -> None
    | Some _ ->
        let f ep =
          let lazy_no_code = Option.default false ep.lazy_no_code in
          match (get_extra ep.derived).lazy_index with
          | Some ix when not lazy_no_code ->
              let ix = Value.nat (Bigint.of_int ix) in
              Some (ix, Value.erase_types (closure_of_entrypoint ~config c ep))
          | _ -> None
        in
        let leps = List.map_some f c.tcontract.entrypoints in
        Some
          (Value.typecheck (entrypoint_map_type c)
             (Value.map ~tkey:Type.nat leps))
  in
  {template = c; state = {balance; storage; baker; metadata; entrypoint_map}}
