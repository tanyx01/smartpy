(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Basics
open Typed
open Utils
open Control

let has_unknowns_talg =
  {(monoid_talg ( || ) false) with f_ttype = Type.has_unknowns}

let has_unknowns_expr = cata_texpr has_unknowns_talg

let has_unknowns_command = cata_tcommand has_unknowns_talg

let gather_unknowns ~tstorage ~tparameter ~private_variables ~entrypoints =
  let xs =
    [
      ("storage", Type.has_unknowns tstorage)
    ; ("tparameter", Type.has_unknowns tparameter)
    ]
    @ List.concat
        (List.map
           (fun {channel; body; derived} ->
             [
               ( channel ^ "(type)"
               , Type.has_unknowns (get_extra derived).tparameter_ep )
             ; (channel ^ "(command)", has_unknowns_command body)
             ])
           entrypoints)
    @ List.map (fun (name, e) -> (name, has_unknowns_expr e)) private_variables
  in
  let xs = List.filter_map (fun (s, b) -> if b then Some s else None) xs in
  if xs = [] then None else Some (String.concat ", " xs)

let full_unknown =
  let id = ref 0 in
  fun () ->
    let v = string_of_int !id in
    incr id;
    Type.unknown_raw ("c" ^ v)

let close_hole default =
  let open Hole in
  function
  | Variable _ -> Value default
  | Value _ as h -> h

let close_layout default row =
  close_hole (Type.default_layout_of_row default (List.map fst row))

let close_type ~config =
  let open Type in
  let f t0 =
    match t0 with
    | TInt {isNat = Variable _} -> F (TInt {isNat = Variable {var_id = 0}})
    | TBounded {base; cases} -> Type.bounded base cases None
    | TRecord {layout; row} ->
        record_or_unit
          (close_layout config.Config.default_record_layout row layout)
          row
    | TVariant {layout; row} ->
        variant
          (close_layout config.Config.default_variant_layout row layout)
          row
    | TTuple (l, Some _) ->
        assert (l = List.sort Stdlib.compare l);
        let rec mk i0 = function
          | [] -> []
          | (i, t) :: xs ->
              if i = i0
              then t :: mk (i0 + 1) xs
              else full_unknown () :: mk (i0 + 1) ((i, t) :: xs)
        in
        Type.tuple (mk 0 l)
    | TLambda ({with_storage; with_operations}, t1, t2) ->
        let with_storage = close_hole None with_storage in
        let with_operations = close_hole false with_operations in
        Type.lambda {with_storage; with_operations} t1 t2
    | T0
        ( T_unit
        | T_bool
        | T_nat
        | T_int
        | T_mutez
        | T_string
        | T_bytes
        | T_chain_id
        | T_timestamp
        | T_address
        | T_key
        | T_key_hash
        | T_signature
        | T_operation
        | T_sapling_state _
        | T_sapling_transaction _
        | T_never
        | T_bls12_381_g1
        | T_bls12_381_g2
        | T_bls12_381_fr
        | T_chest_key
        | T_chest )
    | T1 ((T_option | T_list | T_set | T_contract | T_ticket), _)
    | T2 ((T_map | T_big_map), _, _)
    | TSecretKey | TInt _
    | TTuple (_, None)
    | TSaplingState _ | TSaplingTransaction _ | TUnknown _ -> F t0
    | T2 ((T_lambda | T_pair _ | T_or _), _, _) -> assert false
  in
  Type.cata f

let close_talg ~config =
  let f_texpr line_no et e =
    let e =
      match e with
      | EPrim0 (ELiteral (Int {i; is_nat = Variable _})) ->
          EPrim0 (ELiteral (Literal.intOrNat (Variable {var_id = 0}) i))
      | e -> e
    in
    build_texpr ~line_no e (close_type ~config et)
  in
  let f_tcommand line_no ct c =
    build_tcommand ~line_no c (close_type ~config ct)
  in
  {f_texpr; f_tcommand; f_ttype = close_type ~config}

let close_expr ~config = cata_texpr (close_talg ~config)

let close_command ~config = cata_tcommand (close_talg ~config)

let close_contract {tcontract = c} =
  let extra = get_extra c.derived in
  let tparameter = extra.tparameter in
  let tstorage = extra.tstorage in
  let config = extra.config in
  let ({private_variables; entrypoints; derived} as c : _ contract_f) =
    map_contract_f (close_expr ~config) (close_command ~config)
      (close_type ~config) id c
  in
  let c =
    {
      c with
      unknown_parts =
        gather_unknowns ~tstorage ~tparameter ~entrypoints ~private_variables
    ; derived
    }
  in
  {tcontract = c}

(** Give each entrypoint its index. Done here in the Closer because
   they are determined by the layout. *)
let index_lazy_entrypoints tparameter_lazy eps =
  match Type.unF tparameter_lazy with
  | T0 T_unit -> eps
  | TVariant {layout = Value layout} ->
      let lazy_ids =
        Binary_tree.(
          to_list
            (snd
               (map_accum (fun i x -> (i + 1, (x.Layout.source, i))) 0 layout)))
      in
      let f (ep : _ entrypoint) =
        let derived = get_extra ep.derived in
        match derived.lazy_index with
        | None -> ep
        | Some -1 ->
            let i =
              match List.assoc_opt ep.channel lazy_ids with
              | Some i -> i
              | None -> assert false
            in
            let lazy_index = Some i in
            {ep with derived = T {derived with lazy_index}}
        | _ -> assert false
      in
      List.map f eps
  | _ -> assert false

let index_contract_f (c : _ contract_f) =
  {
    c with
    entrypoints =
      (match (get_extra c.derived).tparameter_lazy with
      | Some t -> index_lazy_entrypoints t c.entrypoints
      | None -> c.entrypoints)
  }

let index_action = function
  | New_contract c ->
      New_contract {c with contract = index_contract_f c.contract}
  | ( Compute _
    | Simulation _
    | Message _
    | ScenarioError _
    | Html _
    | Verify _
    | Show _
    | Exception _
    | Set_delegate _
    | DynamicContract _
    | Add_flag _
    | Prepare_constant_value _
    | Mutation_test _ ) as a -> a

let close_scenario ~config s =
  let s =
    Basics.map_scenario_f (close_expr ~config) (close_command ~config)
      (close_type ~config) id s
  in
  {s with actions = List.map (fun (c, a) -> (c, index_action a)) s.actions}
