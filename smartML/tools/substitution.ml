(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Basics
open Utils
open Typed
open Control
open Type
module Set = VarId.Set

let initial_size = 8

type substitute =
  | SType of Type.t
  | SRow of (Type.t row * VarId.t option)
  | STupleRow of ((int * Type.t) list * VarId.t option)
  | SLayout of Layout.t Hole.t
  | SBool of bool Hole.t
  | SInt of int Hole.t
  | SWithStorage of (Type.access * Type.t) option Hole.t
  | SBounded of (Literal.t list * VarId.t option)
[@@deriving show]

type t = {
    contents_open : (VarId.t, Set.t * substitute) Hashtbl.t
  ; contents_closed : (VarId.t, substitute) Hashtbl.t
  ; mutable frees : Set.t
}

let frees_row r = Set.unions (List.map (fun (_, r) -> Type.frees r) r)

let dump {contents_open; contents_closed} =
  Format.printf "  contents_open:@.";
  let f (i : VarId.t) (fs, t) =
    Format.printf "    %d %a %a@." i.var_id (List.pp VarId.pp) (Set.elements fs)
      pp_substitute t
  in
  Hashtbl.iter f contents_open;
  Format.printf "  contents_closed:@.";
  let f (i : VarId.t) t =
    Format.printf "    %d %a@." i.var_id pp_substitute t
  in
  Hashtbl.iter f contents_closed

let frees_open_row (r, i) =
  match i with
  | None -> frees_row r
  | Some i -> Set.add i (frees_row r)

let frees_hole x =
  match x with
  | Hole.Variable i -> Set.singleton i
  | _ -> Set.empty

let substitute_frees = function
  | SType t -> Type.frees t
  | SRow r -> frees_open_row r
  | STupleRow r -> frees_open_row r
  | SLayout x -> frees_hole x
  | SBool x -> frees_hole x
  | SInt x -> frees_hole x
  | SWithStorage x -> frees_hole x
  | SBounded (_, v) -> Option.cata Set.empty Set.singleton v

let singleton (i, y) =
  let frees = substitute_frees y in
  let contents_closed = Hashtbl.create initial_size in
  let contents_open = Hashtbl.create initial_size in
  if Set.is_empty frees
  then Hashtbl.add contents_closed i y
  else Hashtbl.add contents_open i (frees, y);
  {contents_closed; contents_open; frees}

let empty () =
  let contents_open = Hashtbl.create initial_size in
  let contents_closed = Hashtbl.create initial_size in
  {contents_open; contents_closed; frees = Set.empty}

let find_opt i {contents_open; contents_closed} =
  match Hashtbl.find_opt contents_closed i with
  | Some x -> Some x
  | None -> (
      match Hashtbl.find_opt contents_open i with
      | Some (_, x) -> Some x
      | None -> None)

let on_hole f s =
  let open Hole in
  function
  | Variable i as l -> (
      match find_opt i s with
      | None -> l
      | Some x -> f x)
  | v -> v

let on_layout =
  on_hole (function
    | SLayout x -> x
    | _ -> assert false)

let on_bool =
  on_hole (function
    | SBool x -> x
    | _ -> assert false)

let on_int =
  on_hole (function
    | SInt x -> x
    | _ -> assert false)

let on_with_storage =
  on_hole (function
    | SWithStorage x -> x
    | _ -> assert false)

let on_open_row s (row, var) =
  match var with
  | None -> (row, var)
  | Some var -> (
      match find_opt var s with
      | None -> (row, Some var)
      | Some (SRow (r', var')) -> (List.sort Stdlib.compare (row @ r'), var')
      | Some _ -> assert false)

let on_bounded s (cases, var) =
  match var with
  | None -> (cases, var)
  | Some var -> (
      match find_opt var s with
      | None -> (cases, Some var)
      | Some (SBounded (cases', var')) ->
          (List.sort Literal.compare (cases @ cases'), var')
      | Some _ -> assert false)

let on_type s =
  let open Type in
  let f = function
    | TLambda ({with_storage; with_operations}, t1, t2) ->
        let with_storage = on_with_storage s with_storage in
        let with_operations = on_bool s with_operations in
        F (TLambda ({with_storage; with_operations}, t1, t2))
    | TBounded {base; cases; var} ->
        let cases, var = on_bounded s (cases, var) in
        F (TBounded {base; cases; var})
    | TUnknown (i, _) as t -> (
        match find_opt i s with
        | None -> F t
        | Some (SType t') -> t'
        | Some _ -> assert false)
    | TInt {isNat} ->
        let isNat = on_bool s isNat in
        F (TInt {isNat})
    | TSaplingState {memo} ->
        let memo = on_int s memo in
        F (TSaplingState {memo})
    | TSaplingTransaction {memo} ->
        let memo = on_int s memo in
        F (TSaplingTransaction {memo})
    | TTuple (row, var) ->
        let row, var =
          match var with
          | None -> (row, var)
          | Some var -> (
              match find_opt var s with
              | None -> (row, Some var)
              | Some (STupleRow (r', var')) ->
                  (List.sort Stdlib.compare (row @ r'), var')
              | Some _ -> assert false)
        in
        F (TTuple (row, var))
    | TRecord {row; var; layout} ->
        let row, var =
          match var with
          | None -> (row, var)
          | Some var -> (
              match find_opt var s with
              | None -> (row, Some var)
              | Some (SRow (r', var')) ->
                  (List.sort Stdlib.compare (row @ r'), var')
              | Some _ -> assert false)
        in
        let layout = on_layout s layout in
        F (TRecord {row; var; layout})
    | TVariant {row; var; layout} ->
        let row, var =
          match var with
          | None -> (row, var)
          | Some var -> (
              match find_opt var s with
              | None -> (row, Some var)
              | Some (SRow (r', var')) ->
                  (List.sort Stdlib.compare (row @ r'), var')
              | Some _ -> assert false)
        in
        let layout = on_layout s layout in
        F (TVariant {row; var; layout})
    | t -> F t
  in
  cata f

let on_literal subst =
  let open Literal in
  function
  | Int {i; is_nat} ->
      let is_nat = on_bool subst is_nat in
      Literal.intOrNat is_nat i
  | x -> x

let on_alg subst =
  let f_texpr line_no et = function
    | EPrim0 (ELiteral x) ->
        let e = EPrim0 (ELiteral (on_literal subst x)) in
        build_texpr ~line_no e (on_type subst et)
    | e -> build_texpr ~line_no e (on_type subst et)
  in
  let f_tcommand line_no ct c = build_tcommand ~line_no c (on_type subst ct) in
  let f_ttype = on_type subst in
  {f_texpr; f_tcommand; f_ttype}

let on_expr subst = cata_texpr (on_alg subst)

let on_command subst = cata_tcommand (on_alg subst)

let on_lambda subst =
  map_lambda_f (on_command subst) (on_type subst) (fun x -> x)

let on_contract subst {tcontract} =
  let f_expr = on_expr subst in
  let f_command = on_command subst in
  let f_type = on_type subst in
  {tcontract = map_contract_f f_expr f_command f_type (fun x -> x) tcontract}

let on_scenario subst =
  let f_expr = on_expr subst in
  let f_command = on_command subst in
  let f_type = on_type subst in
  map_scenario_f f_expr f_command f_type (fun x -> x)

let rec on_smart_except subst : smart_except -> smart_except = function
  | `Expr e -> `Expr (on_expr subst e)
  | `Exprs es -> `Exprs (List.map (on_expr subst) es)
  | `Type t -> `Type (on_type subst t)
  | (`Expr_untyped _ | `Value _ | `Literal _ | `Line _ | `Text _ | `Br) as e ->
      e
  | `Rec es -> `Rec (List.map (on_smart_except subst) es)

let on_lazy_smart_except_list subst f () =
  List.map (on_smart_except subst) (f ())

let on_constraint s =
  let on_expr = on_expr s in
  let on_type = on_type s in
  function
  | HasAdd (e1, e2, e3) -> HasAdd (on_expr e1, on_expr e2, on_expr e3)
  | HasMul (e1, e2, e3) -> HasMul (on_expr e1, on_expr e2, on_expr e3)
  | HasSub (e1, e2, e3) -> HasSub (on_expr e1, on_expr e2, on_expr e3)
  | HasDiv (e1, e2, e3) -> HasDiv (on_expr e1, on_expr e2, on_expr e3)
  | HasBitArithmetic (e1, e2, e3) ->
      HasBitArithmetic (on_expr e1, on_expr e2, on_expr e3)
  | HasMap (e1, e2, e3) -> HasMap (on_expr e1, on_expr e2, on_expr e3)
  | IsComparable t -> IsComparable (on_expr t)
  | IsPackable t -> IsPackable (on_type t)
  | HasGetItem (e1, e2, t) -> HasGetItem (on_expr e1, on_expr e2, on_type t)
  | HasContains (e1, e2, ln) -> HasContains (on_expr e1, on_expr e2, ln)
  | HasSize e -> HasSize (on_expr e)
  | HasSlice e -> HasSlice (on_expr e)
  | AssertEqual (t1, t2, exc) ->
      AssertEqual (on_type t1, on_type t2, on_lazy_smart_except_list s exc)
  | IsInt (t, exc) -> IsInt (on_type t, on_lazy_smart_except_list s exc)
  | SaplingVerify (e1, e2) -> SaplingVerify (on_expr e1, on_expr e2)
  | HasNeg (e, t) -> HasNeg (on_expr e, on_type t)
  | HasInt e -> HasInt (on_expr e)
  | IsNotHot (str, t) -> IsNotHot (str, on_type t)
  | IsAnyMap (t1, t2, e) -> IsAnyMap (on_type t1, on_type t2, on_expr e)
  | IsConvertible (t1, t2) -> IsConvertible (on_type t1, on_type t2)
  | IsInstance2 (cls, e, (t1, t2), instances) ->
      IsInstance2 (cls, on_expr e, (on_type t1, on_type t2), instances)

let on_row s (r, i) =
  let r = List.map (map_snd (on_type s)) r in
  match i with
  | None -> (r, None)
  | Some i -> (
      match find_opt i s with
      | None -> (r, Some i)
      | Some (SRow (r', i')) -> (List.sort Stdlib.compare (r @ r'), i')
      | Some _ -> assert false)

let on_tuple_row s (r, i) =
  let r = List.map (map_snd (on_type s)) r in
  match i with
  | None -> (r, None)
  | Some i -> (
      match find_opt i s with
      | None -> (r, Some i)
      | Some (STupleRow (r', i')) -> (List.sort Stdlib.compare (r @ r'), i')
      | Some _ -> assert false)

let on_entry (i, y) (fs, x) =
  if not (Set.mem i fs)
  then (fs, x)
  else
    let s = singleton (i, y) in
    match x with
    | SType t ->
        let t = on_type s t in
        (Type.frees t, SType t)
    | SRow t ->
        let t = on_row s t in
        (frees_open_row t, SRow t)
    | STupleRow t ->
        let t = on_tuple_row s t in
        (frees_open_row t, STupleRow t)
    | SLayout x ->
        let s = on_layout s x in
        (frees_hole s, SLayout s)
    | SBool x ->
        let s = on_bool s x in
        (frees_hole s, SBool s)
    | SInt x ->
        let s = on_int s x in
        (frees_hole s, SInt s)
    | SWithStorage x ->
        let s = on_with_storage s x in
        (frees_hole s, SWithStorage s)
    | SBounded x ->
        let s, i = on_bounded s x in
        (Option.cata Set.empty Set.singleton i, SBounded (s, i))

let add (i, y) s =
  (*
  assert (not (Hashtbl.mem s.contents_open i));
  assert (not (Hashtbl.mem s.contents_closed i));
   *)
  let frees_y = substitute_frees y in
  let closed_y = Set.is_empty frees_y in
  Hashtbl.filter_map_inplace
    (fun j (fs, y2) ->
      let fs', y2' = on_entry (i, y) (fs, y2) in
      if Set.is_empty fs'
      then (
        Hashtbl.add s.contents_closed j y2';
        None)
      else Some (fs', y2'))
    s.contents_open;
  if closed_y
  then Hashtbl.add s.contents_closed i y
  else Hashtbl.add s.contents_open i (frees_y, y);
  s.frees <- Set.union (Set.remove i s.frees) frees_y

let add_disjoint (i, y) s =
  (*
  assert (not (Hashtbl.mem s.contents_open i));
  assert (not (Hashtbl.mem s.contents_closed i));
 *)
  let frees_y = substitute_frees y in
  if Set.is_empty frees_y
  then Hashtbl.add s.contents_closed i y
  else Hashtbl.add s.contents_open i (frees_y, y);
  s.frees <- Set.union (Set.remove i s.frees) frees_y

let add (i, y) s =
  if Set.mem i s.frees then add (i, y) s else add_disjoint (i, y) s

let insert xs s = List.iter (fun (i, y) -> add (i, y) s) xs
