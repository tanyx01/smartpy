(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Utils
open Basics
open Control

type t = value [@@deriving show {with_path = false}]

let rec equal x y =
  match (x.v, y.v) with
  | Literal x, Literal y | Bounded (_, x), Bounded (_, y) -> Literal.equal x y
  | Record x, Record y ->
      List.equal (fun (lx, vx) (ly, vy) -> lx = ly && equal vx vy) x y
  | Variant (cx, vx), Variant (cy, vy) -> cx = cy && equal vx vy
  | Tuple xs, Tuple ys -> List.equal equal xs ys
  | List _, List _
  | Set _, Set _
  | Map _, Map _
  | Closure _, Closure _
  | Operation _, Operation _
  | Contract _, Contract _
  | Ticket _, Ticket _ ->
      (* Not comparable. *)
      assert false
  | ( ( Literal _
      | Bounded _
      | Contract _
      | Record _
      | Variant _
      | List _
      | Set _
      | Map _
      | Tuple _
      | Closure _
      | Operation _
      | Ticket _ )
    , ( Literal _
      | Bounded _
      | Contract _
      | Record _
      | Variant _
      | List _
      | Set _
      | Map _
      | Tuple _
      | Closure _
      | Operation _
      | Ticket _ ) ) ->
      (* Comparison of different constructors indicates a typing error. *)
      assert false

let rec compare t x y =
  match (x.v, y.v) with
  | Literal x, Literal y | Bounded (_, x), Bounded (_, y) -> Literal.compare x y
  | Record x, Record y ->
      let layout, row =
        match Type.unF t with
        | TRecord {layout; row} -> (Hole.get_value layout, row)
        | _ -> assert false
      in
      Binary_tree.compare
        (fun (lx, vx) (ly, vy) ->
          assert (lx = ly);
          let t =
            match List.assoc_opt lx.Layout.source row with
            | Some t -> t
            | None -> assert false
          in
          compare t vx vy)
        (Layout.on_row x layout) (Layout.on_row y layout)
  | Variant (cx, vx), Variant (cy, vy) -> (
      let layout, row =
        match Type.view_variant t with
        | Some (layout, row) -> (Hole.get_value layout, row)
        | _ -> assert false
      in
      let get_ix c =
        List.find_ix c
          (List.map
             (fun Layout.{source} -> source)
             (Binary_tree.to_list layout))
      in
      match Stdlib.compare (get_ix cx) (get_ix cy) with
      | 0 ->
          let t =
            match List.assoc_opt cx row with
            | Some t -> t
            | None -> assert false
          in
          compare t vx vy
      | c -> c)
  | Tuple xs, Tuple ys ->
      let ts =
        match Type.unF t with
        | TTuple (ts, None) -> ts
        | _ -> assert false
      in
      let xs = List.combine ts xs in
      let ys = List.combine ts ys in
      List.compare (fun ((_, t), x) (_, y) -> compare t x y) xs ys
  | List _, List _
  | Set _, Set _
  | Map _, Map _
  | Closure _, Closure _
  | Operation _, Operation _
  | Contract _, Contract _
  | Ticket _, Ticket _ ->
      (* Not comparable. *)
      assert false
  | ( ( Literal _
      | Bounded _
      | Contract _
      | Record _
      | Variant _
      | List _
      | Set _
      | Map _
      | Tuple _
      | Closure _
      | Operation _
      | Ticket _ )
    , ( Literal _
      | Bounded _
      | Contract _
      | Record _
      | Variant _
      | List _
      | Set _
      | Map _
      | Tuple _
      | Closure _
      | Operation _
      | Ticket _ ) ) ->
      (* Comparison of different constructors indicates a typing error. *)
      assert false

let literal l = build_value (Literal l)

let bounded cases x = build_value (Bounded (cases, x))

let int x = literal (Literal.int x)

let nat x = literal (Literal.nat x)

let intOrNat t x = literal (Literal.intOrNat t x)

let mutez i = literal (Literal.mutez i)

let timestamp i = literal (Literal.timestamp i)

let int_of_value v =
  match v.v with
  | Literal (Int {i}) -> Big_int.int_of_big_int i
  | _ ->
      raise
        (SmartExcept
           [`Text "Cannot convert value"; `Text (show v); `Text "into int."])

let bool_of_value v =
  match v.v with
  | Literal (Bool x) -> x
  | _ ->
      raise
        (SmartExcept
           [`Text "Cannot convert value"; `Text (show v); `Text "into bool."])

let lt t v1 v2 = compare t v1 v2 < 0

let le t v1 v2 = compare t v1 v2 <= 0

let openV x = x.v

let string s = literal (Literal.string s)

let bytes s = literal (Literal.bytes s)

let bls12_381_g1 s = literal (Literal.bls12_381_g1 s)

let bls12_381_g2 s = literal (Literal.bls12_381_g2 s)

let bls12_381_fr s = literal (Literal.bls12_381_fr s)

let chest_key b = literal (Literal.chest_key b)

let chest b = literal (Literal.chest b)

let chain_id s = literal (Literal.chain_id s)

let unString ~pp = function
  | {v = Literal (String s)} -> s
  | x ->
      raise
        (SmartExcept
           [`Text (show x); `Text "is not a string"; `Br; `Rec (pp ())])

let operation op = build_value (Operation op)

let build_list (l : t list) = build_value (List l)

let ticket ticketer content amount =
  build_value (Ticket (ticketer, content, amount))

let ordered_type t =
  (module struct
    type t = value

    let compare = compare t
  end : Set.OrderedType
    with type t = value)

let set ~telement xs =
  let module VSet = Set.Make ((val ordered_type telement)) in
  let xs = VSet.(elements (of_list xs)) in
  build_value (Set xs)

let map ~tkey xs =
  let module VMap = Map.Make ((val ordered_type tkey)) in
  let xs = VMap.(to_list (of_list xs)) in
  build_value (Map xs)

let closure lambda args = build_value (Closure (lambda, args))

let unit = literal Literal.unit

let bool x = literal (Literal.bool x)

let unBool ~pp = function
  | {v = Literal (Bool b)} -> b
  | x ->
      raise
        (SmartExcept [`Text (show x); `Text "is not a bool"; `Br; `Rec (pp ())])

let unMap ~pp = function
  | {v = Map l} -> l
  | x ->
      raise
        (SmartExcept [`Text (show x); `Text "is not a map"; `Br; `Rec (pp ())])

let unOption v =
  match v.v with
  | Variant ("Some", arg) -> Some arg
  | Variant ("None", _) -> None
  | _ -> assert false

let unInt ~pp = function
  | {v = Literal (Int {i})} -> i
  | x ->
      raise
        (SmartExcept
           [`Text (show x); `Text "is not an integer"; `Br; `Rec (pp ())])

let unMutez ~pp = function
  | {v = Literal (Mutez b)} -> b
  | x ->
      raise
        (SmartExcept [`Text (show x); `Text "is not a mutez"; `Br; `Rec (pp ())])

let unTimestamp ~pp = function
  | {v = Literal (Timestamp t)} -> t
  | x ->
      raise
        (SmartExcept
           [`Text (show x); `Text "is not a timestamp"; `Br; `Rec (pp ())])

let unChain_id ~pp = function
  | {v = Literal (Chain_id b)} -> b
  | x ->
      raise
        (SmartExcept
           [`Text (show x); `Text "is not a chain_id"; `Br; `Rec (pp ())])

let unAddress ~pp = function
  | {v = Literal (Address {address})} -> address
  | x ->
      raise
        (SmartExcept
           [`Text (show x); `Text "is not an address"; `Br; `Rec (pp ())])

let unKey_hash ~pp = function
  | {v = Literal (Key_hash b)} -> b
  | x ->
      raise
        (SmartExcept
           [`Text (show x); `Text "is not an key_hash"; `Br; `Rec (pp ())])

let un_baker = function
  | {v = Variant ("Some", {v = Literal (Key_hash b)})} -> Some b
  | {v = Variant ("None", _)} -> None
  | _ -> assert false

let plus ~primitives tx x y =
  let module P = (val primitives : Primitives.Primitives) in
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      let isNat =
        match Type.unF tx with
        | TInt {isNat} -> isNat
        | _ -> assert false
      in
      intOrNat isNat (Big_int.add_big_int i j)
      (* TODO type *)
  | Literal (Mutez x), Literal (Mutez y) -> mutez (Big_int.add_big_int x y)
  | Literal (String x), Literal (String y) -> string (x ^ y)
  | Literal (Bytes x), Literal (Bytes y) -> bytes (x ^ y)
  | Literal (Bls12_381_g1 x), Literal (Bls12_381_g1 y) ->
      bls12_381_g1
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.addG1 Hex.(show (of_string x)) Hex.(show (of_string y))
              with e ->
                Printf.ksprintf failwith "add(G1, G1): %s"
                  (Base.Exn.to_string e))))
  | Literal (Bls12_381_g2 x), Literal (Bls12_381_g2 y) ->
      bls12_381_g2
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.addG2 Hex.(show (of_string x)) Hex.(show (of_string y))
              with e ->
                Printf.ksprintf failwith "add(G2, G2): %s"
                  (Base.Exn.to_string e))))
  | Literal (Bls12_381_fr x), Literal (Bls12_381_fr y) ->
      bls12_381_fr
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.addFr Hex.(show (of_string x)) Hex.(show (of_string y))
              with e ->
                Printf.ksprintf failwith "add(Fr, Fr): %s"
                  (Base.Exn.to_string e))))
  | _ ->
      raise
        (SmartExcept
           [
             `Text "Invalid + operation with different types "
           ; `Text (show x)
           ; `Text (show y)
           ])

let mul ~primitives tx x y =
  let module P = (val primitives : Primitives.Primitives) in
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      let isNat =
        match Type.unF tx with
        | TInt {isNat} -> isNat
        | _ -> assert false
      in
      intOrNat isNat (Big_int.mult_big_int i j)
      (* TODO type *)
  | Literal (Int {i}), Literal (Bls12_381_fr fr)
  | Literal (Bls12_381_fr fr), Literal (Int {i}) ->
      bls12_381_fr
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.multiplyFrByInt
                  Hex.(show (of_string fr))
                  (Big_int.string_of_big_int i)
              with e ->
                Printf.ksprintf failwith "multiplyFrByInt: %s"
                  (Base.Exn.to_string e))))
  | Literal (Bls12_381_g1 x), Literal (Bls12_381_fr fr) ->
      bls12_381_g1
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.multiplyG1ByFr
                  Hex.(show (of_string x))
                  Hex.(show (of_string fr))
              with e ->
                Printf.ksprintf failwith "multiplyG1ByFr: %s"
                  (Base.Exn.to_string e))))
  | Literal (Bls12_381_g2 x), Literal (Bls12_381_fr fr) ->
      bls12_381_g2
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.multiplyG2ByFr
                  Hex.(show (of_string x))
                  Hex.(show (of_string fr))
              with e ->
                Printf.ksprintf failwith "multiplyG2ByFr: %s"
                  (Base.Exn.to_string e))))
  | Literal (Bls12_381_fr x), Literal (Bls12_381_fr fr) ->
      bls12_381_fr
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.multiplyFrByFr
                  Hex.(show (of_string x))
                  Hex.(show (of_string fr))
              with e ->
                Printf.ksprintf failwith "multiplyFrByFr: %s"
                  (Base.Exn.to_string e))))
  | Literal (Mutez x), Literal (Int {i}) | Literal (Int {i}), Literal (Mutez x)
    -> mutez (Big_int.mult_big_int x i)
  | _ -> failwith "Invalid * operation with different types"

let add ~primitives tx x y =
  let module P = (val primitives : Primitives.Primitives) in
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      let isNat =
        match Type.unF tx with
        | TInt {isNat} -> isNat
        | _ -> assert false
      in
      intOrNat isNat (Big_int.add_big_int i j)
      (* TODO type *)
  | Literal (Bls12_381_g1 x), Literal (Bls12_381_g1 y) ->
      bls12_381_g1
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.addG1 Hex.(show (of_string x)) Hex.(show (of_string y))
              with e ->
                Printf.ksprintf failwith "addG1: %s" (Base.Exn.to_string e))))
  | Literal (Bls12_381_g2 x), Literal (Bls12_381_g2 y) ->
      bls12_381_g2
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.addG2 Hex.(show (of_string x)) Hex.(show (of_string y))
              with e ->
                Printf.ksprintf failwith "addG2: %s" (Base.Exn.to_string e))))
  | Literal (Bls12_381_fr x), Literal (Bls12_381_fr y) ->
      bls12_381_fr
        (Hex.to_string
           (`Hex
             (try
                P.Bls12.addFr Hex.(show (of_string x)) Hex.(show (of_string y))
              with e ->
                Printf.ksprintf failwith "addFr: %s" (Base.Exn.to_string e))))
  | Literal (Int {i}), Literal (Timestamp t) ->
      timestamp (Big_int.add_big_int i t)
  | Literal (Timestamp t), Literal (Int {i}) ->
      timestamp (Big_int.add_big_int i t)
  | Literal (Mutez x), Literal (Mutez y) -> mutez (Big_int.add_big_int x y)
  | _ -> failwith "Invalid sp.add operation with incompatible types"

let shift_left x y =
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      if Bigint.compare j (Bigint.of_int 1000000) > 1
      then
        raise
          (SmartExcept
             [
               `Text "shift_left with too big shift value"
             ; `Text (show x)
             ; `Text "<<"
             ; `Text (show y)
             ]);
      nat (Big_int.shift_left_big_int i (Big_int.int_of_big_int j))
  | _ -> failwith "Invalid << operation with different types"

let shift_right x y =
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      if Bigint.compare j (Bigint.of_int 1000000) > 1
      then
        raise
          (SmartExcept
             [
               `Text "shift_left with too big shift value"
             ; `Text (show x)
             ; `Text ">>"
             ; `Text (show y)
             ]);
      nat (Big_int.shift_right_big_int i (Big_int.int_of_big_int j))
  | _ -> failwith "Invalid >> operation with different types"

let xor x y =
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) -> nat (Big_int.xor_big_int i j)
  | Literal (Bool i), Literal (Bool j) -> bool (i <> j)
  | _ -> failwith "Invalid xor operation with different types"

let e_mod tx x y =
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      let isNat =
        match Type.unF tx with
        | TInt {isNat} -> isNat
        | _ -> assert false
      in
      intOrNat isNat (Big_int.mod_big_int i j)
      (* TODO type *)
  | _ -> failwith "Invalid * operation with different types"

let div_inner x y =
  match (x.v, y.v) with
  | Literal (Int {i = x}), Literal (Int {i = y}) ->
      nat (Big_int.div_big_int x y) (* TODO type *)
  | _ -> failwith "Invalid / operation with different types"

let div x y = div_inner x y

let key_hash s = literal (Literal.key_hash s)

let key s = literal (Literal.key s)

let secret_key s = literal (Literal.secret_key s)

let signature s = literal (Literal.signature s)

let record = function
  | [] -> unit
  | l -> build_value (Record l)

let tuple vs = build_value (Tuple vs)

let un_record = function
  | {v = Record bs} -> bs
  | _ -> failwith "unRecord"

let variant cons arg = build_value (Variant (cons, arg))

let none = build_value (Variant ("None", unit))

let some x = build_value (Variant ("Some", x))

let left x = build_value (Variant ("Left", x))

let right y = build_value (Variant ("Right", y))

let option = Option.cata none some

let ediv tx ty x y =
  let ediv x y ~a_f ~b_f =
    if Big_int.eq_big_int Big_int.zero_big_int y
    then none
    else
      some
        (tuple [a_f (Big_int.div_big_int x y); b_f (Big_int.mod_big_int x y)])
  in
  match (x.v, y.v) with
  | Literal (Int {i = x_}), Literal (Int {i = y_}) ->
      let both_nat =
        match (Type.unF tx, Type.unF ty) with
        | TInt {isNat = isNat1}, TInt {isNat = isNat2} -> (
            match (Hole.get isNat1, Hole.get isNat2) with
            | Some true, Some true -> true
            | (Some _ | None), (Some _ | None) -> false)
        | _, _ -> assert false
      in
      if both_nat
      then ediv x_ y_ ~a_f:nat ~b_f:nat
      else ediv x_ y_ ~a_f:int ~b_f:nat
  | Literal (Mutez x_), Literal (Mutez y_) ->
      (* tez -> tez -> (nat * tez) *)
      ediv x_ y_ ~a_f:nat ~b_f:mutez
  | Literal (Mutez x_), Literal (Int {i = y_}) ->
      (* tez -> nat -> (tez * tez) *)
      ediv x_ y_ ~a_f:mutez ~b_f:mutez
  | _ -> raise (SmartExcept [`Text "Invalid / operation with different types"])

let intXor a b =
  match (openV a, openV b) with
  | Literal (Int _a), Literal (Int _b) -> assert false (*int (a lxor b)*)
  | _ -> failwith "Invalid intXor operation"

let sub_mutez x y =
  let open Bigint in
  match (x.v, y.v) with
  | Literal (Mutez x), Literal (Mutez y) ->
      let r = sub_big_int x y in
      if lt_big_int zero_big_int r then some (mutez r) else none
  | _ -> assert false

let address ?entry_point s = literal (Literal.address ?entry_point s)

let contract ?entry_point address =
  build_value (Contract {address; entrypoint = entry_point})

let cons x l =
  match l with
  | {v = List l} -> build_value (List (x :: l))
  | _ -> failwith "Type error list"

let rec zero_of_type t =
  match Type.unF t with
  | T0 T_unit -> unit
  | T0 T_bool -> bool false
  | TInt {isNat} -> (
      match Typing.intType isNat with
      | `Nat -> nat Big_int.zero_big_int
      | `Int -> int Big_int.zero_big_int
      | `Unknown -> int Big_int.zero_big_int)
  | TBounded {cases} -> (
      match cases with
      | [] -> assert false
      | case :: _ -> literal case)
  | T0 T_timestamp -> timestamp Big_int.zero_big_int
  | T0 T_string -> string ""
  | T0 T_bytes -> bytes ""
  | TRecord {var = None; row} ->
      record ((List.map (fun (lbl, t) -> (lbl, zero_of_type t))) row)
  | TVariant {var = None; row = []} ->
      failwith "zero_of_type: empty variant type"
  | TVariant {var = None; row = (cons, t0) :: _} ->
      variant cons (zero_of_type t0)
  | T1 (T_set, telement) -> set ~telement []
  | T2 ((T_map | T_big_map), tkey, _tvalue) -> map ~tkey []
  | T0 T_address -> address ""
  | T0 T_key_hash -> key_hash ""
  | T0 T_key -> key ""
  | T0 T_signature -> signature ""
  | T0 T_mutez -> mutez Big_int.zero_big_int
  | TUnknown _
  | TRecord {var = Some _}
  | TVariant {var = Some _}
  | TTuple (_, Some _) -> failwith "zero_of_type: unknown"
  | TTuple (ts, None) -> tuple (List.map (fun (_, t) -> zero_of_type t) ts)
  | T1 (T_list, _) -> build_list []
  | T0 T_chain_id -> chain_id ""
  | TSecretKey -> secret_key ""
  | TLambda _ | T1 (T_contract, _) | T2 (T_lambda, _, _) ->
      raise
        (SmartExcept [`Text "zero_of_type not implemented on type"; `Type t])
  | T0 T_operation -> failwith "zero_of_type: operation"
  | TSaplingState _ -> failwith "zero_of_type: sapling_state"
  | TSaplingTransaction _ -> failwith "zero_of_type: sapling_transaction"
  | T0 T_never -> failwith "zero_of_type: never"
  | T1 (T_ticket, t) -> ticket "" (zero_of_type t) (Bigint.of_int 0)
  | T0 T_bls12_381_g1 -> bls12_381_g1 "0x00"
  | T0 T_bls12_381_g2 -> bls12_381_g2 "0x00"
  | T0 T_bls12_381_fr -> bls12_381_fr "0x00"
  | T0 T_chest_key -> chest_key "0x00"
  | T0 T_chest -> chest "0x00"
  | T0 (T_nat | T_int | T_sapling_state _ | T_sapling_transaction _)
  | T1 (T_option, _)
  | T2 ((T_pair _ | T_or _), _, _) -> assert false

let nextId prefix =
  let ids = ref 0 in
  fun () ->
    incr ids;
    Printf.sprintf "%s%i" prefix !ids

let closure_init l = build_value (Closure (l, []))

let closure_apply v x =
  match v with
  | {v = Closure (l, args)} -> build_value (Closure (l, x :: args))
  | _ -> failwith "closure_apply: not a closure"

let unclosure ~pp = function
  | {v = Closure (l, args)} -> (l, args)
  | x ->
      raise
        (SmartExcept
           [`Text (show x); `Text "is not a closure"; `Br; `Rec (pp ())])

let extract_literals ~config =
  let module Printer = (val Printer.get_by_language ~config Config.SmartPy
                          : Printer.Printer)
  in
  let f _t v path =
    match v with
    | Literal l | Bounded (_, l) -> [(l, path)]
    | Record xs -> List.concat_map (fun (lbl, (_, v)) -> v (lbl :: path)) xs
    | Variant (c, (_, x)) -> x (c :: path)
    | Map xs ->
        List.concat_map
          (fun ((_, k), (v_orig, v)) ->
            k ("key" :: path) @ v (Printer.value_to_string v_orig :: path))
          xs
    | Tuple [(_, x); (_, y)] -> x ("fst" :: path) @ y ("snd" :: path)
    | Tuple xs ->
        List.concat
          (List.mapi (fun i (_, x) -> x (("#" ^ string_of_int i) :: path)) xs)
    | (List _ | Set _ | Contract _ | Closure _ | Operation _ | Ticket _) as v ->
        fold_value_f (fun acc (_, v) -> acc @ v []) [] v
  in
  fun v -> para_tvalue f v []

let rec get_field_opt field v =
  match v.tv with
  | Record l -> List.assoc_opt field l
  | Tuple vs ->
      let rec find = function
        | [] -> None
        | x :: xs -> (
            match get_field_opt field x with
            | None -> find xs
            | Some x -> Some x)
      in
      find vs
  | _ -> None

let unPAIRn = function
  (* "pair" is necessary (tzstats KT1GgUJwMQoFayRYNwamRAYCvHBLzgorLoGo) *)
  | Sequence [m1; m2] ->
      Primitive {name = "Pair"; annotations = []; arguments = [m1; m2]}
  | Sequence (m1 :: rest)
  | Primitive {name = "Pair" | "pair"; arguments = m1 :: (_ :: _ :: _ as rest)}
    ->
      Primitive
        {
          name = "Pair"
        ; annotations = []
        ; arguments =
            [m1; Primitive {name = "Pair"; annotations = []; arguments = rest}]
        }
  | Primitive {name = "pair"; arguments} ->
      Primitive {name = "Pair"; annotations = []; arguments}
  | x -> x

let of_micheline ~config ~pp_mich =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let rec parse_record (layout : Layout.t) row m =
    let rec aux layout m =
      match (layout : _ Binary_tree.t) with
      | Leaf Layout.{source = a} ->
          let t = List.assoc a row in
          [(a, to_value t m)]
      | Node (l1, l2) -> (
          match unPAIRn m with
          | Primitive {name = "Pair"; arguments = [m1; m2]} ->
              let r1 = aux l1 m1 in
              let r2 = aux l2 m2 in
              r1 @ r2
          | _ ->
              Printf.ksprintf failwith "parse_record %s %s" (Layout.show layout)
                (Micheline.pretty "" m))
    in
    aux layout m
  and parse_variant (layout : Layout.t) row m =
    let rec aux layout m =
      match (layout, m) with
      | Binary_tree.Leaf Layout.{source = a}, m ->
          let t = List.assoc a row in
          (a, to_value t m)
      | Binary_tree.Node (l1, _l2), Primitive {name = "Left"; arguments = [m]}
        -> aux l1 m
      | Binary_tree.Node (_l1, l2), Primitive {name = "Right"; arguments = [m]}
        -> aux l2 m
      | _ ->
          Printf.ksprintf failwith "parse_variant %s %s" (Layout.show layout)
            (Micheline.pretty "" m)
    in
    aux layout m
  and to_value t m =
    let err () =
      Printf.ksprintf failwith "to_value %s %s" (Printer.type_to_string t)
        (Micheline.pretty "" m)
    in
    match (Type.unF t, m) with
    | TTuple ([(0, t1); (1, t2)], None), _ -> (
        match unPAIRn m with
        | Primitive {name = "Pair"; arguments = [x1; x2]} ->
            tuple [to_value t1 x1; to_value t2 x2]
        | _ ->
            Printf.ksprintf failwith "to_value tuple || %s || %s ||"
              (Printer.type_to_string t) (Micheline.pretty "" m))
    | ( TVariant {row = [("Left", t1); ("Right", _t2)]}
      , Primitive {name = "Left"; arguments = [x]} ) ->
        variant "Left" (to_value t1 x)
    | ( TVariant {row = [("Left", _t1); ("Right", t2)]}
      , Primitive {name = "Right"; arguments = [x]} ) ->
        variant "Right" (to_value t2 x)
    | ( TVariant {row = [("None", F (T0 T_unit)); ("Some", t)]}
      , Primitive {name = "Some"; arguments = [x]} ) -> some (to_value t x)
    | ( TVariant {row = [("None", F (T0 T_unit)); ("Some", _t)]}
      , Primitive {name = "None"; arguments = []} ) -> none
    | TVariant {layout; row}, m -> (
        match Hole.get layout with
        | Some l ->
            let name, v = parse_variant l row m in
            variant name v
        | None -> err ())
    | ( (T0 T_unit | TRecord {row = []})
      , Primitive {name = "Unit"; arguments = []} ) -> unit
    | TRecord {row = []}, _ -> unit
    | TRecord {layout; row}, m -> (
        match Hole.get layout with
        | Some layout ->
            let entries = parse_record layout row m in
            record entries
        | None -> err ())
    | T0 T_bool, Primitive {name = "False"} -> bool false
    | T0 T_bool, Primitive {name = "True"} -> bool true
    | T0 T_mutez, (Int i | String i) -> mutez (Bigint.of_string ~msg:"value" i)
    | T0 T_timestamp, (Int i | String i) ->
        let i =
          if Base.String.contains i 'Z' then SmartDom.parseDate i else i
        in
        timestamp (Bigint.of_string ~msg:"value" i)
        (* TODO *)
    | TInt _, (Int i | String i) ->
        let isNat =
          match Type.unF t with
          | TInt {isNat} -> isNat
          | _ -> assert false
        in
        intOrNat isNat (Bigint.of_string ~msg:"value" i)
    | T0 T_string, String i -> string i
    | T0 T_key, String i -> key i
    | T0 T_key, Bytes b -> key (Utils.Bs58.encode_key Hex.(show (of_string b)))
    | T0 T_bls12_381_g1, String i -> key i
    | T0 T_bls12_381_g2, String i -> key i
    | T0 T_bls12_381_fr, String i -> key i
    | T0 T_bls12_381_g1, Bytes i -> bls12_381_g1 i
    | T0 T_bls12_381_g2, Bytes i -> bls12_381_g2 i
    | T0 T_bls12_381_fr, Bytes i -> bls12_381_fr i
    | T0 T_signature, String i -> signature i
    | T0 T_signature, Bytes b ->
        signature (Utils.Bs58.encode_signature Hex.(show (of_string b)))
    | T0 T_bytes, String i -> bytes (Hex.to_string (`Hex i))
    | T0 T_bytes, Bytes i -> bytes i
    | T0 T_chain_id, String i -> chain_id (Hex.to_string (`Hex i))
    | T0 T_chain_id, Bytes i -> chain_id i
    | T0 T_key_hash, String i -> key_hash i
    | T0 T_key_hash, Bytes b ->
        key_hash (Utils.Bs58.encode_key_hash Hex.(show (of_string b)))
    | T1 (T_contract, _), String i -> contract i
    | T1 (T_contract, _), Bytes b ->
        contract (Utils.Bs58.encode_address Hex.(show (of_string b)))
    | T0 T_address, String i -> address i
    | T0 T_address, Bytes a ->
        address (Utils.Bs58.encode_address Hex.(show (of_string a)))
    | T1 (T_list, elt), Sequence l -> build_list (List.map (to_value elt) l)
    | T1 (T_option, t), Primitive {name = "Some"; arguments = [x]} ->
        some (to_value t x)
    | T1 (T_option, _t), Primitive {name = "None"; arguments = []} -> none
    | T2 ((T_map | T_big_map), tkey, tvalue), Sequence l ->
        let pairOfStorage = function
          | Primitive {name = "Elt"; arguments = [k; v]} ->
              (to_value tkey k, to_value tvalue v)
          | p ->
              failwith
                (Printf.sprintf "Bad map pair %s " (Micheline.pretty "" p))
        in
        map ~tkey (List.map pairOfStorage l)
    | T2 ((T_map | T_big_map), tkey, _), Int _ ->
        (* show empty big maps *)
        map ~tkey []
    | T1 (T_set, telement), Sequence l ->
        set ~telement (List.map (to_value telement) l)
    | ( T1 (T_ticket, t)
      , Primitive
          {name = "Pair"; arguments = [String ticketer; content; Int amount]} )
      ->
        let ticketer = ticketer in
        let content = to_value t content in
        let amount = Bigint.of_string ~msg:"value" amount in
        ticket ticketer content amount
    | ( T1 (T_ticket, t)
      , Primitive
          {
            name = "Pair"
          ; arguments =
              [
                Bytes ticketer
              ; Primitive {name = "Pair"; arguments = [content; Int amount]}
              ]
          } ) ->
        let ticketer =
          Utils.Bs58.encode_address Hex.(show (of_string ticketer))
        in
        let content = to_value t content in
        let amount = Bigint.of_string ~msg:"value" amount in
        ticket ticketer content amount
    | T2 (T_lambda, _, _), _ -> assert false
    | TLambda (effects, t1, t2), x ->
        assert (not (Type.has_effects effects));
        let line_no = [] in
        let name = "x" in
        let params = Expr.lambda_params ~line_no name in
        let mich =
          let name = pp_mich t1 x in
          {name; parsed = x; typesIn = [t1]; typesOut = [t2]}
        in
        let mich = Expr.inline_michelson ~line_no mich [params] in
        let body = Command.result ~line_no mich in
        let lambda =
          {
            name
          ; body
          ; clean_stack = false
          ; with_storage = None
          ; with_operations = false
          ; recursive = None
          ; derived = U
          }
        in
        let config = Config.default in
        let lambda = Checker.check_lambda config [] lambda in
        closure_init lambda
    | TSaplingState {memo}, _ -> (
        match Hole.get memo with
        | Some memo -> literal (Literal.sapling_state_real memo)
        | None ->
            Printf.ksprintf failwith "to_value (sapling) %s %s"
              (Printer.type_to_string t) (Micheline.pretty "" m))
    | _ -> err ()
  in
  to_value

let typecheck_operation_f (op : (Type.t -> tvalue) operation_f) =
  match op with
  | Transfer {params; destination; amount} ->
      let params = params destination.type_ in
      Transfer {params; destination; amount}
  | CreateContract {id; instance = {template; state = {storage} as state}} ->
      let tstorage = (get_extra template.tcontract.derived).tstorage in
      let storage = Option.map (fun t -> t tstorage) storage in
      let state = {state with storage} in
      CreateContract {id; instance = {template; state}}
  | SetDelegate _ as op -> op
  | Event (tag, ty, x) -> Event (tag, ty, x ty)

(* NB The large number of 'assert (Type.equal ...)' below are
   indicative of redundancy in 'value'. *)
let typecheck_f (tv : (Type.t -> tvalue) value_f) =
  let msg = "Value.typecheck_f" in
  match tv with
  | Literal l as tv ->
      fun t ->
        if false then assert (Type.equal t (Type.type_of_literal l));
        {t; tv}
  | Bounded (_, l) as tv -> (
      function
      | F (TBounded {base}) as t ->
          (* TODO check cases *)
          if false then assert (Type.equal base (Type.type_of_literal l));
          {t; tv}
      | _ -> assert false)
  | Contract _ as tv -> (
      function
      | F (T1 (T_contract, _)) as t -> {t; tv}
      | _ -> assert false)
  | Record entries -> (
      function
      | F (TRecord {row}) as t ->
          let entries =
            let f (fld, v) =
              let t = Option.of_some ~msg (List.assoc_opt fld row) in
              (fld, v t)
            in
            List.map f entries
          in
          {t; tv = Record entries}
      | _ -> assert false)
  | Variant (cons, arg) -> (
      fun t ->
        match Type.view_variant t with
        | None -> assert false
        | Some (_, row) ->
            let t_arg = Option.of_some ~msg (List.assoc_opt cons row) in
            let arg = arg t_arg in
            {t; tv = Variant (cons, arg)})
  | List xs -> (
      function
      | F (T1 (T_list, tx)) as t ->
          let xs = List.map (fun x -> x tx) xs in
          {t; tv = List xs}
      | _ -> assert false)
  | Set xs -> (
      function
      | F (T1 (T_set, tx)) as t ->
          let xs = List.map (fun x -> x tx) xs in
          {t; tv = Set xs}
      | _ -> assert false)
  | Map entries -> (
      function
      | F (T2 ((T_map | T_big_map), tk, tv)) as t ->
          let entries = List.map (fun (k, v) -> (k tk, v tv)) entries in
          {t; tv = Map entries}
      | _ -> assert false)
  | Tuple xs -> (
      function
      | F (TTuple (ts, None)) as t ->
          let xs = List.map2 (fun x (_, t) -> x t) xs ts in
          {t; tv = Tuple xs}
      | _ -> assert false)
  | Closure (({with_storage = Some _} | {with_operations = true}), _ :: _) ->
      assert false
  | Closure (l, []) -> (
      let {tParams; tResult} = get_extra l.derived in
      function
      | F (TLambda (_, a, b)) as t ->
          if false then assert (Type.equal tParams a);
          if false then assert (Type.equal tResult b);
          {t; tv = Closure (l, [])}
      | _ -> assert false)
  | Closure
      ( ({
           with_operations = false
         ; with_storage = None
         ; derived = T {tParams = b; tResult}
         } as l)
      , xs ) -> (
      let rec apply r t xs =
        match (t, xs) with
        | t, [] -> (t, r)
        | Type.F (TTuple ([(0, a1); (1, a2)], _)), x :: xs ->
            let x = x a1 in
            apply (x :: r) a2 xs
        | _ -> assert false
      in
      let a', xs = apply [] b (List.rev xs) in
      function
      | F (TLambda (effects, a, b)) as t ->
          assert (Hole.get effects.with_storage = Some None);
          assert (Hole.get effects.with_operations = Some false);
          assert (Type.equal a a');
          assert (Type.equal b tResult);
          {t; tv = Closure (l, xs)}
      | _ -> assert false)
  | Closure _ -> assert false
  | Operation op -> (
      function
      | F (T0 T_operation) ->
          let op = typecheck_operation_f op in
          {t = Type.operation; tv = Operation op}
      | _ -> assert false)
  | Ticket (s, v, i) -> (
      function
      | F (T1 (T_ticket, tx)) as t ->
          let v = v tx in
          {t; tv = Ticket (s, v, i)}
      | _ -> assert false)

let typecheck t v = cata_value typecheck_f v t

(** Reifies records and variants according to the specified type. *)
let rec smartMLify t_to (v : value) =
  let msg = "Value.smartMLify_f" in
  match (Type.unF t_to, v.v) with
  | TRecord {row = [(fld, t)]}, _ -> {v = Record [(fld, smartMLify t v)]}
  | TRecord {layout; row}, Tuple vs ->
      let layout = Option.of_some ~msg (Hole.get layout) in
      let ts =
        List.map
          (fun ({source} : Layout.l) ->
            (source, Option.of_some ~msg (List.assoc_opt source row)))
          (Binary_tree.to_list layout)
      in
      let entries = List.map2 (fun v (fld, t) -> (fld, smartMLify t v)) vs ts in
      {v = Record entries}
  | T1 (T_option, _), _ -> v
  | T2 (T_or _, _, _), _ -> v
  | TVariant {layout; row}, _ ->
      let layout = Option.of_some ~msg (Hole.get layout) in
      let cases =
        Binary_tree.map
          (fun ({source} : Layout.l) ->
            (source, Option.of_some ~msg (List.assoc_opt source row)))
          layout
      in
      let rec find_case cases v =
        let open Binary_tree in
        match (cases, v.v) with
        | Leaf (case, t), _ -> {v = Variant (case, smartMLify t v)}
        | Node (l, _), Variant ("Left", x) -> find_case l x
        | Node (_, r), Variant ("Right", x) -> find_case r x
        | _ -> assert false
      in
      find_case cases v
  | _, ((Literal _ | Bounded _ | Contract _ | Operation _) as v) -> {v}
  | T1 (T_list, t), List xs ->
      {v = List (List.map (fun x -> smartMLify t x) xs)}
  | T1 (T_set, t), Set xs -> {v = Set (List.map (fun x -> smartMLify t x) xs)}
  | T2 (T_map, tk, tv), Map xs ->
      let xs = List.map (fun (k, v) -> (smartMLify tk k, smartMLify tv v)) xs in
      {v = Map xs}
  | TTuple (ts, None), Tuple xs ->
      {v = Tuple (List.map2 (fun (_, t) x -> smartMLify t x) ts xs)}
  | _, Closure _ -> (* TODO *) assert false
  | _, Ticket _ -> (* TODO *) assert false
  | _ -> failwith ("FAIL: " ^ Type.show t_to ^ "\n" ^ show v)

let erase_types = erase_types_value

let typecheck_instance i =
  let tstorage = (get_extra i.template.tcontract.derived).tstorage in
  map_instance_f (typecheck tstorage) i

module Typed = struct
  let mk t v =
    let v = map_value_f (fun v _t -> v) v in
    typecheck_f v t

  let bool x = typecheck Type.bool (bool x)

  let string x = typecheck Type.string (string x)

  let mutez x = typecheck Type.mutez (mutez x)

  let address x = typecheck Type.address (address x)

  let tuple xs =
    let ts = List.map (fun {t} -> t) xs in
    mk (Type.tuple ts) (Tuple xs)

  let list t xs = mk (Type.list t) (List xs)

  let record xs =
    let row = List.map (fun (lbl, v) -> (lbl, v.t)) xs in
    let layout = Type.comb_layout `Right (List.map fst row) in
    mk (Type.record (Hole.value layout) row) (Record xs)

  let variant t cons arg = mk t (Variant (cons, arg))

  let of_micheline ~config ~pp_mich t x =
    typecheck t (of_micheline ~config ~pp_mich t x)
end

let minus x y =
  match (openV x, openV y) with
  | Literal (Int {i = x}), Literal (Int {i = y}) ->
      int (Big_int.sub_big_int x y) (* TODO type *)
  | Literal (Mutez x_), Literal (Mutez y_) ->
      let res = Big_int.sub_big_int x_ y_ in
      if Bigint.compare res Bigint.zero_big_int < 0
      then
        raise
          (SmartExcept
             [
               `Text "Invalid - operation on mutez (negative result) "
             ; `Value (typecheck Type.mutez x)
             ; `Value (typecheck Type.mutez y)
             ]);
      mutez res
  | Literal (Timestamp x), Literal (Timestamp y) ->
      int (Big_int.sub_big_int x y)
  | _ -> failwith "Invalid - operation"

(** Access the elements of a list. *)
let lens_list =
  Lens.make (function
    | {t; tv = List focus} ->
        let zip focus =
          let focus = List.map erase_types focus in
          typecheck t (build_list focus)
        in
        {focus; zip}
    | _ -> failwith "lens_list")

let lens_list_nth n = Lens.(lens_list @. Lens.nth n)

(** Access the entries of a record. *)
let lens_record =
  Lens.make (function
    | {t; tv = Record focus} ->
        let zip focus =
          let focus = List.map (map_snd erase_types) focus in
          typecheck t (record focus)
        in
        {focus; zip}
    | _ -> failwith "lens_map")

let lens_record_at ~attr =
  Lens.(lens_record @. Lens.assoc ~equal:( = ) ~key:attr)

(** Access the elements of a set. *)
let lens_set =
  Lens.make (function
    | {t = F (T1 (T_set, telement)); tv = Set focus} ->
        let zip focus =
          let focus = List.map erase_types focus in
          typecheck (Type.set ~telement) (set focus ~telement)
        in
        {focus; zip}
    | _ -> failwith "lens_set")

let lens_set_at ~elem =
  let equal x y = equal (erase_types x) (erase_types y) in
  Lens.(lens_set @. sorted_list ~equal ~elem)

(** Access the entries of a map. *)
let lens_map =
  Lens.make (function
    | {t = F (T2 ((T_map | T_big_map), tkey, _tvalue)) as t; tv = Map focus} ->
        let zip focus =
          let focus =
            List.map (fun (x, y) -> (erase_types x, erase_types y)) focus
          in
          typecheck t (map ~tkey focus)
        in
        {focus; zip}
    | _ -> failwith "lens_map")

let lens_map_at ~key =
  let equal x y = equal (erase_types x) (erase_types y) in
  Lens.(lens_map @. Lens.assoc ~equal ~key)
