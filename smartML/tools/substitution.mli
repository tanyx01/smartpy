(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Basics
open Typed

type t

val empty : unit -> t

type substitute =
  | SType of Type.t
  | SRow of (Type.t Type.row * VarId.t option)
  | STupleRow of ((int * Type.t) list * VarId.t option)
  | SLayout of Layout.t Hole.t
  | SBool of bool Hole.t
  | SInt of int Hole.t
  | SWithStorage of (Type.access * Type.t) option Hole.t
  | SBounded of (Literal.t list * VarId.t option)
[@@deriving show]

val dump : t -> unit

val on_hole : (substitute -> 'a Hole.t) -> t -> 'a Hole.t -> 'a Hole.t

val on_type : t -> Type.t -> Type.t

val on_open_row :
  t -> Type.t Type.row * VarId.t option -> Type.t Type.row * VarId.t option

val on_layout : t -> Layout.t Hole.t -> Layout.t Hole.t

val on_expr : t -> texpr -> texpr

val on_command : t -> tcommand -> tcommand

val on_lambda : t -> tlambda -> tlambda

val on_contract : t -> tcontract -> tcontract

val on_scenario : t -> tscenario -> tscenario

val on_constraint : t -> typing_constraint -> typing_constraint

val find_opt : VarId.t -> t -> substitute option

val insert : (VarId.t * substitute) list -> t -> unit
