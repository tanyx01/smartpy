(* Copyright 2019-2022 Smart Chain Arena LLC. *)

module type S = sig
  include Stdlib.Set.S

  val unions : t list -> t

  val of_option : elt option -> t
end

module type OrderedType = Stdlib.Set.OrderedType

module Make (Ord : OrderedType) = struct
  include Stdlib.Set.Make (Ord)

  let unions = List.fold_left union empty

  let of_option = Option.cata empty singleton
end
