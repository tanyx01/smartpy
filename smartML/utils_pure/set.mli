(* Copyright 2019-2022 Smart Chain Arena LLC. *)

module type S = sig
  include Stdlib.Set.S

  val unions : t list -> t

  val of_option : elt option -> t
end

module type OrderedType = Stdlib.Set.OrderedType

module Make (Ord : OrderedType) : S with type elt = Ord.t
