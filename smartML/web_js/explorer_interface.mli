(* Copyright 2019-2022 Smart Chain Arena LLC. *)

type transaction = {
    metaData : (string * string) list
  ; details : (string * string) list
  ; parameters : [ `Error of string | `OK of string * string ]
  ; storage : [ `Error of string | `OK of string ]
  ; errors : string option
  ; operation_json : Yojson.Safe.t
  ; is_target : bool
}
