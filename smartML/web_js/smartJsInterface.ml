(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Js_of_ocaml
open Tools
open Basics
open Html
open Utils
open Control

type exportToJs = {
    exportToJs : 'a 'b. string -> ('a -> 'b) -> unit
  ; exportToJsString : 'a. string -> ('a -> string) -> unit
  ; js_to_string : Js.js_string Js.t -> string
  ; string_to_js : string -> Js.js_string Js.t
  ; getText : checked:bool -> string -> string
  ; setText : string -> string -> unit
  ; setValue : string -> string -> unit
  ; setOutput : string -> unit
  ; setExplorerOutput : string -> string -> unit
  ; addOutput : string -> unit
  ; setOutputToMethod : string -> string -> unit
  ; isChecked : string -> bool
  ; parseDate : string -> string
}

let importContractString ~config ~scenario_state ~env primitives s =
  let c = Import.import_contract env (Parsexp.Single.parse_string_exn s) in
  let c = Checker.check_contract config c in
  let c = Reducer.reduce_contract ~primitives ~scenario_state c in
  let c = Closer.close_contract c in
  let c =
    match c.tcontract.storage with
    | None -> c
    | Some s ->
        let s = Closer.close_expr ~config s in
        let c = {tcontract = {c.tcontract with storage = Some s}} in
        Closer.close_contract c
  in
  let c = {tcontract = {c.tcontract with unknown_parts = None}} in
  Interpreter.interpret_contract ~config ~primitives ~scenario_state c

let js_primitives = lazy (module Smartml_js.Primitives : Primitives.Primitives)

let callGui ~config id sim_id output t tstorage_option ~line_no : unit =
  let nextId = Value.nextId id in
  let input = Value_gui.inputGuiR ~nextId t in
  let v = input.get true in
  let v = Value.typecheck t v in
  let storageInput = Value_gui.inputGuiR ~nextId tstorage_option in
  let contextInput = Value_gui.inputGuiR ~nextId contextSimulationType in
  let contextV = contextInput.get true in
  let storageV = storageInput.get true in
  match (v.tv, contextV.v, Hashtbl.find_opt Html.simulatedContracts sim_id) with
  | Literal Unit, _, _ ->
      failwith "No entry point to call"
      (* This should be coherent with Html.contextSimulationType *)
  | ( Variant (channel, params)
    , Record
        [
          ( _
          , {
              v =
                Record
                  [
                    ("amount", amount)
                  ; ("level", level)
                  ; ("sender", sender)
                  ; ("source", source)
                  ; ("timestamp", timestamp)
                  ; ("voting_powers", voting_powers)
                  ]
            } )
        ; (_, {v = Record [("debug", debug); ("full_output", full_output)]})
        ]
    , Some initContract ) ->
      let time =
        match Value.unString ~pp:(fun () -> []) timestamp with
        | "" -> Big_int.zero_big_int
        | x -> Bigint.of_string ~msg:"jsInterface" x
      in
      let level =
        match Value.unString ~pp:(fun () -> []) level with
        | "" -> Bigint.zero_big_int
        | x -> Bigint.of_string ~msg:"jsInterface" x
      in
      let pp () = [] in
      let voting_powers =
        List.map
          (fun (k, v) -> (Value.unKey_hash ~pp k, Value.unInt ~pp v))
          (Value.unMap ~pp voting_powers)
      in
      let scenario_state = scenario_state ~time ~level ~voting_powers () in
      let amount =
        match amount.v with
        | Variant (_, tez) when Value.unString ~pp:(fun () -> []) tez = "" ->
            Big_int.zero_big_int
        | Variant ("Tez", tez) ->
            Big_int.mult_int_big_int 1000000
              (Bigint.of_string ~msg:"jsInterface"
                 (Value.unString ~pp:(fun () -> []) tez))
        | Variant ("Mutez", mutez) ->
            Bigint.of_string ~msg:"jsInterface"
              (Value.unString ~pp:(fun () -> []) mutez)
        | _ -> assert false
      in
      let context =
        Interpreter.build_context
          ~sender:(Value.unString ~pp:(fun () -> []) sender)
          ~source:(Value.unString ~pp:(fun () -> []) source)
          ~amount ~scenario_state ~line_no
          ~debug:(Value.unBool ~pp:(fun () -> []) debug)
          ()
      in
      let {template; state} = Value.typecheck_instance initContract in
      let state =
        let storageV = Value.typecheck tstorage_option storageV in
        match storageV.tv with
        | Variant ("None", _) -> state
        | Variant ("Some", v) -> {state with storage = Some v}
        | _ -> assert false
      in
      let fake_id = C_static {static_id = sim_id} in
      Hashtbl.add scenario_state.contracts fake_id {template; state};
      let message =
        Contract.execMessageInner ~config ~title:""
          ~primitives:(Lazy.force js_primitives) ~scenario_state
          ~execMessageClass:"" ~context ~id:fake_id ~channel ~params ~line_no
      in
      let outputHtml =
        let michelson =
          match
            (message.contract, Value.unBool ~pp:(fun () -> []) full_output)
          with
          | _, false | None, _ -> ""
          | Some contract, _ ->
              let scenario_vars =
                String.Map.of_hashtbl scenario_state.variables
              in
              let tcontract = Value.typecheck_instance contract in
              render
                (Html.full_html ~config ~contract:tcontract
                   ~compiled_contract:
                     (Compiler.compile_instance ~scenario_vars tcontract)
                   ~def:"Michelson" ~onlyDefault:false
                   ~id:(Printf.sprintf "sim_%s" id)
                   ~line_no ~accept_missings:false ~contract_id:None)
        in
        Printf.sprintf "%s%s" message.html michelson
      in
      SmartDom.setText output outputHtml
  (* ^ delayedInputGui t*)
  | _ ->
      raise
        (SmartExcept
           [
             `Text "Parse Error"
           ; `Br
           ; `Text (show_tvalue v)
           ; `Br
           ; `Text (Value.show contextV)
           ])

let buildTransfer ~config id output destination account t : unit =
  let module Printer = (val Printer.get config : Printer.Printer) in
  try
    let input = Value_gui.inputGuiR ~nextId:(Value.nextId id) t in
    let initialValue = input.get true in
    let initialValue = Value.typecheck t initialValue in
    let value =
      let entry_point, initialValue =
        match initialValue.tv with
        | Variant (entry_point, initialValue) -> (Some entry_point, initialValue)
        | _ -> (None, initialValue)
      in
      let compiled_parameter =
        Compiler.compile_value ~config ~scenario_vars:String.Map.empty
          initialValue
      in
      let compiled_parameter_full =
        let value =
          match entry_point with
          | None -> initialValue
          | Some entry_point -> Value.Typed.variant t entry_point initialValue
        in
        Compiler.compile_value ~config ~scenario_vars:String.Map.empty value
      in
      let as_json = Builder.buildTransferParametersJSON ~compiled_parameter in
      let micheline =
        Builder.buildTransferParametersMicheline ~compiled_parameter
      in
      let as_json_full =
        Builder.buildTransferParametersJSON
          ~compiled_parameter:compiled_parameter_full
      in
      let micheline_full =
        Builder.buildTransferParametersMicheline
          ~compiled_parameter:compiled_parameter_full
      in
      let entry_point = Option.default "default" entry_point in
      SmartDom.setText "michClient"
        (Builder.buildTransferParametersCLI ~entry_point ~account ~destination
           ~compiled_parameter);
      SmartDom.setValue "messageSent" micheline;
      SmartDom.setValue "messageSentJSON" as_json;
      SmartDom.setValue "transferJSON"
        (Builder.buildEntrypointCallJSON ~entry_point ~compiled_parameter);
      let encodings =
        String.concat ""
          (List.map
             (fun (name, encoding) ->
               Printf.sprintf "<tr><td>%s</td><td>%s</td></tr>" name encoding)
             [
               ("Micheline", micheline)
             ; ("JSON", as_json)
             ; ("Micheline (Full Path)", micheline_full)
             ; ("JSON (Full Path)", as_json_full)
             ])
      in
      Printf.sprintf
        "Parameters OK.<br>  Entry Point: %s<br><table \
         class='recordList'>%s</table>"
        entry_point encodings
    in
    SmartDom.setText output value
  with _ as e ->
    SmartDom.setText output
      (Printf.sprintf "Error during execution: %s"
         (Printer.exception_to_string true e))

let buildTransferTwo ~config id t : unit =
  let module Printer = (val Printer.get config : Printer.Printer) in
  try
    let input = Value_gui.inputGuiR ~nextId:(Value.nextId id) t in
    let initialValue = input.get true in
    let initialValue = Value.typecheck t initialValue in
    let entry_point, initialValue =
      match initialValue.tv with
      | Variant (entry_point, initialValue) -> (Some entry_point, initialValue)
      | _ -> (None, initialValue)
    in
    let compiled_parameter_full =
      let value =
        match entry_point with
        | None -> initialValue
        | Some entry_point -> Value.Typed.variant t entry_point initialValue
      in
      Compiler.compile_value ~config ~scenario_vars:String.Map.empty value
    in
    let entry_point = Option.default "default" entry_point in
    let compiled_parameter =
      Compiler.compile_value ~config ~scenario_vars:String.Map.empty
        initialValue
    in
    let as_json = Builder.buildTransferParametersJSON ~compiled_parameter in
    let micheline =
      Builder.buildTransferParametersMicheline ~compiled_parameter
    in
    let as_json_full =
      Builder.buildTransferParametersJSON
        ~compiled_parameter:compiled_parameter_full
    in
    let micheline_full =
      Builder.buildTransferParametersMicheline
        ~compiled_parameter:compiled_parameter_full
    in
    SmartDom.setExplorerOutput "entrypoint" entry_point;
    SmartDom.setExplorerOutput "paramsMicheline" micheline;
    SmartDom.setExplorerOutput "paramsJSON" as_json;
    SmartDom.setExplorerOutput "paramsMichelineFull" micheline_full;
    SmartDom.setExplorerOutput "paramsJSONFull" as_json_full
  with _ as e ->
    SmartDom.setExplorerOutput "errors"
      (Printf.sprintf "Error during execution: %s"
         (Printer.exception_to_string true e))

let is_local () =
  let open Js_of_ocaml.Js in
  "localhost" = to_string (Unsafe.eval_string "location.hostname")

let michelson_view sizes examples types_output_div simplified_types_output_div
    simplified_no_types_output_div raw_no_types_output_div json_output_div
    json_raw_output_div michel_raw_output_div michel_simplified_output_div
    michel_pre_michelson_output_div michel_pre_smartML_output_div
    michel_michelson_output_div michelson_view_div =
  let originationButton =
    {|
    <button class='centertextbutton extramarginbottom'
      onClick='smartpyContext.gotoOrigination(
        michelson_view_json_output.innerText,
        simplified_init_storage_json_0.innerText
      )'>
        Deploy Michelson Contract
    </button>
  |}
  in
  let tabs =
    [
      Html.tab ~active:() "Simplified"
        (Html.tabs ""
           [
             Html.tab "Michelson" ~active:()
               (Html.div ~args:"id='simplified_types_output'"
                  [Html.Raw originationButton; simplified_types_output_div])
           ; Html.tab "No Type"
               (Html.div ~args:"id='simplified_no_types_output'"
                  [simplified_no_types_output_div])
           ; Html.tab "JSON"
               (Html.div ~args:"id='json_output'" [json_output_div])
           ])
    ; Html.tab "Raw"
        (Html.tabs ""
           [
             Html.tab "Michelson" ~active:()
               (Html.div ~args:"id='types_output'" [types_output_div])
           ; Html.tab "No Type"
               (Html.div ~args:"id='raw_no_types_output'"
                  [raw_no_types_output_div])
           ; Html.tab "JSON"
               (Html.div ~args:"id='json_raw_output'" [json_raw_output_div])
           ])
    ; Html.tab "Sizes" (Html.div ~args:"id='sizes_output'" [sizes])
    ; Html.tab "Examples" (Html.div ~args:"id='examples_output'" [examples])
    ]
  in
  let tabs =
    if is_local ()
    then
      tabs
      @ [
          Html.tab "Michel"
            (Html.tabs "Michel"
               [
                 Html.tab ~active:() "Raw"
                   (Html.div ~args:"id='michel_raw_output'"
                      [michel_raw_output_div])
               ; Html.tab "Simplified"
                   (Html.div ~args:"id='michel_simplified_output'"
                      [michel_simplified_output_div])
               ; Html.tab "Pre-Michelson"
                   (Html.div ~args:"id='michel_pre_michelson_output'"
                      [michel_pre_michelson_output_div])
               ; Html.tab "Pre-SmartML"
                   (Html.div ~args:"id='michel_pre_smartML_output'"
                      [michel_pre_smartML_output_div])
               ; Html.tab "Michelson"
                   (Html.div ~args:"id='michel_michelson_output'"
                      [michel_michelson_output_div])
               ])
        ; Html.tab "SmartPy"
            (Html.div ~args:"id='smartpy_output'" [michelson_view_div])
        ]
    else tabs
  in
  Html.tabs "Output" tabs |> Html.render

let update_michelson_view ~config micheline =
  let protocol = config.Config.protocol in
  SmartDom.setOutputToMethod "setFirstMessage" "";
  let first_parsed =
    match micheline with
    | Basics.Sequence
        [
          Primitive {name = "parameter"; arguments = [parameter]}
        ; Primitive {name = "storage"; arguments = [storage]}
        ; Primitive {name = "code"; arguments = [code]}
        ]
    | Basics.Sequence
        [
          Primitive {name = "storage"; arguments = [storage]}
        ; Primitive {name = "parameter"; arguments = [parameter]}
        ; Primitive {name = "code"; arguments = [code]}
        ] -> Some (parameter, storage, code)
    | _ ->
        SmartDom.setOutputToMethod "setFirstMessage"
          "Badly formed contract. Expecting parameter, storage and code (in \
           this order).";
        None
  in
  match first_parsed with
  | None -> ()
  | Some (tparameter, tstorage, code) ->
      let code = Michelson.Of_micheline.instruction code in
      let tstorage = Michelson.Of_micheline.mtype tstorage in
      let tparameter = Michelson.Of_micheline.mtype_annotated tparameter in
      let raw_contract_lax =
        Michelson.typecheck_contract ~protocol ~strict_dup:false
          {
            tparameter
          ; tstorage
          ; lazy_entry_points = None
          ; storage = None
          ; code
          ; views = []
          }
      in
      let raw_contract =
        Michelson.typecheck_contract ~protocol ~strict_dup:true
          {
            tparameter
          ; tstorage
          ; lazy_entry_points = None
          ; storage = None
          ; code
          ; views = []
          }
      in
      let simplified_contract =
        Michelson_rewriter.(
          run_on_tcontract ~protocol (simplify ~protocol) raw_contract_lax)
      in
      let simplified_contract =
        Michelson.typecheck_contract ~protocol ~strict_dup:true
          {
            tparameter
          ; tstorage
          ; lazy_entry_points = None
          ; storage = None
          ; code = Michelson.erase_types_instr simplified_contract.code
          ; views = []
          }
      in
      let first_message =
        let errors_simplifies =
          Michelson.has_error_tcontract ~accept_missings:false
            simplified_contract
        in
        let errors_raw =
          Michelson.has_error_tcontract ~accept_missings:false raw_contract
        in
        let pp_errors =
          if errors_simplifies = errors_raw
          then String.concat "; " errors_raw
          else ""
        in
        let pp_errors =
          if String.length pp_errors > 100
          then String.sub pp_errors 0 100 ^ "..."
          else pp_errors
        in
        match (errors_simplifies, errors_raw) with
        | [], [] -> None
        | [], _ -> Some ("Errors in raw contract. " ^ pp_errors)
        | _, [] -> Some ("Errors in simplified contract. " ^ pp_errors)
        | [e1], [e2]
          when e1 = Michelson.unexpected_final_stack_error
               && e2 = Michelson.unexpected_final_stack_error ->
            Some "Incomplete stack"
        | _ -> Some ("Errors in Michelson contract. " ^ pp_errors)
      in
      let simplifiedCodeSize, simplifiedJSon =
        let simplifiedCode =
          Michelson.to_micheline_tcontract ~protocol simplified_contract
        in
        ( Micheline_encoding.micheline_size_opt simplifiedCode
        , Format.asprintf "%a" (Micheline.pp_as_json ()) simplifiedCode )
      in
      let codeSize, initialJSon =
        let code = Michelson.to_micheline_tcontract ~protocol raw_contract in
        ( Micheline_encoding.micheline_size_opt code
        , Format.asprintf "%a" (Micheline.pp_as_json ()) code )
      in
      let sizes =
        Html.contract_sizes_html ~codeSize
          ~simplifiedCodeSize:
            (if initialJSon <> simplifiedJSon then simplifiedCodeSize else None)
          ~storageSize:None ~nb_bigmaps:0
      in
      let examples =
        let parameter_examples = Michelson.mtype_examples (fst tparameter) in
        let parameter_examples_m =
          List.map (Michelson.string_of_literal ~protocol) parameter_examples
        in
        let parameter_examples_j =
          List.map
            (fun parameter ->
              Format.asprintf "%a" (Micheline.pp_as_json ())
                (Michelson.To_micheline.literal ~protocol parameter))
            parameter_examples
        in
        let storage_examples = Michelson.mtype_examples tstorage in
        let storage_examples_m =
          List.map (Michelson.string_of_literal ~protocol) storage_examples
        in
        let storage_examples_j =
          List.map
            (fun storage ->
              Format.asprintf "%a" (Micheline.pp_as_json ())
                (Michelson.To_micheline.literal ~protocol storage))
            storage_examples
        in
        Html.div
          [
            Html.div
              [
                Html.Raw "<h2>Parameters</h2>"
              ; Html.copy_div ~id:"parameter_examples_" "parameter_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" parameter_examples_m))
              ]
          ; Html.div
              [
                Html.Raw "<br><h2>Storage</h2>"
              ; Html.copy_div ~id:"storage_examples_" "storage_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" storage_examples_m))
              ]
          ; Html.div
              [
                Html.Raw "<br><h2>Parameters JSON</h2>"
              ; Html.copy_div ~id:"parameter_examples_JSON" "parameter_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" parameter_examples_j))
              ]
          ; Html.div
              [
                Html.Raw "<br><h2>Storage JSON</h2>"
              ; Html.copy_div ~id:"storage_examples_JSON" "storage_examples"
                  (Html.div
                     (List.fold_left
                        (fun acc example ->
                          (if List.length acc mod 2 == 1
                          then acc @ [Html.Raw "\n==\n\n"]
                          else acc)
                          @ [
                              Html.div
                                ~args:
                                  (Printf.sprintf
                                     "id='simplified_init_storage_json_%d'"
                                     (List.length acc))
                                [Html.Raw example]
                            ])
                        [] storage_examples_j))
              ]
          ]
      in
      (match first_message with
      | Some msg -> SmartDom.setOutputToMethod "setFirstMessage" msg
      | None -> ());
      let types_output_div =
        Html.copy_div ~id:"_types_output" "michelson_view"
          (Html.Raw (Michelson.render_tcontract ~protocol raw_contract))
      in
      let simplified_types_output_div =
        Html.copy_div ~id:"_simplified_types_output" "michelson_view"
          (Html.Raw (Michelson.render_tcontract ~protocol simplified_contract))
      in
      let simplified_no_types_output_div =
        Html.copy_div ~id:"_simplified_no_types_output" "michelson_view"
          (Html.Raw
             (Michelson.render_tcontract_no_types ~protocol simplified_contract))
      in
      let raw_no_types_output_div =
        Html.copy_div ~id:"_raw_no_types_output" "michelson_view"
          (Html.Raw (Michelson.render_tcontract_no_types ~protocol raw_contract))
      in
      let json_output_div =
        Html.copy_div ~id:"_json_output" "michelson_view"
          (Html.Raw
             (Format.asprintf "<div class='white-space-pre'>%a</div>"
                (Micheline.pp_as_json ())
                (Michelson.to_micheline_tcontract ~protocol simplified_contract)))
      in
      let json_raw_output_div =
        Html.copy_div ~id:"_json_raw_output" "michelson_view"
          (Html.Raw
             (Format.asprintf "<div class='white-space-pre'>%a</div>"
                (Micheline.pp_as_json ())
                (Michelson.to_micheline_tcontract ~protocol raw_contract)))
      in
      let module Printer = (val Printer.get config : Printer.Printer) in
      if is_local ()
      then
        let open Result in
        let open Decompiler in
        let open Michel_decompiler in
        let open Michel.Transformer in
        let st = {var_counter = ref 0} in
        let map_catch f x =
          try Result.map f x
          with exn -> Error (Printer.exception_to_string false exn)
        in
        let show_or_err f x =
          Result.cata f (fun s -> Printf.sprintf "Error: %s\n" s) x
        in
        (* Conversion *)
        let raw_contract = Ok raw_contract in
        let raw = map_catch (decompile_contract st) raw_contract in
        let simplified = map_catch (simplify ~protocol st) raw in
        let pre_michelson = map_catch (michelsonify ~protocol st) raw in
        let pre_smartML = map_catch (smartMLify ~protocol st) raw in
        let pre_michelson =
          pre_michelson >>= Michel.Typing.typecheck_precontract ~protocol
        in
        let michelson =
          map_catch
            (Michel_compiler.compile_contract ~protocol ~views:[])
            pre_michelson
        in
        let michelson =
          map_catch
            Michelson_rewriter.(run_on_tcontract ~protocol (simplify ~protocol))
            michelson
        in
        (* Michel typechecking *)
        let pre_smartML =
          pre_smartML >>= Michel.Typing.typecheck_precontract ~protocol
        in
        let raw = raw >>= Michel.Typing.typecheck_precontract ~protocol in
        let simplified =
          simplified >>= Michel.Typing.typecheck_precontract ~protocol
        in
        (* Michel -> SmartML *)
        let config = Config.default in
        let smartML = map_catch (smartML_of_michel config) pre_smartML in
        let smartML = Result.map Value.typecheck_instance smartML in
        (* Printing *)
        let raw = show_or_err Michel.Typing.show_checked_precontract raw in
        let simplified =
          show_or_err Michel.Typing.show_checked_precontract simplified
        in
        let pre_michelson =
          show_or_err Michel.Typing.show_checked_precontract pre_michelson
        in
        let pre_smartML =
          show_or_err Michel.Typing.show_checked_precontract pre_smartML
        in
        let michelson =
          show_or_err (Michelson.render_tcontract ~protocol) michelson
        in
        let smartML_html =
          show_or_err
            (fun x ->
              Printer.tinstance_to_string ~options:Printer.Options.html x)
            smartML
        in
        let smartML_text =
          let f c =
            Printer.tinstance_to_string c
            ^ "\n\n\
               @sp.add_test(name = \"Test\")\n\
               def test():\n\
              \    s = sp.test_scenario()\n\
              \    c = Contract()\n\
              \    s += c\n\
              \    # s += c.default()"
          in
          show_or_err f smartML
        in
        let michel_raw_output_div =
          Html.copy_div ~id:"_michel_raw_output" "michelson_view" (Html.Raw raw)
        in
        let michel_simplified_output_div =
          Html.copy_div ~id:"_michel_simplified_output" "michelson_view"
            (Html.Raw simplified)
        in
        let michel_pre_michelson_output_div =
          Html.copy_div ~id:"_michel_pre_michelson_output" "michelson_view"
            (Html.Raw pre_michelson)
        in
        let michel_pre_smartML_output_div =
          Html.copy_div ~id:"_michel_pre_smartML_output" "michelson_view"
            (Html.Raw pre_smartML)
        in
        let michel_michelson_output_div =
          Html.copy_div ~id:"_michel_michelson_output" "michelson_view"
            (Html.Raw michelson)
        in
        let michelson_view_div =
          Html.Div
            ( ""
            , [
                Html.Raw smartML_html
              ; Html.Raw "<br>"
              ; Html.copy_div ~id:"_smartpy_output" "michelson_view"
                  (Html.Raw smartML_text)
              ] )
        in
        SmartDom.setOutput
          (michelson_view sizes examples types_output_div
             simplified_types_output_div simplified_no_types_output_div
             raw_no_types_output_div json_output_div json_raw_output_div
             michel_raw_output_div michel_simplified_output_div
             michel_pre_michelson_output_div michel_pre_smartML_output_div
             michel_michelson_output_div michelson_view_div)
      else
        SmartDom.setOutput
          (michelson_view sizes examples types_output_div
             simplified_types_output_div simplified_no_types_output_div
             raw_no_types_output_div json_output_div json_raw_output_div
             (Html.Raw "") (Html.Raw "") (Html.Raw "") (Html.Raw "")
             (Html.Raw "") (Html.Raw ""))

module type INTERFACE = sig
  open Js

  val importType : exportToJs -> js_string t -> Type.t

  val importContract : exportToJs -> js_string t -> Contract.t

  val compileContractStorage : Basics.instance -> string

  val compileContract : Basics.instance -> Michelson.tcontract

  val update_michelson_view : exportToJs -> js_string t -> js_string t -> unit

  val buildTransfer :
       exportToJs
    -> js_string t
    -> js_string t
    -> js_string t
    -> js_string t
    -> Type.t
    -> unit

  val buildTransferTwo : exportToJs -> js_string t -> Type.t -> unit

  val stringOfException : exportToJs -> bool -> exn -> js_string t

  val js_string : 'a -> 'a

  val callGui :
       exportToJs
    -> js_string t
    -> int
    -> js_string t
    -> Type.t
    -> Type.t
    -> int
    -> unit

  val explore : exportToJs -> js_string t -> js_string t -> js_string t -> unit

  val exploreTwo :
    exportToJs -> js_string t -> js_string t -> js_string t -> unit

  val parseStorage : exportToJs -> js_string t -> string

  val exploreOperations :
       exportToJs
    -> Js.js_string Js.t
    -> Js.js_string Js.t
    -> Js.js_string Js.t
    -> unit

  val runSmartMLScript : exportToJs -> js_string t -> string

  val runSmartMLScriptScenarioName : exportToJs -> js_string t -> unit

  val runScenarioInBrowser : exportToJs -> js_string t -> unit

  val lazy_tab : exportToJs -> int -> int -> js_string t
end

let ppx_mappers = ref []

module Toplevel = struct
  open Ast_mapper

  let initialize = Js_of_ocaml_toplevel.JsooTop.initialize

  let preprocess_structure x =
    List.fold_right
      (fun mapper str -> mapper.structure mapper str)
      !ppx_mappers x

  let preprocess_phrase =
    let open Parsetree in
    function
    | Ptop_def def -> Ptop_def (preprocess_structure def)
    | Ptop_dir _ as x -> x

  let with_buffer_formatter size k =
    let b = Buffer.create size in
    let fmt = Format.formatter_of_buffer b in
    let get () =
      Format.pp_print_flush fmt ();
      Buffer.contents b
    in
    k fmt get

  let execute x =
    let x = Lexing.from_string ~with_positions:true x in
    with_buffer_formatter 512 (fun fmt get ->
        try
          let xs = !Toploop.parse_use_file x in
          let xs = List.map preprocess_phrase xs in
          List.iter
            (fun x ->
              if not (Toploop.execute_phrase false fmt x)
              then failwith ("Exception during evaluation: " ^ get ()))
            xs
        with exn ->
          with_buffer_formatter 64 (fun fmt get ->
              Errors.report_error fmt exn;
              failwith ("OCaml error: " ^ get ())))
end

module Interface : INTERFACE = struct
  let config = Config.default

  let protocol = config.protocol

  let importType ctx s =
    Import.import_type (Import.init_env ())
      (Parsexp.Single.parse_string_exn
         (Base.String.substr_replace_all (ctx.js_to_string s) ~pattern:"***"
            ~with_:"\""))

  let importContract ctx s =
    importContractString ~config (Lazy.force js_primitives)
      ~scenario_state:(scenario_state ()) ~env:(Import.init_env ())
      (ctx.js_to_string s)

  let compileContractStorage contract =
    let contract = Value.typecheck_instance contract in
    Base.Option.value_map
      (Compiler.compile_instance ~scenario_vars:String.Map.empty contract)
        .storage ~default:"missing storage"
      ~f:(Michelson.string_of_tliteral ~protocol)

  let compileContract c =
    let c = Value.typecheck_instance c in
    Compiler.compile_instance ~scenario_vars:String.Map.empty c

  let update_michelson_view ctx code protocol =
    let protocol = ctx.js_to_string protocol in
    let config = {config with protocol = Config.protocol_of_string protocol} in
    let micheline = Micheline_encoding.parse_node (ctx.js_to_string code) in
    update_michelson_view ~config micheline

  let buildTransfer ctx s o kt account t =
    buildTransfer ~config (ctx.js_to_string s) (ctx.js_to_string o)
      (ctx.js_to_string kt) (ctx.js_to_string account) t

  let buildTransferTwo ctx s t = buildTransferTwo ~config (ctx.js_to_string s) t

  let stringOfException ctx html exc =
    let module Printer = (val Printer.get config : Printer.Printer) in
    ctx.string_to_js (Printer.exception_to_string html exc)

  let js_string s = s

  let callGui ctx s id o t tstorage line_no =
    callGui ~config (ctx.js_to_string s) id (ctx.js_to_string o) t tstorage
      ~line_no:[("", line_no)]

  let explore ctx address json operations =
    Explorer.explore ~config ~address:(ctx.js_to_string address)
      ~json:(ctx.js_to_string json)
      ~operations:(ctx.js_to_string operations)

  let exploreTwo ctx address json network =
    ExplorerTwo.explore ~config ~address:(ctx.js_to_string address)
      ~json:(ctx.js_to_string json) ~network:(ctx.js_to_string network)

  let parseStorage ctx json =
    ExplorerTwo.parseStorage ~config ~json:(ctx.js_to_string json)

  let exploreOperations ctx address json operations =
    ExplorerTwo.exploreOperations ~config ~address:(ctx.js_to_string address)
      ~json:(ctx.js_to_string json)
      ~operations:(ctx.js_to_string operations)

  let runSmartMLScript ctx script =
    let script = ctx.js_to_string script in
    ppx_mappers := [Ppx_smartml_lib.Plumbing.ast_mapper];
    Target.clear ();
    Toplevel.initialize ();
    Toplevel.execute script;
    Smartml_scenario.run_all_scenarios_browser
      ~primitives:(Lazy.force js_primitives) ~scenario:(Target.to_json None)
      config;
    Target.names_kinds_json ()

  let runSmartMLScriptScenarioName ctx name =
    let name = ctx.js_to_string name in
    let module P = (val Lazy.force js_primitives : Primitives.Primitives) in
    Smartml_scenario.run_all_scenarios_browser
      ~primitives:(Lazy.force js_primitives)
      ~scenario:(Target.to_json (Some name))
      config

  let runScenarioInBrowser ctx scenario =
    Smartml_scenario.run_scenario_browser ~primitives:(Lazy.force js_primitives)
      ~scenario:(ctx.js_to_string scenario)
      config

  let lazy_tab ctx id global_id =
    ctx.string_to_js (Html.render (Html.call_tab id global_id))
end

let interface (ctx : exportToJs) =
  SmartDom.getTextRef := ctx.getText;
  SmartDom.setTextRef := ctx.setText;
  SmartDom.setValueRef := ctx.setValue;
  SmartDom.setOutputRef := ctx.setOutput;
  SmartDom.setExplorerOutputRef := ctx.setExplorerOutput;
  SmartDom.addOutputRef := ctx.addOutput;
  SmartDom.setOutputToMethodRef := ctx.setOutputToMethod;
  SmartDom.isCheckedRef := ctx.isChecked;
  SmartDom.parseDateRef := ctx.parseDate;
  let open Interface in
  ctx.exportToJs "importType" (importType ctx);
  ctx.exportToJs "importContract" (importContract ctx);
  ctx.exportToJsString "compileContractStorage" compileContractStorage;
  ctx.exportToJs "compileContract" compileContract;
  ctx.exportToJs "update_michelson_view" (update_michelson_view ctx);
  ctx.exportToJs "buildTransfer" (buildTransfer ctx);
  ctx.exportToJs "buildTransferTwo" (buildTransferTwo ctx);
  ctx.exportToJs "stringOfException" (stringOfException ctx);
  ctx.exportToJsString "js_string" js_string;
  ctx.exportToJs "callGui" (callGui ctx);
  ctx.exportToJs "explore" (explore ctx);
  ctx.exportToJs "exploreTwo" (exploreTwo ctx);
  ctx.exportToJs "parseStorage" (parseStorage ctx);
  ctx.exportToJs "exploreOperations" (exploreOperations ctx);
  ctx.exportToJsString "runSmartMLScript" (runSmartMLScript ctx);
  ctx.exportToJs "runSmartMLScriptScenarioName"
    (runSmartMLScriptScenarioName ctx);
  ctx.exportToJs "runScenarioInBrowser" (runScenarioInBrowser ctx);
  ctx.exportToJs "lazy_tab" (lazy_tab ctx)
