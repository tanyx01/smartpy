(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Tools

let protocol = SmartML.Config.default.protocol

let buildEntrypointCallJSON ~entry_point ~compiled_parameter =
  Yojson.Safe.to_string
    (`Assoc
      [
        ("entrypoint", `String entry_point)
      ; ( "value"
        , Utils.Misc.json_to_json
            (Micheline.to_json
               (Michelson.To_micheline.literal ~protocol compiled_parameter)) )
      ])

let buildTransferParametersJSON ~compiled_parameter =
  Format.asprintf "%a" (Micheline.pp_as_json ())
    (Michelson.To_micheline.literal ~protocol compiled_parameter)

let buildTransferParametersMicheline ~compiled_parameter : string =
  Michelson.string_of_literal ~protocol compiled_parameter

let buildTransferParametersCLI ~entry_point ~account ~destination
    ~compiled_parameter : string =
  Printf.sprintf
    "octez-client transfer 0 from %s to %s --entrypoint %s --arg '%s'" account
    destination entry_point
    (buildTransferParametersMicheline ~compiled_parameter)
