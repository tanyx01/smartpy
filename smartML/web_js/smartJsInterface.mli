(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Js_of_ocaml

type exportToJs = {
    exportToJs : 'a 'b. string -> ('a -> 'b) -> unit
  ; exportToJsString : 'a. string -> ('a -> string) -> unit
  ; js_to_string : Js.js_string Js.t -> string
  ; string_to_js : string -> Js.js_string Js.t
  ; getText : checked:bool -> string -> string
  ; setText : string -> string -> unit
  ; setValue : string -> string -> unit
  ; setOutput : string -> unit
  ; setExplorerOutput : string -> string -> unit
  ; addOutput : string -> unit
  ; setOutputToMethod : string -> string -> unit
  ; isChecked : string -> bool
  ; parseDate : string -> string
}

val interface : exportToJs -> unit
