(* Copyright 2019-2022 Smart Chain Arena LLC. *)

val explore :
     config:SmartML.Config.t
  -> address:string
  -> json:string
  -> operations:string
  -> unit
