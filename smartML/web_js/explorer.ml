(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open SmartML
open Utils
open Control
open Tools
open Utils.Misc
open Html

let show_full_contract ~config tstorage tparameter mtparameter mtstorage storage
    code =
  let protocol = config.Config.protocol in
  let code = Michelson.Of_micheline.instruction code in
  let storage = Michelson.Of_micheline.literal storage in
  let contract =
    Michelson.typecheck_contract ~protocol ~strict_dup:false
      {
        tparameter = mtparameter
      ; tstorage = mtstorage
      ; lazy_entry_points = None
      ; storage = Some storage
      ; code
      ; views = []
      }
  in
  let simplified_contract =
    Michelson_rewriter.(
      run_on_tcontract ~protocol (simplify ~protocol) contract)
  in
  let module Printer = (val Printer.get config : Printer.Printer) in
  let types =
    tabs ~global_id:1 "Contract:"
      [
        tab ~active:() "Storage Type"
          (Raw (Printer.type_to_string ~options:Printer.Options.html tstorage))
      ; tab "Parameter Type"
          (Raw (Printer.type_to_string ~options:Printer.Options.html tparameter))
      ; tab "Michelson"
          (Html.michelson_html ~protocol ~title:"Michelson" ~lazy_tabs:true
             ~id:"0" ~simplified_contract contract)
        (* ; tab
         *     "SmartPy"
         *     ~lazy_tab:
         *       ( lazy
         *         (Raw
         *            (Printf.sprintf
         *               "Decompilation is experimental!<br><div \
         *                class='white-space-pre'>%s</div>"
         *               (Lazy.force decompiled))) )
         *     (Raw "") *)
        (* TODO Show correct balance. *)
      ]
  in
  SmartDom.setText "types" (render types)

let rec find_entrypoint path entrypoint t =
  match Type.unF t with
  | Type.TVariant {row} -> (
      match List.assoc_opt entrypoint row with
      | Some sub_t -> Some (sub_t, (entrypoint, t) :: path)
      | None ->
          List.fold_left
            (fun acc (n, sub_t) ->
              match acc with
              | Some _ as x -> x
              | None -> find_entrypoint ((n, t) :: path) entrypoint sub_t)
            None row)
  | _ -> None

let show_operation ~config ~pp_mich ~tparameter getStorage
    Explorer_interface.
      {
        metaData
      ; details
      ; parameters
      ; storage
      ; errors
      ; operation_json
      ; is_target
      } =
  let module Printer = (val Printer.get_by_language ~config Config.SmartPy
                          : Printer.Printer)
  in
  let mk_table entries =
    Printer.render_record_list (List.map fst entries, [List.map snd entries]) id
  in
  let params =
    match parameters with
    | `Error error -> error
    | `OK (entrypoint, parameters) -> (
        try
          let parameters =
            Micheline.parse (Yojson.Safe.from_string parameters)
          in
          let params =
            match Type.unF tparameter with
            | Type.TVariant _ -> (
                match find_entrypoint [] entrypoint tparameter with
                | None -> (
                    match entrypoint with
                    | "default" ->
                        Printf.ksprintf print_endline "default entrypoint";
                        Value.Typed.of_micheline ~config ~pp_mich tparameter
                          parameters
                    | _ ->
                        Value.Typed.string
                          (Printf.sprintf "Missing entrypoint %s %s" entrypoint
                             (Printer.type_to_string
                                ~options:Printer.Options.string tparameter)))
                | Some (t, path) ->
                    let parameters =
                      Value.Typed.of_micheline ~config ~pp_mich t parameters
                    in
                    List.fold_left
                      (fun parameters (entrypoint, t) ->
                        Value.Typed.variant t entrypoint parameters)
                      parameters path)
            | _ ->
                Value.Typed.of_micheline ~config ~pp_mich tparameter parameters
          in
          Printer.value_to_string ~options:Printer.Options.html params
        with exn -> Printer.exception_to_string true exn)
  in
  let storage =
    match storage with
    | `Error error -> error
    | `OK storage -> (
        try
          let storage_value =
            getStorage (Micheline.parse (Yojson.Safe.from_string storage))
          in
          Printer.value_to_string ~options:Printer.Options.html storage_value
        with exn -> Printer.exception_to_string true exn)
  in
  let errors =
    match errors with
    | None -> ""
    | Some errors -> Printf.sprintf "<h4>Errors</h4>%s" errors
  in
  let full_data =
    Printf.sprintf
      "<button class='centertextbutton' onClick='popupJson(\"Operation \
       Details\", %s)'>View Full Data</button>"
      (Base.String.substr_replace_all
         (Yojson.Safe.to_string operation_json)
         ~pattern:"'" ~with_:"&apos")
  in
  if is_target
  then
    Printf.sprintf
      "<div \
       class='operation'><h4>Operation</h4>%s%s<h4>Details</h4>%s<h4>Parameters</h4>%s<h4>Storage</h4>%s%s</div>"
      (mk_table metaData) full_data (mk_table details) params storage errors
  else
    Printf.sprintf
      "<div class='operation'><h4>Outbound Internal \
       Operation</h4>%s%s<h4>Details</h4>%s%s</div>"
      (mk_table metaData) full_data (mk_table details) errors

let messageBuilder ~config ~address init_storage tstorage tparameter code
    operations =
  let protocol = config.Config.protocol in
  let mtstorage = Michelson.Of_micheline.mtype tstorage in
  let tstorage = Typing.type_of_mtype mtstorage in
  let pp_mich t i =
    Michelson.display_instr ~protocol
      (Typing.mtype_of_type ~with_annots:true t)
      (Michelson.Of_micheline.instruction i)
  in
  let getStorage x = Value.Typed.of_micheline ~config ~pp_mich tstorage x in
  let storage = getStorage init_storage in
  let addresses =
    let f (x, path) =
      match (x : Literal.t) with
      | Address {address} -> Some (address, path)
      | Key_hash a -> Some (a, path)
      | _ -> None
    in
    List.filter_map f (Value.extract_literals ~config storage)
  in
  let mtparameter, parameterAnnot =
    Michelson.Of_micheline.mtype_annotated tparameter
  in
  let tparameter =
    Typing.type_of_mtype
      ?wrap:(Option.map (fun t -> (`Variant, t)) parameterAnnot)
      mtparameter
  in
  let editor =
    let name = "contractId" in
    let accountName = "accountId" in
    let buttonText = "Build Transaction Parameters" in
    let output = nextOutputGuiId () in
    let id = nextInputGuiId () ^ "." in
    let nextId = Value.nextId id in
    let input = Value_gui.inputGuiR ~nextId tparameter in
    let tparameter =
      Base.String.substr_replace_all
        (SmartML.Export.export_type tparameter)
        ~pattern:"\"" ~with_:"***"
    in
    Printf.sprintf
      "<div class='simulationBuilder'><form><button type='button' \
       class='explorer_button' onClick=\"cleanMessages();t = \
       smartmlCtx.call_exn_handler('importType', '%s'); if (t) \
       smartmlCtx.call_exn_handler('buildTransfer', '%s', '%s', %s.value, \
       %s.value, t)\">%s</button><br>%s</form>\n\
       <div id='%s'></div></div>" tparameter id output name accountName
      buttonText input.gui output
  in
  let custom_metadata, tzcomet =
    let open Basics in
    let show_metadata k v =
      match v.tv with
      | Literal (Literal.String s) ->
          let explore =
            if Base.String.is_prefix s ~prefix:"http://"
               || Base.String.is_prefix s ~prefix:"https://"
            then Printf.sprintf "<a href='%s' target=_blank>Follow link</a>" s
            else if Base.String.is_prefix s ~prefix:"ipfs://"
            then
              let s = Base.String.drop_prefix s 7 in
              Printf.sprintf
                "<a href='https://gateway.ipfs.io/ipfs/%s' \
                 target=_blank>Follow IPFS link</a>"
                s
            else ""
          in
          [`V k; `V v; `T explore]
      | _ -> [`V k; `V v; `T ""]
    in
    match Value.get_field_opt "metadata" storage with
    | Some {tv = Map []} -> ([], true)
    | Some {tv = Map l} -> (List.map (fun (k, v) -> show_metadata k v) l, true)
    | _ -> (
        match Value.get_field_opt "metaData" storage with
        | Some {tv = Map l} ->
            (List.map (fun (k, v) -> show_metadata k v) l, true)
        | _ -> ([], false))
  in
  let module Printer = (val Printer.get_by_language ~config Config.SmartPy
                          : Printer.Printer)
  in
  let custom_metadata =
    let viewer =
      Printf.sprintf
        "<a \
         href='https://tzcomet.io/#/explorer%%3Fexplorer-input%%3D%s&go=true' \
         target=_blank>Explore with TZComet</a>"
        address
    in
    match custom_metadata with
    | [] -> if tzcomet then Printf.sprintf "<h4>Metadata</h4>%s" viewer else ""
    | _ ->
        Printf.sprintf
          "<h4>Metadata</h4>%s<div id='storageMetaDataDiv'>%s</div>"
          (if tzcomet then viewer else "")
          (Printer.render_record_list
             (["Name"; "Metadata"; "Explore"], custom_metadata)
             (function
               | `V s ->
                   Printer.value_to_string ~noEmptyList:true
                     ~options:Printer.Options.htmlStripStrings s
               | `T t -> t))
  in
  let addresses =
    match addresses with
    | [] -> ""
    | _ ->
        let addresses =
          List.map
            (fun (address, path) ->
              let explore =
                let s = address in
                let s =
                  if String.contains s '%'
                  then String.sub s 0 (String.index s '%')
                  else s
                in
                let tzkt =
                  Printf.sprintf
                    "<a href='https://tzkt.io/%s/operations' \
                     target=_blank>TzKT</a>"
                    s
                in
                if Base.String.is_prefix s ~prefix:"tz"
                then tzkt
                else
                  Printf.sprintf
                    "<a href='explorer.html?address=%s'>SmartPy Explorer</a>, \
                     <a href='#' \
                     onClick='window.open(\"https://better-call.dev/search?text=%s\")'>Better \
                     Call Dev</a>, %s"
                    s s tzkt
              in
              let path = String.concat "." (List.rev path) in
              let path = Value.Typed.string path in
              [`V path; `V (Value.Typed.address address); `T explore])
            addresses
        in
        Printf.sprintf
          "<h4>Addresses</h4><div id='storageAddressesDiv'>%s</div>"
          (Printer.render_record_list
             (["Path"; "Address"; "Explore"], addresses)
             (function
               | `V s ->
                   Printer.value_to_string ~noEmptyList:true
                     ~options:Printer.Options.htmlStripStrings s
               | `T t -> t))
  in
  SmartDom.setText "storageDiv"
    (Printf.sprintf "<h3>Storage</h3><div id='storageDivInternal'>%s</div>%s%s"
       (Printer.value_to_string ~options:Printer.Options.html storage)
       addresses custom_metadata);
  SmartDom.setText "messageBuilder" editor;
  show_full_contract ~config tstorage tparameter
    (mtparameter, parameterAnnot)
    mtstorage init_storage code;
  let operations =
    String.concat "<hr>"
      (List.rev_map
         (show_operation ~config ~pp_mich ~tparameter getStorage)
         operations)
  in
  SmartDom.setText "operationsList" operations

let explorerShowContractInfos ~config address balance =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let lines =
    [
      ("Address", Value.Typed.address address)
    ; ( "Balance"
      , Value.Typed.mutez (Bigint.of_string ~msg:"contractInfos" balance) )
    ]
  in
  SmartDom.setText "contractDataDiv"
    (Printer.value_to_string ~options:Printer.Options.html
       (Value.Typed.record lines))

let explore ~config ~address ~json ~operations =
  let json = json_getter (Yojson.Safe.from_string json) in
  let module M = (val json : JsonGetter) in
  let open M in
  explorerShowContractInfos ~config address (string "balance");
  let script = json_sub json "script" in
  let module Script = (val script : JsonGetter) in
  if Script.null
  then
    SmartDom.setText "messageBuilder"
      "<span style='color:grey;'>&mdash; no contract code &mdash;</span>"
  else
    let code = Script.get "code" in
    let storage = Script.get "storage" in
    let tparameter, tstorage, code =
      match code with
      | `List l -> (
          (* Filter out views *)
          let non_views =
            List.filter
              (function
                | `Assoc [("prim", `String "view"); _] -> false
                | _ -> true)
              l
          in
          match non_views with
          | [
            `Assoc [("prim", `String "parameter"); ("args", `List [parameter])]
          ; `Assoc [("prim", `String "storage"); ("args", `List [storage])]
          ; `Assoc [("prim", `String "code"); ("args", `List [code])]
          ] -> (parameter, storage, code)
          | _ -> assert false)
      | `Assoc _ -> assert false
      | _ -> assert false
    in
    let operations =
      match operations with
      | "" -> []
      | _ -> (
          match Yojson.Safe.from_string operations with
          | `List operations ->
              List.map (Tzkt.parse_api ~config address) operations
          | _ -> assert false)
    in
    let parse = Micheline.parse in
    messageBuilder ~config ~address (parse storage) (parse tstorage)
      (parse tparameter) (parse code) operations
