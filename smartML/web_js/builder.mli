(* Copyright 2019-2022 Smart Chain Arena LLC. *)

open Tools

val buildEntrypointCallJSON :
  entry_point:string -> compiled_parameter:Michelson.literal -> string

val buildTransferParametersJSON : compiled_parameter:Michelson.literal -> string

val buildTransferParametersMicheline :
  compiled_parameter:Michelson.literal -> string

val buildTransferParametersCLI :
     entry_point:string
  -> account:string
  -> destination:string
  -> compiled_parameter:Michelson.literal
  -> string
