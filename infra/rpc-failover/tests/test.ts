process.env.FAILOVER_CONFIG_FILE = "tests/test_config.json";

process.on('unhandledRejection', error => {
    // Print "unhandled rejections"
    console.log('unhandledRejection', JSON.stringify(error, null, 4));
});

import * as fs from 'fs';
import moxios from 'moxios';
import axios from 'axios';
import { prepare, runTask } from "../service";

prepare({
    "FAILOVER_ON": 2,
    "ALLOWED_BLOCK_AGE": 1000,
    "CONFIG_LOCATION": "tests/nginx.conf",
    "RELOAD_NGINX_CMD": "tests/bin/reload-nginx",
    "NODES": [
        "node-server1.test",
        "node-server2.test"
    ]
});

function assertConfig(name: string, expected: string) {
    expected = expected.trim();
    const content = fs.readFileSync("tests/nginx.conf", { encoding: "utf-8" }).trim();
    if (content !== expected) {
        console.error("Expected: \n", expected);
        console.error("But Received: \n", content);
        throw new Error(`[FAIL #${name}]`);
    }
    console.log(`[SUCCESS #${name}]`);
}

function buildConfig(server1: string, server2: string) {
    return `upstream mainnet {
        server node-server1.test:443 weight=4${server1 ? " " + server1 : ""};
        server node-server2.test:443 weight=3${server2 ? " " + server2 : ""};
        server mainnet-archive.smartpy.io:443 weight=1 down;
    }`
}

// Tear Down
function resetConfig() {
    const content = buildConfig("down", "down");
    fs.writeFileSync("tests/nginx.conf", content, { encoding: "utf-8" });
}

async function test () {
    resetConfig();
    const startTime = Date.now();
    try {
        moxios.install(axios);
        moxios.stubRequest('https://node-server1.test/chains/main/blocks/head/header', {
            status: 200,
            response: {
                timestamp: new Date(startTime).toISOString()
            }
        });
        moxios.stubRequest('https://node-server2.test/chains/main/blocks/head/header', {
            status: 502
        });

        assertConfig("TEST 1", buildConfig("down", "down"));

        await runTask();
        assertConfig("TEST 2", buildConfig("", "down"));

        moxios.uninstall(axios);
        moxios.install(axios);
        moxios.stubRequest('https://node-server1.test/chains/main/blocks/head/header', {
            status: 502,
        });
        moxios.stubRequest('https://node-server2.test/chains/main/blocks/head/header', {
            status: 200,
            response: {
                timestamp: new Date(startTime).toISOString()
            }
        });

        await runTask();
        assertConfig("TEST 3", buildConfig("", ""));

        await runTask();
        assertConfig("TEST 4", buildConfig("down", ""));

        moxios.uninstall(axios);
        moxios.install(axios);
        moxios.stubRequest('https://node-server2.test/chains/main/blocks/head/header', {
            status: 200,
            response: {
                timestamp: new Date(startTime).toISOString()
            }
        });
        await new Promise(r => setTimeout(r, 2000));
        moxios.stubRequest('https://node-server1.test/chains/main/blocks/head/header', {
            status: 200,
            response: {
                timestamp: new Date().toISOString()
            }
        });

        // Simulate that node2 got FROZEN
        await runTask();
        assertConfig("TEST 5", buildConfig("", "down"));

        moxios.uninstall(axios);
        moxios.install(axios);
        moxios.stubRequest('https://node-server1.test/chains/main/blocks/head/header', {
            status: 200,
            response: {
                timestamp: new Date().toISOString()
            }
        });
        moxios.stubRequest('https://node-server2.test/chains/main/blocks/head/header', {
            status: 200,
            response: {
                timestamp: new Date().toISOString()
            }
        });

        // Simulate that node 2 recovered
        await runTask();
        assertConfig("TEST 6", buildConfig("", ""));
    } catch(e: any) {
        console.error(e.message || e)
    }
    resetConfig();
};

test();
