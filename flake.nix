{
  nixConfig.bash-prompt-suffix = "🌿 ";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
  inputs.nixpkgs_old.url = "github:NixOS/nixpkgs/nixos-21.11";
  inputs.nixpkgs_ancient.url = "github:NixOS/nixpkgs/nixos-21.05";
  inputs.nixpkgs_unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, nixpkgs_old, nixpkgs_ancient, nixpkgs_unstable, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      rec {
        pkgs = nixpkgs.legacyPackages.${system};
        pkgs_old = nixpkgs_old.legacyPackages.${system};
        pkgs_ancient = nixpkgs_ancient.legacyPackages.${system};
        pkgs_unstable = nixpkgs_unstable.legacyPackages.${system};
        devShell = import ./shell.nix { inherit pkgs pkgs_old pkgs_ancient pkgs_unstable system; };
      }
    );
}
