{ pkgs, pkgs_old, pkgs_ancient, pkgs_unstable, system }:

with pkgs;

mkShell rec {
  name = "smartpy";

  buildInputs =
    let
      tezos =
        {
          x86_64-linux = callPackage ./env/pkgs/tezos-linux.nix { };
          aarch64-darwin = callPackage ./env/pkgs/tezos-mac.nix { };
          x86_64-darwin = callPackage ./env/pkgs/tezos-mac.nix { };
        }."${system}";
    in
    [
      asciidoctor
      binutils
      coreutils
      curl
      docker
      git
      gmp
      gnumake
      hidapi
      libev
      libffi
      lsof
      m4
      nodePackages.lerna
      nodePackages.prettier
      nodePackages.pnpm
      nodejs-16_x
      pkgs_old.ocaml-ng.ocamlPackages_4_13.merlin
      ocamlPackages.ocaml
      ocamlPackages.ocaml-lsp
      ocamlformat
      opam
      openssh
      perl
      pkgconfig
      pkgs_old.black
      pkgs_old.python39Packages.pdoc3
      python3
      rsync
      tezos
      tree
      which
      xdg_utils
      yapf
      yarn
      ((pkgs.emacsPackagesFor pkgs.emacs).emacsWithPackages (p: [ p.merlin p.tuareg ]))
    ];

  # Needed for hacl-star-raw on macOS:
  hardeningDisable = [ "strictoverflow" ];

  shellHook = ''
    # Allow opam to run 'curl' on non-NixOS:
    [ -z "$NIX_SSL_CERT_FILE" ] || export SSL_CERT_FILE="$NIX_SSL_CERT_FILE"

    # Prevent opam from running apt, brew, and similar pranks:
    export OPAMNODEPEXTS=1

    # Source opam environment, if any:
    [ -d _opam ] && eval $(opam env)
  '';

  SMARTPY_ENV="nix";
}
