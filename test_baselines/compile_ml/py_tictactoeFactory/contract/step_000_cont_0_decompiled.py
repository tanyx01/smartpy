import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, boards = sp.TMap(sp.TString, sp.TPair(sp.TPair(sp.TPair(sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), sp.TBool), sp.TPair(sp.TMap(sp.TString, sp.TString), sp.TInt)), sp.TPair(sp.TPair(sp.TInt, sp.TAddress), sp.TPair(sp.TAddress, sp.TInt)))), metaData = sp.TMap(sp.TString, sp.TString), paused = sp.TBool).layout((("admin", "boards"), ("metaData", "paused"))))

  @sp.entry_point
  def build(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TPair(sp.TAddress, sp.TAddress)))
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [s2209; s2210; s2211] =
          if __storage.paused
          then
            [ Eq(Compare(Sender, __storage.admin))
            ; l183
            ; Pair
                ( Pair(__storage.admin, __storage.boards)
                , Pair(__storage.metaData, __storage.paused)
                )
            ]
          else
            [ True
            ; l183
            ; Pair
                ( Pair(__storage.admin, __storage.boards)
                , Pair(__storage.metaData, __storage.paused)
                )
            ]
        let x2306 =
          if s2209
          then
            if Mem(Car(s2210), Cdr(Car(s2211)))
            then
              Failwith("WrongCondition: ~ (self.data.boards.contains(params.game))")
            else
              Pair
                ( Pair
                    ( Car(Car(s2211))
                    , Update(Car(s2210), Some_(Pair
                                                 ( Pair
                                                     ( Pair
                                                         ( [0: [0: 0; 1: 0; 2: 0]#map(int,int); 1: [0: 0; 1: 0; 2: 0]#map(int,int); 2: [0: 0; 1: 0; 2: 0]#map(int,int)]#map(int,map(int, int))
                                                         , False
                                                         )
                                                     , Pair
                                                         ( []#map(string,string)
                                                         , 0
                                                         )
                                                     )
                                                 , Pair
                                                     ( Pair
                                                         ( 1
                                                         , Car(Cdr(s2210))
                                                         )
                                                     , Pair
                                                         ( Cdr(Cdr(s2210))
                                                         , 0
                                                         )
                                                     )
                                                 )), Cdr(Car(s2211)))
                    )
                , Cdr(s2211)
                )
          else
            Failwith("WrongCondition: (~ self.data.paused) | (sp.sender == self.data.admin)")
        Pair(Nil<operation>, record_of_tree(..., x2306))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def deleteGame(self, params):
    sp.set_type(params, sp.TString)
    sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
    x19 = sp.local("x19", ((self.data.admin, sp.update_map(self.data.boards, params, sp.none)), (self.data.metaData, self.data.paused)))
    x2304 = sp.local("x2304", x19.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x2304)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def play(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TString, sp.TInt), sp.TPair(sp.TInt, sp.TInt)))
    x34 = sp.bind_block("x34"):
    with x34:
      sp.if self.data.paused:
        sp.failwith('WrongCondition: ~ self.data.paused')
      sp.else:
        with sp.some(self.data.boards[sp.fst(sp.fst(params))]).match_cases() as arg:
          with arg.match('None') as _l23:
            sp.failwith(15)
          with arg.match('Some') as s212:
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [s250; s251; s252] =
          if Eq(Compare(Cdr(Cdr(Cdr(s212))), 0))
          then
            match Get(Car(Car(r186)), __storage.boards) with
            | None _ -> Failwith(15)
            | Some s241 ->
                [ Not(Cdr(Car(Car(s241))))
                ; r186
                ; Pair
                    ( Pair(__storage.admin, __storage.boards)
                    , Pair(__storage.metaData, __storage.paused)
                    )
                ]
            end
          else
            [ False
            ; r186
            ; Pair
                ( Pair(__storage.admin, __storage.boards)
                , Pair(__storage.metaData, __storage.paused)
                )
            ]
        if s250
        then
          let [s273; s274; s275] =
            if Ge(Compare(Cdr(Car(s251)), 0))
            then
              [ Gt(Compare(3, Cdr(Car(s251)))); s251; s252 ]
            else
              [ False; s251; s252 ]
          if s273
          then
            let [s294; s295; s296] =
              if Ge(Compare(Car(Cdr(s274)), 0))
              then
                [ Gt(Compare(3, Car(Cdr(s274)))); s274; s275 ]
              else
                [ False; s274; s275 ]
            if s294
            then
              match Get(Car(Car(s295)), Cdr(Car(s296))) with
              | None _ -> Failwith(18)
              | Some s319 ->
                  if Eq(Compare(Cdr(Cdr(s295)), Car(Car(Cdr(s319)))))
                  then
                    match Get(Car(Car(s295)), Cdr(Car(s296))) with
                    | None _ -> Failwith(19)
                    | Some s353 ->
                        match Get(Cdr(Car(s295)), Car(Car(Car(s353)))) with
                        | None _ -> Failwith(19)
                        | Some s369 ->
                            match Get(Car(Cdr(s295)), s369) with
                            | None _ -> Failwith(19)
                            | Some s381 ->
                                if Eq(Compare(s381, 0))
                                then
                                  let [s461; s462] =
                                    if Eq(Compare(1, Cdr(Cdr(s295))))
                                    then
                                      match Get
                                              ( Car(Car(s295))
                                              , Cdr(Car(s296))
                                              ) with
                                      | None _ -> Failwith(21)
                                      | Some s448 ->
                                          if Eq(Compare
                                                  ( Sender
                                                  , Cdr(Car(Cdr(s448)))
                                                  ))
                                          then
                                            [ s295; s296 ]
                                          else
                                            Failwith("WrongCondition: sp.sender == self.data.boards[params.game].player1")
                                      end
                                    else
                                      match Get
                                              ( Car(Car(s295))
                                              , Cdr(Car(s296))
                                              ) with
                                      | None _ -> Failwith(23)
                                      | Some s417 ->
                                          if Eq(Compare
                                                  ( Sender
                                                  , Car(Cdr(Cdr(s417)))
                                                  ))
                                          then
                                            [ s295; s296 ]
                                          else
                                            Failwith("WrongCondition: sp.sender == self.data.boards[params.game].player2")
                                      end
                                  match Get(Car(Car(s461)), Cdr(Car(s462))) with
                                  | None _ -> Failwith(24)
                                  | Some s492 ->
                                      match Get
                                              ( Car(Car(s461))
                                              , Cdr(Car(s462))
                                              ) with
                                      | None _ -> Failwith(24)
                                      | Some s532 ->
                                          let x557 =
                                            Update(Car(Car(s461)), Some_(
                                          Pair
                                            ( Car(s492)
                                            , Pair
                                                ( Pair
                                                    ( Sub
                                                        ( 3
                                                        , Car(Car(Cdr(s532)))
                                                        )
                                                    , Cdr(Car(Cdr(s492)))
                                                    )
                                                , Cdr(Cdr(s492))
                                                )
                                            )), Cdr(Car(s462)))
                                          match Get(Car(Car(s461)), x557) with
                                          | None _ -> Failwith(25)
                                          | Some s572 ->
                                              match Get
                                                      ( Cdr(Car(s461))
                                                      , Car(Car(Car(s572)))
                                                      ) with
                                              | None _ -> Failwith(25)
                                              | Some s607 ->
                                                  let x661 =
                                                    Update(Car(Car(s461)), Some_(
                                                  Pair
                                                    ( Pair
                                                        ( Pair
                                                            ( Update(Cdr(Car(s461)), Some_(Update(Car(Cdr(s461)), Some_(Cdr(Cdr(s461))), s607)), Car(Car(Car(s572))))
                                                            , Cdr(Car(Car(s572)))
                                                            )
                                                        , Cdr(Car(s572))
                                                        )
                                                    , Cdr(s572)
                                                    )), x557)
                                                  match Get
                                                          ( Car(Car(s461))
                                                          , x661
                                                          ) with
                                                  | None _ -> Failwith(26)
                                                  | Some s676 ->
                                                      let x717 =
                                                        Pair
                                                          ( Pair
                                                              ( Car(Car(s462))
                                                              , Update(Car(Car(s461)), Some_(
                                                            Pair
                                                              ( Pair
                                                                  ( Car(Car(s676))
                                                                  , Pair
                                                                    ( Car(Cdr(Car(s676)))
                                                                    , 
                                                                    Add
                                                                    ( 1
                                                                    , Cdr(Cdr(Car(s676)))
                                                                    )
                                                                    )
                                                                  )
                                                              , Cdr(s676)
                                                              )), x661)
                                                              )
                                                          , Cdr(s462)
                                                          )
                                                      match Get
                                                              ( Car(Car(s461))
                                                              , Cdr(Car(x717))
                                                              ) with
                                                      | None _ ->
                                                          Failwith(27)
                                                      | Some s727 ->
                                                          match Get
                                                                  ( Cdr(Car(s461))
                                                                  , Car(Car(Car(s727)))
                                                                  ) with
                                                          | None _ ->
                                                              Failwith(27)
                                                          | Some s743 ->
                                                              match Get
                                                                    ( 0
                                                                    , s743
                                                                    ) with
                                                              | None _ ->
                                                                  Failwith(27)
                                                              | Some s751 ->
                                                                  let 
                                                                  [s854;
                                                                    s855;
                                                                    s856] =
                                                                    if Neq(
                                                                    Compare
                                                                    ( s751
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s461))
                                                                    , Cdr(Car(x717))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s779 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s461))
                                                                    , Car(Car(Car(s779)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s797 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s797
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s804 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s461))
                                                                    , Cdr(Car(x717))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s822 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s461))
                                                                    , Car(Car(Car(s822)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s838 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s838
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s846 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s846
                                                                    , s804
                                                                    ))
                                                                    ; s461
                                                                    ; x717
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s461
                                                                    ; x717
                                                                    ]
                                                                  let 
                                                                  [s952;
                                                                    s953;
                                                                    s954] =
                                                                    if s854
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s855))
                                                                    , Cdr(Car(s856))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s877 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s855))
                                                                    , Car(Car(Car(s877)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s895 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s895
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s902 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s855))
                                                                    , Cdr(Car(s856))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s920 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s855))
                                                                    , Car(Car(Car(s920)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s936 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s936
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(27)
                                                                    | Some s944 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s944
                                                                    , s902
                                                                    ))
                                                                    ; s855
                                                                    ; s856
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s855
                                                                    ; s856
                                                                    ]
                                                                  let 
                                                                  [s1067;
                                                                    s1068] =
                                                                    if s952
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s953))
                                                                    , Cdr(Car(s954))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(28)
                                                                    | Some s984 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s953))
                                                                    , Cdr(Car(s954))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(28)
                                                                    | Some s1013 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s953))
                                                                    , Car(Car(Car(s1013)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(28)
                                                                    | Some s1036 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1036
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(28)
                                                                    | Some s1047 ->
                                                                    [ s953
                                                                    ; 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s954))
                                                                    , Update(Car(Car(s953)), Some_(
                                                                    let 
                                                                    [x2314;
                                                                    x2315] =
                                                                    Unpair(2, s984)
                                                                    Pair
                                                                    ( x2314
                                                                    , 
                                                                    let 
                                                                    [x2322;
                                                                    x2323] =
                                                                    Unpair(2, x2315)
                                                                    Pair
                                                                    ( x2322
                                                                    , 
                                                                    let 
                                                                    [x2330;
                                                                    x2331] =
                                                                    Unpair(2, x2323)
                                                                    Pair
                                                                    ( x2330
                                                                    , 
                                                                    let _ =
                                                                    x2331
                                                                    s1047
                                                                    )
                                                                    )
                                                                    )), Cdr(Car(s954)))
                                                                    )
                                                                    , Cdr(s954)
                                                                    )
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ s953
                                                                    ; s954
                                                                    ]
                                                                  match 
                                                                  Get
                                                                    ( Car(Car(s1067))
                                                                    , Cdr(Car(s1068))
                                                                    ) with
                                                                  | None _ ->
                                                                    Failwith(29)
                                                                  | Some s1083 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1083)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1094 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1067))
                                                                    , s1094
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1106 ->
                                                                    let 
                                                                    [s1207;
                                                                    s1208;
                                                                    s1209] =
                                                                    if Neq(
                                                                    Compare
                                                                    ( s1106
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1067))
                                                                    , Cdr(Car(s1068))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1134 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(Car(s1134)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1144 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1067))
                                                                    , s1144
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1158 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1067))
                                                                    , Cdr(Car(s1068))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1176 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1176)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1187 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1067))
                                                                    , s1187
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1199 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1199
                                                                    , s1158
                                                                    ))
                                                                    ; s1067
                                                                    ; s1068
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1067
                                                                    ; s1068
                                                                    ]
                                                                    let 
                                                                    [s1303;
                                                                    s1304;
                                                                    s1305] =
                                                                    if s1207
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1208))
                                                                    , Cdr(Car(s1209))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1230 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(Car(s1230)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1240 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1208))
                                                                    , s1240
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1254 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1208))
                                                                    , Cdr(Car(s1209))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1272 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1272)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1283 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1208))
                                                                    , s1283
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(29)
                                                                    | Some s1295 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1295
                                                                    , s1254
                                                                    ))
                                                                    ; s1208
                                                                    ; s1209
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1208
                                                                    ; s1209
                                                                    ]
                                                                    let 
                                                                    [s1417;
                                                                    s1418] =
                                                                    if s1303
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1304))
                                                                    , Cdr(Car(s1305))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(30)
                                                                    | Some s1335 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1304))
                                                                    , Cdr(Car(s1305))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(30)
                                                                    | Some s1364 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1364)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(30)
                                                                    | Some s1378 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1304))
                                                                    , s1378
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(30)
                                                                    | Some s1397 ->
                                                                    [ s1304
                                                                    ; 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s1305))
                                                                    , Update(Car(Car(s1304)), Some_(
                                                                    let 
                                                                    [x2312;
                                                                    x2313] =
                                                                    Unpair(2, s1335)
                                                                    Pair
                                                                    ( x2312
                                                                    , 
                                                                    let 
                                                                    [x2320;
                                                                    x2321] =
                                                                    Unpair(2, x2313)
                                                                    Pair
                                                                    ( x2320
                                                                    , 
                                                                    let 
                                                                    [x2328;
                                                                    x2329] =
                                                                    Unpair(2, x2321)
                                                                    Pair
                                                                    ( x2328
                                                                    , 
                                                                    let _ =
                                                                    x2329
                                                                    s1397
                                                                    )
                                                                    )
                                                                    )), Cdr(Car(s1305)))
                                                                    )
                                                                    , Cdr(s1305)
                                                                    )
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ s1304
                                                                    ; s1305
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1417))
                                                                    , Cdr(Car(s1418))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1433 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1433)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1444 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1444
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1452 ->
                                                                    let 
                                                                    [s1542;
                                                                    s1543;
                                                                    s1544] =
                                                                    if Neq(
                                                                    Compare
                                                                    ( s1452
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1417))
                                                                    , Cdr(Car(s1418))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1480 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(Car(s1480)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1490 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s1490
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1497 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1417))
                                                                    , Cdr(Car(s1418))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1515 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1515)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1526 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1526
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1534 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1534
                                                                    , s1497
                                                                    ))
                                                                    ; s1417
                                                                    ; s1418
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1417
                                                                    ; s1418
                                                                    ]
                                                                    let 
                                                                    [s1627;
                                                                    s1628;
                                                                    s1629] =
                                                                    if s1542
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1543))
                                                                    , Cdr(Car(s1544))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1565 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(Car(s1565)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1575 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s1575
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1582 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1543))
                                                                    , Cdr(Car(s1544))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1600 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1600)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1611 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1611
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(31)
                                                                    | Some s1619 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1619
                                                                    , s1582
                                                                    ))
                                                                    ; s1543
                                                                    ; s1544
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1543
                                                                    ; s1544
                                                                    ]
                                                                    let 
                                                                    [s1733;
                                                                    s1734] =
                                                                    if s1627
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1628))
                                                                    , Cdr(Car(s1629))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(32)
                                                                    | Some s1659 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1628))
                                                                    , Cdr(Car(s1629))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(32)
                                                                    | Some s1688 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1688)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(32)
                                                                    | Some s1702 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1702
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(32)
                                                                    | Some s1713 ->
                                                                    [ s1628
                                                                    ; 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s1629))
                                                                    , Update(Car(Car(s1628)), Some_(
                                                                    let 
                                                                    [x2310;
                                                                    x2311] =
                                                                    Unpair(2, s1659)
                                                                    Pair
                                                                    ( x2310
                                                                    , 
                                                                    let 
                                                                    [x2318;
                                                                    x2319] =
                                                                    Unpair(2, x2311)
                                                                    Pair
                                                                    ( x2318
                                                                    , 
                                                                    let 
                                                                    [x2326;
                                                                    x2327] =
                                                                    Unpair(2, x2319)
                                                                    Pair
                                                                    ( x2326
                                                                    , 
                                                                    let _ =
                                                                    x2327
                                                                    s1713
                                                                    )
                                                                    )
                                                                    )), Cdr(Car(s1629)))
                                                                    )
                                                                    , Cdr(s1629)
                                                                    )
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ s1628
                                                                    ; s1629
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1733))
                                                                    , Cdr(Car(s1734))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1749 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1749)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1760 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s1760
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1768 ->
                                                                    let 
                                                                    [s1858;
                                                                    s1859;
                                                                    s1860] =
                                                                    if Neq(
                                                                    Compare
                                                                    ( s1768
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1733))
                                                                    , Cdr(Car(s1734))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1796 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(Car(s1796)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1806 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s1806
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1813 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1733))
                                                                    , Cdr(Car(s1734))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1831 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1831)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1842 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s1842
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1850 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1850
                                                                    , s1813
                                                                    ))
                                                                    ; s1733
                                                                    ; s1734
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1733
                                                                    ; s1734
                                                                    ]
                                                                    let 
                                                                    [s1943;
                                                                    s1944;
                                                                    s1945] =
                                                                    if s1858
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1859))
                                                                    , Cdr(Car(s1860))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1881 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(Car(s1881)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1891 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1891
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1898 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1859))
                                                                    , Cdr(Car(s1860))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1916 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1916)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1927 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s1927
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(33)
                                                                    | Some s1935 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1935
                                                                    , s1898
                                                                    ))
                                                                    ; s1859
                                                                    ; s1860
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1859
                                                                    ; s1860
                                                                    ]
                                                                    let 
                                                                    [s2049;
                                                                    s2050] =
                                                                    if s1943
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1944))
                                                                    , Cdr(Car(s1945))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(34)
                                                                    | Some s1975 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1944))
                                                                    , Cdr(Car(s1945))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(34)
                                                                    | Some s2004 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s2004)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(34)
                                                                    | Some s2018 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s2018
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(34)
                                                                    | Some s2029 ->
                                                                    [ s1944
                                                                    ; 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s1945))
                                                                    , Update(Car(Car(s1944)), Some_(
                                                                    let 
                                                                    [x2308;
                                                                    x2309] =
                                                                    Unpair(2, s1975)
                                                                    Pair
                                                                    ( x2308
                                                                    , 
                                                                    let 
                                                                    [x2316;
                                                                    x2317] =
                                                                    Unpair(2, x2309)
                                                                    Pair
                                                                    ( x2316
                                                                    , 
                                                                    let 
                                                                    [x2324;
                                                                    x2325] =
                                                                    Unpair(2, x2317)
                                                                    Pair
                                                                    ( x2324
                                                                    , 
                                                                    let _ =
                                                                    x2325
                                                                    s2029
                                                                    )
                                                                    )
                                                                    )), Cdr(Car(s1945)))
                                                                    )
                                                                    , Cdr(s1945)
                                                                    )
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    else
                                                                    [ s1944
                                                                    ; s1945
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s2049))
                                                                    , Cdr(Car(s2050))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(35)
                                                                    | Some s2065 ->
                                                                    let 
                                                                    [s2099;
                                                                    s2100;
                                                                    s2101] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( Cdr(Cdr(Car(s2065)))
                                                                    , 9
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s2049))
                                                                    , Cdr(Car(s2050))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(35)
                                                                    | Some s2090 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( Cdr(Cdr(Cdr(s2090)))
                                                                    , 0
                                                                    ))
                                                                    ; s2049
                                                                    ; s2050
                                                                    ]
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s2049
                                                                    ; s2050
                                                                    ]
                                                                    if s2099
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s2100))
                                                                    , Cdr(Car(s2101))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(36)
                                                                    | Some s2126 ->
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s2101))
                                                                    , Update(Car(Car(s2100)), Some_(
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(Car(s2126)))
                                                                    , True
                                                                    )
                                                                    , Cdr(Car(s2126))
                                                                    )
                                                                    , Cdr(s2126)
                                                                    )), Cdr(Car(s2101)))
                                                                    )
                                                                    , Cdr(s2101)
                                                                    )
                                                                    end
                                                                    else
                                                                    s2101
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                  end
                                                              end
                                                          end
                                                      end
                                                  end
                                              end
                                          end
                                      end
                                  end
                                else
                                  Failwith("WrongCondition: self.data.boards[params.game].deck[params.i][params.j] == 0")
                            end
                        end
                    end
                  else
                    Failwith("WrongCondition: params.move == self.data.boards[params.game].nextPlayer")
              end
            else
              Failwith("WrongCondition: (params.j >= 0) & (params.j < 3)")
          else
            Failwith("WrongCondition: (params.i >= 0) & (params.i < 3)")
        else
          Failwith("WrongCondition: (self.data.boards[params.game].winner == 0) & (~ self.data.boards[params.game].draw)")]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))

    x2302 = sp.local("x2302", x34.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x2302)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def setGameMetaData(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TPair(sp.TString, sp.TString)))
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [s100; s101; s102] =
          if Eq(Compare(Sender, __storage.admin))
          then
            [ True
            ; l5
            ; Pair
                ( Pair(__storage.admin, __storage.boards)
                , Pair(__storage.metaData, __storage.paused)
                )
            ]
          else
            match Get(Car(l5), __storage.boards) with
            | None _ -> Failwith(39)
            | Some s89 ->
                [ Eq(Compare(Sender, Cdr(Car(Cdr(s89)))))
                ; l5
                ; Pair
                    ( Pair(__storage.admin, __storage.boards)
                    , Pair(__storage.metaData, __storage.paused)
                    )
                ]
            end
        let x2300 =
          if s100
          then
            match Get(Car(s101), Cdr(Car(s102))) with
            | None _ -> Failwith(42)
            | Some s129 ->
                Pair
                  ( Pair
                      ( Car(Car(s102))
                      , Update(Car(s101), Some_(Pair
                                                  ( Pair
                                                      ( Car(Car(s129))
                                                      , Pair
                                                          ( Update(Car(Cdr(s101)), Some_(Cdr(Cdr(s101))), Car(Cdr(Car(s129))))
                                                          , Cdr(Cdr(Car(s129)))
                                                          )
                                                      )
                                                  , Cdr(s129)
                                                  )), Cdr(Car(s102)))
                      )
                  , Cdr(s102)
                  )
            end
          else
            Failwith("WrongCondition: (sp.sender == self.data.admin) | (sp.sender == self.data.boards[params.game].player1)")
        Pair(Nil<operation>, record_of_tree(..., x2300))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def setMetaData(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TString))
    sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
    x59 = sp.local("x59", ((self.data.admin, self.data.boards), (sp.update_map(self.data.metaData, sp.fst(params), sp.some(sp.snd(params))), self.data.paused)))
    x2298 = sp.local("x2298", x59.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x2298)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def setPause(self, params):
    sp.set_type(params, sp.TBool)
    sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
    x76 = sp.local("x76", ((self.data.admin, self.data.boards), (self.data.metaData, params)))
    x2296 = sp.local("x2296", x76.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x2296)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
