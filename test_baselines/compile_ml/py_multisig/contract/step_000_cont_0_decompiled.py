import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(multisigs = sp.TMap(sp.TInt, sp.TPair(sp.TPair(sp.TPair(sp.TMutez, sp.TList(sp.TPair(sp.TPair(sp.TInt, sp.TPair(sp.TBool, sp.TList(sp.TPair(sp.TBool, sp.TPair(sp.TAddress, sp.TInt))))), sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt))))), sp.TPair(sp.TInt, sp.TString)), sp.TPair(sp.TPair(sp.TBool, sp.TAddress), sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt))))), nbMultisigs = sp.TInt).layout(("multisigs", "nbMultisigs")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TPair(sp.TPair(sp.TPair(sp.TMutez, sp.TList(sp.TPair(sp.TPair(sp.TInt, sp.TPair(sp.TBool, sp.TList(sp.TPair(sp.TBool, sp.TPair(sp.TAddress, sp.TInt))))), sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt))))), sp.TPair(sp.TInt, sp.TString)), sp.TPair(sp.TPair(sp.TBool, sp.TAddress), sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)))), Right = sp.TPair(sp.TInt, sp.TPair(sp.TString, sp.TAddress))).layout(("Left", "Right")))
    x47 = sp.bind_block("x47"):
    with x47:
      with params.match_cases() as arg:
        with arg.match('Left') as l3:
          sp.result((sp.list([]), (sp.update_map(self.data.multisigs, self.data.nbMultisigs, sp.some(l3)), 1 + self.data.nbMultisigs)))
        with arg.match('Right') as r4:
          sp.verify(sp.sender == sp.snd(sp.snd(r4)), 'WrongCondition: params.id == sp.sender')
          with sp.some(self.data.multisigs[sp.fst(r4)]).match_cases() as arg:
            with arg.match('None') as _l3:
              sp.failwith(11)
            with arg.match('Some') as s31:
              sp.verify(sp.fst(sp.snd(r4)) == sp.snd(sp.snd(sp.fst(s31))), 'WrongCondition: params.contractName == self.data.multisigs[params.contractId].name')
              with sp.some(self.data.multisigs[sp.fst(r4)]).match_cases() as arg:
                with arg.match('None') as _l10:
                  sp.failwith(36)
                with arg.match('Some') as s63:
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [r625; r626; r627; r628] =
          map [ Cdr(Car(Car(s63)))
              ; Nil<operation>
              ; r4
              ; Pair(__storage.multisigs, __storage.nbMultisigs)
              ]
          step s72; s73; s74; s75 ->
            let [r600; r601; r602; r603; r604] =
              map [ Cdr(Cdr(Car(s72))); s72; s73; s74; s75 ]
              step s80; s81; s82; s83; s84 ->
                let [s595; s596; s597; s598; s599] =
                  if Eq(Compare(Car(Cdr(s80)), Cdr(Cdr(s83))))
                  then
                    if Car(s80)
                    then
                      Failwith("WrongCondition: ~ participant.value.hasVoted")
                    else
                      let x120 =
                        let [x914; x915] = Unpair(2, s80)
                        let _ = x914
                        Pair(True, x915)
                      let x125 =
                        let [x912; x913] = Unpair(2, s81)
                        Pair
                          ( x912
                          , let [x920; x921] = Unpair(2, x913)
                            Pair
                              ( x920
                              , let [x926; x927] = Unpair(2, x921)
                                Pair
                                  ( x926
                                  , let _ = x927
                                    Add(Cdr(Cdr(x120)), Cdr(Cdr(Cdr(s81))))
                                  )
                              )
                          )
                      let x131 =
                        let [x910; x911] = Unpair(2, x125)
                        Pair
                          ( x910
                          , let [x918; x919] = Unpair(2, x911)
                            Pair
                              ( x918
                              , let [x924; x925] = Unpair(2, x919)
                                let _ = x924
                                Pair(Add(1, Car(Cdr(Cdr(x125)))), x925)
                              )
                          )
                      let [s155; s156; s157; s158; s159; s160] =
                        if Car(Cdr(Car(x131)))
                        then
                          [ False; x120; x131; s82; s83; s84 ]
                        else
                          [ Le(Compare
                                 ( Car(Car(Cdr(x131)))
                                 , Car(Cdr(Cdr(x131)))
                                 ))
                          ; x120
                          ; x131
                          ; s82
                          ; s83
                          ; s84
                          ]
                      let [s179; s180; s181; s182; s183; s184] =
                        if s155
                        then
                          [ Le(Compare
                                 ( Cdr(Car(Cdr(s157)))
                                 , Cdr(Cdr(Cdr(s157)))
                                 ))
                          ; s156
                          ; s157
                          ; s158
                          ; s159
                          ; s160
                          ]
                        else
                          [ False; s156; s157; s158; s159; s160 ]
                      let [s590; s591; s592; s593; s594] =
                        if s179
                        then
                          match Get(Car(s183), Car(s184)) with
                          | None _ -> Failwith(26)
                          | Some s227 ->
                              let x258 =
                                let [x908; x909] = Unpair(2, s184)
                                let _ = x908
                                Pair
                                  ( Update(Car(s183), Some_(let [x906; x907] =
                                                              Unpair(2, s227)
                                                            Pair
                                                              ( x906
                                                              , let [x916;
                                                                    x917] =
                                                                  Unpair(2, x907)
                                                                Pair
                                                                  ( x916
                                                                  , let 
                                                                    [x922;
                                                                    x923] =
                                                                    Unpair(2, x917)
                                                                    Pair
                                                                    ( x922
                                                                    , 
                                                                    let 
                                                                    [x928;
                                                                    x929] =
                                                                    Unpair(2, x923)
                                                                    Pair
                                                                    ( x928
                                                                    , 
                                                                    let _ =
                                                                    x929
                                                                    Add
                                                                    ( Car(Car(s181))
                                                                    , Cdr(Cdr(Cdr(Cdr(s227))))
                                                                    )
                                                                    )
                                                                    )
                                                                  )
                                                              )), Car(s184))
                                  , x909
                                  )
                              match Get(Car(s183), Car(x258)) with
                              | None _ -> Failwith(27)
                              | Some s277 ->
                                  let x308 =
                                    let [x904; x905] = Unpair(2, x258)
                                    let _ = x904
                                    Pair
                                      ( Update(Car(s183), Some_(Pair
                                                                  ( Pair
                                                                    ( Car(Car(s277))
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Add
                                                                    ( 1
                                                                    , Car(Cdr(Car(s277)))
                                                                    )
                                                                    , Cdr(Cdr(Car(s277)))
                                                                    )
                                                                    )
                                                                  , Cdr(s277)
                                                                  )), Car(x258))
                                      , x905
                                      )
                                  match Get(Car(s183), Car(x308)) with
                                  | None _ -> Failwith(28)
                                  | Some s324 ->
                                      let [s389; s390; s391; s392; s393; s394
                                            ] =
                                        if Car(Car(Cdr(s324)))
                                        then
                                          [ False
                                          ; s180
                                          ; Pair
                                              ( Pair
                                                  ( Car(Car(s181))
                                                  , Pair
                                                      ( True
                                                      , Cdr(Cdr(Car(s181)))
                                                      )
                                                  )
                                              , Cdr(s181)
                                              )
                                          ; s182
                                          ; s183
                                          ; x308
                                          ]
                                        else
                                          match Get(Car(s183), Car(x308)) with
                                          | None _ -> Failwith(28)
                                          | Some s349 ->
                                              match Get(Car(s183), Car(x308)) with
                                              | None _ -> Failwith(28)
                                              | Some s376 ->
                                                  [ Le(Compare
                                                         ( Car(Cdr(Cdr(s376)))
                                                         , Car(Cdr(Car(s349)))
                                                         ))
                                                  ; s180
                                                  ; Pair
                                                      ( Pair
                                                          ( Car(Car(s181))
                                                          , Pair
                                                              ( True
                                                              , Cdr(Cdr(Car(s181)))
                                                              )
                                                          )
                                                      , Cdr(s181)
                                                      )
                                                  ; s182
                                                  ; s183
                                                  ; x308
                                                  ]
                                              end
                                          end
                                      let [s449; s450; s451; s452; s453; s454
                                            ] =
                                        if s389
                                        then
                                          match Get(Car(s393), Car(s394)) with
                                          | None _ -> Failwith(28)
                                          | Some s411 ->
                                              match Get(Car(s393), Car(s394)) with
                                              | None _ -> Failwith(28)
                                              | Some s437 ->
                                                  [ Le(Compare
                                                         ( Car(Cdr(Cdr(Cdr(s437))))
                                                         , Cdr(Cdr(Cdr(Cdr(s411))))
                                                         ))
                                                  ; s390
                                                  ; s391
                                                  ; s392
                                                  ; s393
                                                  ; s394
                                                  ]
                                              end
                                          end
                                        else
                                          [ False
                                          ; s390
                                          ; s391
                                          ; s392
                                          ; s393
                                          ; s394
                                          ]
                                      let [s585; s586; s587; s588; s589] =
                                        if s449
                                        then
                                          match Get(Car(s453), Car(s454)) with
                                          | None _ -> Failwith(30)
                                          | Some s480 ->
                                              let x521 =
                                                let [x902; x903] =
                                                  Unpair(2, s454)
                                                let _ = x902
                                                Pair
                                                  ( Update(Car(s453), Some_(
                                                Pair
                                                  ( Car(s480)
                                                  , Pair
                                                      ( Pair
                                                          ( True
                                                          , Cdr(Car(Cdr(s480)))
                                                          )
                                                      , Cdr(Cdr(s480))
                                                      )
                                                  )), Car(s454))
                                                  , x903
                                                  )
                                              match Get(Car(s453), Car(x521)) with
                                              | None _ -> Failwith(31)
                                              | Some s531 ->
                                                  match Contract(Cdr(Car(Cdr(s531))), unit) with
                                                  | None _ -> Failwith(31)
                                                  | Some s542 ->
                                                      match Get
                                                              ( Car(s453)
                                                              , Car(x521)
                                                              ) with
                                                      | None _ ->
                                                          Failwith(31)
                                                      | Some s567 ->
                                                          [ s450
                                                          ; s451
                                                          ; Cons
                                                              ( Transfer_tokens(Unit, Car(Car(Car(s567))), s542)
                                                              , s452
                                                              )
                                                          ; s453
                                                          ; x521
                                                          ]
                                                      end
                                                  end
                                              end
                                          end
                                        else
                                          [ s450; s451; s452; s453; s454 ]
                                      [ s585; s586; s587; s588; s589 ]
                                  end
                              end
                          end
                        else
                          [ s180; s181; s182; s183; s184 ]
                      [ s590; s591; s592; s593; s594 ]
                  else
                    [ s80; s81; s82; s83; s84 ]
                [ s595; s596; s597; s598; s599 ]
            [ Pair
                ( Pair(Car(Car(r601)), Pair(Car(Cdr(Car(r601))), r600))
                , Cdr(r601)
                )
            ; r602
            ; r603
            ; r604
            ]
        match Get(Car(r627), Car(r628)) with
        | None _ -> Failwith(14)
        | Some s651 ->
            [ r626
            ; let [x900; x901] = Unpair(2, r628)
              let _ = x900
              Pair
                ( Update(Car(r627), Some_(Pair
                                            ( Pair
                                                ( Pair
                                                    ( Car(Car(Car(s651)))
                                                    , r625
                                                    )
                                                , Cdr(Car(s651))
                                                )
                                            , Cdr(s651)
                                            )), Car(r628))
                , x901
                )
            ]
        end]')
                  sp.failwith(sp.build_lambda(ferror)(sp.unit))



    s711 = sp.local("s711", sp.fst(x47.value))
    s712 = sp.local("s712", sp.snd(x47.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s712)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
