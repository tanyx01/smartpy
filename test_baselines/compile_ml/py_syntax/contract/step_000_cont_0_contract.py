import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(aaa = sp.TSet(sp.TNat), abc = sp.TList(sp.TOption(sp.TIntOrNat)), abca = sp.TMap(sp.TIntOrNat, sp.TOption(sp.TIntOrNat)), acb = sp.TString, b = sp.TBool, ddd = sp.TList(sp.TIntOrNat), f = sp.TBool, h = sp.TBytes, i = sp.TInt, m = sp.TMap(sp.TInt, sp.TInt), n = sp.TNat, pkh = sp.TKeyHash, s = sp.TString, toto = sp.TString).layout(((("aaa", ("abc", "abca")), (("acb", "b"), ("ddd", "f"))), (("h", ("i", "m")), (("n", "pkh"), ("s", "toto"))))))
    self.init(aaa = sp.set([1, 2, 3]),
              abc = [sp.some(123), sp.none],
              abca = {0 : sp.some(123), 1 : sp.none},
              acb = 'toto',
              b = True,
              ddd = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
              f = False,
              h = sp.bytes('0xaabb'),
              i = 7,
              m = {},
              n = 123,
              pkh = sp.key_hash('tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk'),
              s = 'abc',
              toto = 'ABC')

  @sp.entry_point
  def comparisons(self):
    sp.verify(self.data.i <= 123)
    sp.verify((2 + self.data.i) == 12)
    sp.verify((2 + self.data.i) != 1234)
    sp.verify((self.data.i + 4) != 123)
    sp.verify((self.data.i - 5) < 123)
    sp.verify((7 - self.data.i) < 123)
    sp.verify(self.data.i > 3)
    sp.verify((4 * self.data.i) > 3)
    sp.verify((self.data.i * 5) > 3)
    sp.verify(self.data.i >= 3)
    sp.verify(self.data.i > 3)
    sp.verify(self.data.i >= 3)
    sp.verify(self.data.i < 3000)
    sp.verify(self.data.i <= 3000)
    sp.verify(self.data.b & True)
    sp.if self.data.i > 3:
      sp.verify(self.data.i > 4)

  @sp.entry_point
  def iterations(self):
    x = sp.local("x", self.data.i)
    x.value = 0
    x.value = 1
    x.value = 2
    x.value = 3
    x.value = 4
    sp.for i in sp.range(0, 5):
      x.value = i.value
    self.data.i = sp.to_int(self.data.n)
    self.data.i = 5
    sp.while self.data.i <= 42:
      self.data.i += 2
    sp.if self.data.i <= 123:
      x.value = 12
      self.data.i += x.value
    sp.else:
      x.value = 5
      self.data.i = x.value

  @sp.entry_point
  def localVariable(self):
    x = sp.local("x", self.data.i)
    x.value *= 2
    self.data.i = 10
    self.data.i = x.value

  @sp.entry_point
  def myMessageName4(self):
    sp.for x in self.data.m.items():
      self.data.i += x.value.key * x.value.value

  @sp.entry_point
  def myMessageName5(self):
    sp.for x in self.data.m.keys():
      self.data.i += 2 * x.value

  @sp.entry_point
  def myMessageName6(self, params):
    sp.for x in self.data.m.values():
      self.data.i += 3 * x.value
    self.data.aaa.remove(2)
    self.data.aaa.add(12)
    self.data.abc.push(sp.some(16))
    self.data.abca[0] = sp.some(16)
    sp.if self.data.aaa.contains(12):
      self.data.m[42] = 43
    self.data.aaa.add(sp.as_nat(- params))

  @sp.entry_point
  def someComputations(self, params):
    sp.verify(self.data.i <= 123)
    self.data.i += params.y
    self.data.acb = params.x
    self.data.i = 100
    self.data.i -= 1
    sp.verify(sp.add(4, 5) == 9)
    sp.verify(sp.add(-4, 5) == 1)
    sp.verify(sp.add(5, -4) == 1)
    sp.verify(sp.add(-4, 5) == 1)
    sp.verify(sp.mul(4, 5) == 20)
    sp.verify(sp.mul(-4, 5) == (-20))
    sp.verify(sp.mul(5, -4) == (-20))
    sp.verify(sp.mul(-4, 5) == (-20))

sp.add_compilation_target("test", Contract())