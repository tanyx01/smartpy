import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y")))
    self.init(x = 0,
              y = 0)

  @sp.entry_point
  def test_commands(self, params):
    sp.transfer(42, params.amount, params.destination)
    sp.set_delegate(params.delegate)
    sp.operations().push(create contract ....operation)

  @sp.entry_point
  def test_transfer(self, params):
    sp.for _op in sp.list([sp.transfer_operation(42, params.amount, params.destination)]):
      sp.operations().push(_op)

  @sp.entry_point
  def test_create_contract(self):
    create_op = sp.local("create_op", create contract ...)
    sp.for _op in sp.list([create_op.value.operation]):
      sp.operations().push(_op)

  @sp.entry_point
  def test_set_delegate(self, params):
    sp.for _op in sp.list([sp.set_delegate_operation(params)]):
      sp.operations().push(_op)

sp.add_compilation_target("test", Contract())