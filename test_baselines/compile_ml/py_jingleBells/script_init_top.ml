#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/py_jingleBells.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/compile_ml/py_jingleBells/scenario.json"
else
  exit 1;;