open SmartML

module Contract = struct
  let%entry_point sing params =
    List.iter (fun i ->
      if data.verse = 16 then
        (
          data.played <- data.played + 1;
          data.current <- "";
          data.verse <- 0
        );
      if data.current <> "" then
        data.current <- data.current + "\n";
      let%mutable lyrics = (set_type_expr (Map.make [(0, "Dashing through the snow"); (1, "In a one-horse open sleigh"); (2, "O'er the fields we go"); (3, "Laughing all the way"); (4, "Bells on bob tail ring"); (5, "Making spirits bright"); (6, "What fun it is to ride and sing"); (7, "A sleighing song tonight!"); (8, "Jingle bells, jingle bells,"); (9, "Jingle all the way."); (10, "Oh! what fun it is to ride"); (11, "In a one-horse open sleigh."); (12, "Jingle bells, jingle bells,"); (13, "Jingle all the way;"); (14, "Oh! what fun it is to ride"); (15, "In a one-horse open sleigh.")]) (map intOrNat string)) in ();
      data.current <- data.current + (Map.get lyrics data.verse);
      data.verse <- data.verse + 1
    ) (range 0 params.verses 1)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {current = string; played = intOrNat; rules = list string; verse = intOrNat}]
      ~storage:[%expr
                 {current = "";
                  played = 0;
                  rules = ["Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"];
                  verse = 0}]
      [sing]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())