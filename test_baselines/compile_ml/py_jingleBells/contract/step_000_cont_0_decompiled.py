import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(current = sp.TString, played = sp.TInt, rules = sp.TList(sp.TString), verse = sp.TInt).layout((("current", "played"), ("rules", "verse"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TInt)
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [_; _; _; r146] =
          loop [ Gt(Compare(__parameter, 0))
               ; 0
               ; __parameter
               ; __parameter
               ; Pair
                   ( Pair(__storage.current, __storage.played)
                   , Pair(__storage.rules, __storage.verse)
                   )
               ]
          step s15; s16; s17; s18 ->
            let [s47; s48; s49; s50] =
              if Eq(Compare(Cdr(Cdr(s18)), 16))
              then
                [ s15
                ; s16
                ; s17
                ; Pair
                    ( Pair("", Add(1, Cdr(Car(s18))))
                    , let [x217; x218] = Unpair(2, Cdr(s18))
                      Pair(x217, let _ = x218
                                 0)
                    )
                ]
              else
                [ s15; s16; s17; s18 ]
            let [s80; s81; s82; s83] =
              if Neq(Compare(Car(Car(s50)), ""))
              then
                [ s47
                ; s48
                ; s49
                ; Pair
                    ( Pair(Concat2(Car(Car(s50)), "\n"), Cdr(Car(s50)))
                    , Cdr(s50)
                    )
                ]
              else
                [ s47; s48; s49; s50 ]
            match Get
                    ( Cdr(Cdr(s83))
                    , [0: "Dashing through the snow"; 1: "In a one-horse open sleigh"; 2: "O'er the fields we go"; 3: "Laughing all the way"; 4: "Bells on bob tail ring"; 5: "Making spirits bright"; 6: "What fun it is to ride and sing"; 7: "A sleighing song tonight!"; 8: "Jingle bells, jingle bells,"; 9: "Jingle all the way."; 10: "Oh! what fun it is to ride"; 11: "In a one-horse open sleigh."; 12: "Jingle bells, jingle bells,"; 13: "Jingle all the way;"; 14: "Oh! what fun it is to ride"; 15: "In a one-horse open sleigh."]#map(int,string)
                    ) with
            | None _ -> Failwith(15)
            | Some s109 ->
                let x124 =
                  Pair
                    ( Pair(Concat2(Car(Car(s83)), s109), Cdr(Car(s83)))
                    , Cdr(s83)
                    )
                let x136 = Add(1, s80)
                [ Gt(Compare(s81, x136))
                ; x136
                ; s81
                ; s82
                ; let [x215; x216] = Unpair(2, x124)
                  Pair
                    ( x215
                    , let [x219; x220] = Unpair(2, x216)
                      Pair(x219, let _ = x220
                                 Add(1, Cdr(Cdr(x124))))
                    )
                ]
            end
        Pair(Nil<operation>, record_of_tree(..., r146))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
