open SmartML

module Lambdas = struct
  let%entry_point abs_test params = data.abcd <- (private_ abs) params

  let%entry_point f () =
    let toto x = result (fst x + snd x) in
    let titi = apply_lambda 5 toto in
    data.value <- titi 8

  let flam = [%expr fun x -> result (322 * x)]

  let square_root =
    [%expr
      fun x ->
        verify (x >= 0) ~msg:"negative";
        let%mutable y = x in
        while y * y > x do
          y <- ((x / y) + y) / 2
        done;
        result y]

  let comp = [%expr fun p -> result (p.f p.x)]

  let abs = [%expr fun x -> if x > 0 then result x else result (-x)]

  let init () =
    let private_variables =
      [("flam", flam); ("square_root", square_root); ("abs", abs)]
    in
    Basics.build_contract
      ~private_variables
      ~storage:[%expr {value = 0; abcd = 0}]
      [f; abs_test]
end

let () =
  Target.register_test
    ~name:"Lambdas"
    [%actions
      h1 "Lambdas";
      let c = register_contract (Lambdas.init ()) in
      call c.abs_test 5;
      verify (c.data.abcd = 5);
      call c.abs_test (-42);
      verify (c.data.abcd = 42)]

let () = Target.register_compilation ~name:"contract" (Lambdas.init ())
