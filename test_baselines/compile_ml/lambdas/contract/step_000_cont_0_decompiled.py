import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(abcd = sp.TInt, value = sp.TInt).layout(("abcd", "value")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TInt, Right = sp.TUnit).layout(("Left", "Right")))
    def ferror(error):
      sp.failwith('[Error: to_cmd: (fun (x7 : nat) : nat ->
if Ge(Compare(x7, 0#nat))
then
  let [r151] =
    loop [ Gt(Compare(Mul(x7, x7), x7)); x7 ]
    step s38 ->
      match Ediv(x7, s38) with
      | None _ -> Failwith(19)
      | Some s51 ->
          match Ediv(Add(Car(s51), s38), 2#nat) with
          | None _ -> Failwith(19)
          | Some s60 ->
              [ Gt(Compare(Mul(Car(s60), Car(s60)), x7)); Car(s60) ]
          end
      end
  r151
else
  Failwith("negative"))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
