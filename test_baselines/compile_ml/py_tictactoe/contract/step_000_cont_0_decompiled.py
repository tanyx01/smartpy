import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), draw = sp.TBool, nbMoves = sp.TInt, nextPlayer = sp.TInt, winner = sp.TInt).layout((("deck", "draw"), ("nbMoves", ("nextPlayer", "winner")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)))
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [s22; s23; s24] =
          if Eq(Compare(__storage.winner, 0))
          then
            [ Not(__storage.draw)
            ; __parameter
            ; Pair
                ( Pair(__storage.deck, __storage.draw)
                , Pair
                    ( __storage.nbMoves
                    , Pair(__storage.nextPlayer, __storage.winner)
                    )
                )
            ]
          else
            [ False
            ; __parameter
            ; Pair
                ( Pair(__storage.deck, __storage.draw)
                , Pair
                    ( __storage.nbMoves
                    , Pair(__storage.nextPlayer, __storage.winner)
                    )
                )
            ]
        if s22
        then
          let [s43; s44; s45] =
            if Ge(Compare(Car(s23), 0))
            then
              [ Gt(Compare(3, Car(s23))); s23; s24 ]
            else
              [ False; s23; s24 ]
          if s43
          then
            let [s64; s65; s66] =
              if Ge(Compare(Car(Cdr(s44)), 0))
              then
                [ Gt(Compare(3, Car(Cdr(s44)))); s44; s45 ]
              else
                [ False; s44; s45 ]
            if s64
            then
              if Eq(Compare(Cdr(Cdr(s65)), Car(Cdr(Cdr(s66)))))
              then
                match Get(Car(s65), Car(Car(s66))) with
                | None _ -> Failwith(9)
                | Some s104 ->
                    match Get(Car(Cdr(s65)), s104) with
                    | None _ -> Failwith(9)
                    | Some s116 ->
                        if Eq(Compare(s116, 0))
                        then
                          match Get(Car(s65), Car(Car(s66))) with
                          | None _ -> Failwith(10)
                          | Some s148 ->
                              let x182 =
                                Pair
                                  ( Pair
                                      ( Update(Car(s65), Some_(Update(Car(Cdr(s65)), Some_(Cdr(Cdr(s65))), s148)), Car(Car(s66)))
                                      , Cdr(Car(s66))
                                      )
                                  , Cdr(s66)
                                  )
                              let x188 =
                                let [x923; x924] = Unpair(2, x182)
                                Pair
                                  ( x923
                                  , let [x935; x936] = Unpair(2, x924)
                                    let _ = x935
                                    Pair(Add(1, Car(Cdr(x182))), x936)
                                  )
                              let x199 =
                                let [x921; x922] = Unpair(2, x188)
                                Pair
                                  ( x921
                                  , let [x933; x934] = Unpair(2, x922)
                                    Pair
                                      ( x933
                                      , let [x945; x946] = Unpair(2, x934)
                                        let _ = x945
                                        Pair
                                          ( Sub(3, Car(Cdr(Cdr(x188))))
                                          , x946
                                          )
                                      )
                                  )
                              match Get(Car(s65), Car(Car(x199))) with
                              | None _ -> Failwith(13)
                              | Some s208 ->
                                  match Get(0, s208) with
                                  | None _ -> Failwith(13)
                                  | Some s216 ->
                                      let [s283; s284; s285] =
                                        if Neq(Compare(s216, 0))
                                        then
                                          match Get(Car(s65), Car(Car(x199))) with
                                          | None _ -> Failwith(13)
                                          | Some s243 ->
                                              match Get(1, s243) with
                                              | None _ -> Failwith(13)
                                              | Some s250 ->
                                                  match Get
                                                          ( Car(s65)
                                                          , Car(Car(x199))
                                                          ) with
                                                  | None _ -> Failwith(13)
                                                  | Some s267 ->
                                                      match Get(0, s267) with
                                                      | None _ ->
                                                          Failwith(13)
                                                      | Some s275 ->
                                                          [ Eq(Compare
                                                                 ( s275
                                                                 , s250
                                                                 ))
                                                          ; s65
                                                          ; x199
                                                          ]
                                                      end
                                                  end
                                              end
                                          end
                                        else
                                          [ False; s65; x199 ]
                                      let [s345; s346; s347] =
                                        if s283
                                        then
                                          match Get(Car(s284), Car(Car(s285))) with
                                          | None _ -> Failwith(13)
                                          | Some s305 ->
                                              match Get(2, s305) with
                                              | None _ -> Failwith(13)
                                              | Some s312 ->
                                                  match Get
                                                          ( Car(s284)
                                                          , Car(Car(s285))
                                                          ) with
                                                  | None _ -> Failwith(13)
                                                  | Some s329 ->
                                                      match Get(0, s329) with
                                                      | None _ ->
                                                          Failwith(13)
                                                      | Some s337 ->
                                                          [ Eq(Compare
                                                                 ( s337
                                                                 , s312
                                                                 ))
                                                          ; s284
                                                          ; s285
                                                          ]
                                                      end
                                                  end
                                              end
                                          end
                                        else
                                          [ False; s284; s285 ]
                                      let [s375; s376] =
                                        if s345
                                        then
                                          match Get(Car(s346), Car(Car(s347))) with
                                          | None _ -> Failwith(14)
                                          | Some s360 ->
                                              match Get(0, s360) with
                                              | None _ -> Failwith(14)
                                              | Some s367 ->
                                                  [ s346
                                                  ; let [x919; x920] =
                                                      Unpair(2, s347)
                                                    Pair
                                                      ( x919
                                                      , let [x931; x932] =
                                                          Unpair(2, x920)
                                                        Pair
                                                          ( x931
                                                          , let [x943; x944] =
                                                              Unpair(2, x932)
                                                            Pair
                                                              ( x943
                                                              , let _ = x944
                                                                s367
                                                              )
                                                          )
                                                      )
                                                  ]
                                              end
                                          end
                                        else
                                          [ s346; s347 ]
                                      match Get(0, Car(Car(s376))) with
                                      | None _ -> Failwith(15)
                                      | Some s386 ->
                                          match Get(Car(Cdr(s375)), s386) with
                                          | None _ -> Failwith(15)
                                          | Some s398 ->
                                              let [s465; s466; s467] =
                                                if Neq(Compare(s398, 0))
                                                then
                                                  match Get(1, Car(Car(s376))) with
                                                  | None _ -> Failwith(15)
                                                  | Some s418 ->
                                                      match Get
                                                              ( Car(Cdr(s375))
                                                              , s418
                                                              ) with
                                                      | None _ ->
                                                          Failwith(15)
                                                      | Some s432 ->
                                                          match Get
                                                                  ( 0
                                                                  , Car(Car(s376))
                                                                  ) with
                                                          | None _ ->
                                                              Failwith(15)
                                                          | Some s445 ->
                                                              match Get
                                                                    ( Car(Cdr(s375))
                                                                    , s445
                                                                    ) with
                                                              | None _ ->
                                                                  Failwith(15)
                                                              | Some s457 ->
                                                                  [ Eq(
                                                                  Compare
                                                                    ( s457
                                                                    , s432
                                                                    ))
                                                                  ; s375
                                                                  ; s376
                                                                  ]
                                                              end
                                                          end
                                                      end
                                                  end
                                                else
                                                  [ False; s375; s376 ]
                                              let [s527; s528; s529] =
                                                if s465
                                                then
                                                  match Get(2, Car(Car(s467))) with
                                                  | None _ -> Failwith(15)
                                                  | Some s480 ->
                                                      match Get
                                                              ( Car(Cdr(s466))
                                                              , s480
                                                              ) with
                                                      | None _ ->
                                                          Failwith(15)
                                                      | Some s494 ->
                                                          match Get
                                                                  ( 0
                                                                  , Car(Car(s467))
                                                                  ) with
                                                          | None _ ->
                                                              Failwith(15)
                                                          | Some s507 ->
                                                              match Get
                                                                    ( Car(Cdr(s466))
                                                                    , s507
                                                                    ) with
                                                              | None _ ->
                                                                  Failwith(15)
                                                              | Some s519 ->
                                                                  [ Eq(
                                                                  Compare
                                                                    ( s519
                                                                    , s494
                                                                    ))
                                                                  ; s466
                                                                  ; s467
                                                                  ]
                                                              end
                                                          end
                                                      end
                                                  end
                                                else
                                                  [ False; s466; s467 ]
                                              let s554 =
                                                if s527
                                                then
                                                  match Get(0, Car(Car(s529))) with
                                                  | None _ -> Failwith(16)
                                                  | Some s539 ->
                                                      match Get
                                                              ( Car(Cdr(s528))
                                                              , s539
                                                              ) with
                                                      | None _ ->
                                                          Failwith(16)
                                                      | Some s549 ->
                                                          let [x917; x918] =
                                                            Unpair(2, s529)
                                                          Pair
                                                            ( x917
                                                            , let [x929; x930
                                                                    ] =
                                                                Unpair(2, x918)
                                                              Pair
                                                                ( x929
                                                                , let 
                                                                  [x941; x942
                                                                    ] =
                                                                    Unpair(2, x930)
                                                                  Pair
                                                                    ( x941
                                                                    , 
                                                                  let _ =
                                                                    x942
                                                                  s549
                                                                    )
                                                                )
                                                            )
                                                      end
                                                  end
                                                else
                                                  s529
                                              match Get(0, Car(Car(s554))) with
                                              | None _ -> Failwith(17)
                                              | Some s564 ->
                                                  match Get(0, s564) with
                                                  | None _ -> Failwith(17)
                                                  | Some s571 ->
                                                      let [s620; s621] =
                                                        if Neq(Compare
                                                                 ( s571
                                                                 , 0
                                                                 ))
                                                        then
                                                          match Get
                                                                  ( 1
                                                                  , Car(Car(s554))
                                                                  ) with
                                                          | None _ ->
                                                              Failwith(17)
                                                          | Some s585 ->
                                                              match Get
                                                                    ( 1
                                                                    , s585
                                                                    ) with
                                                              | None _ ->
                                                                  Failwith(17)
                                                              | Some s591 ->
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , Car(Car(s554))
                                                                    ) with
                                                                  | None _ ->
                                                                    Failwith(17)
                                                                  | Some s606 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s606
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(17)
                                                                    | Some s613 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s613
                                                                    , s591
                                                                    ))
                                                                    ; s554
                                                                    ]
                                                                    end
                                                                  end
                                                              end
                                                          end
                                                        else
                                                          [ False; s554 ]
                                                      let [s664; s665] =
                                                        if s620
                                                        then
                                                          match Get
                                                                  ( 2
                                                                  , Car(Car(s621))
                                                                  ) with
                                                          | None _ ->
                                                              Failwith(17)
                                                          | Some s629 ->
                                                              match Get
                                                                    ( 2
                                                                    , s629
                                                                    ) with
                                                              | None _ ->
                                                                  Failwith(17)
                                                              | Some s635 ->
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , Car(Car(s621))
                                                                    ) with
                                                                  | None _ ->
                                                                    Failwith(17)
                                                                  | Some s650 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s650
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(17)
                                                                    | Some s657 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s657
                                                                    , s635
                                                                    ))
                                                                    ; s621
                                                                    ]
                                                                    end
                                                                  end
                                                              end
                                                          end
                                                        else
                                                          [ False; s621 ]
                                                      let s683 =
                                                        if s664
                                                        then
                                                          match Get
                                                                  ( 0
                                                                  , Car(Car(s665))
                                                                  ) with
                                                          | None _ ->
                                                              Failwith(18)
                                                          | Some s672 ->
                                                              match Get
                                                                    ( 0
                                                                    , s672
                                                                    ) with
                                                              | None _ ->
                                                                  Failwith(18)
                                                              | Some s678 ->
                                                                  let 
                                                                  [x915; x916
                                                                    ] =
                                                                    Unpair(2, s665)
                                                                  Pair
                                                                    ( x915
                                                                    , 
                                                                  let 
                                                                  [x927; x928
                                                                    ] =
                                                                    Unpair(2, x916)
                                                                  Pair
                                                                    ( x927
                                                                    , 
                                                                  let 
                                                                  [x939; x940
                                                                    ] =
                                                                    Unpair(2, x928)
                                                                  Pair
                                                                    ( x939
                                                                    , 
                                                                  let _ =
                                                                    x940
                                                                  s678
                                                                    )
                                                                    )
                                                                    )
                                                              end
                                                          end
                                                        else
                                                          s665
                                                      match Get
                                                              ( 0
                                                              , Car(Car(s683))
                                                              ) with
                                                      | None _ ->
                                                          Failwith(19)
                                                      | Some s693 ->
                                                          match Get(2, s693) with
                                                          | None _ ->
                                                              Failwith(19)
                                                          | Some s700 ->
                                                              let [s749; s750
                                                                    ] =
                                                                if Neq(
                                                                Compare
                                                                  ( s700
                                                                  , 0
                                                                  ))
                                                                then
                                                                  match 
                                                                  Get
                                                                    ( 1
                                                                    , Car(Car(s683))
                                                                    ) with
                                                                  | None _ ->
                                                                    Failwith(19)
                                                                  | Some s714 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s714
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(19)
                                                                    | Some s720 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s683))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(19)
                                                                    | Some s735 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s735
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(19)
                                                                    | Some s742 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s742
                                                                    , s720
                                                                    ))
                                                                    ; s683
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                  end
                                                                else
                                                                  [ False
                                                                  ; s683
                                                                  ]
                                                              let [s793; s794
                                                                    ] =
                                                                if s749
                                                                then
                                                                  match 
                                                                  Get
                                                                    ( 2
                                                                    , Car(Car(s750))
                                                                    ) with
                                                                  | None _ ->
                                                                    Failwith(19)
                                                                  | Some s758 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s758
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(19)
                                                                    | Some s764 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s750))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(19)
                                                                    | Some s779 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s779
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(19)
                                                                    | Some s786 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s786
                                                                    , s764
                                                                    ))
                                                                    ; s750
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                  end
                                                                else
                                                                  [ False
                                                                  ; s750
                                                                  ]
                                                              let s812 =
                                                                if s793
                                                                then
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , Car(Car(s794))
                                                                    ) with
                                                                  | None _ ->
                                                                    Failwith(20)
                                                                  | Some s801 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s801
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(20)
                                                                    | Some s807 ->
                                                                    let 
                                                                    [x913;
                                                                    x914] =
                                                                    Unpair(2, s794)
                                                                    Pair
                                                                    ( x913
                                                                    , 
                                                                    let 
                                                                    [x925;
                                                                    x926] =
                                                                    Unpair(2, x914)
                                                                    Pair
                                                                    ( x925
                                                                    , 
                                                                    let 
                                                                    [x937;
                                                                    x938] =
                                                                    Unpair(2, x926)
                                                                    Pair
                                                                    ( x937
                                                                    , 
                                                                    let _ =
                                                                    x938
                                                                    s807
                                                                    )
                                                                    )
                                                                    )
                                                                    end
                                                                  end
                                                                else
                                                                  s794
                                                              let [s826; s827
                                                                    ] =
                                                                if Eq(
                                                                Compare
                                                                  ( 9
                                                                  , Car(Cdr(s812))
                                                                  ))
                                                                then
                                                                  [ Eq(
                                                                  Compare
                                                                    ( 0
                                                                    , Cdr(Cdr(Cdr(s812)))
                                                                    ))
                                                                  ; s812
                                                                  ]
                                                                else
                                                                  [ False
                                                                  ; s812
                                                                  ]
                                                              Pair
                                                                ( Nil<operation>
                                                                , record_of_tree(..., 
                                                              if s826
                                                              then
                                                                Pair
                                                                  ( Pair
                                                                    ( Car(Car(s827))
                                                                    , True
                                                                    )
                                                                  , Cdr(s827)
                                                                  )
                                                              else
                                                                s827)
                                                                )
                                                          end
                                                      end
                                                  end
                                              end
                                          end
                                      end
                                  end
                              end
                          end
                        else
                          Failwith("WrongCondition: self.data.deck[params.i][params.j] == 0")
                    end
                end
              else
                Failwith("WrongCondition: params.move == self.data.nextPlayer")
            else
              Failwith("WrongCondition: (params.j >= 0) & (params.j < 3)")
          else
            Failwith("WrongCondition: (params.i >= 0) & (params.i < 3)")
        else
          Failwith("WrongCondition: (self.data.winner == 0) & (~ self.data.draw)")]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
