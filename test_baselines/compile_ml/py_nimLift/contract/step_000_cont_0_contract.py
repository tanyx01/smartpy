import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(games = sp.TMap(sp.TIntOrNat, sp.TRecord(bound = sp.TInt, claimed = sp.TBool, deck = sp.TMap(sp.TInt, sp.TInt), lastWins = sp.TBool, nextPlayer = sp.TInt, size = sp.TInt, winner = sp.TInt).layout((("bound", ("claimed", "deck")), (("lastWins", "nextPlayer"), ("size", "winner"))))), nbGames = sp.TIntOrNat).layout(("games", "nbGames")))
    self.init(games = {},
              nbGames = 0)

  @sp.entry_point
  def build(self, params):
    rangeMap = sp.local("rangeMap", {})
    sp.for i in sp.range(1, params.size + 1):
      rangeMap.value[i.value - 1] = i.value
    self.data.games[self.data.nbGames] = sp.record(bound = params.bound, claimed = False, deck = rangeMap.value, lastWins = False, nextPlayer = 1, size = params.size, winner = 0)
    self.data.nbGames += 1

  @sp.entry_point
  def claim(self, params):
    sp.verify(sp.sum(self.data.games[params.gameId].deck.values()) == 0)
    self.data.games[params.gameId].claimed = True
    sp.if self.data.games[params.gameId].lastWins:
      self.data.games[params.gameId].winner = 3 - self.data.games[params.gameId].nextPlayer
    sp.else:
      self.data.games[params.gameId].winner = self.data.games[params.gameId].nextPlayer

  @sp.entry_point
  def remove(self, params):
    sp.verify(params.cell >= 0)
    sp.verify(params.cell < self.data.games[params.gameId].size)
    sp.verify(params.k >= 1)
    sp.verify(params.k <= self.data.games[params.gameId].bound)
    sp.verify(params.k <= self.data.games[params.gameId].deck[params.cell])
    self.data.games[params.gameId].deck[params.cell] -= params.k
    self.data.games[params.gameId].nextPlayer = 3 - self.data.games[params.gameId].nextPlayer

sp.add_compilation_target("test", Contract())