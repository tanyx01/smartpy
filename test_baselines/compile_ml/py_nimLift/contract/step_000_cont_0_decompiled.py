import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(games = sp.TMap(sp.TInt, sp.TPair(sp.TPair(sp.TInt, sp.TPair(sp.TBool, sp.TMap(sp.TInt, sp.TInt))), sp.TPair(sp.TPair(sp.TBool, sp.TInt), sp.TPair(sp.TInt, sp.TInt)))), nbGames = sp.TInt).layout(("games", "nbGames")))

  @sp.entry_point
  def build(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TInt))
    x574 = sp.local("x574", sp.snd(params) + 1)
    c6 = sp.local("c6", x574.value > 1)
    y4 = sp.local("y4", 1)
    r690 = sp.local("r690", {})
    sp.while c6.value:
      x608 = sp.local("x608", 1 + y4.value)
      c6.value = x574.value > x608.value
      y4.value = x608.value
      r690.value[y4.value - 1] = y4.value
    self.data = sp.record(games = sp.update_map(self.data.games, self.data.nbGames, sp.some(((sp.fst(params), (False, r690.value)), ((False, 1), (sp.snd(params), 0))))), nbGames = 1 + self.data.nbGames)

  @sp.entry_point
  def claim(self, params):
    sp.set_type(params, sp.TInt)
    x61 = sp.bind_block("x61"):
    with x61:
      with sp.some(self.data.games[params]).match_cases() as arg:
        with arg.match('None') as _l54:
          sp.failwith(13)
        with arg.match('Some') as s347:
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [r678] =
          iter [ Cdr(Cdr(Car(s347))); 0 ]
          step s356; s357 ->
            [ Add(Cdr(s356), s357) ]
        if Eq(Compare(r678, 0))
        then
          let x378 = __storage.games
          match Get(l5, x378) with
          | None _ -> Failwith(14)
          | Some s390 ->
              let x416 =
                Pair
                  ( Update(l5, Some_(Pair
                                       ( Pair
                                           ( Car(Car(s390))
                                           , Pair(True, Cdr(Cdr(Car(s390))))
                                           )
                                       , Cdr(s390)
                                       )), x378)
                  , __storage.nbGames
                  )
              match Get
                      ( l5
                      , Update(l5, Some_(Pair
                                           ( Pair
                                               ( Car(Car(s390))
                                               , Pair
                                                   ( True
                                                   , Cdr(Cdr(Car(s390)))
                                                   )
                                               )
                                           , Cdr(s390)
                                           )), x378)
                      ) with
              | None _ -> Failwith(15)
              | Some s429 ->
                  if Car(Car(Cdr(s429)))
                  then
                    match Get
                            ( l5
                            , Update(l5, Some_(Pair
                                                 ( Pair
                                                     ( Car(Car(s390))
                                                     , Pair
                                                         ( True
                                                         , Cdr(Cdr(Car(s390)))
                                                         )
                                                     )
                                                 , Cdr(s390)
                                                 )), x378)
                            ) with
                    | None _ -> Failwith(16)
                    | Some s519 ->
                        match Get
                                ( l5
                                , Update(l5, Some_(Pair
                                                     ( Pair
                                                         ( Car(Car(s390))
                                                         , Pair
                                                             ( True
                                                             , Cdr(Cdr(Car(s390)))
                                                             )
                                                         )
                                                     , Cdr(s390)
                                                     )), x378)
                                ) with
                        | None _ -> Failwith(16)
                        | Some s541 ->
                            let [x801; x802] = Unpair(2, x416)
                            let _ = x801
                            Pair
                              ( Update(l5, Some_(let [x799; x800] =
                                                   Unpair(2, s519)
                                                 Pair
                                                   ( x799
                                                   , let [x805; x806] =
                                                       Unpair(2, x800)
                                                     Pair
                                                       ( x805
                                                       , let [x809; x810] =
                                                           Unpair(2, x806)
                                                         Pair
                                                           ( x809
                                                           , let _ = x810
                                                             Sub
                                                               ( 3
                                                               , Cdr(Car(Cdr(s541)))
                                                               )
                                                           )
                                                       )
                                                   )), Update(l5, Some_(
                            Pair
                              ( Pair
                                  ( Car(Car(s390))
                                  , Pair(True, Cdr(Cdr(Car(s390))))
                                  )
                              , Cdr(s390)
                              )), x378))
                              , x802
                              )
                        end
                    end
                  else
                    match Get
                            ( l5
                            , Update(l5, Some_(Pair
                                                 ( Pair
                                                     ( Car(Car(s390))
                                                     , Pair
                                                         ( True
                                                         , Cdr(Cdr(Car(s390)))
                                                         )
                                                     )
                                                 , Cdr(s390)
                                                 )), x378)
                            ) with
                    | None _ -> Failwith(18)
                    | Some s459 ->
                        match Get
                                ( l5
                                , Update(l5, Some_(Pair
                                                     ( Pair
                                                         ( Car(Car(s390))
                                                         , Pair
                                                             ( True
                                                             , Cdr(Cdr(Car(s390)))
                                                             )
                                                         )
                                                     , Cdr(s390)
                                                     )), x378)
                                ) with
                        | None _ -> Failwith(18)
                        | Some s481 ->
                            let [x797; x798] = Unpair(2, x416)
                            let _ = x797
                            Pair
                              ( Update(l5, Some_(let [x795; x796] =
                                                   Unpair(2, s459)
                                                 Pair
                                                   ( x795
                                                   , let [x803; x804] =
                                                       Unpair(2, x796)
                                                     Pair
                                                       ( x803
                                                       , let [x807; x808] =
                                                           Unpair(2, x804)
                                                         Pair
                                                           ( x807
                                                           , let _ = x808
                                                             Cdr(Car(Cdr(s481)))
                                                           )
                                                       )
                                                   )), Update(l5, Some_(
                            Pair
                              ( Pair
                                  ( Car(Car(s390))
                                  , Pair(True, Cdr(Cdr(Car(s390))))
                                  )
                              , Cdr(s390)
                              )), x378))
                              , x798
                              )
                        end
                    end
              end
          end
        else
          Failwith("WrongCondition: sp.sum(self.data.games[params.gameId].deck.values()) == 0")]')
          sp.failwith(sp.build_lambda(ferror)(sp.unit))

    x741 = sp.local("x741", x61.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x741)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def remove(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)))
    sp.verify(sp.fst(params) >= 0, 'WrongCondition: params.cell >= 0')
    x232 = sp.bind_block("x232"):
    with x232:
      with sp.some(self.data.games[sp.fst(sp.snd(params))]).match_cases() as arg:
        with arg.match('None') as _l64:
          sp.failwith(22)
        with arg.match('Some') as s35:
          sp.verify(sp.fst(params) < sp.fst(sp.snd(sp.snd(s35))), 'WrongCondition: params.cell < self.data.games[params.gameId].size')
          sp.verify(sp.snd(sp.snd(params)) >= 1, 'WrongCondition: params.k >= 1')
          with sp.some(self.data.games[sp.fst(sp.snd(params))]).match_cases() as arg:
            with arg.match('None') as _l72:
              sp.failwith(24)
            with arg.match('Some') as s82:
              sp.verify(sp.snd(sp.snd(params)) <= sp.fst(sp.fst(s82)), 'WrongCondition: params.k <= self.data.games[params.gameId].bound')
              with sp.some(self.data.games[sp.fst(sp.snd(params))]).match_cases() as arg:
                with arg.match('None') as _l80:
                  sp.failwith(25)
                with arg.match('Some') as s119:
                  with sp.some(sp.snd(sp.snd(sp.fst(s119)))[sp.fst(params)]).match_cases() as arg:
                    with arg.match('None') as _l88:
                      sp.failwith(25)
                    with arg.match('Some') as s135:
                      sp.verify(sp.snd(sp.snd(params)) <= s135, 'WrongCondition: params.k <= self.data.games[params.gameId].deck[params.cell]')
                      x159 = sp.local("x159", self.data.games)
                      x167 = sp.local("x167", sp.fst(sp.snd(params)))
                      with sp.some(x159.value[x167.value]).match_cases() as arg:
                        with arg.match('None') as _l102:
                          sp.failwith(26)
                        with arg.match('Some') as s172:
                          with sp.some(sp.snd(sp.snd(sp.fst(s172)))[sp.fst(params)]).match_cases() as arg:
                            with arg.match('None') as _l107:
                              sp.failwith(26)
                            with arg.match('Some') as s208:
                              x252 = sp.local("x252", (sp.update_map(x159.value, x167.value, sp.some(((sp.fst(sp.fst(s172)), (sp.fst(sp.snd(sp.fst(s172))), sp.update_map(sp.snd(sp.snd(sp.fst(s172))), sp.fst(params), sp.some(s208 - sp.snd(sp.snd(params)))))), sp.snd(s172)))), self.data.nbGames))
                              x268 = sp.local("x268", sp.fst(sp.snd(params)))
                              with sp.some(sp.fst(x252.value)[x268.value]).match_cases() as arg:
                                with arg.match('None') as _l151:
                                  sp.failwith(27)
                                with arg.match('Some') as s273:
                                  with sp.some(sp.fst(x252.value)[sp.fst(sp.snd(params))]).match_cases() as arg:
                                    with arg.match('None') as _l157:
                                      sp.failwith(27)
                                    with arg.match('Some') as s307:
    def ferror(error):
      sp.failwith('[Error: to_cmd: Unpair(2, x252)]')
                                      sp.failwith(sp.build_lambda(ferror)(sp.unit))








    x739 = sp.local("x739", x232.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x739)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
