#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/py_test_local_record.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/compile_ml/py_test_local_record/scenario.json"
else
  exit 1;;