open SmartML

module Contract = struct
  let%entry_point pop () =
    verify (data.fif.first < data.fif.last);
    Map.delete data.fif.saved data.fif.first;
    data.fif.first <- data.fif.first + (int 1)

  let%entry_point push params =
    set_type params int;
    data.fif.last <- data.fif.last + (int 1);
    Map.set data.fif.saved data.fif.last params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {fif = {first = int; last = int; saved = map int int}}]
      ~storage:[%expr
                 {fif = {first = int 0; last = int ((-1)); saved = Map.make []}}]
      [pop; push]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())