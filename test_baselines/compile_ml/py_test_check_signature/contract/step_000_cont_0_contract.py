import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(bossPublicKey = sp.TKey, counter = sp.TIntOrNat, currentValue = sp.TString).layout(("bossPublicKey", ("counter", "currentValue"))))
    self.init(bossPublicKey = sp.key('edpku5ZuqYibUQrAhove2B1bZDYCQyDRTGGa1ZwtZYiJ6nvJh4GXcE'),
              counter = 0,
              currentValue = 'Hello World')

  @sp.entry_point
  def setCurrentValue(self, params):
    sp.verify(sp.check_signature(self.data.bossPublicKey, params.userSignature, sp.pack(sp.record(c = self.data.counter, n = params.newValue, o = self.data.currentValue))))
    self.data.currentValue = params.newValue
    self.data.counter += 1

sp.add_compilation_target("test", Contract())