import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(creator = sp.TAddress, kitties = sp.TMap(sp.TInt, sp.TPair(sp.TPair(sp.TPair(sp.TTimestamp, sp.TMutez), sp.TPair(sp.TInt, sp.TTimestamp)), sp.TPair(sp.TPair(sp.TBool, sp.TInt), sp.TPair(sp.TAddress, sp.TMutez))))).layout(("creator", "kitties")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TPair(sp.TPair(sp.TMutez, sp.TInt), sp.TPair(sp.TInt, sp.TInt)), Right = sp.TPair(sp.TPair(sp.TPair(sp.TTimestamp, sp.TMutez), sp.TPair(sp.TInt, sp.TTimestamp)), sp.TPair(sp.TPair(sp.TBool, sp.TInt), sp.TPair(sp.TAddress, sp.TMutez)))).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TPair(sp.TInt, sp.TMutez), Right = sp.TVariant(Left = sp.TPair(sp.TMutez, sp.TPair(sp.TInt, sp.TMutez)), Right = sp.TPair(sp.TMutez, sp.TPair(sp.TInt, sp.TMutez))).layout(("Left", "Right"))).layout(("Left", "Right"))).layout(("Left", "Right")))
    x351 = sp.bind_block("x351"):
    with x351:
      with params.match_cases() as arg:
        with arg.match('Left') as l3:
          with l3.match_cases() as arg:
            with arg.match('Left') as l603:
              sp.verify(sp.fst(sp.snd(l603)) != sp.snd(sp.snd(l603)), 'WrongCondition: params.parent1 != params.parent2')
              x348 = sp.bind_block("x348"):
              with x348:
                with sp.some(self.data.kitties[sp.fst(sp.snd(l603))]).match_cases() as arg:
                  with arg.match('None') as _l298:
                    sp.failwith(13)
                  with arg.match('Some') as s672:
                    sp.verify(sp.fst(sp.fst(sp.fst(s672))) < sp.now, 'WrongCondition: self.data.kitties[params.parent1].auction < sp.now')
                    with sp.some(self.data.kitties[sp.fst(sp.snd(l603))]).match_cases() as arg:
                      with arg.match('None') as _l306:
                        sp.failwith(14)
                      with arg.match('Some') as s702:
                        sp.verify(sp.snd(sp.snd(sp.fst(s702))) < sp.now, 'WrongCondition: self.data.kitties[params.parent1].hatching < sp.now')
                        with sp.some(self.data.kitties[sp.snd(sp.snd(l603))]).match_cases() as arg:
                          with arg.match('None') as _l314:
                            sp.failwith(15)
                          with arg.match('Some') as s731:
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [s856; s857; s858] =
          if Neq(Compare(Car(Cdr(Cdr(s731))), Sender))
          then
            match Get(Cdr(Cdr(l603)), __storage.kitties) with
            | None _ -> Failwith(17)
            | Some s752 ->
                if Lt(Compare(0#mutez, Cdr(Car(Car(s752)))))
                then
                  match Get(Cdr(Cdr(l603)), __storage.kitties) with
                  | None _ -> Failwith(18)
                  | Some s790 ->
                      if Lt(Compare(Cdr(Car(Car(s790))), Car(Car(l603))))
                      then
                        if Eq(Compare(Amount, Car(Car(l603))))
                        then
                          match Get(Cdr(Cdr(l603)), __storage.kitties) with
                          | None _ -> Failwith(20)
                          | Some s833 ->
                              match Contract(Car(Cdr(Cdr(s833))), unit) with
                              | None _ -> Failwith(20)
                              | Some s841 ->
                                  [ Cons
                                      ( Transfer_tokens(Unit, Car(Car(l603)), s841)
                                      , Nil<operation>
                                      )
                                  ; l603
                                  ; Pair(__storage.creator, __storage.kitties)
                                  ]
                              end
                          end
                        else
                          Failwith("WrongCondition: sp.amount == params.borrowPrice")
                      else
                        Failwith("WrongCondition: self.data.kitties[params.parent2].borrowPrice < params.borrowPrice")
                  end
                else
                  Failwith("WrongCondition: sp.tez(0) < self.data.kitties[params.parent2].borrowPrice")
            end
          else
            [ Nil<operation>
            ; l603
            ; Pair(__storage.creator, __storage.kitties)
            ]
        match Get(Cdr(Cdr(s857)), Cdr(s858)) with
        | None _ -> Failwith(22)
        | Some s873 ->
            if Lt(Compare(Car(Car(Car(s873))), Now))
            then
              match Get(Cdr(Cdr(s857)), Cdr(s858)) with
              | None _ -> Failwith(23)
              | Some s903 ->
                  if Lt(Compare(Cdr(Cdr(Car(s903))), Now))
                  then
                    let x934 = Car(Cdr(s857))
                    match Get(x934, Cdr(s858)) with
                    | None _ -> Failwith(24)
                    | Some s939 ->
                        let x970 =
                          let [x1208; x1209] = Unpair(2, s858)
                          Pair
                            ( x1208
                            , let _ = x1209
                              Update(x934, Some_(Pair
                                                   ( Pair
                                                       ( Car(Car(s939))
                                                       , Pair
                                                           ( Car(Cdr(Car(s939)))
                                                           , Add(Now, 100)
                                                           )
                                                       )
                                                   , Cdr(s939)
                                                   )), Cdr(s858))
                            )
                        let x982 = Cdr(Cdr(s857))
                        match Get(x982, Cdr(x970)) with
                        | None _ -> Failwith(25)
                        | Some s987 ->
                            let x1018 =
                              let [x1206; x1207] = Unpair(2, x970)
                              Pair
                                ( x1206
                                , let _ = x1207
                                  Update(x982, Some_(Pair
                                                       ( Pair
                                                           ( Car(Car(s987))
                                                           , Pair
                                                               ( Car(Cdr(Car(s987)))
                                                               , Add(Now, 100)
                                                               )
                                                           )
                                                       , Cdr(s987)
                                                       )), Cdr(x970))
                                )
                            match Get(Cdr(Cdr(s857)), Cdr(x1018)) with
                            | None _ -> Failwith(26)
                            | Some s1062 ->
                                match Get(Car(Cdr(s857)), Cdr(x1018)) with
                                | None _ -> Failwith(26)
                                | Some s1094 ->
                                    let s1097 = Car(Cdr(Car(s1062)))
                                    let s1098 = Add(Now, 100)
                                    let x1107 = Car(Cdr(Car(s1094)))
                                    let [s1130; s1131; s1132; s1133; s1134;
                                          s1135; s1136] =
                                      if Le(Compare(s1097, x1107))
                                      then
                                        [ x1107
                                        ; s1098
                                        ; Pair
                                            ( Pair(False, Cdr(Car(s857)))
                                            , Pair(Sender, 0#mutez)
                                            )
                                        ; Cdr(x1018)
                                        ; x1018
                                        ; s856
                                        ; s857
                                        ]
                                      else
                                        [ s1097
                                        ; s1098
                                        ; Pair
                                            ( Pair(False, Cdr(Car(s857)))
                                            , Pair(Sender, 0#mutez)
                                            )
                                        ; Cdr(x1018)
                                        ; x1018
                                        ; s856
                                        ; s857
                                        ]
                                    [ s1135
                                    ; let [x1204; x1205] = Unpair(2, s1134)
                                      Pair
                                        ( x1204
                                        , let _ = x1205
                                          Update(Cdr(Car(s1136)), Some_(
                                          Pair
                                            ( Pair
                                                ( Pair
                                                    ( "1970-01-01T00:00:00Z"#timestamp
                                                    , 0#mutez
                                                    )
                                                , Pair(Add(1, s1130), s1131)
                                                )
                                            , s1132
                                            )), s1133)
                                        )
                                    ]
                                end
                            end
                        end
                    end
                  else
                    Failwith("WrongCondition: self.data.kitties[params.parent2].hatching < sp.now")
              end
            else
              Failwith("WrongCondition: self.data.kitties[params.parent2].auction < sp.now")
        end]')
                            sp.failwith(sp.build_lambda(ferror)(sp.unit))



              s1155 = sp.local("s1155", sp.fst(x348.value))
              s1156 = sp.local("s1156", sp.snd(x348.value))
              sp.result((s1155.value, s1156.value))
            with arg.match('Right') as r604:
              sp.verify(self.data.creator == sp.sender, 'WrongCondition: self.data.creator == sp.sender')
              sp.verify(sp.fst(sp.fst(sp.snd(r604))), 'WrongCondition: params.kitty.isNew')
              x295 = sp.local("x295", (sp.list([]), (self.data.creator, sp.update_map(self.data.kitties, sp.snd(sp.fst(sp.snd(r604))), sp.some(r604)))))
              s1155 = sp.local("s1155", sp.fst(x295.value))
              s1156 = sp.local("s1156", sp.snd(x295.value))
              sp.result((s1155.value, s1156.value))

        with arg.match('Right') as r4:
          with r4.match_cases() as arg:
            with arg.match('Left') as l5:
              x266 = sp.bind_block("x266"):
              with x266:
                with sp.some(self.data.kitties[sp.fst(l5)]).match_cases() as arg:
                  with arg.match('None') as _l157:
                    sp.failwith(35)
                  with arg.match('Some') as s265:
                    sp.verify(sp.tez(0) < sp.snd(sp.snd(sp.snd(s265))), 'WrongCondition: sp.tez(0) < self.data.kitties[params.kittyId].price')
                    with sp.some(self.data.kitties[sp.fst(l5)]).match_cases() as arg:
                      with arg.match('None') as _l164:
                        sp.failwith(36)
                      with arg.match('Some') as s291:
                        sp.verify(sp.snd(sp.snd(sp.snd(s291))) <= sp.snd(l5), 'WrongCondition: self.data.kitties[params.kittyId].price <= params.price')
                        sp.verify(sp.amount == sp.snd(l5), 'WrongCondition: sp.amount == params.price')
                        with sp.some(self.data.kitties[sp.fst(l5)]).match_cases() as arg:
                          with arg.match('None') as _l171:
                            sp.failwith(38)
                          with arg.match('Some') as s324:
                            with sp.contract(sp.TUnit, sp.fst(sp.snd(sp.snd(s324)))).match_cases() as arg:
                              with arg.match('None') as _l178:
                                sp.failwith(38)
                              with arg.match('Some') as s332:
                                x353 = sp.local("x353", self.data.kitties)
                                with sp.some(x353.value[sp.fst(l5)]).match_cases() as arg:
                                  with arg.match('None') as _l187:
                                    sp.failwith(39)
                                  with arg.match('Some') as s367:
    def ferror(error):
      sp.failwith('[Error: to_cmd: Unpair(2, s367)]')
                                    sp.failwith(sp.build_lambda(ferror)(sp.unit))





              s601 = sp.local("s601", sp.fst(x266.value))
              s602 = sp.local("s602", sp.snd(x266.value))
              sp.result((s601.value, s602.value))
            with arg.match('Right') as r6:
              with r6.match_cases() as arg:
                with arg.match('Left') as l7:
                  sp.verify(sp.tez(0) <= sp.snd(sp.snd(l7)), 'WrongCondition: sp.tez(0) <= params.price')
                  x154 = sp.bind_block("x154"):
                  with x154:
                    with sp.some(self.data.kitties[sp.fst(sp.snd(l7))]).match_cases() as arg:
                      with arg.match('None') as _l82:
                        sp.failwith(58)
                      with arg.match('Some') as s146:
                        sp.verify(sp.fst(sp.fst(sp.fst(s146))) < sp.now, 'WrongCondition: self.data.kitties[params.kittyId].auction < sp.now')
                        with sp.some(self.data.kitties[sp.fst(sp.snd(l7))]).match_cases() as arg:
                          with arg.match('None') as _l90:
                            sp.failwith(59)
                          with arg.match('Some') as s176:
                            sp.verify(sp.snd(sp.snd(sp.fst(s176))) < sp.now, 'WrongCondition: self.data.kitties[params.kittyId].hatching < sp.now')
                            x198 = sp.local("x198", self.data.kitties)
                            x207 = sp.local("x207", sp.fst(sp.snd(l7)))
                            with sp.some(x198.value[x207.value]).match_cases() as arg:
                              with arg.match('None') as _l103:
                                sp.failwith(60)
                              with arg.match('Some') as s212:
                                sp.result((sp.list([]), (self.data.creator, sp.update_map(x198.value, x207.value, sp.some((((sp.fst(sp.fst(sp.fst(s212))), sp.snd(sp.snd(l7))), sp.snd(sp.fst(s212))), sp.snd(s212)))))))



                  s246 = sp.local("s246", sp.fst(x154.value))
                  s247 = sp.local("s247", sp.snd(x154.value))
                  sp.result((s246.value, s247.value))
                with arg.match('Right') as r8:
                  sp.verify(sp.tez(0) <= sp.snd(sp.snd(r8)), 'WrongCondition: sp.tez(0) <= params.price')
                  x79 = sp.bind_block("x79"):
                  with x79:
                    with sp.some(self.data.kitties[sp.fst(sp.snd(r8))]).match_cases() as arg:
                      with arg.match('None') as _l9:
                        sp.failwith(71)
                      with arg.match('Some') as s33:
                        sp.verify(sp.fst(sp.fst(sp.fst(s33))) < sp.now, 'WrongCondition: self.data.kitties[params.kittyId].auction < sp.now')
                        with sp.some(self.data.kitties[sp.fst(sp.snd(r8))]).match_cases() as arg:
                          with arg.match('None') as _l17:
                            sp.failwith(72)
                          with arg.match('Some') as s63:
                            sp.verify(sp.snd(sp.snd(sp.fst(s63))) < sp.now, 'WrongCondition: self.data.kitties[params.kittyId].hatching < sp.now')
                            x85 = sp.local("x85", self.data.kitties)
                            x94 = sp.local("x94", sp.fst(sp.snd(r8)))
                            with sp.some(x85.value[x94.value]).match_cases() as arg:
                              with arg.match('None') as _l30:
                                sp.failwith(73)
                              with arg.match('Some') as s99:
    def ferror(error):
      sp.failwith('[Error: to_cmd: Unpair(2, s99)]')
                                sp.failwith(sp.build_lambda(ferror)(sp.unit))



                  s246 = sp.local("s246", sp.fst(x79.value))
                  s247 = sp.local("s247", sp.snd(x79.value))
                  sp.result((s246.value, s247.value))



    s1157 = sp.local("s1157", sp.fst(x351.value))
    s1158 = sp.local("s1158", sp.snd(x351.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s1158)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
