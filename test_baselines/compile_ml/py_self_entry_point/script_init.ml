open SmartML

module Contract = struct
  let%entry_point ep1 params =
    set_type params unit

  let%entry_point ep2 params =
    set_type params (contract timestamp)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep1; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())