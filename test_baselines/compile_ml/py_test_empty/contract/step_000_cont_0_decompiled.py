import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(l1 = sp.TList(sp.TString), l2 = sp.TList(sp.TString), m1 = sp.TMap(sp.TString, sp.TString), m2 = sp.TMap(sp.TString, sp.TString), o1 = sp.TOption(sp.TString)).layout((("l1", "l2"), ("m1", ("m2", "o1")))))

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TUnit)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let x18 =
                              Pair
                                ( Pair(__storage.l1, __storage.l2)
                                , Pair
                                    ( ["e": "f"]#map(string,string)
                                    , Pair(__storage.m2, __storage.o1)
                                    )
                                )
                            Pair
                              ( Pair(["g"]#list(string), __storage.l2)
                              , Pair
                                  ( ["e": "f"]#map(string,string)
                                  , Pair(__storage.m2, Some_("h"))
                                  )
                              ))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TUnit)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let x7 =
                              Pair
                                ( Pair(__storage.l1, __storage.l2)
                                , Pair
                                    ( __storage.m1
                                    , Pair
                                        ( Empty_set<string,string>
                                        , __storage.o1
                                        )
                                    )
                                )
                            Pair
                              ( Pair(__storage.l1, Nil<string>)
                              , Pair
                                  ( __storage.m1
                                  , Pair
                                      ( Empty_set<string,string>
                                      , __storage.o1
                                      )
                                  )
                              ))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
