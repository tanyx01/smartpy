import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TInt, b = sp.TNat).layout(("a", "b")))

  @sp.entry_point
  def run_record(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TPair(sp.TString, sp.TBool)))

  @sp.entry_point
  def run_record_2(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TInt, sp.TString), sp.TPair(sp.TBool, sp.TNat)))

  @sp.entry_point
  def run_type_record(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TString))

  @sp.entry_point
  def run_type_variant(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TString, Right = sp.TInt).layout(("Left", "Right")))

  @sp.entry_point
  def run_variant(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TInt, Right = sp.TVariant(Left = sp.TString, Right = sp.TBool).layout(("Left", "Right"))).layout(("Left", "Right")))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
