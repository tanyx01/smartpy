open SmartML

module Contract = struct
  let%entry_point add params =
    data.value <- params.x + params.y

  let%entry_point factorial params =
    data.value <- nat 1;
    List.iter (fun y ->
      data.value <- data.value * y
    ) (range (nat 1) (params + (nat 1)) (nat 1))

  let%entry_point log2 params =
    data.value <- nat 0;
    let%mutable y = params in ();
    while y > (nat 1) do
      data.value <- data.value + (nat 1);
      y <- y / (nat 2)
    done

  let%entry_point multiply params =
    data.value <- params.x * params.y

  let%entry_point square params =
    data.value <- params * params

  let%entry_point squareRoot params =
    verify (params >= (nat 0));
    let%mutable y = params in ();
    while (y * y) > params do
      y <- ((params / y) + y) / (nat 2)
    done;
    verify (((y * y) <= params) && (params < ((y + (nat 1)) * (y + (nat 1)))));
    data.value <- y

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = nat}]
      ~storage:[%expr
                 {value = nat 0}]
      [add; factorial; log2; multiply; square; squareRoot]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())