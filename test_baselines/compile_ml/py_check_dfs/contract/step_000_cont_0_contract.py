import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(conclusion = sp.TString, steps = sp.TString).layout(("conclusion", "steps")))
    self.init(conclusion = '',
              steps = '')

  @sp.entry_point
  def a(self):
    self.data.steps += '.a'
    sp.transfer(sp.unit, sp.tez(0), sp.self_entry_point('aa'))

  @sp.entry_point
  def aa(self):
    self.data.steps += '.aa'

  @sp.entry_point
  def b(self):
    self.data.steps += '.b'
    sp.if self.data.steps == 'check.a.b':
      self.data.conclusion = 'BFS'
    sp.else:
      self.data.conclusion = 'DFS'

  @sp.entry_point
  def check(self):
    self.data.steps = 'check'
    self.data.conclusion = ''
    sp.transfer(sp.unit, sp.tez(0), sp.self_entry_point('a'))
    sp.transfer(sp.unit, sp.tez(0), sp.self_entry_point('b'))

sp.add_compilation_target("test", Contract())