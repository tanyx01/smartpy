import smartpy as sp

tstorage = sp.TRecord(counter = sp.TIntOrNat, steps = sp.TList(sp.TInt)).layout(("counter", "steps"))
tparameter = sp.TVariant(compute = sp.TInt, run = sp.TInt).layout(("compute", "run"))
tprivates = { }
tviews = { }
