#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/store_value.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/compile_ml/store_value/scenario.json"
else
  exit 1;;