import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TPair(sp.TUnit, sp.TBigMap(sp.TNat, sp.TLambda(sp.TPair(sp.TUnit, sp.TUnit), sp.TPair(sp.TList(sp.TOperation), sp.TUnit)))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TUnit, Right = sp.TVariant(Left = sp.TUnit, Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right"))).layout(("Left", "Right")))
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [x1; x2; x3] = Unpair(3, Pair(__parameter, __storage))
        match x1 with
        | any_lazy_ep l4 ->
            let [s50; s51; s52] =
              match Get(0#nat, x3) with
              | None _ -> Failwith(-1)
              | Some s37 ->
                  let x47 = Exec(Pair(l4, x2), s37)
                  [ Car(x47); Cdr(x47); x3 ]
              end
            Pair(s50, Pair(s51, s52))
        | . r5 ->
            match r5 with
            | any_ep _ -> Pair(Nil<operation>, Pair(x2, x3))
            | . r7 ->
                match r7 with
                | transfer_KO _ ->
                    let [s21; s22] =
                      if Eq(Compare(0#mutez, Amount))
                      then
                        [ x2; x3 ]
                      else
                        Failwith(Amount)
                    Pair(Nil<operation>, Pair(s21, s22))
                | transfer_OK _ -> Pair(Nil<operation>, Pair(x2, x3))
                end
            end
        end]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
