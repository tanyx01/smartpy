open SmartML

module Contract = struct
  let%entry_point swap () =
    let%mutable x = data.a.x1 in ();
    data.a.x1 <- data.a.x2;
    data.a.x2 <- x;
    data.b.x1 <- data.b.x2;
    data.b.x2 <- data.b.x2 * (int 2)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = {x1 = int; x2 = int}; b = {x1 = int; x2 = int}}]
      ~storage:[%expr
                 {a = {x1 = int ((-1)); x2 = int ((-2))};
                  b = {x1 = int ((-3)); x2 = int ((-4))}}]
      [swap]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())