import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TOption(sp.TNat))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TNat)
    self.data = sp.some(params)

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
