import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)

  @sp.entry_point
  def bounce(self, params):
    sp.set_type(params, sp.TUnit)
    x12 = sp.bind_block("x12"):
    with x12:
      with sp.contract(sp.TUnit, sp.source).match_cases() as arg:
        with arg.match('None') as _l0:
          sp.failwith(11)
        with arg.match('Some') as s41:
          sp.result((sp.cons(sp.transfer_operation(sp.unit, sp.amount, s41), sp.list([])), self.data))

    s50 = sp.local("s50", sp.fst(x12.value))
    s51 = sp.local("s51", sp.snd(x12.value))
    sp.operations() = s50.value
    self.data = s51.value

  @sp.entry_point
  def bounce2(self, params):
    sp.set_type(params, sp.TUnit)
    x44 = sp.bind_block("x44"):
    with x44:
      with sp.contract(sp.TUnit, sp.source).match_cases() as arg:
        with arg.match('None') as _l16:
          sp.failwith(15)
        with arg.match('Some') as s9:
          with sp.contract(sp.TUnit, sp.source).match_cases() as arg:
            with arg.match('None') as _l20:
              sp.failwith(16)
            with arg.match('Some') as s20:
              with sp.sub_mutez(sp.amount, sp.tez(1)).match_cases() as arg:
                with arg.match('None') as _l24:
                  sp.failwith(16)
                with arg.match('Some') as s28:
                  sp.result((sp.cons(sp.transfer_operation(sp.unit, s28, s20), sp.cons(sp.transfer_operation(sp.unit, sp.tez(1), s9), sp.list([]))), self.data))



    s50 = sp.local("s50", sp.fst(x44.value))
    s51 = sp.local("s51", sp.snd(x44.value))
    sp.operations() = s50.value
    self.data = s51.value

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
