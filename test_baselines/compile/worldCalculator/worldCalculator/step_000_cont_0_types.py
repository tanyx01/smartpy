import smartpy as sp

tstorage = sp.TRecord(Help = sp.TList(sp.TString), formula = sp.TString, operations = sp.TList(sp.TString), result = sp.TInt, summary = sp.TString).layout((("Help", "formula"), ("operations", ("result", "summary"))))
tparameter = sp.TVariant(compute = sp.TString).layout("compute")
tprivates = { "nat_of_string": sp.TLambda(sp.TString, sp.TInt), "string_of_nat": sp.TLambda(sp.TNat, sp.TString), "string_split": sp.TLambda(sp.TRecord(s = sp.TString, sep = sp.TString).layout(("s", "sep")), sp.TList(sp.TString)) }
tviews = { }
