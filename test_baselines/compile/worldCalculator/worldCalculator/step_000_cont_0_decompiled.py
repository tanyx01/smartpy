import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(Help = sp.TList(sp.TString), formula = sp.TString, operations = sp.TList(sp.TString), result = sp.TInt, summary = sp.TString).layout((("Help", "formula"), ("operations", ("result", "summary")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TString)
    def ferror(error):
      sp.failwith('[Error: to_cmd: (fun (x1 : string) : int ->
let x17 = Size(x1)
let [_; r1217] =
  loop [ Gt(Compare(x17, 0#nat)); 0#nat; 0 ]
  step s21; s23 ->
    match Slice(s21, 1#nat, x1) with
    | None _ -> Failwith(18)
    | Some s39 ->
        match Get
                ( s39
                , ["0": 0; "1": 1; "2": 2; "3": 3; "4": 4; "5": 5; "6": 6; "7": 7; "8": 8; "9": 9]#map(string,int)
                ) with
        | None _ -> Failwith(18)
        | Some s48 ->
            let x68 = Add(1#nat, s21)
            [ Gt(Compare(x17, x68)); x68; Add(Mul(10, s23), s48) ]
        end
    end
r1217)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
