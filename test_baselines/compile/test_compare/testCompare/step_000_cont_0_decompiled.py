import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)

  @sp.entry_point
  def compare_eq(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify(sp.unit == sp.unit, 'WrongCondition: sp.compare(sp.unit, sp.unit) == 0')
    sp.verify(sp.none == sp.none, 'WrongCondition: sp.compare(sp.set_type_expr(sp.none, sp.TOption(sp.TNat)), sp.set_type_expr(sp.none, sp.TOption(sp.TNat))) == 0')

  @sp.entry_point
  def compare_inferior(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify(sp.compare(sp.timestamp(1), sp.timestamp(5)) == (-1), 'WrongCondition: sp.compare(sp.timestamp(1), sp.timestamp(5)) == (-1)')
    sp.verify(sp.compare(sp.address('KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT'), sp.address('KT1XvNYseNDJJ6Kw27qhSEDF8ys8JhDopzfG')) == (-1), 'WrongCondition: sp.compare(sp.address('KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT'), sp.address('KT1XvNYseNDJJ6Kw27qhSEDF8ys8JhDopzfG')) == (-1)')
    sp.verify(sp.compare(False, True) == (-1), 'WrongCondition: sp.compare(False, True) == (-1)')
    sp.verify(sp.compare(sp.bytes('0x00'), sp.bytes('0x01')) == (-1), 'WrongCondition: sp.compare(sp.bytes('0x00'), sp.bytes('0x01')) == (-1)')
    sp.verify(sp.compare(sp.chain_id('0x00'), sp.chain_id('0x01')) == (-1), 'WrongCondition: sp.compare(sp.chain_id('0x00'), sp.chain_id('0x01')) == (-1)')
    sp.verify(sp.compare(sp.key('edpkuBknW28nW72KG6RoH'), sp.key('edpkuJqtDcA2m2muMxViS')) == (-1), 'WrongCondition: sp.compare(sp.key('edpkuBknW28nW72KG6RoH'), sp.key('edpkuJqtDcA2m2muMxViS')) == (-1)')
    sp.verify(sp.compare(sp.key_hash('tz1KqTpEZ7Yob7QbPE4Hy'), sp.key_hash('tz1XPTDmvT3vVE5Uunngm')) == (-1), 'WrongCondition: sp.compare(sp.key_hash('tz1KqTpEZ7Yob7QbPE4Hy'), sp.key_hash('tz1XPTDmvT3vVE5Uunngm')) == (-1)')
    sp.verify(sp.compare('a', 'e') == (-1), 'WrongCondition: sp.compare('a', 'e') == (-1)')
    sp.verify(sp.compare((1, 5), (5, 1)) == (-1), 'WrongCondition: sp.compare((1, 5), (5, 1)) == (-1)')
    sp.verify(sp.compare(sp.none, sp.some(sp.unit)) == (-1), 'WrongCondition: sp.compare(sp.none, sp.some(sp.unit)) == (-1)')
    sp.verify(sp.compare(sp.some(1), sp.some(5)) == (-1), 'WrongCondition: sp.compare(sp.some(1), sp.some(5)) == (-1)')
    def ferror(error):
      sp.failwith('[Error: prim1: (Left (None, None, (T0 T_nat)))]')
    def ferror(error):
      sp.failwith('[Error: prim1: (Right (None, None, (T0 T_nat)))]')
    sp.verify(sp.compare(sp.build_lambda(ferror)(sp.unit), sp.build_lambda(ferror)(sp.unit)) == (-1), 'WrongCondition: sp.compare(sp.set_type_expr(variant('Left', 5), sp.TVariant(Left = sp.TNat, Right = sp.TNat).layout(("Left", "Right"))), variant('Right', 1)) == (-1)')
    def ferror(error):
      sp.failwith('[Error: prim1: (Left (None, None, (T0 T_nat)))]')
    def ferror(error):
      sp.failwith('[Error: prim1: (Left (None, None, (T0 T_nat)))]')
    sp.verify(sp.compare(sp.build_lambda(ferror)(sp.unit), sp.build_lambda(ferror)(sp.unit)) == (-1), 'WrongCondition: sp.compare(sp.set_type_expr(variant('Left', 1), sp.TVariant(Left = sp.TNat, Right = sp.TNat).layout(("Left", "Right"))), variant('Left', 5)) == (-1)')
    def ferror(error):
      sp.failwith('[Error: prim1: (Left (None, None, (T0 T_nat)))]')
    def ferror(error):
      sp.failwith('[Error: prim1: (Left (None, None, (T0 T_nat)))]')
    sp.verify(sp.compare(sp.build_lambda(ferror)(sp.unit), sp.build_lambda(ferror)(sp.unit)) == (-1), 'WrongCondition: sp.compare(sp.set_type_expr(variant('Left', 1), sp.TVariant(Left = sp.TNat, Right = sp.TNat).layout(("Left", "Right"))), variant('Left', 5)) == (-1)')
    def ferror(error):
      sp.failwith('[Error: prim1: (Right (None, None, (T0 T_nat)))]')
    def ferror(error):
      sp.failwith('[Error: prim1: (Right (None, None, (T0 T_nat)))]')
    sp.verify(sp.compare(sp.build_lambda(ferror)(sp.unit), sp.build_lambda(ferror)(sp.unit)) == (-1), 'WrongCondition: sp.compare(sp.set_type_expr(variant('Right', 1), sp.TVariant(Left = sp.TNat, Right = sp.TNat).layout(("Left", "Right"))), variant('Right', 5)) == (-1)')

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
