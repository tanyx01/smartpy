import smartpy as sp

tstorage = sp.TRecord(l1 = sp.TList(sp.TString), l2 = sp.TList(sp.TString), m1 = sp.TMap(sp.TString, sp.TString), m2 = sp.TMap(sp.TString, sp.TString), o1 = sp.TOption(sp.TString)).layout((("l1", "l2"), ("m1", ("m2", "o1"))))
tparameter = sp.TVariant(ep1 = sp.TUnit, ep2 = sp.TUnit).layout(("ep1", "ep2"))
tprivates = { }
tviews = { }
