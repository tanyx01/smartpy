import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, collateral = sp.TMutez, duration = sp.TInt, ledger = sp.TMap(sp.TAddress, sp.TPair(sp.TMutez, sp.TTimestamp)), rate = sp.TNat).layout((("admin", "collateral"), ("duration", ("ledger", "rate")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TUnit, Right = sp.TVariant(Left = sp.TKeyHash, Right = sp.TPair(sp.TInt, sp.TNat)).layout(("Left", "Right"))).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as _l0:
        sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
        x357 = sp.local("x357", sp.amount + self.data.collateral)
        x135 = sp.local("x135", (sp.list([]), ((self.data.admin, x357.value), (self.data.duration, (self.data.ledger, self.data.rate)))))
        s363 = sp.local("s363", sp.fst(x135.value))
        s364 = sp.local("s364", sp.snd(x135.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s364)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))
      with arg.match('Right') as r194:
        with r194.match_cases() as arg:
          with arg.match('Left') as l195:
            sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
            sp.verify(sp.amount == sp.tez(0), 'WrongCondition: sp.amount == sp.tez(0)')
    def ferror(error):
      sp.failwith('[Error: prim1: Implicit_account]')
            sp.verify(sp.sender == sp.to_address(sp.build_lambda(ferror)(sp.unit)), 'WrongCondition: sp.sender == sp.to_address(sp.implicit_account(params))')
            x108 = sp.local("x108", (sp.cons(sp.set_delegate_operation(sp.some(l195)), sp.list([])), ((self.data.admin, self.data.collateral), (self.data.duration, (self.data.ledger, self.data.rate)))))
            s338 = sp.local("s338", sp.fst(x108.value))
            s339 = sp.local("s339", sp.snd(x108.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s339)]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))
          with arg.match('Right') as r196:
            sp.verify(self.data.rate >= sp.snd(r196), 'WrongCondition: self.data.rate >= params.rate')
            sp.verify(self.data.duration <= sp.fst(r196), 'WrongCondition: self.data.duration <= params.duration')
            x70 = sp.bind_block("x70"):
            with x70:
    def ferror(error):
      sp.failwith('[Error: Decompiler TODO prim2: Mem]')
              sp.if sp.build_lambda(ferror)(sp.unit):
                sp.failwith('WrongCondition: ~ (self.data.ledger.contains(sp.sender))')
              sp.else:
                x237 = sp.local("x237", sp.mul(sp.amount, self.data.rate))
                with sp.ediv(x237.value, 10000).match_cases() as arg:
                  with arg.match('None') as _l11:
                    sp.failwith(105)
                  with arg.match('Some') as s239:
                    with sp.sub_mutez(self.data.collateral, sp.fst(s239)).match_cases() as arg:
                      with arg.match('None') as _l16:
                        sp.failwith(106)
                      with arg.match('Some') as s260:
                        x293 = sp.local("x293", sp.amount + sp.fst(s239))
                        sp.result((sp.list([]), ((self.data.admin, s260), (self.data.duration, (sp.update_map(self.data.ledger, sp.sender, sp.some((x293.value, sp.add(sp.now, self.data.duration * 86400)))), self.data.rate)))))


            s338 = sp.local("s338", sp.fst(x70.value))
            s339 = sp.local("s339", sp.snd(x70.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s339)]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))



  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TPair(sp.TInt, sp.TNat), Right = sp.TVariant(Left = sp.TPair(sp.TMutez, sp.TAddress), Right = sp.TAddress).layout(("Left", "Right"))).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l5:
        sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
        sp.verify(sp.amount == sp.tez(0), 'WrongCondition: sp.amount == sp.tez(0)')
        x267 = sp.local("x267", (sp.list([]), ((self.data.admin, self.data.collateral), (sp.fst(l5), (self.data.ledger, sp.snd(l5))))))
        s191 = sp.local("s191", sp.fst(x267.value))
        s192 = sp.local("s192", sp.snd(x267.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s192)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))
      with arg.match('Right') as r6:
        with r6.match_cases() as arg:
          with arg.match('Left') as l7:
            sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
            sp.verify(sp.fst(l7) <= self.data.collateral, 'insufficient collateral')
            x239 = sp.bind_block("x239"):
            with x239:
              with sp.sub_mutez(self.data.collateral, sp.fst(l7)).match_cases() as arg:
                with arg.match('None') as _l194:
                  sp.failwith(73)
                with arg.match('Some') as s124:
                  with sp.contract(sp.TUnit, sp.snd(l7)).match_cases() as arg:
                    with arg.match('None') as _l201:
                      sp.failwith(74)
                    with arg.match('Some') as s140:
                      sp.result((sp.cons(sp.transfer_operation(sp.unit, sp.fst(l7), s140), sp.list([])), ((self.data.admin, s124), (self.data.duration, (self.data.ledger, self.data.rate)))))


            s155 = sp.local("s155", sp.fst(x239.value))
            s156 = sp.local("s156", sp.snd(x239.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s156)]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))
          with arg.match('Right') as r8:
            sp.verify(sp.amount == sp.tez(0), 'WrongCondition: sp.amount == sp.tez(0)')
            x190 = sp.bind_block("x190"):
            with x190:
              with sp.some(self.data.ledger[sp.sender]).match_cases() as arg:
                with arg.match('None') as _l145:
                  sp.failwith('NoDeposit')
                with arg.match('Some') as s26:
                  sp.verify(sp.now >= sp.snd(s26), 'WrongCondition: sp.now >= compute_baking_swap_118.value.due')
                  with sp.contract(sp.TUnit, r8).match_cases() as arg:
                    with arg.match('None') as _l151:
                      sp.failwith(120)
                    with arg.match('Some') as s46:
                      sp.result((sp.cons(sp.transfer_operation(sp.unit, sp.fst(s26), s46), sp.list([])), ((self.data.admin, self.data.collateral), (self.data.duration, (sp.update_map(self.data.ledger, sp.sender, sp.none), self.data.rate)))))


            s155 = sp.local("s155", sp.fst(x190.value))
            s156 = sp.local("s156", sp.snd(x190.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s156)]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))



sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
