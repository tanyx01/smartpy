open SmartML

module Contract = struct
  let%entry_point collateralize () =
    verify (sender = data.admin);
    data.collateral <- data.collateral + amount

  let%entry_point delegate params =
    verify (sender = data.admin);
    verify (amount = (tez 0));
    verify (sender = (to_address (implicit_account params)));
    set_delegate (some params)

  let%entry_point deposit params =
    verify (data.rate >= params.rate);
    verify (data.duration <= params.duration);
    verify (not (contains sender data.ledger));
    let%mutable compute_baking_swap_105 = (split_tokens amount data.rate (nat 10000)) in ();
    data.collateral <- data.collateral - compute_baking_swap_105;
    Map.set data.ledger sender {amount = (amount + compute_baking_swap_105); due = (add_seconds now (data.duration * (int 86400)))}

  let%entry_point set_offer params =
    verify (sender = data.admin);
    verify (amount = (tez 0));
    data.rate <- params.rate;
    data.duration <- params.duration

  let%entry_point uncollateralize params =
    verify (sender = data.admin);
    verify (params.amount <= data.collateral) ~msg:"insufficient collateral";
    data.collateral <- data.collateral - params.amount;
    send params.receiver params.amount

  let%entry_point withdraw params =
    verify (amount = (tez 0));
    let%mutable compute_baking_swap_118 = (get ~message:%s data.ledger sender "NoDeposit") in ();
    verify (now >= compute_baking_swap_118.due);
    send params compute_baking_swap_118.amount;
    Map.delete data.ledger sender

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admin = address; collateral = mutez; duration = int; ledger = map address {amount = mutez; due = timestamp}; rate = nat}]
      ~storage:[%expr
                 {admin = address "tz1UyQDepgtUBnWjyzzonqeDwaiWoQzRKSP5";
                  collateral = tez 0;
                  duration = int 365;
                  ledger = Map.make [];
                  rate = nat 700}]
      [collateralize; delegate; deposit; set_offer; uncollateralize; withdraw]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())