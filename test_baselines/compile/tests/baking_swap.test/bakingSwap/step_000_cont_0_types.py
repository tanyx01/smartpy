import smartpy as sp

tstorage = sp.TRecord(admin = sp.TAddress, collateral = sp.TMutez, duration = sp.TInt, ledger = sp.TMap(sp.TAddress, sp.TRecord(amount = sp.TMutez, due = sp.TTimestamp).layout(("amount", "due"))), rate = sp.TNat).layout((("admin", "collateral"), ("duration", ("ledger", "rate"))))
tparameter = sp.TVariant(collateralize = sp.TUnit, delegate = sp.TKeyHash, deposit = sp.TRecord(duration = sp.TInt, rate = sp.TNat).layout(("duration", "rate")), set_offer = sp.TRecord(duration = sp.TInt, rate = sp.TNat).layout(("duration", "rate")), uncollateralize = sp.TRecord(amount = sp.TMutez, receiver = sp.TAddress).layout(("amount", "receiver")), withdraw = sp.TAddress).layout((("collateralize", ("delegate", "deposit")), ("set_offer", ("uncollateralize", "withdraw"))))
tprivates = { }
tviews = { }
