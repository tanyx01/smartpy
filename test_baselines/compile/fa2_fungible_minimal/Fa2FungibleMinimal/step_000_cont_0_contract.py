import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(administrator = sp.TAddress, ledger = sp.TBigMap(sp.TPair(sp.TAddress, sp.TNat), sp.TNat), metadata = sp.TBigMap(sp.TString, sp.TBytes), next_token_id = sp.TNat, operators = sp.TBigMap(sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), sp.TUnit), supply = sp.TBigMap(sp.TNat, sp.TNat), token_metadata = sp.TBigMap(sp.TNat, sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info")))).layout((("administrator", ("ledger", "metadata")), (("next_token_id", "operators"), ("supply", "token_metadata")))))
    self.init(administrator = sp.address('tz1ZAHUDpqV6WYHV1JLz8iJVowLV2vPQwGMJ'),
              ledger = {},
              metadata = {'' : sp.bytes('0x68747470732f2f6578616d706c652e636f6d')},
              next_token_id = 0,
              operators = {},
              supply = {},
              token_metadata = {})

  @sp.entry_point
  def balance_of(self, params):
    sp.set_type(params, sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")))
    def f_x0(_x0):
      sp.verify(_x0.token_id < self.data.next_token_id, 'FA2_TOKEN_UNDEFINED')
      sp.result(sp.record(request = sp.record(owner = _x0.owner, token_id = _x0.token_id), balance = self.data.ledger.get((_x0.owner, _x0.token_id), default_value = 0)))
    sp.transfer(params.requests.map(sp.build_lambda(f_x0)), sp.tez(0), params.callback)

  @sp.entry_point
  def mint(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    with params.token.match_cases() as arg:
      with arg.match('new') as new:
        compute_fa2_fungible_minimal_163i = sp.local("compute_fa2_fungible_minimal_163i", self.data.next_token_id)
        self.data.token_metadata[compute_fa2_fungible_minimal_163i.value] = sp.record(token_id = compute_fa2_fungible_minimal_163i.value, token_info = new)
        self.data.supply[compute_fa2_fungible_minimal_163i.value] = params.amount
        self.data.ledger[(params.to_, compute_fa2_fungible_minimal_163i.value)] = params.amount
        self.data.next_token_id += 1
      with arg.match('existing') as existing:
        sp.verify(existing < self.data.next_token_id, 'FA2_TOKEN_UNDEFINED')
        self.data.supply[existing] += params.amount
        self.data.ledger[(params.to_, existing)] = self.data.ledger.get((params.to_, existing), default_value = 0) + params.amount


  @sp.entry_point
  def transfer(self, params):
    sp.for transfer in params:
      sp.for tx in transfer.txs:
        sp.set_type(tx, sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))
        sp.verify(tx.token_id < self.data.next_token_id, 'FA2_TOKEN_UNDEFINED')
        sp.verify((transfer.from_ == sp.sender) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))), 'FA2_NOT_OPERATOR')
        self.data.ledger[(transfer.from_, tx.token_id)] = sp.as_nat(self.data.ledger.get((transfer.from_, tx.token_id), default_value = 0) - tx.amount, message = 'FA2_INSUFFICIENT_BALANCE')
        self.data.ledger[(tx.to_, tx.token_id)] = self.data.ledger.get((tx.to_, tx.token_id), default_value = 0) + tx.amount

  @sp.entry_point
  def update_operators(self, params):
    sp.for update in params:
      with update.match_cases() as arg:
        with arg.match('add_operator') as add_operator:
          sp.verify(add_operator.owner == sp.sender, 'FA2_NOT_OWNER')
          self.data.operators[add_operator] = sp.unit
        with arg.match('remove_operator') as remove_operator:
          sp.verify(remove_operator.owner == sp.sender, 'FA2_NOT_OWNER')
          del self.data.operators[remove_operator]


sp.add_compilation_target("test", Contract())