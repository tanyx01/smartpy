open SmartML

module Contract = struct
  let%entry_point bar () =
    data.board <- self.zero;
    List.iter (fun k ->
      Map.set (Map.get data.board (int 5)) k 1
    ) [int 0; int 1; int 2; int 3; int 4; int 5; int 6; int 7; int 8; int 9]

  let%entry_point glider () =
    data.board <- self.zero;
    Map.set (Map.get data.board (int 3)) (int 3) 1;
    Map.set (Map.get data.board (int 4)) (int 4) 1;
    Map.set (Map.get data.board (int 5)) (int 2) 1;
    Map.set (Map.get data.board (int 5)) (int 3) 1;
    Map.set (Map.get data.board (int 5)) (int 4) 1

  let%entry_point reset () =
    data.board <- self.zero

  let%entry_point run () =
    let%mutable next = self.zero in ();
    List.iter (fun i ->
      List.iter (fun j ->
        let%mutable sum = 0 in ();
        List.iter (fun k ->
          if ((i + k) >= (int 0)) && ((i + k) < (int 10)) then
            List.iter (fun l ->
              if ((j + l) >= (int 0)) && ((j + l) < (int 10)) then
                if (k <> (int 0)) || (l <> (int 0)) then
                  sum <- sum + (Map.get (Map.get data.board (i + k)) (j + l))
            ) (range (int ((-1))) (int 2) (int 1))
        ) (range (int ((-1))) (int 2) (int 1));
        if (Map.get (Map.get data.board i) j) = 0 then
          if sum = 3 then
            Map.set (Map.get next i) j 1
        else
          if (sum >= 2) && (sum <= 3) then
            Map.set (Map.get next i) j 1
      ) [int 0; int 1; int 2; int 3; int 4; int 5; int 6; int 7; int 8; int 9]
    ) [int 0; int 1; int 2; int 3; int 4; int 5; int 6; int 7; int 8; int 9];
    data.board <- next

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {board = map int (map int intOrNat)}]
      ~storage:[%expr
                 {board = Map.make []}]
      [bar; glider; reset; run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())