open SmartML

module Contract = struct
  let%entry_point administrate params =
    verify (sender = data.admin) ~msg:"Aggregator_NotAdmin";
    set_type params (list (`changeActive bool + `changeAdmin address + `changeOracles {added = list (pair address {adminAddress = address; endingRound = option nat; startingRound = nat}); removed = list address} + `updateFutureRounds {maxSubmissions = nat; minSubmissions = nat; oraclePayment = nat; restartDelay = nat; timeout = nat}));
    List.iter (fun action ->
      match action with
        | `changeActive changeActive ->
          data.active <- changeActive
        | `changeAdmin changeAdmin ->
          data.admin <- changeAdmin
        | `updateFutureRounds updateFutureRounds ->
          verify (sender = data.admin) ~msg:"Aggregator_NotAdmin";
          let%mutable compute_price_feed_672 = (len data.oracles) in ();
          verify (updateFutureRounds.maxSubmissions >= updateFutureRounds.minSubmissions) ~msg:"Aggregator_MaxInferiorToMin";
          verify (compute_price_feed_672 >= updateFutureRounds.maxSubmissions) ~msg:"Aggregator_MaxExceedActive";
          verify ((compute_price_feed_672 = (nat 0)) || (compute_price_feed_672 > updateFutureRounds.restartDelay)) ~msg:"Aggregator_DelayExceedTotal";
          verify ((compute_price_feed_672 = (nat 0)) || (updateFutureRounds.minSubmissions > (nat 0))) ~msg:"Aggregator_MinSubmissionsTooLow";
          verify (data.recordedFunds.available >= ((updateFutureRounds.oraclePayment * compute_price_feed_672) * (nat 2))) ~msg:"Aggregator_InsufficientFundsForPayment";
          data.restartDelay <- updateFutureRounds.restartDelay;
          data.minSubmissions <- updateFutureRounds.minSubmissions;
          data.maxSubmissions <- updateFutureRounds.maxSubmissions;
          data.timeout <- updateFutureRounds.timeout;
          data.oraclePayment <- updateFutureRounds.oraclePayment
        | `changeOracles changeOracles ->
          verify (sender = data.admin) ~msg:"Aggregator_NotAdmin";
          List.iter (fun oracleAddress ->
            Map.delete data.oracles oracleAddress
          ) changeOracles.removed;
          List.iter (fun oracle ->
            match_pair_price_feed_696_fst, match_pair_price_feed_696_snd = match_tuple(oracle, "match_pair_price_feed_696_fst", "match_pair_price_feed_696_snd")
            set_type match_pair_price_feed_696_snd.endingRound (option nat);
            let%mutable endingRound = (nat 4294967295) in ();
            if is_some match_pair_price_feed_696_snd.endingRound then
              endingRound <- open_some match_pair_price_feed_696_snd.endingRound;
            Map.set data.oracles match_pair_price_feed_696_fst {adminAddress = match_pair_price_feed_696_snd.adminAddress; endingRound = endingRound; lastStartedRound = (nat 0); startingRound = match_pair_price_feed_696_snd.startingRound; withdrawable = (nat 0)};
            if (data.reportingRoundId <> (nat 0)) && (match_pair_price_feed_696_snd.startingRound <= data.reportingRoundId) then
              Set.add data.reportingRoundDetails.activeOracles match_pair_price_feed_696_fst
          ) changeOracles.added

    ) params

  let%entry_point decimals params =
    transfer data.decimals (tez 0) params

  let%entry_point forceBalanceUpdate () =
    transfer {requests = [{owner = self_address; token_id = (nat 0)}]; callback = (self_entry_point "updateAvailableFunds")} (tez 0) (open_some ~message:"Aggregator_InvalidTokenkInterface" (contract {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}} data.linkToken , entry_point='balance_of'))

  let%entry_point latestRoundData params =
    transfer (Map.get data.rounds data.latestRoundId) (tez 0) params

  let%entry_point submit params =
    match_pair_price_feed_622_fst, match_pair_price_feed_622_snd = match_tuple(params, "match_pair_price_feed_622_fst", "match_pair_price_feed_622_snd")
    verify data.active;
    verify (contains sender data.oracles) ~msg:"Aggregator_NotOracle";
    verify ((Map.get data.oracles sender).startingRound <= match_pair_price_feed_622_fst) ~msg:"Aggregator_NotYetEnabledOracle";
    verify ((Map.get data.oracles sender).endingRound > match_pair_price_feed_622_fst) ~msg:"Aggregator_NotLongerAllowedOracle";
    verify ((((match_pair_price_feed_622_fst + (nat 1)) = data.reportingRoundId) || (match_pair_price_feed_622_fst = data.reportingRoundId)) || (match_pair_price_feed_622_fst = (data.reportingRoundId + (nat 1)))) ~msg:"Aggregator_InvalidRound";
    if (match_pair_price_feed_622_fst + (nat 1)) = data.reportingRoundId then
      (
        verify (not (contains sender data.previousRoundDetails.submissions)) ~msg:"Aggregator_SubmittedInCurrent";
        verify ((len data.previousRoundDetails.submissions) < data.previousRoundDetails.maxSubmissions) ~msg:"Aggregator_RoundMaxSubmissionExceed";
        Map.set data.previousRoundDetails.submissions sender match_pair_price_feed_622_snd;
        if (len data.previousRoundDetails.submissions) >= data.previousRoundDetails.minSubmissions then
          (
            (Map.get data.rounds (open_some (is_nat (data.reportingRoundId - (nat 1))))).answer <- (Map.values data.previousRoundDetails.submissions) self.median;
            (Map.get data.rounds (open_some (is_nat (data.reportingRoundId - (nat 1))))).updatedAt <- now;
            (Map.get data.rounds (open_some (is_nat (data.reportingRoundId - (nat 1))))).answeredInRound <- data.reportingRoundId
          )
      )
    else
      if match_pair_price_feed_622_fst = data.reportingRoundId then
        (
          verify (not (contains sender data.reportingRoundDetails.submissions)) ~msg:"Aggregator_AlreadySubmittedForThisRound";
          verify ((len data.reportingRoundDetails.submissions) < data.reportingRoundDetails.maxSubmissions) ~msg:"Aggregator_RoundMaxSubmissionExceed";
          Map.set data.reportingRoundDetails.submissions sender match_pair_price_feed_622_snd;
          if (len data.reportingRoundDetails.submissions) >= data.reportingRoundDetails.minSubmissions then
            (
              (Map.get data.rounds data.reportingRoundId).answer <- (Map.values data.reportingRoundDetails.submissions) self.median;
              (Map.get data.rounds data.reportingRoundId).updatedAt <- now;
              (Map.get data.rounds data.reportingRoundId).answeredInRound <- data.reportingRoundId;
              data.latestRoundId <- data.reportingRoundId
            )
        )
      else
        (
          if data.reportingRoundId > (nat 0) then
            (
              verify (((Map.get data.oracles sender).lastStartedRound = (nat 0)) || ((data.reportingRoundId + (nat 1)) > ((Map.get data.oracles sender).lastStartedRound + data.restartDelay))) ~msg:"Aggregator_WaitBeforeInit";
              verify ((now > (add_seconds (Map.get data.rounds data.reportingRoundId).startedAt ((to_int data.timeout) * (int 60)))) || ((Map.get data.rounds data.reportingRoundId).answeredInRound = data.reportingRoundId)) ~msg:"Aggregator_PreviousRoundNotOver"
            );
          let%mutable answer = (nat 0) in ();
          let%mutable answeredInRound = (nat 0) in ();
          if data.minSubmissions = (nat 1) then
            (
              answer <- match_pair_price_feed_622_snd;
              answeredInRound <- data.reportingRoundId + (nat 1)
            );
          Map.set data.rounds (data.reportingRoundId + (nat 1)) {answer = answer; answeredInRound = answeredInRound; roundId = (data.reportingRoundId + (nat 1)); startedAt = now; updatedAt = now};
          data.previousRoundDetails <- data.reportingRoundDetails;
          let%mutable compute_price_feed_588 = ((data.reportingRoundId + (nat 1)) self.getActiveOracles) in ();
          data.reportingRoundDetails <- {activeOracles = compute_price_feed_588; maxSubmissions = data.maxSubmissions; minSubmissions = data.minSubmissions; submissions = (Map.make [(sender, match_pair_price_feed_622_snd)]); timeout = data.timeout};
          (Map.get data.oracles sender).lastStartedRound <- data.reportingRoundId + (nat 1);
          data.reportingRoundId <- data.reportingRoundId + (nat 1)
        );
    data.recordedFunds.available <- open_some ~message:"Aggregator_OraclePaymentUnderflow" (is_nat (data.recordedFunds.available - data.oraclePayment));
    data.recordedFunds.allocated <- data.recordedFunds.allocated + data.oraclePayment;
    (Map.get data.oracles sender).withdrawable <- (Map.get data.oracles sender).withdrawable + data.oraclePayment

  let%entry_point updateAvailableFunds params =
    set_type params (list {balance = nat; request = {owner = address; token_id = nat}});
    verify (sender = data.linkToken) ~msg:"Aggregator_NotLinkToken";
    let%mutable balance = (nat 0) in ();
    List.iter (fun resp ->
      verify (resp.request.owner = self_address);
      balance <- resp.balance
    ) params;
    if balance <> data.recordedFunds.available then
      data.recordedFunds.available <- balance

  let%entry_point withdrawPayment params =
    verify ((Map.get data.oracles params.oracleAddress).adminAddress = sender) ~msg:"Aggregator_NotOracleAdmin";
    verify ((Map.get data.oracles params.oracleAddress).withdrawable >= params.amount) ~msg:"Aggregator_InsufficientWithdrawableFunds";
    (Map.get data.oracles params.oracleAddress).withdrawable <- open_some (is_nat ((Map.get data.oracles params.oracleAddress).withdrawable - params.amount));
    data.recordedFunds.allocated <- open_some (is_nat (data.recordedFunds.allocated - params.amount));
    transfer [{from_ = self_address; txs = [{to_ = params.recipientAddress; token_id = (nat 0); amount = params.amount}]}] (tez 0) (open_some ~message:"Aggregator_InvalidTokenkInterface" (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) data.linkToken , entry_point='transfer'));
    transfer {requests = [{owner = self_address; token_id = (nat 0)}]; callback = (self_entry_point "updateAvailableFunds")} (tez 0) (open_some ~message:"Aggregator_InvalidTokenkInterface" (contract {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}} data.linkToken , entry_point='balance_of'))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {active = bool; admin = address; decimals = nat; latestRoundId = nat; linkToken = address; maxSubmissions = nat; metadata = big_map string bytes; minSubmissions = nat; oraclePayment = nat; oracles = map address {adminAddress = address; endingRound = nat; lastStartedRound = nat; startingRound = nat; withdrawable = nat}; previousRoundDetails = {activeOracles = set address; maxSubmissions = nat; minSubmissions = nat; submissions = map address nat; timeout = nat}; recordedFunds = {allocated = nat; available = nat}; reportingRoundDetails = {activeOracles = set address; maxSubmissions = nat; minSubmissions = nat; submissions = map address nat; timeout = nat}; reportingRoundId = nat; restartDelay = nat; rounds = big_map nat {answer = nat; answeredInRound = nat; roundId = nat; startedAt = timestamp; updatedAt = timestamp}; timeout = nat}]
      ~storage:[%expr
                 {active = false;
                  admin = address "KT1_ADMIN_ADDRESS";
                  decimals = nat 8;
                  latestRoundId = nat 0;
                  linkToken = address "KT1_LINK_TOKEN_ADDRESS";
                  maxSubmissions = nat 6;
                  metadata = Map.make [("", bytes "0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d5033594851473542674b4d6d576b436e59326a4c536f7761633357546a376355583754643463477347336279")];
                  minSubmissions = nat 3;
                  oraclePayment = nat 1;
                  oracles = Map.make [];
                  previousRoundDetails = {activeOracles = Set.make([]); maxSubmissions = nat 0; minSubmissions = nat 0; submissions = Map.make []; timeout = nat 0};
                  recordedFunds = {allocated = nat 0; available = nat 0};
                  reportingRoundDetails = {activeOracles = Set.make([]); maxSubmissions = nat 0; minSubmissions = nat 0; submissions = Map.make []; timeout = nat 0};
                  reportingRoundId = nat 0;
                  restartDelay = nat 2;
                  rounds = Map.make [];
                  timeout = nat 10}]
      [administrate; decimals; forceBalanceUpdate; latestRoundData; submit; updateAvailableFunds; withdrawPayment]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())