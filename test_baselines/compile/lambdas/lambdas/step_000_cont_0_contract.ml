open SmartML

module Contract = struct
  let%entry_point abs_test params =
    data.abcd <- params self.abs

  let%entry_point comp_test () =
    data.abcd <- {f = (fun _x12 -> result (_x12 + (int 3))); x = (int 2)} self.comp

  let%entry_point f () =
    let%mutable toto = (fun _x14 -> result ((fst _x14) + (snd _x14))) in ();
    let%mutable titi = (apply_lambda (nat 5) toto) in ();
    data.value <- (nat 8) titi

  let%entry_point fact params =
    let%mutable compute_lambdas_133 = (fun _x16 -> result (eif (_x16 <= (int 1)) (int 1) (_x16 * ((_x16 - (int 1)) _f17)))) in ();
    data.abcd <- params compute_lambdas_133

  let%entry_point flambda () =
    data.value <- (((nat 15) self.flam) self.flam) + ((nat 12345) self.square_root)

  let%entry_point h () =
    data.fff <- some (fun _x18 -> verify (_x18 >= (nat 0));
let%mutable y = _x18 in ();
while (y * y) > _x18 do
  y <- ((_x18 / y) + y) / (nat 2)
done;
verify (((y * y) <= _x18) && (_x18 < ((y + (nat 1)) * (y + (nat 1)))));
result y)

  let%entry_point hh params =
    data.value <- params (open_some data.fff)

  let%entry_point i () =
    let%mutable ch1 = (fun _x20 -> verify (_x20 >= 0)) in ();
    let%mutable ch2 = (fun _x22 -> verify (_x22 >= 0);
result (_x22 - 2)) in ();
    let%mutable ch3 = (fun _x24 -> verify (_x24 >= 0);
result true) in ();
    let%mutable ch4 = (fun _x26 -> let%mutable ch3b = (fun _x28 -> verify (_x28 >= 0);
result false) in ();
verify (_x26 >= (nat 0));
result ((nat 3) * _x26)) in ();
    data.value <- (nat 12) ch4;
    let%mutable compute_lambdas_96 = (() self.not_pure) in ();
    verify (compute_lambdas_96 = data.value)

  let%entry_point operation_creation () =
    let%mutable f = (fun _x30 -> let%mutable __operations__ = (set_type_expr [] (list operation)) in ();
let%mutable create_contract_lambdas_101 = create contract ... in ();
operations <- create_contract_lambdas_101.operation :: operations;
let%mutable create_contract_lambdas_102 = create contract ... in ();
operations <- create_contract_lambdas_102.operation :: operations;
result operations) in ();
    List.iter (fun op ->
      operations <- op :: operations
    ) ((int 12345001) f);
    List.iter (fun op ->
      operations <- op :: operations
    ) ((int 12345002) f)

  let%entry_point operation_creation_result () =
    let%mutable f = (fun _x32 -> let%mutable __operations__ = (set_type_expr [] (list operation)) in ();
let%mutable create_contract_lambdas_110 = create contract ... in ();
operations <- create_contract_lambdas_110.operation :: operations;
let%var __s2 = 4 in
result (operations, __s2)) in ();
    let%mutable x = ((int 12345001) f) in ();
    let%mutable y = ((int 12345002) f) in ();
    List.iter (fun op ->
      operations <- op :: operations
    ) (fst x);
    List.iter (fun op ->
      operations <- op :: operations
    ) (fst y);
    let%mutable sum = ((snd x) + (snd y)) in ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {abcd = int; f = lambda intOrNat intOrNat; fff = option lambda nat nat; ggg = option intOrNat; value = nat}]
      ~storage:[%expr
                 {abcd = int 0;
                  f = lambda;
                  fff = None;
                  ggg = Some(42);
                  value = nat 0}]
      [abs_test; comp_test; f; fact; flambda; h; hh; i; operation_creation; operation_creation_result]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())