import smartpy as sp

tstorage = sp.TRecord(abcd = sp.TInt, f = sp.TLambda(sp.TIntOrNat, sp.TIntOrNat), fff = sp.TOption(sp.TLambda(sp.TNat, sp.TNat)), ggg = sp.TOption(sp.TIntOrNat), value = sp.TNat).layout((("abcd", "f"), ("fff", ("ggg", "value"))))
tparameter = sp.TVariant(abs_test = sp.TInt, comp_test = sp.TUnit, f = sp.TUnit, fact = sp.TInt, flambda = sp.TUnit, h = sp.TUnit, hh = sp.TNat, i = sp.TUnit, operation_creation = sp.TUnit, operation_creation_result = sp.TUnit).layout(((("abs_test", "comp_test"), ("f", ("fact", "flambda"))), (("h", "hh"), ("i", ("operation_creation", "operation_creation_result")))))
tprivates = { "abs": sp.TLambda(sp.TInt, sp.TInt), "comp": sp.TLambda(sp.TRecord(f = sp.TLambda(sp.TInt, sp.TInt), x = sp.TInt).layout(("f", "x")), sp.TInt), "flam": sp.TLambda(sp.TNat, sp.TNat), "not_pure": sp.TLambda(sp.TUnit, sp.TNat, with_storage="read-write"), "oh_no": sp.TLambda(sp.TIntOrNat, sp.TInt), "square_root": sp.TLambda(sp.TNat, sp.TNat) }
tviews = { }
