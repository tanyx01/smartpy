import smartpy as sp

tstorage = sp.TRecord(deck = sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TInt)), draw = sp.TBool, nbMoves = sp.TIntOrNat, nextPlayer = sp.TInt, winner = sp.TInt).layout((("deck", "draw"), ("nbMoves", ("nextPlayer", "winner"))))
tparameter = sp.TVariant(play = sp.TRecord(i = sp.TIntOrNat, j = sp.TIntOrNat, move = sp.TInt).layout(("i", ("j", "move")))).layout("play")
tprivates = { }
tviews = { }
