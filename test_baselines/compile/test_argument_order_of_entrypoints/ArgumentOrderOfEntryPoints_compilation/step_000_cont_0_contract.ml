open SmartML

module Contract = struct
  let%entry_point ep1 params =
    data.b <- params.b;
    data.a <- params.a;
    data.c <- params.c

  let%entry_point ep2 params =
    set_type params.a int;
    set_type params.c bool;
    set_type params.b string

  let%entry_point ep3 params =
    data.b <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = nat; b = string; c = bool}]
      ~storage:[%expr
                 {a = nat 1;
                  b = "";
                  c = true}]
      [ep1; ep2; ep3]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())