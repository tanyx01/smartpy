open SmartML

module Contract = struct
  let%entry_point aggregated_endorsement params =
    set_type params (list {proposalId = nat; signatures = list {signature = signature; signerAddress = address}});
    List.iter (fun endorsment ->
      List.iter (fun signature ->
        verify (contains signature.signerAddress data.signers) ~msg:"MULTISIG_SignerUnknown";
        verify (check_signature (Map.get data.signers signature.signerAddress).publicKey signature.signature (pack {contractAddress = self_address; proposalId = endorsment.proposalId})) ~msg:"MULTISIG_Badsig";
        let%mutable compute_admin_multisig_302 = ({proposalId = endorsment.proposalId; signerAddress = signature.signerAddress} self.registerEndorsement) in ()
      ) endorsment.signatures;
      let%mutable compute_admin_multisig_308 = (Map.get data.proposals endorsment.proposalId) in ();
      if (len compute_admin_multisig_308.endorsements) >= data.quorum then
        let%mutable compute_admin_multisig_310 = ({actions = compute_admin_multisig_308.actions; proposalId = endorsment.proposalId} self.onApproved) in ()
    ) params

  let%entry_point aggregated_proposal params =
    set_type params {actions = `external (list {actions = bytes; target = address}) + `internal (list (`changeMetadata (pair string (option bytes)) + `changeQuorum nat + `changeSigners (`added (list {address = address; publicKey = key}) + `removed (set address)))); proposalId = nat; signatures = list {signature = signature; signerAddress = address}};
    verify (contains sender data.signers) ~msg:"MULTISIG_SignerUnknown";
    data.lastProposalId <- data.lastProposalId + (nat 1);
    verify (data.lastProposalId = params.proposalId) ~msg:"MULTISIG_InvalidProposalId";
    let%mutable compute_admin_multisig_232 = {actions = params.actions; endorsements = (Set.make [sender]); initiator = sender; startedAt = now} in ();
    if (Map.get data.signers sender).lastProposalId <> None then
      Set.remove data.activeProposals (open_some (Map.get data.signers sender).lastProposalId);
    (Map.get data.signers sender).lastProposalId <- some params.proposalId;
    Set.add data.activeProposals params.proposalId;
    Map.set data.proposals params.proposalId compute_admin_multisig_232;
    let%mutable compute_admin_multisig_251 = (pack {actions = params.actions; contractAddress = self_address; proposalId = params.proposalId}) in ();
    List.iter (fun signature ->
      verify (contains signature.signerAddress data.signers) ~msg:"MULTISIG_SignerUnknown";
      verify (check_signature (Map.get data.signers signature.signerAddress).publicKey signature.signature compute_admin_multisig_251) ~msg:"MULTISIG_Badsig";
      Set.add compute_admin_multisig_232.endorsements signature.signerAddress
    ) params.signatures;
    if (len compute_admin_multisig_232.endorsements) >= data.quorum then
      let%mutable compute_admin_multisig_273 = ({actions = compute_admin_multisig_232.actions; proposalId = params.proposalId} self.onApproved) in ()

  let%entry_point cancel_proposal params =
    verify (contains sender data.signers) ~msg:"MULTISIG_SignerUnknown";
    verify ((Map.get data.proposals params).initiator = sender) ~msg:"MULTISIG_NotInitiator";
    Set.remove data.activeProposals params

  let%entry_point endorsement params =
    verify (contains sender data.signers) ~msg:"MULTISIG_SignerUnknown";
    List.iter (fun pId ->
      let%mutable compute_admin_multisig_204 = ({proposalId = pId; signerAddress = sender} self.registerEndorsement) in ();
      if (len (Map.get data.proposals pId).endorsements) >= data.quorum then
        let%mutable compute_admin_multisig_214 = ({actions = (Map.get data.proposals pId).actions; proposalId = pId} self.onApproved) in ()
    ) params

  let%entry_point proposal params =
    verify (contains sender data.signers) ~msg:"MULTISIG_SignerUnknown";
    if (Map.get data.signers sender).lastProposalId <> None then
      Set.remove data.activeProposals (open_some (Map.get data.signers sender).lastProposalId);
    data.lastProposalId <- data.lastProposalId + (nat 1);
    Set.add data.activeProposals data.lastProposalId;
    Map.set data.proposals data.lastProposalId {actions = params; endorsements = (Set.make [sender]); initiator = sender; startedAt = now};
    (Map.get data.signers sender).lastProposalId <- some data.lastProposalId;
    if data.quorum < (nat 2) then
      let%mutable compute_admin_multisig_187 = ({actions = params; proposalId = data.lastProposalId} self.onApproved) in ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {activeProposals = set nat; lastProposalId = nat; metadata = big_map string bytes; proposals = big_map nat {actions = `external (list {actions = bytes; target = address}) + `internal (list (`changeMetadata (pair string (option bytes)) + `changeQuorum nat + `changeSigners (`added (list {address = address; publicKey = key}) + `removed (set address)))); endorsements = set address; initiator = address; startedAt = timestamp}; quorum = nat; signers = map address {lastProposalId = option nat; publicKey = key}}]
      ~storage:[%expr
                 {activeProposals = Set.make([]);
                  lastProposalId = nat 0;
                  metadata = Map.make [("", bytes "0x697066733a2f2f")];
                  proposals = Map.make [];
                  quorum = nat 1;
                  signers = Map.make [(address "KT1_SIGNER1_ADDRESS", {lastProposalId = None; publicKey = key "KT1_SIGNER1_KEY"})]}]
      [aggregated_endorsement; aggregated_proposal; cancel_proposal; endorsement; proposal]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())