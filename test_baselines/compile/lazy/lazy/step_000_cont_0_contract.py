import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(bb = sp.TBigMap(sp.TBool, sp.TInt), msg = sp.TBigMap(sp.TNat, sp.TString), s = sp.TString, x = sp.TIntOrNat).layout((("bb", "msg"), ("s", "x"))))
    self.init(bb = {},
              msg = {0 : 'Bad compare', 1 : 'abcdefg'},
              s = '',
              x = 0)

  @sp.entry_point
  def myEntryPoint(self, params):
    sp.verify(params.x <= 123, self.get_error((self.data, 0)))
    sp.verify(params.y <= 123, self.get_error((self.data, 1)))
    sp.verify(params.z <= 123, self.get_error((self.data, 2)))
    self.data.x += (params.x + params.y) + params.z

  @sp.entry_point
  def myEntryPoint2(self):
    self.data.s = self.get_error((self.data, 0)) + self.get_error((self.data, 1))

  @sp.private_lambda()
  def get_error(_x0):
    sp.result(sp.fst(_x0).msg.get(sp.snd(_x0), default_value = ''))

sp.add_compilation_target("test", Contract())