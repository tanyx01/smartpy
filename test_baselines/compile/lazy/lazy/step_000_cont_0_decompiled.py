import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(bb = sp.TBigMap(sp.TBool, sp.TInt), msg = sp.TBigMap(sp.TNat, sp.TString), s = sp.TString, x = sp.TInt).layout((("bb", "msg"), ("s", "x"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)), Right = sp.TUnit).layout(("Left", "Right")))
    def ferror(error):
      sp.failwith('[Error: to_cmd: (fun (x1 : { ((big_map(bool, int) ; big_map(nat, string)) ; (string ; int)) ; nat }) : string ->
match Get(Cdr(x1), Cdr(Car(Car(x1)))) with
| None _ -> ""
| Some s11 -> s11
end)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
