import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(myParameter1 = sp.TInt, myParameter2 = sp.TInt).layout(("myParameter1", "myParameter2")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TInt)
    sp.verify(self.data.myParameter1 <= 123, 'WrongCondition: self.data.myParameter1 <= 123')
    self.data.myParameter1 = params + self.data.myParameter1

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
