import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(balanceCounterparty = sp.TMutez, balanceOwner = sp.TMutez, counterparty = sp.TAddress, epoch = sp.TTimestamp, fromCounterparty = sp.TMutez, fromOwner = sp.TMutez, hashedSecret = sp.TBytes, owner = sp.TAddress).layout(((("balanceCounterparty", "balanceOwner"), ("counterparty", "epoch")), (("fromCounterparty", "fromOwner"), ("hashedSecret", "owner")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as _l0:
        sp.verify(sp.tez(0) == self.data.balanceCounterparty, 'WrongCondition: self.data.balanceCounterparty == sp.tez(0)')
        sp.verify(sp.amount == self.data.fromCounterparty, 'WrongCondition: sp.amount == self.data.fromCounterparty')
        x70 = sp.local("x70", (((self.data.fromCounterparty, self.data.balanceOwner), (self.data.counterparty, self.data.epoch)), ((self.data.fromCounterparty, self.data.fromOwner), (self.data.hashedSecret, self.data.owner))))
        s231 = sp.local("s231", x70.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s231)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))
      with arg.match('Right') as _r1:
        sp.verify(sp.tez(0) == self.data.balanceOwner, 'WrongCondition: self.data.balanceOwner == sp.tez(0)')
        sp.verify(sp.amount == self.data.fromOwner, 'WrongCondition: sp.amount == self.data.fromOwner')
        x35 = sp.local("x35", (((self.data.balanceCounterparty, self.data.fromOwner), (self.data.counterparty, self.data.epoch)), ((self.data.fromCounterparty, self.data.fromOwner), (self.data.hashedSecret, self.data.owner))))
        s231 = sp.local("s231", x35.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s231)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))


  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TBytes, Right = sp.TUnit).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l5:
        sp.verify(sp.now < self.data.epoch, 'WrongCondition: sp.now < self.data.epoch')
    def ferror(error):
      sp.failwith('[Error: prim1: Blake2b]')
        sp.verify(self.data.hashedSecret == sp.build_lambda(ferror)(sp.unit), 'WrongCondition: self.data.hashedSecret == sp.blake2b(params.secret)')
        sp.verify(sp.sender == self.data.counterparty, 'WrongCondition: sp.sender == self.data.counterparty')
        x182 = sp.bind_block("x182"):
        with x182:
          with sp.contract(sp.TUnit, self.data.counterparty).match_cases() as arg:
            with arg.match('None') as _l128:
              sp.failwith(30)
            with arg.match('Some') as s112:
              x139 = sp.local("x139", self.data.balanceOwner + self.data.balanceCounterparty)
              sp.result((sp.cons(sp.transfer_operation(sp.unit, x139.value, s112), sp.list([])), (((sp.tez(0), sp.tez(0)), (self.data.counterparty, self.data.epoch)), ((self.data.fromCounterparty, self.data.fromOwner), (self.data.hashedSecret, self.data.owner)))))

        s153 = sp.local("s153", sp.fst(x182.value))
        s154 = sp.local("s154", sp.snd(x182.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s154)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))
      with arg.match('Right') as _r74:
        sp.verify(sp.now > self.data.epoch, 'WrongCondition: self.data.epoch < sp.now')
        sp.verify(sp.sender == self.data.owner, 'WrongCondition: sp.sender == self.data.owner')
        x124 = sp.bind_block("x124"):
        with x124:
          with sp.contract(sp.TUnit, self.data.owner).match_cases() as arg:
            with arg.match('None') as _l76:
              sp.failwith(30)
            with arg.match('Some') as s29:
              x56 = sp.local("x56", self.data.balanceOwner + self.data.balanceCounterparty)
              sp.result((sp.cons(sp.transfer_operation(sp.unit, x56.value, s29), sp.list([])), (((sp.tez(0), sp.tez(0)), (self.data.counterparty, self.data.epoch)), ((self.data.fromCounterparty, self.data.fromOwner), (self.data.hashedSecret, self.data.owner)))))

        s153 = sp.local("s153", sp.fst(x124.value))
        s154 = sp.local("s154", sp.snd(x124.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s154)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))


sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
