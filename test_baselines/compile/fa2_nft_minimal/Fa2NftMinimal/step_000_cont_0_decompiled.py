import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(administrator = sp.TAddress, ledger = sp.TBigMap(sp.TNat, sp.TAddress), metadata = sp.TBigMap(sp.TString, sp.TBytes), next_token_id = sp.TNat, operators = sp.TBigMap(sp.TPair(sp.TAddress, sp.TPair(sp.TAddress, sp.TNat)), sp.TUnit), token_metadata = sp.TBigMap(sp.TNat, sp.TPair(sp.TNat, sp.TMap(sp.TString, sp.TBytes)))).layout((("administrator", ("ledger", "metadata")), ("next_token_id", ("operators", "token_metadata")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TPair(sp.TList(sp.TPair(sp.TAddress, sp.TNat)), sp.TContract(sp.TList(sp.TPair(sp.TPair(sp.TAddress, sp.TNat), sp.TNat)))), Right = sp.TPair(sp.TMap(sp.TString, sp.TBytes), sp.TAddress)).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l252:
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [r434; r435; r436; r437; _; _; r440] =
          map [ Car(l252)
              ; 0#mutez
              ; Cdr(l252)
              ; Nil<operation>
              ; Nil<operation>
              ; l252
              ; Pair
                  ( Pair
                      ( __storage.administrator
                      , Pair(__storage.ledger, __storage.metadata)
                      )
                  , Pair
                      ( __storage.next_token_id
                      , Pair(__storage.operators, __storage.token_metadata)
                      )
                  )
              ]
          step s354; s355; s356; s357; s358; s359; s360 ->
            if Lt(Compare(Cdr(s354), Car(Cdr(s360))))
            then
              match Get(Cdr(s354), Car(Cdr(Car(s360)))) with
              | None _ -> Failwith(141)
              | Some s408 ->
                  let [s423; s424; s425; s426; s427; s428; s429; s430] =
                    if Eq(Compare(s408, Car(s354)))
                    then
                      [ 1#nat; s354; s355; s356; s357; s358; s359; s360 ]
                    else
                      [ 0#nat; s354; s355; s356; s357; s358; s359; s360 ]
                  [ Pair(s424, s423); s425; s426; s427; s428; s429; s430 ]
              end
            else
              Failwith("FA2_TOKEN_UNDEFINED")
        Pair
          ( Cons(Transfer_tokens(r434, r435, r436), r437)
          , record_of_tree(..., r440)
          )]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))
      with arg.match('Right') as r253:
        sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
        x293 = sp.local("x293", self.data.next_token_id)
        x302 = sp.local("x302", ((self.data.administrator, (self.data.ledger, self.data.metadata)), (self.data.next_token_id, (self.data.operators, sp.update_map(self.data.token_metadata, x293.value, sp.some((x293.value, sp.fst(r253))))))))
        x332 = sp.local("x332", ((self.data.administrator, (sp.update_map(self.data.ledger, x293.value, sp.some(sp.snd(r253))), self.data.metadata)), (self.data.next_token_id, (self.data.operators, sp.update_map(self.data.token_metadata, x293.value, sp.some((x293.value, sp.fst(r253))))))))
    def ferror(error):
      sp.failwith('[Error: to_cmd: Unpair(2, x332)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))


  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TList(sp.TPair(sp.TAddress, sp.TList(sp.TPair(sp.TAddress, sp.TPair(sp.TNat, sp.TNat))))), Right = sp.TList(sp.TVariant(Left = sp.TPair(sp.TAddress, sp.TPair(sp.TAddress, sp.TNat)), Right = sp.TPair(sp.TAddress, sp.TPair(sp.TAddress, sp.TNat))).layout(("Left", "Right")))).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l5:
    def ferror(error):
      sp.failwith('[Error: to_cmd: iter [ l5
             ; l5
             ; Pair
                 ( Pair
                     ( __storage.administrator
                     , Pair(__storage.ledger, __storage.metadata)
                     )
                 , Pair
                     ( __storage.next_token_id
                     , Pair(__storage.operators, __storage.token_metadata)
                     )
                 )
             ]
        step s71; s72; s73 ->
          let [_; r243; r244] =
            iter [ Cdr(s71); s71; s72; s73 ]
            step s77; s78; s79; s80 ->
              if Lt(Compare(Car(Cdr(s77)), Car(Cdr(s80))))
              then
                let [s135; s136; s137; s138; s139] =
                  if Eq(Compare(Car(s78), Sender))
                  then
                    [ True; s77; s78; s79; s80 ]
                  else
                    [ Mem
                        ( Pair(Car(s78), Pair(Sender, Car(Cdr(s77))))
                        , Car(Cdr(Cdr(s80)))
                        )
                    ; s77
                    ; s78
                    ; s79
                    ; s80
                    ]
                if s135
                then
                  let [s239; s240; s241] =
                    if Lt(Compare(0#nat, Cdr(Cdr(s136))))
                    then
                      let [s193; s194; s195; s196; s197] =
                        if Eq(Compare(1#nat, Cdr(Cdr(s136))))
                        then
                          match Get(Car(Cdr(s136)), Car(Cdr(Car(s139)))) with
                          | None _ -> Failwith(93)
                          | Some s183 ->
                              [ Eq(Compare(s183, Car(s137)))
                              ; s136
                              ; s137
                              ; s138
                              ; s139
                              ]
                          end
                        else
                          [ False; s136; s137; s138; s139 ]
                      if s193
                      then
                        [ s195
                        ; s196
                        ; Pair
                            ( Pair
                                ( Car(Car(s197))
                                , Pair
                                    ( Update(Car(Cdr(s194)), Some_(Car(s194)), Car(Cdr(Car(s197))))
                                    , Cdr(Cdr(Car(s197)))
                                    )
                                )
                            , Cdr(s197)
                            )
                        ]
                      else
                        Failwith("FA2_INSUFFICIENT_BALANCE")
                    else
                      [ s137; s138; s139 ]
                  [ s239; s240; s241 ]
                else
                  Failwith("FA2_NOT_OPERATOR")
              else
                Failwith("FA2_TOKEN_UNDEFINED")
          [ r243; r244 ]]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))
      with arg.match('Right') as r6:
    def ferror(error):
      sp.failwith('[Error: to_cmd: iter [ r6
             ; r6
             ; Pair
                 ( Pair
                     ( __storage.administrator
                     , Pair(__storage.ledger, __storage.metadata)
                     )
                 , Pair
                     ( __storage.next_token_id
                     , Pair(__storage.operators, __storage.token_metadata)
                     )
                 )
             ]
        step s9; s10; s11 ->
          match s9 with
          | add_operator l12 ->
              let [s64; s65] =
                if Eq(Compare(Sender, Car(l12)))
                then
                  [ s10
                  ; let [x775; x776] = Unpair(2, s11)
                    Pair
                      ( x775
                      , let [x781; x782] = Unpair(2, x776)
                        Pair
                          ( x781
                          , let [x787; x788] = Unpair(2, x782)
                            let _ = x787
                            Pair
                              ( Update(l12, Some_(()), Car(Cdr(Cdr(s11))))
                              , x788
                              )
                          )
                      )
                  ]
                else
                  Failwith("FA2_NOT_OWNER")
              [ s64; s65 ]
          | remove_operator r13 ->
              let [s64; s65] =
                if Eq(Compare(Sender, Car(r13)))
                then
                  [ s10
                  ; let [x773; x774] = Unpair(2, s11)
                    Pair
                      ( x773
                      , let [x779; x780] = Unpair(2, x774)
                        Pair
                          ( x779
                          , let [x785; x786] = Unpair(2, x780)
                            let _ = x785
                            Pair
                              ( Update(r13, None<unit>, Car(Cdr(Cdr(s11))))
                              , x786
                              )
                          )
                      )
                  ]
                else
                  Failwith("FA2_NOT_OWNER")
              [ s64; s65 ]
          end]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))


sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
