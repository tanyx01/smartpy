import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TPair(sp.TInt, sp.TInt))

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = (sp.snd(self.data), sp.fst(self.data))

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = (sp.snd(self.data), sp.fst(self.data))

  @sp.entry_point
  def ep3(self, params):
    sp.set_type(params, sp.TUnit)
    sp.snd(self.data) = sp.fst(self.data)

  @sp.entry_point
  def ep4(self, params):
    sp.set_type(params, sp.TUnit)
    sp.fst(self.data) = sp.snd(self.data)

  @sp.entry_point
  def ep5(self, params):
    sp.set_type(params, sp.TNat)
    with sp.ediv(params, 2).match_cases() as arg:
      with arg.match('None') as _l24:
        sp.fst(self.data) = 0
        sp.snd(self.data) = 0
      with arg.match('Some') as _r25:
        sp.fst(self.data) = 1
        sp.snd(self.data) = 1


  @sp.entry_point
  def ep6(self, params):
    sp.set_type(params, sp.TNat)
    with sp.ediv(params, 2).match_cases() as arg:
      with arg.match('None') as _l36:
        sp.fst(self.data) = 0
        sp.snd(self.data) = 0
      with arg.match('Some') as _r37:
        sp.fst(self.data) = 1
        sp.snd(self.data) = 1


sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
