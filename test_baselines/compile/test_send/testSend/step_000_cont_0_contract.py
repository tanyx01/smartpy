import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep(self):
    sp.send(sp.address('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'), sp.tez(1))

sp.add_compilation_target("test", Contract())