import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(counterparty = sp.TAddress, epoch = sp.TTimestamp, hashedSecret = sp.TBytes, notional = sp.TMutez, owner = sp.TAddress).layout((("counterparty", "epoch"), ("hashedSecret", ("notional", "owner")))))

  @sp.entry_point
  def allSigned(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify(sp.tez(0) != self.data.notional, 'WrongCondition: self.data.notional != sp.tez(0)')
    sp.verify(sp.sender == self.data.owner, 'WrongCondition: self.data.owner == sp.sender')
    x37 = sp.bind_block("x37"):
    with x37:
      with sp.contract(sp.TUnit, self.data.counterparty).match_cases() as arg:
        with arg.match('None') as _l0:
          sp.failwith(23)
        with arg.match('Some') as s144:
          sp.result((sp.cons(sp.transfer_operation(sp.unit, self.data.notional, s144), sp.list([])), ((self.data.counterparty, self.data.epoch), (self.data.hashedSecret, (sp.tez(0), self.data.owner)))))

    s165 = sp.local("s165", sp.fst(x37.value))
    s166 = sp.local("s166", sp.snd(x37.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s166)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TUnit, Right = sp.TBytes).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as _l41:
        sp.verify(sp.tez(0) != self.data.notional, 'WrongCondition: self.data.notional != sp.tez(0)')
        sp.verify(sp.sender == self.data.owner, 'WrongCondition: self.data.owner == sp.sender')
        sp.verify(sp.now > self.data.epoch, 'WrongCondition: self.data.epoch < sp.now')
        x133 = sp.bind_block("x133"):
        with x133:
          with sp.contract(sp.TUnit, self.data.owner).match_cases() as arg:
            with arg.match('None') as _l91:
              sp.failwith(30)
            with arg.match('Some') as s99:
              sp.result((sp.cons(sp.transfer_operation(sp.unit, self.data.notional, s99), sp.list([])), ((self.data.counterparty, self.data.epoch), (self.data.hashedSecret, (sp.tez(0), self.data.owner)))))

        s120 = sp.local("s120", sp.fst(x133.value))
        s121 = sp.local("s121", sp.snd(x133.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s121)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))
      with arg.match('Right') as r6:
        sp.verify(self.data.notional != sp.tez(0), 'WrongCondition: self.data.notional != sp.tez(0)')
        sp.verify(self.data.counterparty == sp.sender, 'WrongCondition: self.data.counterparty == sp.sender')
    def ferror(error):
      sp.failwith('[Error: prim1: Blake2b]')
        sp.verify(self.data.hashedSecret == sp.build_lambda(ferror)(sp.unit), 'WrongCondition: self.data.hashedSecret == sp.blake2b(params.secret)')
        x87 = sp.bind_block("x87"):
        with x87:
          with sp.contract(sp.TUnit, self.data.counterparty).match_cases() as arg:
            with arg.match('None') as _l44:
              sp.failwith(37)
            with arg.match('Some') as s48:
              sp.result((sp.cons(sp.transfer_operation(sp.unit, self.data.notional, s48), sp.list([])), ((self.data.counterparty, self.data.epoch), (self.data.hashedSecret, (sp.tez(0), self.data.owner)))))

        s120 = sp.local("s120", sp.fst(x87.value))
        s121 = sp.local("s121", sp.snd(x87.value))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s121)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))


sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
