import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(aaa = sp.TSet(sp.TNat), abc = sp.TList(sp.TOption(sp.TInt)), abca = sp.TMap(sp.TInt, sp.TOption(sp.TInt)), acb = sp.TString, b = sp.TBool, ddd = sp.TList(sp.TInt), f = sp.TBool, h = sp.TBytes, i = sp.TInt, m = sp.TMap(sp.TInt, sp.TInt), n = sp.TNat, pkh = sp.TKeyHash, s = sp.TString, toto = sp.TString).layout(((("aaa", ("abc", "abca")), (("acb", "b"), ("ddd", "f"))), (("h", ("i", "m")), (("n", "pkh"), ("s", "toto"))))))

  @sp.entry_point
  def comparisons(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify(self.data.i <= 123, 'WrongCondition: self.data.i <= 123')
    sp.verify((2 + self.data.i) == 12, 'WrongCondition: (2 + self.data.i) == 12')
    sp.verify((2 + self.data.i) != 1234, 'WrongCondition: (2 + self.data.i) != 1234')
    sp.verify((self.data.i + 4) != 123, 'WrongCondition: (self.data.i + 4) != 123')
    sp.verify((self.data.i - 5) < 123, 'WrongCondition: (self.data.i - 5) < 123')
    sp.verify((7 - self.data.i) < 123, 'WrongCondition: (7 - self.data.i) < 123')
    sp.verify(3 < self.data.i, 'WrongCondition: self.data.i > 3')
    sp.verify((4 * self.data.i) > 3, 'WrongCondition: (4 * self.data.i) > 3')
    sp.verify((self.data.i * 5) > 3, 'WrongCondition: (self.data.i * 5) > 3')
    sp.verify(self.data.i >= 3, 'WrongCondition: self.data.i >= 3')
    sp.verify(3 < self.data.i, 'WrongCondition: self.data.i > 3')
    sp.verify(self.data.i >= 3, 'WrongCondition: self.data.i >= 3')
    sp.verify(3000 > self.data.i, 'WrongCondition: self.data.i < 3000')
    sp.verify(self.data.i <= 3000, 'WrongCondition: self.data.i <= 3000')
    sp.verify(self.data.b, 'WrongCondition: self.data.b & True')
    x178 = sp.bind_block("x178"):
    with x178:
      sp.if 3 < self.data.i:
        sp.verify(4 < self.data.i, 'WrongCondition: self.data.i > 4')
        sp.result((((self.data.aaa, (self.data.abc, self.data.abca)), ((self.data.acb, self.data.b), (self.data.ddd, self.data.f))), ((self.data.h, (self.data.i, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto)))))
      sp.else:
        sp.result((((self.data.aaa, (self.data.abc, self.data.abca)), ((self.data.acb, self.data.b), (self.data.ddd, self.data.f))), ((self.data.h, (self.data.i, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto)))))
    x1069 = sp.local("x1069", x178.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x1069)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def iterations(self, params):
    sp.set_type(params, sp.TUnit)
    c182 = sp.local("c182", True)
    y181 = sp.local("y181", 0)
    sp.while c182.value:
      x493 = sp.local("x493", 1 + y181.value)
      c182.value = 5 > x493.value
      y181.value = x493.value
    x535 = sp.local("x535", (((self.data.aaa, (self.data.abc, self.data.abca)), ((self.data.acb, self.data.b), (self.data.ddd, self.data.f))), ((self.data.h, (5, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto)))))
    c224 = sp.local("c224", sp.fst(sp.snd(sp.fst(sp.snd(x535.value)))) <= 42)
    r915 = sp.local("r915", x535.value)
    sp.while c224.value:
      x577 = sp.local("x577", (sp.fst(r915.value), ((sp.fst(sp.fst(sp.snd(r915.value))), (2 + sp.fst(sp.snd(sp.fst(sp.snd(r915.value)))), sp.snd(sp.snd(sp.fst(sp.snd(r915.value)))))), sp.snd(sp.snd(r915.value)))))
      c224.value = sp.fst(sp.snd(sp.fst(sp.snd(x577.value)))) <= 42
      r915.value = x577.value
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., if Le(Compare(Car(Cdr(Car(Cdr(r915)))), 123))
                            then
                              Pair
                                ( Car(r915)
                                , Pair
                                    ( Pair
                                        ( Car(Car(Cdr(r915)))
                                        , Pair
                                            ( Add
                                                ( 12
                                                , Car(Cdr(Car(Cdr(r915))))
                                                )
                                            , Cdr(Cdr(Car(Cdr(r915))))
                                            )
                                        )
                                    , Cdr(Cdr(r915))
                                    )
                                )
                            else
                              Pair
                                ( Car(r915)
                                , Pair
                                    ( Pair
                                        ( Car(Car(Cdr(r915)))
                                        , Pair(5, Cdr(Cdr(Car(Cdr(r915)))))
                                        )
                                    , Cdr(Cdr(r915))
                                    )
                                ))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def localVariable(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = sp.record(aaa = self.data.aaa, abc = self.data.abc, abca = self.data.abca, acb = self.data.acb, b = self.data.b, ddd = self.data.ddd, f = self.data.f, h = self.data.h, i = self.data.i * 2, m = self.data.m, n = self.data.n, pkh = self.data.pkh, s = self.data.s, toto = self.data.toto)

  @sp.entry_point
  def myMessageName4(self, params):
    sp.set_type(params, sp.TUnit)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [r914] =
                              iter [ __storage.m
                                   ; Pair
                                       ( Pair
                                           ( Pair
                                               ( __storage.aaa
                                               , Pair
                                                   ( __storage.abc
                                                   , __storage.abca
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.acb
                                                   , __storage.b
                                                   )
                                               , Pair
                                                   ( __storage.ddd
                                                   , __storage.f
                                                   )
                                               )
                                           )
                                       , Pair
                                           ( Pair
                                               ( __storage.h
                                               , Pair
                                                   ( __storage.i
                                                   , __storage.m
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.n
                                                   , __storage.pkh
                                                   )
                                               , Pair
                                                   ( __storage.s
                                                   , __storage.toto
                                                   )
                                               )
                                           )
                                       )
                                   ]
                              step s393; s395 ->
                                [ Pair
                                    ( Car(s395)
                                    , Pair
                                        ( Pair
                                            ( Car(Car(Cdr(s395)))
                                            , Pair
                                                ( Add
                                                    ( Mul
                                                        ( Car(s393)
                                                        , Cdr(s393)
                                                        )
                                                    , Car(Cdr(Car(Cdr(s395))))
                                                    )
                                                , Cdr(Cdr(Car(Cdr(s395))))
                                                )
                                            )
                                        , Cdr(Cdr(s395))
                                        )
                                    )
                                ]
                            r914)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def myMessageName5(self, params):
    sp.set_type(params, sp.TUnit)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [r913] =
                              iter [ __storage.m
                                   ; Pair
                                       ( Pair
                                           ( Pair
                                               ( __storage.aaa
                                               , Pair
                                                   ( __storage.abc
                                                   , __storage.abca
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.acb
                                                   , __storage.b
                                                   )
                                               , Pair
                                                   ( __storage.ddd
                                                   , __storage.f
                                                   )
                                               )
                                           )
                                       , Pair
                                           ( Pair
                                               ( __storage.h
                                               , Pair
                                                   ( __storage.i
                                                   , __storage.m
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.n
                                                   , __storage.pkh
                                                   )
                                               , Pair
                                                   ( __storage.s
                                                   , __storage.toto
                                                   )
                                               )
                                           )
                                       )
                                   ]
                              step s343; s345 ->
                                [ Pair
                                    ( Car(s345)
                                    , Pair
                                        ( Pair
                                            ( Car(Car(Cdr(s345)))
                                            , Pair
                                                ( Add
                                                    ( Mul(2, Car(s343))
                                                    , Car(Cdr(Car(Cdr(s345))))
                                                    )
                                                , Cdr(Cdr(Car(Cdr(s345))))
                                                )
                                            )
                                        , Cdr(Cdr(s345))
                                        )
                                    )
                                ]
                            r913)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def myMessageName6(self, params):
    sp.set_type(params, sp.TInt)
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [r912] =
          iter [ __storage.m
               ; Pair
                   ( Pair
                       ( Pair
                           ( __storage.aaa
                           , Pair(__storage.abc, __storage.abca)
                           )
                       , Pair
                           ( Pair(__storage.acb, __storage.b)
                           , Pair(__storage.ddd, __storage.f)
                           )
                       )
                   , Pair
                       ( Pair(__storage.h, Pair(__storage.i, __storage.m))
                       , Pair
                           ( Pair(__storage.n, __storage.pkh)
                           , Pair(__storage.s, __storage.toto)
                           )
                       )
                   )
               ]
          step s164; s166 ->
            [ Pair
                ( Car(s166)
                , Pair
                    ( Pair
                        ( Car(Car(Cdr(s166)))
                        , Pair
                            ( Add(Mul(3, Cdr(s164)), Car(Cdr(Car(Cdr(s166)))))
                            , Cdr(Cdr(Car(Cdr(s166))))
                            )
                        )
                    , Cdr(Cdr(s166))
                    )
                )
            ]
        let [s300; s301] =
          if Mem
               ( 12#nat
               , Update(12#nat, True, Update(2#nat, False, Car(Car(Car(r912)))))
               )
          then
            [ l7
            ; Pair
                ( Pair
                    ( Pair
                        ( Update(12#nat, True, Update(2#nat, False, Car(Car(Car(r912)))))
                        , Pair
                            ( Cons(Some_(16), Car(Cdr(Car(Car(r912)))))
                            , Update(0, Some_(Some_(16)), Cdr(Cdr(Car(Car(r912)))))
                            )
                        )
                    , Cdr(Car(r912))
                    )
                , Pair
                    ( Pair
                        ( Car(Car(Cdr(r912)))
                        , Pair
                            ( Car(Cdr(Car(Cdr(r912))))
                            , Update(42, Some_(43), Cdr(Cdr(Car(Cdr(r912)))))
                            )
                        )
                    , Cdr(Cdr(r912))
                    )
                )
            ]
          else
            [ l7
            ; Pair
                ( Pair
                    ( Pair
                        ( Update(12#nat, True, Update(2#nat, False, Car(Car(Car(r912)))))
                        , Pair
                            ( Cons(Some_(16), Car(Cdr(Car(Car(r912)))))
                            , Update(0, Some_(Some_(16)), Cdr(Cdr(Car(Car(r912)))))
                            )
                        )
                    , Cdr(Car(r912))
                    )
                , Cdr(r912)
                )
            ]
        let x1059 =
          match IsNat(Neg(s300)) with
          | None _ -> Failwith(104)
          | Some s319 ->
              Pair
                ( Pair
                    ( Pair
                        ( Update(s319, True, Car(Car(Car(s301))))
                        , Cdr(Car(Car(s301)))
                        )
                    , Cdr(Car(s301))
                    )
                , Cdr(s301)
                )
          end
        Pair(Nil<operation>, record_of_tree(..., x1059))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def someComputations(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TInt))
    sp.verify(self.data.i <= 123, 'WrongCondition: self.data.i <= 123')
    x69 = sp.local("x69", ((self.data.h, (sp.snd(params) + self.data.i, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto))))
    sp.verify(sp.add(-4, 5) == 1, 'WrongCondition: sp.add(-4, 5) == 1')
    sp.verify(sp.add(5, -4) == 1, 'WrongCondition: sp.add(5, -4) == 1')
    sp.verify(sp.mul(-4, 5) == (-20), 'WrongCondition: sp.mul(-4, 5) == (-20)')
    sp.verify(sp.mul(5, -4) == (-20), 'WrongCondition: sp.mul(5, -4) == (-20)')
    x402 = sp.local("x402", (((self.data.aaa, (self.data.abc, self.data.abca)), ((sp.fst(params), self.data.b), (self.data.ddd, self.data.f))), ((sp.fst(sp.fst(x69.value)), (100 - 1, sp.snd(sp.snd(sp.fst(x69.value))))), sp.snd(x69.value))))
    x1057 = sp.local("x1057", x402.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x1057)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
