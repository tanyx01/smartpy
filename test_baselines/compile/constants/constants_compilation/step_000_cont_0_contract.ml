open SmartML

module Contract = struct
  let%entry_point ep params =
    data <- (params constant("c1", t = lambda nat nat)) + constant("c2", t = nat)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ nat]
      ~storage:[%expr nat 0]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())