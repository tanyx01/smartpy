open SmartML

module Contract = struct
  let%entry_point test_pair () =
    data.x <- convert (5, 6);
    verify (data.x = {a = (int 5); b = (int 6)})

  let%entry_point test_record () =
    data.x <- convert (set_type_expr {c = (int 3); d = (int 4)} {c = int; d = int});
    verify (data.x = {a = (int 3); b = (int 4)})

  let%entry_point test_variant () =
    data.y <- convert (set_type_expr (variant C (int 43)) (`C int + `D int));
    verify (data.y = (variant A (int 43)))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = {a = int; b = int}; y = `A int + `B int}]
      ~storage:[%expr
                 {x = {a = int 1; b = int 2};
                  y = A(int 42)}]
      [test_pair; test_record; test_variant]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())