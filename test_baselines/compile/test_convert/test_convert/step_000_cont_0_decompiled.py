import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TPair(sp.TInt, sp.TInt), y = sp.TVariant(Left = sp.TInt, Right = sp.TInt).layout(("Left", "Right"))).layout(("x", "y")))

  @sp.entry_point
  def test_pair(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify((5, 6) == (5, 6), 'WrongCondition: self.data.x == sp.record(a = 5, b = 6)')
    x13 = sp.local("x13", ((5, 6), self.data.y))
    x58 = sp.local("x58", x13.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x58)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def test_record(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify((3, 4) == (3, 4), 'WrongCondition: self.data.x == sp.record(a = 3, b = 4)')
    x29 = sp.local("x29", ((3, 4), self.data.y))
    x56 = sp.local("x56", x29.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x56)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def test_variant(self, params):
    sp.set_type(params, sp.TUnit)
    def ferror(error):
      sp.failwith('[Error: prim1: (Left (None, None, (T0 T_int)))]')
    def ferror(error):
      sp.failwith('[Error: prim1: (Left (None, None, (T0 T_int)))]')
    sp.verify(sp.build_lambda(ferror)(sp.unit) == sp.build_lambda(ferror)(sp.unit), 'WrongCondition: self.data.y == variant('A', 43)')
    x42 = sp.local("x42", (self.data.x, sp.build_lambda(ferror)(sp.unit)))
    x54 = sp.local("x54", x42.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x54)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
