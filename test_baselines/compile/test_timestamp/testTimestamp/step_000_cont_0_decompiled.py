import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(next = sp.TTimestamp, out = sp.TBool).layout(("next", "out")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify((sp.add(sp.now, 12) - sp.now) == 12, 'WrongCondition: (sp.add_seconds(sp.now, 12) - sp.now) == 12')
    sp.verify((sp.now - sp.add(sp.now, 12)) == (-12), 'WrongCondition: (sp.now - sp.add_seconds(sp.now, 12)) == (-12)')
    sp.verify((sp.now - sp.add(sp.now, 12)) == (-12), 'WrongCondition: (sp.now - sp.add_seconds(sp.now, 12)) == (-12)')
    sp.verify(sp.add(sp.now, 500) == sp.timestamp(1500), 'WrongCondition: sp.add(sp.now, 500) == sp.timestamp(1500)')
    sp.verify(sp.add(500, sp.now) == sp.timestamp(1500), 'WrongCondition: sp.add(500, sp.now) == sp.timestamp(1500)')
    self.data.out = sp.now > sp.add(sp.now, 1)
    self.data.next = sp.add(sp.now, 86400)

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
