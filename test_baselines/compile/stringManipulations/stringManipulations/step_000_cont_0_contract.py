import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(b0 = sp.TOption(sp.TBytes), l0 = sp.TNat, l1 = sp.TNat, nat_of_string = sp.TIntOrNat, s0 = sp.TOption(sp.TString), split = sp.TList(sp.TString), string_of_nat = sp.TString).layout((("b0", ("l0", "l1")), (("nat_of_string", "s0"), ("split", "string_of_nat")))))
    self.init(b0 = sp.some(sp.bytes('0xaa')),
              l0 = 0,
              l1 = 0,
              nat_of_string = 0,
              s0 = sp.some('hello'),
              split = [],
              string_of_nat = '')

  @sp.entry_point
  def concatenating(self, params):
    self.data.s0 = sp.some(sp.concat(params.s))
    self.data.b0 = sp.some(sp.concat(sp.list([params.b0, params.b1, sp.concat(params.sb)])))

  @sp.entry_point
  def concatenating2(self, params):
    self.data.s0 = sp.some(params.s1 + params.s2)
    self.data.b0 = sp.some(params.b1 + params.b2)

  @sp.entry_point
  def slicing(self, params):
    self.data.s0 = sp.slice(params.s, 2, 5)
    self.data.b0 = sp.slice(params.b, 1, 2)
    self.data.l0 = sp.len(params.s)
    with self.data.s0.match('Some') as Some:
      self.data.l0 += sp.len(Some)
    self.data.l1 = sp.len(params.b)

  @sp.entry_point
  def test_nat_of_string(self, params):
    res = sp.local("res", 0)
    sp.for idx in sp.range(0, sp.len(params)):
      res.value = (10 * res.value) + {'0' : 0, '1' : 1, '2' : 2, '3' : 3, '4' : 4, '5' : 5, '6' : 6, '7' : 7, '8' : 8, '9' : 9}[sp.slice(params, idx, 1).open_some()]
    self.data.nat_of_string = res.value

  @sp.entry_point
  def test_split(self, params):
    prev_idx = sp.local("prev_idx", 0)
    res = sp.local("res", sp.list([]))
    sp.for idx in sp.range(0, sp.len(params)):
      sp.if sp.slice(params, idx, 1).open_some() == ',':
        res.value.push(sp.slice(params, prev_idx.value, sp.as_nat(idx - prev_idx.value)).open_some())
        prev_idx.value = idx + 1
    sp.if sp.len(params) > 0:
      res.value.push(sp.slice(params, prev_idx.value, sp.as_nat(sp.len(params) - prev_idx.value)).open_some())
    self.data.split = res.value.rev()

  @sp.entry_point
  def test_string_of_nat(self, params):
    x = sp.local("x", params)
    res = sp.local("res", sp.list([]))
    sp.if x.value == 0:
      res.value.push('0')
    sp.while x.value > 0:
      res.value.push({0 : '0', 1 : '1', 2 : '2', 3 : '3', 4 : '4', 5 : '5', 6 : '6', 7 : '7', 8 : '8', 9 : '9'}[x.value % 10])
      x.value //= 10
    self.data.string_of_nat = sp.concat(res.value)

sp.add_compilation_target("test", Contract())