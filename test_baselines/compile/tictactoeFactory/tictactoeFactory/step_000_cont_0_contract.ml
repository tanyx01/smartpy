open SmartML

module Contract = struct
  let%entry_point build params =
    verify ((not data.paused) || (sender = data.admin));
    verify (not (contains params.game data.boards));
    Map.set data.boards params.game {deck = (set_type_expr (Map.make [(0, Map.make [(0, int 0); (1, int 0); (2, int 0)]); (1, Map.make [(0, int 0); (1, int 0); (2, int 0)]); (2, Map.make [(0, int 0); (1, int 0); (2, int 0)])]) (map intOrNat (map intOrNat int))); draw = false; metaData = (Map.make []); nbMoves = 0; nextPlayer = (int 1); player1 = params.player1; player2 = params.player2; winner = (int 0)}

  let%entry_point deleteGame params =
    verify (sender = data.admin);
    Map.delete data.boards params.game

  let%entry_point play params =
    verify (not data.paused);
    verify (((Map.get data.boards params.game).winner = (int 0)) && (not (Map.get data.boards params.game).draw));
    verify ((params.i >= 0) && (params.i < 3));
    verify ((params.j >= 0) && (params.j < 3));
    verify (params.move = (Map.get data.boards params.game).nextPlayer);
    verify ((Map.get (Map.get (Map.get data.boards params.game).deck params.i) params.j) = (int 0));
    if params.move = (int 1) then
      verify (sender = (Map.get data.boards params.game).player1)
    else
      verify (sender = (Map.get data.boards params.game).player2);
    (Map.get data.boards params.game).nextPlayer <- (int 3) - (Map.get data.boards params.game).nextPlayer;
    Map.set (Map.get (Map.get data.boards params.game).deck params.i) params.j params.move;
    (Map.get data.boards params.game).nbMoves <- (Map.get data.boards params.game).nbMoves + 1;
    if (((Map.get (Map.get (Map.get data.boards params.game).deck params.i) 0) <> (int 0)) && ((Map.get (Map.get (Map.get data.boards params.game).deck params.i) 0) = (Map.get (Map.get (Map.get data.boards params.game).deck params.i) 1))) && ((Map.get (Map.get (Map.get data.boards params.game).deck params.i) 0) = (Map.get (Map.get (Map.get data.boards params.game).deck params.i) 2)) then
      (Map.get data.boards params.game).winner <- Map.get (Map.get (Map.get data.boards params.game).deck params.i) 0;
    if (((Map.get (Map.get (Map.get data.boards params.game).deck 0) params.j) <> (int 0)) && ((Map.get (Map.get (Map.get data.boards params.game).deck 0) params.j) = (Map.get (Map.get (Map.get data.boards params.game).deck 1) params.j))) && ((Map.get (Map.get (Map.get data.boards params.game).deck 0) params.j) = (Map.get (Map.get (Map.get data.boards params.game).deck 2) params.j)) then
      (Map.get data.boards params.game).winner <- Map.get (Map.get (Map.get data.boards params.game).deck 0) params.j;
    if (((Map.get (Map.get (Map.get data.boards params.game).deck 0) 0) <> (int 0)) && ((Map.get (Map.get (Map.get data.boards params.game).deck 0) 0) = (Map.get (Map.get (Map.get data.boards params.game).deck 1) 1))) && ((Map.get (Map.get (Map.get data.boards params.game).deck 0) 0) = (Map.get (Map.get (Map.get data.boards params.game).deck 2) 2)) then
      (Map.get data.boards params.game).winner <- Map.get (Map.get (Map.get data.boards params.game).deck 0) 0;
    if (((Map.get (Map.get (Map.get data.boards params.game).deck 0) 2) <> (int 0)) && ((Map.get (Map.get (Map.get data.boards params.game).deck 0) 2) = (Map.get (Map.get (Map.get data.boards params.game).deck 1) 1))) && ((Map.get (Map.get (Map.get data.boards params.game).deck 0) 2) = (Map.get (Map.get (Map.get data.boards params.game).deck 2) 0)) then
      (Map.get data.boards params.game).winner <- Map.get (Map.get (Map.get data.boards params.game).deck 0) 2;
    if ((Map.get data.boards params.game).nbMoves = 9) && ((Map.get data.boards params.game).winner = (int 0)) then
      (Map.get data.boards params.game).draw <- true

  let%entry_point setGameMetaData params =
    verify ((sender = data.admin) || (sender = (Map.get data.boards params.game).player1));
    set_type params.name string;
    set_type params.value string;
    Map.set (Map.get data.boards params.game).metaData params.name params.value

  let%entry_point setMetaData params =
    verify (sender = data.admin);
    set_type params.name string;
    set_type params.value string;
    Map.set data.metaData params.name params.value

  let%entry_point setPause params =
    verify (sender = data.admin);
    data.paused <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admin = address; boards = big_map string {deck = map intOrNat (map intOrNat int); draw = bool; metaData = map string string; nbMoves = intOrNat; nextPlayer = int; player1 = address; player2 = address; winner = int}; metaData = map string string; paused = bool}]
      ~storage:[%expr
                 {admin = address "tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr";
                  boards = Map.make [];
                  metaData = Map.make [];
                  paused = false}]
      [build; deleteGame; play; setGameMetaData; setMetaData; setPause]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())