import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TNat)

  @sp.entry_point
  def add(self, params):
    sp.set_type(params, sp.TPair(sp.TNat, sp.TNat))
    self.data = sp.fst(params) + sp.snd(params)

  @sp.entry_point
  def factorial(self, params):
    sp.set_type(params, sp.TNat)
    x197 = sp.local("x197", 1 + params)
    c11 = sp.local("c11", x197.value > 1)
    y9 = sp.local("y9", 1)
    r279 = sp.local("r279", 1)
    sp.while c11.value:
      x220 = sp.local("x220", 1 + y9.value)
      c11.value = x197.value > x220.value
      y9.value = x220.value
      r279.value *= y9.value
    self.data = r279.value

  @sp.entry_point
  def log2(self, params):
    sp.set_type(params, sp.TNat)
    c31 = sp.local("c31", 1 < params)
    y29 = sp.local("y29", params)
    r292 = sp.local("r292", 0)
    sp.while c31.value:
      with sp.ediv(y29.value, 2).match_cases() as arg:
        with arg.match('None') as _l32:
          sp.failwith(42)
        with arg.match('Some') as s169:
          c31.value = 1 < sp.fst(s169)
          y29.value = sp.fst(s169)
          r292.value += 1

    self.data = r292.value

  @sp.entry_point
  def multiply(self, params):
    sp.set_type(params, sp.TPair(sp.TNat, sp.TNat))
    self.data = sp.fst(params) * sp.snd(params)

  @sp.entry_point
  def square(self, params):
    sp.set_type(params, sp.TNat)
    self.data = params * params

  @sp.entry_point
  def squareRoot(self, params):
    sp.set_type(params, sp.TNat)
    sp.verify(params >= 0, 'WrongCondition: params >= 0')
    c65 = sp.local("c65", (params * params) > params)
    r290 = sp.local("r290", params)
    sp.while c65.value:
      with sp.ediv(params, r290.value).match_cases() as arg:
        with arg.match('None') as _l86:
          sp.failwith(26)
        with arg.match('Some') as s51:
          with sp.ediv(sp.fst(s51) + r290.value, 2).match_cases() as arg:
            with arg.match('None') as _l91:
              sp.failwith(26)
            with arg.match('Some') as s60:
              c65.value = (sp.fst(s60) * sp.fst(s60)) > params
              r290.value = sp.fst(s60)


    s127 = sp.local("s127", sp.fst(sp.eif((r290.value * r290.value) <= params, (params < ((r290.value + 1) * (1 + r290.value)), r290.value), (False, r290.value))))
    s128 = sp.local("s128", sp.snd(sp.eif((r290.value * r290.value) <= params, (params < ((r290.value + 1) * (1 + r290.value)), r290.value), (False, r290.value))))
    sp.verify(s127.value, 'WrongCondition: ((y.value * y.value) <= params) & (params < ((y.value + 1) * (y.value + 1)))')
    x116 = sp.local("x116", s128.value)
    self.data = x116.value

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
