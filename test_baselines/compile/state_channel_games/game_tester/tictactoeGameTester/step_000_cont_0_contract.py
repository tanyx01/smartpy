import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(apply_ = sp.TLambda(sp.TRecord(move_data = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")), move_nb = sp.TNat, player = sp.TInt, state = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt))).layout((("move_data", "move_nb"), ("player", "state"))), sp.TPair(sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), sp.TOption(sp.TBounded(['draw', 'player_1_won', 'player_2_won'], t=sp.TString)))), constants = sp.TRecord(channel_id = sp.TBytes, game_nonce = sp.TString, loser = sp.TMutez, model_id = sp.TBytes, player1 = sp.TRecord(addr = sp.TAddress, pk = sp.TKey).layout(("addr", "pk")), player2 = sp.TRecord(addr = sp.TAddress, pk = sp.TKey).layout(("addr", "pk")), winner = sp.TMutez).layout((("channel_id", ("game_nonce", "loser")), (("model_id", "player1"), ("player2", "winner")))), current = sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TString), player = sp.TInt).layout(("move_nb", ("outcome", "player"))), state = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt))).layout((("apply_", "constants"), ("current", "state"))))
    self.init(apply_ = lambda,
              constants = sp.record(channel_id = sp.bytes('0x01'), game_nonce = '', loser = sp.tez(0), model_id = sp.bytes('0x'), player1 = sp.record(addr = sp.address('tz1_PLAYER1_ADDRESS'), pk = sp.key('PLAYER1_KEY')), player2 = sp.record(addr = sp.address('tz1_PLAYER2_ADDRESS'), pk = sp.key('PLAYER2_KEY')), winner = sp.tez(0)),
              current = sp.record(move_nb = 0, outcome = sp.none, player = 1),
              state = {0 : {0 : 0, 1 : 0, 2 : 0}, 1 : {0 : 0, 1 : 0, 2 : 0}, 2 : {0 : 0, 1 : 0, 2 : 0}})

  @sp.entry_point
  def play(self, params):
    sp.set_type(params.move_nb, sp.TNat)
    sp.set_type(params.move_data, sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))
    sp.if self.data.current.player == 1:
      sp.verify(sp.sender == self.data.constants.player1.addr, 'Game_WrongPlayer')
    sp.else:
      sp.verify(sp.sender == self.data.constants.player2.addr, 'Game_WrongPlayer')
    sp.verify(self.data.current.outcome.is_variant('None'))
    sp.verify(self.data.current.move_nb == params.move_nb)
    compute_game_tester_40 = sp.local("compute_game_tester_40", self.data.apply_(sp.record(move_data = params.move_data, move_nb = self.data.current.move_nb, player = self.data.current.player, state = self.data.state)))
    match_pair_game_tester_50_fst, match_pair_game_tester_50_snd = sp.match_tuple(compute_game_tester_40.value, "match_pair_game_tester_50_fst", "match_pair_game_tester_50_snd")
    sp.set_type(match_pair_game_tester_50_snd, sp.TOption(sp.TBounded(['draw', 'player_1_won', 'player_2_won'], t=sp.TString)))
    with match_pair_game_tester_50_snd.match_cases() as arg:
      with arg.match('Some') as Some:
        self.data.current.outcome = sp.some(sp.unbound(Some))
      with arg.match('None') as None:
        self.data.current.outcome = sp.none

    self.data.current.move_nb += 1
    self.data.current.player = 3 - self.data.current.player
    self.data.state = match_pair_game_tester_50_fst

sp.add_compilation_target("test", Contract())