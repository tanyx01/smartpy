open SmartML

module Contract = struct
  let%entry_point balance_of params =
    verify (not data.paused) ~msg:"FA2_PAUSED";
    set_type params {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}};
    let%mutable responses = (map (fun _x0 -> verify (contains _x0.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
if contains (set_type_expr _x0.owner address, set_type_expr _x0.token_id nat) data.ledger then
  result {request = {owner = (set_type_expr _x0.owner address); token_id = (set_type_expr _x0.token_id nat)}; balance = (Map.get data.ledger (set_type_expr _x0.owner address, set_type_expr _x0.token_id nat)).balance}
else
  result {request = {owner = (set_type_expr _x0.owner address); token_id = (set_type_expr _x0.token_id nat)}; balance = (nat 0)}) params.requests) in ();
    transfer responses (tez 0) (set_type_expr params.callback (contract (list {balance = nat; request = {owner = address; token_id = nat}})))

  let%entry_point burn params =
    verify (contains params.token_id data.token_permissions) ~msg:("Ledger_token_unpermitted", params.token_id);
    let%mutable compute_ledger_177 = (Map.get data.token_permissions params.token_id) in ();
    verify (contains params.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
    verify (contains "burn_permissions" compute_ledger_177) ~msg:("Ledger_No_burn_permissions", params.token_id);
    let%mutable compute_ledger_185 = (open_some ~message:"Ledger_unpack_err" (unpack (Map.get compute_ledger_177 "burn_permissions") (`allow_only (set address) + `only_owner unit + `only_owner_and_admin unit))) in ();
    match compute_ledger_185 with
      | `allow_only allow_only ->
        verify (contains sender allow_only) ~msg:("Ledger_not_allowed", params.token_id)
      | `only_owner only_owner ->
        verify (sender = params.address) ~msg:"Ledger_onlyOwner"
      | `only_owner_and_admin only_owner_and_admin ->
        verify ((sender = params.address) || (data.administrator = sender)) ~msg:"Ledger_onlyOwner"
;
    verify (contains "type" compute_ledger_177) ~msg:("Ledger_No_Type", params.token_id);
    let%mutable compute_ledger_196 = (open_some (unpack (Map.get compute_ledger_177 "type") string)) in ();
    verify (compute_ledger_196 = "NATIVE") ~msg:"Ledger_Can_Only_Burn_Native";
    let%mutable supply = (open_some (unpack (Map.get compute_ledger_177 "supply") nat)) in ();
    if contains "supply" compute_ledger_177 then
      supply <- open_some (is_nat (supply - params.amount));
    Map.set (Map.get data.token_permissions params.token_id) "supply" (pack supply);
    (Map.get data.ledger (set_type_expr sender address, set_type_expr params.token_id nat)).balance <- open_some (is_nat ((Map.get data.ledger (set_type_expr sender address, set_type_expr params.token_id nat)).balance - params.amount))

  let%entry_point mint params =
    verify (contains params.token_id data.token_permissions) ~msg:("Ledger_token_unpermitted", params.token_id);
    let%mutable compute_ledger_145 = (Map.get data.token_permissions params.token_id) in ();
    verify (contains params.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
    verify (contains "mint_permissions" compute_ledger_145) ~msg:("Ledger_No_mint_permissions", params.token_id);
    let%mutable compute_ledger_153 = (open_some ~message:"Ledger_unpack_err" (unpack (Map.get compute_ledger_145 "mint_permissions") (`allow_everyone unit + `allow_only (set address)))) in ();
    match compute_ledger_153 with
      | `allow_only allow_only ->
        verify (contains sender allow_only) ~msg:("Ledger_not_allowed", params.token_id)
      | `allow_everyone allow_everyone ->
        ()
;
    if contains "mint_cost" compute_ledger_145 then
      (
        let%mutable compute_ledger_162 = (open_some ~message:"Ledger_unpack_err" (unpack (Map.get compute_ledger_145 "mint_cost") mutez)) in ();
        let%mutable compute_ledger_163 = (mul params.amount compute_ledger_162) in ();
        verify (amount = compute_ledger_163) ~msg:("FA2_WRONG_AMOUNT_expected", compute_ledger_163)
      );
    verify (contains "type" compute_ledger_145) ~msg:("Ledger_No_Type", params.token_id);
    let%mutable compute_ledger_170 = (open_some (unpack (Map.get compute_ledger_145 "type") string)) in ();
    verify (compute_ledger_170 = "NATIVE") ~msg:"Ledger_Can_Only_Mint_NATIVE";
    let%mutable supply = params.amount in ();
    if contains "supply" compute_ledger_145 then
      supply <- supply + (open_some (unpack (Map.get compute_ledger_145 "supply") nat));
    Map.set (Map.get data.token_permissions params.token_id) "supply" (pack supply);
    if contains (set_type_expr params.address address, set_type_expr params.token_id nat) data.ledger then
      (Map.get data.ledger (set_type_expr params.address address, set_type_expr params.token_id nat)).balance <- (Map.get data.ledger (set_type_expr params.address address, set_type_expr params.token_id nat)).balance + params.amount
    else
      Map.set data.ledger (set_type_expr params.address address, set_type_expr params.token_id nat) {balance = params.amount}

  let%entry_point on_received_bonds params =
    set_type params {amount = nat; data = bytes; receiver = address; sender = address; token_id = nat};
    transfer {amount = params.amount; data = params.data; receiver = params.receiver; sender = params.sender; token_id = (Map.get (Map.get data.token_ids sender) params.token_id)} (tez 0) (open_some ~message:"Ledger_Platform_Unreacheable" (contract {amount = nat; data = bytes; receiver = address; sender = address; token_id = nat} params.receiver , entry_point='on_received_bonds'))

  let%entry_point push_bonds params =
    set_type params.channel_id bytes;
    set_type params.player_addr address;
    List.iter (fun token ->
      set_type [{from_ = sender; txs = [{amount = token.value; callback = (to_address (open_some (contract {amount = nat; data = bytes; receiver = address; sender = address; token_id = nat} params.platform , entry_point='on_received_bonds'))); data = (pack {channel_id = params.channel_id; player_addr = params.player_addr}); to_ = params.platform; token_id = token.key}]}] (list {from_ = address; txs = list {amount = nat; callback = address; data = bytes; to_ = address; token_id = nat}});
      List.iter (fun batchs ->
        List.iter (fun transaction ->
          set_type [{from_ = batchs.from_; txs = [{to_ = transaction.to_; token_id = transaction.token_id; amount = transaction.amount}]}] (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}});
          verify (not data.paused) ~msg:"FA2_PAUSED";
          List.iter (fun transfer ->
            List.iter (fun tx ->
              set_type data.token_permissions (big_map nat (map string bytes));
              verify (contains tx.token_id data.token_permissions) ~msg:("Ledger_token_unpermitted", tx.token_id);
              let%mutable compute_ledger_204 = (Map.get data.token_permissions tx.token_id) in ();
              if contains "transfer_only_to" compute_ledger_204 then
                (
                  let%mutable compute_ledger_207 = (open_some ~message:"Ledger_unpack_err" (unpack (Map.get compute_ledger_204 "transfer_only_to") (set address))) in ();
                  verify (contains tx.to_ compute_ledger_207) ~msg:("transfer_only_to", compute_ledger_207)
                );
              if contains "transfer_only_from" compute_ledger_204 then
                (
                  let%mutable compute_ledger_210 = (open_some ~message:"Ledger_unpack_err" (unpack (Map.get compute_ledger_204 "transfer_only_from") (set address))) in ();
                  verify (contains transfer.from_ compute_ledger_210) ~msg:("transfer_only_from", compute_ledger_210)
                );
              verify (contains "type" compute_ledger_204) ~msg:("Ledger_No_Type", tx.token_id);
              let%mutable compute_ledger_271 = (open_some (unpack (Map.get compute_ledger_204 "type") string)) in ();
              if compute_ledger_271 = "FA2" then
                (
                  verify (transfer.from_ = sender) ~msg:"Ledger_Current_From_And_Sender_Differs";
                  match_pair_ledger_220_fst, match_pair_ledger_220_snd = match_tuple(open_some (unpack (Map.get compute_ledger_204 "fa_token") (pair address nat)), "match_pair_ledger_220_fst", "match_pair_ledger_220_snd")
                  transfer [{from_ = sender; txs = [{to_ = tx.to_; token_id = match_pair_ledger_220_snd; amount = tx.amount}]}] (tez 0) (open_some ~message:"Ledger_FA2_Unreacheable" (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) match_pair_ledger_220_fst , entry_point='transfer'))
                )
              else
                (
                  verify (compute_ledger_271 = "NATIVE") ~msg:("Ledger_Type_Unknown", compute_ledger_271);
                  verify (((sender = data.administrator) || (transfer.from_ = sender)) || (contains (set_type_expr {owner = transfer.from_; operator = sender; token_id = tx.token_id} {operator = address; owner = address; token_id = nat}) data.operators)) ~msg:"FA2_NOT_OPERATOR";
                  verify (contains tx.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
                  if tx.amount > (nat 0) then
                    (
                      let%mutable compute_ledger_252 = (Map.get ~default_value:{balance = (nat 0)} data.ledger (set_type_expr transfer.from_ address, set_type_expr tx.token_id nat)) in ();
                      verify (compute_ledger_252.balance >= tx.amount) ~msg:"FA2_INSUFFICIENT_BALANCE";
                      (Map.get data.ledger (set_type_expr transfer.from_ address, set_type_expr tx.token_id nat)).balance <- open_some (is_nat (compute_ledger_252.balance - tx.amount));
                      if contains (set_type_expr tx.to_ address, set_type_expr tx.token_id nat) data.ledger then
                        (Map.get data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat)).balance <- (Map.get data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat)).balance + tx.amount
                      else
                        Map.set data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat) {balance = tx.amount}
                    )
                )
            ) transfer.txs
          ) [{from_ = batchs.from_; txs = [{amount = transaction.amount; to_ = transaction.to_; token_id = transaction.token_id}]}];
          transfer {amount = transaction.amount; data = transaction.data; receiver = transaction.to_; sender = batchs.from_; token_id = transaction.token_id} (tez 0) (open_some ~message:"FA2_WRONG_CALLBACK_INTERFACE" (contract {amount = nat; data = bytes; receiver = address; sender = address; token_id = nat} transaction.callback ))
        ) batchs.txs
      ) [{from_ = sender; txs = [{amount = token.value; callback = (to_address (open_some (contract {amount = nat; data = bytes; receiver = address; sender = address; token_id = nat} params.platform , entry_point='on_received_bonds'))); data = (pack {channel_id = params.channel_id; player_addr = params.player_addr}); to_ = params.platform; token_id = token.key}]}]
    ) (Map.items params.bonds)

  let%entry_point set_administrator params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    data.administrator <- params

  let%entry_point set_metadata params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    Map.set data.metadata params.k params.v

  let%entry_point set_pause params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    data.paused <- params

  let%entry_point set_token_info params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    Map.set data.token_metadata params.token_id {token_id = params.token_id; token_info = params.token_info}

  let%entry_point transfer params =
    set_type params (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}});
    verify (not data.paused) ~msg:"FA2_PAUSED";
    List.iter (fun transfer ->
      List.iter (fun tx ->
        set_type data.token_permissions (big_map nat (map string bytes));
        verify (contains tx.token_id data.token_permissions) ~msg:("Ledger_token_unpermitted", tx.token_id);
        let%mutable compute_ledger_204 = (Map.get data.token_permissions tx.token_id) in ();
        if contains "transfer_only_to" compute_ledger_204 then
          (
            let%mutable compute_ledger_207 = (open_some ~message:"Ledger_unpack_err" (unpack (Map.get compute_ledger_204 "transfer_only_to") (set address))) in ();
            verify (contains tx.to_ compute_ledger_207) ~msg:("transfer_only_to", compute_ledger_207)
          );
        if contains "transfer_only_from" compute_ledger_204 then
          (
            let%mutable compute_ledger_210 = (open_some ~message:"Ledger_unpack_err" (unpack (Map.get compute_ledger_204 "transfer_only_from") (set address))) in ();
            verify (contains transfer.from_ compute_ledger_210) ~msg:("transfer_only_from", compute_ledger_210)
          );
        verify (contains "type" compute_ledger_204) ~msg:("Ledger_No_Type", tx.token_id);
        let%mutable compute_ledger_271 = (open_some (unpack (Map.get compute_ledger_204 "type") string)) in ();
        if compute_ledger_271 = "FA2" then
          (
            verify (transfer.from_ = sender) ~msg:"Ledger_Current_From_And_Sender_Differs";
            match_pair_ledger_220_fst, match_pair_ledger_220_snd = match_tuple(open_some (unpack (Map.get compute_ledger_204 "fa_token") (pair address nat)), "match_pair_ledger_220_fst", "match_pair_ledger_220_snd")
            transfer [{from_ = sender; txs = [{to_ = tx.to_; token_id = match_pair_ledger_220_snd; amount = tx.amount}]}] (tez 0) (open_some ~message:"Ledger_FA2_Unreacheable" (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) match_pair_ledger_220_fst , entry_point='transfer'))
          )
        else
          (
            verify (compute_ledger_271 = "NATIVE") ~msg:("Ledger_Type_Unknown", compute_ledger_271);
            verify (((sender = data.administrator) || (transfer.from_ = sender)) || (contains (set_type_expr {owner = transfer.from_; operator = sender; token_id = tx.token_id} {operator = address; owner = address; token_id = nat}) data.operators)) ~msg:"FA2_NOT_OPERATOR";
            verify (contains tx.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
            if tx.amount > (nat 0) then
              (
                let%mutable compute_ledger_252 = (Map.get ~default_value:{balance = (nat 0)} data.ledger (set_type_expr transfer.from_ address, set_type_expr tx.token_id nat)) in ();
                verify (compute_ledger_252.balance >= tx.amount) ~msg:"FA2_INSUFFICIENT_BALANCE";
                (Map.get data.ledger (set_type_expr transfer.from_ address, set_type_expr tx.token_id nat)).balance <- open_some (is_nat (compute_ledger_252.balance - tx.amount));
                if contains (set_type_expr tx.to_ address, set_type_expr tx.token_id nat) data.ledger then
                  (Map.get data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat)).balance <- (Map.get data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat)).balance + tx.amount
                else
                  Map.set data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat) {balance = tx.amount}
              )
          )
      ) transfer.txs
    ) params

  let%entry_point transfer_and_call params =
    set_type params (list {from_ = address; txs = list {amount = nat; callback = address; data = bytes; to_ = address; token_id = nat}});
    List.iter (fun batchs ->
      List.iter (fun transaction ->
        set_type [{from_ = batchs.from_; txs = [{to_ = transaction.to_; token_id = transaction.token_id; amount = transaction.amount}]}] (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}});
        verify (not data.paused) ~msg:"FA2_PAUSED";
        List.iter (fun transfer ->
          List.iter (fun tx ->
            set_type data.token_permissions (big_map nat (map string bytes));
            verify (contains tx.token_id data.token_permissions) ~msg:("Ledger_token_unpermitted", tx.token_id);
            let%mutable compute_ledger_204 = (Map.get data.token_permissions tx.token_id) in ();
            if contains "transfer_only_to" compute_ledger_204 then
              (
                let%mutable compute_ledger_207 = (open_some ~message:"Ledger_unpack_err" (unpack (Map.get compute_ledger_204 "transfer_only_to") (set address))) in ();
                verify (contains tx.to_ compute_ledger_207) ~msg:("transfer_only_to", compute_ledger_207)
              );
            if contains "transfer_only_from" compute_ledger_204 then
              (
                let%mutable compute_ledger_210 = (open_some ~message:"Ledger_unpack_err" (unpack (Map.get compute_ledger_204 "transfer_only_from") (set address))) in ();
                verify (contains transfer.from_ compute_ledger_210) ~msg:("transfer_only_from", compute_ledger_210)
              );
            verify (contains "type" compute_ledger_204) ~msg:("Ledger_No_Type", tx.token_id);
            let%mutable compute_ledger_271 = (open_some (unpack (Map.get compute_ledger_204 "type") string)) in ();
            if compute_ledger_271 = "FA2" then
              (
                verify (transfer.from_ = sender) ~msg:"Ledger_Current_From_And_Sender_Differs";
                match_pair_ledger_220_fst, match_pair_ledger_220_snd = match_tuple(open_some (unpack (Map.get compute_ledger_204 "fa_token") (pair address nat)), "match_pair_ledger_220_fst", "match_pair_ledger_220_snd")
                transfer [{from_ = sender; txs = [{to_ = tx.to_; token_id = match_pair_ledger_220_snd; amount = tx.amount}]}] (tez 0) (open_some ~message:"Ledger_FA2_Unreacheable" (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) match_pair_ledger_220_fst , entry_point='transfer'))
              )
            else
              (
                verify (compute_ledger_271 = "NATIVE") ~msg:("Ledger_Type_Unknown", compute_ledger_271);
                verify (((sender = data.administrator) || (transfer.from_ = sender)) || (contains (set_type_expr {owner = transfer.from_; operator = sender; token_id = tx.token_id} {operator = address; owner = address; token_id = nat}) data.operators)) ~msg:"FA2_NOT_OPERATOR";
                verify (contains tx.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
                if tx.amount > (nat 0) then
                  (
                    let%mutable compute_ledger_252 = (Map.get ~default_value:{balance = (nat 0)} data.ledger (set_type_expr transfer.from_ address, set_type_expr tx.token_id nat)) in ();
                    verify (compute_ledger_252.balance >= tx.amount) ~msg:"FA2_INSUFFICIENT_BALANCE";
                    (Map.get data.ledger (set_type_expr transfer.from_ address, set_type_expr tx.token_id nat)).balance <- open_some (is_nat (compute_ledger_252.balance - tx.amount));
                    if contains (set_type_expr tx.to_ address, set_type_expr tx.token_id nat) data.ledger then
                      (Map.get data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat)).balance <- (Map.get data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat)).balance + tx.amount
                    else
                      Map.set data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat) {balance = tx.amount}
                  )
              )
          ) transfer.txs
        ) [{from_ = batchs.from_; txs = [{amount = transaction.amount; to_ = transaction.to_; token_id = transaction.token_id}]}];
        transfer {amount = transaction.amount; data = transaction.data; receiver = transaction.to_; sender = batchs.from_; token_id = transaction.token_id} (tez 0) (open_some ~message:"FA2_WRONG_CALLBACK_INTERFACE" (contract {amount = nat; data = bytes; receiver = address; sender = address; token_id = nat} transaction.callback ))
      ) batchs.txs
    ) params

  let%entry_point update_operators params =
    set_type params (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat}));
    List.iter (fun update ->
      match update with
        | `add_operator add_operator ->
          verify ((add_operator.owner = sender) || (sender = data.administrator)) ~msg:"FA2_NOT_ADMIN_OR_OPERATOR";
          Map.set data.operators (set_type_expr {owner = add_operator.owner; operator = add_operator.operator; token_id = add_operator.token_id} {operator = address; owner = address; token_id = nat}) ()
        | `remove_operator remove_operator ->
          verify ((remove_operator.owner = sender) || (sender = data.administrator)) ~msg:"FA2_NOT_ADMIN_OR_OPERATOR";
          Map.delete data.operators (set_type_expr {owner = remove_operator.owner; operator = remove_operator.operator; token_id = remove_operator.token_id} {operator = address; owner = address; token_id = nat})

    ) params

  let%entry_point update_token_permissions params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    let%mutable new_permissions = (Map.get ~default_value:(Map.make []) data.token_permissions params.token_id) in ();
    List.iter (fun permission ->
      if is_some permission.value then
        (
          Map.set new_permissions permission.key (open_some permission.value);
          if permission.key = "fa_token" then
            match_pair_ledger_407_fst, match_pair_ledger_407_snd = match_tuple(open_some (unpack (open_some permission.value) (pair address nat)), "match_pair_ledger_407_fst", "match_pair_ledger_407_snd")
            if not (contains match_pair_ledger_407_fst data.token_ids) then
              Map.set data.token_ids match_pair_ledger_407_fst (Map.make [(match_pair_ledger_407_snd, params.token_id)])
            else
              Map.set (Map.get data.token_ids match_pair_ledger_407_fst) match_pair_ledger_407_snd params.token_id
        )
      else
        (
          if permission.key = "fa_token" then
            match_pair_ledger_402_fst, match_pair_ledger_402_snd = match_tuple(open_some (unpack (open_some permission.value) (pair address nat)), "match_pair_ledger_402_fst", "match_pair_ledger_402_snd")
            Map.delete (Map.get data.token_ids match_pair_ledger_402_fst) match_pair_ledger_402_snd;
          Map.delete new_permissions permission.key
        )
    ) (Map.items params.token_permissions);
    if (len new_permissions) = (nat 0) then
      Map.delete data.token_permissions params.token_id
    else
      Map.set data.token_permissions params.token_id new_permissions

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {administrator = address; all_tokens = set nat; ledger = big_map (pair address nat) {balance = nat}; metadata = big_map string bytes; operators = big_map {operator = address; owner = address; token_id = nat} unit; paused = bool; token_ids = big_map address (map nat nat); token_metadata = big_map nat {token_id = nat; token_info = map string bytes}; token_permissions = big_map nat (map string bytes); total_supply = big_map nat nat}]
      ~storage:[%expr
                 {administrator = address "tz1_ADMIN_LEDGER";
                  all_tokens = Set.make([]);
                  ledger = Map.make [];
                  metadata = Map.make [("", bytes "0x68747470733a2f2f6578616d706c652e636f6d")];
                  operators = Map.make [];
                  paused = false;
                  token_ids = Map.make [];
                  token_metadata = Map.make [(nat 0, {token_id = nat 0; token_info = Map.make [("decimals", bytes "0x36"); ("name", bytes "0x577261707065642058545a"); ("symbol", bytes "0x5758545a"); ("thumbnailUri", bytes "0x68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f332f33332f54657a6f735f6c6f676f2e737667")]})];
                  token_permissions = Map.make [(nat 0, Map.make [("mint_cost", bytes "0x050001"); ("mint_permissions", bytes "0x050505030b"); ("type", bytes "0x0501000000064e4154495645")])];
                  total_supply = Map.make []}]
      [balance_of; burn; mint; on_received_bonds; push_bonds; set_administrator; set_metadata; set_pause; set_token_info; transfer; transfer_and_call; update_operators; update_token_permissions]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())