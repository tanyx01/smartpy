import smartpy as sp

tstorage = sp.TRecord(a = sp.TInt, b = sp.TBool).layout(("a", "b"))
tparameter = sp.TVariant(f = sp.TInt).layout("f")
tprivates = { }
tviews = { }
