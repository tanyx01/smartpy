import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(claimed = sp.TBool, deck = sp.TMap(sp.TInt, sp.TInt), nextPlayer = sp.TInt, size = sp.TInt, winner = sp.TInt).layout((("claimed", "deck"), ("nextPlayer", ("size", "winner")))))

  @sp.entry_point
  def claim(self, params):
    sp.set_type(params, sp.TInt)
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [r216] =
          iter [ __storage.deck; 0 ]
          step s160; s161 ->
            [ Add(Cdr(s160), s161) ]
        let x238 =
          if Eq(Compare(r216, 0))
          then
            if __storage.claimed
            then
              Failwith("WrongCondition: ~ self.data.claimed")
            else
              let x197 =
                Pair
                  ( Pair(True, __storage.deck)
                  , Pair
                      ( __storage.nextPlayer
                      , Pair(__storage.size, __storage.winner)
                      )
                  )
              let x201 =
                let [x251; x252] = Unpair(2, x197)
                Pair
                  ( x251
                  , let [x255; x256] = Unpair(2, x252)
                    Pair
                      ( x255
                      , let [x257; x258] = Unpair(2, x256)
                        Pair(x257, let _ = x258
                                   Car(Cdr(x197)))
                      )
                  )
              if Eq(Compare(Cdr(Cdr(Cdr(x201))), l3))
              then
                x201
              else
                Failwith("WrongCondition: params.winner == self.data.winner")
          else
            Failwith("WrongCondition: sp.sum(self.data.deck.values()) == 0")
        Pair(Nil<operation>, record_of_tree(..., x238))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def remove(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TInt))
    sp.verify(sp.fst(params) >= 0, 'WrongCondition: params.cell >= 0')
    sp.verify(sp.fst(params) < self.data.size, 'WrongCondition: params.cell < self.data.size')
    sp.verify(sp.snd(params) >= 1, 'WrongCondition: params.k >= 1')
    sp.verify(sp.snd(params) <= 2, 'WrongCondition: params.k <= 2')
    x76 = sp.bind_block("x76"):
    with x76:
      with sp.some(self.data.deck[sp.fst(params)]).match_cases() as arg:
        with arg.match('None') as _l1:
          sp.failwith(22)
        with arg.match('Some') as s77:
          sp.verify(sp.snd(params) <= s77, 'WrongCondition: params.k <= self.data.deck[params.cell]')
          x104 = sp.local("x104", self.data.deck)
          with sp.some(x104.value[sp.fst(params)]).match_cases() as arg:
            with arg.match('None') as _l10:
              sp.failwith(23)
            with arg.match('Some') as s118:
              x145 = sp.local("x145", ((self.data.claimed, sp.update_map(x104.value, sp.fst(params), sp.some(s118 - sp.snd(params)))), (self.data.nextPlayer, (self.data.size, self.data.winner))))
    def ferror(error):
      sp.failwith('[Error: to_cmd: Unpair(2, x145)]')
              sp.failwith(sp.build_lambda(ferror)(sp.unit))


    x236 = sp.local("x236", x76.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x236)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
