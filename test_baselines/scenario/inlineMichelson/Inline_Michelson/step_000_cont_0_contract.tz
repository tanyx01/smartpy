parameter (or (or (or (unit %arith) (unit %concat1)) (or (unit %concat2) (or (unit %lambdas) (unit %overflow_add)))) (or (or (unit %overflow_mul) (unit %prim0)) (or (unit %seq) (or (unit %test_operations) (unit %underflow)))));
storage   (pair (string %s) (int %value));
code
  {
    UNPAIR;     # @parameter : @storage
    IF_LEFT
      {
        IF_LEFT
          {
            IF_LEFT
              {
                DROP;       # @storage
                # == arith ==
                # sp.verify(sp.michelson("ADD")(1, 2) == 3) # @storage
                PUSH int 3; # int : @storage
                PUSH int 2; # int : int : @storage
                PUSH int 1; # int : int : int : @storage
                ADD;        # int : int : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"ADD\")(1, 2) == 3"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
                # sp.verify(sp.michelson("ADD")(1, -2) == (-1)) # @storage
                PUSH int -1; # int : @storage
                PUSH int -2; # int : int : @storage
                PUSH int 1; # int : int : int : @storage
                ADD;        # int : int : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"ADD\")(1, -2) == (-1)"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
                # sp.verify(sp.michelson("SUB")(1, -2) == 3) # @storage
                PUSH int 3; # int : @storage
                PUSH int -2; # int : int : @storage
                PUSH int 1; # int : int : int : @storage
                SUB;        # int : int : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"SUB\")(1, -2) == 3"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
                # sp.verify(sp.michelson("SUB")(1, 2) == (-1)) # @storage
                PUSH int -1; # int : @storage
                PUSH int 2; # int : int : @storage
                PUSH int 1; # int : int : int : @storage
                SUB;        # int : int : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"SUB\")(1, 2) == (-1)"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
              }
              {
                DROP;       # @storage
                # == concat1 ==
                # sp.verify(sp.michelson("CONCAT")(sp.list(['a', 'b', 'c'])) == 'abc') # @storage
                PUSH string "abc"; # string : @storage
                PUSH (list string) {"a"; "b"; "c"}; # list string : string : @storage
                CONCAT;     # string : string : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"CONCAT\")(sp.list(['a', 'b', 'c'])) == 'abc'"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
              }; # @storage
          }
          {
            IF_LEFT
              {
                DROP;       # @storage
                # == concat2 ==
                # sp.verify(sp.michelson("CONCAT")('a', 'b') == 'ab') # @storage
                PUSH string "ab"; # string : @storage
                PUSH string "b"; # string : string : @storage
                PUSH string "a"; # string : string : string : @storage
                CONCAT;     # string : string : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"CONCAT\")('a', 'b') == 'ab'"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
              }
              {
                IF_LEFT
                  {
                    DROP;       # @storage
                    # == lambdas ==
                    # sp.verify(sp.michelson("PUSH (lambda int int) {DUP; ADD}; SWAP; EXEC;")(100) == 200) # @storage
                    PUSH int 200; # int : @storage
                    LAMBDA
                      int
                      int
                      {
                        DUP;        # int : int
                        ADD;        # int
                      }; # lambda int int : int : @storage
                    PUSH int 100; # int : lambda int int : int : @storage
                    EXEC;       # int : int : @storage
                    COMPARE;    # int : @storage
                    EQ;         # bool : @storage
                    IF
                      {}
                      {
                        PUSH string "WrongCondition: sp.michelson(\"PUSH (lambda int int) {DUP; ADD}; SWAP; EXEC;\")(100) == 200"; # string : @storage
                        FAILWITH;   # FAILED
                      }; # @storage
                    # sp.verify(sp.michelson("PAIR; PUSH (lambda (pair int int) int) { UNPAIR; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(2, 5) == 25) # @storage
                    PUSH int 25; # int : @storage
                    LAMBDA
                      (pair int int)
                      int
                      {
                        UNPAIR;     # int : int
                        PUSH int 10; # int : int : int
                        MUL;        # int : int
                        ADD;        # int
                      }; # lambda (pair int int) int : int : @storage
                    PUSH (pair int int) (Pair 2 5); # pair int int : lambda (pair int int) int : int : @storage
                    EXEC;       # int : int : @storage
                    COMPARE;    # int : @storage
                    EQ;         # bool : @storage
                    IF
                      {}
                      {
                        PUSH string "WrongCondition: sp.michelson(\"PAIR; PUSH (lambda (pair int int) int) { UNPAIR; PUSH int 10; MUL; ADD;}; SWAP; EXEC;\")(2, 5) == 25"; # string : @storage
                        FAILWITH;   # FAILED
                      }; # @storage
                    # sp.verify(sp.michelson("PAIR 3; PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(2, 5, 7) == 257) # @storage
                    PUSH int 257; # int : @storage
                    PUSH int 7; # int : int : @storage
                    PUSH int 5; # int : int : int : @storage
                    PUSH int 2; # int : int : int : int : @storage
                    PAIR 3;     # pair int (pair int int) : int : @storage
                    LAMBDA
                      (pair int (pair int int))
                      int
                      {
                        UNPAIR 3;   # int : int : int
                        PUSH int 10; # int : int : int : int
                        MUL;        # int : int : int
                        ADD;        # int : int
                        PUSH int 10; # int : int : int
                        MUL;        # int : int
                        ADD;        # int
                      }; # lambda (pair int (pair int int)) int : pair int (pair int int) : int : @storage
                    SWAP;       # pair int (pair int int) : lambda (pair int (pair int int)) int : int : @storage
                    EXEC;       # int : int : @storage
                    COMPARE;    # int : @storage
                    EQ;         # bool : @storage
                    IF
                      {}
                      {
                        PUSH string "WrongCondition: sp.michelson(\"PAIR 3; PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; SWAP; EXEC;\")(2, 5, 7) == 257"; # string : @storage
                        FAILWITH;   # FAILED
                      }; # @storage
                    # sp.verify(sp.michelson("PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; PUSH int 2; APPLY; PUSH int 5; APPLY; PUSH int 7; EXEC;")() == 257) # @storage
                    PUSH int 257; # int : @storage
                    LAMBDA
                      (pair int (pair int int))
                      int
                      {
                        UNPAIR 3;   # int : int : int
                        PUSH int 10; # int : int : int : int
                        MUL;        # int : int : int
                        ADD;        # int : int
                        PUSH int 10; # int : int : int
                        MUL;        # int : int
                        ADD;        # int
                      }; # lambda (pair int (pair int int)) int : int : @storage
                    PUSH int 2; # int : lambda (pair int (pair int int)) int : int : @storage
                    APPLY;      # lambda (pair int int) int : int : @storage
                    PUSH int 5; # int : lambda (pair int int) int : int : @storage
                    APPLY;      # lambda int int : int : @storage
                    PUSH int 7; # int : lambda int int : int : @storage
                    EXEC;       # int : int : @storage
                    COMPARE;    # int : @storage
                    EQ;         # bool : @storage
                    IF
                      {}
                      {
                        PUSH string "WrongCondition: sp.michelson(\"PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; PUSH int 2; APPLY; PUSH int 5; APPLY; PUSH int 7; EXEC;\")() == 257"; # string : @storage
                        FAILWITH;   # FAILED
                      }; # @storage
                    # sp.verify(sp.michelson("LAMBDA_REC int int                { DUP; PUSH int 1; COMPARE; GE; IF { DROP 2; PUSH int 1; } { DUP; PUSH int 1; SWAP; SUB; DIG 2; SWAP; EXEC; MUL; }; }; SWAP; EXEC;")(5) == 120) # @storage
                    PUSH int 120; # int : @storage
                    LAMBDA_REC
                      int
                      int
                      {
                        DUP;        # int : int : lambda int int
                        PUSH int 1; # int : int : int : lambda int int
                        COMPARE;    # int : int : lambda int int
                        GE;         # bool : int : lambda int int
                        IF
                          {
                            DROP 2;     # 
                            PUSH int 1; # int
                          }
                          {
                            DUP;        # int : int : lambda int int
                            PUSH int 1; # int : int : int : lambda int int
                            SWAP;       # int : int : int : lambda int int
                            SUB;        # int : int : lambda int int
                            DIG 2;      # lambda int int : int : int
                            SWAP;       # int : lambda int int : int
                            EXEC;       # int : int
                            MUL;        # int
                          }; # int
                      }; # lambda int int : int : @storage
                    PUSH int 5; # int : lambda int int : int : @storage
                    EXEC;       # int : int : @storage
                    COMPARE;    # int : @storage
                    EQ;         # bool : @storage
                    IF
                      {}
                      {
                        PUSH string "WrongCondition: sp.michelson(\"LAMBDA_REC int int                { DUP; PUSH int 1; COMPARE; GE; IF { DROP 2; PUSH int 1; } { DUP; PUSH int 1; SWAP; SUB; DIG 2; SWAP; EXEC; MUL; }; }; SWAP; EXEC;\")(5) == 120"; # string : @storage
                        FAILWITH;   # FAILED
                      }; # @storage
                    # sp.verify(sp.michelson("PUSH (lambda int int) (Lambda_rec { DUP; PUSH int 1; COMPARE; GE; IF { DROP 2; PUSH int 1; } { DUP; PUSH int 1; SWAP; SUB; DIG 2; SWAP; EXEC; MUL; }; }); SWAP; EXEC;")(5) == 120) # @storage
                    PUSH int 120; # int : @storage
                    LAMBDA_REC
                      int
                      int
                      {
                        DUP;        # int : int : lambda int int
                        PUSH int 1; # int : int : int : lambda int int
                        COMPARE;    # int : int : lambda int int
                        GE;         # bool : int : lambda int int
                        IF
                          {
                            DROP 2;     # 
                            PUSH int 1; # int
                          }
                          {
                            DUP;        # int : int : lambda int int
                            PUSH int 1; # int : int : int : lambda int int
                            SWAP;       # int : int : int : lambda int int
                            SUB;        # int : int : lambda int int
                            DIG 2;      # lambda int int : int : int
                            SWAP;       # int : lambda int int : int
                            EXEC;       # int : int
                            MUL;        # int
                          }; # int
                      }; # lambda int int : int : @storage
                    PUSH int 5; # int : lambda int int : int : @storage
                    EXEC;       # int : int : @storage
                    COMPARE;    # int : @storage
                    EQ;         # bool : @storage
                    IF
                      {}
                      {
                        PUSH string "WrongCondition: sp.michelson(\"PUSH (lambda int int) (Lambda_rec { DUP; PUSH int 1; COMPARE; GE; IF { DROP 2; PUSH int 1; } { DUP; PUSH int 1; SWAP; SUB; DIG 2; SWAP; EXEC; MUL; }; }); SWAP; EXEC;\")(5) == 120"; # string : @storage
                        FAILWITH;   # FAILED
                      }; # @storage
                  }
                  {
                    DROP;       # @storage
                    # == overflow_add ==
                    # compute_inlineMichelson_36 = sp.local("compute_inlineMichelson_36", sp.michelson("ADD")(sp.mutez(9223372036854775807), sp.mutez(1))) # @storage
                    PUSH mutez 1; # mutez : @storage
                    PUSH mutez 9223372036854775807; # mutez : mutez : @storage
                    ADD;        # mutez : @storage
                    DROP;       # @storage
                  }; # @storage
              }; # @storage
          }; # @storage
        NIL operation; # list operation : @storage
      }
      {
        IF_LEFT
          {
            IF_LEFT
              {
                DROP;       # @storage
                # == overflow_mul ==
                # compute_inlineMichelson_41 = sp.local("compute_inlineMichelson_41", sp.michelson("MUL")(sp.mutez(4611686018427387904), 2)) # @storage
                PUSH nat 2; # nat : @storage
                PUSH mutez 4611686018427387904; # mutez : nat : @storage
                MUL;        # mutez : @storage
                DROP;       # @storage
              }
              {
                DROP;       # @storage
                # == prim0 ==
                # sp.verify(sp.michelson("UNIT")() == sp.unit) # @storage
                UNIT;       # unit : @storage
                UNIT;       # unit : unit : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"UNIT\")() == sp.unit"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
                # sp.verify(sp.michelson("NONE unit")() == sp.none) # @storage
                NONE unit;  # option unit : @storage
                NONE unit;  # option unit : option unit : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"NONE unit\")() == sp.none"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
                # sp.verify(sp.michelson("PUSH int 2; PUSH int 1; PAIR")() == (1, 2)) # @storage
                PUSH (pair int int) (Pair 1 2); # pair int int : @storage
                DUP;        # pair int int : pair int int : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"PUSH int 2; PUSH int 1; PAIR\")() == (1, 2)"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
                # sp.verify(sp.len(sp.michelson("NIL unit")()) == 0) # @storage
                PUSH nat 0; # nat : @storage
                NIL unit;   # list unit : nat : @storage
                SIZE;       # nat : nat : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.len(sp.michelson(\"NIL unit\")()) == 0"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
                # sp.verify(sp.len(sp.michelson("EMPTY_SET unit")()) == 0) # @storage
                PUSH nat 0; # nat : @storage
                EMPTY_SET unit; # set unit : nat : @storage
                SIZE;       # nat : nat : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.len(sp.michelson(\"EMPTY_SET unit\")()) == 0"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
                # sp.verify(sp.len(sp.michelson("EMPTY_MAP unit unit")()) == 0) # @storage
                PUSH nat 0; # nat : @storage
                EMPTY_MAP unit unit; # map unit unit : nat : @storage
                SIZE;       # nat : nat : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.len(sp.michelson(\"EMPTY_MAP unit unit\")()) == 0"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
              }; # @storage
            NIL operation; # list operation : @storage
          }
          {
            IF_LEFT
              {
                DROP;       # @storage
                # == seq ==
                # sp.verify(sp.michelson("DIP {SWAP}; ADD; MUL; DUP; MUL;")(15, 16, 17) == 262144) # @storage
                PUSH int 262144; # int : @storage
                PUSH int 16; # int : int : @storage
                PUSH int 17; # int : int : int : @storage
                PUSH int 15; # int : int : int : int : @storage
                ADD;        # int : int : int : @storage
                MUL;        # int : int : @storage
                DUP;        # int : int : int : @storage
                MUL;        # int : int : @storage
                COMPARE;    # int : @storage
                EQ;         # bool : @storage
                IF
                  {}
                  {
                    PUSH string "WrongCondition: sp.michelson(\"DIP {SWAP}; ADD; MUL; DUP; MUL;\")(15, 16, 17) == 262144"; # string : @storage
                    FAILWITH;   # FAILED
                  }; # @storage
                NIL operation; # list operation : @storage
              }
              {
                IF_LEFT
                  {
                    # == test_operations ==
                    # for op in sp.list([sp.michelson("NONE key_hash; SET_DELEGATE;")()]): ... # @parameter%test_operations : @storage
                    NIL operation; # list operation : @parameter%test_operations : @storage
                    NIL operation; # list operation : list operation : @parameter%test_operations : @storage
                    NONE key_hash; # option key_hash : list operation : list operation : @parameter%test_operations : @storage
                    SET_DELEGATE; # operation : list operation : list operation : @parameter%test_operations : @storage
                    CONS;       # list operation : list operation : @parameter%test_operations : @storage
                    ITER
                      {
                        # sp.operations().push(op) # operation : list operation : @parameter%test_operations : @storage
                        CONS;       # list operation : @parameter%test_operations : @storage
                      }; # list operation : @parameter%test_operations : @storage
                    # match_pair_inlineMichelson_95_fst, match_pair_inlineMichelson_95_snd = sp.match_tuple(sp.michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")(), "match_pair_inlineMichelson_95_fst", "match_pair_inlineMichelson_95_snd") # list operation : @parameter%test_operations : @storage
                    PUSH int 123; # int : list operation : @parameter%test_operations : @storage
                    PUSH mutez 0; # mutez : int : list operation : @parameter%test_operations : @storage
                    NONE key_hash; # option key_hash : mutez : int : list operation : @parameter%test_operations : @storage
                    CREATE_CONTRACT
                     { parameter int;
                       storage   int;
                       code
                         {
                           UNPAIR;     # @parameter : @storage
                           ADD;        # int
                           NIL operation; # list operation : int
                           PAIR;       # pair (list operation) int
                         };
                     }; # operation : address : list operation : @parameter%test_operations : @storage
                    # for op in sp.list([sp.fst(sp.michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")())]): ... # operation : address : list operation : @parameter%test_operations : @storage
                    NIL operation; # list operation : operation : address : list operation : @parameter%test_operations : @storage
                    PUSH int 123; # int : list operation : operation : address : list operation : @parameter%test_operations : @storage
                    PUSH mutez 0; # mutez : int : list operation : operation : address : list operation : @parameter%test_operations : @storage
                    NONE key_hash; # option key_hash : mutez : int : list operation : operation : address : list operation : @parameter%test_operations : @storage
                    CREATE_CONTRACT
                     { parameter int;
                       storage   int;
                       code
                         {
                           UNPAIR;     # @parameter : @storage
                           ADD;        # int
                           NIL operation; # list operation : int
                           PAIR;       # pair (list operation) int
                         };
                     }; # operation : address : list operation : operation : address : list operation : @parameter%test_operations : @storage
                    SWAP;       # address : operation : list operation : operation : address : list operation : @parameter%test_operations : @storage
                    DROP;       # operation : list operation : operation : address : list operation : @parameter%test_operations : @storage
                    CONS;       # list operation : operation : address : list operation : @parameter%test_operations : @storage
                    ITER
                      {
                        # sp.operations().push(op) # operation : operation : address : list operation : @parameter%test_operations : @storage
                        DIG 3;      # list operation : operation : operation : address : @parameter%test_operations : @storage
                        SWAP;       # operation : list operation : operation : address : @parameter%test_operations : @storage
                        CONS;       # list operation : operation : address : @parameter%test_operations : @storage
                        DUG 2;      # operation : address : list operation : @parameter%test_operations : @storage
                      }; # operation : address : list operation : @parameter%test_operations : @storage
                    DROP 2;     # list operation : @parameter%test_operations : @storage
                    SWAP;       # @parameter%test_operations : list operation : @storage
                    DROP;       # list operation : @storage
                  }
                  {
                    DROP;       # @storage
                    # == underflow ==
                    # sp.verify(sp.michelson("SUB_MUTEZ")(sp.tez(0), sp.mutez(1)) == sp.none) # @storage
                    NONE mutez; # option mutez : @storage
                    PUSH mutez 1; # mutez : option mutez : @storage
                    PUSH mutez 0; # mutez : mutez : option mutez : @storage
                    SUB_MUTEZ;  # option mutez : option mutez : @storage
                    COMPARE;    # int : @storage
                    EQ;         # bool : @storage
                    IF
                      {}
                      {
                        PUSH string "WrongCondition: sp.michelson(\"SUB_MUTEZ\")(sp.tez(0), sp.mutez(1)) == sp.none"; # string : @storage
                        FAILWITH;   # FAILED
                      }; # @storage
                    NIL operation; # list operation : @storage
                  }; # list operation : @storage
              }; # list operation : @storage
          }; # list operation : @storage
      }; # list operation : @storage
    NIL operation; # list operation : list operation : @storage
    SWAP;       # list operation : list operation : @storage
    ITER
      {
        CONS;       # list operation : @storage
      }; # list operation : @storage
    PAIR;       # pair (list operation) @storage
  };