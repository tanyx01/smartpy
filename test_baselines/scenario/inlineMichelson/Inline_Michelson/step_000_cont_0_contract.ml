open SmartML

module Contract = struct
  let%entry_point arith () =
    verify (michelson("ADD")(int 1, int 2) = (int 3));
    verify (michelson("ADD")(int 1, int ((-2))) = (int ((-1))));
    verify (michelson("SUB")(int 1, int ((-2))) = (int 3));
    verify (michelson("SUB")(int 1, int 2) = (int ((-1))))

  let%entry_point concat1 () =
    verify (michelson("CONCAT")(["a"; "b"; "c"]) = "abc")

  let%entry_point concat2 () =
    verify (michelson("CONCAT")("a", "b") = "ab")

  let%entry_point lambdas () =
    verify (michelson("PUSH (lambda int int) {DUP; ADD}; SWAP; EXEC;")(int 100) = (int 200));
    verify (michelson("PAIR; PUSH (lambda (pair int int) int) { UNPAIR; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(int 2, int 5) = (int 25));
    verify (michelson("PAIR 3; PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(int 2, int 5, int 7) = (int 257));
    verify (michelson("PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; PUSH int 2; APPLY; PUSH int 5; APPLY; PUSH int 7; EXEC;")() = (int 257));
    verify (michelson("LAMBDA_REC int int                { DUP; PUSH int 1; COMPARE; GE; IF { DROP 2; PUSH int 1; } { DUP; PUSH int 1; SWAP; SUB; DIG 2; SWAP; EXEC; MUL; }; }; SWAP; EXEC;")(int 5) = (int 120));
    verify (michelson("PUSH (lambda int int) (Lambda_rec { DUP; PUSH int 1; COMPARE; GE; IF { DROP 2; PUSH int 1; } { DUP; PUSH int 1; SWAP; SUB; DIG 2; SWAP; EXEC; MUL; }; }); SWAP; EXEC;")(int 5) = (int 120))

  let%entry_point overflow_add () =
    let%mutable compute_inlineMichelson_36 = michelson("ADD")(mutez 9223372036854775807, mutez 1) in ()

  let%entry_point overflow_mul () =
    let%mutable compute_inlineMichelson_41 = michelson("MUL")(mutez 4611686018427387904, nat 2) in ()

  let%entry_point prim0 () =
    verify (michelson("UNIT")() = ());
    verify (michelson("NONE unit")() = None);
    verify (michelson("PUSH int 2; PUSH int 1; PAIR")() = (int 1, int 2));
    verify ((len michelson("NIL unit")()) = (nat 0));
    verify ((len michelson("EMPTY_SET unit")()) = (nat 0));
    verify ((len michelson("EMPTY_MAP unit unit")()) = (nat 0))

  let%entry_point seq () =
    verify (michelson("DIP {SWAP}; ADD; MUL; DUP; MUL;")(int 15, int 16, int 17) = (int 262144))

  let%entry_point test_operations () =
    List.iter (fun op ->
      operations <- op :: operations
    ) [michelson("NONE key_hash; SET_DELEGATE;")()];
    match_pair_inlineMichelson_95_fst, match_pair_inlineMichelson_95_snd = match_tuple(michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")(), "match_pair_inlineMichelson_95_fst", "match_pair_inlineMichelson_95_snd")
    List.iter (fun op ->
      operations <- op :: operations
    ) [fst michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")()]

  let%entry_point underflow () =
    verify (michelson("SUB_MUTEZ")(tez 0, mutez 1) = None)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {s = string; value = intOrNat}]
      ~storage:[%expr
                 {s = "";
                  value = 0}]
      [arith; concat1; concat2; lambdas; overflow_add; overflow_mul; prim0; seq; test_operations; underflow]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())