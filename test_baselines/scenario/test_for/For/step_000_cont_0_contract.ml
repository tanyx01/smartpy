open SmartML

module Contract = struct
  let%entry_point ep1 () =
    data.x <- 0;
    let%mutable s = {a = 5; b = 6} in ();
    List.iter (fun r ->
      data.x <- data.x + r.a
    ) [{a = 2; b = 3}; s];
    verify (data.x = 7)

  let%entry_point ep2 () =
    data.x <- 2;
    List.iter (fun i ->
      data.x <- data.x + i
    ) [data.x; data.x; data.x];
    verify (data.x = 8)

  let%entry_point ep3 () =
    data.rs <- [{a = 2; b = 3}; {a = 2; b = 3}; {a = 2; b = 3}];
    List.iter (fun i ->
      i.a <- 0
    ) data.rs

  let%entry_point ep4 () =
    data.xxs <- set_type_expr (Map.make [(0, Map.make [(0, nat 2); (1, nat 3)]); (1, Map.make [(0, nat 2); (1, nat 3)]); (2, Map.make [(0, nat 2); (1, nat 3)])]) (map intOrNat (map intOrNat nat));
    List.iter (fun xs ->
      Map.set xs 0 (nat 0);
      send address "tz1PiDHTNJXhqpkbRUYNZEzmePNd21WcB8yB" (mul (set_type_expr (Map.get xs 1) nat) (mutez 1))
    ) (Map.values data.xxs)

  let%entry_point ep5 () =
    data.x <- 0;
    data.xxxs <- set_type_expr (Map.make [(0, Map.make [(0, Map.make [(0, 1)])]); (1, Map.make [(0, Map.make [(0, 2)])])]) (map intOrNat (map intOrNat (map intOrNat intOrNat)));
    List.iter (fun xs ->
      data.x <- 1;
      Map.set xs 0 0
    ) (Map.values (Map.get data.xxxs data.x))

  let%entry_point ep6 () =
    data.xxxs <- set_type_expr (Map.make [(0, Map.make [(0, Map.make [(0, 0)]); (1, Map.make [(0, 10)])]); (1, Map.make [(0, Map.make [(0, 20)]); (1, Map.make [(0, 30)])])]) (map intOrNat (map intOrNat (map intOrNat intOrNat)));
    List.iter (fun xs ->
      Map.set xs 0 1
    ) (Map.values (Map.get data.xxxs (Map.get (Map.get (Map.get data.xxxs 0) 0) 0)))

  let%entry_point ep7 () =
    data.ms <- [Map.make [("a", 97); ("b", 98)]; Map.make [("a", 0); ("b", 1)]];
    List.iter (fun m ->
      Map.delete m "a"
    ) data.ms

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {ms = list (map string intOrNat); rs = list {a = intOrNat; b = intOrNat}; x = intOrNat; xxs = map intOrNat (map intOrNat nat); xxxs = map intOrNat (map intOrNat (map intOrNat intOrNat))}]
      ~storage:[%expr
                 {ms = [Map.make [("a", 97); ("b", 98)]; Map.make [("a", 0); ("b", 1)]];
                  rs = [{a = 5; b = 6}];
                  x = 0;
                  xxs = Map.make [(0, Map.make [(0, nat 0)])];
                  xxxs = Map.make [(0, Map.make [(0, Map.make [(0, 0)])])]}]
      [ep1; ep2; ep3; ep4; ep5; ep6; ep7]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())