open SmartML

module Contract = struct
  let%entry_point receive_balances params =
    set_type params (list {balance = nat; request = {owner = address; token_id = nat}});
    data.last_sum <- nat 0;
    List.iter (fun resp ->
      data.last_sum <- data.last_sum + resp.balance
    ) params

  let%entry_point reinit () =
    data.last_sum <- nat 0

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {last_sum = nat; operator_support = bool}]
      ~storage:[%expr
                 {last_sum = nat 0;
                  operator_support = true}]
      [receive_balances; reinit]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())