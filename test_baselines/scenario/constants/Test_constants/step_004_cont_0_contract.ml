open SmartML

module Contract = struct
  let%entry_point ep params =
    data <- (params (constant_var "0")) + (constant_var "1")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ intOrNat]
      ~storage:[%expr 0]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())