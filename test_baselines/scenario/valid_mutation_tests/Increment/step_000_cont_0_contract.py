import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TInt)
    self.init(0)

  @sp.entry_point
  def decrement(self):
    self.data -= 1

  @sp.entry_point
  def increment(self):
    self.data += 1

sp.add_compilation_target("test", Contract())