import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TNat)
    self.init(0)

sp.add_compilation_target("test", Contract())