open SmartML

module Contract = struct
  let%entry_point set_data params =
    failwith ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ intOrNat]
      ~storage:[%expr 0]
      [set_data]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())