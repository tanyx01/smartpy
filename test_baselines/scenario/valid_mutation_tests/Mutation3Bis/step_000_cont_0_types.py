import smartpy as sp

tstorage = sp.TIntOrNat
tparameter = sp.TVariant(set_data = sp.TRecord(a = sp.TBool, b = sp.TBool).layout(("a", "b"))).layout("set_data")
tprivates = { }
tviews = { }
