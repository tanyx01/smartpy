open SmartML

module Contract = struct

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat; z = nat}]
      ~storage:[%expr
                 {x = 10;
                  z = nat 42}]
      []
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())