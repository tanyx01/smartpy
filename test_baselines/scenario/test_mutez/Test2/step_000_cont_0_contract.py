import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TMutez)).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def decrease(self, params):
    self.data.result = sp.some(self.data.result.open_some() - params)

  @sp.entry_point
  def store(self, params):
    self.data.result = sp.some(params)

  @sp.entry_point
  def sub(self, params):
    self.data.result = sp.sub_mutez(params.x, params.y)

sp.add_compilation_target("test", Contract())