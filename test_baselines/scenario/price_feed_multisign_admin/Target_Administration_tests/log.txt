Comment...
 h1: Target Administration tests
Table Of Contents

 Target Administration tests
# Init contracts
## Administrated
## multisignAdmin
# Set multisignAdmin as administrated's admin
# Use multisignadmin to set a target
## Signer 1 new proposal: changeTarget
# Use multisignadmin to administrate target
## Signer 1 new proposal: setActive = True on target
Comment...
 h2: Init contracts
Comment...
 h3: Administrated
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair False (Pair "tz1UyQDepgtUBnWjyzzonqeDwaiWoQzRKSP5" None))
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_005_cont_0_storage.tz 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_005_cont_0_storage.json 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_005_cont_0_sizes.csv 2
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_005_cont_0_storage.py 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_005_cont_0_types.py 7
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_005_cont_0_contract.tz 64
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_005_cont_0_contract.json 79
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_005_cont_0_contract.py 27
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_005_cont_0_contract.ml 34
Comment...
 h3: multisignAdmin
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 -> (Pair {Elt "tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT" 0; Elt "tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR" 1} (Pair {Elt "edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd" 0; Elt "edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe" 1} (Pair "1970-01-01T00:00:00Z" (Pair 1 (Pair {Elt "" 0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d596e55326a7771345855346a775831526d75314547666d79684d687044437050704846356a396a4639793177} (Pair 2 (Pair {} (Pair 1 (Pair None (Pair 5 {Elt 0 (Pair "tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT" (Pair 0 "edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd")); Elt 1 (Pair "tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR" (Pair 0 "edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe"))}))))))))))
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_storage.tz 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_storage.json 104
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_sizes.csv 2
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_storage.py 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_types.py 7
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_metadata.metadata.json 303
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_contract.tz 1332
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_contract.json 2268
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_contract.py 114
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_007_cont_1_contract.ml 70
Comment...
 h2: Set multisignAdmin as administrated's admin
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_009_cont_0_params.py 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_009_cont_0_params.tz 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_009_cont_0_params.json 1
Executing administrate(sp.list([variant('setAdmin', sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'))]))...
 -> (Pair False (Pair "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF" None))
Comment...
 h2: Use multisignadmin to set a target
Comment...
 h3: Signer 1 new proposal: changeTarget
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_012_cont_1_params.py 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_012_cont_1_params.tz 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_012_cont_1_params.json 1
Executing newProposal(sp.list([variant('selfAdmin', sp.list([variant('changeTarget', sp.to_address(sp.contract(sp.TList(sp.TVariant(setActive = sp.TBool, setAdmin = sp.TAddress).layout(("setActive", "setAdmin"))), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'), entry_point='administrate').open_some()))]))]))...
 -> (Pair {Elt "tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT" 0; Elt "tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR" 1} (Pair {Elt "edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd" 0; Elt "edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe" 1} (Pair "1970-01-01T00:00:12Z" (Pair 1 (Pair {Elt "" 0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d596e55326a7771345855346a775831526d75314547666d79684d687044437050704846356a396a4639793177} (Pair 2 (Pair {Elt 0 (Pair {Left {Left (Right "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate")}} (Pair False (Pair 1 (Pair {} (Pair "1970-01-01T00:00:12Z" {0})))))} (Pair 1 (Pair (Some "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate") (Pair 5 {Elt 0 (Pair "tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT" (Pair 1 "edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd")); Elt 1 (Pair "tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR" (Pair 0 "edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe"))}))))))))))
Comment...
 h2: Use multisignadmin to administrate target
Comment...
 h3: Signer 1 new proposal: setActive = True on target
Verifying ~ sp.contract_data(0).active...
 OK
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_016_cont_1_params.py 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_016_cont_1_params.tz 1
 => test_baselines/scenario/price_feed_multisign_admin/Target_Administration_tests/step_016_cont_1_params.json 1
Executing newProposal(sp.list([variant('targetAdmin', sp.list([variant('setActive', True)]))]))...
 -> (Pair {Elt "tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT" 0; Elt "tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR" 1} (Pair {Elt "edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd" 0; Elt "edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe" 1} (Pair "1970-01-01T00:00:24Z" (Pair 1 (Pair {Elt "" 0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d596e55326a7771345855346a775831526d75314547666d79684d687044437050704846356a396a4639793177} (Pair 2 (Pair {Elt 0 (Pair {Right {Left True}} (Pair False (Pair 2 (Pair {} (Pair "1970-01-01T00:00:24Z" {0})))))} (Pair 1 (Pair (Some "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate") (Pair 5 {Elt 0 (Pair "tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT" (Pair 2 "edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd")); Elt 1 (Pair "tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR" (Pair 0 "edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe"))}))))))))))
  + Transfer
     params: [setActive(True)]
     amount: sp.tez(0)
     to:     sp.contract(sp.TList(sp.TVariant(setActive = sp.TBool, setAdmin = sp.TAddress).layout(("setActive", "setAdmin"))), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%administrate')).open_some()
Executing (queue) administrate([setActive(True)])...
 -> (Pair True (Pair "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF" None))
Verifying sp.contract_data(0).active...
 OK
