import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admins = sp.TSet(sp.TAddress)).layout("admins"))
    self.init(admins = sp.set([sp.address('tz1NajGagueXKGFqf9msG66qsAreKnPiFuEb')]))

  @sp.entry_point
  def add_admins(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Only an admin can call this entrypoint.')
    sp.for admin in params:
      self.data.admins.add(admin)

  @sp.entry_point
  def protected(self):
    sp.verify(self.data.admins.contains(sp.sender), 'Only an admin can call this entrypoint.')

  @sp.entry_point
  def remove_admins(self, params):
    sp.failwith(sp.unit)

sp.add_compilation_target("test", Contract())