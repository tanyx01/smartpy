open SmartML

module Contract = struct
  let%entry_point default () =
    ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [default]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())