import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TInt, y = sp.TBool).layout(("x", "y")))
    self.init(x = 1,
              y = True)

  @sp.entry_point
  def bounce(self, params):
    sp.set_type(params, sp.TAddress)

  @sp.entry_point
  def check_bounce(self):
    sp.verify(sp.entrypoint_map().contains(sp.entrypoint_id("bounce")))

  @sp.entry_point
  def modify_x(self, params):
    self.data.x = params

  @sp.entry_point
  def modify_x2(self, params):
    self.data.x = 2 * params

  @sp.entry_point
  def modify_x3(self, params):
    self.data.x = 3 * params

  @sp.entry_point
  def take_ticket(self, params):
    sp.set_type(params, sp.TTicket(sp.TInt))

  @sp.entry_point
  def update_bounce(self, params):
    sp.entrypoint_map()[sp.entrypoint_id("bounce")] = params
    sp.send(sp.sender, sp.tez(1))

  @sp.entry_point
  def update_modify_x(self, params):
    sp.entrypoint_map()[sp.entrypoint_id("modify_x")] = params

sp.add_compilation_target("test", Contract())