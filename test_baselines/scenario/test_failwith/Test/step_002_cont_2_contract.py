import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat).layout("x"))
    self.init(x = 1)

  @sp.entry_point
  def ep1(self):
    sp.failwith('A')
    sp.failwith('B')

  @sp.entry_point
  def ep2(self):
    sp.failwith('A')
    self.data.x = 2

sp.add_compilation_target("test", Contract())