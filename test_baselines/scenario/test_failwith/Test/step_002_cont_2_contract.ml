open SmartML

module Contract = struct
  let%entry_point ep1 () =
    failwith "A";
    failwith "B"

  let%entry_point ep2 () =
    failwith "A";
    data.x <- 2

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat}]
      ~storage:[%expr
                 {x = 1}]
      [ep1; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())