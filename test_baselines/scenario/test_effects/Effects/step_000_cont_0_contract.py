import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TNat).layout("a"))
    self.init(a = 0)

  @sp.entry_point
  def sub_double(self):
    self.data.a = 2 * self.data.a

  @sp.entry_point
  def sub_incr(self):
    self.data.a += 1

sp.add_compilation_target("test", Contract())