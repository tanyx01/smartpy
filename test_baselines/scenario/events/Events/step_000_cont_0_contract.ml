open SmartML

module Contract = struct
  let%entry_point ep () =
    operations <- emit_operation("Hello") :: operations;
    operations <- emit_operation("World", tag = "mytag") :: operations;
    operations <- emit_operation({a = "ABC"; b = "XYZ"}, tag = "mytag2") :: operations;
    operations <- emit_operation({a = "ABC"; b = "XYZ"}, tag = "mytag2", with_type = false) :: operations

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())