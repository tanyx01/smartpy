import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat, z = sp.TIntOrNat).layout(("x", ("y", "z")))
tparameter = sp.TVariant(myEntryPoint = sp.TIntOrNat, myEntryPoint2 = sp.TUnit).layout(("myEntryPoint", "myEntryPoint2"))
tprivates = { }
tviews = { }
