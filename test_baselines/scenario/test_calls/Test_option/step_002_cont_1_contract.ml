open SmartML

module Contract = struct
  let%entry_point default () =
    data.x <- amount

  let%entry_point other () =
    ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = mutez}]
      ~storage:[%expr
                 {x = tez 0}]
      [default; other]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())