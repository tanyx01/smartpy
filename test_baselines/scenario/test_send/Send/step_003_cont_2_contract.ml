open SmartML

module Contract = struct
  let%entry_point ep1 () =
    send address "tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr" (tez 1)

  let%entry_point ep2 () =
    ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep1; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())