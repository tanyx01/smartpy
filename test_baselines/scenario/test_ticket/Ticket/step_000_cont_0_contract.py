import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TOption(sp.TTicket(sp.TInt)), y = sp.TOption(sp.TTicket(sp.TString))).layout(("x", "y")))
    self.init(x = sp.none,
              y = sp.none)

  @sp.entry_point
  def auto_call(self):
    ticket_9 = sp.local("ticket_9", sp.ticket(1, 43))
    sp.transfer(ticket_9.value, sp.tez(0), sp.self_entry_point('run'))

  @sp.entry_point
  def run(self, params):
    sp.set_type(params, sp.TTicket(sp.TInt))
    ticket_test_ticket_14_data, ticket_test_ticket_14_copy = sp.match_tuple(sp.read_ticket_raw(params), "ticket_test_ticket_14_data", "ticket_test_ticket_14_copy")
    ticket_test_ticket_14_ticketer, ticket_test_ticket_14_content, ticket_test_ticket_14_amount = sp.match_tuple(ticket_test_ticket_14_data, "ticket_test_ticket_14_ticketer", "ticket_test_ticket_14_content", "ticket_test_ticket_14_amount")
    ticket_15 = sp.local("ticket_15", sp.ticket('abc', 42))
    self.data.y = sp.some(ticket_15.value)
    ticket1_test_ticket_16, ticket2_test_ticket_16 = sp.match_tuple(sp.split_ticket_raw(ticket_test_ticket_14_copy, (ticket_test_ticket_14_amount // 3, sp.as_nat(ticket_test_ticket_14_amount - (ticket_test_ticket_14_amount // 3)))).open_some(), "ticket1_test_ticket_16", "ticket2_test_ticket_16")
    self.data.x = sp.some(sp.join_tickets_raw((ticket2_test_ticket_16, ticket1_test_ticket_16)).open_some())

  @sp.entry_point
  def run2(self, params):
    sp.set_type(params, sp.TRecord(t = sp.TTicket(sp.TInt), x = sp.TInt).layout(("t", "x")))
    x_test_ticket_22, t_test_ticket_22 = sp.match_record(params, "x", "t")
    sp.verify(x_test_ticket_22 == 42)
    ticket_test_ticket_24_data, ticket_test_ticket_24_copy = sp.match_tuple(sp.read_ticket_raw(t_test_ticket_22), "ticket_test_ticket_24_data", "ticket_test_ticket_24_copy")
    ticket_test_ticket_24_ticketer, ticket_test_ticket_24_content, ticket_test_ticket_24_amount = sp.match_tuple(ticket_test_ticket_24_data, "ticket_test_ticket_24_ticketer", "ticket_test_ticket_24_content", "ticket_test_ticket_24_amount")
    ticket_25 = sp.local("ticket_25", sp.ticket('abc', 42))
    self.data.y = sp.some(ticket_25.value)
    ticket1_test_ticket_26, ticket2_test_ticket_26 = sp.match_tuple(sp.split_ticket_raw(ticket_test_ticket_24_copy, (ticket_test_ticket_24_amount // 3, sp.as_nat(ticket_test_ticket_24_amount - (ticket_test_ticket_24_amount // 3)))).open_some(), "ticket1_test_ticket_26", "ticket2_test_ticket_26")
    self.data.x = sp.some(sp.join_tickets_raw((ticket2_test_ticket_26, ticket1_test_ticket_26)).open_some())

sp.add_compilation_target("test", Contract())