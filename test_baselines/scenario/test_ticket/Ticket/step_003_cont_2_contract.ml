open SmartML

module Contract = struct
  let%entry_point run () =
    with match_record(data, "data") as data:
      ticket_test_ticket_49_data, ticket_test_ticket_49_copy = match_tuple(read_ticket_raw data.t, "ticket_test_ticket_49_data", "ticket_test_ticket_49_copy")
      ticket_test_ticket_49_ticketer, ticket_test_ticket_49_content, ticket_test_ticket_49_amount = match_tuple(ticket_test_ticket_49_data, "ticket_test_ticket_49_ticketer", "ticket_test_ticket_49_content", "ticket_test_ticket_49_amount")
      verify (ticket_test_ticket_49_content = (nat 42));
      ticket1_test_ticket_51, ticket2_test_ticket_51 = match_tuple(open_some (split_ticket_raw ticket_test_ticket_49_copy (ticket_test_ticket_49_amount / (nat 2), ticket_test_ticket_49_amount / (nat 2))), "ticket1_test_ticket_51", "ticket2_test_ticket_51")
      data.t <- open_some (join_tickets_raw (ticket2_test_ticket_51, ticket1_test_ticket_51))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {t = ticket nat; x = int}]
      [run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())