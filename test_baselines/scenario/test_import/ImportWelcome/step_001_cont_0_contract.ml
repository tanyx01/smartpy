open SmartML

module Contract = struct
  let%entry_point myEntryPoint params =
    verify (data.myParameter1 <= 123);
    data.myParameter1 <- data.myParameter1 + params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {myParameter1 = intOrNat; myParameter2 = intOrNat}]
      ~storage:[%expr
                 {myParameter1 = 1;
                  myParameter2 = 1}]
      [myEntryPoint]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())