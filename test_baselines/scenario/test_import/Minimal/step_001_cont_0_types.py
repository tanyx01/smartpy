import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat).layout("x")
tparameter = sp.TVariant(entry_point_1 = sp.TPair(sp.TIntOrNat, sp.TPair(sp.TIntOrNat, sp.TIntOrNat))).layout("entry_point_1")
tprivates = { }
tviews = { }
