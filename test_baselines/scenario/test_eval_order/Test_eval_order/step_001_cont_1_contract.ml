open SmartML

module Contract = struct
  let%entry_point put_int params =
    set_type params int

  let%entry_point test_expr_other () =
    let%mutable compute_test_eval_order_313 = (open_some ~message:(("B", int 1) self.f_int) (("A", some (int 1)) self.f_option)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_315 = (open_some ~message:(("B", int 1) self.f_int) (some 1)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_318 = (Map.get (("A", Map.make [(int 1, int 2)]) self.f_map) (("B", int 1) self.f_int)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_320 = (("A", int 1) self.f_int, ("B", int 1) self.f_int) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_322 = {a = (("A", int 1) self.f_int); b = (("B", int 1) self.f_int)} in ();
    let%mutable compute_test_eval_order_324 = [("A", int 1) self.f_int; ("B", int 1) self.f_int] in ();
    let%mutable compute_test_eval_order_326 = (Set.make [("A", int 1) self.f_int; ("B", int 1) self.f_int]) in ();
    let%mutable compute_test_eval_order_328 = (Map.make [(("A", int 1) self.f_int, ("B", int 1) self.f_int); (("C", int 1) self.f_int, ("D", int 1) self.f_int)]) in ();
    let%mutable compute_test_eval_order_330 = (Map.make [(int 1, ("B", int 1) self.f_int); (("C", int 1) self.f_int, ("D", int 1) self.f_int)]) in ();
    let%mutable compute_test_eval_order_333 = (map (("B", fun _x40 -> result _x40) self.f_lambda) (("A", []) self.f_list)) in ();
    verify (data.x = "A");
    let%mutable create_contract_test_eval_order_337 = create contract ... in ();
    let%mutable compute_test_eval_order_337 = create_contract_test_eval_order_337 in ();
    verify (data.x = "A");
    let%mutable create_contract_test_eval_order_339 = create contract ... in ();
    let%mutable compute_test_eval_order_339 = create_contract_test_eval_order_339 in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_343 = (slice (("A", "abc") self.f_string) (("B", nat 1) self.f_nat) (("C", nat 1) self.f_nat)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_345 = (slice (("A", "abc") self.f_string) (nat 0) (("C", nat 1) self.f_nat)) in ();
    verify (data.x = "C");
    let%mutable compute_test_eval_order_348 = (transfer_operation (("A", int 1) self.f_int) (("B", mutez 1) self.f_mutez) (("C", self_entry_point "put_int") self.f_contract)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_350 = (transfer_operation (int 0) (("B", mutez 1) self.f_mutez) (("C", self_entry_point "put_int") self.f_contract)) in ();
    verify (data.x = "B")

  let%entry_point test_mprim2 () =
    let%mutable compute_test_eval_order_258 = ((("A", nat 1) self.f_nat) << (("B", nat 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_261 = ((("A", nat 1) self.f_nat) >> (("B", nat 1) self.f_nat)) in ();
    verify (data.x = "A")

  let%entry_point test_mprim3 params =
    verify (check_signature (("A", params.key) self.f_key) (("B", params.signature) self.f_signature) (("C", bytes "0x00") self.f_bytes));
    verify (data.x = "A");
    verify (check_signature params.key (("B", params.signature) self.f_signature) (("C", bytes "0x00") self.f_bytes));
    verify (data.x = "B")

  let%entry_point test_prim2 () =
    let%mutable ticket_136 = (ticket 1 (nat 1)) in ();
    let%mutable compute_test_eval_order_144 = (get_opt (("B", int 1) self.f_int) (("A", Map.make []) self.f_map)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_146 = (contains (("B", int 1) self.f_int) (("A", Map.make []) self.f_map)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_148 = ((("B", int 1) self.f_int) (("A", fun _x42 -> result _x42) self.f_lambda)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_150 = (apply_lambda (("B", int 1) self.f_int) (("A", fun _x44 -> result (int 0)) self.f_lambda2)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_152 = ((("A", int 1) self.f_int) :: (("B", []) self.f_list)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_154 = (add_seconds (("A", timestamp 1) self.f_timestamp) (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable ticket_156 = (ticket (("A", int 1) self.f_int) (("B", nat 1) self.f_nat)) in ();
    let%mutable compute_test_eval_order_156 = ticket_156 in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_161 = ((("A", int 1) self.f_int) + (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_163 = ((("A", int 1) self.f_int) * (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_165 = ((("A", int 1) self.f_int) - (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_167 = ((("A", int 1) self.f_int) + (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_169 = ((("A", nat 1) self.f_nat) / (("B", nat 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_171 = ((("A", int 1) self.f_int) = (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_173 = ((("A", int 1) self.f_int) <> (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_175 = ((("A", int 1) self.f_int) < (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_177 = ((("A", int 1) self.f_int) <= (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_179 = ((("A", int 1) self.f_int) > (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_181 = ((("A", int 1) self.f_int) >= (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_183 = ((("A", int 1) self.f_int) % (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_185 = ((("A", nat 1) self.f_nat) ^ (("B", nat 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_187 = (min (("A", int 1) self.f_int) (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_189 = (max (("A", int 1) self.f_int) (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_191 = (ediv (("A", int 1) self.f_int) (("B", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_194 = ((("A", false) self.f_bool) || (("B", false) self.f_bool)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_196 = ((("A", false) self.f_bool) || (("B", true) self.f_bool)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_198 = ((("A", true) self.f_bool) || (("B", false) self.f_bool)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_200 = ((("A", true) self.f_bool) || (("B", true) self.f_bool)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_203 = ((("A", false) self.f_bool) && (("B", false) self.f_bool)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_205 = ((("A", false) self.f_bool) && (("B", true) self.f_bool)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_207 = ((("A", true) self.f_bool) && (("B", false) self.f_bool)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_209 = ((("A", true) self.f_bool) && (("B", true) self.f_bool)) in ();
    verify (data.x = "B")

  let%entry_point test_prim3 () =
    let%mutable compute_test_eval_order_223 = (split_tokens (("A", mutez 1) self.f_mutez) (("B", nat 1) self.f_nat) (("C", nat 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_225 = (split_tokens (tez 0) (("B", nat 1) self.f_nat) (("C", nat 1) self.f_nat)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_228 = (range (("A", nat 1) self.f_nat) (("B", nat 1) self.f_nat) (("C", nat 1) self.f_nat)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_232 = (Map.update (("B", int 1) self.f_int) (("C", some (int 1)) self.f_option) (("A", Map.make []) self.f_map)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_234 = (Map.update (int 0) (("C", some (int 1)) self.f_option) (("A", Map.make []) self.f_map)) in ();
    verify (data.x = "C");
    match_pair_test_eval_order_238_fst, match_pair_test_eval_order_238_snd = match_tuple(get_and_update (("B", int 1) self.f_int) (("C", some (int 1)) self.f_option) (("A", Map.make []) self.f_map), "match_pair_test_eval_order_238_fst", "match_pair_test_eval_order_238_snd")
    let%mutable compute_test_eval_order_238 = (match_pair_test_eval_order_238_fst, match_pair_test_eval_order_238_snd) in ();
    verify (data.x = "B");
    match_pair_test_eval_order_240_fst, match_pair_test_eval_order_240_snd = match_tuple(get_and_update (int 0) (("C", some (int 1)) self.f_option) (("A", Map.make []) self.f_map), "match_pair_test_eval_order_240_fst", "match_pair_test_eval_order_240_snd")
    let%mutable compute_test_eval_order_240 = (match_pair_test_eval_order_240_fst, match_pair_test_eval_order_240_snd) in ();
    verify (data.x = "C");
    let%mutable compute_test_eval_order_243 = (eif (("A", true) self.f_bool) (("B", int 1) self.f_int) (("C", int 1) self.f_int)) in ();
    verify (data.x = "B");
    let%mutable compute_test_eval_order_245 = (eif (("A", true) self.f_bool) (int 0) (("C", int 1) self.f_int)) in ();
    verify (data.x = "A");
    let%mutable compute_test_eval_order_248 = (eif (("A", false) self.f_bool) (("B", int 1) self.f_int) (("C", int 1) self.f_int)) in ();
    verify (data.x = "C");
    let%mutable compute_test_eval_order_250 = (eif (("A", false) self.f_bool) (("B", int 1) self.f_int) (int 0)) in ();
    verify (data.x = "A")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {sub = address; x = string}]
      ~storage:[%expr
                 {sub = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  x = ""}]
      [put_int; test_expr_other; test_mprim2; test_mprim3; test_prim2; test_prim3]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())