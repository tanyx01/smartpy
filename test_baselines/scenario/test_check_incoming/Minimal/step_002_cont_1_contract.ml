open SmartML

module Contract = struct
  let%entry_point any_ep () =
    ()

  let%entry_point any_lazy_ep () =
    ()

  let%entry_point transfer_KO () =
    ()

  let%entry_point transfer_OK () =
    ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat}]
      ~storage:[%expr
                 {x = 12}]
      [any_ep; any_lazy_ep; transfer_KO; transfer_OK]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())