import smartpy as sp

tstorage = sp.TUnit
tparameter = sp.TVariant(test_factorial = sp.TUnit, test_failing = sp.TUnit).layout(("test_factorial", "test_failing"))
tprivates = { "f": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat), "factorial": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat), "factorial_five": sp.TLambda(sp.TUnit, sp.TIntOrNat), "factorial_rec": sp.TLambda(sp.TInt, sp.TInt), "failing": sp.TLambda(sp.TString, sp.TInt), "g": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat) }
tviews = { }
