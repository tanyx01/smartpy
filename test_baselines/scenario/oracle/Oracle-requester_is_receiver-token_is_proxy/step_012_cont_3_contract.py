import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(next_id = sp.TNat, requests = sp.TBigMap(sp.TNat, sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))))), reverse_requests = sp.TBigMap(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat).layout(("client", "client_request_id")), sp.TNat), setup = sp.TRecord(active = sp.TBool, admin = sp.TAddress, escrow = sp.TAddress, min_amount = sp.TNat, min_cancel_timeout = sp.TInt, min_fulfill_timeout = sp.TInt).layout(("active", ("admin", ("escrow", ("min_amount", ("min_cancel_timeout", "min_fulfill_timeout"))))))).layout(("next_id", ("requests", ("reverse_requests", "setup")))))
    self.init(next_id = 0,
              requests = {},
              reverse_requests = {},
              setup = sp.record(active = True, admin = sp.address('tz1R2pWakgAQYaFU9aHbyzYtDPyP3Bds7BTi'), escrow = sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H'), min_amount = 0, min_cancel_timeout = 5, min_fulfill_timeout = 5))

  @sp.entry_point
  def cancel_request(self, params):
    with sp.match_record(self.data, "data") as data:
      sp.verify((sp.sender == data.setup.escrow) | (sp.sender == data.setup.admin), 'OracleSenderIsNotEscrowOrAdmin')
      match_pair_oracle_237_fst, match_pair_oracle_237_snd = sp.match_tuple(sp.get_and_update(data.reverse_requests, sp.record(client = params.client, client_request_id = params.client_request_id), sp.none), "match_pair_oracle_237_fst", "match_pair_oracle_237_snd")
      data.reverse_requests = match_pair_oracle_237_snd
      del data.requests[match_pair_oracle_237_fst.open_some(message = 'OracleRequestUnknown')]

  @sp.entry_point
  def create_request(self, params):
    sp.set_type(params, sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))))).layout(("amount", "request")))
    amount_oracle_181, request_oracle_181 = sp.match_record(params, "amount", "request")
    with sp.match_record(self.data, "data") as data:
      sp.verify(sp.sender == data.setup.escrow, 'OracleNotEscrow')
      sp.verify(data.setup.active, 'OracleInactive')
      ticket_oracle_186_data, ticket_oracle_186_copy = sp.match_tuple(sp.read_ticket_raw(request_oracle_181), "ticket_oracle_186_data", "ticket_oracle_186_copy")
      ticket_oracle_186_ticketer, ticket_oracle_186_content, ticket_oracle_186_amount = sp.match_tuple(ticket_oracle_186_data, "ticket_oracle_186_ticketer", "ticket_oracle_186_content", "ticket_oracle_186_amount")
      sp.verify(data.setup.min_amount <= amount_oracle_181, 'OracleAmountBelowMin')
      sp.verify(sp.add_seconds(sp.now, data.setup.min_cancel_timeout * 60) <= ticket_oracle_186_content.cancel_timeout, 'OracleTimeoutBelowMinTimeout')
      sp.verify(sp.add_seconds(sp.now, data.setup.min_fulfill_timeout * 60) <= ticket_oracle_186_content.fulfill_timeout, 'OracleTimeoutBelowMinTimeout')
      compute_oracle_192 = sp.local("compute_oracle_192", sp.record(client = ticket_oracle_186_ticketer, client_request_id = ticket_oracle_186_content.client_request_id))
      sp.verify(~ (data.reverse_requests.contains(compute_oracle_192.value)), 'OracleRequestKeyAlreadyKnown')
      data.reverse_requests[compute_oracle_192.value] = data.next_id
      data.requests[data.next_id] = ticket_oracle_186_copy
      data.next_id += 1

  @sp.entry_point
  def fulfill_request(self, params):
    with sp.match_record(self.data, "modify_record_oracle_209") as modify_record_oracle_209:
      sp.verify(sp.sender == modify_record_oracle_209.setup.admin, 'OracleNotAdmin')
      match_pair_oracle_212_fst, match_pair_oracle_212_snd = sp.match_tuple(sp.get_and_update(modify_record_oracle_209.requests, params.request_id, sp.none), "match_pair_oracle_212_fst", "match_pair_oracle_212_snd")
      modify_record_oracle_209.requests = match_pair_oracle_212_snd
      ticket_oracle_214_data, ticket_oracle_214_copy = sp.match_tuple(sp.read_ticket_raw(match_pair_oracle_212_fst.open_some(message = 'OracleRequestUnknown')), "ticket_oracle_214_data", "ticket_oracle_214_copy")
      ticket_oracle_214_ticketer, ticket_oracle_214_content, ticket_oracle_214_amount = sp.match_tuple(ticket_oracle_214_data, "ticket_oracle_214_ticketer", "ticket_oracle_214_content", "ticket_oracle_214_amount")
      sp.if params.force:
        pass
      ticket_221 = sp.local("ticket_221", sp.ticket(sp.record(client = ticket_oracle_214_ticketer, client_request_id = ticket_oracle_214_content.client_request_id, result = params.result, tag = 'OracleResult'), 1))
      sp.transfer(sp.record(request = ticket_oracle_214_copy, result = ticket_221.value), sp.tez(0), sp.contract(sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")), modify_record_oracle_209.setup.escrow, entry_point='fulfill_request').open_some())
      del modify_record_oracle_209.reverse_requests[sp.record(client = ticket_oracle_214_ticketer, client_request_id = ticket_oracle_214_content.client_request_id)]

  @sp.entry_point
  def setup(self, params):
    with sp.match_record(self.data, "modify_record_oracle_200") as modify_record_oracle_200:
      sp.verify(sp.sender == modify_record_oracle_200.setup.admin, 'OracleNotAdmin')
      modify_record_oracle_200.setup = params

sp.add_compilation_target("test", Contract())