open SmartML

module Contract = struct
  let%entry_point cancelProposal params =
    verify (contains sender data.addrVoterId) ~msg:"MultisignAdmin_VoterUnknown";
    verify ((contains (Map.get data.addrVoterId sender) data.proposals) && ((Map.get data.proposals (Map.get data.addrVoterId sender)).id = params)) ~msg:"MultisignAdmin_ProposalUnknown";
    (Map.get data.proposals (Map.get data.addrVoterId sender)).canceled <- true

  let%entry_point getLastProposal params =
    transfer (Map.get data.proposals (fst params)) (tez 0) (open_some ~message:"MultisignAdmin_WrongCallbackInterface" (contract {batchs = list (`selfAdmin (list (`changeQuorum nat + `changeTarget address + `changeTimeout int + `changeVoters {added = list {addr = address; publicKey = key}; removed = set address})) + `targetAdmin (list (`changeActive bool + `changeAdmin address + `changeOracles {added = list (pair address {adminAddress = address; endingRound = option nat; startingRound = nat}); removed = list address} + `updateFutureRounds {maxSubmissions = nat; minSubmissions = nat; oraclePayment = nat; restartDelay = nat; timeout = nat}))); canceled = bool; id = nat; nay = set nat; startedAt = timestamp; yay = set nat} (snd params) ))

  let%entry_point getParams params =
    transfer {quorum = data.quorum; target = data.target; timeout = data.timeout} (tez 0) params

  let%entry_point multiVote params =
    set_type params (list {initiatorId = nat; proposalId = nat; votes = list {signature = signature; voterId = nat; yay = bool}});
    List.iter (fun proposalVotes ->
      List.iter (fun vote ->
        verify (contains vote.voterId data.voters) ~msg:"MultisignAdmin_VoterUnknown";
        verify (check_signature (Map.get data.voters vote.voterId).publicKey vote.signature (pack (self_address, (proposalVotes.initiatorId, proposalVotes.proposalId)))) ~msg:"MultisignAdmin_Badsig";
        let%mutable compute_price_feed_multisign_admin_218 = ({initiatorId = proposalVotes.initiatorId; proposalId = proposalVotes.proposalId; voterId = vote.voterId; yay = vote.yay} self.registerVote) in ()
      ) proposalVotes.votes;
      if ((len (Map.get data.proposals proposalVotes.initiatorId).nay) + (len (Map.get data.proposals proposalVotes.initiatorId).yay)) >= data.quorum then
        if (len (Map.get data.proposals proposalVotes.initiatorId).nay) >= (len (Map.get data.proposals proposalVotes.initiatorId).yay) then
          (Map.get data.proposals proposalVotes.initiatorId).canceled <- true
        else
          let%mutable compute_price_feed_multisign_admin_229 = ((Map.get data.proposals proposalVotes.initiatorId) self.onVoted) in ()
    ) params

  let%entry_point newProposal params =
    verify (contains sender data.addrVoterId) ~msg:"MultisignAdmin_VoterUnknown";
    (Map.get data.voters (Map.get data.addrVoterId sender)).lastProposalId <- (Map.get data.voters (Map.get data.addrVoterId sender)).lastProposalId + (nat 1);
    Map.set data.proposals (Map.get data.addrVoterId sender) {batchs = params; canceled = false; id = (Map.get data.voters (Map.get data.addrVoterId sender)).lastProposalId; nay = (Set.make []); startedAt = now; yay = (Set.make [Map.get data.addrVoterId sender])};
    if data.quorum < (nat 2) then
      let%mutable compute_price_feed_multisign_admin_163 = ((Map.get data.proposals (Map.get data.addrVoterId sender)) self.onVoted) in ()

  let%entry_point vote params =
    verify (contains sender data.addrVoterId) ~msg:"MultisignAdmin_VoterUnknown";
    List.iter (fun vote ->
      let%mutable compute_price_feed_multisign_admin_171 = ({initiatorId = vote.initiatorId; proposalId = vote.proposalId; voterId = (Map.get data.addrVoterId sender); yay = vote.yay} self.registerVote) in ();
      if ((len (Map.get data.proposals vote.initiatorId).nay) + (len (Map.get data.proposals vote.initiatorId).yay)) >= data.quorum then
        if (len (Map.get data.proposals vote.initiatorId).nay) >= (len (Map.get data.proposals vote.initiatorId).yay) then
          (Map.get data.proposals vote.initiatorId).canceled <- true
        else
          let%mutable compute_price_feed_multisign_admin_184 = ((Map.get data.proposals vote.initiatorId) self.onVoted) in ()
    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {addrVoterId = big_map address nat; keyVoterId = big_map key nat; lastVoteTimestamp = timestamp; lastVoterId = nat; metadata = big_map string bytes; nbVoters = nat; proposals = big_map nat {batchs = list (`selfAdmin (list (`changeQuorum nat + `changeTarget address + `changeTimeout int + `changeVoters {added = list {addr = address; publicKey = key}; removed = set address})) + `targetAdmin (list (`changeActive bool + `changeAdmin address + `changeOracles {added = list (pair address {adminAddress = address; endingRound = option nat; startingRound = nat}); removed = list address} + `updateFutureRounds {maxSubmissions = nat; minSubmissions = nat; oraclePayment = nat; restartDelay = nat; timeout = nat}))); canceled = bool; id = nat; nay = set nat; startedAt = timestamp; yay = set nat}; quorum = nat; target = option address; timeout = int; voters = big_map nat {addr = address; lastProposalId = nat; publicKey = key}}]
      ~storage:[%expr
                 {addrVoterId = Map.make [(address "tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a", nat 0)];
                  keyVoterId = Map.make [(key "edpkuZ7ERiU5B8knLqQsVMH86j9RLMUyHyL665oCXDkPQxF7HGqSeJ", nat 0)];
                  lastVoteTimestamp = timestamp 0;
                  lastVoterId = nat 0;
                  metadata = Map.make [("", bytes "0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d57474c57783470475a4272564639427a313270415433413544756e77336f394e4d414b5676574b3531436679")];
                  nbVoters = nat 1;
                  proposals = Map.make [];
                  quorum = nat 1;
                  target = None;
                  timeout = int 5;
                  voters = Map.make [(nat 0, {addr = address "tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a"; lastProposalId = nat 0; publicKey = key "edpkuZ7ERiU5B8knLqQsVMH86j9RLMUyHyL665oCXDkPQxF7HGqSeJ"})]}]
      [cancelProposal; getLastProposal; getParams; multiVote; newProposal; vote]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())