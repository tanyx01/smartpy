import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(addrVoterId = sp.TBigMap(sp.TAddress, sp.TNat), keyVoterId = sp.TBigMap(sp.TKey, sp.TNat), lastVoteTimestamp = sp.TTimestamp, lastVoterId = sp.TNat, metadata = sp.TBigMap(sp.TString, sp.TBytes), nbVoters = sp.TNat, proposals = sp.TBigMap(sp.TNat, sp.TRecord(batchs = sp.TList(sp.TVariant(selfAdmin = sp.TList(sp.TVariant(changeQuorum = sp.TNat, changeTarget = sp.TAddress, changeTimeout = sp.TInt, changeVoters = sp.TRecord(added = sp.TList(sp.TRecord(addr = sp.TAddress, publicKey = sp.TKey).layout(("addr", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters")))), targetAdmin = sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeOracles = sp.TRecord(added = sp.TList(sp.TPair(sp.TAddress, sp.TRecord(adminAddress = sp.TAddress, endingRound = sp.TOption(sp.TNat), startingRound = sp.TNat).layout(("adminAddress", ("endingRound", "startingRound"))))), removed = sp.TList(sp.TAddress)).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = sp.TNat, minSubmissions = sp.TNat, oraclePayment = sp.TNat, restartDelay = sp.TNat, timeout = sp.TNat).layout(("maxSubmissions", ("minSubmissions", ("oraclePayment", ("restartDelay", "timeout")))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds"))))).layout(("selfAdmin", "targetAdmin"))), canceled = sp.TBool, id = sp.TNat, nay = sp.TSet(sp.TNat), startedAt = sp.TTimestamp, yay = sp.TSet(sp.TNat)).layout(("batchs", ("canceled", ("id", ("nay", ("startedAt", "yay"))))))), quorum = sp.TNat, target = sp.TOption(sp.TAddress), timeout = sp.TInt, voters = sp.TBigMap(sp.TNat, sp.TRecord(addr = sp.TAddress, lastProposalId = sp.TNat, publicKey = sp.TKey).layout(("addr", ("lastProposalId", "publicKey"))))).layout(("addrVoterId", ("keyVoterId", ("lastVoteTimestamp", ("lastVoterId", ("metadata", ("nbVoters", ("proposals", ("quorum", ("target", ("timeout", "voters"))))))))))))
    self.init(addrVoterId = {sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a') : 0},
              keyVoterId = {sp.key('edpkuZ7ERiU5B8knLqQsVMH86j9RLMUyHyL665oCXDkPQxF7HGqSeJ') : 0},
              lastVoteTimestamp = sp.timestamp(0),
              lastVoterId = 0,
              metadata = {'' : sp.bytes('0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d57474c57783470475a4272564639427a313270415433413544756e77336f394e4d414b5676574b3531436679')},
              nbVoters = 1,
              proposals = {},
              quorum = 1,
              target = sp.none,
              timeout = 5,
              voters = {0 : sp.record(addr = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'), lastProposalId = 0, publicKey = sp.key('edpkuZ7ERiU5B8knLqQsVMH86j9RLMUyHyL665oCXDkPQxF7HGqSeJ'))})

  @sp.entry_point
  def cancelProposal(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), 'MultisignAdmin_VoterUnknown')
    sp.verify((self.data.proposals.contains(self.data.addrVoterId[sp.sender])) & (self.data.proposals[self.data.addrVoterId[sp.sender]].id == params), 'MultisignAdmin_ProposalUnknown')
    self.data.proposals[self.data.addrVoterId[sp.sender]].canceled = True

  @sp.entry_point
  def getLastProposal(self, params):
    sp.transfer(self.data.proposals[sp.fst(params)], sp.tez(0), sp.contract(sp.TRecord(batchs = sp.TList(sp.TVariant(selfAdmin = sp.TList(sp.TVariant(changeQuorum = sp.TNat, changeTarget = sp.TAddress, changeTimeout = sp.TInt, changeVoters = sp.TRecord(added = sp.TList(sp.TRecord(addr = sp.TAddress, publicKey = sp.TKey).layout(("addr", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters")))), targetAdmin = sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeOracles = sp.TRecord(added = sp.TList(sp.TPair(sp.TAddress, sp.TRecord(adminAddress = sp.TAddress, endingRound = sp.TOption(sp.TNat), startingRound = sp.TNat).layout(("adminAddress", ("endingRound", "startingRound"))))), removed = sp.TList(sp.TAddress)).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = sp.TNat, minSubmissions = sp.TNat, oraclePayment = sp.TNat, restartDelay = sp.TNat, timeout = sp.TNat).layout(("maxSubmissions", ("minSubmissions", ("oraclePayment", ("restartDelay", "timeout")))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds"))))).layout(("selfAdmin", "targetAdmin"))), canceled = sp.TBool, id = sp.TNat, nay = sp.TSet(sp.TNat), startedAt = sp.TTimestamp, yay = sp.TSet(sp.TNat)).layout(("batchs", ("canceled", ("id", ("nay", ("startedAt", "yay")))))), sp.snd(params)).open_some(message = 'MultisignAdmin_WrongCallbackInterface'))

  @sp.entry_point
  def getParams(self, params):
    sp.transfer(sp.record(quorum = self.data.quorum, target = self.data.target, timeout = self.data.timeout), sp.tez(0), params)

  @sp.entry_point
  def multiVote(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(initiatorId = sp.TNat, proposalId = sp.TNat, votes = sp.TList(sp.TRecord(signature = sp.TSignature, voterId = sp.TNat, yay = sp.TBool).layout(("signature", ("voterId", "yay"))))).layout(("initiatorId", ("proposalId", "votes")))))
    sp.for proposalVotes in params:
      sp.for vote in proposalVotes.votes:
        sp.verify(self.data.voters.contains(vote.voterId), 'MultisignAdmin_VoterUnknown')
        sp.verify(sp.check_signature(self.data.voters[vote.voterId].publicKey, vote.signature, sp.pack((sp.self_address, (proposalVotes.initiatorId, proposalVotes.proposalId)))), 'MultisignAdmin_Badsig')
        compute_price_feed_multisign_admin_218 = sp.local("compute_price_feed_multisign_admin_218", self.registerVote(sp.record(initiatorId = proposalVotes.initiatorId, proposalId = proposalVotes.proposalId, voterId = vote.voterId, yay = vote.yay)))
      sp.if (sp.len(self.data.proposals[proposalVotes.initiatorId].nay) + sp.len(self.data.proposals[proposalVotes.initiatorId].yay)) >= self.data.quorum:
        sp.if sp.len(self.data.proposals[proposalVotes.initiatorId].nay) >= sp.len(self.data.proposals[proposalVotes.initiatorId].yay):
          self.data.proposals[proposalVotes.initiatorId].canceled = True
        sp.else:
          compute_price_feed_multisign_admin_229 = sp.local("compute_price_feed_multisign_admin_229", self.onVoted(self.data.proposals[proposalVotes.initiatorId]))

  @sp.entry_point
  def newProposal(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), 'MultisignAdmin_VoterUnknown')
    self.data.voters[self.data.addrVoterId[sp.sender]].lastProposalId += 1
    self.data.proposals[self.data.addrVoterId[sp.sender]] = sp.record(batchs = params, canceled = False, id = self.data.voters[self.data.addrVoterId[sp.sender]].lastProposalId, nay = sp.set([]), startedAt = sp.now, yay = sp.set([self.data.addrVoterId[sp.sender]]))
    sp.if self.data.quorum < 2:
      compute_price_feed_multisign_admin_163 = sp.local("compute_price_feed_multisign_admin_163", self.onVoted(self.data.proposals[self.data.addrVoterId[sp.sender]]))

  @sp.entry_point
  def vote(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), 'MultisignAdmin_VoterUnknown')
    sp.for vote in params:
      compute_price_feed_multisign_admin_171 = sp.local("compute_price_feed_multisign_admin_171", self.registerVote(sp.record(initiatorId = vote.initiatorId, proposalId = vote.proposalId, voterId = self.data.addrVoterId[sp.sender], yay = vote.yay)))
      sp.if (sp.len(self.data.proposals[vote.initiatorId].nay) + sp.len(self.data.proposals[vote.initiatorId].yay)) >= self.data.quorum:
        sp.if sp.len(self.data.proposals[vote.initiatorId].nay) >= sp.len(self.data.proposals[vote.initiatorId].yay):
          self.data.proposals[vote.initiatorId].canceled = True
        sp.else:
          compute_price_feed_multisign_admin_184 = sp.local("compute_price_feed_multisign_admin_184", self.onVoted(self.data.proposals[vote.initiatorId]))

  @sp.private_lambda()
  def onVoted(_x6):
    self.data.lastVoteTimestamp = sp.now
    sp.for batch in _x6.batchs:
      with batch.match_cases() as arg:
        with arg.match('selfAdmin') as selfAdmin:
          sp.for selfAdminAction in selfAdmin:
            with selfAdminAction.match_cases() as arg:
              with arg.match('changeQuorum') as quorum:
                sp.verify(quorum <= self.data.nbVoters, 'MultisignAdmin_MoreQuorumThanVoters')
                self.data.quorum = quorum
              with arg.match('changeTarget') as target:
                self.data.target = sp.some(target)
              with arg.match('changeTimeout') as timeout:
                self.data.timeout = timeout
              with arg.match('changeVoters') as changeVoters:
                sp.for voterAddress in changeVoters.removed.elements():
                  sp.verify(self.data.addrVoterId.contains(voterAddress), 'MultisignAdmin_VoterUnknown')
                  compute_price_feed_multisign_admin_347 = sp.local("compute_price_feed_multisign_admin_347", self.data.addrVoterId[voterAddress])
                  del self.data.addrVoterId[voterAddress]
                  del self.data.keyVoterId[self.data.voters[compute_price_feed_multisign_admin_347.value].publicKey]
                  del self.data.proposals[compute_price_feed_multisign_admin_347.value]
                  del self.data.voters[compute_price_feed_multisign_admin_347.value]
                self.data.nbVoters = sp.as_nat(self.data.nbVoters - sp.len(changeVoters.removed))
                sp.for voter in changeVoters.added:
                  sp.verify((~ (self.data.addrVoterId.contains(voter.addr))) & (~ (self.data.keyVoterId.contains(voter.publicKey))), 'MultisignAdmin_VoterAlreadyknown')
                  self.data.lastVoterId += 1
                  self.data.voters[self.data.lastVoterId] = sp.record(addr = voter.addr, lastProposalId = 0, publicKey = voter.publicKey)
                  self.data.addrVoterId[voter.addr] = self.data.lastVoterId
                  self.data.keyVoterId[voter.publicKey] = self.data.lastVoterId
                self.data.nbVoters += sp.len(changeVoters.added)
                sp.verify(self.data.nbVoters > 0, 'MultisignAdmin_VotersLessThan1')
                sp.verify(self.data.quorum <= self.data.nbVoters, 'MultisignAdmin_MoreQuorumThanVoters')

        with arg.match('targetAdmin') as targetAdmin:
          sp.transfer(targetAdmin, sp.tez(0), sp.contract(sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeOracles = sp.TRecord(added = sp.TList(sp.TPair(sp.TAddress, sp.TRecord(adminAddress = sp.TAddress, endingRound = sp.TOption(sp.TNat), startingRound = sp.TNat).layout(("adminAddress", ("endingRound", "startingRound"))))), removed = sp.TList(sp.TAddress)).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = sp.TNat, minSubmissions = sp.TNat, oraclePayment = sp.TNat, restartDelay = sp.TNat, timeout = sp.TNat).layout(("maxSubmissions", ("minSubmissions", ("oraclePayment", ("restartDelay", "timeout")))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds")))), self.data.target.open_some(message = 'MultisignAdmin_TargetNotSet')).open_some(message = 'MultisignAdmin_TargetUnkown'))


  @sp.private_lambda()
  def registerVote(_x8):
    sp.verify((self.data.proposals.contains(_x8.initiatorId)) & (self.data.proposals[_x8.initiatorId].id == _x8.proposalId), 'MultisignAdmin_ProposalUnknown')
    sp.verify((self.data.proposals[_x8.initiatorId].startedAt > self.data.lastVoteTimestamp) & (~ self.data.proposals[_x8.initiatorId].canceled), 'MultisignAdmin_ProposalClosed')
    sp.verify(sp.now < sp.add_seconds(self.data.proposals[_x8.initiatorId].startedAt, self.data.timeout * 60), 'MultisignAdmin_ProposalTimedout')
    sp.verify((~ (self.data.proposals[_x8.initiatorId].yay.contains(_x8.voterId))) & (~ (self.data.proposals[_x8.initiatorId].nay.contains(_x8.voterId))), 'MultisignAdmin_AlreadyVoted')
    sp.if _x8.yay:
      self.data.proposals[_x8.initiatorId].yay.add(_x8.voterId)
    sp.else:
      self.data.proposals[_x8.initiatorId].nay.add(_x8.voterId)

sp.add_compilation_target("test", Contract())