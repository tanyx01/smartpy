open SmartML

module Contract = struct
  let%entry_point getLatestRoundData () =
    transfer (self_entry_point_address setLatestRoundData) (tez 0) (open_some ~message:"Wrong Interface: Could not resolve proxy latestRoundData entry-point." (contract address data.proxy , entry_point='latestRoundData'))

  let%entry_point setLatestRoundData params =
    set_type params {answer = nat; answeredInRound = nat; roundId = nat; startedAt = timestamp; updatedAt = timestamp};
    verify (sender = data.proxy);
    data.latestRoundData <- some params

  let%entry_point setup params =
    verify (sender = data.admin);
    data.admin <- params.admin;
    data.proxy <- params.proxy

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admin = address; latestRoundData = option {answer = nat; answeredInRound = nat; roundId = nat; startedAt = timestamp; updatedAt = timestamp}; proxy = address}]
      ~storage:[%expr
                 {admin = address "tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a";
                  latestRoundData = None;
                  proxy = address "KT1PG6uK91ymZYVtjnRXv2mEdFYSH6P6uJhC"}]
      [getLatestRoundData; setLatestRoundData; setup]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())