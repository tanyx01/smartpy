open SmartML

module Contract = struct
  let%entry_point validate params =
    data.a <- voting_power (hash_key params);
    data.b <- total_voting_power

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = nat; b = nat}]
      ~storage:[%expr
                 {a = nat 0;
                  b = nat 0}]
      [validate]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())