Comment...
 h1: Inter-Contract Calls - example
Table Of Contents

 Inter-Contract Calls - example
# Worker
## Call the Worker contract entry points directly
### set_message
### append_message
# Main
## Call the Main contract entry points, which in turn call through to the Worker contract entry points
### store_single_message
### append_multiple_messages
Comment...
 h2: Worker
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> ""
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_003_cont_0_storage.tz 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_003_cont_0_storage.json 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_003_cont_0_sizes.csv 2
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_003_cont_0_storage.py 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_003_cont_0_types.py 7
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_003_cont_0_contract.tz 45
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_003_cont_0_contract.json 57
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_003_cont_0_contract.py 19
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_003_cont_0_contract.ml 25
Comment...
 h3: Call the Worker contract entry points directly
Comment...
 h4: set_message
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_006_cont_0_params.py 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_006_cont_0_params.tz 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_006_cont_0_params.json 1
Executing set_message('Directly set a message')...
 -> "Directly set a message"
Verifying sp.contract_data(0).message == 'Directly set a message'...
 OK
Comment...
 h4: append_message
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_009_cont_0_params.py 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_009_cont_0_params.tz 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_009_cont_0_params.json 1
Executing append_message(sp.record(message = 'and append another', separator = ', '))...
 -> "Directly set a message, and append another"
Verifying sp.contract_data(0).message == 'Directly set a message, and append another'...
 OK
Comment...
 h2: Main
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 -> "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_012_cont_1_storage.tz 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_012_cont_1_storage.json 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_012_cont_1_sizes.csv 2
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_012_cont_1_storage.py 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_012_cont_1_types.py 7
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_012_cont_1_contract.tz 61
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_012_cont_1_contract.json 76
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_012_cont_1_contract.py 18
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_012_cont_1_contract.ml 25
Comment...
 h3: Call the Main contract entry points, which in turn call through to the Worker contract entry points
Comment...
 h4: store_single_message
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_015_cont_1_params.py 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_015_cont_1_params.tz 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_015_cont_1_params.json 1
Executing store_single_message('Indirectly set a message')...
 -> "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"
  + Transfer
     params: 'Indirectly set a message'
     amount: sp.tez(0)
     to:     sp.contract(sp.TString, sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%set_message')).open_some()
Executing (queue) set_message('Indirectly set a message')...
 -> "Indirectly set a message"
Verifying sp.contract_data(0).message == 'Indirectly set a message'...
 OK
Comment...
 h4: append_multiple_messages
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_018_cont_1_params.py 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_018_cont_1_params.tz 1
 => test_baselines/scenario/inter_contract_calls/InterContractCallsExample/step_018_cont_1_params.json 1
Executing append_multiple_messages(sp.record(messages = sp.list(['and', 'append', 'some', 'more']), separator = ', '))...
 -> "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"
  + Transfer
     params: sp.record(message = 'and', separator = ', ')
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(message = sp.TString, separator = sp.TString).layout(("message", "separator")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%append_message')).open_some()
  + Transfer
     params: sp.record(message = 'append', separator = ', ')
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(message = sp.TString, separator = sp.TString).layout(("message", "separator")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%append_message')).open_some()
  + Transfer
     params: sp.record(message = 'some', separator = ', ')
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(message = sp.TString, separator = sp.TString).layout(("message", "separator")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%append_message')).open_some()
  + Transfer
     params: sp.record(message = 'more', separator = ', ')
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(message = sp.TString, separator = sp.TString).layout(("message", "separator")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%append_message')).open_some()
Executing (queue) append_message(sp.record(message = 'and', separator = ', '))...
 -> "Indirectly set a message, and"
Executing (queue) append_message(sp.record(message = 'append', separator = ', '))...
 -> "Indirectly set a message, and, append"
Executing (queue) append_message(sp.record(message = 'some', separator = ', '))...
 -> "Indirectly set a message, and, append, some"
Executing (queue) append_message(sp.record(message = 'more', separator = ', '))...
 -> "Indirectly set a message, and, append, some, more"
Verifying sp.contract_data(0).message == 'Indirectly set a message, and, append, some, more'...
 OK
Table Of Contents

 Inter-Contract Calls - example
# Worker
## Call the Worker contract entry points directly
### set_message
### append_message
# Main
## Call the Main contract entry points, which in turn call through to the Worker contract entry points
### store_single_message
### append_multiple_messages
