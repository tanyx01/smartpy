parameter (or (list %administrate bytes) (or (list %send_proposal (pair (list %actions bytes) (address %target))) (nat %vote)));
storage   (pair (nat %inactiveBefore) (pair (nat %nextId) (pair (big_map %proposals nat (list (pair (list %actions bytes) (address %target)))) (pair (nat %quorum) (pair (set %signers address) (big_map %votes nat (set address)))))));
code
  {
    UNPAIR;     # @parameter : @storage
    IF_LEFT
      {
        # == administrate ==
        # sp.verify(sp.sender == sp.self_address, 'This entrypoint must be called through the proposal system.') # @parameter%administrate : @storage
        SELF_ADDRESS; # @self : @parameter%administrate : @storage
        SENDER;     # @sender : @self : @parameter%administrate : @storage
        COMPARE;    # int : @parameter%administrate : @storage
        EQ;         # bool : @parameter%administrate : @storage
        IF
          {}
          {
            PUSH string "This entrypoint must be called through the proposal system."; # string : @parameter%administrate : @storage
            FAILWITH;   # FAILED
          }; # @parameter%administrate : @storage
        # for action in params: ... # @parameter%administrate : @storage
        DUP;        # @parameter%administrate : @parameter%administrate : @storage
        ITER
          {
            # with sp.unpack(action, sp.TVariant(addSigners = sp.TList(sp.TAddress), changeQuorum = sp.TNat, removeSigners = sp.TList(sp.TAddress)).layout(("addSigners", ("changeQuorum", "removeSigners")))).open_some(message = 'Bad actions format').match_cases(...): # bytes : @parameter%administrate : @storage
            DUP;        # bytes : bytes : @parameter%administrate : @storage
            UNPACK (or (list address) (or nat (list address))); # option (or (list address) (or nat (list address))) : bytes : @parameter%administrate : @storage
            IF_NONE
              {
                PUSH string "Bad actions format"; # string : bytes : @parameter%administrate : @storage
                FAILWITH;   # FAILED
              }
              {}; # @some : bytes : @parameter%administrate : @storage
            IF_LEFT
              {
                # for signer in addSigners: ... # @some.left : bytes : @parameter%administrate : @storage
                DUP;        # @some.left : @some.left : bytes : @parameter%administrate : @storage
                ITER
                  {
                    # self.data.signers.add(signer) # address : @some.left : bytes : @parameter%administrate : @storage
                    DIG 4;      # @storage : address : @some.left : bytes : @parameter%administrate
                    DUP;        # @storage : @storage : address : @some.left : bytes : @parameter%administrate
                    GET 9;      # set address : @storage : address : @some.left : bytes : @parameter%administrate
                    PUSH bool True; # bool : set address : @storage : address : @some.left : bytes : @parameter%administrate
                    DIG 3;      # address : bool : set address : @storage : @some.left : bytes : @parameter%administrate
                    UPDATE;     # set address : @storage : @some.left : bytes : @parameter%administrate
                    UPDATE 9;   # @storage : @some.left : bytes : @parameter%administrate
                    DUG 3;      # @some.left : bytes : @parameter%administrate : @storage
                  }; # @some.left : bytes : @parameter%administrate : @storage
                DROP 2;     # @parameter%administrate : @storage
              }
              {
                IF_LEFT
                  {
                    SWAP;       # bytes : @some.right.left : @parameter%administrate : @storage
                    DROP;       # @some.right.left : @parameter%administrate : @storage
                    # self.data.quorum = changeQuorum # @some.right.left : @parameter%administrate : @storage
                    DIG 2;      # @storage : @some.right.left : @parameter%administrate
                    SWAP;       # @some.right.left : @storage : @parameter%administrate
                    UPDATE 7;   # @storage : @parameter%administrate
                    SWAP;       # @parameter%administrate : @storage
                  }
                  {
                    # for address in removeSigners: ... # @some.right.right : bytes : @parameter%administrate : @storage
                    DUP;        # @some.right.right : @some.right.right : bytes : @parameter%administrate : @storage
                    ITER
                      {
                        # self.data.signers.remove(address) # address : @some.right.right : bytes : @parameter%administrate : @storage
                        DIG 4;      # @storage : address : @some.right.right : bytes : @parameter%administrate
                        DUP;        # @storage : @storage : address : @some.right.right : bytes : @parameter%administrate
                        GET 9;      # set address : @storage : address : @some.right.right : bytes : @parameter%administrate
                        PUSH bool False; # bool : set address : @storage : address : @some.right.right : bytes : @parameter%administrate
                        DIG 3;      # address : bool : set address : @storage : @some.right.right : bytes : @parameter%administrate
                        UPDATE;     # set address : @storage : @some.right.right : bytes : @parameter%administrate
                        UPDATE 9;   # @storage : @some.right.right : bytes : @parameter%administrate
                        DUG 3;      # @some.right.right : bytes : @parameter%administrate : @storage
                      }; # @some.right.right : bytes : @parameter%administrate : @storage
                    DROP 2;     # @parameter%administrate : @storage
                  }; # list bytes : @storage
              }; # list bytes : @storage
            SWAP;       # @storage : list bytes
            # sp.verify(self.data.quorum <= sp.len(self.data.signers), 'More quorum than signers.') # @storage : list bytes
            DUP;        # @storage : @storage : list bytes
            DUG 2;      # @storage : list bytes : @storage
            GET 9;      # set address : list bytes : @storage
            SIZE;       # nat : list bytes : @storage
            DUP 3;      # @storage : nat : list bytes : @storage
            GET 7;      # nat : nat : list bytes : @storage
            COMPARE;    # int : list bytes : @storage
            LE;         # bool : list bytes : @storage
            IF
              {}
              {
                PUSH string "More quorum than signers."; # string : list bytes : @storage
                FAILWITH;   # FAILED
              }; # list bytes : @storage
          }; # @parameter%administrate : @storage
        DROP;       # @storage
        NIL operation; # list operation : @storage
      }
      {
        IF_LEFT
          {
            SWAP;       # @storage : @parameter%send_proposal
            # == send_proposal ==
            # sp.verify(self.data.signers.contains(sp.sender), 'Only signers can propose') # @storage : @parameter%send_proposal
            DUP;        # @storage : @storage : @parameter%send_proposal
            DUG 2;      # @storage : @parameter%send_proposal : @storage
            GET 9;      # set address : @parameter%send_proposal : @storage
            SENDER;     # @sender : set address : @parameter%send_proposal : @storage
            MEM;        # bool : @parameter%send_proposal : @storage
            IF
              {}
              {
                PUSH string "Only signers can propose"; # string : @parameter%send_proposal : @storage
                FAILWITH;   # FAILED
              }; # @parameter%send_proposal : @storage
            SWAP;       # @storage : @parameter%send_proposal
            # self.data.proposals[self.data.nextId] = params # @storage : @parameter%send_proposal
            DUP;        # @storage : @storage : @parameter%send_proposal
            DUG 2;      # @storage : @parameter%send_proposal : @storage
            DUP;        # @storage : @storage : @parameter%send_proposal : @storage
            GET 5;      # big_map nat (list (pair (list %actions bytes) (address %target))) : @storage : @parameter%send_proposal : @storage
            DIG 2;      # @parameter%send_proposal : big_map nat (list (pair (list %actions bytes) (address %target))) : @storage : @storage
            SOME;       # option (list (pair (list %actions bytes) (address %target))) : big_map nat (list (pair (list %actions bytes) (address %target))) : @storage : @storage
            DIG 3;      # @storage : option (list (pair (list %actions bytes) (address %target))) : big_map nat (list (pair (list %actions bytes) (address %target))) : @storage
            GET 3;      # nat : option (list (pair (list %actions bytes) (address %target))) : big_map nat (list (pair (list %actions bytes) (address %target))) : @storage
            UPDATE;     # big_map nat (list (pair (list %actions bytes) (address %target))) : @storage
            UPDATE 5;   # @storage
            # self.data.votes[self.data.nextId] = sp.set([]) # @storage
            DUP;        # @storage : @storage
            DUP;        # @storage : @storage : @storage
            GET 10;     # big_map nat (set address) : @storage : @storage
            PUSH (option (set address)) (Some {}); # option (set address) : big_map nat (set address) : @storage : @storage
            DIG 3;      # @storage : option (set address) : big_map nat (set address) : @storage
            GET 3;      # nat : option (set address) : big_map nat (set address) : @storage
            UPDATE;     # big_map nat (set address) : @storage
            UPDATE 10;  # @storage
            # self.data.nextId += 1 # @storage
            DUP;        # @storage : @storage
            GET 3;      # nat : @storage
            PUSH nat 1; # nat : nat : @storage
            ADD;        # nat : @storage
            UPDATE 3;   # @storage
            NIL operation; # list operation : @storage
          }
          {
            SWAP;       # @storage : @parameter%vote
            # == vote ==
            # sp.verify(self.data.signers.contains(sp.sender), 'Only signers can vote') # @storage : @parameter%vote
            DUP;        # @storage : @storage : @parameter%vote
            DUG 2;      # @storage : @parameter%vote : @storage
            GET 9;      # set address : @parameter%vote : @storage
            SENDER;     # @sender : set address : @parameter%vote : @storage
            MEM;        # bool : @parameter%vote : @storage
            IF
              {}
              {
                PUSH string "Only signers can vote"; # string : @parameter%vote : @storage
                FAILWITH;   # FAILED
              }; # @parameter%vote : @storage
            SWAP;       # @storage : @parameter%vote
            # sp.verify(self.data.votes.contains(params), 'Proposal unknown') # @storage : @parameter%vote
            DUP;        # @storage : @storage : @parameter%vote
            DUG 2;      # @storage : @parameter%vote : @storage
            GET 10;     # big_map nat (set address) : @parameter%vote : @storage
            SWAP;       # @parameter%vote : big_map nat (set address) : @storage
            DUP;        # @parameter%vote : @parameter%vote : big_map nat (set address) : @storage
            DUG 2;      # @parameter%vote : big_map nat (set address) : @parameter%vote : @storage
            MEM;        # bool : @parameter%vote : @storage
            IF
              {}
              {
                PUSH string "Proposal unknown"; # string : @parameter%vote : @storage
                FAILWITH;   # FAILED
              }; # @parameter%vote : @storage
            SWAP;       # @storage : @parameter%vote
            # sp.verify(params >= self.data.inactiveBefore, 'The proposal is inactive') # @storage : @parameter%vote
            DUP;        # @storage : @storage : @parameter%vote
            DUG 2;      # @storage : @parameter%vote : @storage
            CAR;        # nat : @parameter%vote : @storage
            SWAP;       # @parameter%vote : nat : @storage
            DUP;        # @parameter%vote : @parameter%vote : nat : @storage
            DUG 2;      # @parameter%vote : nat : @parameter%vote : @storage
            COMPARE;    # int : @parameter%vote : @storage
            GE;         # bool : @parameter%vote : @storage
            IF
              {}
              {
                PUSH string "The proposal is inactive"; # string : @parameter%vote : @storage
                FAILWITH;   # FAILED
              }; # @parameter%vote : @storage
            SWAP;       # @storage : @parameter%vote
            # self.data.votes[params].add(sp.sender) # @storage : @parameter%vote
            DUP;        # @storage : @storage : @parameter%vote
            GET 10;     # big_map nat (set address) : @storage : @parameter%vote
            DUP;        # big_map nat (set address) : big_map nat (set address) : @storage : @parameter%vote
            DUP 4;      # @parameter%vote : big_map nat (set address) : big_map nat (set address) : @storage : @parameter%vote
            DUP;        # @parameter%vote : @parameter%vote : big_map nat (set address) : big_map nat (set address) : @storage : @parameter%vote
            DUG 2;      # @parameter%vote : big_map nat (set address) : @parameter%vote : big_map nat (set address) : @storage : @parameter%vote
            GET;        # option (set address) : @parameter%vote : big_map nat (set address) : @storage : @parameter%vote
            IF_NONE
              {
                PUSH int 60; # int : @parameter%vote : big_map nat (set address) : @storage : @parameter%vote
                FAILWITH;   # FAILED
              }
              {}; # @some : @parameter%vote : big_map nat (set address) : @storage : @parameter%vote
            PUSH bool True; # bool : @some : @parameter%vote : big_map nat (set address) : @storage : @parameter%vote
            SENDER;     # @sender : bool : @some : @parameter%vote : big_map nat (set address) : @storage : @parameter%vote
            UPDATE;     # set address : @parameter%vote : big_map nat (set address) : @storage : @parameter%vote
            SOME;       # option (set address) : @parameter%vote : big_map nat (set address) : @storage : @parameter%vote
            SWAP;       # @parameter%vote : option (set address) : big_map nat (set address) : @storage : @parameter%vote
            UPDATE;     # big_map nat (set address) : @storage : @parameter%vote
            UPDATE 10;  # @storage : @parameter%vote
            # if sp.len(self.data.votes.get(params, default_value = sp.set([]))) >= self.data.quorum: # @storage : @parameter%vote
            DUP;        # @storage : @storage : @parameter%vote
            GET 7;      # nat : @storage : @parameter%vote
            SWAP;       # @storage : nat : @parameter%vote
            DUP;        # @storage : @storage : nat : @parameter%vote
            DUG 3;      # @storage : nat : @parameter%vote : @storage
            GET 10;     # big_map nat (set address) : nat : @parameter%vote : @storage
            DUP 3;      # @parameter%vote : big_map nat (set address) : nat : @parameter%vote : @storage
            GET;        # option (set address) : nat : @parameter%vote : @storage
            IF_NONE
              {
                EMPTY_SET address; # set address : nat : @parameter%vote : @storage
              }
              {}; # set address : nat : @parameter%vote : @storage
            SIZE;       # nat : nat : @parameter%vote : @storage
            COMPARE;    # int : @parameter%vote : @storage
            GE;         # bool : @parameter%vote : @storage
            IF
              {
                # for p_item in self.data.proposals.get(params, default_value = sp.list([])): ... # @parameter%vote : @storage
                NIL operation; # list operation : @parameter%vote : @storage
                DUP 3;      # @storage : list operation : @parameter%vote : @storage
                GET 5;      # big_map nat (list (pair (list %actions bytes) (address %target))) : list operation : @parameter%vote : @storage
                DIG 2;      # @parameter%vote : big_map nat (list (pair (list %actions bytes) (address %target))) : list operation : @storage
                GET;        # option (list (pair (list %actions bytes) (address %target))) : list operation : @storage
                IF_NONE
                  {
                    NIL (pair (list bytes) address); # list (pair (list bytes) address) : list operation : @storage
                  }
                  {}; # list (pair (list bytes) address) : list operation : @storage
                ITER
                  {
                    # sp.transfer(p_item.actions, sp.tez(0), sp.contract(sp.TList(sp.TBytes), p_item.target).open_some(message = 'InvalidTarget')) # pair (list bytes) address : list operation : @storage
                    DUP;        # pair (list bytes) address : pair (list bytes) address : list operation : @storage
                    DUG 2;      # pair (list bytes) address : list operation : pair (list bytes) address : @storage
                    CDR;        # address : list operation : pair (list bytes) address : @storage
                    CONTRACT (list bytes); # option (contract (list bytes)) : list operation : pair (list bytes) address : @storage
                    IF_NONE
                      {
                        PUSH string "InvalidTarget"; # string : list operation : pair (list bytes) address : @storage
                        FAILWITH;   # FAILED
                      }
                      {}; # @some : list operation : pair (list bytes) address : @storage
                    PUSH mutez 0; # mutez : @some : list operation : pair (list bytes) address : @storage
                    DIG 3;      # pair (list bytes) address : mutez : @some : list operation : @storage
                    CAR;        # list bytes : mutez : @some : list operation : @storage
                    TRANSFER_TOKENS; # operation : list operation : @storage
                    CONS;       # list operation : @storage
                  }; # list operation : @storage
                SWAP;       # @storage : list operation
                # self.data.inactiveBefore = self.data.nextId # @storage : list operation
                DUP;        # @storage : @storage : list operation
                GET 3;      # nat : @storage : list operation
                UPDATE 1;   # @storage : list operation
                SWAP;       # list operation : @storage
              }
              {
                DROP;       # @storage
                NIL operation; # list operation : @storage
              }; # list operation : @storage
          }; # list operation : @storage
      }; # list operation : @storage
    NIL operation; # list operation : list operation : @storage
    SWAP;       # list operation : list operation : @storage
    ITER
      {
        CONS;       # list operation : @storage
      }; # list operation : @storage
    PAIR;       # pair (list operation) @storage
  };