open SmartML

module Contract = struct
  let%entry_point administrate params =
    verify (sender = self_address) ~msg:"This entrypoint must be called through the proposal system.";
    List.iter (fun action ->
      match open_some ~message:"Bad actions format" (unpack action (`addSigners (list address) + `changeQuorum nat + `removeSigners (list address))) with
        | `changeQuorum changeQuorum ->
          data.quorum <- changeQuorum
        | `addSigners addSigners ->
          List.iter (fun signer ->
            Set.add data.signers signer
          ) addSigners
        | `removeSigners removeSigners ->
          List.iter (fun address ->
            Set.remove data.signers address
          ) removeSigners
;
      verify (data.quorum <= (len data.signers)) ~msg:"More quorum than signers."
    ) params

  let%entry_point send_proposal params =
    verify (contains sender data.signers) ~msg:"Only signers can propose";
    Map.set data.proposals data.nextId params;
    Map.set data.votes data.nextId (Set.make []);
    data.nextId <- data.nextId + (nat 1)

  let%entry_point vote params =
    verify (contains sender data.signers) ~msg:"Only signers can vote";
    verify (contains params data.votes) ~msg:"Proposal unknown";
    verify (params >= data.inactiveBefore) ~msg:"The proposal is inactive";
    Set.add (Map.get data.votes params) sender;
    if (len (Map.get ~default_value:(Set.make []) data.votes params)) >= data.quorum then
      (
        List.iter (fun p_item ->
          transfer p_item.actions (tez 0) (open_some ~message:"InvalidTarget" (contract (list bytes) p_item.target ))
        ) (Map.get ~default_value:[] data.proposals params);
        data.inactiveBefore <- data.nextId
      )

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {inactiveBefore = nat; nextId = nat; proposals = big_map nat (list {actions = list bytes; target = address}); quorum = nat; signers = set address; votes = big_map nat (set address)}]
      ~storage:[%expr
                 {inactiveBefore = nat 0;
                  nextId = nat 0;
                  proposals = Map.make [];
                  quorum = nat 2;
                  signers = Set.make([address "tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT"; address "tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR"]);
                  votes = Map.make []}]
      [administrate; send_proposal; vote]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())