import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(r = sp.TIntOrNat).layout("r"))
    self.init(r = 0)

sp.add_compilation_target("test", Contract())