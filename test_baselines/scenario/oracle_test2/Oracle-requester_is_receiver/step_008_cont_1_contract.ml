open SmartML

module Contract = struct
  let%entry_point configure params =
    verify (data.admin = sender) ~msg:"Privileged operation";
    data <- params

  let%entry_point request_tokens params =
    set_type params (set address);
    transfer [{from_ = self_address; txs = (map (fun _x2 -> result {to_ = _x2; token_id = (nat 0); amount = data.max_amount}) (Set.elements params))}] (tez 0) (open_some ~message:"Incompatible token interface" (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) data.token , entry_point='transfer'))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {active = bool; admin = address; max_amount = nat; token = address}]
      ~storage:[%expr
                 {active = true;
                  admin = address "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w";
                  max_amount = nat 10;
                  token = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"}]
      [configure; request_tokens]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())