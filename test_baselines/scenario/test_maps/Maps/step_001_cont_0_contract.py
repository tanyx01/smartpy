import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TIntOrNat, sp.TString), x = sp.TString, y = sp.TOption(sp.TString), z = sp.TOption(sp.TString)).layout(("m", ("x", ("y", "z")))))
    self.init(m = {},
              x = 'a',
              y = sp.none,
              z = sp.some('na'))

  @sp.entry_point
  def test_get_and_update(self):
    match_pair_test_maps_46_fst, match_pair_test_maps_46_snd = sp.match_tuple(sp.get_and_update(self.data.m, 1, sp.some('one')), "match_pair_test_maps_46_fst", "match_pair_test_maps_46_snd")
    self.data.z = match_pair_test_maps_46_fst

  @sp.entry_point
  def test_map_get(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params[12]

  @sp.entry_point
  def test_map_get2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params[12]

  @sp.entry_point
  def test_map_get_default_values(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params.get(12, default_value = 'abc')

  @sp.entry_point
  def test_map_get_missing_value(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params.get(12, message = 'missing 12')

  @sp.entry_point
  def test_map_get_missing_value2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params.get(12, message = 1234)

  @sp.entry_point
  def test_map_get_opt(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.y = params.get_opt(12)

  @sp.entry_point
  def test_update_map(self):
    self.data.m[1] = 'one'

sp.add_compilation_target("test", Contract())