open SmartML

module Contract = struct
  let%entry_point comparisons () =
    verify (data.i <= (int 123));
    verify (((int 2) + data.i) = (int 12));
    verify (((int 2) + data.i) <> (int 1234));
    verify ((data.i + (int 4)) <> (int 123));
    verify ((data.i - (int 5)) < (int 123));
    verify (((int 7) - data.i) < (int 123));
    verify (data.i > (int 3));
    verify (((int 4) * data.i) > (int 3));
    verify ((data.i * (int 5)) > (int 3));
    verify (data.i >= (int 3));
    verify (data.i > (int 3));
    verify (data.i >= (int 3));
    verify (data.i < (int 3000));
    verify (data.i <= (int 3000));
    verify (data.b && true);
    if data.i > (int 3) then
      verify (data.i > (int 4))

  let%entry_point iterations () =
    let%mutable x = data.i in ();
    x <- int 0;
    x <- int 1;
    x <- int 2;
    x <- int 3;
    x <- int 4;
    List.iter (fun i ->
      x <- i
    ) (range (int 0) (int 5) (int 1));
    data.i <- to_int data.n;
    data.i <- int 5;
    while data.i <= (int 42) do
      data.i <- data.i + (int 2)
    done;
    if data.i <= (int 123) then
      (
        x <- int 12;
        data.i <- data.i + x
      )
    else
      (
        x <- int 5;
        data.i <- x
      )

  let%entry_point localVariable () =
    let%mutable x = data.i in ();
    x <- x * (int 2);
    data.i <- int 10;
    data.i <- x

  let%entry_point myMessageName4 () =
    List.iter (fun x ->
      data.i <- data.i + (x.key * x.value)
    ) (Map.items data.m)

  let%entry_point myMessageName5 () =
    List.iter (fun x ->
      data.i <- data.i + ((int 2) * x)
    ) (Map.keys data.m)

  let%entry_point myMessageName6 params =
    List.iter (fun x ->
      data.i <- data.i + ((int 3) * x)
    ) (Map.values data.m);
    Set.remove data.aaa (nat 2);
    Set.add data.aaa (nat 12);
    data.abc <- (some 16) :: data.abc;
    Map.set data.abca 0 (some 16);
    if contains (nat 12) data.aaa then
      Map.set data.m (int 42) (int 43);
    Set.add data.aaa (open_some (is_nat (- params)))

  let%entry_point someComputations params =
    verify (data.i <= (int 123));
    data.i <- data.i + params.y;
    data.acb <- params.x;
    data.i <- int 100;
    data.i <- data.i - (int 1);
    verify ((add (nat 4) (nat 5)) = (nat 9));
    verify ((add (int ((-4))) (nat 5)) = (int 1));
    verify ((add (nat 5) (int ((-4)))) = (int 1));
    verify ((add (int ((-4))) (int 5)) = (int 1));
    verify ((mul (nat 4) (nat 5)) = (nat 20));
    verify ((mul (int ((-4))) (nat 5)) = (int ((-20))));
    verify ((mul (nat 5) (int ((-4)))) = (int ((-20))));
    verify ((mul (int ((-4))) (int 5)) = (int ((-20))))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {aaa = set nat; abc = list (option intOrNat); abca = map intOrNat (option intOrNat); acb = string; b = bool; ddd = list intOrNat; f = bool; h = bytes; i = int; m = map int int; n = nat; pkh = key_hash; s = string; toto = string}]
      ~storage:[%expr
                 {aaa = Set.make([nat 1; nat 2; nat 3]);
                  abc = [Some(123); None];
                  abca = Map.make [(0, Some(123)); (1, None)];
                  acb = "toto";
                  b = true;
                  ddd = [0; 1; 2; 3; 4; 5; 6; 7; 8; 9];
                  f = false;
                  h = bytes "0x0000ab112233aa";
                  i = int 7;
                  m = Map.make [];
                  n = nat 123;
                  pkh = key_hash "tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk";
                  s = "abc";
                  toto = "ABC"}]
      [comparisons; iterations; localVariable; myMessageName4; myMessageName5; myMessageName6; someComputations]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())