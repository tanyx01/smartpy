open SmartML

module Contract = struct
  let%entry_point ep () =
    verify (contains {a = (nat 0); b = sender; c = (nat 0)} data.x)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = map {a = nat; b = address; c = nat} unit}]
      ~storage:[%expr
                 {x = Map.make []}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())