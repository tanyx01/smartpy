import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def pack_lambda_record(self):
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda _x0: sp.record(a = 1, b = _x0 + 2))), sp.TLambda(sp.TInt, sp.TRecord(a = sp.TInt, b = sp.TInt).layout(("a", "b")))).open_some()(100).a == 1)
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda _x2: sp.record(a = _x2 + 1))), sp.TLambda(sp.TInt, sp.TRecord(a = sp.TInt).layout("a"))).open_some()(100).a == 101)

  @sp.entry_point
  def pack_lambda_variant(self):
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda _x4: sp.set_type_expr(variant('a', _x4 + 1), sp.TVariant(a = sp.TInt, b = sp.TInt).layout(("a", "b"))))), sp.TLambda(sp.TInt, sp.TVariant(a = sp.TInt, b = sp.TInt).layout(("a", "b")))).open_some()(100).open_variant('a') == 101)
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda _x6: variant('a', _x6 + 1))), sp.TLambda(sp.TInt, sp.TVariant(a = sp.TInt).layout("a"))).open_some()(100).open_variant('a') == 101)

  @sp.entry_point
  def pack_lambdas(self):
    def f_x8(_x8):
      y = sp.local("y", 0)
      r = sp.local("r", 'A')
      sp.if _x8 == 0:
        sp.if _x8 == y.value:
          r.value = 'B'
      sp.result(r.value)
    f1 = sp.local("f1", sp.build_lambda(f_x8), sp.TLambda(sp.TInt, sp.TString))
    p1 = sp.local("p1", sp.pack(f1.value))
    f2 = sp.local("f2", sp.unpack(p1.value, sp.TLambda(sp.TInt, sp.TString)).open_some(), sp.TLambda(sp.TInt, sp.TString))
    p2 = sp.local("p2", sp.pack(f2.value))
    sp.verify(p1.value == p2.value)
    sp.verify(f1.value(0) == f2.value(0))
    sp.verify(f1.value(1) == f2.value(1))
    sp.verify(f1.value(2) == f2.value(2))

  @sp.entry_point
  def pack_lambdas2(self):
    f1 = sp.local("f1", sp.build_lambda(lambda _x10: _x10 > 0), sp.TLambda(sp.TInt, sp.TBool))
    p1 = sp.local("p1", sp.pack(f1.value))
    f2 = sp.local("f2", sp.unpack(p1.value, sp.TLambda(sp.TInt, sp.TBool)).open_some(), sp.TLambda(sp.TInt, sp.TBool))
    sp.verify(f2.value(1))

  @sp.entry_point
  def run(self):
    sp.verify(sp.pack(True) == sp.bytes('0x05030a'))
    sp.verify(sp.pack(sp.set_type_expr(True, sp.TBool)) == sp.pack(sp.set_type_expr(True, sp.TBool)))
    sp.verify(sp.pack(False) == sp.bytes('0x050303'))
    sp.verify(sp.pack(sp.set_type_expr(False, sp.TBool)) == sp.pack(sp.set_type_expr(False, sp.TBool)))
    sp.verify(sp.pack(sp.unit) == sp.bytes('0x05030b'))
    sp.verify(sp.pack(sp.set_type_expr(sp.unit, sp.TUnit)) == sp.pack(sp.set_type_expr(sp.unit, sp.TUnit)))
    sp.verify(sp.pack(sp.tez(0)) == sp.bytes('0x050000'))
    sp.verify(sp.pack(sp.set_type_expr(sp.tez(0), sp.TMutez)) == sp.pack(sp.set_type_expr(sp.tez(0), sp.TMutez)))
    sp.verify(sp.pack(sp.mutez(1)) == sp.bytes('0x050001'))
    sp.verify(sp.pack(sp.set_type_expr(sp.mutez(1), sp.TMutez)) == sp.pack(sp.set_type_expr(sp.mutez(1), sp.TMutez)))
    sp.verify(sp.pack(sp.mutez(2)) == sp.bytes('0x050002'))
    sp.verify(sp.pack(sp.set_type_expr(sp.mutez(2), sp.TMutez)) == sp.pack(sp.set_type_expr(sp.mutez(2), sp.TMutez)))
    sp.verify(sp.pack(sp.mutez(3)) == sp.bytes('0x050003'))
    sp.verify(sp.pack(sp.set_type_expr(sp.mutez(3), sp.TMutez)) == sp.pack(sp.set_type_expr(sp.mutez(3), sp.TMutez)))
    sp.verify(sp.pack(sp.mutez(10)) == sp.bytes('0x05000a'))
    sp.verify(sp.pack(sp.set_type_expr(sp.mutez(10), sp.TMutez)) == sp.pack(sp.set_type_expr(sp.mutez(10), sp.TMutez)))
    sp.verify(sp.pack(sp.mutez(10000)) == sp.bytes('0x0500909c01'))
    sp.verify(sp.pack(sp.set_type_expr(sp.mutez(10000), sp.TMutez)) == sp.pack(sp.set_type_expr(sp.mutez(10000), sp.TMutez)))
    sp.verify(sp.pack(sp.tez(1)) == sp.bytes('0x050080897a'))
    sp.verify(sp.pack(sp.set_type_expr(sp.tez(1), sp.TMutez)) == sp.pack(sp.set_type_expr(sp.tez(1), sp.TMutez)))
    sp.verify(sp.pack(sp.tez(1000000)) == sp.bytes('0x050080c0a8ca9a3a'))
    sp.verify(sp.pack(sp.set_type_expr(sp.tez(1000000), sp.TMutez)) == sp.pack(sp.set_type_expr(sp.tez(1000000), sp.TMutez)))
    sp.verify(sp.pack(0) == sp.bytes('0x050000'))
    sp.verify(sp.pack(sp.set_type_expr(0, sp.TNat)) == sp.pack(sp.set_type_expr(0, sp.TNat)))
    sp.verify(sp.pack(1) == sp.bytes('0x050001'))
    sp.verify(sp.pack(sp.set_type_expr(1, sp.TNat)) == sp.pack(sp.set_type_expr(1, sp.TNat)))
    sp.verify(sp.pack(2) == sp.bytes('0x050002'))
    sp.verify(sp.pack(sp.set_type_expr(2, sp.TNat)) == sp.pack(sp.set_type_expr(2, sp.TNat)))
    sp.verify(sp.pack(3) == sp.bytes('0x050003'))
    sp.verify(sp.pack(sp.set_type_expr(3, sp.TNat)) == sp.pack(sp.set_type_expr(3, sp.TNat)))
    sp.verify(sp.pack(10) == sp.bytes('0x05000a'))
    sp.verify(sp.pack(sp.set_type_expr(10, sp.TNat)) == sp.pack(sp.set_type_expr(10, sp.TNat)))
    sp.verify(sp.pack(10000) == sp.bytes('0x0500909c01'))
    sp.verify(sp.pack(sp.set_type_expr(10000, sp.TNat)) == sp.pack(sp.set_type_expr(10000, sp.TNat)))
    sp.verify(sp.pack(1000000) == sp.bytes('0x050080897a'))
    sp.verify(sp.pack(sp.set_type_expr(1000000, sp.TNat)) == sp.pack(sp.set_type_expr(1000000, sp.TNat)))
    sp.verify(sp.pack(1000000000000) == sp.bytes('0x050080c0a8ca9a3a'))
    sp.verify(sp.pack(sp.set_type_expr(1000000000000, sp.TNat)) == sp.pack(sp.set_type_expr(1000000000000, sp.TNat)))
    sp.verify(sp.pack(0) == sp.bytes('0x050000'))
    sp.verify(sp.pack(sp.set_type_expr(0, sp.TInt)) == sp.pack(sp.set_type_expr(0, sp.TInt)))
    sp.verify(sp.pack(1) == sp.bytes('0x050001'))
    sp.verify(sp.pack(sp.set_type_expr(1, sp.TInt)) == sp.pack(sp.set_type_expr(1, sp.TInt)))
    sp.verify(sp.pack(2) == sp.bytes('0x050002'))
    sp.verify(sp.pack(sp.set_type_expr(2, sp.TInt)) == sp.pack(sp.set_type_expr(2, sp.TInt)))
    sp.verify(sp.pack(3) == sp.bytes('0x050003'))
    sp.verify(sp.pack(sp.set_type_expr(3, sp.TInt)) == sp.pack(sp.set_type_expr(3, sp.TInt)))
    sp.verify(sp.pack(10) == sp.bytes('0x05000a'))
    sp.verify(sp.pack(sp.set_type_expr(10, sp.TInt)) == sp.pack(sp.set_type_expr(10, sp.TInt)))
    sp.verify(sp.pack(10000) == sp.bytes('0x0500909c01'))
    sp.verify(sp.pack(sp.set_type_expr(10000, sp.TInt)) == sp.pack(sp.set_type_expr(10000, sp.TInt)))
    sp.verify(sp.pack(1000000) == sp.bytes('0x050080897a'))
    sp.verify(sp.pack(sp.set_type_expr(1000000, sp.TInt)) == sp.pack(sp.set_type_expr(1000000, sp.TInt)))
    sp.verify(sp.pack(1000000000000) == sp.bytes('0x050080c0a8ca9a3a'))
    sp.verify(sp.pack(sp.set_type_expr(1000000000000, sp.TInt)) == sp.pack(sp.set_type_expr(1000000000000, sp.TInt)))
    sp.verify(sp.pack(0) == sp.bytes('0x050000'))
    sp.verify(sp.pack(sp.set_type_expr(0, sp.TInt)) == sp.pack(sp.set_type_expr(0, sp.TInt)))
    sp.verify(sp.pack(-1) == sp.bytes('0x050041'))
    sp.verify(sp.pack(sp.set_type_expr(-1, sp.TInt)) == sp.pack(sp.set_type_expr(-1, sp.TInt)))
    sp.verify(sp.pack(-2) == sp.bytes('0x050042'))
    sp.verify(sp.pack(sp.set_type_expr(-2, sp.TInt)) == sp.pack(sp.set_type_expr(-2, sp.TInt)))

sp.add_compilation_target("test", Contract())