import smartpy as sp

tstorage = sp.TUnit
tparameter = sp.TVariant(pack_lambda_record = sp.TUnit, pack_lambda_variant = sp.TUnit, pack_lambdas = sp.TUnit, pack_lambdas2 = sp.TUnit, run = sp.TUnit).layout((("pack_lambda_record", "pack_lambda_variant"), ("pack_lambdas", ("pack_lambdas2", "run"))))
tprivates = { }
tviews = { }
