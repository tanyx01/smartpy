open SmartML

module Contract = struct
  let%entry_point allSigned () =
    verify (data.notional <> (tez 0));
    verify (data.owner = sender);
    send data.counterparty data.notional;
    data.notional <- tez 0

  let%entry_point cancelSwap () =
    verify (data.notional <> (tez 0));
    verify (data.owner = sender);
    verify (data.epoch < now);
    send data.owner data.notional;
    data.notional <- tez 0

  let%entry_point knownSecret params =
    verify (data.notional <> (tez 0));
    verify (data.counterparty = sender);
    verify (data.hashedSecret = (blake2b params.secret));
    send data.counterparty data.notional;
    data.notional <- tez 0

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {counterparty = address; epoch = timestamp; hashedSecret = bytes; notional = mutez; owner = address}]
      ~storage:[%expr
                 {counterparty = address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi";
                  epoch = timestamp 50;
                  hashedSecret = bytes "0x1f98c84caf714d00ede5d23142bc166d84f8cd42adc18be22c3d47453853ea49";
                  notional = mutez 20;
                  owner = address "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP"}]
      [allSigned; cancelSwap; knownSecret]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())