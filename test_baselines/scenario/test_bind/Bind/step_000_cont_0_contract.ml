open SmartML

module Contract = struct
  let%entry_point call_f params =
    let%var __s1 = data.x + params in
    data.x <- __s1;
    let%var __s2 = data.x + 5 in
    data.x <- __s2;
    let%var __s3 = data.x + 32 in
    data.x <- __s3

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat; y = intOrNat}]
      ~storage:[%expr
                 {x = 12;
                  y = 0}]
      [call_f]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())