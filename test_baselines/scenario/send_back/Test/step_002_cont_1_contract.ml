open SmartML

module Contract = struct
  let%entry_point bounce () =
    send source amount

  let%entry_point bounce2 () =
    send source (tez 1);
    send source (amount - (tez 1))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [bounce; bounce2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())