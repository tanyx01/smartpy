Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 0 (Pair {} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair {} {}))))))
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_storage.tz 1
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_storage.json 25
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_storage.py 1
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_types.py 7
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_metadata.metadata_base.json 173
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_contract.tz 677
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_contract.json 873
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_contract.py 96
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_000_cont_0_contract.ml 101
Comment...
 h1: FA2_change_metadataFungible
Comment...
 h2: Change metadata
Comment...
 h3: Non admin cannot set metadata
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_004_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_004_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_004_cont_0_params.json 1
Executing set_metadata({'' : sp.bytes('0x687474703a2f2f6578616d706c652e636f6d')})...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sender == self.data.administrator : sp.TBool) (templates/fa2_lib.py, line 513)
Message: 'FA2_NOT_ADMIN'
 (templates/fa2_lib.py, line 531)
Comment...
 h3: Admin set metadata
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_006_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_006_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test2/FA2_change_metadataFungible/step_006_cont_0_params.json 1
Executing set_metadata({'' : sp.bytes('0x687474703a2f2f6578616d706c652e636f6d')})...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 0 (Pair {} (Pair {Elt "" 0x687474703a2f2f6578616d706c652e636f6d} (Pair {} (Pair {} {}))))))
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).metadata[''], sp.TBytes)) == sp.pack(sp.set_type_expr({'' : sp.bytes('0x687474703a2f2f6578616d706c652e636f6d')}[''], sp.TBytes))...
 OK
