Comment...
 h1: Hash Functions
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair 0x (Pair 0x (Pair 0x (Pair 0x (Pair 0x (Pair "tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr" 0x))))))
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_001_cont_0_storage.tz 1
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_001_cont_0_storage.json 25
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_001_cont_0_storage.py 1
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_001_cont_0_types.py 7
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_001_cont_0_contract.tz 50
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_001_cont_0_contract.json 83
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_001_cont_0_contract.py 27
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_001_cont_0_contract.ml 33
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_002_cont_0_params.py 1
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_002_cont_0_params.tz 1
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_002_cont_0_params.json 1
Executing new_value(sp.bytes('0x001234'))...
 -> (Pair 0xfffdfd672ff9075528f51a30408cf768a093d8c67fb3c5c8782dff49eab0724d (Pair 0xa3e1b4de203e0e5d02205d680485390711ef58bc4c3687b17277b62fde10a45d (Pair 0x61a706dfe2ddb1339d7b1d6f10c15a26786dcd1c99b743e0b0e351a6a168d99f (Pair 0x0110f7f5dc329eed3f1d0e8d1ae204cc58b2a790506acfb793200fc60ba22525de2c5147fff19128807352a3a33c44d673cbda3b9840973fdfe4ad6516a73a49 (Pair 0x6c487a73002ced6e865cc88e87fc0ead2dbb1d8b4b5b90aea7e3254e9060ca95 (Pair "tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr" 0x001234))))))
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_003_cont_0_params.py 1
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_003_cont_0_params.tz 1
 => test_baselines/scenario/test_hash_functions/HashFunctions/step_003_cont_0_params.json 1
Executing new_key(sp.resolve(sp.test_account("Robert").public_key))...
 -> (Pair 0xfffdfd672ff9075528f51a30408cf768a093d8c67fb3c5c8782dff49eab0724d (Pair 0xa3e1b4de203e0e5d02205d680485390711ef58bc4c3687b17277b62fde10a45d (Pair 0x61a706dfe2ddb1339d7b1d6f10c15a26786dcd1c99b743e0b0e351a6a168d99f (Pair 0x0110f7f5dc329eed3f1d0e8d1ae204cc58b2a790506acfb793200fc60ba22525de2c5147fff19128807352a3a33c44d673cbda3b9840973fdfe4ad6516a73a49 (Pair 0x6c487a73002ced6e865cc88e87fc0ead2dbb1d8b4b5b90aea7e3254e9060ca95 (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0x001234))))))
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0), sp.TRecord(b2b = sp.TBytes, keccak = sp.TBytes, s256 = sp.TBytes, s512 = sp.TBytes, sha3 = sp.TBytes, tz1 = sp.TKeyHash, v = sp.TBytes).layout(("b2b", ("keccak", ("s256", ("s512", ("sha3", ("tz1", "v"))))))))) == sp.pack(sp.set_type_expr(sp.record(b2b = sp.bytes('0xfffdfd672ff9075528f51a30408cf768a093d8c67fb3c5c8782dff49eab0724d'), keccak = sp.bytes('0xa3e1b4de203e0e5d02205d680485390711ef58bc4c3687b17277b62fde10a45d'), s256 = sp.bytes('0x61a706dfe2ddb1339d7b1d6f10c15a26786dcd1c99b743e0b0e351a6a168d99f'), s512 = sp.bytes('0x0110f7f5dc329eed3f1d0e8d1ae204cc58b2a790506acfb793200fc60ba22525de2c5147fff19128807352a3a33c44d673cbda3b9840973fdfe4ad6516a73a49'), sha3 = sp.bytes('0x6c487a73002ced6e865cc88e87fc0ead2dbb1d8b4b5b90aea7e3254e9060ca95'), tz1 = sp.resolve(sp.test_account("Robert").public_key_hash), v = sp.bytes('0x001234')), sp.TRecord(b2b = sp.TBytes, keccak = sp.TBytes, s256 = sp.TBytes, s512 = sp.TBytes, sha3 = sp.TBytes, tz1 = sp.TKeyHash, v = sp.TBytes).layout(("b2b", ("keccak", ("s256", ("s512", ("sha3", ("tz1", "v")))))))))...
 OK
