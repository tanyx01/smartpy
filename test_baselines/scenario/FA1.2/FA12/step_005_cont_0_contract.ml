open SmartML

module Contract = struct
  let%entry_point approve params =
    set_type params {spender = address; value = nat};
    if not (contains sender data.balances) then
      Map.set data.balances sender {approvals = (Map.make []); balance = (nat 0)};
    verify (not data.paused) ~msg:"FA1.2_Paused";
    verify (((Map.get ~default_value:(nat 0) (Map.get data.balances sender).approvals params.spender) = (nat 0)) || (params.value = (nat 0))) ~msg:"FA1.2_UnsafeAllowanceChange";
    Map.set (Map.get data.balances sender).approvals params.spender params.value

  let%entry_point burn params =
    set_type params {address = address; value = nat};
    verify (sender = data.administrator) ~msg:"FA1.2_NotAdmin";
    verify ((Map.get data.balances params.address).balance >= params.value) ~msg:"FA1.2_InsufficientBalance";
    (Map.get data.balances params.address).balance <- open_some (is_nat ((Map.get data.balances params.address).balance - params.value));
    data.totalSupply <- open_some (is_nat (data.totalSupply - params.value))

  let%entry_point getAdministrator params =
    set_type (fst params) unit;
    let%var __s1 = data.administrator in
    set_type (snd params) (contract address);
    transfer __s1 (tez 0) (snd params)

  let%entry_point getAllowance params =
    let __s2 =
      if contains (fst params).owner data.balances then
        result (Map.get ~default_value:(nat 0) (Map.get data.balances (fst params).owner).approvals (fst params).spender)
      else
        result (nat 0)
    in
    set_type (snd params) (contract nat);
    transfer __s2 (tez 0) (snd params)

  let%entry_point getBalance params =
    let __s3 =
      if contains (fst params) data.balances then
        result (Map.get data.balances (fst params)).balance
      else
        result (nat 0)
    in
    set_type (snd params) (contract nat);
    transfer __s3 (tez 0) (snd params)

  let%entry_point getTotalSupply params =
    set_type (fst params) unit;
    let%var __s4 = data.totalSupply in
    set_type (snd params) (contract nat);
    transfer __s4 (tez 0) (snd params)

  let%entry_point mint params =
    set_type params {address = address; value = nat};
    verify (sender = data.administrator) ~msg:"FA1.2_NotAdmin";
    if not (contains params.address data.balances) then
      Map.set data.balances params.address {approvals = (Map.make []); balance = (nat 0)};
    (Map.get data.balances params.address).balance <- (Map.get data.balances params.address).balance + params.value;
    data.totalSupply <- data.totalSupply + params.value

  let%entry_point setAdministrator params =
    set_type params address;
    verify (sender = data.administrator) ~msg:"FA1.2_NotAdmin";
    data.administrator <- params

  let%entry_point setPause params =
    set_type params bool;
    verify (sender = data.administrator) ~msg:"FA1.2_NotAdmin";
    data.paused <- params

  let%entry_point transfer params =
    set_type params {from_ = address; to_ = address; value = nat};
    verify ((sender = data.administrator) || ((not data.paused) && ((params.from_ = sender) || ((Map.get (Map.get data.balances params.from_).approvals sender) >= params.value)))) ~msg:"FA1.2_NotAllowed";
    if not (contains params.from_ data.balances) then
      Map.set data.balances params.from_ {approvals = (Map.make []); balance = (nat 0)};
    if not (contains params.to_ data.balances) then
      Map.set data.balances params.to_ {approvals = (Map.make []); balance = (nat 0)};
    verify ((Map.get data.balances params.from_).balance >= params.value) ~msg:"FA1.2_InsufficientBalance";
    (Map.get data.balances params.from_).balance <- open_some (is_nat ((Map.get data.balances params.from_).balance - params.value));
    (Map.get data.balances params.to_).balance <- (Map.get data.balances params.to_).balance + params.value;
    if (params.from_ <> sender) && (not (sender = data.administrator)) then
      Map.set (Map.get data.balances params.from_).approvals sender (open_some (is_nat ((Map.get (Map.get data.balances params.from_).approvals sender) - params.value)))

  let%entry_point update_metadata params =
    verify (sender = data.administrator) ~msg:"FA1.2_NotAdmin";
    Map.set data.metadata params.key params.value

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {administrator = address; balances = big_map address {approvals = map address nat; balance = nat}; metadata = big_map string bytes; paused = bool; token_metadata = big_map nat {token_id = nat; token_info = map string bytes}; totalSupply = nat}]
      ~storage:[%expr
                 {administrator = address "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w";
                  balances = Map.make [];
                  metadata = Map.make [("", bytes "0x697066733a2f2f516d616941556a3146464e4759547538724c426a633365654e3963534b7761463845474d424e446d687a504e4664")];
                  paused = false;
                  token_metadata = Map.make [(nat 0, {token_id = nat 0; token_info = Map.make [("decimals", bytes "0x3138"); ("icon", bytes "0x68747470733a2f2f736d61727470792e696f2f7374617469632f696d672f6c6f676f2d6f6e6c792e737667"); ("name", bytes "0x4d7920477265617420546f6b656e"); ("symbol", bytes "0x4d4754")]})];
                  totalSupply = nat 0}]
      [approve; burn; getAdministrator; getAllowance; getBalance; getTotalSupply; mint; setAdministrator; setPause; transfer; update_metadata]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())