import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(active = sp.TBool, admin = sp.TAddress, max_amount = sp.TNat, token = sp.TAddress).layout(("active", ("admin", ("max_amount", "token")))))
    self.init(active = True,
              admin = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
              max_amount = 10,
              token = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'))

  @sp.entry_point
  def configure(self, params):
    sp.verify(self.data.admin == sp.sender, 'Privileged operation')
    self.data = params

  @sp.entry_point
  def request_tokens(self, params):
    sp.set_type(params, sp.TSet(sp.TAddress))
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = params.elements().map(sp.build_lambda(lambda _x2: sp.record(to_ = _x2, token_id = 0, amount = self.data.max_amount))))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some(message = 'Incompatible token interface'))

sp.add_compilation_target("test", Contract())