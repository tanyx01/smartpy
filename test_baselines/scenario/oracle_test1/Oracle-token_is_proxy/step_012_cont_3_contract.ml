open SmartML

module Contract = struct
  let%entry_point cancel_request params =
    with match_record(data, "data") as data:
      verify ((sender = data.setup.escrow) || (sender = data.setup.admin)) ~msg:"OracleSenderIsNotEscrowOrAdmin";
      match_pair_oracle_237_fst, match_pair_oracle_237_snd = match_tuple(get_and_update {client = params.client; client_request_id = params.client_request_id} None data.reverse_requests, "match_pair_oracle_237_fst", "match_pair_oracle_237_snd")
      data.reverse_requests <- match_pair_oracle_237_snd;
      Map.delete data.requests (open_some ~message:"OracleRequestUnknown" match_pair_oracle_237_fst)

  let%entry_point create_request params =
    set_type params {amount = nat; request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}};
    amount_oracle_181, request_oracle_181 = match_record(params, "amount", "request")
    with match_record(data, "data") as data:
      verify (sender = data.setup.escrow) ~msg:"OracleNotEscrow";
      verify data.setup.active ~msg:"OracleInactive";
      ticket_oracle_186_data, ticket_oracle_186_copy = match_tuple(read_ticket_raw request_oracle_181, "ticket_oracle_186_data", "ticket_oracle_186_copy")
      ticket_oracle_186_ticketer, ticket_oracle_186_content, ticket_oracle_186_amount = match_tuple(ticket_oracle_186_data, "ticket_oracle_186_ticketer", "ticket_oracle_186_content", "ticket_oracle_186_amount")
      verify (data.setup.min_amount <= amount_oracle_181) ~msg:"OracleAmountBelowMin";
      verify ((add_seconds now (data.setup.min_cancel_timeout * (int 60))) <= ticket_oracle_186_content.cancel_timeout) ~msg:"OracleTimeoutBelowMinTimeout";
      verify ((add_seconds now (data.setup.min_fulfill_timeout * (int 60))) <= ticket_oracle_186_content.fulfill_timeout) ~msg:"OracleTimeoutBelowMinTimeout";
      let%mutable compute_oracle_192 = {client = ticket_oracle_186_ticketer; client_request_id = ticket_oracle_186_content.client_request_id} in ();
      verify (not (contains compute_oracle_192 data.reverse_requests)) ~msg:"OracleRequestKeyAlreadyKnown";
      Map.set data.reverse_requests compute_oracle_192 data.next_id;
      Map.set data.requests data.next_id ticket_oracle_186_copy;
      data.next_id <- data.next_id + (nat 1)

  let%entry_point fulfill_request params =
    with match_record(data, "modify_record_oracle_209") as modify_record_oracle_209:
      verify (sender = modify_record_oracle_209.setup.admin) ~msg:"OracleNotAdmin";
      match_pair_oracle_212_fst, match_pair_oracle_212_snd = match_tuple(get_and_update params.request_id None modify_record_oracle_209.requests, "match_pair_oracle_212_fst", "match_pair_oracle_212_snd")
      modify_record_oracle_209.requests <- match_pair_oracle_212_snd;
      ticket_oracle_214_data, ticket_oracle_214_copy = match_tuple(read_ticket_raw (open_some ~message:"OracleRequestUnknown" match_pair_oracle_212_fst), "ticket_oracle_214_data", "ticket_oracle_214_copy")
      ticket_oracle_214_ticketer, ticket_oracle_214_content, ticket_oracle_214_amount = match_tuple(ticket_oracle_214_data, "ticket_oracle_214_ticketer", "ticket_oracle_214_content", "ticket_oracle_214_amount")
      if params.force then
        ();
      let%mutable ticket_221 = (ticket {client = ticket_oracle_214_ticketer; client_request_id = ticket_oracle_214_content.client_request_id; result = params.result; tag = "OracleResult"} (nat 1)) in ();
      transfer {request = ticket_oracle_214_copy; result = ticket_221} (tez 0) (open_some (contract {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}} modify_record_oracle_209.setup.escrow , entry_point='fulfill_request'));
      Map.delete modify_record_oracle_209.reverse_requests {client = ticket_oracle_214_ticketer; client_request_id = ticket_oracle_214_content.client_request_id}

  let%entry_point setup params =
    with match_record(data, "modify_record_oracle_200") as modify_record_oracle_200:
      verify (sender = modify_record_oracle_200.setup.admin) ~msg:"OracleNotAdmin";
      modify_record_oracle_200.setup <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {next_id = nat; requests = big_map nat (ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}); reverse_requests = big_map {client = address; client_request_id = nat} nat; setup = {active = bool; admin = address; escrow = address; min_amount = nat; min_cancel_timeout = int; min_fulfill_timeout = int}}]
      ~storage:[%expr
                 {next_id = nat 0;
                  requests = Map.make [];
                  reverse_requests = Map.make [];
                  setup = {active = true; admin = address "tz1R2pWakgAQYaFU9aHbyzYtDPyP3Bds7BTi"; escrow = address "KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H"; min_amount = nat 0; min_cancel_timeout = int 5; min_fulfill_timeout = int 5}}]
      [cancel_request; create_request; fulfill_request; setup]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())