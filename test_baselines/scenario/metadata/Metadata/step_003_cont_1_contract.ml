open SmartML

module Contract = struct
  let%entry_point a_lazy_entry_point () =
    ()

  let%entry_point change_metadata params =
    Map.set data.metadata "" params

  let%entry_point incr () =
    data.x <- data.x + 1

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {metadata = big_map string bytes; x = intOrNat}]
      ~storage:[%expr
                 {metadata = Map.make [("", bytes "0x697066733a2f2f516d65394c3479365a76507751746169734e4754554537566a55375052746e4a4673384e6a4e797a744533644754")];
                  x = 1}]
      [a_lazy_entry_point; change_metadata; incr]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())