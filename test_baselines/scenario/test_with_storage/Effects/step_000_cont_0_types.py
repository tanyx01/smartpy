import smartpy as sp

tstorage = sp.TRecord(x = sp.TInt).layout("x")
tparameter = sp.TUnit
tprivates = { }
tviews = { "get_set_f": ((), sp.TLambda(sp.TInt, sp.TInt, with_storage="read-write")) }
