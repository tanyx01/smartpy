import smartpy as sp

tstorage = sp.TRecord(x = sp.TInt).layout("x")
tparameter = sp.TVariant(run = sp.TRecord(f = sp.TLambda(sp.TInt, sp.TInt, with_storage="read-write"), x = sp.TInt).layout(("f", "x")), save = sp.TRecord(f = sp.TLambda(sp.TInt, sp.TInt, with_storage="read-write"), x = sp.TInt).layout(("f", "x"))).layout(("run", "save"))
tprivates = { }
tviews = { }
