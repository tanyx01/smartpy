open SmartML

module Contract = struct
  let%entry_point call params =
    let%mutable compute_test_with_storage_44 = view("get_set_f", (), params.lib, lambda int int ~with_storage:"read-write") in ();
    let%mutable compute_test_with_storage_45 = (contract {f = lambda int int ~with_storage:"read-write"; x = int} params.main , entry_point='run') in ();
    transfer {f = (open_some compute_test_with_storage_44); x = params.x} (tez 0) (open_some compute_test_with_storage_45)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [call]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())