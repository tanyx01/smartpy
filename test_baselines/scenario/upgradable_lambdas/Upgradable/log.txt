Comment...
 h1: Upgradable
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair { DUP; UNPACK (pair nat nat); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; CDR; SWAP; UNPACK (pair nat nat); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; CAR; ADD } 0)
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_001_cont_0_storage.tz 1
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_001_cont_0_storage.json 17
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_001_cont_0_storage.py 1
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_001_cont_0_types.py 7
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_001_cont_0_contract.tz 24
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_001_cont_0_contract.json 41
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_001_cont_0_contract.py 17
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_001_cont_0_contract.ml 23
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_002_cont_0_params.py 1
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_002_cont_0_params.tz 1
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_002_cont_0_params.json 1
Executing calc(sp.pack(sp.record(x = 1, y = 2)))...
 -> (Pair { DUP; UNPACK (pair nat nat); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; CDR; SWAP; UNPACK (pair nat nat); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; CAR; ADD } 3)
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_003_cont_0_params.py 1
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_003_cont_0_params.tz 1
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_003_cont_0_params.json 18
Executing updateLogic(sp.build_lambda(lambda _x2: (sp.unpack(_x2, sp.TRecord(x = sp.TNat, y = sp.TNat, z = sp.TNat).layout(("x", ("y", "z")))).open_some(message = 'Cannot UNPACK').x + sp.unpack(_x2, sp.TRecord(x = sp.TNat, y = sp.TNat, z = sp.TNat).layout(("x", ("y", "z")))).open_some(message = 'Cannot UNPACK').y) + sp.unpack(_x2, sp.TRecord(x = sp.TNat, y = sp.TNat, z = sp.TNat).layout(("x", ("y", "z")))).open_some(message = 'Cannot UNPACK').z))...
 -> (Pair { DUP; UNPACK (pair nat (pair nat nat)); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; GET 4; SWAP; DUP; DUG 2; UNPACK (pair nat (pair nat nat)); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; GET 3; DIG 2; UNPACK (pair nat (pair nat nat)); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; CAR; ADD; ADD } 3)
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_004_cont_0_params.py 1
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_004_cont_0_params.tz 1
 => test_baselines/scenario/upgradable_lambdas/Upgradable/step_004_cont_0_params.json 1
Executing calc(sp.pack(sp.record(x = 1, y = 2, z = 3)))...
 -> (Pair { DUP; UNPACK (pair nat (pair nat nat)); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; GET 4; SWAP; DUP; DUG 2; UNPACK (pair nat (pair nat nat)); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; GET 3; DIG 2; UNPACK (pair nat (pair nat nat)); IF_NONE { PUSH string "Cannot UNPACK"; FAILWITH } {}; CAR; ADD; ADD } 6)
