import smartpy as sp

tstorage = sp.TRecord(last_known_balances = sp.TBigMap(sp.TAddress, sp.TMap(sp.TPair(sp.TAddress, sp.TNat), sp.TNat))).layout("last_known_balances")
tparameter = sp.TVariant(receive_balances = sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))).layout("receive_balances")
tprivates = { }
tviews = { }
