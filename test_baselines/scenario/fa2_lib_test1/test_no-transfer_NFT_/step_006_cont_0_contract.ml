open SmartML

module Contract = struct
  let%entry_point balance_of params =
    set_type params {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}};
    set_type params.requests (list {owner = address; token_id = nat});
    transfer (map (fun _x0 -> verify (contains _x0.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
result {request = _x0; balance = (eif ((Map.get data.ledger _x0.token_id) = _x0.owner) (nat 1) (nat 0))}) params.requests) (tez 0) params.callback

  let%entry_point transfer params =
    set_type params (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}});
    failwith "FA2_TX_DENIED"

  let%entry_point update_operators params =
    set_type params (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat}));
    failwith "FA2_OPERATORS_UNSUPPORTED"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {last_token_id = nat; ledger = big_map nat address; metadata = big_map string bytes; token_metadata = big_map nat {token_id = nat; token_info = map string bytes}}]
      ~storage:[%expr
                 {last_token_id = nat 3;
                  ledger = Map.make [(nat 0, address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"); (nat 1, address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"); (nat 2, address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi")];
                  metadata = Map.make [("", bytes "0x697066733a2f2f6578616d706c65")];
                  token_metadata = Map.make [(nat 0, {token_id = nat 0; token_info = Map.make [("decimals", bytes "0x31"); ("name", bytes "0x546f6b656e205a65726f"); ("symbol", bytes "0x546f6b30")]}); (nat 1, {token_id = nat 1; token_info = Map.make [("decimals", bytes "0x31"); ("name", bytes "0x546f6b656e204f6e65"); ("symbol", bytes "0x546f6b31")]}); (nat 2, {token_id = nat 2; token_info = Map.make [("decimals", bytes "0x31"); ("name", bytes "0x546f6b656e2054776f"); ("symbol", bytes "0x546f6b32")]})]}]
      [balance_of; transfer; update_operators]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())