Comment...
 h1: test_no-transfer_NFT_
Table Of Contents

 test_no-transfer_NFT_
# Accounts
# FA2 with NoTransfer policy
# Alice cannot transfer: FA2_TX_DENIED
# Admin cannot transfer alice's token: FA2_TX_DENIED
# Alice cannot add operator: FA2_OPERATORS_UNSUPPORTED
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Bob")])...
 => [sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edsk3SUiUcR33jiBmxRDke8MKfd18dxmq2fUbZWZFYoiEsTkpAz5F7')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edsk34XphRR5Rs6EeAGrxktxAhstbwPr5YZ4m7RMzjaed3n9g5JcBB')), sp.record(seed = 'Bob', address = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), public_key = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT'), public_key_hash = sp.key_hash('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), secret_key = sp.secret_key('edsk3Rg6sSnow8KiHdHbWBZVF4Xui8ucyxmujHeA35HaAgvwKuXWio'))]
Comment...
 h2: FA2 with NoTransfer policy
Comment...
 p: No transfer are allowed.
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair 3 (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_storage.tz 1
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_storage.json 75
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_sizes.csv 2
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_storage.py 1
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_types.py 7
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_metadata.metadata_base.json 150
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_contract.tz 81
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_contract.json 203
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_contract.py 30
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_006_cont_0_contract.ml 33
Comment...
 h2: Alice cannot transfer: FA2_TX_DENIED
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_008_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_008_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_008_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Bob").address), token_id = 0, amount = 1)]))]))...
 -> --- Expected failure in transaction --- Failure: 'FA2_TX_DENIED'
 (templates/fa2_lib.py, line 288)
Comment...
 h2: Admin cannot transfer alice's token: FA2_TX_DENIED
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_010_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_010_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_010_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]))...
 -> --- Expected failure in transaction --- Failure: 'FA2_TX_DENIED'
 (templates/fa2_lib.py, line 288)
Comment...
 h2: Alice cannot add operator: FA2_OPERATORS_UNSUPPORTED
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_012_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_012_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/test_no-transfer_NFT_/step_012_cont_0_params.json 11
Executing update_operators(sp.list([variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))]))...
 -> --- Expected failure in transaction --- Failure: 'FA2_OPERATORS_UNSUPPORTED'
 (templates/fa2_lib.py, line 255)
