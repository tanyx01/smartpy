open SmartML

module Contract = struct
  let%entry_point balance_of params =
    set_type params {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}};
    set_type params.requests (list {owner = address; token_id = nat});
    transfer (map (fun _x0 -> verify (contains _x0.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
result {request = _x0; balance = (eif ((Map.get data.ledger _x0.token_id) = _x0.owner) (nat 1) (nat 0))}) params.requests) (tez 0) params.callback

  let%entry_point burn params =
    set_type params (list {amount = nat; from_ = address; token_id = nat});
    verify true ~msg:"FA2_TX_DENIED";
    List.iter (fun action ->
      verify (contains action.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
      verify ((sender = action.from_) || (contains {owner = action.from_; operator = sender; token_id = action.token_id} data.operators)) ~msg:"FA2_NOT_OPERATOR";
      if action.amount > (nat 0) then
        (
          verify ((action.amount = (nat 1)) && ((Map.get data.ledger action.token_id) = action.from_)) ~msg:"FA2_INSUFFICIENT_BALANCE";
          Map.delete data.ledger action.token_id;
          Map.delete data.token_metadata action.token_id
        )
    ) params

  let%entry_point mint params =
    set_type params (list {metadata = map string bytes; to_ = address});
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    List.iter (fun action ->
      let%mutable compute_fa2_lib_601i = data.last_token_id in ();
      Map.set data.token_metadata compute_fa2_lib_601i {token_id = compute_fa2_lib_601i; token_info = action.metadata};
      Map.set data.ledger compute_fa2_lib_601i action.to_;
      data.last_token_id <- data.last_token_id + (nat 1)
    ) params

  let%entry_point set_administrator params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    data.administrator <- params

  let%entry_point set_metadata params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    data.metadata <- params

  let%entry_point transfer params =
    set_type params (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}});
    List.iter (fun transfer ->
      List.iter (fun tx ->
        verify (contains tx.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
        verify ((sender = transfer.from_) || (contains {owner = transfer.from_; operator = sender; token_id = tx.token_id} data.operators)) ~msg:"FA2_NOT_OPERATOR";
        if tx.amount > (nat 0) then
          (
            verify ((tx.amount = (nat 1)) && ((Map.get data.ledger tx.token_id) = transfer.from_)) ~msg:"FA2_INSUFFICIENT_BALANCE";
            Map.set data.ledger tx.token_id tx.to_
          )
      ) transfer.txs
    ) params

  let%entry_point update_operators params =
    set_type params (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat}));
    List.iter (fun action ->
      match action with
        | `add_operator add_operator ->
          verify (add_operator.owner = sender) ~msg:"FA2_NOT_OWNER";
          Map.set data.operators add_operator ()
        | `remove_operator remove_operator ->
          verify (remove_operator.owner = sender) ~msg:"FA2_NOT_OWNER";
          Map.delete data.operators remove_operator

    ) params

  let%entry_point withdraw_mutez params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    send params.destination params.amount

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {administrator = address; last_token_id = nat; ledger = big_map nat address; metadata = big_map string bytes; operators = big_map {operator = address; owner = address; token_id = nat} unit; token_metadata = big_map nat {token_id = nat; token_info = map string bytes}}]
      ~storage:[%expr
                 {administrator = address "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w";
                  last_token_id = nat 0;
                  ledger = Map.make [];
                  metadata = Map.make [("", bytes "0x697066733a2f2f6578616d706c65")];
                  operators = Map.make [];
                  token_metadata = Map.make []}]
      [balance_of; burn; mint; set_administrator; set_metadata; transfer; update_operators; withdraw_mutez]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())