Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 3 (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_storage.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_storage.json 87
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_storage.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_types.py 7
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_metadata.metadata_base.json 174
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_contract.tz 564
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_contract.json 762
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_contract.py 83
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_000_cont_0_contract.ml 90
Comment...
 h1: FA2_burn_NFT
Comment...
 h2: Burn entrypoint
Comment...
 h3: Cannot burn others tokens
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_004_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_004_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_004_cont_0_params.json 1
Executing burn(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))...
 -> --- Expected failure in transaction --- Wrong condition: ((sp.sender == action.from_) | (self.data.operators.contains(sp.record(owner = action.from_, operator = sp.sender, token_id = action.token_id))) : sp.TBool) (templates/fa2_lib.py, line 111)
Message: 'FA2_NOT_OPERATOR'
 (templates/fa2_lib.py, line 110)
Comment...
 h3: Owner burns his nft tokens
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_006_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_006_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_006_cont_0_params.json 1
Executing burn(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 3 (Pair {Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} {Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
Verifying ~ (sp.contract_data(0).ledger.contains(0))...
 OK
Verifying ~ (sp.contract_data(0).token_metadata.contains(0))...
 OK
Comment...
 h3: Burn with insufficent balance
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_010_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_010_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_010_cont_0_params.json 1
Executing burn(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), token_id = 1, amount = 43)]))...
 -> --- Expected failure in transaction --- Wrong condition: ((action.amount == 1) & (self.data.ledger[action.token_id] == action.from_) : sp.TBool) (templates/fa2_lib.py, line 702)
Message: 'FA2_INSUFFICIENT_BALANCE'
 (templates/fa2_lib.py, line 701)
Comment...
 h3: Operator can burn on behalf of the owner
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_012_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_012_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_012_cont_0_params.json 11
Executing update_operators(sp.list([variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 1))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 3 (Pair {Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" 1)) Unit} {Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_013_cont_0_params.py 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_013_cont_0_params.tz 1
 => test_baselines/scenario/fa2_lib_test1/FA2_burn_NFT/step_013_cont_0_params.json 1
Executing burn(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), token_id = 1, amount = 0)]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 3 (Pair {Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" 1)) Unit} {Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
