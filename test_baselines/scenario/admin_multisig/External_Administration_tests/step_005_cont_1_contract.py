import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(active = sp.TBool, admin = sp.TAddress).layout(("active", "admin")))
    self.init(active = False,
              admin = sp.address('tz1UyQDepgtUBnWjyzzonqeDwaiWoQzRKSP5'))

  @sp.entry_point
  def administrate(self, params):
    sp.verify(sp.sender == self.data.admin, 'NOT ADMIN')
    sp.for action in sp.unpack(params, sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress).layout(("changeActive", "changeAdmin")))).open_some(message = 'Actions are invalid'):
      with action.match_cases() as arg:
        with arg.match('changeActive') as changeActive:
          self.data.active = changeActive
        with arg.match('changeAdmin') as changeAdmin:
          self.data.admin = changeAdmin


  @sp.entry_point
  def verifyActive(self):
    sp.verify(self.data.active, 'NOT ACTIVE')

sp.add_compilation_target("test", Contract())