Comment...
 h1: Create Contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair { PUSH nat 15; SWAP; PAIR; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter (pair %myEntryPoint (int %x) (nat %y)); storage (pair (int %a) (nat %b)); code { UNPAIR; SWAP; DUP; CAR; DUP 3; CAR; ADD; UPDATE 1; DUP; CDR; DIG 2; CDR; ADD; UPDATE 2; NIL operation; PAIR }}; PAIR } None)
 => test_baselines/scenario/create_contract/Create/step_001_cont_0_storage.tz 1
 => test_baselines/scenario/create_contract/Create/step_001_cont_0_storage.json 49
 => test_baselines/scenario/create_contract/Create/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario/create_contract/Create/step_001_cont_0_storage.py 1
 => test_baselines/scenario/create_contract/Create/step_001_cont_0_types.py 7
 => test_baselines/scenario/create_contract/Create/step_001_cont_0_contract.tz 312
 => test_baselines/scenario/create_contract/Create/step_001_cont_0_contract.json 436
 => test_baselines/scenario/create_contract/Create/step_001_cont_0_contract.py 46
 => test_baselines/scenario/create_contract/Create/step_001_cont_0_contract.ml 49
 => test_baselines/scenario/create_contract/Create/step_002_cont_0_params.py 1
 => test_baselines/scenario/create_contract/Create/step_002_cont_0_params.tz 1
 => test_baselines/scenario/create_contract/Create/step_002_cont_0_params.json 1
Executing create1(sp.record())...
 -> (Pair { PUSH nat 15; SWAP; PAIR; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter (pair %myEntryPoint (int %x) (nat %y)); storage (pair (int %a) (nat %b)); code { UNPAIR; SWAP; DUP; CAR; DUP 3; CAR; ADD; UPDATE 1; DUP; CDR; DIG 2; CDR; ADD; UPDATE 2; NIL operation; PAIR }}; PAIR } (Some "KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU"))
  + Create Contract(address: KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU, baker: sp.none)sp.record(a = 12, b = 15)
Creating contract KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU
 -> (Pair 12 15)
 => test_baselines/scenario/create_contract/Create/step_002_cont_Dyn_0_storage.tz 1
 => test_baselines/scenario/create_contract/Create/step_002_cont_Dyn_0_storage.json 1
 => test_baselines/scenario/create_contract/Create/step_002_cont_Dyn_0_sizes.csv 2
 => test_baselines/scenario/create_contract/Create/step_002_cont_Dyn_0_storage.py 1
 => test_baselines/scenario/create_contract/Create/step_002_cont_Dyn_0_types.py 7
 => test_baselines/scenario/create_contract/Create/step_002_cont_Dyn_0_contract.tz 24
 => test_baselines/scenario/create_contract/Create/step_002_cont_Dyn_0_contract.json 27
 => test_baselines/scenario/create_contract/Create/step_002_cont_Dyn_0_contract.py 14
 => test_baselines/scenario/create_contract/Create/step_002_cont_Dyn_0_contract.ml 21
 => test_baselines/scenario/create_contract/Create/step_003_cont_0_params.py 1
 => test_baselines/scenario/create_contract/Create/step_003_cont_0_params.tz 1
 => test_baselines/scenario/create_contract/Create/step_003_cont_0_params.json 1
Executing create2(sp.record())...
 -> (Pair { PUSH nat 15; SWAP; PAIR; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter (pair %myEntryPoint (int %x) (nat %y)); storage (pair (int %a) (nat %b)); code { UNPAIR; SWAP; DUP; CAR; DUP 3; CAR; ADD; UPDATE 1; DUP; CDR; DIG 2; CDR; ADD; UPDATE 2; NIL operation; PAIR }}; PAIR } (Some "KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU"))
  + Create Contract(address: KT1Tezooo1zzSmartPyzzDYNAMiCzztcr8AZ, baker: sp.none)sp.record(a = 12, b = 15)
  + Create Contract(address: KT1Tezooo2zzSmartPyzzDYNAMiCzzxyHfG9, baker: sp.none)sp.record(a = 12, b = 16)
Creating contract KT1Tezooo1zzSmartPyzzDYNAMiCzztcr8AZ
 -> (Pair 12 15)
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_1_storage.tz 1
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_1_storage.json 1
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_1_sizes.csv 2
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_1_storage.py 1
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_1_types.py 7
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_1_contract.tz 24
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_1_contract.json 27
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_1_contract.py 14
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_1_contract.ml 21
Creating contract KT1Tezooo2zzSmartPyzzDYNAMiCzzxyHfG9
 -> (Pair 12 16)
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_2_storage.tz 1
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_2_storage.json 1
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_2_sizes.csv 2
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_2_storage.py 1
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_2_types.py 7
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_2_contract.tz 24
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_2_contract.json 27
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_2_contract.py 14
 => test_baselines/scenario/create_contract/Create/step_003_cont_Dyn_2_contract.ml 21
 => test_baselines/scenario/create_contract/Create/step_004_cont_0_params.py 1
 => test_baselines/scenario/create_contract/Create/step_004_cont_0_params.tz 1
 => test_baselines/scenario/create_contract/Create/step_004_cont_0_params.json 1
Executing create4(sp.list([1, 2]))...
 -> (Pair { PUSH nat 15; SWAP; PAIR; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter (pair %myEntryPoint (int %x) (nat %y)); storage (pair (int %a) (nat %b)); code { UNPAIR; SWAP; DUP; CAR; DUP 3; CAR; ADD; UPDATE 1; DUP; CDR; DIG 2; CDR; ADD; UPDATE 2; NIL operation; PAIR }}; PAIR } (Some "KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU"))
  + Create Contract(address: KT1Tezooo3zzSmartPyzzDYNAMiCzzvqsJQk, baker: sp.none)sp.record(a = 1, b = 15)
  + Create Contract(address: KT1Tezooo4zzSmartPyzzDYNAMiCzzywTMhC, baker: sp.none)sp.record(a = 2, b = 15)
Creating contract KT1Tezooo3zzSmartPyzzDYNAMiCzzvqsJQk
 -> (Pair 1 15)
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_3_storage.tz 1
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_3_storage.json 1
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_3_sizes.csv 2
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_3_storage.py 1
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_3_types.py 7
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_3_contract.tz 24
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_3_contract.json 27
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_3_contract.py 14
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_3_contract.ml 21
Creating contract KT1Tezooo4zzSmartPyzzDYNAMiCzzywTMhC
 -> (Pair 2 15)
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_4_storage.tz 1
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_4_storage.json 1
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_4_sizes.csv 2
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_4_storage.py 1
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_4_types.py 7
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_4_contract.tz 24
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_4_contract.json 27
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_4_contract.py 14
 => test_baselines/scenario/create_contract/Create/step_004_cont_Dyn_4_contract.ml 21
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 -> missing storage
 => test_baselines/scenario/create_contract/Create/step_005_cont_1_sizes.csv 2
 => test_baselines/scenario/create_contract/Create/step_005_cont_1_types.py 7
 => test_baselines/scenario/create_contract/Create/step_005_cont_1_contract.tz 24
 => test_baselines/scenario/create_contract/Create/step_005_cont_1_contract.json 27
 => test_baselines/scenario/create_contract/Create/step_005_cont_1_contract.py 12
 => test_baselines/scenario/create_contract/Create/step_005_cont_1_contract.ml 18
 => test_baselines/scenario/create_contract/Create/step_007_cont_Dyn_0_params.py 1
 => test_baselines/scenario/create_contract/Create/step_007_cont_Dyn_0_params.tz 1
 => test_baselines/scenario/create_contract/Create/step_007_cont_Dyn_0_params.json 1
Executing myEntryPoint(sp.record(x = 1, y = 16))...
 -> (Pair 13 31)
Verifying sp.contract_data(Dyn_0).a == 13...
 OK
Verifying sp.contract_balance(Dyn_0) == sp.tez(0)...
 OK
 => test_baselines/scenario/create_contract/Create/step_010_cont_Dyn_0_params.py 1
 => test_baselines/scenario/create_contract/Create/step_010_cont_Dyn_0_params.tz 1
 => test_baselines/scenario/create_contract/Create/step_010_cont_Dyn_0_params.json 1
Executing myEntryPoint(sp.record(x = 1, y = 15))...
 -> (Pair 14 46)
Verifying sp.contract_data(Dyn_0).a == 14...
 OK
Verifying sp.contract_balance(Dyn_0) == sp.tez(2)...
 OK
Computing sp.resolve(sp.contract_baker(Dyn_0))...
 => sp.none
Computing sp.address('KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU')...
 => sp.address('KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU')
