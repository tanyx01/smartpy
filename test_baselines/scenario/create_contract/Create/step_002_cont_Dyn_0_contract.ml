open SmartML

module Contract = struct
  let%entry_point myEntryPoint params =
    data.a <- data.a + params.x;
    data.b <- data.b + params.y

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = int; b = nat}]
      ~storage:[%expr
                 {a = int 12;
                  b = nat 15}]
      [myEntryPoint]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())