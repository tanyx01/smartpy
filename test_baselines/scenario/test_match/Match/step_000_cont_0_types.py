import smartpy as sp

tstorage = sp.TRecord(m = sp.TMap(sp.TString, sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat).layout(("a", "b"))), out = sp.TString).layout(("m", "out"))
tparameter = sp.TVariant(ep1 = sp.TPair(sp.TString, sp.TIntOrNat), ep2 = sp.TTuple(sp.TString, sp.TIntOrNat, sp.TBool), ep3 = sp.TRecord(x = sp.TString, y = sp.TInt, z = sp.TBool).layout(("x", ("y", "z"))), ep4 = sp.TRecord(x01 = sp.TInt, x02 = sp.TKey, x03 = sp.TString, x04 = sp.TTimestamp, x05 = sp.TBytes, x06 = sp.TAddress, x07 = sp.TBool, x08 = sp.TKeyHash, x09 = sp.TSignature, x10 = sp.TMutez).layout(("x01", ("x02", ("x03", ("x04", ("x05", ("x06", ("x07", ("x08", ("x09", "x10")))))))))), ep5 = sp.TTuple(sp.TIntOrNat, sp.TIntOrNat, sp.TIntOrNat, sp.TIntOrNat), ep6 = sp.TTuple(sp.TIntOrNat, sp.TIntOrNat, sp.TInt, sp.TIntOrNat), ep7 = sp.TUnit).layout((("ep1", ("ep2", "ep3")), (("ep4", "ep5"), ("ep6", "ep7"))))
tprivates = { }
tviews = { }
