open SmartML

module Contract = struct
  let%entry_point handle params =
    List.iter (fun operation ->
      let%mutable compute_sapling2_16 = (open_some (sapling_verify_update data.ledger operation.transaction)) in ();
      bound_data, amount, ledger = match_tuple(compute_sapling2_16, "bound_data", "amount", "ledger")
      data.ledger <- ledger;
      let%mutable amount_tez = (mutez abs amount) in ();
      if amount > (int 0) then
        transfer () amount_tez (implicit_account (open_some operation.key))
      else
        (
          verify (not (is_some operation.key));
          verify (amount = amount_tez)
        )
    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {ledger = sapling_state 8}]
      ~storage:[%expr
                 {ledger = []}]
      [handle]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())