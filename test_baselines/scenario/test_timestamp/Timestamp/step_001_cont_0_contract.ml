open SmartML

module Contract = struct
  let%entry_point ep () =
    data.out <- now > (add_seconds now (int 1));
    verify (((add_seconds now (int 12)) - now) = (int 12));
    verify ((now - (add_seconds now (int 12))) = (int ((-12))));
    verify ((now - (add_seconds now (int 12))) = (int ((-12))));
    verify ((add now (int 500)) = timestamp 1500);
    verify ((add (int 500) now) = timestamp 1500);
    data.next <- add_seconds now (int 86400)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {next = timestamp; out = bool}]
      ~storage:[%expr
                 {next = timestamp 0;
                  out = false}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())