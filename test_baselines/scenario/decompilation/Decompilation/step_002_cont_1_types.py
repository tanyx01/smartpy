import smartpy as sp

tstorage = sp.TRecord(Left = sp.TRecord(Left = sp.TInt, Right = sp.TInt).layout(("Left", "Right")), Right = sp.TMap(sp.TInt, sp.TInt)).layout(("Left", "Right"))
tparameter = sp.TVariant(fifo = sp.TVariant(Left = sp.TUnit, Right = sp.TInt).layout(("Left", "Right"))).layout("fifo")
tprivates = { }
tviews = { }
