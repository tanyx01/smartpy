open SmartML

module Contract = struct
  let%entry_point welcome params =
    verify (data.Left <= 123);
    data.Left <- data.Left + params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {Left = intOrNat; Right = intOrNat}]
      ~storage:[%expr
                 {Left = 1;
                  Right = 12}]
      [welcome]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())