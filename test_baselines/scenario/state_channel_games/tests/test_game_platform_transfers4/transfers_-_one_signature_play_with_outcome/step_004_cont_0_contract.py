import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(administrator = sp.TAddress, all_tokens = sp.TSet(sp.TNat), ledger = sp.TBigMap(sp.TPair(sp.TAddress, sp.TNat), sp.TRecord(balance = sp.TNat).layout("balance")), metadata = sp.TBigMap(sp.TString, sp.TBytes), operators = sp.TBigMap(sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), sp.TUnit), paused = sp.TBool, token_ids = sp.TBigMap(sp.TAddress, sp.TMap(sp.TNat, sp.TNat)), token_metadata = sp.TBigMap(sp.TNat, sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info"))), token_permissions = sp.TBigMap(sp.TNat, sp.TMap(sp.TString, sp.TBytes)), total_supply = sp.TBigMap(sp.TNat, sp.TNat)).layout(("administrator", ("all_tokens", ("ledger", ("metadata", ("operators", ("paused", ("token_ids", ("token_metadata", ("token_permissions", "total_supply")))))))))))
    self.init(administrator = sp.address('tz1QJMtj2n8sKfHaCqhsiNCzqMK5xsYwHQY2'),
              all_tokens = sp.set([]),
              ledger = {},
              metadata = {'' : sp.bytes('0x68747470733a2f2f6578616d706c652e636f6d')},
              operators = {},
              paused = False,
              token_ids = {},
              token_metadata = {},
              token_permissions = {},
              total_supply = {})

  @sp.entry_point
  def balance_of(self, params):
    sp.verify(~ self.data.paused, 'FA2_PAUSED')
    sp.set_type(params, sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")))
    def f_x0(_x0):
      sp.verify(self.data.token_metadata.contains(_x0.token_id), 'FA2_TOKEN_UNDEFINED')
      sp.if self.data.ledger.contains((sp.set_type_expr(_x0.owner, sp.TAddress), sp.set_type_expr(_x0.token_id, sp.TNat))):
        sp.result(sp.record(request = sp.record(owner = sp.set_type_expr(_x0.owner, sp.TAddress), token_id = sp.set_type_expr(_x0.token_id, sp.TNat)), balance = self.data.ledger[(sp.set_type_expr(_x0.owner, sp.TAddress), sp.set_type_expr(_x0.token_id, sp.TNat))].balance))
      sp.else:
        sp.result(sp.record(request = sp.record(owner = sp.set_type_expr(_x0.owner, sp.TAddress), token_id = sp.set_type_expr(_x0.token_id, sp.TNat)), balance = 0))
    responses = sp.local("responses", params.requests.map(sp.build_lambda(f_x0)))
    sp.transfer(responses.value, sp.tez(0), sp.set_type_expr(params.callback, sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))))))

  @sp.entry_point
  def burn(self, params):
    sp.verify(self.data.token_permissions.contains(params.token_id), ('Ledger_token_unpermitted', params.token_id))
    compute_ledger_177 = sp.local("compute_ledger_177", self.data.token_permissions[params.token_id])
    sp.verify(self.data.token_metadata.contains(params.token_id), 'FA2_TOKEN_UNDEFINED')
    sp.verify(compute_ledger_177.value.contains('burn_permissions'), ('Ledger_No_burn_permissions', params.token_id))
    compute_ledger_185 = sp.local("compute_ledger_185", sp.unpack(compute_ledger_177.value['burn_permissions'], sp.TVariant(allow_only = sp.TSet(sp.TAddress), only_owner = sp.TUnit, only_owner_and_admin = sp.TUnit).layout(("allow_only", ("only_owner", "only_owner_and_admin")))).open_some(message = 'Ledger_unpack_err'))
    with compute_ledger_185.value.match_cases() as arg:
      with arg.match('allow_only') as allow_only:
        sp.verify(allow_only.contains(sp.sender), ('Ledger_not_allowed', params.token_id))
      with arg.match('only_owner') as only_owner:
        sp.verify(sp.sender == params.address, 'Ledger_onlyOwner')
      with arg.match('only_owner_and_admin') as only_owner_and_admin:
        sp.verify((sp.sender == params.address) | (self.data.administrator == sp.sender), 'Ledger_onlyOwner')

    sp.verify(compute_ledger_177.value.contains('type'), ('Ledger_No_Type', params.token_id))
    compute_ledger_196 = sp.local("compute_ledger_196", sp.unpack(compute_ledger_177.value['type'], sp.TString).open_some())
    sp.verify(compute_ledger_196.value == 'NATIVE', 'Ledger_Can_Only_Burn_Native')
    supply = sp.local("supply", sp.unpack(compute_ledger_177.value['supply'], sp.TNat).open_some())
    sp.if compute_ledger_177.value.contains('supply'):
      supply.value = sp.as_nat(supply.value - params.amount)
    self.data.token_permissions[params.token_id]['supply'] = sp.pack(supply.value)
    self.data.ledger[(sp.set_type_expr(sp.sender, sp.TAddress), sp.set_type_expr(params.token_id, sp.TNat))].balance = sp.as_nat(self.data.ledger[(sp.set_type_expr(sp.sender, sp.TAddress), sp.set_type_expr(params.token_id, sp.TNat))].balance - params.amount)

  @sp.entry_point
  def mint(self, params):
    sp.verify(self.data.token_permissions.contains(params.token_id), ('Ledger_token_unpermitted', params.token_id))
    compute_ledger_145 = sp.local("compute_ledger_145", self.data.token_permissions[params.token_id])
    sp.verify(self.data.token_metadata.contains(params.token_id), 'FA2_TOKEN_UNDEFINED')
    sp.verify(compute_ledger_145.value.contains('mint_permissions'), ('Ledger_No_mint_permissions', params.token_id))
    compute_ledger_153 = sp.local("compute_ledger_153", sp.unpack(compute_ledger_145.value['mint_permissions'], sp.TVariant(allow_everyone = sp.TUnit, allow_only = sp.TSet(sp.TAddress)).layout(("allow_everyone", "allow_only"))).open_some(message = 'Ledger_unpack_err'))
    with compute_ledger_153.value.match_cases() as arg:
      with arg.match('allow_only') as allow_only:
        sp.verify(allow_only.contains(sp.sender), ('Ledger_not_allowed', params.token_id))
      with arg.match('allow_everyone') as allow_everyone:
        pass

    sp.if compute_ledger_145.value.contains('mint_cost'):
      compute_ledger_162 = sp.local("compute_ledger_162", sp.unpack(compute_ledger_145.value['mint_cost'], sp.TMutez).open_some(message = 'Ledger_unpack_err'))
      compute_ledger_163 = sp.local("compute_ledger_163", sp.mul(params.amount, compute_ledger_162.value))
      sp.verify(sp.amount == compute_ledger_163.value, ('FA2_WRONG_AMOUNT_expected', compute_ledger_163.value))
    sp.verify(compute_ledger_145.value.contains('type'), ('Ledger_No_Type', params.token_id))
    compute_ledger_170 = sp.local("compute_ledger_170", sp.unpack(compute_ledger_145.value['type'], sp.TString).open_some())
    sp.verify(compute_ledger_170.value == 'NATIVE', 'Ledger_Can_Only_Mint_NATIVE')
    supply = sp.local("supply", params.amount)
    sp.if compute_ledger_145.value.contains('supply'):
      supply.value += sp.unpack(compute_ledger_145.value['supply'], sp.TNat).open_some()
    self.data.token_permissions[params.token_id]['supply'] = sp.pack(supply.value)
    sp.if self.data.ledger.contains((sp.set_type_expr(params.address, sp.TAddress), sp.set_type_expr(params.token_id, sp.TNat))):
      self.data.ledger[(sp.set_type_expr(params.address, sp.TAddress), sp.set_type_expr(params.token_id, sp.TNat))].balance += params.amount
    sp.else:
      self.data.ledger[(sp.set_type_expr(params.address, sp.TAddress), sp.set_type_expr(params.token_id, sp.TNat))] = sp.record(balance = params.amount)

  @sp.entry_point
  def on_received_bonds(self, params):
    sp.set_type(params, sp.TRecord(amount = sp.TNat, data = sp.TBytes, receiver = sp.TAddress, sender = sp.TAddress, token_id = sp.TNat).layout(("amount", ("data", ("receiver", ("sender", "token_id"))))))
    sp.transfer(sp.record(amount = params.amount, data = params.data, receiver = params.receiver, sender = params.sender, token_id = self.data.token_ids[sp.sender][params.token_id]), sp.tez(0), sp.contract(sp.TRecord(amount = sp.TNat, data = sp.TBytes, receiver = sp.TAddress, sender = sp.TAddress, token_id = sp.TNat).layout(("amount", ("data", ("receiver", ("sender", "token_id"))))), params.receiver, entry_point='on_received_bonds').open_some(message = 'Ledger_Platform_Unreacheable'))

  @sp.entry_point
  def push_bonds(self, params):
    sp.set_type(params.channel_id, sp.TBytes)
    sp.set_type(params.player_addr, sp.TAddress)
    sp.for token in params.bonds.items():
      sp.set_type(sp.list([sp.record(from_ = sp.sender, txs = sp.list([sp.record(amount = token.value, callback = sp.to_address(sp.contract(sp.TRecord(amount = sp.TNat, data = sp.TBytes, receiver = sp.TAddress, sender = sp.TAddress, token_id = sp.TNat).layout(("amount", ("data", ("receiver", ("sender", "token_id"))))), params.platform, entry_point='on_received_bonds').open_some()), data = sp.pack(sp.record(channel_id = params.channel_id, player_addr = params.player_addr)), to_ = params.platform, token_id = token.key)]))]), sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, callback = sp.TAddress, data = sp.TBytes, to_ = sp.TAddress, token_id = sp.TNat).layout(("amount", ("callback", ("data", ("to_", "token_id"))))))).layout(("from_", "txs"))))
      sp.for batchs in sp.list([sp.record(from_ = sp.sender, txs = sp.list([sp.record(amount = token.value, callback = sp.to_address(sp.contract(sp.TRecord(amount = sp.TNat, data = sp.TBytes, receiver = sp.TAddress, sender = sp.TAddress, token_id = sp.TNat).layout(("amount", ("data", ("receiver", ("sender", "token_id"))))), params.platform, entry_point='on_received_bonds').open_some()), data = sp.pack(sp.record(channel_id = params.channel_id, player_addr = params.player_addr)), to_ = params.platform, token_id = token.key)]))]):
        sp.for transaction in batchs.txs:
          sp.set_type(sp.list([sp.record(from_ = batchs.from_, txs = sp.list([sp.record(to_ = transaction.to_, token_id = transaction.token_id, amount = transaction.amount)]))]), sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))))
          sp.verify(~ self.data.paused, 'FA2_PAUSED')
          sp.for transfer in sp.list([sp.record(from_ = batchs.from_, txs = sp.list([sp.record(amount = transaction.amount, to_ = transaction.to_, token_id = transaction.token_id)]))]):
            sp.for tx in transfer.txs:
              sp.set_type(self.data.token_permissions, sp.TBigMap(sp.TNat, sp.TMap(sp.TString, sp.TBytes)))
              sp.verify(self.data.token_permissions.contains(tx.token_id), ('Ledger_token_unpermitted', tx.token_id))
              compute_ledger_204 = sp.local("compute_ledger_204", self.data.token_permissions[tx.token_id])
              sp.if compute_ledger_204.value.contains('transfer_only_to'):
                compute_ledger_207 = sp.local("compute_ledger_207", sp.unpack(compute_ledger_204.value['transfer_only_to'], sp.TSet(sp.TAddress)).open_some(message = 'Ledger_unpack_err'))
                sp.verify(compute_ledger_207.value.contains(tx.to_), ('transfer_only_to', compute_ledger_207.value))
              sp.if compute_ledger_204.value.contains('transfer_only_from'):
                compute_ledger_210 = sp.local("compute_ledger_210", sp.unpack(compute_ledger_204.value['transfer_only_from'], sp.TSet(sp.TAddress)).open_some(message = 'Ledger_unpack_err'))
                sp.verify(compute_ledger_210.value.contains(transfer.from_), ('transfer_only_from', compute_ledger_210.value))
              sp.verify(compute_ledger_204.value.contains('type'), ('Ledger_No_Type', tx.token_id))
              compute_ledger_271 = sp.local("compute_ledger_271", sp.unpack(compute_ledger_204.value['type'], sp.TString).open_some())
              sp.if compute_ledger_271.value == 'FA2':
                sp.verify(transfer.from_ == sp.sender, 'Ledger_Current_From_And_Sender_Differs')
                match_pair_ledger_220_fst, match_pair_ledger_220_snd = sp.match_tuple(sp.unpack(compute_ledger_204.value['fa_token'], sp.TPair(sp.TAddress, sp.TNat)).open_some(), "match_pair_ledger_220_fst", "match_pair_ledger_220_snd")
                sp.transfer(sp.list([sp.record(from_ = sp.sender, txs = sp.list([sp.record(to_ = tx.to_, token_id = match_pair_ledger_220_snd, amount = tx.amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), match_pair_ledger_220_fst, entry_point='transfer').open_some(message = 'Ledger_FA2_Unreacheable'))
              sp.else:
                sp.verify(compute_ledger_271.value == 'NATIVE', ('Ledger_Type_Unknown', compute_ledger_271.value))
                sp.verify(((sp.sender == self.data.administrator) | (transfer.from_ == sp.sender)) | (self.data.operators.contains(sp.set_type_expr(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))))), 'FA2_NOT_OPERATOR')
                sp.verify(self.data.token_metadata.contains(tx.token_id), 'FA2_TOKEN_UNDEFINED')
                sp.if tx.amount > 0:
                  compute_ledger_252 = sp.local("compute_ledger_252", self.data.ledger.get((sp.set_type_expr(transfer.from_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat)), default_value = sp.record(balance = 0)))
                  sp.verify(compute_ledger_252.value.balance >= tx.amount, 'FA2_INSUFFICIENT_BALANCE')
                  self.data.ledger[(sp.set_type_expr(transfer.from_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))].balance = sp.as_nat(compute_ledger_252.value.balance - tx.amount)
                  sp.if self.data.ledger.contains((sp.set_type_expr(tx.to_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))):
                    self.data.ledger[(sp.set_type_expr(tx.to_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))].balance += tx.amount
                  sp.else:
                    self.data.ledger[(sp.set_type_expr(tx.to_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))] = sp.record(balance = tx.amount)
          sp.transfer(sp.record(amount = transaction.amount, data = transaction.data, receiver = transaction.to_, sender = batchs.from_, token_id = transaction.token_id), sp.tez(0), sp.contract(sp.TRecord(amount = sp.TNat, data = sp.TBytes, receiver = sp.TAddress, sender = sp.TAddress, token_id = sp.TNat).layout(("amount", ("data", ("receiver", ("sender", "token_id"))))), transaction.callback).open_some(message = 'FA2_WRONG_CALLBACK_INTERFACE'))

  @sp.entry_point
  def set_administrator(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.administrator = params

  @sp.entry_point
  def set_metadata(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.metadata[params.k] = params.v

  @sp.entry_point
  def set_pause(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.paused = params

  @sp.entry_point
  def set_token_info(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.token_metadata[params.token_id] = sp.record(token_id = params.token_id, token_info = params.token_info)

  @sp.entry_point
  def transfer(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))))
    sp.verify(~ self.data.paused, 'FA2_PAUSED')
    sp.for transfer in params:
      sp.for tx in transfer.txs:
        sp.set_type(self.data.token_permissions, sp.TBigMap(sp.TNat, sp.TMap(sp.TString, sp.TBytes)))
        sp.verify(self.data.token_permissions.contains(tx.token_id), ('Ledger_token_unpermitted', tx.token_id))
        compute_ledger_204 = sp.local("compute_ledger_204", self.data.token_permissions[tx.token_id])
        sp.if compute_ledger_204.value.contains('transfer_only_to'):
          compute_ledger_207 = sp.local("compute_ledger_207", sp.unpack(compute_ledger_204.value['transfer_only_to'], sp.TSet(sp.TAddress)).open_some(message = 'Ledger_unpack_err'))
          sp.verify(compute_ledger_207.value.contains(tx.to_), ('transfer_only_to', compute_ledger_207.value))
        sp.if compute_ledger_204.value.contains('transfer_only_from'):
          compute_ledger_210 = sp.local("compute_ledger_210", sp.unpack(compute_ledger_204.value['transfer_only_from'], sp.TSet(sp.TAddress)).open_some(message = 'Ledger_unpack_err'))
          sp.verify(compute_ledger_210.value.contains(transfer.from_), ('transfer_only_from', compute_ledger_210.value))
        sp.verify(compute_ledger_204.value.contains('type'), ('Ledger_No_Type', tx.token_id))
        compute_ledger_271 = sp.local("compute_ledger_271", sp.unpack(compute_ledger_204.value['type'], sp.TString).open_some())
        sp.if compute_ledger_271.value == 'FA2':
          sp.verify(transfer.from_ == sp.sender, 'Ledger_Current_From_And_Sender_Differs')
          match_pair_ledger_220_fst, match_pair_ledger_220_snd = sp.match_tuple(sp.unpack(compute_ledger_204.value['fa_token'], sp.TPair(sp.TAddress, sp.TNat)).open_some(), "match_pair_ledger_220_fst", "match_pair_ledger_220_snd")
          sp.transfer(sp.list([sp.record(from_ = sp.sender, txs = sp.list([sp.record(to_ = tx.to_, token_id = match_pair_ledger_220_snd, amount = tx.amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), match_pair_ledger_220_fst, entry_point='transfer').open_some(message = 'Ledger_FA2_Unreacheable'))
        sp.else:
          sp.verify(compute_ledger_271.value == 'NATIVE', ('Ledger_Type_Unknown', compute_ledger_271.value))
          sp.verify(((sp.sender == self.data.administrator) | (transfer.from_ == sp.sender)) | (self.data.operators.contains(sp.set_type_expr(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))))), 'FA2_NOT_OPERATOR')
          sp.verify(self.data.token_metadata.contains(tx.token_id), 'FA2_TOKEN_UNDEFINED')
          sp.if tx.amount > 0:
            compute_ledger_252 = sp.local("compute_ledger_252", self.data.ledger.get((sp.set_type_expr(transfer.from_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat)), default_value = sp.record(balance = 0)))
            sp.verify(compute_ledger_252.value.balance >= tx.amount, 'FA2_INSUFFICIENT_BALANCE')
            self.data.ledger[(sp.set_type_expr(transfer.from_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))].balance = sp.as_nat(compute_ledger_252.value.balance - tx.amount)
            sp.if self.data.ledger.contains((sp.set_type_expr(tx.to_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))):
              self.data.ledger[(sp.set_type_expr(tx.to_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))].balance += tx.amount
            sp.else:
              self.data.ledger[(sp.set_type_expr(tx.to_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))] = sp.record(balance = tx.amount)

  @sp.entry_point
  def transfer_and_call(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, callback = sp.TAddress, data = sp.TBytes, to_ = sp.TAddress, token_id = sp.TNat).layout(("amount", ("callback", ("data", ("to_", "token_id"))))))).layout(("from_", "txs"))))
    sp.for batchs in params:
      sp.for transaction in batchs.txs:
        sp.set_type(sp.list([sp.record(from_ = batchs.from_, txs = sp.list([sp.record(to_ = transaction.to_, token_id = transaction.token_id, amount = transaction.amount)]))]), sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))))
        sp.verify(~ self.data.paused, 'FA2_PAUSED')
        sp.for transfer in sp.list([sp.record(from_ = batchs.from_, txs = sp.list([sp.record(amount = transaction.amount, to_ = transaction.to_, token_id = transaction.token_id)]))]):
          sp.for tx in transfer.txs:
            sp.set_type(self.data.token_permissions, sp.TBigMap(sp.TNat, sp.TMap(sp.TString, sp.TBytes)))
            sp.verify(self.data.token_permissions.contains(tx.token_id), ('Ledger_token_unpermitted', tx.token_id))
            compute_ledger_204 = sp.local("compute_ledger_204", self.data.token_permissions[tx.token_id])
            sp.if compute_ledger_204.value.contains('transfer_only_to'):
              compute_ledger_207 = sp.local("compute_ledger_207", sp.unpack(compute_ledger_204.value['transfer_only_to'], sp.TSet(sp.TAddress)).open_some(message = 'Ledger_unpack_err'))
              sp.verify(compute_ledger_207.value.contains(tx.to_), ('transfer_only_to', compute_ledger_207.value))
            sp.if compute_ledger_204.value.contains('transfer_only_from'):
              compute_ledger_210 = sp.local("compute_ledger_210", sp.unpack(compute_ledger_204.value['transfer_only_from'], sp.TSet(sp.TAddress)).open_some(message = 'Ledger_unpack_err'))
              sp.verify(compute_ledger_210.value.contains(transfer.from_), ('transfer_only_from', compute_ledger_210.value))
            sp.verify(compute_ledger_204.value.contains('type'), ('Ledger_No_Type', tx.token_id))
            compute_ledger_271 = sp.local("compute_ledger_271", sp.unpack(compute_ledger_204.value['type'], sp.TString).open_some())
            sp.if compute_ledger_271.value == 'FA2':
              sp.verify(transfer.from_ == sp.sender, 'Ledger_Current_From_And_Sender_Differs')
              match_pair_ledger_220_fst, match_pair_ledger_220_snd = sp.match_tuple(sp.unpack(compute_ledger_204.value['fa_token'], sp.TPair(sp.TAddress, sp.TNat)).open_some(), "match_pair_ledger_220_fst", "match_pair_ledger_220_snd")
              sp.transfer(sp.list([sp.record(from_ = sp.sender, txs = sp.list([sp.record(to_ = tx.to_, token_id = match_pair_ledger_220_snd, amount = tx.amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), match_pair_ledger_220_fst, entry_point='transfer').open_some(message = 'Ledger_FA2_Unreacheable'))
            sp.else:
              sp.verify(compute_ledger_271.value == 'NATIVE', ('Ledger_Type_Unknown', compute_ledger_271.value))
              sp.verify(((sp.sender == self.data.administrator) | (transfer.from_ == sp.sender)) | (self.data.operators.contains(sp.set_type_expr(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))))), 'FA2_NOT_OPERATOR')
              sp.verify(self.data.token_metadata.contains(tx.token_id), 'FA2_TOKEN_UNDEFINED')
              sp.if tx.amount > 0:
                compute_ledger_252 = sp.local("compute_ledger_252", self.data.ledger.get((sp.set_type_expr(transfer.from_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat)), default_value = sp.record(balance = 0)))
                sp.verify(compute_ledger_252.value.balance >= tx.amount, 'FA2_INSUFFICIENT_BALANCE')
                self.data.ledger[(sp.set_type_expr(transfer.from_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))].balance = sp.as_nat(compute_ledger_252.value.balance - tx.amount)
                sp.if self.data.ledger.contains((sp.set_type_expr(tx.to_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))):
                  self.data.ledger[(sp.set_type_expr(tx.to_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))].balance += tx.amount
                sp.else:
                  self.data.ledger[(sp.set_type_expr(tx.to_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))] = sp.record(balance = tx.amount)
        sp.transfer(sp.record(amount = transaction.amount, data = transaction.data, receiver = transaction.to_, sender = batchs.from_, token_id = transaction.token_id), sp.tez(0), sp.contract(sp.TRecord(amount = sp.TNat, data = sp.TBytes, receiver = sp.TAddress, sender = sp.TAddress, token_id = sp.TNat).layout(("amount", ("data", ("receiver", ("sender", "token_id"))))), transaction.callback).open_some(message = 'FA2_WRONG_CALLBACK_INTERFACE'))

  @sp.entry_point
  def update_operators(self, params):
    sp.set_type(params, sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))))
    sp.for update in params:
      with update.match_cases() as arg:
        with arg.match('add_operator') as add_operator:
          sp.verify((add_operator.owner == sp.sender) | (sp.sender == self.data.administrator), 'FA2_NOT_ADMIN_OR_OPERATOR')
          self.data.operators[sp.set_type_expr(sp.record(owner = add_operator.owner, operator = add_operator.operator, token_id = add_operator.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))))] = sp.unit
        with arg.match('remove_operator') as remove_operator:
          sp.verify((remove_operator.owner == sp.sender) | (sp.sender == self.data.administrator), 'FA2_NOT_ADMIN_OR_OPERATOR')
          del self.data.operators[sp.set_type_expr(sp.record(owner = remove_operator.owner, operator = remove_operator.operator, token_id = remove_operator.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))))]


  @sp.entry_point
  def update_token_permissions(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    new_permissions = sp.local("new_permissions", self.data.token_permissions.get(params.token_id, default_value = {}))
    sp.for permission in params.token_permissions.items():
      sp.if permission.value.is_some():
        new_permissions.value[permission.key] = permission.value.open_some()
        sp.if permission.key == 'fa_token':
          match_pair_ledger_407_fst, match_pair_ledger_407_snd = sp.match_tuple(sp.unpack(permission.value.open_some(), sp.TPair(sp.TAddress, sp.TNat)).open_some(), "match_pair_ledger_407_fst", "match_pair_ledger_407_snd")
          sp.if ~ (self.data.token_ids.contains(match_pair_ledger_407_fst)):
            self.data.token_ids[match_pair_ledger_407_fst] = {match_pair_ledger_407_snd : params.token_id}
          sp.else:
            self.data.token_ids[match_pair_ledger_407_fst][match_pair_ledger_407_snd] = params.token_id
      sp.else:
        sp.if permission.key == 'fa_token':
          match_pair_ledger_402_fst, match_pair_ledger_402_snd = sp.match_tuple(sp.unpack(permission.value.open_some(), sp.TPair(sp.TAddress, sp.TNat)).open_some(), "match_pair_ledger_402_fst", "match_pair_ledger_402_snd")
          del self.data.token_ids[match_pair_ledger_402_fst][match_pair_ledger_402_snd]
        del new_permissions.value[permission.key]
    sp.if sp.len(new_permissions.value) == 0:
      del self.data.token_permissions[params.token_id]
    sp.else:
      self.data.token_permissions[params.token_id] = new_permissions.value

sp.add_compilation_target("test", Contract())