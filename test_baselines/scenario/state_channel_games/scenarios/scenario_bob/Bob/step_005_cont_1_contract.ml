open SmartML

module Contract = struct
  let%entry_point admin_game_permissions params =
    verify (contains sender data.admins) ~msg:"Platform_NotAdmin";
    verify (contains params.game_id data.games) ~msg:"Platform_GameNotFound";
    set_type params.permission (option bytes);
    set_type params.key string;
    if is_some params.permission then
      Map.set (Map.get data.games params.game_id).permissions params.key (open_some params.permission)
    else
      Map.delete (Map.get data.games params.game_id).permissions params.key

  let%entry_point admin_global_metadata params =
    verify (contains sender data.admins) ~msg:"Platform_NotAdmin";
    set_type params.metadata (option bytes);
    set_type params.key string;
    if is_some params.metadata then
      Map.set data.metadata params.key (open_some params.metadata)
    else
      Map.delete data.metadata params.key

  let%entry_point admin_model_metadata params =
    verify (contains sender data.admins) ~msg:"Platform_NotAdmin";
    verify (contains params.model_id data.models) ~msg:"Platform_ModelNotFound";
    set_type params.metadata (option bytes);
    set_type params.key string;
    if is_some params.metadata then
      Map.set (Map.get data.model_metadata params.model_id) params.key (open_some params.metadata)
    else
      Map.delete (Map.get data.model_metadata params.model_id) params.key

  let%entry_point admin_model_permissions params =
    verify (contains sender data.admins) ~msg:"Platform_NotAdmin";
    verify (contains params.model_id data.models) ~msg:"Platform_ModelNotFound";
    set_type params.permission (option bytes);
    set_type params.key string;
    if is_some params.permission then
      Map.set (Map.get data.model_permissions params.model_id) params.key (open_some params.permission)
    else
      Map.delete (Map.get data.model_permissions params.model_id) params.key

  let%entry_point admin_new_model params =
    verify (contains sender data.admins) ~msg:"Platform_NotAdmin";
    let%mutable compute_game_platform_962 = (blake2b params.model) in ();
    if not (contains compute_game_platform_962 data.models) then
      (
        let%mutable compute_game_platform_964 = (open_some ~message:"Platform_ModelUnpackFailure" (unpack params.model {apply_ = lambda {move_data = bytes; move_nb = nat; player = int; state = bytes} (pair bytes (option string)); init = lambda bytes bytes; outcomes = list string})) in ();
        Map.set data.model_metadata compute_game_platform_962 params.metadata;
        Map.set data.model_permissions compute_game_platform_962 params.permissions;
        Map.set data.models compute_game_platform_962 {apply_ = compute_game_platform_964.apply_; init = compute_game_platform_964.init; outcomes = compute_game_platform_964.outcomes}
      );
    if (len params.rewards) > (nat 0) then
      transfer [{from_ = address "KT1LX9dx1MMSBbG3seX7DHC7kd2tuKUmwyZs"; txs = params.rewards}] (tez 0) (open_some ~message:"Platform_LedgerNotFound" (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) data.ledger , entry_point='transfer'))

  let%entry_point admin_setup params =
    verify (contains sender data.admins) ~msg:"Platform_NotAdmin";
    if is_some params.ledger then
      data.ledger <- open_some params.ledger;
    List.iter (fun admin ->
      Set.add data.admins admin
    ) params.add_admins;
    List.iter (fun admin ->
      verify (not (admin = sender)) ~msg:"Platform_CannotRemoveSelf";
      Set.remove data.admins admin
    ) (Set.elements params.remove_admins)

  let%entry_point admin_update_operators params =
    verify (contains sender data.admins) ~msg:"Platform_NotAdmin";
    transfer params.operators (tez 0) (open_some ~message:"Plateform_FA2Unreacheable" (contract (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat})) params.fa2 , entry_point='update_operators'))

  let%entry_point dispute_double_signed params =
    let%mutable game = (get ~message:%s data.games params.game_id ("Platform_GameNotFound: ", params.game_id)) in ();
    with game.current.outcome.match('Some') as Some:
      verify (is_variant "pending" Some) ~msg:"Platform_GameNotRunning";
    set_type game.constants {bonds = map int (map nat nat); channel_id = bytes; game_nonce = string; model_id = bytes; play_delay = int; players_addr = map int address; settlements = map (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) (list {bonds = map nat nat; receiver = int; sender = int})};
    let%mutable player = (Map.get (Map.get data.channels game.constants.channel_id).players (Map.get game.constants.players_addr params.player_id)) in ();
    verify (check_signature player.pk params.statement1.sig (pack ("Play", set_type_expr params.game_id bytes, set_type_expr params.statement1.current {move_nb = nat; outcome = option (`final (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) + `pending (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int)); player = int}, set_type_expr params.statement1.state bytes, set_type_expr params.statement1.move_data bytes))) ~msg:"Platform_badSig";
    verify (check_signature player.pk params.statement2.sig (pack ("Play", set_type_expr params.game_id bytes, set_type_expr params.statement2.current {move_nb = nat; outcome = option (`final (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) + `pending (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int)); player = int}, set_type_expr params.statement2.state bytes, set_type_expr params.statement2.move_data bytes))) ~msg:"Platform_badSig";
    verify (params.statement1.current.move_nb = params.statement2.current.move_nb) ~msg:"Platform_MoveNBMismatch";
    verify (params.statement1.sig <> params.statement2.sig) ~msg:"Platform_NotDifferentSignatures";
    game.current.outcome <- some (variant final (variant player_double_played params.player_id));
    Map.set data.games params.game_id game

  let%entry_point dispute_starved params =
    let%mutable game = (get ~message:%s data.games params.game_id ("Platform_GameNotFound: ", params.game_id)) in ();
    with game.current.outcome.match('Some') as Some:
      verify (is_variant "pending" Some) ~msg:"Platform_GameNotRunning";
    verify (game.current.player = params.player_id) ~msg:"Platform_NotPlayersTurn";
    verify (contains params.player_id game.timeouts) ~msg:"Platform_NoTimeoutSetup";
    verify (now > (Map.get game.timeouts params.player_id)) ~msg:"Platform_NotTimedOut";
    game.current.outcome <- some (variant final (variant player_inactive params.player_id));
    Map.set data.games params.game_id game

  let%entry_point dispute_starving params =
    let%mutable game = (get ~message:%s data.games params.game_id ("Platform_GameNotFound: ", params.game_id)) in ();
    with game.current.outcome.match('Some') as Some:
      verify (is_variant "pending" Some) ~msg:"Platform_GameNotRunning";
    if params.flag then
      Map.set game.timeouts ((int 3) - (Map.get game.addr_players sender)) (add_seconds now game.constants.play_delay)
    else
      Map.delete game.timeouts ((int 3) - (Map.get game.addr_players sender));
    Map.set data.games params.game_id game

  let%entry_point game_play params =
    let%mutable game = (get ~message:%s data.games params.game_id ("Platform_GameNotFound: ", params.game_id)) in ();
    with game.current.outcome.match('Some') as Some:
      verify (is_variant "pending" Some) ~msg:"Platform_GameNotRunning";
    set_type params.signatures (map key signature);
    let%mutable signers = [] in ();
    if (len params.signatures) = (nat 1) then
      (
        let%mutable pending = false in ();
        set_type game.constants {bonds = map int (map nat nat); channel_id = bytes; game_nonce = string; model_id = bytes; play_delay = int; players_addr = map int address; settlements = map (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) (list {bonds = map nat nat; receiver = int; sender = int})};
        let%mutable player = (Map.get (Map.get data.channels game.constants.channel_id).players (Map.get game.constants.players_addr game.current.player)) in ();
        signers <- [player];
        match game.current.outcome with
          | `None None ->
            let%mutable model = (get ~message:%s data.models game.constants.model_id ("Platform_ModelNotFound: ", game.constants.model_id)) in ();
            if not (is_some game.state) then
              game.state <- some (game.initial_config model.init);
            let%mutable compute_game_platform_347 = ({move_data = params.move_data; move_nb = game.current.move_nb; player = game.current.player; state = (open_some game.state)} model.apply_) in ();
            game.current <- {move_nb = (game.current.move_nb + (nat 1)); outcome = (eif (is_some (snd compute_game_platform_347)) (some (variant final (variant game_finished (open_some (snd compute_game_platform_347))))) None); player = ((int 3) - game.current.player)};
            game.state <- some (fst compute_game_platform_347);
            if is_some game.current.outcome then
              pending <- true
          | `Some Some ->
            game.current.outcome <- some (variant final (open_variant Some pending))
;
        verify (game.current = params.new_current) ~msg:"Platform_CurrentMismatch";
        verify ((open_some game.state) = params.new_state) ~msg:"Platform_StateMismatch";
        if pending then
          game.current.outcome <- some (variant pending (open_variant (open_some game.current.outcome) final))
      )
    else
      (
        let%mutable equal_move_nb = false in ();
        if is_some game.current.outcome then
          if is_variant "pending" (open_some game.current.outcome) then
            if params.new_current.move_nb = game.current.move_nb then
              equal_move_nb <- true;
        verify ((params.new_current.move_nb > game.current.move_nb) || equal_move_nb) ~msg:"Platform_OutdatedMoveNB";
        let%mutable channel = (get ~message:%s data.channels game.constants.channel_id ("Platform_ChannelNotFound: ", game.constants.channel_id)) in ();
        verify (not channel.closed) ~msg:"Platform_ChannelClosed";
        signers <- Map.values channel.players;
        game.current <- params.new_current;
        game.state <- some params.new_state
      );
    let%mutable compute_game_platform_441 = (pack ("Play", set_type_expr params.game_id bytes, set_type_expr params.new_current {move_nb = nat; outcome = option (`final (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) + `pending (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int)); player = int}, set_type_expr params.new_state bytes, set_type_expr params.move_data bytes)) in ();
    List.iter (fun player ->
      verify (check_signature player.pk (Map.get params.signatures player.pk) compute_game_platform_441) ~msg:("Platform_badSig", player.pk)
    ) signers;
    if contains game.current.player game.timeouts then
      Map.set game.timeouts game.current.player (add_seconds now game.constants.play_delay);
    Map.set data.games params.game_id game

  let%entry_point game_set_outcome params =
    set_type params.outcome (`game_aborted unit + `game_finished string);
    set_type params.signatures (map key signature);
    verify (now <= params.timeout) ~msg:"Platform_OutcomeTimedout";
    let%mutable game = (get ~message:%s data.games params.game_id ("Platform_GameNotFound: ", params.game_id)) in ();
    with game.current.outcome.match('Some') as Some:
      verify (is_variant "pending" Some) ~msg:"Platform_GameNotRunning";
    let%mutable compute_game_platform_618 = (pack ("New Outcome", set_type_expr params.game_id bytes, set_type_expr params.outcome (`game_aborted unit + `game_finished string), params.timeout)) in ();
    let%mutable channel = (get ~message:%s data.channels game.constants.channel_id ("Platform_ChannelNotFound: ", game.constants.channel_id)) in ();
    verify (not channel.closed) ~msg:"Platform_ChannelClosed";
    let%mutable players = channel.players in ();
    List.iter (fun player ->
      verify (contains player.pk params.signatures) ~msg:"Platform_MissingSig";
      verify (check_signature player.pk (Map.get params.signatures player.pk) compute_game_platform_618) ~msg:"Platform_badSig"
    ) (Map.values players);
    if is_variant "game_aborted" params.outcome then
      game.current.outcome <- some (variant final (variant game_aborted ()))
    else
      game.current.outcome <- some (variant final (variant game_finished (open_variant params.outcome game_finished)));
    Map.set data.games params.game_id game

  let%entry_point game_settle params =
    let%mutable game = (get ~message:%s data.games params ("Platform_GameNotFound: ", params)) in ();
    let%mutable compute_game_platform_658 = (open_some ~message:"Plateform_GameIsntOver" game.current.outcome) in ();
    let%mutable model_permissions = (eif (contains game.constants.model_id data.model_permissions) (some (Map.get data.model_permissions game.constants.model_id)) None) in ();
    let%mutable channel = (get ~message:%s data.channels game.constants.channel_id ("Platform_ChannelNotFound: ", game.constants.channel_id)) in ();
    verify (not channel.closed) ~msg:"Platform_ChannelClosed";
    verify (not game.settled) ~msg:"Plateform_GameSettled";
    let%mutable allowed_mint_model = (Map.make []) in ();
    let%mutable allowed_mint_game = (Map.make []) in ();
    if is_some model_permissions then
      if contains "allowed_mint" (open_some model_permissions) then
        allowed_mint_model <- open_some (unpack (Map.get (open_some model_permissions) "allowed_mint") (map nat nat));
    if contains "allowed_mint" game.permissions then
      allowed_mint_game <- open_some (unpack (Map.get game.permissions "allowed_mint") (map nat nat));
    List.iter (fun transfer ->
      if transfer.sender = (int 0) then
        (
          let%mutable compute_game_platform_502 = (Map.get game.constants.players_addr transfer.receiver) in ();
          let%mutable receiver = (Map.get channel.players compute_game_platform_502) in ();
          List.iter (fun tokens ->
            let%mutable delta = ((Map.get ~default_value:(nat 0) allowed_mint_game tokens.key) - tokens.value) in ();
            Map.set allowed_mint_game tokens.key (eif (delta > (int 0)) (open_some (is_nat delta)) (nat 0));
            if delta < (int 0) then
              Map.set allowed_mint_model tokens.key (open_some ~message:("Platform_NotAllowedMinting", {amount = tokens.value; game_id = params; token = tokens.key}) (is_nat ((to_int (Map.get ~default_value:(nat 0) allowed_mint_model tokens.key)) + delta)));
            Map.set receiver.bonds tokens.key ((Map.get ~default_value:(nat 0) receiver.bonds tokens.key) + tokens.value)
          ) (Map.items transfer.bonds);
          Map.set channel.players compute_game_platform_502 receiver
        )
      else
        (
          let%mutable compute_game_platform_529 = (Map.get game.constants.players_addr transfer.sender) in ();
          let%mutable compute_game_platform_530 = (Map.get game.constants.players_addr transfer.receiver) in ();
          if not (compute_game_platform_529 = compute_game_platform_530) then
            (
              let%mutable sender = (Map.get channel.players compute_game_platform_529) in ();
              let%mutable receiver = (Map.get channel.players compute_game_platform_530) in ();
              List.iter (fun bond ->
                if not (bond.value = (nat 0)) then
                  (
                    verify (contains bond.key sender.bonds) ~msg:("Platform_NotEnoughToken:", {amount = bond.value; sender = compute_game_platform_529; token = bond.key});
                    Map.set sender.bonds bond.key (open_some ~message:("Platform_NotEnoughToken:", {amount = bond.value; sender = compute_game_platform_529; token = bond.key}) (is_nat ((Map.get sender.bonds bond.key) - bond.value)));
                    if (Map.get sender.bonds bond.key) = (nat 0) then
                      Map.delete sender.bonds bond.key;
                    Map.set receiver.bonds bond.key ((Map.get ~default_value:(nat 0) receiver.bonds bond.key) + bond.value)
                  )
              ) (Map.items transfer.bonds);
              Map.set channel.players compute_game_platform_529 sender;
              Map.set channel.players compute_game_platform_530 receiver
            )
        )
    ) (get ~message:%s game.constants.settlements (open_variant compute_game_platform_658 final ~message:"Plateform_OutcomeNotFinalized") ("Platform_UnknownOutcome", compute_game_platform_658));
    (Map.get data.channels game.constants.channel_id).players <- channel.players;
    game.settled <- true;
    Map.set data.games params game;
    if (len allowed_mint_model) > (nat 0) then
      Map.set (Map.get data.model_permissions game.constants.model_id) "allowed_mint" (pack allowed_mint_model)
    else
      if is_some model_permissions then
        Map.delete (Map.get data.model_permissions game.constants.model_id) "allowed_mint"

  let%entry_point new_channel params =
    let%mutable compute_game_platform_577 = (blake2b (pack (address "KT1LX9dx1MMSBbG3seX7DHC7kd2tuKUmwyZs", params.players, params.nonce))) in ();
    verify (not (contains compute_game_platform_577 data.channels)) ~msg:"Platform_ChannelAlreadyExists";
    verify ((len params.players) = (nat 2)) ~msg:"Platform_Only2PlayersAllowed";
    let%mutable players_map = (Map.make []) in ();
    List.iter (fun player ->
      Map.set players_map player.key {bonds = (Map.make []); pk = player.value; withdraw = None; withdraw_id = (nat 0)}
    ) (Map.items params.players);
    Map.set data.channels compute_game_platform_577 {closed = false; nonce = params.nonce; players = players_map; withdraw_delay = params.withdraw_delay}

  let%entry_point new_game params =
    set_type params.constants {bonds = map int (map nat nat); channel_id = bytes; game_nonce = string; model_id = bytes; play_delay = int; players_addr = map int address; settlements = map (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) (list {bonds = map nat nat; receiver = int; sender = int})};
    set_type params.initial_config bytes;
    set_type params.signatures (map key signature);
    verify (not (contains (blake2b (pack (params.constants.channel_id, (params.constants.model_id, params.constants.game_nonce)))) data.games)) ~msg:"Platform_GameAlreadyExists";
    verify ((len params.constants.players_addr) = (nat 2)) ~msg:"Platform_Only2PlayersAllowed";
    let%mutable channel = (get ~message:%s data.channels params.constants.channel_id ("Platform_ChannelNotFound: ", params.constants.channel_id)) in ();
    verify (not channel.closed) ~msg:"Platform_ChannelClosed";
    let%mutable addr_players = (Map.make []) in ();
    List.iter (fun addr_player ->
      Map.set addr_players addr_player.value addr_player.key;
      verify (contains addr_player.value channel.players) ~msg:"Platform_GamePlayerNotInChannel"
    ) (Map.items params.constants.players_addr);
    List.iter (fun player ->
      verify (contains player.pk params.signatures) ~msg:"Platform_MissingSig";
      verify (check_signature player.pk (Map.get params.signatures player.pk) (pack ("New Game", set_type_expr params.constants {bonds = map int (map nat nat); channel_id = bytes; game_nonce = string; model_id = bytes; play_delay = int; players_addr = map int address; settlements = map (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) (list {bonds = map nat nat; receiver = int; sender = int})}, set_type_expr params.initial_config bytes))) ~msg:("Platform_BadSig", {player = player.pk; signature = (Map.get params.signatures player.pk)})
    ) (Map.values channel.players);
    let%mutable model = (get ~message:%s data.models params.constants.model_id ("Platform_ModelNotFound: ", params.constants.model_id)) in ();
    List.iter (fun outcome ->
      verify (contains (variant game_finished outcome) params.constants.settlements) ~msg:("Platform_MissingSettlement", variant game_finished outcome)
    ) model.outcomes;
    verify (contains (variant player_double_played (int 1)) params.constants.settlements) ~msg:("Platform_MissingSettlement", variant player_double_played 1);
    verify (contains (variant player_double_played (int 2)) params.constants.settlements) ~msg:("Platform_MissingSettlement", variant player_double_played 2);
    verify (contains (variant player_inactive (int 1)) params.constants.settlements) ~msg:("Platform_MissingSettlement", variant player_inactive 1);
    verify (contains (variant player_inactive (int 2)) params.constants.settlements) ~msg:("Platform_MissingSettlement", variant player_inactive 2);
    verify (contains (variant game_aborted ()) params.constants.settlements) ~msg:("Platform_MissingSettlement", variant game_aborted ());
    Map.set data.games (blake2b (pack (params.constants.channel_id, (params.constants.model_id, params.constants.game_nonce)))) (set_type_expr {addr_players = addr_players; constants = params.constants; current = {move_nb = (nat 0); outcome = None; player = (int 1)}; initial_config = params.initial_config; permissions = (Map.make []); settled = false; state = None; timeouts = (Map.make [])} {addr_players = map address int; constants = {bonds = map int (map nat nat); channel_id = bytes; game_nonce = string; model_id = bytes; play_delay = int; players_addr = map int address; settlements = map (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) (list {bonds = map nat nat; receiver = int; sender = int})}; current = {move_nb = nat; outcome = option (`final (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) + `pending (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int)); player = int}; initial_config = bytes; permissions = map string bytes; settled = bool; state = option bytes; timeouts = map int timestamp})

  let%entry_point on_received_bonds params =
    set_type params {amount = nat; data = bytes; receiver = address; sender = address; token_id = nat};
    verify (data.ledger = sender) ~msg:"Platform_NotLedger";
    verify (self_address = params.receiver) ~msg:"Platform_NotReceiver";
    let%mutable compute_game_platform_807 = (open_some ~message:"Platform_UnpackErr" (unpack params.data {channel_id = bytes; player_addr = address})) in ();
    let%mutable channel = (get ~message:%s data.channels compute_game_platform_807.channel_id ("Platform_ChannelNotFound: ", compute_game_platform_807.channel_id)) in ();
    verify (not channel.closed) ~msg:"Platform_ChannelClosed";
    let%mutable player = (get ~message:%s channel.players compute_game_platform_807.player_addr "Platform_NotChannelPlayer") in ();
    Map.set player.bonds params.token_id ((Map.get ~default_value:(nat 0) player.bonds params.token_id) + params.amount);
    Map.set (Map.get data.channels compute_game_platform_807.channel_id).players compute_game_platform_807.player_addr player

  let%entry_point withdraw_cancel params =
    let%mutable channel = (get ~message:%s data.channels params ("Platform_ChannelNotFound: ", params)) in ();
    verify (not channel.closed) ~msg:"Platform_ChannelClosed";
    verify (contains sender channel.players) ~msg:"Platform_SenderNotInChannel";
    let%mutable player = (Map.get channel.players sender) in ();
    verify (is_some player.withdraw) ~msg:"Platform_NoWithdrawOpened";
    verify (now >= (open_some player.withdraw).timeout) ~msg:"Platform_ChallengeDelayNotOver";
    (Map.get channel.players sender).withdraw <- None;
    Map.set data.channels params channel

  let%entry_point withdraw_challenge params =
    set_type params.game_ids (set bytes);
    let%mutable channel = (get ~message:%s data.channels params.channel_id ("Platform_ChannelNotFound: ", params.channel_id)) in ();
    verify (not channel.closed) ~msg:"Platform_ChannelClosed";
    verify (contains sender channel.players) ~msg:"Platform_SenderNotInChannel";
    verify (contains params.withdrawer channel.players) ~msg:"Platform_WithdrawerNotInChannel";
    verify (is_some (Map.get channel.players params.withdrawer).withdraw) ~msg:"Platform_NoWithdrawRequestOpened";
    let%mutable withdraw = (open_some (Map.get channel.players params.withdrawer).withdraw) in ();
    List.iter (fun game_id ->
      let%mutable game = (get ~message:%s data.games game_id ("Platform_GameNotFound: ", game_id)) in ();
      verify (game.constants.channel_id = params.channel_id) ~msg:"Platform_ChannelIdMismatch";
      if game.settled then
        (
          verify (contains game_id withdraw.challenge) ~msg:"Platform_GameSettled";
          Set.remove withdraw.challenge game_id;
          set_type withdraw.challenge_tokens (map nat int);
          set_type (Map.get game.constants.bonds (Map.get game.addr_players params.withdrawer)) (map nat nat);
          List.iter (fun token ->
            if contains token.key withdraw.challenge_tokens then
              (
                let%mutable compute_game_platform_1074 = ((Map.get withdraw.challenge_tokens token.key) - (to_int (Map.get (Map.get game.constants.bonds (Map.get game.addr_players params.withdrawer)) token.key))) in ();
                if compute_game_platform_1074 = (int 0) then
                  Map.delete withdraw.challenge_tokens token.key
                else
                  Map.set withdraw.challenge_tokens token.key compute_game_platform_1074
              )
            else
              Map.set withdraw.challenge_tokens token.key ((int 0) - (to_int (Map.get (Map.get game.constants.bonds (Map.get game.addr_players params.withdrawer)) token.key)))
          ) (Map.items (Map.get game.constants.bonds (Map.get game.addr_players params.withdrawer)))
        )
      else
        if not (contains game_id withdraw.challenge) then
          (
            Set.add withdraw.challenge game_id;
            set_type withdraw.challenge_tokens (map nat int);
            set_type (Map.get game.constants.bonds (Map.get game.addr_players params.withdrawer)) (map nat nat);
            List.iter (fun token ->
              if contains token.key withdraw.challenge_tokens then
                Map.set withdraw.challenge_tokens token.key ((Map.get withdraw.challenge_tokens token.key) + (to_int (Map.get (Map.get game.constants.bonds (Map.get game.addr_players params.withdrawer)) token.key)))
              else
                if not ((to_int (Map.get (Map.get game.constants.bonds (Map.get game.addr_players params.withdrawer)) token.key)) = (int 0)) then
                  Map.set withdraw.challenge_tokens token.key (to_int (Map.get (Map.get game.constants.bonds (Map.get game.addr_players params.withdrawer)) token.key))
            ) (Map.items (Map.get game.constants.bonds (Map.get game.addr_players params.withdrawer)))
          )
    ) (Set.elements params.game_ids);
    if params.withdrawer <> sender then
      withdraw.timeout <- now;
    (Map.get channel.players params.withdrawer).withdraw <- some withdraw;
    Map.set data.channels params.channel_id channel

  let%entry_point withdraw_finalize params =
    let%mutable channel = (get ~message:%s data.channels params ("Platform_ChannelNotFound: ", params)) in ();
    verify (not channel.closed) ~msg:"Platform_ChannelClosed";
    verify (contains sender channel.players) ~msg:"Platform_SenderNotInChannel";
    let%mutable player = (Map.get channel.players sender) in ();
    verify (is_some player.withdraw) ~msg:"Platform_NoWithdrawOpened";
    let%mutable withdraw = (open_some player.withdraw) in ();
    verify (now >= withdraw.timeout) ~msg:"Platform_ChallengeDelayNotOver";
    let%mutable txs = [] in ();
    List.iter (fun token ->
      if (token.value > (int 0)) && (contains token.key player.bonds) then
        verify (((Map.get player.bonds token.key) - (Map.get withdraw.tokens token.key)) > token.value) ~msg:"Platform_TokenChallenged"
    ) (Map.items withdraw.challenge_tokens);
    List.iter (fun token ->
      if token.value > (nat 0) then
        (
          verify (contains token.key player.bonds) ~msg:"Platform_NotEnoughTokens";
          Map.set player.bonds token.key (open_some ~message:"Platform_NotEnoughTokens" (is_nat ((Map.get player.bonds token.key) - token.value)));
          txs <- {to_ = sender; token_id = token.key; amount = token.value} :: txs
        )
    ) (Map.items withdraw.tokens);
    transfer [{from_ = address "KT1LX9dx1MMSBbG3seX7DHC7kd2tuKUmwyZs"; txs = txs}] (tez 0) (open_some ~message:"Platform_LedgerNotFound" (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) data.ledger , entry_point='transfer'));
    player.withdraw <- None;
    Map.set channel.players sender player;
    Map.set data.channels params channel

  let%entry_point withdraw_request params =
    let%mutable channel = (get ~message:%s data.channels params.channel_id ("Platform_ChannelNotFound: ", params.channel_id)) in ();
    verify (not channel.closed) ~msg:"Platform_ChannelClosed";
    let%mutable player = (get ~message:%s channel.players sender "Platform_SenderNotInChannel") in ();
    verify (is_variant "None" player.withdraw) ~msg:"Platform_AnotherWithdrawIsRunning";
    player.withdraw_id <- player.withdraw_id + (nat 1);
    player.withdraw <- some {challenge = (Set.make []); challenge_tokens = (Map.make []); timeout = (add_seconds now channel.withdraw_delay); tokens = params.tokens};
    Map.set channel.players sender player;
    Map.set data.channels params.channel_id channel

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admins = set address; channels = big_map bytes {closed = bool; nonce = string; players = map address {bonds = map nat nat; pk = key; withdraw = option {challenge = set bytes; challenge_tokens = map nat int; timeout = timestamp; tokens = map nat nat}; withdraw_id = nat}; withdraw_delay = int}; games = big_map bytes {addr_players = map address int; constants = {bonds = map int (map nat nat); channel_id = bytes; game_nonce = string; model_id = bytes; play_delay = int; players_addr = map int address; settlements = map (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) (list {bonds = map nat nat; receiver = int; sender = int})}; current = {move_nb = nat; outcome = option (`final (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int) + `pending (`game_aborted unit + `game_finished string + `player_double_played int + `player_inactive int)); player = int}; initial_config = bytes; permissions = map string bytes; settled = bool; state = option bytes; timeouts = map int timestamp}; ledger = address; metadata = big_map string bytes; model_metadata = big_map bytes (map string bytes); model_permissions = big_map bytes (map string bytes); models = big_map bytes {apply_ = lambda {move_data = bytes; move_nb = nat; player = int; state = bytes} (pair bytes (option string)); init = lambda bytes bytes; outcomes = list string}}]
      ~storage:[%expr
                 {admins = Set.make([address "tz1bQx5gL9WLBsc9qyxKj7wGiit78A7frBmK"]);
                  channels = Map.make [];
                  games = Map.make [];
                  ledger = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  metadata = Map.make [];
                  model_metadata = Map.make [];
                  model_permissions = Map.make [];
                  models = Map.make []}]
      [admin_game_permissions; admin_global_metadata; admin_model_metadata; admin_model_permissions; admin_new_model; admin_setup; admin_update_operators; dispute_double_signed; dispute_starved; dispute_starving; game_play; game_set_outcome; game_settle; new_channel; new_game; on_received_bonds; withdraw_cancel; withdraw_challenge; withdraw_finalize; withdraw_request]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())