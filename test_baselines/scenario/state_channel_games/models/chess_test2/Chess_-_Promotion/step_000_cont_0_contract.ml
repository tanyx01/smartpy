open SmartML

module Contract = struct
  let%entry_point play params =
    set_type params.move_nb nat;
    set_type params.move_data (`answer_stalemate (`accept unit + `answer {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}}) + `claim_stalemate unit + `play {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}});
    if data.current.player = (int 1) then
      verify (sender = data.constants.player1.addr) ~msg:"Game_WrongPlayer"
    else
      verify (sender = data.constants.player2.addr) ~msg:"Game_WrongPlayer";
    verify (is_variant "None" data.current.outcome);
    verify (data.current.move_nb = params.move_nb);
    let%mutable compute_game_tester_40 = ({move_data = params.move_data; move_nb = data.current.move_nb; player = data.current.player; state = data.state} data.apply_) in ();
    match_pair_game_tester_50_fst, match_pair_game_tester_50_snd = match_tuple(compute_game_tester_40, "match_pair_game_tester_50_fst", "match_pair_game_tester_50_snd")
    set_type match_pair_game_tester_50_snd (option bounded(["draw", "player_1_won", "player_2_won"], t=string));
    match match_pair_game_tester_50_snd with
      | `Some Some ->
        data.current.outcome <- some (unbound Some)
      | `None None ->
        data.current.outcome <- None
;
    data.current.move_nb <- data.current.move_nb + (nat 1);
    data.current.player <- (int 3) - data.current.player;
    data.state <- match_pair_game_tester_50_fst

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {apply_ = lambda {move_data = `answer_stalemate (`accept unit + `answer {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}}) + `claim_stalemate unit + `play {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}}; move_nb = nat; player = int; state = {board_state = {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes}; status = `claim_stalemate unit + `force_play unit + `play unit}} (pair {board_state = {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes}; status = `claim_stalemate unit + `force_play unit + `play unit} (option bounded(["draw", "player_1_won", "player_2_won"], t=string))); constants = {channel_id = bytes; game_nonce = string; loser = mutez; model_id = bytes; player1 = {addr = address; pk = key}; player2 = {addr = address; pk = key}; winner = mutez}; current = {move_nb = nat; outcome = option string; player = int}; state = {board_state = {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes}; status = `claim_stalemate unit + `force_play unit + `play unit}}]
      ~storage:[%expr
                 {apply_ = lambda;
                  constants = {channel_id = bytes "0x01"; game_nonce = ""; loser = tez 0; model_id = bytes "0x"; player1 = {addr = address "tz1RcgwVfqGhaYFyB1vYTSsJy9hBdBXDq43b"; pk = key "edpkvE7SvRKBubmAzEAQHrRUENMLA2dqQQLZwEWg4j2iQVhftBHKLX"}; player2 = {addr = address "tz1RJHgDYgResSRsFSJxt9onzx1vSZMyYjZL"; pk = key "edpktrqVE7L63E9CoQBiHo1YUxSQBa6D7EKXMPZTFGtN7PggFNFwJq"}; winner = tez 0};
                  current = {move_nb = nat 0; outcome = None; player = int 1};
                  state = {board_state = {castle = Map.make [(int ((-1)), Map.make [(int ((-1)), true); (int 1, true)]); (int 1, Map.make [(int ((-1)), true); (int 1, true)])]; check = false; deck = Map.make [(int 0, Map.make [(int 0, int 2); (int 1, int 3); (int 2, int 4); (int 3, int 5); (int 4, int 6); (int 5, int 4); (int 6, int 3); (int 7, int 2)]); (int 1, Map.make [(int 0, int 1); (int 1, int 1); (int 2, int 1); (int 3, int 1); (int 4, int 1); (int 5, int 1); (int 6, int 1); (int 7, int 1)]); (int 2, Map.make [(int 0, int 0); (int 1, int 0); (int 2, int 0); (int 3, int 0); (int 4, int 0); (int 5, int 0); (int 6, int 0); (int 7, int 0)]); (int 3, Map.make [(int 0, int 0); (int 1, int 0); (int 2, int 0); (int 3, int 0); (int 4, int 0); (int 5, int 0); (int 6, int 0); (int 7, int 0)]); (int 4, Map.make [(int 0, int 0); (int 1, int 0); (int 2, int 0); (int 3, int 0); (int 4, int 0); (int 5, int 0); (int 6, int 0); (int 7, int 0)]); (int 5, Map.make [(int 0, int 0); (int 1, int 0); (int 2, int 0); (int 3, int 0); (int 4, int 0); (int 5, int 0); (int 6, int 0); (int 7, int 0)]); (int 6, Map.make [(int 0, int ((-1))); (int 1, int ((-1))); (int 2, int ((-1))); (int 3, int ((-1))); (int 4, int ((-1))); (int 5, int ((-1))); (int 6, int ((-1))); (int 7, int ((-1)))]); (int 7, Map.make [(int 0, int ((-2))); (int 1, int ((-3))); (int 2, int ((-4))); (int 3, int ((-5))); (int 4, int ((-6))); (int 5, int ((-4))); (int 6, int ((-3))); (int 7, int ((-2)))])]; enPassant = None; fullMove = nat 1; halfMoveClock = nat 0; kings = Map.make [(int ((-1)), {i = int 7; j = int 4}); (int 1, {i = int 0; j = int 4})]; nextPlayer = int 1; pastMoves = Map.make [((nat 0, int ((-1))), bytes "0xbaf82e0f3ca2b90e482d6b6846873df9fee4e1baf369bc897bcef3b113a359df")]}; status = play}}]
      [play]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())