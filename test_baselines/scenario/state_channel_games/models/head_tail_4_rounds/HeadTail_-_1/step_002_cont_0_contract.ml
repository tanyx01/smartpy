open SmartML

module Contract = struct
  let%entry_point play params =
    set_type params.move_nb nat;
    set_type params.move_data (`hash bytes + `revealed nat);
    if data.current.player = (int 1) then
      verify (sender = data.constants.player1.addr) ~msg:"Game_WrongPlayer"
    else
      verify (sender = data.constants.player2.addr) ~msg:"Game_WrongPlayer";
    verify (is_variant "None" data.current.outcome);
    verify (data.current.move_nb = params.move_nb);
    let%mutable compute_game_tester_40 = ({move_data = params.move_data; move_nb = data.current.move_nb; player = data.current.player; state = data.state} data.apply_) in ();
    match_pair_game_tester_50_fst, match_pair_game_tester_50_snd = match_tuple(compute_game_tester_40, "match_pair_game_tester_50_fst", "match_pair_game_tester_50_snd")
    set_type match_pair_game_tester_50_snd (option bounded(["player_1_won", "player_2_won"], t=string));
    match match_pair_game_tester_50_snd with
      | `Some Some ->
        data.current.outcome <- some (unbound Some)
      | `None None ->
        data.current.outcome <- None
;
    data.current.move_nb <- data.current.move_nb + (nat 1);
    data.current.player <- (int 3) - data.current.player;
    data.state <- match_pair_game_tester_50_fst

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {apply_ = lambda {move_data = `hash bytes + `revealed nat; move_nb = nat; player = int; state = {hash1 = option bytes; hash2 = option bytes; revealed1 = option nat; revealed2 = option nat}} (pair {hash1 = option bytes; hash2 = option bytes; revealed1 = option nat; revealed2 = option nat} (option bounded(["player_1_won", "player_2_won"], t=string))); constants = {channel_id = bytes; game_nonce = string; loser = mutez; model_id = bytes; player1 = {addr = address; pk = key}; player2 = {addr = address; pk = key}; winner = mutez}; current = {move_nb = nat; outcome = option string; player = int}; state = {hash1 = option bytes; hash2 = option bytes; revealed1 = option nat; revealed2 = option nat}}]
      ~storage:[%expr
                 {apply_ = lambda;
                  constants = {channel_id = bytes "0x01"; game_nonce = ""; loser = tez 0; model_id = bytes "0x"; player1 = {addr = address "tz1RcgwVfqGhaYFyB1vYTSsJy9hBdBXDq43b"; pk = key "edpkvE7SvRKBubmAzEAQHrRUENMLA2dqQQLZwEWg4j2iQVhftBHKLX"}; player2 = {addr = address "tz1RJHgDYgResSRsFSJxt9onzx1vSZMyYjZL"; pk = key "edpktrqVE7L63E9CoQBiHo1YUxSQBa6D7EKXMPZTFGtN7PggFNFwJq"}; winner = tez 0};
                  current = {move_nb = nat 0; outcome = None; player = int 1};
                  state = {hash1 = None; hash2 = None; revealed1 = None; revealed2 = None}}]
      [play]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())