open SmartML

module Contract = struct
  let%entry_point f params =
    trace (params * 2);
    trace "double"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [f]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())