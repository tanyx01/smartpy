import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(administrator = sp.TAddress, ledger = sp.TBigMap(sp.TNat, sp.TAddress), metadata = sp.TBigMap(sp.TString, sp.TBytes), next_token_id = sp.TNat, operators = sp.TBigMap(sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), sp.TUnit), token_metadata = sp.TBigMap(sp.TNat, sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info")))).layout(("administrator", ("ledger", ("metadata", ("next_token_id", ("operators", "token_metadata")))))))
    self.init(administrator = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
              ledger = {0 : sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), 1 : sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), 2 : sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi')},
              metadata = {'' : sp.bytes('0x68747470733a2f2f6578616d706c652e636f6d')},
              next_token_id = 3,
              operators = {},
              token_metadata = {0 : sp.record(token_id = 0, token_info = {'decimals' : sp.bytes('0x31'), 'name' : sp.bytes('0x546f6b656e205a65726f'), 'symbol' : sp.bytes('0x546f6b30')}), 1 : sp.record(token_id = 1, token_info = {'decimals' : sp.bytes('0x31'), 'name' : sp.bytes('0x546f6b656e204f6e65'), 'symbol' : sp.bytes('0x546f6b31')}), 2 : sp.record(token_id = 2, token_info = {'decimals' : sp.bytes('0x31'), 'name' : sp.bytes('0x546f6b656e2054776f'), 'symbol' : sp.bytes('0x546f6b32')})})

  @sp.entry_point
  def balance_of(self, params):
    sp.set_type(params, sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")))
    def f_x0(_x0):
      sp.verify(_x0.token_id < self.data.next_token_id, 'FA2_TOKEN_UNDEFINED')
      sp.result(sp.record(request = sp.record(owner = _x0.owner, token_id = _x0.token_id), balance = sp.eif(self.data.ledger[_x0.token_id] == _x0.owner, 1, 0)))
    sp.transfer(params.requests.map(sp.build_lambda(f_x0)), sp.tez(0), params.callback)

  @sp.entry_point
  def mint(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    compute_fa2_nft_minimal_161 = sp.local("compute_fa2_nft_minimal_161", self.data.next_token_id)
    self.data.token_metadata[compute_fa2_nft_minimal_161.value] = sp.record(token_id = compute_fa2_nft_minimal_161.value, token_info = params.metadata)
    self.data.ledger[compute_fa2_nft_minimal_161.value] = params.to_
    self.data.next_token_id += 1

  @sp.entry_point
  def transfer(self, params):
    sp.for transfer in params:
      sp.for tx in transfer.txs:
        sp.set_type(tx, sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))
        sp.verify(tx.token_id < self.data.next_token_id, 'FA2_TOKEN_UNDEFINED')
        sp.verify((transfer.from_ == sp.sender) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))), 'FA2_NOT_OPERATOR')
        sp.if tx.amount > 0:
          sp.verify((tx.amount == 1) & (self.data.ledger[tx.token_id] == transfer.from_), 'FA2_INSUFFICIENT_BALANCE')
          self.data.ledger[tx.token_id] = tx.to_

  @sp.entry_point
  def update_operators(self, params):
    sp.for update in params:
      with update.match_cases() as arg:
        with arg.match('add_operator') as add_operator:
          sp.verify(add_operator.owner == sp.sender, 'FA2_NOT_OWNER')
          self.data.operators[add_operator] = sp.unit
        with arg.match('remove_operator') as remove_operator:
          sp.verify(remove_operator.owner == sp.sender, 'FA2_NOT_OWNER')
          del self.data.operators[remove_operator]


sp.add_compilation_target("test", Contract())