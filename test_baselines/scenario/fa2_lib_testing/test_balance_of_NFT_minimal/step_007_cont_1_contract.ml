open SmartML

module Contract = struct
  let%entry_point receive_balances params =
    set_type params (list {balance = nat; request = {owner = address; token_id = nat}});
    List.iter (fun resp ->
      if contains sender data.last_known_balances then
        Map.set (Map.get data.last_known_balances sender) (resp.request.owner, resp.request.token_id) resp.balance
      else
        Map.set data.last_known_balances sender (Map.make [((resp.request.owner, resp.request.token_id), resp.balance)])
    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {last_known_balances = big_map address (map (pair address nat) nat)}]
      ~storage:[%expr
                 {last_known_balances = Map.make []}]
      [receive_balances]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())