open SmartML

module Contract = struct
  let%entry_point balance_of params =
    set_type params {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}};
    transfer (map (fun _x0 -> verify (_x0.token_id < data.next_token_id) ~msg:"FA2_TOKEN_UNDEFINED";
result {request = {owner = _x0.owner; token_id = _x0.token_id}; balance = (Map.get ~default_value:(nat 0) data.ledger (_x0.owner, _x0.token_id))}) params.requests) (tez 0) params.callback

  let%entry_point mint params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    match params.token with
      | `new new ->
        let%mutable compute_fa2_fungible_minimal_163 = data.next_token_id in ();
        Map.set data.token_metadata compute_fa2_fungible_minimal_163 {token_id = compute_fa2_fungible_minimal_163; token_info = new};
        Map.set data.supply compute_fa2_fungible_minimal_163 params.amount;
        Map.set data.ledger (params.to_, compute_fa2_fungible_minimal_163) params.amount;
        data.next_token_id <- data.next_token_id + (nat 1)
      | `existing existing ->
        verify (existing < data.next_token_id) ~msg:"FA2_TOKEN_UNDEFINED";
        Map.set data.supply existing ((Map.get data.supply existing) + params.amount);
        Map.set data.ledger (params.to_, existing) ((Map.get ~default_value:(nat 0) data.ledger (params.to_, existing)) + params.amount)


  let%entry_point transfer params =
    List.iter (fun transfer ->
      List.iter (fun tx ->
        set_type tx {amount = nat; to_ = address; token_id = nat};
        verify (tx.token_id < data.next_token_id) ~msg:"FA2_TOKEN_UNDEFINED";
        verify ((transfer.from_ = sender) || (contains {owner = transfer.from_; operator = sender; token_id = tx.token_id} data.operators)) ~msg:"FA2_NOT_OPERATOR";
        Map.set data.ledger (transfer.from_, tx.token_id) (open_some ~message:"FA2_INSUFFICIENT_BALANCE" (is_nat ((Map.get ~default_value:(nat 0) data.ledger (transfer.from_, tx.token_id)) - tx.amount)));
        Map.set data.ledger (tx.to_, tx.token_id) ((Map.get ~default_value:(nat 0) data.ledger (tx.to_, tx.token_id)) + tx.amount)
      ) transfer.txs
    ) params

  let%entry_point update_operators params =
    List.iter (fun update ->
      match update with
        | `add_operator add_operator ->
          verify (add_operator.owner = sender) ~msg:"FA2_NOT_OWNER";
          Map.set data.operators add_operator ()
        | `remove_operator remove_operator ->
          verify (remove_operator.owner = sender) ~msg:"FA2_NOT_OWNER";
          Map.delete data.operators remove_operator

    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {administrator = address; ledger = big_map (pair address nat) nat; metadata = big_map string bytes; next_token_id = nat; operators = big_map {operator = address; owner = address; token_id = nat} unit; supply = big_map nat nat; token_metadata = big_map nat {token_id = nat; token_info = map string bytes}}]
      ~storage:[%expr
                 {administrator = address "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w";
                  ledger = Map.make [((address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi", nat 0), nat 42); ((address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi", nat 1), nat 42); ((address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi", nat 2), nat 42)];
                  metadata = Map.make [("", bytes "0x68747470733a2f2f6578616d706c652e636f6d")];
                  next_token_id = nat 3;
                  operators = Map.make [];
                  supply = Map.make [];
                  token_metadata = Map.make [(nat 0, {token_id = nat 0; token_info = Map.make [("decimals", bytes "0x31"); ("name", bytes "0x546f6b656e205a65726f"); ("symbol", bytes "0x546f6b30")]}); (nat 1, {token_id = nat 1; token_info = Map.make [("decimals", bytes "0x31"); ("name", bytes "0x546f6b656e204f6e65"); ("symbol", bytes "0x546f6b31")]}); (nat 2, {token_id = nat 2; token_info = Map.make [("decimals", bytes "0x31"); ("name", bytes "0x546f6b656e2054776f"); ("symbol", bytes "0x546f6b32")]})]}]
      [balance_of; mint; transfer; update_operators]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())