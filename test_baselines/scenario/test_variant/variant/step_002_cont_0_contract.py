import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(r = sp.TInt, s = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y")), x = sp.TVariant(A = sp.TInt, B = sp.TInt, C = sp.TIntOrNat, Toto = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y"))).layout((("A", "B"), ("C", "Toto"))), y = sp.TOption(sp.TInt), z = sp.TVariant(Left = sp.TInt, Right = sp.TInt).layout(("Left", "Right"))).layout(("r", ("s", ("x", ("y", "z"))))))
    self.init(r = 0,
              s = sp.record(x = 0, y = 1),
              x = A(-1),
              y = sp.some(-42),
              z = Left(-10))

  @sp.entry_point
  def ep1(self):
    with self.data.x.match('A') as A:
      self.data.x = variant('B', -2)
      self.data.r = A
    self.data.x = variant('C', 3)

  @sp.entry_point
  def ep3(self):
    with self.data.z.match('Left') as Left:
      self.data.r = Left

  @sp.entry_point
  def ep4(self):
    with self.data.x.match('A') as a1:
      with self.data.z.match('Right') as a2:
        self.data.r = a1 + a2

  @sp.entry_point
  def ep5(self):
    sp.if self.data.x.is_variant('Toto'):
      self.data.r = 42

  @sp.entry_point
  def ep6(self):
    self.data.s = self.data.x.open_variant('Toto', message = 'no toto')

  @sp.entry_point
  def ep7(self, params):
    self.data.x = params

  @sp.entry_point
  def ep8(self, params):
    with params.x.match_cases() as arg:
      with arg.match('A') as A:
        self.data.x = params.x
      with arg.match('B') as B:
        self.data.y = sp.some((12 + B) + params.y)


  @sp.entry_point
  def ep9(self, params):
    sp.set_type(params.other, sp.TInt)
    with params.z.match('A') as A:
      self.data.x = params.z

  @sp.entry_point
  def options(self):
    sp.if self.data.y.is_some():
      self.data.r = 44 + self.data.y.open_some(message = 'Not a some!')
      self.data.y = sp.none
    sp.else:
      self.data.r = 3
      self.data.y = sp.some(12)

sp.add_compilation_target("test", Contract())