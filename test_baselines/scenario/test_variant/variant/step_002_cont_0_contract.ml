open SmartML

module Contract = struct
  let%entry_point ep1 () =
    with data.x.match('A') as A:
      data.x <- variant B (int ((-2)));
      data.r <- A;
    data.x <- variant C 3

  let%entry_point ep3 () =
    with data.z.match('Left') as Left:
      data.r <- Left

  let%entry_point ep4 () =
    with data.x.match('A') as a1:
      with data.z.match('Right') as a2:
        data.r <- a1 + a2

  let%entry_point ep5 () =
    if is_variant "Toto" data.x then
      data.r <- int 42

  let%entry_point ep6 () =
    data.s <- open_variant data.x Toto ~message:"no toto"

  let%entry_point ep7 params =
    data.x <- params

  let%entry_point ep8 params =
    match params.x with
      | `A A ->
        data.x <- params.x
      | `B B ->
        data.y <- some (((int 12) + B) + params.y)


  let%entry_point ep9 params =
    set_type params.other int;
    with params.z.match('A') as A:
      data.x <- params.z

  let%entry_point options () =
    if is_some data.y then
      (
        data.r <- (int 44) + (open_some ~message:"Not a some!" data.y);
        data.y <- None
      )
    else
      (
        data.r <- int 3;
        data.y <- some (int 12)
      )

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {r = int; s = {x = intOrNat; y = intOrNat}; x = `A int + `B int + `C intOrNat + `Toto {x = intOrNat; y = intOrNat}; y = option int; z = `Left int + `Right int}]
      ~storage:[%expr
                 {r = int 0;
                  s = {x = 0; y = 1};
                  x = A(int ((-1)));
                  y = Some(int ((-42)));
                  z = Left(int ((-10)))}]
      [ep1; ep3; ep4; ep5; ep6; ep7; ep8; ep9; options]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())