Comment...
 h1: Basic scenario.
Comment...
 h2: Origination
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair {"tz1Qi4ShtraJF1xagU5kRUjU6xzXYS5ufnXm"; "tz1RnNX4gzQTy6wDFHNGV7ewuXS2Po7cpC3W"; "tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx"} (Pair {} (Pair 2 {})))
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_002_cont_0_storage.tz 1
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_002_cont_0_storage.json 7
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_002_cont_0_storage.py 1
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_002_cont_0_types.py 7
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_002_cont_0_contract.tz 141
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_002_cont_0_contract.json 141
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_002_cont_0_contract.py 25
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_002_cont_0_contract.ml 31
Comment...
 h2: submit_proposal
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_004_cont_0_params.py 1
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_004_cont_0_params.tz 1
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_004_cont_0_params.json 1
Executing submit_proposal(sp.bytes('0x42'))...
 -> (Pair {"tz1Qi4ShtraJF1xagU5kRUjU6xzXYS5ufnXm"; "tz1RnNX4gzQTy6wDFHNGV7ewuXS2Po7cpC3W"; "tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx"} (Pair {Elt 0x42 False} (Pair 2 {Elt 0x42 {}})))
Comment...
 h2: vote_proposal
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_006_cont_0_params.py 1
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_006_cont_0_params.tz 1
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_006_cont_0_params.json 1
Executing vote_proposal(sp.bytes('0x42'))...
 -> (Pair {"tz1Qi4ShtraJF1xagU5kRUjU6xzXYS5ufnXm"; "tz1RnNX4gzQTy6wDFHNGV7ewuXS2Po7cpC3W"; "tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx"} (Pair {Elt 0x42 False} (Pair 2 {Elt 0x42 {"tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx"}})))
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_007_cont_0_params.py 1
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_007_cont_0_params.tz 1
 => test_baselines/scenario/multisig_view/MultisigView_basic_scenario/step_007_cont_0_params.json 1
Executing vote_proposal(sp.bytes('0x42'))...
 -> (Pair {"tz1Qi4ShtraJF1xagU5kRUjU6xzXYS5ufnXm"; "tz1RnNX4gzQTy6wDFHNGV7ewuXS2Po7cpC3W"; "tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx"} (Pair {Elt 0x42 True} (Pair 2 {Elt 0x42 {"tz1Qi4ShtraJF1xagU5kRUjU6xzXYS5ufnXm"; "tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx"}})))
Verifying sp.contract_view(0, "is_voted", sp.bytes('0x42')).open_some(message = 'View is_voted is invalid!')...
 OK
