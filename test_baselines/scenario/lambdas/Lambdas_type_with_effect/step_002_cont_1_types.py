import smartpy as sp

tstorage = sp.TRecord(result = sp.TOption(sp.TLambda(sp.TUnit, sp.TUnit, with_operations=True))).layout("result")
tparameter = sp.TVariant(exec_lambda = sp.TLambda(sp.TUnit, sp.TUnit, with_operations=True), nothing = sp.TUnit).layout(("exec_lambda", "nothing"))
tprivates = { }
tviews = { }
