import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(abcd = sp.TInt, f = sp.TLambda(sp.TIntOrNat, sp.TIntOrNat), fff = sp.TOption(sp.TLambda(sp.TNat, sp.TNat)), ggg = sp.TOption(sp.TIntOrNat), value = sp.TNat).layout(("abcd", ("f", ("fff", ("ggg", "value"))))))
    self.init(abcd = 0,
              f = sp.build_lambda(lambda _x0: _x0 + 1),
              fff = sp.none,
              ggg = sp.some(42),
              value = 0)

  @sp.entry_point
  def abs_test(self, params):
    self.data.abcd = self.abs(params)

  @sp.entry_point
  def comp_test(self):
    self.data.abcd = self.comp(sp.record(f = sp.build_lambda(lambda _x14: _x14 + 3), x = 2))

  @sp.entry_point
  def f(self):
    toto = sp.local("toto", sp.build_lambda(lambda _x16: sp.fst(_x16) + sp.snd(_x16)))
    titi = sp.local("titi", toto.value.apply(5))
    self.data.value = titi.value(8)

  @sp.entry_point
  def fact(self, params):
    compute_lambdas_133 = sp.local("compute_lambdas_133", sp.build_lambda(lambda _x18: sp.eif(_x18 <= 1, 1, _x18 * _f19(_x18 - 1))))
    self.data.abcd = compute_lambdas_133.value(params)

  @sp.entry_point
  def flambda(self):
    self.data.value = self.flam(self.flam(15)) + self.square_root(12345)

  @sp.entry_point
  def h(self):
    def f_x20(_x20):
      sp.verify(_x20 >= 0)
      y = sp.local("y", _x20)
      sp.while (y.value * y.value) > _x20:
        y.value = ((_x20 // y.value) + y.value) // 2
      sp.verify(((y.value * y.value) <= _x20) & (_x20 < ((y.value + 1) * (y.value + 1))))
      sp.result(y.value)
    self.data.fff = sp.some(sp.build_lambda(f_x20))

  @sp.entry_point
  def hh(self, params):
    self.data.value = self.data.fff.open_some()(params)

  @sp.entry_point
  def i(self):
    def f_x22(_x22):
      sp.verify(_x22 >= 0)
    ch1 = sp.local("ch1", sp.build_lambda(f_x22))
    def f_x24(_x24):
      sp.verify(_x24 >= 0)
      sp.result(_x24 - 2)
    ch2 = sp.local("ch2", sp.build_lambda(f_x24))
    def f_x26(_x26):
      sp.verify(_x26 >= 0)
      sp.result(True)
    ch3 = sp.local("ch3", sp.build_lambda(f_x26))
    def f_x28(_x28):
      def f_x30(_x30):
        sp.verify(_x30 >= 0)
        sp.result(False)
      ch3b = sp.local("ch3b", sp.build_lambda(f_x30))
      sp.verify(_x28 >= 0)
      sp.result(3 * _x28)
    ch4 = sp.local("ch4", sp.build_lambda(f_x28))
    self.data.value = ch4.value(12)
    compute_lambdas_96 = sp.local("compute_lambdas_96", self.not_pure(sp.unit))
    sp.verify(compute_lambdas_96.value == self.data.value)

  @sp.entry_point
  def operation_creation(self):
    def f_x32(_x32):
      __operations__ = sp.local("__operations__", sp.list([]), sp.TList(sp.TOperation))
      create_contract_lambdas_101 = sp.local("create_contract_lambdas_101", create contract ...)
      sp.operations().push(create_contract_lambdas_101.value.operation)
      create_contract_lambdas_102 = sp.local("create_contract_lambdas_102", create contract ...)
      sp.operations().push(create_contract_lambdas_102.value.operation)
      sp.result(sp.operations())
    f = sp.local("f", sp.build_lambda(f_x32))
    sp.for op in f.value(12345001):
      sp.operations().push(op)
    sp.for op in f.value(12345002):
      sp.operations().push(op)

  @sp.entry_point
  def operation_creation_result(self):
    def f_x34(_x34):
      __operations__ = sp.local("__operations__", sp.list([]), sp.TList(sp.TOperation))
      create_contract_lambdas_110 = sp.local("create_contract_lambdas_110", create contract ...)
      sp.operations().push(create_contract_lambdas_110.value.operation)
      __s1 = sp.local("__s1", 4)
      sp.result((sp.operations(), __s1.value))
    f = sp.local("f", sp.build_lambda(f_x34))
    x = sp.local("x", f.value(12345001))
    y = sp.local("y", f.value(12345002))
    sp.for op in sp.fst(x.value):
      sp.operations().push(op)
    sp.for op in sp.fst(y.value):
      sp.operations().push(op)
    sum = sp.local("sum", sp.snd(x.value) + sp.snd(y.value))

  @sp.private_lambda()
  def abs(_x2):
    sp.if _x2 > 0:
      sp.result(_x2)
    sp.else:
      sp.result(- _x2)

  @sp.private_lambda()
  def comp(_x4):
    sp.result(_x4.f(_x4.x))

  @sp.private_lambda()
  def flam(_x6):
    sp.result(322 * _x6)

  @sp.private_lambda()
  def not_pure(_x8):
    sp.result(self.data.value)

  @sp.private_lambda()
  def oh_no(_x10):
    with sp.set_result_type(sp.TInt):
      sp.if _x10 > 0:
        sp.failwith('too big')
      sp.else:
        sp.failwith('too small')

  @sp.private_lambda()
  def square_root(_x12):
    sp.verify(_x12 >= 0)
    y = sp.local("y", _x12)
    sp.while (y.value * y.value) > _x12:
      y.value = ((_x12 // y.value) + y.value) // 2
    sp.verify(((y.value * y.value) <= _x12) & (_x12 < ((y.value + 1) * (y.value + 1))))
    sp.result(y.value)

sp.add_compilation_target("test", Contract())