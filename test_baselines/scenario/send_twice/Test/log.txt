Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> Unit
 => test_baselines/scenario/send_twice/Test/step_000_cont_0_storage.tz 1
 => test_baselines/scenario/send_twice/Test/step_000_cont_0_storage.json 1
 => test_baselines/scenario/send_twice/Test/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario/send_twice/Test/step_000_cont_0_storage.py 1
 => test_baselines/scenario/send_twice/Test/step_000_cont_0_types.py 7
 => test_baselines/scenario/send_twice/Test/step_000_cont_0_contract.tz 49
 => test_baselines/scenario/send_twice/Test/step_000_cont_0_contract.json 47
 => test_baselines/scenario/send_twice/Test/step_000_cont_0_contract.py 13
 => test_baselines/scenario/send_twice/Test/step_000_cont_0_contract.ml 19
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 -> Unit
 => test_baselines/scenario/send_twice/Test/step_001_cont_1_storage.tz 1
 => test_baselines/scenario/send_twice/Test/step_001_cont_1_storage.json 1
 => test_baselines/scenario/send_twice/Test/step_001_cont_1_sizes.csv 2
 => test_baselines/scenario/send_twice/Test/step_001_cont_1_storage.py 1
 => test_baselines/scenario/send_twice/Test/step_001_cont_1_types.py 7
 => test_baselines/scenario/send_twice/Test/step_001_cont_1_contract.tz 42
 => test_baselines/scenario/send_twice/Test/step_001_cont_1_contract.json 54
 => test_baselines/scenario/send_twice/Test/step_001_cont_1_contract.py 13
 => test_baselines/scenario/send_twice/Test/step_001_cont_1_contract.ml 19
 => test_baselines/scenario/send_twice/Test/step_002_cont_0_params.py 1
 => test_baselines/scenario/send_twice/Test/step_002_cont_0_params.tz 1
 => test_baselines/scenario/send_twice/Test/step_002_cont_0_params.json 1
Executing send(sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'))...
 -> Unit
  + Transfer
     params: sp.record(receiver_balance = sp.tez(1), sender_balance = sp.tez(110))
     amount: sp.tez(1)
     to:     sp.contract(sp.TRecord(receiver_balance = sp.TMutez, sender_balance = sp.TMutez).layout(("receiver_balance", "sender_balance")), sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF%receive_and_check')).open_some()
  + Transfer
     params: sp.record(receiver_balance = sp.tez(11), sender_balance = sp.tez(100))
     amount: sp.tez(10)
     to:     sp.contract(sp.TRecord(receiver_balance = sp.TMutez, sender_balance = sp.TMutez).layout(("receiver_balance", "sender_balance")), sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF%receive_and_check')).open_some()
Executing (queue) receive_and_check(sp.record(receiver_balance = sp.tez(1), sender_balance = sp.tez(110)))...
 -> Unit
Executing (queue) receive_and_check(sp.record(receiver_balance = sp.tez(11), sender_balance = sp.tez(100)))...
 -> Unit
