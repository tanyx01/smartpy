import smartpy as sp

tstorage = sp.TUnit
tparameter = sp.TVariant(receive_and_check = sp.TRecord(receiver_balance = sp.TMutez, sender_balance = sp.TMutez).layout(("receiver_balance", "sender_balance"))).layout("receive_and_check")
tprivates = { }
tviews = { }
