import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def send(self, params):
    sp.transfer(sp.record(receiver_balance = sp.tez(1), sender_balance = sp.tez(110)), sp.tez(1), sp.contract(sp.TRecord(receiver_balance = sp.TMutez, sender_balance = sp.TMutez).layout(("receiver_balance", "sender_balance")), params, entry_point='receive_and_check').open_some())
    sp.transfer(sp.record(receiver_balance = sp.tez(11), sender_balance = sp.tez(100)), sp.tez(10), sp.contract(sp.TRecord(receiver_balance = sp.TMutez, sender_balance = sp.TMutez).layout(("receiver_balance", "sender_balance")), params, entry_point='receive_and_check').open_some())

sp.add_compilation_target("test", Contract())