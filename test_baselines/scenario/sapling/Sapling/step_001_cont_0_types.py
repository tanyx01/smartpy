import smartpy as sp

tstorage = sp.TRecord(h = sp.TLambda(sp.TRecord(a = sp.TSaplingState(12), b = sp.TSaplingTransaction(12)).layout(("a", "b")), sp.TOption(sp.TTuple(sp.TBytes, sp.TInt, sp.TSaplingState(12)))), x = sp.TIntOrNat, y = sp.TOption(sp.TTuple(sp.TBytes, sp.TInt, sp.TSaplingState(15))), z = sp.TLambda(sp.TIntOrNat, sp.TPair(sp.TIntOrNat, sp.TSaplingState(8)))).layout(("h", ("x", ("y", "z"))))
tparameter = sp.TVariant(entry_point_1 = sp.TRecord(s = sp.TSaplingState(15), t = sp.TSaplingTransaction(15)).layout(("s", "t"))).layout("entry_point_1")
tprivates = { }
tviews = { }
