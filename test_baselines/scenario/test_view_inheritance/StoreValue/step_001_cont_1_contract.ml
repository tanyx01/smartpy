open SmartML

module Contract = struct
  let%entry_point add params =
    data.storedValue <- data.storedValue + params.value

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {storedValue = intOrNat}]
      ~storage:[%expr
                 {storedValue = 14}]
      [add]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())