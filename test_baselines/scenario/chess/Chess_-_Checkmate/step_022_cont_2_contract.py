import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(board_state = sp.TRecord(castle = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TBool)), check = sp.TBool, deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), enPassant = sp.TOption(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))), fullMove = sp.TNat, halfMoveClock = sp.TNat, kings = sp.TMap(sp.TInt, sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))), nextPlayer = sp.TInt, pastMoves = sp.TMap(sp.TPair(sp.TNat, sp.TInt), sp.TBytes)).layout(("castle", ("check", ("deck", ("enPassant", ("fullMove", ("halfMoveClock", ("kings", ("nextPlayer", "pastMoves"))))))))), draw_offer = sp.TSet(sp.TAddress), metadata = sp.TBigMap(sp.TString, sp.TBytes), players = sp.TSet(sp.TAddress), players_map = sp.TMap(sp.TInt, sp.TAddress), status = sp.TVariant(claim_stalemate = sp.TUnit, finished = sp.TBounded(['draw', 'player_1_won', 'player_2_won'], t=sp.TString), force_play = sp.TUnit, play = sp.TUnit).layout((("claim_stalemate", "finished"), ("force_play", "play")))).layout(("board_state", ("draw_offer", ("metadata", ("players", ("players_map", "status")))))))
    self.init(board_state = sp.record(castle = {-1 : {-1 : True, 1 : True}, 1 : {-1 : True, 1 : True}}, check = False, deck = {0 : {0 : 2, 1 : 3, 2 : 4, 3 : 5, 4 : 6, 5 : 4, 6 : 3, 7 : 2}, 1 : {0 : 1, 1 : 1, 2 : 1, 3 : 1, 4 : 1, 5 : 1, 6 : 1, 7 : 1}, 2 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 3 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 4 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 5 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 6 : {0 : -1, 1 : -1, 2 : -1, 3 : -1, 4 : -1, 5 : -1, 6 : -1, 7 : -1}, 7 : {0 : -2, 1 : -3, 2 : -4, 3 : -5, 4 : -6, 5 : -4, 6 : -3, 7 : -2}}, enPassant = sp.none, fullMove = 1, halfMoveClock = 0, kings = {-1 : sp.record(i = 7, j = 4), 1 : sp.record(i = 0, j = 4)}, nextPlayer = 1, pastMoves = {(0, -1) : sp.bytes('0xbaf82e0f3ca2b90e482d6b6846873df9fee4e1baf369bc897bcef3b113a359df')}),
              draw_offer = sp.set([]),
              metadata = {},
              players = sp.set([sp.address('tz1RJHgDYgResSRsFSJxt9onzx1vSZMyYjZL'), sp.address('tz1RcgwVfqGhaYFyB1vYTSsJy9hBdBXDq43b')]),
              players_map = {-1 : sp.address('tz1RJHgDYgResSRsFSJxt9onzx1vSZMyYjZL'), 1 : sp.address('tz1RcgwVfqGhaYFyB1vYTSsJy9hBdBXDq43b')},
              status = play)

  @sp.entry_point
  def answer_stalemate(self, params):
    sp.set_type(params, sp.TVariant(accept = sp.TUnit, refuse = sp.TRecord(claim_repeat = sp.TOption(sp.TPair(sp.TNat, sp.TNat)), f = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")), promotion = sp.TOption(sp.TNat), t = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))).layout(("claim_repeat", ("f", ("promotion", "t"))))).layout(("accept", "refuse")))
    sp.verify(sp.sender == self.data.players_map[self.data.board_state.nextPlayer], 'Wrong player')
    sp.verify(self.data.status.is_variant('claim_stalemate'))
    with params.match_cases() as arg:
      with arg.match('accept') as accept:
        self.data.status = variant('finished', 'draw')
      with arg.match('refuse') as refuse:
        board_state = sp.local("board_state", self.data.board_state)
        is_draw = sp.local("is_draw", False)
        sp.set_type(refuse, sp.TRecord(claim_repeat = sp.TOption(sp.TPair(sp.TNat, sp.TNat)), f = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")), promotion = sp.TOption(sp.TNat), t = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))).layout(("claim_repeat", ("f", ("promotion", "t")))))
        sp.set_type(board_state.value, sp.TRecord(castle = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TBool)), check = sp.TBool, deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), enPassant = sp.TOption(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))), fullMove = sp.TNat, halfMoveClock = sp.TNat, kings = sp.TMap(sp.TInt, sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))), nextPlayer = sp.TInt, pastMoves = sp.TMap(sp.TPair(sp.TNat, sp.TInt), sp.TBytes)).layout(("castle", ("check", ("deck", ("enPassant", ("fullMove", ("halfMoveClock", ("kings", ("nextPlayer", "pastMoves"))))))))))
        sp.verify((refuse.f.i >= 0) & (refuse.f.i < 8))
        sp.verify((refuse.f.j >= 0) & (refuse.f.j < 8))
        sp.verify((refuse.t.i >= 0) & (refuse.t.i < 8))
        sp.verify((refuse.t.j >= 0) & (refuse.t.j < 8))
        sp.verify(((refuse.t.j - refuse.f.j) != 0) | ((refuse.t.i - refuse.f.i) != 0))
        compute_chess_logic_386 = sp.local("compute_chess_logic_386", board_state.value.nextPlayer)
        deck = sp.local("deck", board_state.value.deck)
        sp.verify(sp.sign(deck.value[refuse.f.i][refuse.f.j]) == compute_chess_logic_386.value)
        sp.verify(sp.sign(deck.value[refuse.t.i][refuse.t.j]) != compute_chess_logic_386.value)
        sp.verify(deck.value[refuse.t.i][refuse.t.j] != (6 * compute_chess_logic_386.value))
        resetClock = sp.local("resetClock", False)
        promotion = sp.local("promotion", False)
        sp.if (abs(deck.value[refuse.f.i][refuse.f.j])) == 6:
          sp.if ((abs(refuse.t.j - refuse.f.j)) == 2) & ((refuse.t.i - refuse.f.i) == 0):
            sp.verify((((compute_chess_logic_386.value == 1) & (refuse.f.i == 0)) & (refuse.f.j == 4)) | (((compute_chess_logic_386.value == (-1)) & (refuse.f.i == 7)) & (refuse.f.j == 4)))
            sp.verify(board_state.value.castle[compute_chess_logic_386.value][sp.sign(refuse.t.j - refuse.f.j)])
            sp.if (refuse.t.j - refuse.f.j) > 0:
              deck.value[refuse.t.i][7] = 0
            sp.else:
              deck.value[refuse.t.i][0] = 0
            deck.value[refuse.t.i][refuse.t.j - sp.sign(refuse.t.j - refuse.f.j)] = 2 * compute_chess_logic_386.value
          sp.else:
            sp.verify(((abs(refuse.t.j - refuse.f.j)) <= 1) & ((abs(refuse.t.i - refuse.f.i)) <= 1))
          board_state.value.kings[compute_chess_logic_386.value] = sp.record(i = refuse.t.i, j = refuse.t.j)
          board_state.value.castle[compute_chess_logic_386.value] = {-1 : False, 1 : False}
        sp.if (abs(deck.value[refuse.f.i][refuse.f.j])) == 2:
          sp.verify(((refuse.t.j - refuse.f.j) == 0) | ((refuse.t.i - refuse.f.i) == 0))
          sp.for k in sp.range(1, sp.to_int(sp.max(abs(refuse.t.j - refuse.f.j), abs(refuse.t.i - refuse.f.i))) - 1):
            sp.verify(deck.value[refuse.f.i + (k * sp.sign(refuse.t.i - refuse.f.i))][refuse.f.j + (k * sp.sign(refuse.t.j - refuse.f.j))] == 0)
          sp.if (((compute_chess_logic_386.value == 1) & (refuse.f.i == 0)) & (refuse.f.j == 0)) | (((compute_chess_logic_386.value == (-1)) & (refuse.f.i == 7)) & (refuse.f.j == 0)):
            board_state.value.castle[compute_chess_logic_386.value][-1] = False
          sp.if (((compute_chess_logic_386.value == 1) & (refuse.f.i == 0)) & (refuse.f.j == 7)) | (((compute_chess_logic_386.value == (-1)) & (refuse.f.i == 7)) & (refuse.f.j == 7)):
            board_state.value.castle[compute_chess_logic_386.value][1] = False
        sp.if (abs(deck.value[refuse.f.i][refuse.f.j])) == 5:
          sp.verify((((refuse.t.j - refuse.f.j) == 0) | ((refuse.t.i - refuse.f.i) == 0)) | ((abs(refuse.t.j - refuse.f.j)) == (abs(refuse.t.i - refuse.f.i))))
          sp.for k in sp.range(1, sp.to_int(sp.max(abs(refuse.t.j - refuse.f.j), abs(refuse.t.i - refuse.f.i))) - 1):
            sp.verify(deck.value[refuse.f.i + (k * sp.sign(refuse.t.i - refuse.f.i))][refuse.f.j + (k * sp.sign(refuse.t.j - refuse.f.j))] == 0)
        sp.if (abs(deck.value[refuse.f.i][refuse.f.j])) == 4:
          sp.verify((abs(refuse.t.j - refuse.f.j)) == (abs(refuse.t.i - refuse.f.i)))
          sp.for k in sp.range(1, sp.to_int(sp.max(abs(refuse.t.j - refuse.f.j), abs(refuse.t.i - refuse.f.i))) - 1):
            sp.verify(deck.value[refuse.f.i + (k * sp.sign(refuse.t.i - refuse.f.i))][refuse.f.j + (k * sp.sign(refuse.t.j - refuse.f.j))] == 0)
        sp.if (abs(deck.value[refuse.f.i][refuse.f.j])) == 3:
          sp.verify((abs((refuse.t.j - refuse.f.j) * (refuse.t.i - refuse.f.i))) == 2)
        sp.if (abs(deck.value[refuse.f.i][refuse.f.j])) == 1:
          sp.verify(((((((((abs(refuse.t.j - refuse.f.j)) == 1) & (((refuse.t.i - refuse.f.i) * compute_chess_logic_386.value) == 1)) & board_state.value.enPassant.is_some()) & (refuse.f.i == board_state.value.enPassant.open_some().i)) & (refuse.f.j == board_state.value.enPassant.open_some().j)) | ((((refuse.t.j - refuse.f.j) == 0) & (((refuse.t.i - refuse.f.i) * compute_chess_logic_386.value) == 2)) & (deck.value[refuse.t.i - compute_chess_logic_386.value][refuse.t.j] == 0))) | (((refuse.t.j - refuse.f.j) == 0) & (((refuse.t.i - refuse.f.i) * compute_chess_logic_386.value) == 1))) | ((((abs(refuse.t.j - refuse.f.j)) == 1) & (((refuse.t.i - refuse.f.i) * compute_chess_logic_386.value) == 1)) & (deck.value[refuse.t.i][refuse.t.j] != compute_chess_logic_386.value)))
          sp.if ((refuse.t.i - refuse.f.i) * compute_chess_logic_386.value) == 2:
            board_state.value.enPassant = sp.some(sp.record(i = refuse.t.i - compute_chess_logic_386.value, j = refuse.t.j))
          sp.else:
            board_state.value.enPassant = sp.none
          resetClock.value = True
          sp.if (refuse.t.i == 7) | (refuse.t.i == 0):
            promotion.value = True
        sp.else:
          board_state.value.enPassant = sp.none
        compute_chess_logic_406 = sp.local("compute_chess_logic_406", board_state.value.kings[compute_chess_logic_386.value])
        sp.if deck.value[refuse.t.i][refuse.t.j] != 0:
          resetClock.value = True
        sp.if promotion.value:
          compute_chess_logic_412 = sp.local("compute_chess_logic_412", refuse.promotion.open_some())
          sp.verify((compute_chess_logic_412.value > 1) & (compute_chess_logic_412.value < 6))
          deck.value[refuse.t.i][refuse.t.j] = sp.mul(compute_chess_logic_386.value, compute_chess_logic_412.value)
        sp.else:
          sp.verify(refuse.promotion.is_variant('None'))
          deck.value[refuse.t.i][refuse.t.j] = deck.value[refuse.f.i][refuse.f.j]
        deck.value[refuse.f.i][refuse.f.j] = 0
        sp.verify(sp.len(sp.set_type_expr(self.get_movable_to, sp.TLambda(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), i = sp.TInt, j = sp.TInt, pawn_capturing = sp.TBool, player = sp.TInt).layout(("deck", ("i", ("j", ("pawn_capturing", "player"))))), sp.TList(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))))(sp.record(deck = deck.value, i = compute_chess_logic_406.value.i, j = compute_chess_logic_406.value.j, pawn_capturing = True, player = compute_chess_logic_386.value * (-1)))) == 0)
        board_state.value.pastMoves[(board_state.value.fullMove, compute_chess_logic_386.value)] = sp.blake2b(sp.pack((board_state.value.castle, deck.value, board_state.value.enPassant)))
        board_state.value.deck = deck.value
        sp.if refuse.claim_repeat.is_some():
          match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd = sp.match_tuple(refuse.claim_repeat.open_some(), "match_pair_chess_logic_428_fst", "match_pair_chess_logic_428_snd")
          sp.set_type(sp.set([match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd, board_state.value.fullMove]), sp.TSet(sp.TNat))
          sp.verify(sp.len(sp.set([match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd, board_state.value.fullMove])) == 3)
          sp.for fullMove in sp.set([match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd, board_state.value.fullMove]).elements():
            sp.verify(sp.blake2b(sp.pack((board_state.value.castle, board_state.value.deck, board_state.value.enPassant))) == board_state.value.pastMoves[(fullMove, compute_chess_logic_386.value)], ('NotSameMove', sp.record(fullMove = fullMove)))
          is_draw.value = True
        sp.if (compute_chess_logic_386.value * (-1)) == 1:
          board_state.value.fullMove += 1
        sp.if resetClock.value:
          board_state.value.halfMoveClock = 0
        sp.else:
          board_state.value.halfMoveClock += 1
        compute_chess_logic_440 = sp.local("compute_chess_logic_440", board_state.value.kings[compute_chess_logic_386.value * (-1)])
        sp.if sp.len(sp.set_type_expr(self.get_movable_to, sp.TLambda(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), i = sp.TInt, j = sp.TInt, pawn_capturing = sp.TBool, player = sp.TInt).layout(("deck", ("i", ("j", ("pawn_capturing", "player"))))), sp.TList(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))))(sp.record(deck = deck.value, i = compute_chess_logic_440.value.i, j = compute_chess_logic_440.value.j, pawn_capturing = True, player = (compute_chess_logic_386.value * (-1)) * (-1)))) > 0:
          board_state.value.check = True
        sp.else:
          board_state.value.check = False
        sp.if board_state.value.halfMoveClock > 49:
          is_draw.value = True
        self.data.board_state.nextPlayer *= -1
        self.data.status = variant('force_play', sp.unit)


  @sp.entry_point
  def claim_checkmate(self):
    board_state = sp.local("board_state", self.data.board_state)
    checkmate = sp.local("checkmate", True)
    deck = sp.local("deck", board_state.value.deck)
    compute_chess_logic_192 = sp.local("compute_chess_logic_192", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i, j = board_state.value.kings[board_state.value.nextPlayer].j, pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
    sp.if sp.len(compute_chess_logic_192.value) == 0:
      checkmate.value = False
    sp.else:
      sp.if checkmate.value:
        destination_piece1_-1 = sp.local("destination_piece1_-1", deck.value.get(board_state.value.kings[board_state.value.nextPlayer].i + 1, default_value = {}).get(board_state.value.kings[board_state.value.nextPlayer].j + (-1), default_value = board_state.value.nextPlayer * 1))
        sp.if (destination_piece1_-1.value == 0) | (sp.sign(destination_piece1_-1.value) != board_state.value.nextPlayer):
          compute_chess_logic_207 = sp.local("compute_chess_logic_207", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i + 1, j = board_state.value.kings[board_state.value.nextPlayer].j + (-1), pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
          sp.if sp.len(compute_chess_logic_207.value) == 0:
            checkmate.value = False
      sp.if checkmate.value:
        destination_piece1_0 = sp.local("destination_piece1_0", deck.value.get(board_state.value.kings[board_state.value.nextPlayer].i + 1, default_value = {}).get(board_state.value.kings[board_state.value.nextPlayer].j + 0, default_value = board_state.value.nextPlayer * 1))
        sp.if (destination_piece1_0.value == 0) | (sp.sign(destination_piece1_0.value) != board_state.value.nextPlayer):
          compute_chess_logic_207i = sp.local("compute_chess_logic_207i", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i + 1, j = board_state.value.kings[board_state.value.nextPlayer].j + 0, pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
          sp.if sp.len(compute_chess_logic_207i.value) == 0:
            checkmate.value = False
      sp.if checkmate.value:
        destination_piece1_1 = sp.local("destination_piece1_1", deck.value.get(board_state.value.kings[board_state.value.nextPlayer].i + 1, default_value = {}).get(board_state.value.kings[board_state.value.nextPlayer].j + 1, default_value = board_state.value.nextPlayer * 1))
        sp.if (destination_piece1_1.value == 0) | (sp.sign(destination_piece1_1.value) != board_state.value.nextPlayer):
          compute_chess_logic_207 = sp.local("compute_chess_logic_207", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i + 1, j = board_state.value.kings[board_state.value.nextPlayer].j + 1, pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
          sp.if sp.len(compute_chess_logic_207.value) == 0:
            checkmate.value = False
      sp.if checkmate.value:
        destination_piece0_1 = sp.local("destination_piece0_1", deck.value.get(board_state.value.kings[board_state.value.nextPlayer].i + 0, default_value = {}).get(board_state.value.kings[board_state.value.nextPlayer].j + 1, default_value = board_state.value.nextPlayer * 1))
        sp.if (destination_piece0_1.value == 0) | (sp.sign(destination_piece0_1.value) != board_state.value.nextPlayer):
          compute_chess_logic_207i = sp.local("compute_chess_logic_207i", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i + 0, j = board_state.value.kings[board_state.value.nextPlayer].j + 1, pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
          sp.if sp.len(compute_chess_logic_207i.value) == 0:
            checkmate.value = False
      sp.if checkmate.value:
        destination_piece-1_1 = sp.local("destination_piece-1_1", deck.value.get(board_state.value.kings[board_state.value.nextPlayer].i + (-1), default_value = {}).get(board_state.value.kings[board_state.value.nextPlayer].j + 1, default_value = board_state.value.nextPlayer * 1))
        sp.if (destination_piece-1_1.value == 0) | (sp.sign(destination_piece-1_1.value) != board_state.value.nextPlayer):
          compute_chess_logic_207 = sp.local("compute_chess_logic_207", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i + (-1), j = board_state.value.kings[board_state.value.nextPlayer].j + 1, pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
          sp.if sp.len(compute_chess_logic_207.value) == 0:
            checkmate.value = False
      sp.if checkmate.value:
        destination_piece-1_0 = sp.local("destination_piece-1_0", deck.value.get(board_state.value.kings[board_state.value.nextPlayer].i + (-1), default_value = {}).get(board_state.value.kings[board_state.value.nextPlayer].j + 0, default_value = board_state.value.nextPlayer * 1))
        sp.if (destination_piece-1_0.value == 0) | (sp.sign(destination_piece-1_0.value) != board_state.value.nextPlayer):
          compute_chess_logic_207i = sp.local("compute_chess_logic_207i", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i + (-1), j = board_state.value.kings[board_state.value.nextPlayer].j + 0, pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
          sp.if sp.len(compute_chess_logic_207i.value) == 0:
            checkmate.value = False
      sp.if checkmate.value:
        destination_piece-1_-1 = sp.local("destination_piece-1_-1", deck.value.get(board_state.value.kings[board_state.value.nextPlayer].i + (-1), default_value = {}).get(board_state.value.kings[board_state.value.nextPlayer].j + (-1), default_value = board_state.value.nextPlayer * 1))
        sp.if (destination_piece-1_-1.value == 0) | (sp.sign(destination_piece-1_-1.value) != board_state.value.nextPlayer):
          compute_chess_logic_207 = sp.local("compute_chess_logic_207", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i + (-1), j = board_state.value.kings[board_state.value.nextPlayer].j + (-1), pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
          sp.if sp.len(compute_chess_logic_207.value) == 0:
            checkmate.value = False
      sp.if checkmate.value:
        destination_piece0_-1 = sp.local("destination_piece0_-1", deck.value.get(board_state.value.kings[board_state.value.nextPlayer].i + 0, default_value = {}).get(board_state.value.kings[board_state.value.nextPlayer].j + (-1), default_value = board_state.value.nextPlayer * 1))
        sp.if (destination_piece0_-1.value == 0) | (sp.sign(destination_piece0_-1.value) != board_state.value.nextPlayer):
          compute_chess_logic_207i = sp.local("compute_chess_logic_207i", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i + 0, j = board_state.value.kings[board_state.value.nextPlayer].j + (-1), pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
          sp.if sp.len(compute_chess_logic_207i.value) == 0:
            checkmate.value = False
      sp.if checkmate.value:
        sp.if sp.len(compute_chess_logic_192.value) == 1:
          sp.for attacking_square in compute_chess_logic_192.value:
            compute_chess_logic_218 = sp.local("compute_chess_logic_218", self.get_movable_to(sp.record(deck = deck.value, i = attacking_square.i, j = attacking_square.j, pawn_capturing = True, player = board_state.value.nextPlayer * (-1))))
            compute_chess_logic_218i = sp.local("compute_chess_logic_218i", compute_chess_logic_218.value)
    def f_x16(_x16):
      defending_squares_chess_logic_538, attacking_square_chess_logic_538, deck_chess_logic_538, king_chess_logic_538, nextPlayer_chess_logic_538, checkmate_chess_logic_538, get_movable_to_chess_logic_538 = sp.match_record(_x16, "defending_squares", "attacking_square", "deck", "king", "nextPlayer", "checkmate", "get_movable_to")
      checkmate = sp.local("checkmate", checkmate_chess_logic_538)
      sp.for defending_square in defending_squares_chess_logic_538:
        sp.if checkmate.value:
          test_deck = sp.local("test_deck", deck_chess_logic_538)
          test_deck.value[attacking_square_chess_logic_538.i][attacking_square_chess_logic_538.j] = test_deck.value[defending_square.i][defending_square.j]
          test_deck.value[defending_square.i][defending_square.j] = 0
          king = sp.local("king", king_chess_logic_538)
          sp.if test_deck.value[attacking_square_chess_logic_538.i][attacking_square_chess_logic_538.j] == (6 * nextPlayer_chess_logic_538):
            king.value = sp.record(i = attacking_square_chess_logic_538.i, j = attacking_square_chess_logic_538.j)
          sp.if sp.len(get_movable_to_chess_logic_538(sp.record(deck = test_deck.value, i = king.value.i, j = king.value.j, pawn_capturing = True, player = nextPlayer_chess_logic_538 * (-1)))) == 0:
            checkmate.value = False
      sp.result(checkmate.value)
            checkmate.value = sp.build_lambda(f_x16)(sp.record(attacking_square = attacking_square, checkmate = checkmate.value, deck = deck.value, defending_squares = compute_chess_logic_218i.value, get_movable_to = self.get_movable_to, king = board_state.value.kings[board_state.value.nextPlayer], nextPlayer = board_state.value.nextPlayer))
            sp.if checkmate.value:
              sp.if (deck.value[attacking_square.i][attacking_square.j] != ((3 * board_state.value.nextPlayer) * (-1))) & (deck.value[attacking_square.i][attacking_square.j] != ((1 * board_state.value.nextPlayer) * (-1))):
                sp.if attacking_square.i == board_state.value.kings[board_state.value.nextPlayer].i:
                  sp.for obstructing_j in sp.range(sp.min(board_state.value.kings[board_state.value.nextPlayer].j, attacking_square.j), sp.max(board_state.value.kings[board_state.value.nextPlayer].j, attacking_square.j)):
                    compute_chess_logic_231 = sp.local("compute_chess_logic_231", self.get_movable_to(sp.record(deck = deck.value, i = board_state.value.kings[board_state.value.nextPlayer].i, j = obstructing_j, pawn_capturing = False, player = board_state.value.nextPlayer)))
                    compute_chess_logic_231i = sp.local("compute_chess_logic_231i", compute_chess_logic_231.value)
    def f_x16(_x16):
      defending_squares_chess_logic_538, attacking_square_chess_logic_538, deck_chess_logic_538, king_chess_logic_538, nextPlayer_chess_logic_538, checkmate_chess_logic_538, get_movable_to_chess_logic_538 = sp.match_record(_x16, "defending_squares", "attacking_square", "deck", "king", "nextPlayer", "checkmate", "get_movable_to")
      checkmate = sp.local("checkmate", checkmate_chess_logic_538)
      sp.for defending_square in defending_squares_chess_logic_538:
        sp.if checkmate.value:
          test_deck = sp.local("test_deck", deck_chess_logic_538)
          test_deck.value[attacking_square_chess_logic_538.i][attacking_square_chess_logic_538.j] = test_deck.value[defending_square.i][defending_square.j]
          test_deck.value[defending_square.i][defending_square.j] = 0
          king = sp.local("king", king_chess_logic_538)
          sp.if test_deck.value[attacking_square_chess_logic_538.i][attacking_square_chess_logic_538.j] == (6 * nextPlayer_chess_logic_538):
            king.value = sp.record(i = attacking_square_chess_logic_538.i, j = attacking_square_chess_logic_538.j)
          sp.if sp.len(get_movable_to_chess_logic_538(sp.record(deck = test_deck.value, i = king.value.i, j = king.value.j, pawn_capturing = True, player = nextPlayer_chess_logic_538 * (-1)))) == 0:
            checkmate.value = False
      sp.result(checkmate.value)
                    checkmate.value = sp.build_lambda(f_x16)(sp.record(attacking_square = sp.record(i = board_state.value.kings[board_state.value.nextPlayer].i, j = obstructing_j), checkmate = checkmate.value, deck = deck.value, defending_squares = compute_chess_logic_231i.value, get_movable_to = self.get_movable_to, king = board_state.value.kings[board_state.value.nextPlayer], nextPlayer = board_state.value.nextPlayer))
                sp.else:
                  sp.if attacking_square.j == board_state.value.kings[board_state.value.nextPlayer].j:
                    sp.for obstructing_i in sp.range(sp.min(board_state.value.kings[board_state.value.nextPlayer].i, attacking_square.i), sp.max(board_state.value.kings[board_state.value.nextPlayer].i, attacking_square.i)):
                      compute_chess_logic_239 = sp.local("compute_chess_logic_239", self.get_movable_to(sp.record(deck = deck.value, i = obstructing_i, j = board_state.value.kings[board_state.value.nextPlayer].j, pawn_capturing = False, player = board_state.value.nextPlayer)))
                      compute_chess_logic_239i = sp.local("compute_chess_logic_239i", compute_chess_logic_239.value)
    def f_x16(_x16):
      defending_squares_chess_logic_538, attacking_square_chess_logic_538, deck_chess_logic_538, king_chess_logic_538, nextPlayer_chess_logic_538, checkmate_chess_logic_538, get_movable_to_chess_logic_538 = sp.match_record(_x16, "defending_squares", "attacking_square", "deck", "king", "nextPlayer", "checkmate", "get_movable_to")
      checkmate = sp.local("checkmate", checkmate_chess_logic_538)
      sp.for defending_square in defending_squares_chess_logic_538:
        sp.if checkmate.value:
          test_deck = sp.local("test_deck", deck_chess_logic_538)
          test_deck.value[attacking_square_chess_logic_538.i][attacking_square_chess_logic_538.j] = test_deck.value[defending_square.i][defending_square.j]
          test_deck.value[defending_square.i][defending_square.j] = 0
          king = sp.local("king", king_chess_logic_538)
          sp.if test_deck.value[attacking_square_chess_logic_538.i][attacking_square_chess_logic_538.j] == (6 * nextPlayer_chess_logic_538):
            king.value = sp.record(i = attacking_square_chess_logic_538.i, j = attacking_square_chess_logic_538.j)
          sp.if sp.len(get_movable_to_chess_logic_538(sp.record(deck = test_deck.value, i = king.value.i, j = king.value.j, pawn_capturing = True, player = nextPlayer_chess_logic_538 * (-1)))) == 0:
            checkmate.value = False
      sp.result(checkmate.value)
                      checkmate.value = sp.build_lambda(f_x16)(sp.record(attacking_square = sp.record(i = obstructing_i, j = board_state.value.kings[board_state.value.nextPlayer].j), checkmate = checkmate.value, deck = deck.value, defending_squares = compute_chess_logic_239i.value, get_movable_to = self.get_movable_to, king = board_state.value.kings[board_state.value.nextPlayer], nextPlayer = board_state.value.nextPlayer))
                  sp.else:
                    compute_chess_logic_245 = sp.local("compute_chess_logic_245", sp.to_int(abs(board_state.value.kings[board_state.value.nextPlayer].i - attacking_square.i)))
                    compute_chess_logic_246 = sp.local("compute_chess_logic_246", sp.sign(attacking_square.i - board_state.value.kings[board_state.value.nextPlayer].i))
                    compute_chess_logic_247 = sp.local("compute_chess_logic_247", sp.sign(attacking_square.j - board_state.value.kings[board_state.value.nextPlayer].j))
                    sp.for i in sp.range(1, compute_chess_logic_245.value):
                      compute_chess_logic_250 = sp.local("compute_chess_logic_250", board_state.value.kings[board_state.value.nextPlayer].i + (compute_chess_logic_246.value * i))
                      compute_chess_logic_251 = sp.local("compute_chess_logic_251", board_state.value.kings[board_state.value.nextPlayer].j + (compute_chess_logic_247.value * i))
                      compute_chess_logic_252 = sp.local("compute_chess_logic_252", self.get_movable_to(sp.record(deck = deck.value, i = compute_chess_logic_250.value, j = compute_chess_logic_251.value, pawn_capturing = False, player = board_state.value.nextPlayer)))
                      compute_chess_logic_252i = sp.local("compute_chess_logic_252i", compute_chess_logic_252.value)
    def f_x16(_x16):
      defending_squares_chess_logic_538, attacking_square_chess_logic_538, deck_chess_logic_538, king_chess_logic_538, nextPlayer_chess_logic_538, checkmate_chess_logic_538, get_movable_to_chess_logic_538 = sp.match_record(_x16, "defending_squares", "attacking_square", "deck", "king", "nextPlayer", "checkmate", "get_movable_to")
      checkmate = sp.local("checkmate", checkmate_chess_logic_538)
      sp.for defending_square in defending_squares_chess_logic_538:
        sp.if checkmate.value:
          test_deck = sp.local("test_deck", deck_chess_logic_538)
          test_deck.value[attacking_square_chess_logic_538.i][attacking_square_chess_logic_538.j] = test_deck.value[defending_square.i][defending_square.j]
          test_deck.value[defending_square.i][defending_square.j] = 0
          king = sp.local("king", king_chess_logic_538)
          sp.if test_deck.value[attacking_square_chess_logic_538.i][attacking_square_chess_logic_538.j] == (6 * nextPlayer_chess_logic_538):
            king.value = sp.record(i = attacking_square_chess_logic_538.i, j = attacking_square_chess_logic_538.j)
          sp.if sp.len(get_movable_to_chess_logic_538(sp.record(deck = test_deck.value, i = king.value.i, j = king.value.j, pawn_capturing = True, player = nextPlayer_chess_logic_538 * (-1)))) == 0:
            checkmate.value = False
      sp.result(checkmate.value)
                      checkmate.value = sp.build_lambda(f_x16)(sp.record(attacking_square = sp.record(i = compute_chess_logic_250.value, j = compute_chess_logic_251.value), checkmate = checkmate.value, deck = deck.value, defending_squares = compute_chess_logic_252i.value, get_movable_to = self.get_movable_to, king = board_state.value.kings[board_state.value.nextPlayer], nextPlayer = board_state.value.nextPlayer))
    sp.if checkmate.value:
      self.data.status = sp.eif(board_state.value.nextPlayer == 1, variant('finished', 'player_2_won'), variant('finished', 'player_1_won'))
    sp.else:
      sp.failwith('NotCheckmate')

  @sp.entry_point
  def claim_stalemate(self):
    sp.verify(sp.sender == self.data.players_map[self.data.board_state.nextPlayer], 'Wrong player')
    sp.verify(self.data.status.is_variant('play'))
    self.data.status = variant('claim_stalemate', sp.unit)
    self.data.board_state.nextPlayer *= -1

  @sp.entry_point
  def giveup(self):
    sp.verify(~ self.data.status.is_variant('finished'))
    sp.if sp.sender == self.data.players_map[-1]:
      self.data.status = variant('finished', 'player_1_won')
    sp.else:
      sp.if sp.sender == self.data.players_map[1]:
        self.data.status = variant('finished', 'player_2_won')
      sp.else:
        sp.failwith('Wrong player')

  @sp.entry_point
  def offer_draw(self):
    sp.verify(~ self.data.status.is_variant('finished'), 'Game finished')
    sp.verify(self.data.players.contains(sp.sender), 'Wrong player')
    self.data.draw_offer.add(sp.sender)
    sp.if sp.len(self.data.draw_offer) == 2:
      self.data.status = variant('finished', 'draw')

  @sp.entry_point
  def play(self, params):
    sp.verify(sp.sender == self.data.players_map[self.data.board_state.nextPlayer], 'Wrong player')
    sp.verify(self.data.status.is_variant('play') | self.data.status.is_variant('force_play'))
    board_state = sp.local("board_state", self.data.board_state)
    is_draw = sp.local("is_draw", False)
    sp.set_type(params, sp.TRecord(claim_repeat = sp.TOption(sp.TPair(sp.TNat, sp.TNat)), f = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")), promotion = sp.TOption(sp.TNat), t = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))).layout(("claim_repeat", ("f", ("promotion", "t")))))
    sp.set_type(board_state.value, sp.TRecord(castle = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TBool)), check = sp.TBool, deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), enPassant = sp.TOption(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))), fullMove = sp.TNat, halfMoveClock = sp.TNat, kings = sp.TMap(sp.TInt, sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))), nextPlayer = sp.TInt, pastMoves = sp.TMap(sp.TPair(sp.TNat, sp.TInt), sp.TBytes)).layout(("castle", ("check", ("deck", ("enPassant", ("fullMove", ("halfMoveClock", ("kings", ("nextPlayer", "pastMoves"))))))))))
    sp.verify((params.f.i >= 0) & (params.f.i < 8))
    sp.verify((params.f.j >= 0) & (params.f.j < 8))
    sp.verify((params.t.i >= 0) & (params.t.i < 8))
    sp.verify((params.t.j >= 0) & (params.t.j < 8))
    sp.verify(((params.t.j - params.f.j) != 0) | ((params.t.i - params.f.i) != 0))
    compute_chess_logic_386 = sp.local("compute_chess_logic_386", board_state.value.nextPlayer)
    deck = sp.local("deck", board_state.value.deck)
    sp.verify(sp.sign(deck.value[params.f.i][params.f.j]) == compute_chess_logic_386.value)
    sp.verify(sp.sign(deck.value[params.t.i][params.t.j]) != compute_chess_logic_386.value)
    sp.verify(deck.value[params.t.i][params.t.j] != (6 * compute_chess_logic_386.value))
    resetClock = sp.local("resetClock", False)
    promotion = sp.local("promotion", False)
    sp.if (abs(deck.value[params.f.i][params.f.j])) == 6:
      sp.if ((abs(params.t.j - params.f.j)) == 2) & ((params.t.i - params.f.i) == 0):
        sp.verify((((compute_chess_logic_386.value == 1) & (params.f.i == 0)) & (params.f.j == 4)) | (((compute_chess_logic_386.value == (-1)) & (params.f.i == 7)) & (params.f.j == 4)))
        sp.verify(board_state.value.castle[compute_chess_logic_386.value][sp.sign(params.t.j - params.f.j)])
        sp.if (params.t.j - params.f.j) > 0:
          deck.value[params.t.i][7] = 0
        sp.else:
          deck.value[params.t.i][0] = 0
        deck.value[params.t.i][params.t.j - sp.sign(params.t.j - params.f.j)] = 2 * compute_chess_logic_386.value
      sp.else:
        sp.verify(((abs(params.t.j - params.f.j)) <= 1) & ((abs(params.t.i - params.f.i)) <= 1))
      board_state.value.kings[compute_chess_logic_386.value] = sp.record(i = params.t.i, j = params.t.j)
      board_state.value.castle[compute_chess_logic_386.value] = {-1 : False, 1 : False}
    sp.if (abs(deck.value[params.f.i][params.f.j])) == 2:
      sp.verify(((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0))
      sp.for k in sp.range(1, sp.to_int(sp.max(abs(params.t.j - params.f.j), abs(params.t.i - params.f.i))) - 1):
        sp.verify(deck.value[params.f.i + (k * sp.sign(params.t.i - params.f.i))][params.f.j + (k * sp.sign(params.t.j - params.f.j))] == 0)
      sp.if (((compute_chess_logic_386.value == 1) & (params.f.i == 0)) & (params.f.j == 0)) | (((compute_chess_logic_386.value == (-1)) & (params.f.i == 7)) & (params.f.j == 0)):
        board_state.value.castle[compute_chess_logic_386.value][-1] = False
      sp.if (((compute_chess_logic_386.value == 1) & (params.f.i == 0)) & (params.f.j == 7)) | (((compute_chess_logic_386.value == (-1)) & (params.f.i == 7)) & (params.f.j == 7)):
        board_state.value.castle[compute_chess_logic_386.value][1] = False
    sp.if (abs(deck.value[params.f.i][params.f.j])) == 5:
      sp.verify((((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0)) | ((abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i))))
      sp.for k in sp.range(1, sp.to_int(sp.max(abs(params.t.j - params.f.j), abs(params.t.i - params.f.i))) - 1):
        sp.verify(deck.value[params.f.i + (k * sp.sign(params.t.i - params.f.i))][params.f.j + (k * sp.sign(params.t.j - params.f.j))] == 0)
    sp.if (abs(deck.value[params.f.i][params.f.j])) == 4:
      sp.verify((abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i)))
      sp.for k in sp.range(1, sp.to_int(sp.max(abs(params.t.j - params.f.j), abs(params.t.i - params.f.i))) - 1):
        sp.verify(deck.value[params.f.i + (k * sp.sign(params.t.i - params.f.i))][params.f.j + (k * sp.sign(params.t.j - params.f.j))] == 0)
    sp.if (abs(deck.value[params.f.i][params.f.j])) == 3:
      sp.verify((abs((params.t.j - params.f.j) * (params.t.i - params.f.i))) == 2)
    sp.if (abs(deck.value[params.f.i][params.f.j])) == 1:
      sp.verify(((((((((abs(params.t.j - params.f.j)) == 1) & (((params.t.i - params.f.i) * compute_chess_logic_386.value) == 1)) & board_state.value.enPassant.is_some()) & (params.f.i == board_state.value.enPassant.open_some().i)) & (params.f.j == board_state.value.enPassant.open_some().j)) | ((((params.t.j - params.f.j) == 0) & (((params.t.i - params.f.i) * compute_chess_logic_386.value) == 2)) & (deck.value[params.t.i - compute_chess_logic_386.value][params.t.j] == 0))) | (((params.t.j - params.f.j) == 0) & (((params.t.i - params.f.i) * compute_chess_logic_386.value) == 1))) | ((((abs(params.t.j - params.f.j)) == 1) & (((params.t.i - params.f.i) * compute_chess_logic_386.value) == 1)) & (deck.value[params.t.i][params.t.j] != compute_chess_logic_386.value)))
      sp.if ((params.t.i - params.f.i) * compute_chess_logic_386.value) == 2:
        board_state.value.enPassant = sp.some(sp.record(i = params.t.i - compute_chess_logic_386.value, j = params.t.j))
      sp.else:
        board_state.value.enPassant = sp.none
      resetClock.value = True
      sp.if (params.t.i == 7) | (params.t.i == 0):
        promotion.value = True
    sp.else:
      board_state.value.enPassant = sp.none
    compute_chess_logic_406 = sp.local("compute_chess_logic_406", board_state.value.kings[compute_chess_logic_386.value])
    sp.if deck.value[params.t.i][params.t.j] != 0:
      resetClock.value = True
    sp.if promotion.value:
      compute_chess_logic_412 = sp.local("compute_chess_logic_412", params.promotion.open_some())
      sp.verify((compute_chess_logic_412.value > 1) & (compute_chess_logic_412.value < 6))
      deck.value[params.t.i][params.t.j] = sp.mul(compute_chess_logic_386.value, compute_chess_logic_412.value)
    sp.else:
      sp.verify(params.promotion.is_variant('None'))
      deck.value[params.t.i][params.t.j] = deck.value[params.f.i][params.f.j]
    deck.value[params.f.i][params.f.j] = 0
    sp.verify(sp.len(sp.set_type_expr(self.get_movable_to, sp.TLambda(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), i = sp.TInt, j = sp.TInt, pawn_capturing = sp.TBool, player = sp.TInt).layout(("deck", ("i", ("j", ("pawn_capturing", "player"))))), sp.TList(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))))(sp.record(deck = deck.value, i = compute_chess_logic_406.value.i, j = compute_chess_logic_406.value.j, pawn_capturing = True, player = compute_chess_logic_386.value * (-1)))) == 0)
    board_state.value.pastMoves[(board_state.value.fullMove, compute_chess_logic_386.value)] = sp.blake2b(sp.pack((board_state.value.castle, deck.value, board_state.value.enPassant)))
    board_state.value.deck = deck.value
    sp.if params.claim_repeat.is_some():
      match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd = sp.match_tuple(params.claim_repeat.open_some(), "match_pair_chess_logic_428_fst", "match_pair_chess_logic_428_snd")
      sp.set_type(sp.set([match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd, board_state.value.fullMove]), sp.TSet(sp.TNat))
      sp.verify(sp.len(sp.set([match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd, board_state.value.fullMove])) == 3)
      sp.for fullMove in sp.set([match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd, board_state.value.fullMove]).elements():
        sp.verify(sp.blake2b(sp.pack((board_state.value.castle, board_state.value.deck, board_state.value.enPassant))) == board_state.value.pastMoves[(fullMove, compute_chess_logic_386.value)], ('NotSameMove', sp.record(fullMove = fullMove)))
      is_draw.value = True
    sp.if (compute_chess_logic_386.value * (-1)) == 1:
      board_state.value.fullMove += 1
    sp.if resetClock.value:
      board_state.value.halfMoveClock = 0
    sp.else:
      board_state.value.halfMoveClock += 1
    compute_chess_logic_440 = sp.local("compute_chess_logic_440", board_state.value.kings[compute_chess_logic_386.value * (-1)])
    sp.if sp.len(sp.set_type_expr(self.get_movable_to, sp.TLambda(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), i = sp.TInt, j = sp.TInt, pawn_capturing = sp.TBool, player = sp.TInt).layout(("deck", ("i", ("j", ("pawn_capturing", "player"))))), sp.TList(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))))(sp.record(deck = deck.value, i = compute_chess_logic_440.value.i, j = compute_chess_logic_440.value.j, pawn_capturing = True, player = (compute_chess_logic_386.value * (-1)) * (-1)))) > 0:
      board_state.value.check = True
    sp.else:
      board_state.value.check = False
    sp.if board_state.value.halfMoveClock > 49:
      is_draw.value = True
    match_pair_chess_181_fst, match_pair_chess_181_snd = sp.match_tuple((board_state.value, is_draw.value), "match_pair_chess_181_fst", "match_pair_chess_181_snd")
    compute_chess_182 = sp.local("compute_chess_182", match_pair_chess_181_fst)
    compute_chess_182.value.nextPlayer *= -1
    self.data.board_state = compute_chess_182.value
    sp.if match_pair_chess_181_snd:
      self.data.status = variant('finished', 'draw')
    self.data.draw_offer = sp.set([])

  @sp.entry_point
  def threefold_repetition_claim(self, params):
    sp.verify(~ self.data.status.is_variant('finished'), 'Game finished')
    sp.verify(self.data.players_map[self.data.board_state.nextPlayer] == sp.sender, 'Wrong player')
    sp.set_type(sp.set([params.fullMove1, params.fullMove2, sp.eif(self.data.board_state.nextPlayer == 1, sp.as_nat(self.data.board_state.fullMove - 1), self.data.board_state.fullMove)]), sp.TSet(sp.TNat))
    sp.verify(sp.len(sp.set([params.fullMove1, params.fullMove2, sp.eif(self.data.board_state.nextPlayer == 1, sp.as_nat(self.data.board_state.fullMove - 1), self.data.board_state.fullMove)])) == 3)
    sp.for fullMove in sp.set([params.fullMove1, params.fullMove2, sp.eif(self.data.board_state.nextPlayer == 1, sp.as_nat(self.data.board_state.fullMove - 1), self.data.board_state.fullMove)]).elements():
      sp.verify(sp.blake2b(sp.pack((self.data.board_state.castle, self.data.board_state.deck, self.data.board_state.enPassant))) == self.data.board_state.pastMoves[(fullMove, self.data.board_state.nextPlayer * (-1))], ('NotSameMove', sp.record(fullMove = fullMove)))
    self.data.status = variant('finished', 'draw')

  @sp.private_lambda()
  def get_movable_to(_x12):
    sp.set_type(_x12, sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), i = sp.TInt, j = sp.TInt, pawn_capturing = sp.TBool, player = sp.TInt).layout(("deck", ("i", ("j", ("pawn_capturing", "player"))))))
    i_chess_logic_286, j_chess_logic_286, player_chess_logic_286, deck_chess_logic_286 = sp.match_record(_x12, "i", "j", "player", "deck")
    check = sp.local("check", sp.list([]), sp.TList(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))))
    sp.if _x12.pawn_capturing:
      sp.if deck_chess_logic_286.get(i_chess_logic_286 + player_chess_logic_286, default_value = {}).get(j_chess_logic_286 - 1, default_value = 0) == (1 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 + player_chess_logic_286, j = j_chess_logic_286 - 1))
      sp.if deck_chess_logic_286.get(i_chess_logic_286 + player_chess_logic_286, default_value = {}).get(j_chess_logic_286 + 1, default_value = 0) == (1 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 + player_chess_logic_286, j = j_chess_logic_286 + 1))
    sp.else:
      sp.if deck_chess_logic_286.get(i_chess_logic_286 - player_chess_logic_286, default_value = {}).get(j_chess_logic_286, default_value = 0) == (1 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 - player_chess_logic_286, j = j_chess_logic_286))
      sp.if ((player_chess_logic_286 == 1) & (i_chess_logic_286 == 3)) | ((player_chess_logic_286 == (-1)) & (i_chess_logic_286 == 4)):
        sp.if deck_chess_logic_286.get(i_chess_logic_286 + (player_chess_logic_286 * 2), default_value = {}).get(j_chess_logic_286, default_value = 0) == (1 * player_chess_logic_286):
          check.value.push(sp.record(i = i_chess_logic_286 + (player_chess_logic_286 * 2), j = j_chess_logic_286))
    sp.if deck_chess_logic_286.get(i_chess_logic_286 + 2, default_value = {}).get(j_chess_logic_286 - 1, default_value = 0) == (3 * player_chess_logic_286):
      check.value.push(sp.record(i = i_chess_logic_286 + 2, j = j_chess_logic_286 - 1))
    sp.if deck_chess_logic_286.get(i_chess_logic_286 + 2, default_value = {}).get(j_chess_logic_286 + 1, default_value = 0) == (3 * player_chess_logic_286):
      check.value.push(sp.record(i = i_chess_logic_286 + 2, j = j_chess_logic_286 + 1))
    sp.if deck_chess_logic_286.get(i_chess_logic_286 + 1, default_value = {}).get(j_chess_logic_286 - 2, default_value = 0) == (3 * player_chess_logic_286):
      check.value.push(sp.record(i = i_chess_logic_286 + 1, j = j_chess_logic_286 - 2))
    sp.if deck_chess_logic_286.get(i_chess_logic_286 + 1, default_value = {}).get(j_chess_logic_286 + 2, default_value = 0) == (3 * player_chess_logic_286):
      check.value.push(sp.record(i = i_chess_logic_286 + 1, j = j_chess_logic_286 + 2))
    sp.if deck_chess_logic_286.get(i_chess_logic_286 - 2, default_value = {}).get(j_chess_logic_286 - 1, default_value = 0) == (3 * player_chess_logic_286):
      check.value.push(sp.record(i = i_chess_logic_286 - 2, j = j_chess_logic_286 - 1))
    sp.if deck_chess_logic_286.get(i_chess_logic_286 - 2, default_value = {}).get(j_chess_logic_286 + 1, default_value = 0) == (3 * player_chess_logic_286):
      check.value.push(sp.record(i = i_chess_logic_286 - 2, j = j_chess_logic_286 + 1))
    sp.if deck_chess_logic_286.get(i_chess_logic_286 - 1, default_value = {}).get(j_chess_logic_286 - 2, default_value = 0) == (3 * player_chess_logic_286):
      check.value.push(sp.record(i = i_chess_logic_286 - 1, j = j_chess_logic_286 - 2))
    sp.if deck_chess_logic_286.get(i_chess_logic_286 - 1, default_value = {}).get(j_chess_logic_286 + 2, default_value = 0) == (3 * player_chess_logic_286):
      check.value.push(sp.record(i = i_chess_logic_286 - 1, j = j_chess_logic_286 + 2))
    sp.if deck_chess_logic_286.get(i_chess_logic_286, default_value = {}).get(j_chess_logic_286, default_value = 0) != ((6 * player_chess_logic_286) * (-1)):
      sp.if deck_chess_logic_286.get(i_chess_logic_286 + 1, default_value = {}).get(j_chess_logic_286 + 1, default_value = 0) == (6 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 + 1, j = j_chess_logic_286 + 1))
      sp.if deck_chess_logic_286.get(i_chess_logic_286 - 1, default_value = {}).get(j_chess_logic_286 - 1, default_value = 0) == (6 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 - 1, j = j_chess_logic_286 - 1))
      sp.if deck_chess_logic_286.get(i_chess_logic_286 + 1, default_value = {}).get(j_chess_logic_286 - 1, default_value = 0) == (6 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 + 1, j = j_chess_logic_286 - 1))
      sp.if deck_chess_logic_286.get(i_chess_logic_286 - 1, default_value = {}).get(j_chess_logic_286 + 1, default_value = 0) == (6 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 - 1, j = j_chess_logic_286 + 1))
      sp.if deck_chess_logic_286.get(i_chess_logic_286 + 0, default_value = {}).get(j_chess_logic_286 + 1, default_value = 0) == (6 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 + 0, j = j_chess_logic_286 + 1))
      sp.if deck_chess_logic_286.get(i_chess_logic_286 + 1, default_value = {}).get(j_chess_logic_286 + 0, default_value = 0) == (6 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 + 1, j = j_chess_logic_286 + 0))
      sp.if deck_chess_logic_286.get(i_chess_logic_286 + 0, default_value = {}).get(j_chess_logic_286 - 1, default_value = 0) == (6 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 + 0, j = j_chess_logic_286 - 1))
      sp.if deck_chess_logic_286.get(i_chess_logic_286 - 1, default_value = {}).get(j_chess_logic_286 + 0, default_value = 0) == (6 * player_chess_logic_286):
        check.value.push(sp.record(i = i_chess_logic_286 - 1, j = j_chess_logic_286 + 0))
    continue_ = sp.local("continue_", True)
    sp.for l in sp.range(1, j_chess_logic_286):
      sp.if continue_.value:
        compute_chess_logic_323 = sp.local("compute_chess_logic_323", deck_chess_logic_286.get(i_chess_logic_286 + (l * 0), default_value = {}).get(j_chess_logic_286 + (l * (-1)), default_value = 0))
        sp.if (compute_chess_logic_323.value != 0) & (compute_chess_logic_323.value != ((6 * player_chess_logic_286) * (-1))):
          sp.if (compute_chess_logic_323.value == (5 * player_chess_logic_286)) | (compute_chess_logic_323.value == (2 * player_chess_logic_286)):
            check.value.push(sp.record(i = i_chess_logic_286 + (l * 0), j = j_chess_logic_286 + (l * (-1))))
          sp.else:
            continue_.value = False
    continue_.value = True
    sp.for l in sp.range(1, 8 - j_chess_logic_286):
      sp.if continue_.value:
        compute_chess_logic_323i = sp.local("compute_chess_logic_323i", deck_chess_logic_286.get(i_chess_logic_286 + (l * 0), default_value = {}).get(j_chess_logic_286 + (l * 1), default_value = 0))
        sp.if (compute_chess_logic_323i.value != 0) & (compute_chess_logic_323i.value != ((6 * player_chess_logic_286) * (-1))):
          sp.if (compute_chess_logic_323i.value == (5 * player_chess_logic_286)) | (compute_chess_logic_323i.value == (2 * player_chess_logic_286)):
            check.value.push(sp.record(i = i_chess_logic_286 + (l * 0), j = j_chess_logic_286 + (l * 1)))
          sp.else:
            continue_.value = False
    continue_.value = True
    sp.for l in sp.range(1, i_chess_logic_286):
      sp.if continue_.value:
        compute_chess_logic_323 = sp.local("compute_chess_logic_323", deck_chess_logic_286.get(i_chess_logic_286 + (l * (-1)), default_value = {}).get(j_chess_logic_286 + (l * 0), default_value = 0))
        sp.if (compute_chess_logic_323.value != 0) & (compute_chess_logic_323.value != ((6 * player_chess_logic_286) * (-1))):
          sp.if (compute_chess_logic_323.value == (5 * player_chess_logic_286)) | (compute_chess_logic_323.value == (2 * player_chess_logic_286)):
            check.value.push(sp.record(i = i_chess_logic_286 + (l * (-1)), j = j_chess_logic_286 + (l * 0)))
          sp.else:
            continue_.value = False
    continue_.value = True
    sp.for l in sp.range(1, 8 - i_chess_logic_286):
      sp.if continue_.value:
        compute_chess_logic_323i = sp.local("compute_chess_logic_323i", deck_chess_logic_286.get(i_chess_logic_286 + (l * 1), default_value = {}).get(j_chess_logic_286 + (l * 0), default_value = 0))
        sp.if (compute_chess_logic_323i.value != 0) & (compute_chess_logic_323i.value != ((6 * player_chess_logic_286) * (-1))):
          sp.if (compute_chess_logic_323i.value == (5 * player_chess_logic_286)) | (compute_chess_logic_323i.value == (2 * player_chess_logic_286)):
            check.value.push(sp.record(i = i_chess_logic_286 + (l * 1), j = j_chess_logic_286 + (l * 0)))
          sp.else:
            continue_.value = False
    continue_.value = True
    sp.for l in sp.range(1, sp.min(i_chess_logic_286, j_chess_logic_286)):
      sp.verify((j_chess_logic_286 + (l * (-1))) < 9, sp.record(b = -1, i = i_chess_logic_286, j = j_chess_logic_286, l = l, n = 0))
      sp.if continue_.value:
        compute_chess_logic_323 = sp.local("compute_chess_logic_323", deck_chess_logic_286.get(i_chess_logic_286 + (l * (-1)), default_value = {}).get(j_chess_logic_286 + (l * (-1)), default_value = 0))
        sp.if (compute_chess_logic_323.value != 0) & (compute_chess_logic_323.value != ((6 * player_chess_logic_286) * (-1))):
          sp.if (compute_chess_logic_323.value == (5 * player_chess_logic_286)) | (compute_chess_logic_323.value == (4 * player_chess_logic_286)):
            check.value.push(sp.record(i = i_chess_logic_286 + (l * (-1)), j = j_chess_logic_286 + (l * (-1))))
          sp.else:
            continue_.value = False
    continue_.value = True
    sp.for l in sp.range(1, sp.min(8 - i_chess_logic_286, 8 - j_chess_logic_286)):
      sp.verify((j_chess_logic_286 + (l * 1)) < 9, sp.record(b = 1, i = i_chess_logic_286, j = j_chess_logic_286, l = l, n = 1))
      sp.if continue_.value:
        compute_chess_logic_323i = sp.local("compute_chess_logic_323i", deck_chess_logic_286.get(i_chess_logic_286 + (l * 1), default_value = {}).get(j_chess_logic_286 + (l * 1), default_value = 0))
        sp.if (compute_chess_logic_323i.value != 0) & (compute_chess_logic_323i.value != ((6 * player_chess_logic_286) * (-1))):
          sp.if (compute_chess_logic_323i.value == (5 * player_chess_logic_286)) | (compute_chess_logic_323i.value == (4 * player_chess_logic_286)):
            check.value.push(sp.record(i = i_chess_logic_286 + (l * 1), j = j_chess_logic_286 + (l * 1)))
          sp.else:
            continue_.value = False
    continue_.value = True
    sp.for l in sp.range(1, sp.min(i_chess_logic_286, 8 - j_chess_logic_286)):
      sp.verify((j_chess_logic_286 + (l * 1)) < 9, sp.record(b = 1, i = i_chess_logic_286, j = j_chess_logic_286, l = l, n = 2))
      sp.if continue_.value:
        compute_chess_logic_323 = sp.local("compute_chess_logic_323", deck_chess_logic_286.get(i_chess_logic_286 + (l * (-1)), default_value = {}).get(j_chess_logic_286 + (l * 1), default_value = 0))
        sp.if (compute_chess_logic_323.value != 0) & (compute_chess_logic_323.value != ((6 * player_chess_logic_286) * (-1))):
          sp.if (compute_chess_logic_323.value == (5 * player_chess_logic_286)) | (compute_chess_logic_323.value == (4 * player_chess_logic_286)):
            check.value.push(sp.record(i = i_chess_logic_286 + (l * (-1)), j = j_chess_logic_286 + (l * 1)))
          sp.else:
            continue_.value = False
    continue_.value = True
    sp.for l in sp.range(1, sp.min(8 - i_chess_logic_286, j_chess_logic_286)):
      sp.verify((j_chess_logic_286 + (l * (-1))) < 9, sp.record(b = -1, i = i_chess_logic_286, j = j_chess_logic_286, l = l, n = 3))
      sp.if continue_.value:
        compute_chess_logic_323i = sp.local("compute_chess_logic_323i", deck_chess_logic_286.get(i_chess_logic_286 + (l * 1), default_value = {}).get(j_chess_logic_286 + (l * (-1)), default_value = 0))
        sp.if (compute_chess_logic_323i.value != 0) & (compute_chess_logic_323i.value != ((6 * player_chess_logic_286) * (-1))):
          sp.if (compute_chess_logic_323i.value == (5 * player_chess_logic_286)) | (compute_chess_logic_323i.value == (4 * player_chess_logic_286)):
            check.value.push(sp.record(i = i_chess_logic_286 + (l * 1), j = j_chess_logic_286 + (l * (-1))))
          sp.else:
            continue_.value = False
    continue_.value = True
    sp.result(sp.set_type_expr(sp.set_type_expr(check.value, sp.TList(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))), sp.TList(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))))

  @sp.private_lambda()
  def move_piece(_x14):
    sp.set_type(_x14.move, sp.TRecord(claim_repeat = sp.TOption(sp.TPair(sp.TNat, sp.TNat)), f = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")), promotion = sp.TOption(sp.TNat), t = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))).layout(("claim_repeat", ("f", ("promotion", "t")))))
    board_state = sp.local("board_state", self.data.board_state)
    is_draw = sp.local("is_draw", False)
    sp.set_type(_x14.move, sp.TRecord(claim_repeat = sp.TOption(sp.TPair(sp.TNat, sp.TNat)), f = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")), promotion = sp.TOption(sp.TNat), t = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))).layout(("claim_repeat", ("f", ("promotion", "t")))))
    sp.set_type(board_state.value, sp.TRecord(castle = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TBool)), check = sp.TBool, deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), enPassant = sp.TOption(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))), fullMove = sp.TNat, halfMoveClock = sp.TNat, kings = sp.TMap(sp.TInt, sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j"))), nextPlayer = sp.TInt, pastMoves = sp.TMap(sp.TPair(sp.TNat, sp.TInt), sp.TBytes)).layout(("castle", ("check", ("deck", ("enPassant", ("fullMove", ("halfMoveClock", ("kings", ("nextPlayer", "pastMoves"))))))))))
    sp.verify((_x14.move.f.i >= 0) & (_x14.move.f.i < 8))
    sp.verify((_x14.move.f.j >= 0) & (_x14.move.f.j < 8))
    sp.verify((_x14.move.t.i >= 0) & (_x14.move.t.i < 8))
    sp.verify((_x14.move.t.j >= 0) & (_x14.move.t.j < 8))
    sp.verify(((_x14.move.t.j - _x14.move.f.j) != 0) | ((_x14.move.t.i - _x14.move.f.i) != 0))
    compute_chess_logic_386 = sp.local("compute_chess_logic_386", board_state.value.nextPlayer)
    deck = sp.local("deck", board_state.value.deck)
    sp.verify(sp.sign(deck.value[_x14.move.f.i][_x14.move.f.j]) == compute_chess_logic_386.value)
    sp.verify(sp.sign(deck.value[_x14.move.t.i][_x14.move.t.j]) != compute_chess_logic_386.value)
    sp.verify(deck.value[_x14.move.t.i][_x14.move.t.j] != (6 * compute_chess_logic_386.value))
    resetClock = sp.local("resetClock", False)
    promotion = sp.local("promotion", False)
    sp.if (abs(deck.value[_x14.move.f.i][_x14.move.f.j])) == 6:
      sp.if ((abs(_x14.move.t.j - _x14.move.f.j)) == 2) & ((_x14.move.t.i - _x14.move.f.i) == 0):
        sp.verify((((compute_chess_logic_386.value == 1) & (_x14.move.f.i == 0)) & (_x14.move.f.j == 4)) | (((compute_chess_logic_386.value == (-1)) & (_x14.move.f.i == 7)) & (_x14.move.f.j == 4)))
        sp.verify(board_state.value.castle[compute_chess_logic_386.value][sp.sign(_x14.move.t.j - _x14.move.f.j)])
        sp.if (_x14.move.t.j - _x14.move.f.j) > 0:
          deck.value[_x14.move.t.i][7] = 0
        sp.else:
          deck.value[_x14.move.t.i][0] = 0
        deck.value[_x14.move.t.i][_x14.move.t.j - sp.sign(_x14.move.t.j - _x14.move.f.j)] = 2 * compute_chess_logic_386.value
      sp.else:
        sp.verify(((abs(_x14.move.t.j - _x14.move.f.j)) <= 1) & ((abs(_x14.move.t.i - _x14.move.f.i)) <= 1))
      board_state.value.kings[compute_chess_logic_386.value] = sp.record(i = _x14.move.t.i, j = _x14.move.t.j)
      board_state.value.castle[compute_chess_logic_386.value] = {-1 : False, 1 : False}
    sp.if (abs(deck.value[_x14.move.f.i][_x14.move.f.j])) == 2:
      sp.verify(((_x14.move.t.j - _x14.move.f.j) == 0) | ((_x14.move.t.i - _x14.move.f.i) == 0))
      sp.for k in sp.range(1, sp.to_int(sp.max(abs(_x14.move.t.j - _x14.move.f.j), abs(_x14.move.t.i - _x14.move.f.i))) - 1):
        sp.verify(deck.value[_x14.move.f.i + (k * sp.sign(_x14.move.t.i - _x14.move.f.i))][_x14.move.f.j + (k * sp.sign(_x14.move.t.j - _x14.move.f.j))] == 0)
      sp.if (((compute_chess_logic_386.value == 1) & (_x14.move.f.i == 0)) & (_x14.move.f.j == 0)) | (((compute_chess_logic_386.value == (-1)) & (_x14.move.f.i == 7)) & (_x14.move.f.j == 0)):
        board_state.value.castle[compute_chess_logic_386.value][-1] = False
      sp.if (((compute_chess_logic_386.value == 1) & (_x14.move.f.i == 0)) & (_x14.move.f.j == 7)) | (((compute_chess_logic_386.value == (-1)) & (_x14.move.f.i == 7)) & (_x14.move.f.j == 7)):
        board_state.value.castle[compute_chess_logic_386.value][1] = False
    sp.if (abs(deck.value[_x14.move.f.i][_x14.move.f.j])) == 5:
      sp.verify((((_x14.move.t.j - _x14.move.f.j) == 0) | ((_x14.move.t.i - _x14.move.f.i) == 0)) | ((abs(_x14.move.t.j - _x14.move.f.j)) == (abs(_x14.move.t.i - _x14.move.f.i))))
      sp.for k in sp.range(1, sp.to_int(sp.max(abs(_x14.move.t.j - _x14.move.f.j), abs(_x14.move.t.i - _x14.move.f.i))) - 1):
        sp.verify(deck.value[_x14.move.f.i + (k * sp.sign(_x14.move.t.i - _x14.move.f.i))][_x14.move.f.j + (k * sp.sign(_x14.move.t.j - _x14.move.f.j))] == 0)
    sp.if (abs(deck.value[_x14.move.f.i][_x14.move.f.j])) == 4:
      sp.verify((abs(_x14.move.t.j - _x14.move.f.j)) == (abs(_x14.move.t.i - _x14.move.f.i)))
      sp.for k in sp.range(1, sp.to_int(sp.max(abs(_x14.move.t.j - _x14.move.f.j), abs(_x14.move.t.i - _x14.move.f.i))) - 1):
        sp.verify(deck.value[_x14.move.f.i + (k * sp.sign(_x14.move.t.i - _x14.move.f.i))][_x14.move.f.j + (k * sp.sign(_x14.move.t.j - _x14.move.f.j))] == 0)
    sp.if (abs(deck.value[_x14.move.f.i][_x14.move.f.j])) == 3:
      sp.verify((abs((_x14.move.t.j - _x14.move.f.j) * (_x14.move.t.i - _x14.move.f.i))) == 2)
    sp.if (abs(deck.value[_x14.move.f.i][_x14.move.f.j])) == 1:
      sp.verify(((((((((abs(_x14.move.t.j - _x14.move.f.j)) == 1) & (((_x14.move.t.i - _x14.move.f.i) * compute_chess_logic_386.value) == 1)) & board_state.value.enPassant.is_some()) & (_x14.move.f.i == board_state.value.enPassant.open_some().i)) & (_x14.move.f.j == board_state.value.enPassant.open_some().j)) | ((((_x14.move.t.j - _x14.move.f.j) == 0) & (((_x14.move.t.i - _x14.move.f.i) * compute_chess_logic_386.value) == 2)) & (deck.value[_x14.move.t.i - compute_chess_logic_386.value][_x14.move.t.j] == 0))) | (((_x14.move.t.j - _x14.move.f.j) == 0) & (((_x14.move.t.i - _x14.move.f.i) * compute_chess_logic_386.value) == 1))) | ((((abs(_x14.move.t.j - _x14.move.f.j)) == 1) & (((_x14.move.t.i - _x14.move.f.i) * compute_chess_logic_386.value) == 1)) & (deck.value[_x14.move.t.i][_x14.move.t.j] != compute_chess_logic_386.value)))
      sp.if ((_x14.move.t.i - _x14.move.f.i) * compute_chess_logic_386.value) == 2:
        board_state.value.enPassant = sp.some(sp.record(i = _x14.move.t.i - compute_chess_logic_386.value, j = _x14.move.t.j))
      sp.else:
        board_state.value.enPassant = sp.none
      resetClock.value = True
      sp.if (_x14.move.t.i == 7) | (_x14.move.t.i == 0):
        promotion.value = True
    sp.else:
      board_state.value.enPassant = sp.none
    compute_chess_logic_406 = sp.local("compute_chess_logic_406", board_state.value.kings[compute_chess_logic_386.value])
    sp.if deck.value[_x14.move.t.i][_x14.move.t.j] != 0:
      resetClock.value = True
    sp.if promotion.value:
      compute_chess_logic_412 = sp.local("compute_chess_logic_412", _x14.move.promotion.open_some())
      sp.verify((compute_chess_logic_412.value > 1) & (compute_chess_logic_412.value < 6))
      deck.value[_x14.move.t.i][_x14.move.t.j] = sp.mul(compute_chess_logic_386.value, compute_chess_logic_412.value)
    sp.else:
      sp.verify(_x14.move.promotion.is_variant('None'))
      deck.value[_x14.move.t.i][_x14.move.t.j] = deck.value[_x14.move.f.i][_x14.move.f.j]
    deck.value[_x14.move.f.i][_x14.move.f.j] = 0
    sp.verify(sp.len(sp.set_type_expr(_x14.get_movable_to, sp.TLambda(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), i = sp.TInt, j = sp.TInt, pawn_capturing = sp.TBool, player = sp.TInt).layout(("deck", ("i", ("j", ("pawn_capturing", "player"))))), sp.TList(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))))(sp.record(deck = deck.value, i = compute_chess_logic_406.value.i, j = compute_chess_logic_406.value.j, pawn_capturing = True, player = compute_chess_logic_386.value * (-1)))) == 0)
    board_state.value.pastMoves[(board_state.value.fullMove, compute_chess_logic_386.value)] = sp.blake2b(sp.pack((board_state.value.castle, deck.value, board_state.value.enPassant)))
    board_state.value.deck = deck.value
    sp.if _x14.move.claim_repeat.is_some():
      match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd = sp.match_tuple(_x14.move.claim_repeat.open_some(), "match_pair_chess_logic_428_fst", "match_pair_chess_logic_428_snd")
      sp.set_type(sp.set([match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd, board_state.value.fullMove]), sp.TSet(sp.TNat))
      sp.verify(sp.len(sp.set([match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd, board_state.value.fullMove])) == 3)
      sp.for fullMove in sp.set([match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd, board_state.value.fullMove]).elements():
        sp.verify(sp.blake2b(sp.pack((board_state.value.castle, board_state.value.deck, board_state.value.enPassant))) == board_state.value.pastMoves[(fullMove, compute_chess_logic_386.value)], ('NotSameMove', sp.record(fullMove = fullMove)))
      is_draw.value = True
    sp.if (compute_chess_logic_386.value * (-1)) == 1:
      board_state.value.fullMove += 1
    sp.if resetClock.value:
      board_state.value.halfMoveClock = 0
    sp.else:
      board_state.value.halfMoveClock += 1
    compute_chess_logic_440 = sp.local("compute_chess_logic_440", board_state.value.kings[compute_chess_logic_386.value * (-1)])
    sp.if sp.len(sp.set_type_expr(_x14.get_movable_to, sp.TLambda(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), i = sp.TInt, j = sp.TInt, pawn_capturing = sp.TBool, player = sp.TInt).layout(("deck", ("i", ("j", ("pawn_capturing", "player"))))), sp.TList(sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))))(sp.record(deck = deck.value, i = compute_chess_logic_440.value.i, j = compute_chess_logic_440.value.j, pawn_capturing = True, player = (compute_chess_logic_386.value * (-1)) * (-1)))) > 0:
      board_state.value.check = True
    sp.else:
      board_state.value.check = False
    sp.if board_state.value.halfMoveClock > 49:
      is_draw.value = True
    sp.result((board_state.value, is_draw.value))

sp.add_compilation_target("test", Contract())