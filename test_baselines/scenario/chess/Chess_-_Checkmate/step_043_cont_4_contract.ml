open SmartML

module Contract = struct
  let%entry_point answer_stalemate params =
    set_type params (`accept unit + `refuse {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}});
    verify (sender = (Map.get data.players_map data.board_state.nextPlayer)) ~msg:"Wrong player";
    verify (is_variant "claim_stalemate" data.status);
    match params with
      | `accept accept ->
        data.status <- variant finished "draw"
      | `refuse refuse ->
        let%mutable board_state = data.board_state in ();
        let%mutable is_draw = false in ();
        set_type refuse {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}};
        set_type board_state {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes};
        verify ((refuse.f.i >= (int 0)) && (refuse.f.i < (int 8)));
        verify ((refuse.f.j >= (int 0)) && (refuse.f.j < (int 8)));
        verify ((refuse.t.i >= (int 0)) && (refuse.t.i < (int 8)));
        verify ((refuse.t.j >= (int 0)) && (refuse.t.j < (int 8)));
        verify (((refuse.t.j - refuse.f.j) <> (int 0)) || ((refuse.t.i - refuse.f.i) <> (int 0)));
        let%mutable compute_chess_logic_386 = board_state.nextPlayer in ();
        let%mutable deck = board_state.deck in ();
        verify ((sign (Map.get (Map.get deck refuse.f.i) refuse.f.j)) = compute_chess_logic_386);
        verify ((sign (Map.get (Map.get deck refuse.t.i) refuse.t.j)) <> compute_chess_logic_386);
        verify ((Map.get (Map.get deck refuse.t.i) refuse.t.j) <> ((int 6) * compute_chess_logic_386));
        let%mutable resetClock = false in ();
        let%mutable promotion = false in ();
        if (abs (Map.get (Map.get deck refuse.f.i) refuse.f.j)) = (nat 6) then
          (
            if ((abs (refuse.t.j - refuse.f.j)) = (nat 2)) && ((refuse.t.i - refuse.f.i) = (int 0)) then
              (
                verify ((((compute_chess_logic_386 = (int 1)) && (refuse.f.i = (int 0))) && (refuse.f.j = (int 4))) || (((compute_chess_logic_386 = (int ((-1)))) && (refuse.f.i = (int 7))) && (refuse.f.j = (int 4))));
                verify (Map.get (Map.get board_state.castle compute_chess_logic_386) (sign (refuse.t.j - refuse.f.j)));
                if (refuse.t.j - refuse.f.j) > (int 0) then
                  Map.set (Map.get deck refuse.t.i) (int 7) (int 0)
                else
                  Map.set (Map.get deck refuse.t.i) (int 0) (int 0);
                Map.set (Map.get deck refuse.t.i) (refuse.t.j - (sign (refuse.t.j - refuse.f.j))) ((int 2) * compute_chess_logic_386)
              )
            else
              verify (((abs (refuse.t.j - refuse.f.j)) <= (nat 1)) && ((abs (refuse.t.i - refuse.f.i)) <= (nat 1)));
            Map.set board_state.kings compute_chess_logic_386 {i = refuse.t.i; j = refuse.t.j};
            Map.set board_state.castle compute_chess_logic_386 (Map.make [(int ((-1)), false); (int 1, false)])
          );
        if (abs (Map.get (Map.get deck refuse.f.i) refuse.f.j)) = (nat 2) then
          (
            verify (((refuse.t.j - refuse.f.j) = (int 0)) || ((refuse.t.i - refuse.f.i) = (int 0)));
            List.iter (fun k ->
              verify ((Map.get (Map.get deck (refuse.f.i + (k * (sign (refuse.t.i - refuse.f.i))))) (refuse.f.j + (k * (sign (refuse.t.j - refuse.f.j))))) = (int 0))
            ) (range (int 1) ((to_int (max (abs (refuse.t.j - refuse.f.j)) (abs (refuse.t.i - refuse.f.i)))) - (int 1)) (int 1));
            if (((compute_chess_logic_386 = (int 1)) && (refuse.f.i = (int 0))) && (refuse.f.j = (int 0))) || (((compute_chess_logic_386 = (int ((-1)))) && (refuse.f.i = (int 7))) && (refuse.f.j = (int 0))) then
              Map.set (Map.get board_state.castle compute_chess_logic_386) (int ((-1))) false;
            if (((compute_chess_logic_386 = (int 1)) && (refuse.f.i = (int 0))) && (refuse.f.j = (int 7))) || (((compute_chess_logic_386 = (int ((-1)))) && (refuse.f.i = (int 7))) && (refuse.f.j = (int 7))) then
              Map.set (Map.get board_state.castle compute_chess_logic_386) (int 1) false
          );
        if (abs (Map.get (Map.get deck refuse.f.i) refuse.f.j)) = (nat 5) then
          (
            verify ((((refuse.t.j - refuse.f.j) = (int 0)) || ((refuse.t.i - refuse.f.i) = (int 0))) || ((abs (refuse.t.j - refuse.f.j)) = (abs (refuse.t.i - refuse.f.i))));
            List.iter (fun k ->
              verify ((Map.get (Map.get deck (refuse.f.i + (k * (sign (refuse.t.i - refuse.f.i))))) (refuse.f.j + (k * (sign (refuse.t.j - refuse.f.j))))) = (int 0))
            ) (range (int 1) ((to_int (max (abs (refuse.t.j - refuse.f.j)) (abs (refuse.t.i - refuse.f.i)))) - (int 1)) (int 1))
          );
        if (abs (Map.get (Map.get deck refuse.f.i) refuse.f.j)) = (nat 4) then
          (
            verify ((abs (refuse.t.j - refuse.f.j)) = (abs (refuse.t.i - refuse.f.i)));
            List.iter (fun k ->
              verify ((Map.get (Map.get deck (refuse.f.i + (k * (sign (refuse.t.i - refuse.f.i))))) (refuse.f.j + (k * (sign (refuse.t.j - refuse.f.j))))) = (int 0))
            ) (range (int 1) ((to_int (max (abs (refuse.t.j - refuse.f.j)) (abs (refuse.t.i - refuse.f.i)))) - (int 1)) (int 1))
          );
        if (abs (Map.get (Map.get deck refuse.f.i) refuse.f.j)) = (nat 3) then
          verify ((abs ((refuse.t.j - refuse.f.j) * (refuse.t.i - refuse.f.i))) = (nat 2));
        if (abs (Map.get (Map.get deck refuse.f.i) refuse.f.j)) = (nat 1) then
          (
            verify (((((((((abs (refuse.t.j - refuse.f.j)) = (nat 1)) && (((refuse.t.i - refuse.f.i) * compute_chess_logic_386) = (int 1))) && (is_some board_state.enPassant)) && (refuse.f.i = (open_some board_state.enPassant).i)) && (refuse.f.j = (open_some board_state.enPassant).j)) || ((((refuse.t.j - refuse.f.j) = (int 0)) && (((refuse.t.i - refuse.f.i) * compute_chess_logic_386) = (int 2))) && ((Map.get (Map.get deck (refuse.t.i - compute_chess_logic_386)) refuse.t.j) = (int 0)))) || (((refuse.t.j - refuse.f.j) = (int 0)) && (((refuse.t.i - refuse.f.i) * compute_chess_logic_386) = (int 1)))) || ((((abs (refuse.t.j - refuse.f.j)) = (nat 1)) && (((refuse.t.i - refuse.f.i) * compute_chess_logic_386) = (int 1))) && ((Map.get (Map.get deck refuse.t.i) refuse.t.j) <> compute_chess_logic_386)));
            if ((refuse.t.i - refuse.f.i) * compute_chess_logic_386) = (int 2) then
              board_state.enPassant <- some {i = (refuse.t.i - compute_chess_logic_386); j = refuse.t.j}
            else
              board_state.enPassant <- None;
            resetClock <- true;
            if (refuse.t.i = (int 7)) || (refuse.t.i = (int 0)) then
              promotion <- true
          )
        else
          board_state.enPassant <- None;
        let%mutable compute_chess_logic_406 = (Map.get board_state.kings compute_chess_logic_386) in ();
        if (Map.get (Map.get deck refuse.t.i) refuse.t.j) <> (int 0) then
          resetClock <- true;
        if promotion then
          (
            let%mutable compute_chess_logic_412 = (open_some refuse.promotion) in ();
            verify ((compute_chess_logic_412 > (nat 1)) && (compute_chess_logic_412 < (nat 6)));
            Map.set (Map.get deck refuse.t.i) refuse.t.j (mul compute_chess_logic_386 compute_chess_logic_412)
          )
        else
          (
            verify (is_variant "None" refuse.promotion);
            Map.set (Map.get deck refuse.t.i) refuse.t.j (Map.get (Map.get deck refuse.f.i) refuse.f.j)
          );
        Map.set (Map.get deck refuse.f.i) refuse.f.j (int 0);
        verify ((len ({deck = deck; i = compute_chess_logic_406.i; j = compute_chess_logic_406.j; pawn_capturing = true; player = (compute_chess_logic_386 * (int ((-1))))} (set_type_expr self.get_movable_to lambda {deck = map int (map int int); i = int; j = int; pawn_capturing = bool; player = int} (list {i = int; j = int})))) = (nat 0));
        Map.set board_state.pastMoves (board_state.fullMove, compute_chess_logic_386) (blake2b (pack (board_state.castle, deck, board_state.enPassant)));
        board_state.deck <- deck;
        if is_some refuse.claim_repeat then
          match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd = match_tuple(open_some refuse.claim_repeat, "match_pair_chess_logic_428_fst", "match_pair_chess_logic_428_snd")
          set_type (Set.make [match_pair_chess_logic_428_fst; match_pair_chess_logic_428_snd; board_state.fullMove]) (set nat);
          verify ((len (Set.make [match_pair_chess_logic_428_fst; match_pair_chess_logic_428_snd; board_state.fullMove])) = (nat 3));
          List.iter (fun fullMove ->
            verify ((blake2b (pack (board_state.castle, board_state.deck, board_state.enPassant))) = (Map.get board_state.pastMoves (fullMove, compute_chess_logic_386))) ~msg:("NotSameMove", {fullMove = fullMove})
          ) (Set.elements (Set.make [match_pair_chess_logic_428_fst; match_pair_chess_logic_428_snd; board_state.fullMove]));
          is_draw <- true;
        if (compute_chess_logic_386 * (int ((-1)))) = (int 1) then
          board_state.fullMove <- board_state.fullMove + (nat 1);
        if resetClock then
          board_state.halfMoveClock <- nat 0
        else
          board_state.halfMoveClock <- board_state.halfMoveClock + (nat 1);
        let%mutable compute_chess_logic_440 = (Map.get board_state.kings (compute_chess_logic_386 * (int ((-1))))) in ();
        if (len ({deck = deck; i = compute_chess_logic_440.i; j = compute_chess_logic_440.j; pawn_capturing = true; player = ((compute_chess_logic_386 * (int ((-1)))) * (int ((-1))))} (set_type_expr self.get_movable_to lambda {deck = map int (map int int); i = int; j = int; pawn_capturing = bool; player = int} (list {i = int; j = int})))) > (nat 0) then
          board_state.check <- true
        else
          board_state.check <- false;
        if board_state.halfMoveClock > (nat 49) then
          is_draw <- true;
        data.board_state.nextPlayer <- data.board_state.nextPlayer * (int ((-1)));
        data.status <- variant force_play ()


  let%entry_point claim_checkmate () =
    let%mutable board_state = data.board_state in ();
    let%mutable checkmate = true in ();
    let%mutable deck = board_state.deck in ();
    let%mutable compute_chess_logic_192 = ({deck = deck; i = (Map.get board_state.kings board_state.nextPlayer).i; j = (Map.get board_state.kings board_state.nextPlayer).j; pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
    if (len compute_chess_logic_192) = (nat 0) then
      checkmate <- false
    else
      (
        if checkmate then
          (
            let%mutable destination_piece1_-1 = (Map.get ~default_value:(board_state.nextPlayer * (int 1)) (Map.get ~default_value:(Map.make []) deck ((Map.get board_state.kings board_state.nextPlayer).i + (int 1))) ((Map.get board_state.kings board_state.nextPlayer).j + (int ((-1))))) in ();
            if (destination_piece1_-1 = (int 0)) || ((sign destination_piece1_-1) <> board_state.nextPlayer) then
              (
                let%mutable compute_chess_logic_207 = ({deck = deck; i = ((Map.get board_state.kings board_state.nextPlayer).i + (int 1)); j = ((Map.get board_state.kings board_state.nextPlayer).j + (int ((-1)))); pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
                if (len compute_chess_logic_207) = (nat 0) then
                  checkmate <- false
              )
          );
        if checkmate then
          (
            let%mutable destination_piece1_0 = (Map.get ~default_value:(board_state.nextPlayer * (int 1)) (Map.get ~default_value:(Map.make []) deck ((Map.get board_state.kings board_state.nextPlayer).i + (int 1))) ((Map.get board_state.kings board_state.nextPlayer).j + (int 0))) in ();
            if (destination_piece1_0 = (int 0)) || ((sign destination_piece1_0) <> board_state.nextPlayer) then
              (
                let%mutable compute_chess_logic_207i = ({deck = deck; i = ((Map.get board_state.kings board_state.nextPlayer).i + (int 1)); j = ((Map.get board_state.kings board_state.nextPlayer).j + (int 0)); pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
                if (len compute_chess_logic_207i) = (nat 0) then
                  checkmate <- false
              )
          );
        if checkmate then
          (
            let%mutable destination_piece1_1 = (Map.get ~default_value:(board_state.nextPlayer * (int 1)) (Map.get ~default_value:(Map.make []) deck ((Map.get board_state.kings board_state.nextPlayer).i + (int 1))) ((Map.get board_state.kings board_state.nextPlayer).j + (int 1))) in ();
            if (destination_piece1_1 = (int 0)) || ((sign destination_piece1_1) <> board_state.nextPlayer) then
              (
                let%mutable compute_chess_logic_207 = ({deck = deck; i = ((Map.get board_state.kings board_state.nextPlayer).i + (int 1)); j = ((Map.get board_state.kings board_state.nextPlayer).j + (int 1)); pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
                if (len compute_chess_logic_207) = (nat 0) then
                  checkmate <- false
              )
          );
        if checkmate then
          (
            let%mutable destination_piece0_1 = (Map.get ~default_value:(board_state.nextPlayer * (int 1)) (Map.get ~default_value:(Map.make []) deck ((Map.get board_state.kings board_state.nextPlayer).i + (int 0))) ((Map.get board_state.kings board_state.nextPlayer).j + (int 1))) in ();
            if (destination_piece0_1 = (int 0)) || ((sign destination_piece0_1) <> board_state.nextPlayer) then
              (
                let%mutable compute_chess_logic_207i = ({deck = deck; i = ((Map.get board_state.kings board_state.nextPlayer).i + (int 0)); j = ((Map.get board_state.kings board_state.nextPlayer).j + (int 1)); pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
                if (len compute_chess_logic_207i) = (nat 0) then
                  checkmate <- false
              )
          );
        if checkmate then
          (
            let%mutable destination_piece-1_1 = (Map.get ~default_value:(board_state.nextPlayer * (int 1)) (Map.get ~default_value:(Map.make []) deck ((Map.get board_state.kings board_state.nextPlayer).i + (int ((-1))))) ((Map.get board_state.kings board_state.nextPlayer).j + (int 1))) in ();
            if (destination_piece-1_1 = (int 0)) || ((sign destination_piece-1_1) <> board_state.nextPlayer) then
              (
                let%mutable compute_chess_logic_207 = ({deck = deck; i = ((Map.get board_state.kings board_state.nextPlayer).i + (int ((-1)))); j = ((Map.get board_state.kings board_state.nextPlayer).j + (int 1)); pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
                if (len compute_chess_logic_207) = (nat 0) then
                  checkmate <- false
              )
          );
        if checkmate then
          (
            let%mutable destination_piece-1_0 = (Map.get ~default_value:(board_state.nextPlayer * (int 1)) (Map.get ~default_value:(Map.make []) deck ((Map.get board_state.kings board_state.nextPlayer).i + (int ((-1))))) ((Map.get board_state.kings board_state.nextPlayer).j + (int 0))) in ();
            if (destination_piece-1_0 = (int 0)) || ((sign destination_piece-1_0) <> board_state.nextPlayer) then
              (
                let%mutable compute_chess_logic_207i = ({deck = deck; i = ((Map.get board_state.kings board_state.nextPlayer).i + (int ((-1)))); j = ((Map.get board_state.kings board_state.nextPlayer).j + (int 0)); pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
                if (len compute_chess_logic_207i) = (nat 0) then
                  checkmate <- false
              )
          );
        if checkmate then
          (
            let%mutable destination_piece-1_-1 = (Map.get ~default_value:(board_state.nextPlayer * (int 1)) (Map.get ~default_value:(Map.make []) deck ((Map.get board_state.kings board_state.nextPlayer).i + (int ((-1))))) ((Map.get board_state.kings board_state.nextPlayer).j + (int ((-1))))) in ();
            if (destination_piece-1_-1 = (int 0)) || ((sign destination_piece-1_-1) <> board_state.nextPlayer) then
              (
                let%mutable compute_chess_logic_207 = ({deck = deck; i = ((Map.get board_state.kings board_state.nextPlayer).i + (int ((-1)))); j = ((Map.get board_state.kings board_state.nextPlayer).j + (int ((-1)))); pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
                if (len compute_chess_logic_207) = (nat 0) then
                  checkmate <- false
              )
          );
        if checkmate then
          (
            let%mutable destination_piece0_-1 = (Map.get ~default_value:(board_state.nextPlayer * (int 1)) (Map.get ~default_value:(Map.make []) deck ((Map.get board_state.kings board_state.nextPlayer).i + (int 0))) ((Map.get board_state.kings board_state.nextPlayer).j + (int ((-1))))) in ();
            if (destination_piece0_-1 = (int 0)) || ((sign destination_piece0_-1) <> board_state.nextPlayer) then
              (
                let%mutable compute_chess_logic_207i = ({deck = deck; i = ((Map.get board_state.kings board_state.nextPlayer).i + (int 0)); j = ((Map.get board_state.kings board_state.nextPlayer).j + (int ((-1)))); pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
                if (len compute_chess_logic_207i) = (nat 0) then
                  checkmate <- false
              )
          );
        if checkmate then
          if (len compute_chess_logic_192) = (nat 1) then
            List.iter (fun attacking_square ->
              let%mutable compute_chess_logic_218 = ({deck = deck; i = attacking_square.i; j = attacking_square.j; pawn_capturing = true; player = (board_state.nextPlayer * (int ((-1))))} self.get_movable_to) in ();
              let%mutable compute_chess_logic_218i = compute_chess_logic_218 in ();
              checkmate <- {attacking_square = attacking_square; checkmate = checkmate; deck = deck; defending_squares = compute_chess_logic_218i; get_movable_to = self.get_movable_to; king = (Map.get board_state.kings board_state.nextPlayer); nextPlayer = board_state.nextPlayer} (fun _x28 -> defending_squares_chess_logic_538, attacking_square_chess_logic_538, deck_chess_logic_538, king_chess_logic_538, nextPlayer_chess_logic_538, checkmate_chess_logic_538, get_movable_to_chess_logic_538 = match_record(_x28, "defending_squares", "attacking_square", "deck", "king", "nextPlayer", "checkmate", "get_movable_to")
let%mutable checkmate = checkmate_chess_logic_538 in ();
List.iter (fun defending_square ->
  if checkmate then
    (
      let%mutable test_deck = deck_chess_logic_538 in ();
      Map.set (Map.get test_deck attacking_square_chess_logic_538.i) attacking_square_chess_logic_538.j (Map.get (Map.get test_deck defending_square.i) defending_square.j);
      Map.set (Map.get test_deck defending_square.i) defending_square.j (int 0);
      let%mutable king = king_chess_logic_538 in ();
      if (Map.get (Map.get test_deck attacking_square_chess_logic_538.i) attacking_square_chess_logic_538.j) = ((int 6) * nextPlayer_chess_logic_538) then
        king <- {i = attacking_square_chess_logic_538.i; j = attacking_square_chess_logic_538.j};
      if (len ({deck = test_deck; i = king.i; j = king.j; pawn_capturing = true; player = (nextPlayer_chess_logic_538 * (int ((-1))))} get_movable_to_chess_logic_538)) = (nat 0) then
        checkmate <- false
    )
) defending_squares_chess_logic_538;
result checkmate);
              if checkmate then
                if ((Map.get (Map.get deck attacking_square.i) attacking_square.j) <> (((int 3) * board_state.nextPlayer) * (int ((-1))))) && ((Map.get (Map.get deck attacking_square.i) attacking_square.j) <> (((int 1) * board_state.nextPlayer) * (int ((-1))))) then
                  if attacking_square.i = (Map.get board_state.kings board_state.nextPlayer).i then
                    List.iter (fun obstructing_j ->
                      let%mutable compute_chess_logic_231 = ({deck = deck; i = (Map.get board_state.kings board_state.nextPlayer).i; j = obstructing_j; pawn_capturing = false; player = board_state.nextPlayer} self.get_movable_to) in ();
                      let%mutable compute_chess_logic_231i = compute_chess_logic_231 in ();
                      checkmate <- {attacking_square = {i = (Map.get board_state.kings board_state.nextPlayer).i; j = obstructing_j}; checkmate = checkmate; deck = deck; defending_squares = compute_chess_logic_231i; get_movable_to = self.get_movable_to; king = (Map.get board_state.kings board_state.nextPlayer); nextPlayer = board_state.nextPlayer} (fun _x28 -> defending_squares_chess_logic_538, attacking_square_chess_logic_538, deck_chess_logic_538, king_chess_logic_538, nextPlayer_chess_logic_538, checkmate_chess_logic_538, get_movable_to_chess_logic_538 = match_record(_x28, "defending_squares", "attacking_square", "deck", "king", "nextPlayer", "checkmate", "get_movable_to")
let%mutable checkmate = checkmate_chess_logic_538 in ();
List.iter (fun defending_square ->
  if checkmate then
    (
      let%mutable test_deck = deck_chess_logic_538 in ();
      Map.set (Map.get test_deck attacking_square_chess_logic_538.i) attacking_square_chess_logic_538.j (Map.get (Map.get test_deck defending_square.i) defending_square.j);
      Map.set (Map.get test_deck defending_square.i) defending_square.j (int 0);
      let%mutable king = king_chess_logic_538 in ();
      if (Map.get (Map.get test_deck attacking_square_chess_logic_538.i) attacking_square_chess_logic_538.j) = ((int 6) * nextPlayer_chess_logic_538) then
        king <- {i = attacking_square_chess_logic_538.i; j = attacking_square_chess_logic_538.j};
      if (len ({deck = test_deck; i = king.i; j = king.j; pawn_capturing = true; player = (nextPlayer_chess_logic_538 * (int ((-1))))} get_movable_to_chess_logic_538)) = (nat 0) then
        checkmate <- false
    )
) defending_squares_chess_logic_538;
result checkmate)
                    ) (range (min (Map.get board_state.kings board_state.nextPlayer).j attacking_square.j) (max (Map.get board_state.kings board_state.nextPlayer).j attacking_square.j) (int 1))
                  else
                    if attacking_square.j = (Map.get board_state.kings board_state.nextPlayer).j then
                      List.iter (fun obstructing_i ->
                        let%mutable compute_chess_logic_239 = ({deck = deck; i = obstructing_i; j = (Map.get board_state.kings board_state.nextPlayer).j; pawn_capturing = false; player = board_state.nextPlayer} self.get_movable_to) in ();
                        let%mutable compute_chess_logic_239i = compute_chess_logic_239 in ();
                        checkmate <- {attacking_square = {i = obstructing_i; j = (Map.get board_state.kings board_state.nextPlayer).j}; checkmate = checkmate; deck = deck; defending_squares = compute_chess_logic_239i; get_movable_to = self.get_movable_to; king = (Map.get board_state.kings board_state.nextPlayer); nextPlayer = board_state.nextPlayer} (fun _x28 -> defending_squares_chess_logic_538, attacking_square_chess_logic_538, deck_chess_logic_538, king_chess_logic_538, nextPlayer_chess_logic_538, checkmate_chess_logic_538, get_movable_to_chess_logic_538 = match_record(_x28, "defending_squares", "attacking_square", "deck", "king", "nextPlayer", "checkmate", "get_movable_to")
let%mutable checkmate = checkmate_chess_logic_538 in ();
List.iter (fun defending_square ->
  if checkmate then
    (
      let%mutable test_deck = deck_chess_logic_538 in ();
      Map.set (Map.get test_deck attacking_square_chess_logic_538.i) attacking_square_chess_logic_538.j (Map.get (Map.get test_deck defending_square.i) defending_square.j);
      Map.set (Map.get test_deck defending_square.i) defending_square.j (int 0);
      let%mutable king = king_chess_logic_538 in ();
      if (Map.get (Map.get test_deck attacking_square_chess_logic_538.i) attacking_square_chess_logic_538.j) = ((int 6) * nextPlayer_chess_logic_538) then
        king <- {i = attacking_square_chess_logic_538.i; j = attacking_square_chess_logic_538.j};
      if (len ({deck = test_deck; i = king.i; j = king.j; pawn_capturing = true; player = (nextPlayer_chess_logic_538 * (int ((-1))))} get_movable_to_chess_logic_538)) = (nat 0) then
        checkmate <- false
    )
) defending_squares_chess_logic_538;
result checkmate)
                      ) (range (min (Map.get board_state.kings board_state.nextPlayer).i attacking_square.i) (max (Map.get board_state.kings board_state.nextPlayer).i attacking_square.i) (int 1))
                    else
                      (
                        let%mutable compute_chess_logic_245 = (to_int (abs ((Map.get board_state.kings board_state.nextPlayer).i - attacking_square.i))) in ();
                        let%mutable compute_chess_logic_246 = (sign (attacking_square.i - (Map.get board_state.kings board_state.nextPlayer).i)) in ();
                        let%mutable compute_chess_logic_247 = (sign (attacking_square.j - (Map.get board_state.kings board_state.nextPlayer).j)) in ();
                        List.iter (fun i ->
                          let%mutable compute_chess_logic_250 = ((Map.get board_state.kings board_state.nextPlayer).i + (compute_chess_logic_246 * i)) in ();
                          let%mutable compute_chess_logic_251 = ((Map.get board_state.kings board_state.nextPlayer).j + (compute_chess_logic_247 * i)) in ();
                          let%mutable compute_chess_logic_252 = ({deck = deck; i = compute_chess_logic_250; j = compute_chess_logic_251; pawn_capturing = false; player = board_state.nextPlayer} self.get_movable_to) in ();
                          let%mutable compute_chess_logic_252i = compute_chess_logic_252 in ();
                          checkmate <- {attacking_square = {i = compute_chess_logic_250; j = compute_chess_logic_251}; checkmate = checkmate; deck = deck; defending_squares = compute_chess_logic_252i; get_movable_to = self.get_movable_to; king = (Map.get board_state.kings board_state.nextPlayer); nextPlayer = board_state.nextPlayer} (fun _x28 -> defending_squares_chess_logic_538, attacking_square_chess_logic_538, deck_chess_logic_538, king_chess_logic_538, nextPlayer_chess_logic_538, checkmate_chess_logic_538, get_movable_to_chess_logic_538 = match_record(_x28, "defending_squares", "attacking_square", "deck", "king", "nextPlayer", "checkmate", "get_movable_to")
let%mutable checkmate = checkmate_chess_logic_538 in ();
List.iter (fun defending_square ->
  if checkmate then
    (
      let%mutable test_deck = deck_chess_logic_538 in ();
      Map.set (Map.get test_deck attacking_square_chess_logic_538.i) attacking_square_chess_logic_538.j (Map.get (Map.get test_deck defending_square.i) defending_square.j);
      Map.set (Map.get test_deck defending_square.i) defending_square.j (int 0);
      let%mutable king = king_chess_logic_538 in ();
      if (Map.get (Map.get test_deck attacking_square_chess_logic_538.i) attacking_square_chess_logic_538.j) = ((int 6) * nextPlayer_chess_logic_538) then
        king <- {i = attacking_square_chess_logic_538.i; j = attacking_square_chess_logic_538.j};
      if (len ({deck = test_deck; i = king.i; j = king.j; pawn_capturing = true; player = (nextPlayer_chess_logic_538 * (int ((-1))))} get_movable_to_chess_logic_538)) = (nat 0) then
        checkmate <- false
    )
) defending_squares_chess_logic_538;
result checkmate)
                        ) (range (int 1) compute_chess_logic_245 (int 1))
                      )
            ) compute_chess_logic_192
      );
    if checkmate then
      data.status <- eif (board_state.nextPlayer = (int 1)) (variant finished "player_2_won") (variant finished "player_1_won")
    else
      failwith "NotCheckmate"

  let%entry_point claim_stalemate () =
    verify (sender = (Map.get data.players_map data.board_state.nextPlayer)) ~msg:"Wrong player";
    verify (is_variant "play" data.status);
    data.status <- variant claim_stalemate ();
    data.board_state.nextPlayer <- data.board_state.nextPlayer * (int ((-1)))

  let%entry_point giveup () =
    verify (not (is_variant "finished" data.status));
    if sender = (Map.get data.players_map (int ((-1)))) then
      data.status <- variant finished "player_1_won"
    else
      if sender = (Map.get data.players_map (int 1)) then
        data.status <- variant finished "player_2_won"
      else
        failwith "Wrong player"

  let%entry_point offer_draw () =
    verify (not (is_variant "finished" data.status)) ~msg:"Game finished";
    verify (contains sender data.players) ~msg:"Wrong player";
    Set.add data.draw_offer sender;
    if (len data.draw_offer) = (nat 2) then
      data.status <- variant finished "draw"

  let%entry_point play params =
    verify (sender = (Map.get data.players_map data.board_state.nextPlayer)) ~msg:"Wrong player";
    verify ((is_variant "play" data.status) || (is_variant "force_play" data.status));
    let%mutable board_state = data.board_state in ();
    let%mutable is_draw = false in ();
    set_type params {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}};
    set_type board_state {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes};
    verify ((params.f.i >= (int 0)) && (params.f.i < (int 8)));
    verify ((params.f.j >= (int 0)) && (params.f.j < (int 8)));
    verify ((params.t.i >= (int 0)) && (params.t.i < (int 8)));
    verify ((params.t.j >= (int 0)) && (params.t.j < (int 8)));
    verify (((params.t.j - params.f.j) <> (int 0)) || ((params.t.i - params.f.i) <> (int 0)));
    let%mutable compute_chess_logic_386 = board_state.nextPlayer in ();
    let%mutable deck = board_state.deck in ();
    verify ((sign (Map.get (Map.get deck params.f.i) params.f.j)) = compute_chess_logic_386);
    verify ((sign (Map.get (Map.get deck params.t.i) params.t.j)) <> compute_chess_logic_386);
    verify ((Map.get (Map.get deck params.t.i) params.t.j) <> ((int 6) * compute_chess_logic_386));
    let%mutable resetClock = false in ();
    let%mutable promotion = false in ();
    if (abs (Map.get (Map.get deck params.f.i) params.f.j)) = (nat 6) then
      (
        if ((abs (params.t.j - params.f.j)) = (nat 2)) && ((params.t.i - params.f.i) = (int 0)) then
          (
            verify ((((compute_chess_logic_386 = (int 1)) && (params.f.i = (int 0))) && (params.f.j = (int 4))) || (((compute_chess_logic_386 = (int ((-1)))) && (params.f.i = (int 7))) && (params.f.j = (int 4))));
            verify (Map.get (Map.get board_state.castle compute_chess_logic_386) (sign (params.t.j - params.f.j)));
            if (params.t.j - params.f.j) > (int 0) then
              Map.set (Map.get deck params.t.i) (int 7) (int 0)
            else
              Map.set (Map.get deck params.t.i) (int 0) (int 0);
            Map.set (Map.get deck params.t.i) (params.t.j - (sign (params.t.j - params.f.j))) ((int 2) * compute_chess_logic_386)
          )
        else
          verify (((abs (params.t.j - params.f.j)) <= (nat 1)) && ((abs (params.t.i - params.f.i)) <= (nat 1)));
        Map.set board_state.kings compute_chess_logic_386 {i = params.t.i; j = params.t.j};
        Map.set board_state.castle compute_chess_logic_386 (Map.make [(int ((-1)), false); (int 1, false)])
      );
    if (abs (Map.get (Map.get deck params.f.i) params.f.j)) = (nat 2) then
      (
        verify (((params.t.j - params.f.j) = (int 0)) || ((params.t.i - params.f.i) = (int 0)));
        List.iter (fun k ->
          verify ((Map.get (Map.get deck (params.f.i + (k * (sign (params.t.i - params.f.i))))) (params.f.j + (k * (sign (params.t.j - params.f.j))))) = (int 0))
        ) (range (int 1) ((to_int (max (abs (params.t.j - params.f.j)) (abs (params.t.i - params.f.i)))) - (int 1)) (int 1));
        if (((compute_chess_logic_386 = (int 1)) && (params.f.i = (int 0))) && (params.f.j = (int 0))) || (((compute_chess_logic_386 = (int ((-1)))) && (params.f.i = (int 7))) && (params.f.j = (int 0))) then
          Map.set (Map.get board_state.castle compute_chess_logic_386) (int ((-1))) false;
        if (((compute_chess_logic_386 = (int 1)) && (params.f.i = (int 0))) && (params.f.j = (int 7))) || (((compute_chess_logic_386 = (int ((-1)))) && (params.f.i = (int 7))) && (params.f.j = (int 7))) then
          Map.set (Map.get board_state.castle compute_chess_logic_386) (int 1) false
      );
    if (abs (Map.get (Map.get deck params.f.i) params.f.j)) = (nat 5) then
      (
        verify ((((params.t.j - params.f.j) = (int 0)) || ((params.t.i - params.f.i) = (int 0))) || ((abs (params.t.j - params.f.j)) = (abs (params.t.i - params.f.i))));
        List.iter (fun k ->
          verify ((Map.get (Map.get deck (params.f.i + (k * (sign (params.t.i - params.f.i))))) (params.f.j + (k * (sign (params.t.j - params.f.j))))) = (int 0))
        ) (range (int 1) ((to_int (max (abs (params.t.j - params.f.j)) (abs (params.t.i - params.f.i)))) - (int 1)) (int 1))
      );
    if (abs (Map.get (Map.get deck params.f.i) params.f.j)) = (nat 4) then
      (
        verify ((abs (params.t.j - params.f.j)) = (abs (params.t.i - params.f.i)));
        List.iter (fun k ->
          verify ((Map.get (Map.get deck (params.f.i + (k * (sign (params.t.i - params.f.i))))) (params.f.j + (k * (sign (params.t.j - params.f.j))))) = (int 0))
        ) (range (int 1) ((to_int (max (abs (params.t.j - params.f.j)) (abs (params.t.i - params.f.i)))) - (int 1)) (int 1))
      );
    if (abs (Map.get (Map.get deck params.f.i) params.f.j)) = (nat 3) then
      verify ((abs ((params.t.j - params.f.j) * (params.t.i - params.f.i))) = (nat 2));
    if (abs (Map.get (Map.get deck params.f.i) params.f.j)) = (nat 1) then
      (
        verify (((((((((abs (params.t.j - params.f.j)) = (nat 1)) && (((params.t.i - params.f.i) * compute_chess_logic_386) = (int 1))) && (is_some board_state.enPassant)) && (params.f.i = (open_some board_state.enPassant).i)) && (params.f.j = (open_some board_state.enPassant).j)) || ((((params.t.j - params.f.j) = (int 0)) && (((params.t.i - params.f.i) * compute_chess_logic_386) = (int 2))) && ((Map.get (Map.get deck (params.t.i - compute_chess_logic_386)) params.t.j) = (int 0)))) || (((params.t.j - params.f.j) = (int 0)) && (((params.t.i - params.f.i) * compute_chess_logic_386) = (int 1)))) || ((((abs (params.t.j - params.f.j)) = (nat 1)) && (((params.t.i - params.f.i) * compute_chess_logic_386) = (int 1))) && ((Map.get (Map.get deck params.t.i) params.t.j) <> compute_chess_logic_386)));
        if ((params.t.i - params.f.i) * compute_chess_logic_386) = (int 2) then
          board_state.enPassant <- some {i = (params.t.i - compute_chess_logic_386); j = params.t.j}
        else
          board_state.enPassant <- None;
        resetClock <- true;
        if (params.t.i = (int 7)) || (params.t.i = (int 0)) then
          promotion <- true
      )
    else
      board_state.enPassant <- None;
    let%mutable compute_chess_logic_406 = (Map.get board_state.kings compute_chess_logic_386) in ();
    if (Map.get (Map.get deck params.t.i) params.t.j) <> (int 0) then
      resetClock <- true;
    if promotion then
      (
        let%mutable compute_chess_logic_412 = (open_some params.promotion) in ();
        verify ((compute_chess_logic_412 > (nat 1)) && (compute_chess_logic_412 < (nat 6)));
        Map.set (Map.get deck params.t.i) params.t.j (mul compute_chess_logic_386 compute_chess_logic_412)
      )
    else
      (
        verify (is_variant "None" params.promotion);
        Map.set (Map.get deck params.t.i) params.t.j (Map.get (Map.get deck params.f.i) params.f.j)
      );
    Map.set (Map.get deck params.f.i) params.f.j (int 0);
    verify ((len ({deck = deck; i = compute_chess_logic_406.i; j = compute_chess_logic_406.j; pawn_capturing = true; player = (compute_chess_logic_386 * (int ((-1))))} (set_type_expr self.get_movable_to lambda {deck = map int (map int int); i = int; j = int; pawn_capturing = bool; player = int} (list {i = int; j = int})))) = (nat 0));
    Map.set board_state.pastMoves (board_state.fullMove, compute_chess_logic_386) (blake2b (pack (board_state.castle, deck, board_state.enPassant)));
    board_state.deck <- deck;
    if is_some params.claim_repeat then
      match_pair_chess_logic_428_fst, match_pair_chess_logic_428_snd = match_tuple(open_some params.claim_repeat, "match_pair_chess_logic_428_fst", "match_pair_chess_logic_428_snd")
      set_type (Set.make [match_pair_chess_logic_428_fst; match_pair_chess_logic_428_snd; board_state.fullMove]) (set nat);
      verify ((len (Set.make [match_pair_chess_logic_428_fst; match_pair_chess_logic_428_snd; board_state.fullMove])) = (nat 3));
      List.iter (fun fullMove ->
        verify ((blake2b (pack (board_state.castle, board_state.deck, board_state.enPassant))) = (Map.get board_state.pastMoves (fullMove, compute_chess_logic_386))) ~msg:("NotSameMove", {fullMove = fullMove})
      ) (Set.elements (Set.make [match_pair_chess_logic_428_fst; match_pair_chess_logic_428_snd; board_state.fullMove]));
      is_draw <- true;
    if (compute_chess_logic_386 * (int ((-1)))) = (int 1) then
      board_state.fullMove <- board_state.fullMove + (nat 1);
    if resetClock then
      board_state.halfMoveClock <- nat 0
    else
      board_state.halfMoveClock <- board_state.halfMoveClock + (nat 1);
    let%mutable compute_chess_logic_440 = (Map.get board_state.kings (compute_chess_logic_386 * (int ((-1))))) in ();
    if (len ({deck = deck; i = compute_chess_logic_440.i; j = compute_chess_logic_440.j; pawn_capturing = true; player = ((compute_chess_logic_386 * (int ((-1)))) * (int ((-1))))} (set_type_expr self.get_movable_to lambda {deck = map int (map int int); i = int; j = int; pawn_capturing = bool; player = int} (list {i = int; j = int})))) > (nat 0) then
      board_state.check <- true
    else
      board_state.check <- false;
    if board_state.halfMoveClock > (nat 49) then
      is_draw <- true;
    match_pair_chess_181_fst, match_pair_chess_181_snd = match_tuple((board_state, is_draw), "match_pair_chess_181_fst", "match_pair_chess_181_snd")
    let%mutable compute_chess_182 = match_pair_chess_181_fst in ();
    compute_chess_182.nextPlayer <- compute_chess_182.nextPlayer * (int ((-1)));
    data.board_state <- compute_chess_182;
    if match_pair_chess_181_snd then
      data.status <- variant finished "draw";
    data.draw_offer <- Set.make []

  let%entry_point threefold_repetition_claim params =
    verify (not (is_variant "finished" data.status)) ~msg:"Game finished";
    verify ((Map.get data.players_map data.board_state.nextPlayer) = sender) ~msg:"Wrong player";
    set_type (Set.make [params.fullMove1; params.fullMove2; eif (data.board_state.nextPlayer = (int 1)) (open_some (is_nat (data.board_state.fullMove - (nat 1)))) data.board_state.fullMove]) (set nat);
    verify ((len (Set.make [params.fullMove1; params.fullMove2; eif (data.board_state.nextPlayer = (int 1)) (open_some (is_nat (data.board_state.fullMove - (nat 1)))) data.board_state.fullMove])) = (nat 3));
    List.iter (fun fullMove ->
      verify ((blake2b (pack (data.board_state.castle, data.board_state.deck, data.board_state.enPassant))) = (Map.get data.board_state.pastMoves (fullMove, data.board_state.nextPlayer * (int ((-1)))))) ~msg:("NotSameMove", {fullMove = fullMove})
    ) (Set.elements (Set.make [params.fullMove1; params.fullMove2; eif (data.board_state.nextPlayer = (int 1)) (open_some (is_nat (data.board_state.fullMove - (nat 1)))) data.board_state.fullMove]));
    data.status <- variant finished "draw"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {board_state = {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes}; draw_offer = set address; metadata = big_map string bytes; players = set address; players_map = map int address; status = `claim_stalemate unit + `finished bounded(["draw", "player_1_won", "player_2_won"], t=string) + `force_play unit + `play unit}]
      ~storage:[%expr
                 {board_state = {castle = Map.make [(int ((-1)), Map.make [(int ((-1)), true); (int 1, true)]); (int 1, Map.make [(int ((-1)), true); (int 1, true)])]; check = false; deck = Map.make [(int 0, Map.make [(int 0, int 2); (int 1, int 3); (int 2, int 4); (int 3, int 5); (int 4, int 6); (int 5, int 4); (int 6, int 3); (int 7, int 2)]); (int 1, Map.make [(int 0, int 1); (int 1, int 1); (int 2, int 1); (int 3, int 1); (int 4, int 1); (int 5, int 1); (int 6, int 1); (int 7, int 1)]); (int 2, Map.make [(int 0, int 0); (int 1, int 0); (int 2, int 0); (int 3, int 0); (int 4, int 0); (int 5, int 0); (int 6, int 0); (int 7, int 0)]); (int 3, Map.make [(int 0, int 0); (int 1, int 0); (int 2, int 0); (int 3, int 0); (int 4, int 0); (int 5, int 0); (int 6, int 0); (int 7, int 0)]); (int 4, Map.make [(int 0, int 0); (int 1, int 0); (int 2, int 0); (int 3, int 0); (int 4, int 0); (int 5, int 0); (int 6, int 0); (int 7, int 0)]); (int 5, Map.make [(int 0, int 0); (int 1, int 0); (int 2, int 0); (int 3, int 0); (int 4, int 0); (int 5, int 0); (int 6, int 0); (int 7, int 0)]); (int 6, Map.make [(int 0, int ((-1))); (int 1, int ((-1))); (int 2, int ((-1))); (int 3, int ((-1))); (int 4, int ((-1))); (int 5, int ((-1))); (int 6, int ((-1))); (int 7, int ((-1)))]); (int 7, Map.make [(int 0, int ((-2))); (int 1, int ((-3))); (int 2, int ((-4))); (int 3, int ((-5))); (int 4, int ((-6))); (int 5, int ((-4))); (int 6, int ((-3))); (int 7, int ((-2)))])]; enPassant = None; fullMove = nat 1; halfMoveClock = nat 0; kings = Map.make [(int ((-1)), {i = int 7; j = int 4}); (int 1, {i = int 0; j = int 4})]; nextPlayer = int 1; pastMoves = Map.make [((nat 0, int ((-1))), bytes "0xbaf82e0f3ca2b90e482d6b6846873df9fee4e1baf369bc897bcef3b113a359df")]};
                  draw_offer = Set.make([]);
                  metadata = Map.make [];
                  players = Set.make([address "tz1RJHgDYgResSRsFSJxt9onzx1vSZMyYjZL"; address "tz1RcgwVfqGhaYFyB1vYTSsJy9hBdBXDq43b"]);
                  players_map = Map.make [(int ((-1)), address "tz1RJHgDYgResSRsFSJxt9onzx1vSZMyYjZL"); (int 1, address "tz1RcgwVfqGhaYFyB1vYTSsJy9hBdBXDq43b")];
                  status = play}]
      [answer_stalemate; claim_checkmate; claim_stalemate; giveup; offer_draw; play; threefold_repetition_claim]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())