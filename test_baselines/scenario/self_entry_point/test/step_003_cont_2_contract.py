import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TString).layout("x"))
    self.init(x = 'abc')

sp.add_compilation_target("test", Contract())