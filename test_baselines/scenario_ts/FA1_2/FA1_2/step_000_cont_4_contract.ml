open SmartML

module Contract = struct
  let%entry_point approve params =
    set_type params {spender = address; value = nat};
    verify (not data.config.paused) ~msg:"FA1.2_Paused";
    if not (contains sender data.balances) then
      (
        let%mutable value = {approvals = (Map.make []); balance = (nat 0)} in ();
        Map.set data.balances sender value
      );
    verify (((Map.get ~default_value:(nat 0) (Map.get data.balances sender).approvals params.spender) = (nat 0)) || (params.value = (nat 0))) ~msg:"FA1.2_UnsafeAllowanceChange";
    Map.set (Map.get data.balances sender).approvals params.spender params.value

  let%entry_point burn params =
    set_type params {address = address; value = nat};
    verify (sender = data.config.admin) ~msg:"FA1.2_NotAdmin";
    verify ((Map.get data.balances params.address).balance >= params.value) ~msg:"FA1.2_InsufficientBalance";
    (Map.get data.balances params.address).balance <- open_some (is_nat ((Map.get data.balances params.address).balance - params.value));
    data.totalSupply <- open_some (is_nat (data.totalSupply - params.value))

  let%entry_point mint params =
    set_type params {address = address; value = nat};
    verify (sender = data.config.admin) ~msg:"FA1.2_NotAdmin";
    if not (contains params.address data.balances) then
      (
        let%mutable value = {approvals = (Map.make []); balance = (nat 0)} in ();
        Map.set data.balances params.address value
      );
    (Map.get data.balances params.address).balance <- (Map.get data.balances params.address).balance + params.value;
    data.totalSupply <- data.totalSupply + params.value

  let%entry_point setAdministrator params =
    set_type params address;
    verify (sender = data.config.admin) ~msg:"FA1.2_NotAdmin";
    data.config.admin <- params

  let%entry_point setPause params =
    set_type params bool;
    verify (sender = data.config.admin) ~msg:"FA1.2_NotAdmin";
    data.config.paused <- params

  let%entry_point transfer params =
    set_type params {from = address; to = address; value = nat};
    verify (not data.config.paused) ~msg:"FA1.2_Paused";
    verify ((params.from = sender) || ((Map.get (Map.get data.balances params.from).approvals sender) >= params.value)) ~msg:"FA1.2_NotAllowed";
    if not (contains params.from data.balances) then
      (
        let%mutable value = {approvals = (Map.make []); balance = (nat 0)} in ();
        Map.set data.balances params.from value
      );
    if not (contains params.to data.balances) then
      (
        let%mutable value = {approvals = (Map.make []); balance = (nat 0)} in ();
        Map.set data.balances params.to value
      );
    verify ((Map.get data.balances params.from).balance >= params.value) ~msg:"FA1.2_InsufficientBalance";
    (Map.get data.balances params.from).balance <- open_some (is_nat ((Map.get data.balances params.from).balance - params.value));
    (Map.get data.balances params.to).balance <- (Map.get data.balances params.to).balance + params.value;
    if params.from <> sender then
      (
        let%mutable approval = (open_some (is_nat ((Map.get (Map.get data.balances params.from).approvals sender) - params.value))) in ();
        Map.set (Map.get data.balances params.from).approvals sender approval
      )

  let%entry_point updateMetadata params =
    set_type params (big_map string bytes);
    verify (sender = data.config.admin) ~msg:"FA1.2_NotAdmin";
    data.metadata <- params

  let%entry_point getAdministrator params =
    set_type params (pair unit (contract address));
    transfer data.config.admin (tez 0) (snd params)

  let%entry_point getAllowance params =
    set_type params (pair {owner = address; spender = address} (contract nat));
    let%mutable allowance = (nat 0) in ();
    if contains (fst params).owner data.balances then
      allowance <- Map.get ~default_value:(nat 0) (Map.get data.balances (fst params).owner).approvals (fst params).spender;
    transfer allowance (tez 0) (snd params)

  let%entry_point getBalance params =
    set_type params (pair address (contract nat));
    let%mutable balance = (nat 0) in ();
    if contains (fst params) data.balances then
      balance <- (Map.get data.balances (fst params)).balance;
    transfer balance (tez 0) (snd params)

  let%entry_point getTotalSupply params =
    set_type params (pair unit (contract nat));
    transfer data.totalSupply (tez 0) (snd params)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {balances = big_map address {approvals = map address nat; balance = nat}; config = {admin = address; paused = bool}; metadata = big_map string bytes; token_metadata = big_map nat {token_id = nat; token_info = map string bytes}; totalSupply = nat}]
      ~storage:[%expr
                 {balances = Map.make [];
                  config = {admin = address "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU"; paused = false};
                  metadata = Map.make [];
                  token_metadata = Map.make [];
                  totalSupply = nat 0}]
      [approve; burn; mint; setAdministrator; setPause; transfer; updateMetadata; getAdministrator; getAllowance; getBalance; getTotalSupply]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())