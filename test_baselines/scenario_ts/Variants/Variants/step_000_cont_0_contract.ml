open SmartML

module Contract = struct
  let%entry_point switchCase1 params =
    set_type params (`action1 string + `action2 nat);
    match params with
      | `action1 action1 ->
        verify (action1 = "A String") ~msg:"Expected value to be ('A String')."
      | `action2 action2 ->
        verify (action2 = (nat 10)) ~msg:"Expected value to be (10)."
;
    if is_variant "action2" params then
      data.value <- some (open_variant params action2)

  let%entry_point switchCase2 params =
    set_type params (`action1 string + `action2 nat);
    match params with
      | `action1 action1 ->
        verify (action1 = "A String") ~msg:"Expected value to be ('A String')."
      | `action2 action2 ->
        verify (action2 = (nat 10)) ~msg:"Expected value to be (10)."
;
    if is_variant "action2" params then
      data.value <- some (open_variant params action2)

  let%entry_point switchCase3 params =
    set_type params (`action1 string + `action2 nat);
    match params with
      | `action1 action1 ->
        verify (action1 = "A String") ~msg:"Expected value to be ('A String')."
      | `action2 action2 ->
        verify (action2 = (nat 10)) ~msg:"Expected value to be (10)."
;
    if is_variant "action2" params then
      data.value <- some (open_variant params action2)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = option nat}]
      ~storage:[%expr
                 {value = None}]
      [switchCase1; switchCase2; switchCase3]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())