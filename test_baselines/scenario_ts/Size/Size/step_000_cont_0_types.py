import smartpy as sp

tstorage = sp.TRecord(bytes = sp.TBytes, list = sp.TList(sp.TNat), map = sp.TMap(sp.TString, sp.TBytes), set = sp.TSet(sp.TString), string = sp.TString).layout(("bytes", ("list", ("map", ("set", "string")))))
tparameter = sp.TVariant(checkSizes = sp.TUnit).layout("checkSizes")
tprivates = { }
tviews = { }
