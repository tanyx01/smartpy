open SmartML

module Contract = struct
  let%entry_point checkSizes () =
    verify ((len data.string) = (nat 8)) ~msg:"String size is not 8.";
    verify ((len data.bytes) = (nat 3)) ~msg:"Bytes size is not 3.";
    verify ((len data.map) = (nat 1)) ~msg:"Map size is not 1.";
    verify ((len data.set) = (nat 2)) ~msg:"Set size is not 2.";
    verify ((len data.list) = (nat 2)) ~msg:"List size is not 2."

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {bytes = bytes; list = list nat; map = map string bytes; set = set string; string = string}]
      ~storage:[%expr
                 {bytes = bytes "0x012345";
                  list = [nat 1; nat 2];
                  map = Map.make [("String", bytes "0x00")];
                  set = Set.make(["String"; "String2"]);
                  string = "A STRING"}]
      [checkSizes]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())