open SmartML

module Contract = struct
  let%entry_point pack params =
    set_type params {value = nat};
    data.packed <- some (pack params)

  let%entry_point unpack () =
    if is_some data.packed then
      data.unpacked <- unpack (open_some ~message:"this.storage.packed is None" data.packed) {value = nat}

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {packed = option bytes; unpacked = option {value = nat}}]
      ~storage:[%expr
                 {packed = None;
                  unpacked = None}]
      [pack; unpack]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())