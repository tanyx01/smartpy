open SmartML

module Contract = struct
  let%entry_point equal () =
    data.counter1 <- int 1

  let%entry_point plus () =
    data.counter1 <- (int 1) + (int 1);
    data.counter1 <- data.counter1 + (int 1);
    data.counter1 <- data.counter1 + (data.counter2 + (int 1))

  let%entry_point minus () =
    data.counter1 <- 1 - 1;
    data.counter1 <- data.counter1 - (int 1);
    data.counter1 <- data.counter1 - (data.counter2 - (int 1))

  let%entry_point mul () =
    data.counter1 <- (int 1) * (int 1);
    data.counter1 <- data.counter1 * (int 1);
    data.counter1 <- data.counter1 * (data.counter2 * (int 1))

  let%entry_point div () =
    data.counter3 <- (nat 1) / (nat 1)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {counter1 = int; counter2 = int; counter3 = nat}]
      ~storage:[%expr
                 {counter1 = int 0;
                  counter2 = int 0;
                  counter3 = nat 0}]
      [equal; plus; minus; mul; div]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())