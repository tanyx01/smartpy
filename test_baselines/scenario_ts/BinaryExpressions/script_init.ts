interface BinaryExpressionsStorage {
    counter1: TInt;
    counter2: TInt;
    counter3: TNat;
}

@Contract
export class BinaryExpressions {
    storage = {
        counter1: 0,
        counter2: 0,
        counter3: 0,
    } as BinaryExpressionsStorage;

    @EntryPoint
    equal() {
        this.storage.counter1 = 1;
    }

    @EntryPoint
    plus() {
        this.storage.counter1 = 1 + 1;
        this.storage.counter1 = this.storage.counter1 + 1;
        this.storage.counter1 += this.storage.counter2 + 1;
    }

    @EntryPoint
    minus() {
        this.storage.counter1 = 1 - 1;
        this.storage.counter1 = this.storage.counter1 - 1;
        this.storage.counter1 -= this.storage.counter2 - 1;
    }

    @EntryPoint
    mul() {
        this.storage.counter1 = 1 * 1;
        this.storage.counter1 = this.storage.counter1 * 1;
        this.storage.counter1 *= this.storage.counter2 * 1;
    }

    @EntryPoint
    div() {
        this.storage.counter3 = 1 / 1;
    }
}

Dev.test({ name: 'BinaryExpressions' }, () => {
    const c1 = Scenario.originate(new BinaryExpressions());
    Scenario.transfer(c1.equal());
    Scenario.transfer(c1.plus());
    Scenario.transfer(c1.minus());
    Scenario.transfer(c1.mul());
    Scenario.transfer(c1.div());
});

Dev.compileContract('compile_contract', new BinaryExpressions());
