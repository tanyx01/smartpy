open SmartML

module Contract = struct
  let%entry_point set params =
    set_type params nat;
    if not (is_some data.value) then
      data.value <- some params

  let%entry_point openAndAdd params =
    set_type params nat;
    let%mutable value = (open_some ~message:"Error: is None" data.value) in ();
    if value = (nat 10) then
      data.value <- some (value + params)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = option nat}]
      ~storage:[%expr
                 {value = None}]
      [set; openAndAdd]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())