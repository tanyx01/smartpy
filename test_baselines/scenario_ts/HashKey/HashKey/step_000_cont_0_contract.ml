open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params key;
    verify (key_hash "tz1Xv78KMT7cHyVDLi9RmQP1KuWULHDafZdK" = (hash_key params))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())