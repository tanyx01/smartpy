interface TCreatedStorage {
    value: TString;
}

@Contract
export class Created {
    storage: TCreatedStorage = {
        value: '',
    };

    @EntryPoint
    updateValue(value: TString): void {
        this.storage.value = value;
    }
}

interface TCreatorStorage {
    value: TOption<TAddress>;
}

@Contract
export class Creator {
    storage: TCreatorStorage = {
        value: Sp.none,
    };

    @EntryPoint
    createWithoutOrigination(value: TString): void {
        // Just generate the address (Don't emit the origination operation)
        const op = Sp.createContractOperation(Created, { value: value });
        this.storage.value = Sp.some(op.address);
    }
}

Dev.test({ name: 'Create Record' }, () => {
    const c1 = Scenario.originate(new Creator());
    Scenario.transfer(c1.createWithoutOrigination('Hello World'));
});

Dev.compileContract('create_record_compilation', new Creator());
