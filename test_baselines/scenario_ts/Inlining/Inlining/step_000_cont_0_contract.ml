open SmartML

module Contract = struct
  let%entry_point ep1 () =
    let%mutable i = (nat 0) in ();
    while i < (nat 10) do
      i <- i + (i + (nat 1));
      let%mutable ii = (nat 0) in ();
      while ii < (nat 10) do
        data.value <- data.value + ((i + ii) + (nat 1));
        ii <- ii + (nat 1)
      done;
      i <- i + (nat 1)
    done

  let%entry_point ep2 () =
    if (data.value + (nat 1)) > (nat 2) then
      data.value <- nat 0

  let%entry_point ep3 () =
    data.value <- fst (open_some ~message:"Failed to divide mutez" (ediv (mutez 10) (mutez 1)))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = nat}]
      ~storage:[%expr
                 {value = nat 1}]
      [ep1; ep2; ep3]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())