import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(value = sp.TString).layout("value"))
    self.init(value = 'Hello World')

  @sp.entry_point
  def updateValue(self, params):
    sp.set_type(params, sp.TString)
    self.data.value = params

sp.add_compilation_target("test", Contract())