import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(expr1 = sp.TBool, expr2 = sp.TBool).layout(("expr1", "expr2")))
    self.init(expr1 = True,
              expr2 = False)

  @sp.entry_point
  def ep(self):
    expr1 = sp.local("expr1", self.data.expr1)
    expr2 = sp.local("expr2", self.data.expr2)
    sp.verify(expr2.value == expr2.value, 'expr1 && expr2')
    sp.verify(expr1.value & expr1.value, 'expr1 && expr2')
    sp.verify(expr1.value | expr2.value, 'expr1 || expr2')
    sp.verify(expr1.value != expr2.value, 'expr1 !== expr2')
    self.data.expr1 = expr1.value & expr2.value

sp.add_compilation_target("test", Contract())