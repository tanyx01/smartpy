open SmartML

module Contract = struct
  let%entry_point ep () =
    let%mutable expr1 = data.expr1 in ();
    let%mutable expr2 = data.expr2 in ();
    verify (expr2 = expr2) ~msg:"expr1 && expr2";
    verify (expr1 && expr1) ~msg:"expr1 && expr2";
    verify (expr1 || expr2) ~msg:"expr1 || expr2";
    verify (expr1 <> expr2) ~msg:"expr1 !== expr2";
    data.expr1 <- expr1 && expr2

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {expr1 = bool; expr2 = bool}]
      ~storage:[%expr
                 {expr1 = true;
                  expr2 = false}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())