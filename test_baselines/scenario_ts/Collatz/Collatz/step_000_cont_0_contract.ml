open SmartML

module Contract = struct
  let%entry_point run params =
    set_type params nat;
    if params > (nat 1) then
      (
        data.counter <- data.counter + (nat 1);
        let%mutable params = {k = (self_entry_point "run"); param = params} in ();
        if (params % (nat 2)) = (nat 0) then
          transfer params (tez 0) (open_some (contract {k = contract nat; param = nat} data.onEven ))
        else
          transfer params (tez 0) (open_some (contract {k = contract nat; param = nat} data.onOdd ))
      )

  let%entry_point reset () =
    data.counter <- nat 0

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {counter = nat; onEven = address; onOdd = address}]
      ~storage:[%expr
                 {counter = nat 0;
                  onEven = address "KT1QEaMVhcGvnf31cmWN4YWcujfzwhEQqX8c";
                  onOdd = address "KT1CbRd63JiSrZsVEJzmXAmA5ACNvpqYTGbM"}]
      [run; reset]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())