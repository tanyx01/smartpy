open SmartML

module Contract = struct
  let%entry_point ep1 () =
    verify (not data.paused)

  let%entry_point ep2 params =
    set_type params nat;
    data.value <- some params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {paused = bool; value = option nat}]
      ~storage:[%expr
                 {paused = false;
                  value = None}]
      [ep1; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())