import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(paused = sp.TBool, value = sp.TOption(sp.TNat)).layout(("paused", "value")))
    self.init(paused = False,
              value = sp.none)

  @sp.entry_point
  def ep1(self):
    sp.verify(~ self.data.paused)

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TNat)
    self.data.value = sp.some(params)

sp.add_compilation_target("test", Contract())