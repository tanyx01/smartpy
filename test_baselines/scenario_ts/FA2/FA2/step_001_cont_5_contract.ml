open SmartML

module Contract = struct
  let%entry_point request_balance params =
    set_type params address;
    let%mutable request = {requests = [{owner = self_address; token_id = (nat 0)}]; callback = (self_entry_point "receive_balance")} in ();
    let%mutable contact = (open_some ~message:"Invalid Interface" (contract {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}} params , entry_point='balance_of')) in ();
    transfer request (tez 0) contact

  let%entry_point receive_balance params =
    set_type params (list {balance = nat; request = {owner = address; token_id = nat}});
    List.iter (fun response ->
      if response.request.owner = self_address then
        data <- response.balance
    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ nat]
      ~storage:[%expr nat 0]
      [request_balance; receive_balance]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())