Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair { PUSH nat 1; SWAP; SUB } (Pair { PUSH nat 1; ADD } (Pair 1 { DUP; PUSH nat 5; COMPARE; LT; IF { DROP; PUSH nat 1 } {} })))
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_000_cont_0_storage.tz 1
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_000_cont_0_storage.json 25
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_000_cont_0_storage.py 1
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_000_cont_0_types.py 7
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_000_cont_0_contract.tz 224
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_000_cont_0_contract.json 249
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_000_cont_0_contract.py 56
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_000_cont_0_contract.ml 45
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_001_cont_0_params.py 1
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_001_cont_0_params.tz 1
 => test_baselines/scenario_ts/Lambdas/Lambdas/step_001_cont_0_params.json 1
Executing store(sp.record(transformer2 = sp.build_lambda(lambda v: v + 1), value = 1))...
 -> (Pair { PUSH nat 1; SWAP; SUB } (Pair { PUSH nat 1; ADD } (Pair 15 { DUP; PUSH nat 5; COMPARE; LT; IF { DROP; PUSH nat 1 } {} })))
  + Transfer
     params: sp.unit
     amount: sp.tez(0)
     to:     sp.contract(sp.TUnit, sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.unit)...
 -> (Pair { PUSH nat 1; SWAP; SUB } (Pair { PUSH nat 1; ADD } (Pair 10 { DUP; PUSH nat 5; COMPARE; LT; IF { DROP; PUSH nat 1 } {} })))
