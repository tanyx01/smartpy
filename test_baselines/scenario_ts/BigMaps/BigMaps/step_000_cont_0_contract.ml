open SmartML

module Contract = struct
  let%entry_point set params =
    set_type params {key = string; value = nat};
    Map.set data.map params.key params.value

  let%entry_point remove params =
    set_type params string;
    let%mutable value = (Map.get data.map params) in ();
    if (contains params data.map) && (value = (nat 1)) then
      Map.delete data.map params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {map = big_map string nat}]
      ~storage:[%expr
                 {map = Map.make []}]
      [set; remove]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())