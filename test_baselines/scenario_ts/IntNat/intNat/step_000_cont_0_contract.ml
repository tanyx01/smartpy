open SmartML

module Contract = struct
  let%entry_point ep () =
    data.int <- to_int data.nat;
    data.nat <- open_some (is_nat data.int);
    data.nat <- abs data.int

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {int = int; nat = nat}]
      ~storage:[%expr
                 {int = int 0;
                  nat = nat 0}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())