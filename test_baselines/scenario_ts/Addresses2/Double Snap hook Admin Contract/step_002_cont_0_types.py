import smartpy as sp

tstorage = sp.TRecord(admins = sp.TSet(sp.TAddress)).layout("admins")
tparameter = sp.TVariant(changeAdmins = sp.TRecord(add = sp.TOption(sp.TSet(sp.TAddress)), remove = sp.TOption(sp.TSet(sp.TAddress))).layout(("add", "remove"))).layout("changeAdmins")
tprivates = { }
tviews = { }
