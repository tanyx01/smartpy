Comment...
 h1: Double Snap hook Admin contract
Comment...
 h2: Originating contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> {"tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm"}
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_002_cont_0_storage.json 1
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_002_cont_0_storage.py 1
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_002_cont_0_types.py 7
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_002_cont_0_contract.tz 87
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_002_cont_0_contract.json 90
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_002_cont_0_contract.py 21
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_002_cont_0_contract.ml 30
Verifying sp.contract_data(0).admins.contains(sp.address('tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm'))...
 OK
Comment...
 h2: Add new admin
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_005_cont_0_params.py 1
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_005_cont_0_params.tz 1
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_005_cont_0_params.json 1
Executing changeAdmins(sp.record(add = sp.some(sp.set([sp.address('tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU')])), remove = sp.none))...
 -> {"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU"; "tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm"}
Verifying sp.pack(sp.contract_data(0).admins) == sp.pack(sp.set([sp.address('tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm'), sp.address('tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU')]))...
 OK
Comment...
 h2: Remove old admin
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_008_cont_0_params.py 1
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_008_cont_0_params.tz 1
 => test_baselines/scenario_ts/Addresses2/Double Snap hook Admin Contract/step_008_cont_0_params.json 1
Executing changeAdmins(sp.record(add = sp.none, remove = sp.some(sp.set([sp.address('tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm')]))))...
 -> {"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU"}
Verifying sp.pack(sp.contract_data(0).admins) == sp.pack(sp.set([sp.address('tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU')]))...
 OK
