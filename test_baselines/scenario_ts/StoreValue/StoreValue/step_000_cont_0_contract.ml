open SmartML

module Contract = struct
  let%entry_point replace params =
    set_type params {value = nat};
    data.storedValue <- params.value

  let%entry_point double () =
    data.storedValue <- data.storedValue * (nat 2)

  let%entry_point divide params =
    set_type params {divisor = nat};
    data.storedValue <- data.storedValue / params.divisor

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {storedValue = nat}]
      ~storage:[%expr
                 {storedValue = nat 1}]
      [replace; double; divide]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())