open SmartML

module Contract = struct
  let%entry_point check () =
    verify (data.storedValue = (nat 1))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {storedValue = nat}]
      ~storage:[%expr
                 {storedValue = nat 1}]
      [check]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())