open SmartML

module Contract = struct
  let%entry_point checkSignature params =
    set_type params signature;
    verify (check_signature data.publicKey params bytes "0x00") ~msg:"Signature doesn't match."

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {publicKey = key}]
      ~storage:[%expr
                 {publicKey = key "edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT"}]
      [checkSignature]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())