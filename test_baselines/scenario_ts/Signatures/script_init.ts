interface TStorage {
    publicKey: TKey;
}

@Contract
export class Signatures {
    constructor(public storage: TStorage) {}

    @EntryPoint
    checkSignature(signature: TSignature): void {
        Sp.verify(Sp.checkSignature(this.storage.publicKey, signature, '0x00'), "Signature doesn't match.");
    }
}

Dev.test({ name: 'Signatures' }, () => {
    // Create testing implicit account
    const bob = Scenario.testAccount('Bob');

    const signature: TSignature = Scenario.makeSignature(bob.secretKey, '0x00');

    // Originate contract
    const c1 = Scenario.originate(new Signatures({ publicKey: bob.publicKey }));

    Scenario.transfer(c1.checkSignature(signature));
});

Dev.compileContract('minimal', new Signatures({ publicKey: 'edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT' }));
