open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params nat;
    data.value <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = nat}]
      ~storage:[%expr
                 {value = nat 1}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())