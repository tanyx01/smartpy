interface TStorage {
    map: TMap<TString, TNat>;
    keys: TList<TString>;
    values: TList<TNat>;
    entries: TList<{ key: TString; value: TNat }>;
    value: TNat;
}

@Contract
export class Maps {
    storage: TStorage = {
        map: [],
        keys: [],
        values: [],
        entries: [],
        value: 0,
    };

    @EntryPoint
    set(param: { key: TString; value: TNat }): void {
        this.storage.map.set(param.key, param.value);
        this.storage.keys = this.storage.map.keys();
        this.storage.values = this.storage.map.values();
        this.storage.entries = this.storage.map.entries();
        for (const x of this.storage.entries) {
            this.storage.value += x.value;
        }
    }

    @EntryPoint
    remove(key: TString): void {
        const value = this.storage.map.get(key);
        if (this.storage.map.hasKey(key) && value === 1 && this.storage.map.size() === 3) {
            this.storage.map.remove(key);
        }
    }
}

Dev.test({ name: 'Maps' }, () => {
    const c1 = Scenario.originate(new Maps());
    Scenario.transfer(c1.set({ key: 'key1', value: 0 }));
    Scenario.transfer(c1.set({ key: 'key2', value: 1 }));
    Scenario.transfer(c1.set({ key: 'key3', value: 2 }));

    Scenario.transfer(c1.remove('key1'));
});

Dev.compileContract('Maps_compiled', new Maps());
