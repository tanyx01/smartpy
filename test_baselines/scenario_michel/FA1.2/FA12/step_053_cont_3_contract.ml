open SmartML

module Contract = struct
  let%entry_point target params =
    data.last <- some params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {last = option address}]
      ~storage:[%expr
                 {last = None}]
      [target]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())