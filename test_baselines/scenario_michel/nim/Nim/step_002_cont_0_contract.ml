open SmartML

module Contract = struct
  let%entry_point claim params =
    verify ((sum (Map.values data.deck)) = (int 0));
    verify (not data.claimed);
    data.claimed <- true;
    data.winner <- data.nextPlayer;
    verify (params.winner = data.winner)

  let%entry_point remove params =
    verify (params.cell >= 0);
    verify (params.cell < data.size);
    verify (params.k >= (int 1));
    verify (params.k <= (int 2));
    verify (params.k <= (Map.get data.deck params.cell));
    Map.set data.deck params.cell ((Map.get data.deck params.cell) - params.k);
    data.nextPlayer <- (int 3) - data.nextPlayer

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {claimed = bool; deck = map intOrNat int; nextPlayer = int; size = intOrNat; winner = int}]
      ~storage:[%expr
                 {claimed = false;
                  deck = Map.make [(0, int 1); (1, int 2); (2, int 3); (3, int 4); (4, int 5)];
                  nextPlayer = int 1;
                  size = 5;
                  winner = int 0}]
      [claim; remove]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())