open SmartML

module Contract = struct
  let%entry_point ep params =
    if params = true then
      send address "tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4" (tez 2)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())