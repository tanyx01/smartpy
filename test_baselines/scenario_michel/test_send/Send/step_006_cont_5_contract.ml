open SmartML

module Contract = struct
  let%entry_point ep () =
    transfer (int ((-42))) (tez 0) (open_some (contract int address "tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr" , entry_point='myEntryPoint'))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())