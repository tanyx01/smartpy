open SmartML

module Contract = struct
  let%entry_point deposit () =
    ()

  let%entry_point withdraw params =
    send params.address params.quantity

  let%entry_point withdraw_all params =
    send params sp_balance

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [deposit; withdraw; withdraw_all]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())