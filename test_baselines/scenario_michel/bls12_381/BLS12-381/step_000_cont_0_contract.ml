open SmartML

module Contract = struct
  let%entry_point add params =
    data.g1 <- data.g1 + params.g1;
    data.g2 <- data.g2 + params.g2;
    data.fr <- data.fr + params.fr

  let%entry_point conv params =
    data.fr <- mul (fst (open_some (ediv params (mutez 1)))) bls12_381_fr('0x01')

  let%entry_point mul params =
    data.g1 <- mul data.g1 params.fr;
    data.g2 <- mul data.g2 params.fr;
    data.fr <- mul data.fr params.fr;
    data.fr <- mul data.fr params.i;
    data.fr <- mul data.fr params.n

  let%entry_point negate () =
    data.g1 <- - data.g1;
    data.g2 <- - data.g2;
    data.fr <- - data.fr

  let%entry_point pairing_check params =
    data.checkResult <- some (pairing_check params)

  let%entry_point toInt () =
    data.toIntResult <- some (to_int data.fr)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {checkResult = option bool; fr = bls12_381_fr; g1 = bls12_381_g1; g2 = bls12_381_g2; toIntResult = option int}]
      ~storage:[%expr
                 {checkResult = None;
                  fr = bls12_381_fr('0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221');
                  g1 = bls12_381_g1 "0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1";
                  g2 = bls12_381_g2 "0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801";
                  toIntResult = None}]
      [add; conv; mul; negate; pairing_check; toInt]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())