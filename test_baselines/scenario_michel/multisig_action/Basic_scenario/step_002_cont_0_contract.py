import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(inactiveBefore = sp.TNat, nextId = sp.TNat, proposals = sp.TBigMap(sp.TNat, sp.TList(sp.TRecord(actions = sp.TList(sp.TBytes), target = sp.TAddress).layout(("actions", "target")))), quorum = sp.TNat, signers = sp.TSet(sp.TAddress), votes = sp.TBigMap(sp.TNat, sp.TSet(sp.TAddress))).layout(("inactiveBefore", ("nextId", ("proposals", ("quorum", ("signers", "votes")))))))
    self.init(inactiveBefore = 0,
              nextId = 0,
              proposals = {},
              quorum = 2,
              signers = sp.set([sp.address('tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT'), sp.address('tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR')]),
              votes = {})

  @sp.entry_point
  def administrate(self, params):
    sp.verify(sp.sender == sp.self_address, 'This entrypoint must be called through the proposal system.')
    sp.for action in params:
      with sp.unpack(action, sp.TVariant(addSigners = sp.TList(sp.TAddress), changeQuorum = sp.TNat, removeSigners = sp.TList(sp.TAddress)).layout(("addSigners", ("changeQuorum", "removeSigners")))).open_some(message = 'Bad actions format').match_cases() as arg:
        with arg.match('changeQuorum') as changeQuorum:
          self.data.quorum = changeQuorum
        with arg.match('addSigners') as addSigners:
          sp.for signer in addSigners:
            self.data.signers.add(signer)
        with arg.match('removeSigners') as removeSigners:
          sp.for address in removeSigners:
            self.data.signers.remove(address)

      sp.verify(self.data.quorum <= sp.len(self.data.signers), 'More quorum than signers.')

  @sp.entry_point
  def send_proposal(self, params):
    sp.verify(self.data.signers.contains(sp.sender), 'Only signers can propose')
    self.data.proposals[self.data.nextId] = params
    self.data.votes[self.data.nextId] = sp.set([])
    self.data.nextId += 1

  @sp.entry_point
  def vote(self, params):
    sp.verify(self.data.signers.contains(sp.sender), 'Only signers can vote')
    sp.verify(self.data.votes.contains(params), 'Proposal unknown')
    sp.verify(params >= self.data.inactiveBefore, 'The proposal is inactive')
    self.data.votes[params].add(sp.sender)
    sp.if sp.len(self.data.votes.get(params, default_value = sp.set([]))) >= self.data.quorum:
      sp.for p_item in self.data.proposals.get(params, default_value = sp.list([])):
        sp.transfer(p_item.actions, sp.tez(0), sp.contract(sp.TList(sp.TBytes), p_item.target).open_some(message = 'InvalidTarget'))
      self.data.inactiveBefore = self.data.nextId

sp.add_compilation_target("test", Contract())