import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(y = sp.TString).layout("y"))
    self.init(y = 'abc')

sp.add_compilation_target("test", Contract())