import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TInt).layout("x"))
    self.init(x = 0)

sp.add_compilation_target("test", Contract())