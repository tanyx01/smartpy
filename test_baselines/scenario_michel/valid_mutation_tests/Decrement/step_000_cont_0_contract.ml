open SmartML

module Contract = struct
  let%entry_point decrement () =
    data <- data - (int 1)

  let%entry_point increment () =
    data <- data + (int 1)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ int]
      ~storage:[%expr int 0]
      [decrement; increment]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())