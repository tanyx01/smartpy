open SmartML

module Contract = struct
  let%entry_point channelAccuseDoubleMove params =
    verify data.active;
    data.active <- false;
    verify (params.msg1.seq = params.msg2.seq);
    set_type params.msg1.seq int;
    verify ((pack params.msg1) <> (pack params.msg2));
    if params.party = 1 then
      (
        verify (check_signature data.party1.pk params.sig1 (pack {id = data.id; name = "state"; seq = params.msg1.seq; state = params.msg1.state}));
        verify (check_signature data.party1.pk params.sig2 (pack {id = data.id; name = "state"; seq = params.msg2.seq; state = params.msg2.state}));
        send data.party2.address (data.party1.bond + data.party2.bond)
      )
    else
      (
        verify (check_signature data.party2.pk params.sig1 (pack {id = data.id; name = "state"; seq = params.msg1.seq; state = params.msg1.state}));
        verify (check_signature data.party2.pk params.sig2 (pack {id = data.id; name = "state"; seq = params.msg2.seq; state = params.msg2.state}));
        send data.party1.address (data.party1.bond + data.party2.bond)
      );
    set_type data.baseState {claimed = bool; deck = map intOrNat int; nextPlayer = int; size = intOrNat; winner = int};
    set_type params.msg1.state {claimed = bool; deck = map intOrNat int; nextPlayer = int; size = intOrNat; winner = int};
    set_type params.msg2.state {claimed = bool; deck = map intOrNat int; nextPlayer = int; size = intOrNat; winner = int}

  let%entry_point channelNewState params =
    verify data.active;
    verify (data.seq < params.msg.seq);
    data.seq <- params.msg.seq;
    verify (check_signature data.party1.pk params.sig1 (pack {id = data.id; name = "state"; seq = params.msg.seq; state = params.msg.state}));
    verify (check_signature data.party2.pk params.sig2 (pack {id = data.id; name = "state"; seq = params.msg.seq; state = params.msg.state}));
    data.baseState <- params.msg.state

  let%entry_point channelRenounce params =
    verify data.active;
    data.active <- false;
    if params.party = 1 then
      (
        verify (check_signature data.party1.pk (open_some params.sig) (pack {id = data.id; name = "renounce"}));
        send data.party2.address ((data.party1.bond + data.party2.bond) - data.party1.looserClaim);
        send data.party1.address data.party1.looserClaim
      )
    else
      (
        send data.party1.address ((data.party1.bond + data.party2.bond) - data.party2.looserClaim);
        send data.party2.address data.party2.looserClaim
      )

  let%entry_point channelSetBond params =
    verify data.active;
    if params.party = 1 then
      (
        verify (not data.party1.hasBond);
        verify (data.party1.bond = amount);
        data.party1.hasBond <- true
      )
    else
      (
        verify (not data.party2.hasBond);
        verify (data.party2.bond = amount);
        data.party2.hasBond <- true
      )

  let%entry_point claim params =
    verify data.active;
    data.seq <- data.seq + 1;
    verify ((sum (Map.values data.baseState.deck)) = (int 0));
    verify (not data.baseState.claimed);
    data.baseState.claimed <- true;
    data.baseState.winner <- data.baseState.nextPlayer;
    if data.baseState.winner <> (int 0) then
      if data.baseState.winner = (int 1) then
        (
          data.active <- false;
          send data.party1.address ((data.party1.bond + data.party2.bond) - data.party2.looserClaim);
          send data.party2.address data.party2.looserClaim
        )
      else
        (
          data.active <- false;
          send data.party2.address ((data.party1.bond + data.party2.bond) - data.party1.looserClaim);
          send data.party1.address data.party1.looserClaim
        );
    verify (params.sub.winner = data.baseState.winner);
    if params.party = 1 then
      verify (check_signature data.party1.pk (open_some params.sig) (pack {id = data.id; name = "state"; seq = data.seq; state = data.baseState}));
    data.nextParty <- (int 3) - data.nextParty

  let%entry_point remove params =
    verify data.active;
    verify (data.nextParty = params.party);
    data.seq <- data.seq + 1;
    verify (params.sub.cell >= 0);
    verify (params.sub.cell < data.baseState.size);
    verify (params.sub.k >= (int 1));
    verify (params.sub.k <= (int 2));
    verify (params.sub.k <= (Map.get data.baseState.deck params.sub.cell));
    Map.set data.baseState.deck params.sub.cell ((Map.get data.baseState.deck params.sub.cell) - params.sub.k);
    data.baseState.nextPlayer <- (int 3) - data.baseState.nextPlayer;
    if params.party = (int 1) then
      verify (check_signature data.party1.pk (open_some params.sig) (pack {id = data.id; name = "state"; seq = data.seq; state = data.baseState}));
    data.nextParty <- (int 3) - data.nextParty

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {active = bool; baseState = {claimed = bool; deck = map intOrNat int; nextPlayer = int; size = intOrNat; winner = int}; id = string; nextParty = int; party1 = {address = address; bond = mutez; hasBond = bool; looserClaim = mutez; pk = key}; party2 = {address = address; bond = mutez; hasBond = bool; looserClaim = mutez; pk = key}; seq = intOrNat}]
      ~storage:[%expr
                 {active = true;
                  baseState = {claimed = false; deck = Map.make [(0, int 1); (1, int 2); (2, int 3); (3, int 4); (4, int 5)]; nextPlayer = int 1; size = 5; winner = int 0};
                  id = "1234";
                  nextParty = int 1;
                  party1 = {address = address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; bond = tez 12; hasBond = false; looserClaim = tez 2; pk = key "edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG"};
                  party2 = {address = address "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C"; bond = tez 15; hasBond = false; looserClaim = tez 3; pk = key "edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT"};
                  seq = 0}]
      [channelAccuseDoubleMove; channelNewState; channelRenounce; channelSetBond; claim; remove]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())