import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(active = sp.TBool, baseState = sp.TRecord(claimed = sp.TBool, deck = sp.TMap(sp.TIntOrNat, sp.TInt), nextPlayer = sp.TInt, size = sp.TIntOrNat, winner = sp.TInt).layout(("claimed", ("deck", ("nextPlayer", ("size", "winner"))))), id = sp.TString, nextParty = sp.TInt, party1 = sp.TRecord(address = sp.TAddress, bond = sp.TMutez, hasBond = sp.TBool, looserClaim = sp.TMutez, pk = sp.TKey).layout(("address", ("bond", ("hasBond", ("looserClaim", "pk"))))), party2 = sp.TRecord(address = sp.TAddress, bond = sp.TMutez, hasBond = sp.TBool, looserClaim = sp.TMutez, pk = sp.TKey).layout(("address", ("bond", ("hasBond", ("looserClaim", "pk"))))), seq = sp.TIntOrNat).layout(("active", ("baseState", ("id", ("nextParty", ("party1", ("party2", "seq"))))))))
    self.init(active = True,
              baseState = sp.record(claimed = False, deck = {0 : 1, 1 : 2, 2 : 3, 3 : 4, 4 : 5}, nextPlayer = 1, size = 5, winner = 0),
              id = '1234',
              nextParty = 1,
              party1 = sp.record(address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), bond = sp.tez(12), hasBond = False, looserClaim = sp.tez(2), pk = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG')),
              party2 = sp.record(address = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), bond = sp.tez(15), hasBond = False, looserClaim = sp.tez(3), pk = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT')),
              seq = 0)

  @sp.entry_point
  def channelAccuseDoubleMove(self, params):
    sp.verify(self.data.active)
    self.data.active = False
    sp.verify(params.msg1.seq == params.msg2.seq)
    sp.set_type(params.msg1.seq, sp.TInt)
    sp.verify(sp.pack(params.msg1) != sp.pack(params.msg2))
    sp.if params.party == 1:
      sp.verify(sp.check_signature(self.data.party1.pk, params.sig1, sp.pack(sp.record(id = self.data.id, name = 'state', seq = params.msg1.seq, state = params.msg1.state))))
      sp.verify(sp.check_signature(self.data.party1.pk, params.sig2, sp.pack(sp.record(id = self.data.id, name = 'state', seq = params.msg2.seq, state = params.msg2.state))))
      sp.send(self.data.party2.address, self.data.party1.bond + self.data.party2.bond)
    sp.else:
      sp.verify(sp.check_signature(self.data.party2.pk, params.sig1, sp.pack(sp.record(id = self.data.id, name = 'state', seq = params.msg1.seq, state = params.msg1.state))))
      sp.verify(sp.check_signature(self.data.party2.pk, params.sig2, sp.pack(sp.record(id = self.data.id, name = 'state', seq = params.msg2.seq, state = params.msg2.state))))
      sp.send(self.data.party1.address, self.data.party1.bond + self.data.party2.bond)
    sp.set_type(self.data.baseState, sp.TRecord(claimed = sp.TBool, deck = sp.TMap(sp.TIntOrNat, sp.TInt), nextPlayer = sp.TInt, size = sp.TIntOrNat, winner = sp.TInt).layout(("claimed", ("deck", ("nextPlayer", ("size", "winner"))))))
    sp.set_type(params.msg1.state, sp.TRecord(claimed = sp.TBool, deck = sp.TMap(sp.TIntOrNat, sp.TInt), nextPlayer = sp.TInt, size = sp.TIntOrNat, winner = sp.TInt).layout(("claimed", ("deck", ("nextPlayer", ("size", "winner"))))))
    sp.set_type(params.msg2.state, sp.TRecord(claimed = sp.TBool, deck = sp.TMap(sp.TIntOrNat, sp.TInt), nextPlayer = sp.TInt, size = sp.TIntOrNat, winner = sp.TInt).layout(("claimed", ("deck", ("nextPlayer", ("size", "winner"))))))

  @sp.entry_point
  def channelNewState(self, params):
    sp.verify(self.data.active)
    sp.verify(self.data.seq < params.msg.seq)
    self.data.seq = params.msg.seq
    sp.verify(sp.check_signature(self.data.party1.pk, params.sig1, sp.pack(sp.record(id = self.data.id, name = 'state', seq = params.msg.seq, state = params.msg.state))))
    sp.verify(sp.check_signature(self.data.party2.pk, params.sig2, sp.pack(sp.record(id = self.data.id, name = 'state', seq = params.msg.seq, state = params.msg.state))))
    self.data.baseState = params.msg.state

  @sp.entry_point
  def channelRenounce(self, params):
    sp.verify(self.data.active)
    self.data.active = False
    sp.if params.party == 1:
      sp.verify(sp.check_signature(self.data.party1.pk, params.sig.open_some(), sp.pack(sp.record(id = self.data.id, name = 'renounce'))))
      sp.send(self.data.party2.address, (self.data.party1.bond + self.data.party2.bond) - self.data.party1.looserClaim)
      sp.send(self.data.party1.address, self.data.party1.looserClaim)
    sp.else:
      sp.send(self.data.party1.address, (self.data.party1.bond + self.data.party2.bond) - self.data.party2.looserClaim)
      sp.send(self.data.party2.address, self.data.party2.looserClaim)

  @sp.entry_point
  def channelSetBond(self, params):
    sp.verify(self.data.active)
    sp.if params.party == 1:
      sp.verify(~ self.data.party1.hasBond)
      sp.verify(self.data.party1.bond == sp.amount)
      self.data.party1.hasBond = True
    sp.else:
      sp.verify(~ self.data.party2.hasBond)
      sp.verify(self.data.party2.bond == sp.amount)
      self.data.party2.hasBond = True

  @sp.entry_point
  def claim(self, params):
    sp.verify(self.data.active)
    self.data.seq += 1
    sp.verify(sp.sum(self.data.baseState.deck.values()) == 0)
    sp.verify(~ self.data.baseState.claimed)
    self.data.baseState.claimed = True
    self.data.baseState.winner = self.data.baseState.nextPlayer
    sp.if self.data.baseState.winner != 0:
      sp.if self.data.baseState.winner == 1:
        self.data.active = False
        sp.send(self.data.party1.address, (self.data.party1.bond + self.data.party2.bond) - self.data.party2.looserClaim)
        sp.send(self.data.party2.address, self.data.party2.looserClaim)
      sp.else:
        self.data.active = False
        sp.send(self.data.party2.address, (self.data.party1.bond + self.data.party2.bond) - self.data.party1.looserClaim)
        sp.send(self.data.party1.address, self.data.party1.looserClaim)
    sp.verify(params.sub.winner == self.data.baseState.winner)
    sp.if params.party == 1:
      sp.verify(sp.check_signature(self.data.party1.pk, params.sig.open_some(), sp.pack(sp.record(id = self.data.id, name = 'state', seq = self.data.seq, state = self.data.baseState))))
    self.data.nextParty = 3 - self.data.nextParty

  @sp.entry_point
  def remove(self, params):
    sp.verify(self.data.active)
    sp.verify(self.data.nextParty == params.party)
    self.data.seq += 1
    sp.verify(params.sub.cell >= 0)
    sp.verify(params.sub.cell < self.data.baseState.size)
    sp.verify(params.sub.k >= 1)
    sp.verify(params.sub.k <= 2)
    sp.verify(params.sub.k <= self.data.baseState.deck[params.sub.cell])
    self.data.baseState.deck[params.sub.cell] -= params.sub.k
    self.data.baseState.nextPlayer = 3 - self.data.baseState.nextPlayer
    sp.if params.party == 1:
      sp.verify(sp.check_signature(self.data.party1.pk, params.sig.open_some(), sp.pack(sp.record(id = self.data.id, name = 'state', seq = self.data.seq, state = self.data.baseState))))
    self.data.nextParty = 3 - self.data.nextParty

sp.add_compilation_target("test", Contract())