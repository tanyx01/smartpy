import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat).layout("x"))
    self.init(x = 12)

  @sp.entry_point
  def any_ep(self):
    pass

  @sp.entry_point
  def any_lazy_ep(self):
    pass

  @sp.entry_point
  def transfer_KO(self):
    pass

  @sp.entry_point
  def transfer_OK(self):
    pass

sp.add_compilation_target("test", Contract())