import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat).layout("x")
tparameter = sp.TVariant(any_ep = sp.TUnit, any_lazy_ep = sp.TUnit, transfer_KO = sp.TUnit, transfer_OK = sp.TUnit).layout((("any_ep", "any_lazy_ep"), ("transfer_KO", "transfer_OK")))
tprivates = { }
tviews = { }
