open SmartML

module Contract = struct
  let%entry_point breed params =
    verify (params.parent1 <> params.parent2);
    if false then
      (
        verify ((tez 0) < (Map.get data.kitties params.parent1).borrowPrice);
        verify ((Map.get data.kitties params.parent1).borrowPrice < params.borrowPrice);
        verify (amount = params.borrowPrice);
        send (Map.get data.kitties params.parent1).owner params.borrowPrice
      );
    verify ((Map.get data.kitties params.parent1).auction < now);
    verify ((Map.get data.kitties params.parent1).hatching < now);
    if (Map.get data.kitties params.parent2).owner <> sender then
      (
        verify ((tez 0) < (Map.get data.kitties params.parent2).borrowPrice);
        verify ((Map.get data.kitties params.parent2).borrowPrice < params.borrowPrice);
        verify (amount = params.borrowPrice);
        send (Map.get data.kitties params.parent2).owner params.borrowPrice
      );
    verify ((Map.get data.kitties params.parent2).auction < now);
    verify ((Map.get data.kitties params.parent2).hatching < now);
    (Map.get data.kitties params.parent1).hatching <- add_seconds now (int 100);
    (Map.get data.kitties params.parent2).hatching <- add_seconds now (int 100);
    Map.set data.kitties params.kittyId {auction = timestamp 0; borrowPrice = (tez 0); generation = (1 + (max (Map.get data.kitties params.parent1).generation (Map.get data.kitties params.parent2).generation)); hatching = (add_seconds now (int 100)); isNew = false; kittyId = params.kittyId; owner = sender; price = (tez 0)}

  let%entry_point build params =
    verify (data.creator = sender);
    verify params.kitty.isNew;
    set_type params.kitty.kittyId int;
    Map.set data.kitties params.kitty.kittyId params.kitty

  let%entry_point buy params =
    verify ((tez 0) < (Map.get data.kitties params.kittyId).price);
    verify ((Map.get data.kitties params.kittyId).price <= params.price);
    verify (amount = params.price);
    send (Map.get data.kitties params.kittyId).owner params.price;
    (Map.get data.kitties params.kittyId).owner <- sender;
    if (Map.get data.kitties params.kittyId).isNew then
      (
        (Map.get data.kitties params.kittyId).isNew <- false;
        (Map.get data.kitties params.kittyId).auction <- add_seconds now (int 10)
      );
    verify (now <= (Map.get data.kitties params.kittyId).auction);
    if now <= (Map.get data.kitties params.kittyId).auction then
      (Map.get data.kitties params.kittyId).price <- params.price + (mutez 1)

  let%entry_point lend params =
    verify ((tez 0) <= params.price);
    if false then
      (
        verify ((tez 0) < (Map.get data.kitties params.kittyId).borrowPrice);
        verify ((Map.get data.kitties params.kittyId).borrowPrice < params.borrowPrice);
        verify (amount = params.borrowPrice);
        send (Map.get data.kitties params.kittyId).owner params.borrowPrice
      );
    verify ((Map.get data.kitties params.kittyId).auction < now);
    verify ((Map.get data.kitties params.kittyId).hatching < now);
    (Map.get data.kitties params.kittyId).borrowPrice <- params.price

  let%entry_point sell params =
    verify ((tez 0) <= params.price);
    if false then
      (
        verify ((tez 0) < (Map.get data.kitties params.kittyId).borrowPrice);
        verify ((Map.get data.kitties params.kittyId).borrowPrice < params.borrowPrice);
        verify (amount = params.borrowPrice);
        send (Map.get data.kitties params.kittyId).owner params.borrowPrice
      );
    verify ((Map.get data.kitties params.kittyId).auction < now);
    verify ((Map.get data.kitties params.kittyId).hatching < now);
    (Map.get data.kitties params.kittyId).price <- params.price

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {creator = address; kitties = map int {auction = timestamp; borrowPrice = mutez; generation = intOrNat; hatching = timestamp; isNew = bool; kittyId = int; owner = address; price = mutez}}]
      ~storage:[%expr
                 {creator = address "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs";
                  kitties = Map.make []}]
      [breed; build; buy; lend; sell]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())