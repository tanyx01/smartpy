Comment...
 h1: Mini Kitties
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_pre_michelson.michel 693
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {})
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_storage.json 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_storage.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_types.py 7
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_contract.tz 797
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_contract.json 786
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_contract.py 75
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_001_cont_0_contract.ml 88
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_002_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_002_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_002_cont_0_params.json 31
Executing build(sp.record(kitty = sp.record(auction = sp.timestamp(0), borrowPrice = sp.tez(0), generation = 0, hatching = sp.timestamp(0), isNew = True, kittyId = 0, owner = sp.resolve(sp.test_account("Creator").address), price = sp.mutez(10))))...
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {Elt 0 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 0 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10)))))))})
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_003_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_003_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_003_cont_0_params.json 31
Executing build(sp.record(kitty = sp.record(auction = sp.timestamp(0), borrowPrice = sp.tez(0), generation = 0, hatching = sp.timestamp(0), isNew = True, kittyId = 1, owner = sp.resolve(sp.test_account("Creator").address), price = sp.mutez(10))))...
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {Elt 0 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 0 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 1 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 1 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10)))))))})
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_004_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_004_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_004_cont_0_params.json 31
Executing build(sp.record(kitty = sp.record(auction = sp.timestamp(0), borrowPrice = sp.tez(0), generation = 0, hatching = sp.timestamp(0), isNew = True, kittyId = 2, owner = sp.resolve(sp.test_account("Creator").address), price = sp.mutez(10))))...
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {Elt 0 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 0 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 1 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 1 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 2 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 2 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10)))))))})
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_005_cont_0_params.json 31
Executing build(sp.record(kitty = sp.record(auction = sp.timestamp(0), borrowPrice = sp.tez(0), generation = 0, hatching = sp.timestamp(0), isNew = True, kittyId = 3, owner = sp.resolve(sp.test_account("Creator").address), price = sp.mutez(10))))...
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {Elt 0 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 0 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 1 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 1 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 2 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 2 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 3 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 3 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10)))))))})
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_006_cont_0_params.json 1
Executing buy(sp.record(kittyId = 1, price = sp.mutez(10)))...
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {Elt 0 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 0 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 1 (Pair "1970-01-01T00:00:10Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair False (Pair 1 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 11))))))); Elt 2 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 2 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 3 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 3 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10)))))))})
  + Transfer
     params: sp.unit
     amount: sp.mutez(10)
     to:     sp.contract(sp.TUnit, sp.address('tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs')).open_some()
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_007_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_007_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_007_cont_0_params.json 1
Executing buy(sp.record(kittyId = 2, price = sp.mutez(10)))...
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {Elt 0 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 0 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 1 (Pair "1970-01-01T00:00:10Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair False (Pair 1 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 11))))))); Elt 2 (Pair "1970-01-01T00:00:10Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair False (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 11))))))); Elt 3 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 3 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10)))))))})
  + Transfer
     params: sp.unit
     amount: sp.mutez(10)
     to:     sp.contract(sp.TUnit, sp.address('tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs')).open_some()
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_008_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_008_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_008_cont_0_params.json 1
Executing buy(sp.record(kittyId = 1, price = sp.mutez(11)))...
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {Elt 0 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 0 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 1 (Pair "1970-01-01T00:00:10Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair False (Pair 1 (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 12))))))); Elt 2 (Pair "1970-01-01T00:00:10Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair False (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 11))))))); Elt 3 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 3 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10)))))))})
  + Transfer
     params: sp.unit
     amount: sp.mutez(11)
     to:     sp.contract(sp.TUnit, sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi')).open_some()
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_009_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_009_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_009_cont_0_params.json 1
Executing buy(sp.record(kittyId = 1, price = sp.mutez(15)))...
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {Elt 0 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 0 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 1 (Pair "1970-01-01T00:00:10Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair False (Pair 1 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 16))))))); Elt 2 (Pair "1970-01-01T00:00:10Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair False (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 11))))))); Elt 3 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 3 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10)))))))})
  + Transfer
     params: sp.unit
     amount: sp.mutez(15)
     to:     sp.contract(sp.TUnit, sp.address('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP')).open_some()
Comment...
 h2: A bad execution
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_011_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_011_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_011_cont_0_params.json 1
Executing buy(sp.record(kittyId = 1, price = sp.mutez(20)))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.now <= self.data.kitties[params.kittyId].auction : sp.TBool) (python/templates/minikitties.py, line 43)
 (python/templates/minikitties.py, line 43)
Comment...
 h2: Hatching
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_013_cont_0_params.py 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_013_cont_0_params.tz 1
 => test_baselines/scenario_michel/minikitties/MiniKitties/step_013_cont_0_params.json 1
Executing breed(sp.record(borrowPrice = sp.mutez(10), kittyId = 4, parent1 = 1, parent2 = 2))...
 -> (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" {Elt 0 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 0 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 1 (Pair "1970-01-01T00:00:10Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:01:55Z" (Pair False (Pair 1 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 16))))))); Elt 2 (Pair "1970-01-01T00:00:10Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:01:55Z" (Pair False (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 11))))))); Elt 3 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 0 (Pair "1970-01-01T00:00:00Z" (Pair True (Pair 3 (Pair "tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs" 10))))))); Elt 4 (Pair "1970-01-01T00:00:00Z" (Pair 0 (Pair 1 (Pair "1970-01-01T00:01:55Z" (Pair False (Pair 4 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0)))))))})
