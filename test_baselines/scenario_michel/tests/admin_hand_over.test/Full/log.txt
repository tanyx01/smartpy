Comment...
 h1: Full test
Table Of Contents

 Full test
# Origination
# add_admins
## (Failure) Non admin fails to a new admin.
## An admin adds a new admin.
# remove_admins
## (Failure) Non admin fails to remove the older admin.
## (Failure) Admin tries to remove themselves.
## The new admin removes the older admin.
# protected
## The new admin calls the protected entrypoint
## (Failure) Non admin calls the protected entrypoint
Comment...
 h2: Origination
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_pre_michelson.michel 83
 -> {"tz1NajGagueXKGFqf9msG66qsAreKnPiFuEb"}
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_storage.tz 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_storage.json 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_storage.py 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_types.py 7
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_contract.tz 91
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_contract.json 105
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_contract.py 25
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_003_cont_0_contract.ml 32
Comment...
 h2: add_admins
Comment...
 h3: (Failure) Non admin fails to a new admin.
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_006_cont_0_params.json 1
Executing add_admins(sp.list([sp.resolve(sp.test_account("new_admin").address)]))...
 -> --- Expected failure in transaction --- Wrong condition: (self.data.admins.contains(sp.sender) : sp.TBool) (templates/admin_hand_over.py, line 36)
Message: 'Only an admin can call this entrypoint.'
 (templates/admin_hand_over.py, line 35)
Comment...
 h3: An admin adds a new admin.
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_008_cont_0_params.py 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_008_cont_0_params.tz 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_008_cont_0_params.json 1
Executing add_admins(sp.list([sp.resolve(sp.test_account("new_admin").address)]))...
 -> {"tz1NajGagueXKGFqf9msG66qsAreKnPiFuEb"; "tz1ZEZr6xyjSiQFyCA7y3fVdz6fckLT9gJdG"}
Comment...
 h2: remove_admins
Comment...
 h3: (Failure) Non admin fails to remove the older admin.
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_011_cont_0_params.py 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_011_cont_0_params.tz 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_011_cont_0_params.json 1
Executing remove_admins(sp.list([sp.resolve(sp.test_account("first_admin").address)]))...
 -> --- Expected failure in transaction --- Wrong condition: (self.data.admins.contains(sp.sender) : sp.TBool) (templates/admin_hand_over.py, line 55)
Message: 'Only an admin can call this entrypoint.'
 (templates/admin_hand_over.py, line 54)
Comment...
 h3: (Failure) Admin tries to remove themselves.
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_013_cont_0_params.py 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_013_cont_0_params.tz 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_013_cont_0_params.json 1
Executing remove_admins(sp.list([sp.resolve(sp.test_account("first_admin").address)]))...
 -> --- Expected failure in transaction --- Wrong condition: (admin != sp.sender : sp.TBool) (templates/admin_hand_over.py, line 59)
Message: 'An admin cannot remove themselves.'
 (templates/admin_hand_over.py, line 59)
Comment...
 h3: The new admin removes the older admin.
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_015_cont_0_params.py 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_015_cont_0_params.tz 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_015_cont_0_params.json 1
Executing remove_admins(sp.list([sp.resolve(sp.test_account("first_admin").address)]))...
 -> {"tz1ZEZr6xyjSiQFyCA7y3fVdz6fckLT9gJdG"}
Verifying ~ (sp.contract_data(0).admins.contains(sp.resolve(sp.test_account("first_admin").address)))...
 OK
Comment...
 h2: protected
Comment...
 h3: The new admin calls the protected entrypoint
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_019_cont_0_params.py 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_019_cont_0_params.tz 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_019_cont_0_params.json 1
Executing protected(sp.record())...
 -> {"tz1ZEZr6xyjSiQFyCA7y3fVdz6fckLT9gJdG"}
Comment...
 h3: (Failure) Non admin calls the protected entrypoint
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_021_cont_0_params.py 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_021_cont_0_params.tz 1
 => test_baselines/scenario_michel/tests/admin_hand_over.test/Full/step_021_cont_0_params.json 1
Executing protected(sp.record())...
 -> --- Expected failure in transaction --- Wrong condition: (self.data.admins.contains(sp.sender) : sp.TBool) (templates/admin_hand_over.py, line 70)
Message: 'Only an admin can call this entrypoint.'
 (templates/admin_hand_over.py, line 69)
