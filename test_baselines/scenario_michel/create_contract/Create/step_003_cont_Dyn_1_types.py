import smartpy as sp

tstorage = sp.TRecord(a = sp.TInt, b = sp.TNat).layout(("a", "b"))
tparameter = sp.TVariant(myEntryPoint = sp.TRecord(x = sp.TInt, y = sp.TNat).layout(("x", "y"))).layout("myEntryPoint")
tprivates = { }
tviews = { }
