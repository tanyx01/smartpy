open SmartML

module Contract = struct
  let%entry_point f params =
    data.a <- 2 * params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = intOrNat}]
      [f]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())