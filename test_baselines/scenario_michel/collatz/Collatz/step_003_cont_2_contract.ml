open SmartML

module Contract = struct
  let%entry_point reset () =
    data.counter <- 0

  let%entry_point run params =
    if params > (nat 1) then
      (
        data.counter <- data.counter + 1;
        if (params % (nat 2)) = (nat 0) then
          transfer {k = (self_entry_point "run"); x = params} (tez 0) (open_some (contract {k = contract nat; x = nat} data.onEven ))
        else
          transfer {k = (self_entry_point "run"); x = params} (tez 0) (open_some (contract {k = contract nat; x = nat} data.onOdd ))
      )

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {counter = intOrNat; onEven = address; onOdd = address}]
      ~storage:[%expr
                 {counter = 0;
                  onEven = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  onOdd = address "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"}]
      [reset; run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())