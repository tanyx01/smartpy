open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params (list (`V1 {a = bool; b = string} + `V2 nat))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())