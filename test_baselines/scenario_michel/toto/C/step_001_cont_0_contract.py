import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep(self, params):
    sp.set_type(params, sp.TList(sp.TVariant(V1 = sp.TRecord(a = sp.TBool, b = sp.TString).layout(("b", "a")), V2 = sp.TNat).layout(("V2", "V1"))))

sp.add_compilation_target("test", Contract())