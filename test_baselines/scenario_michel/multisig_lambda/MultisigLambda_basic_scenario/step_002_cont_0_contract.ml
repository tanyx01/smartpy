open SmartML

module Contract = struct
  let%entry_point submit_lambda params =
    verify (contains sender data.members) ~msg:"You are not a member";
    Map.set data.lambdas data.nextId params;
    Map.set data.votes data.nextId (Set.make []);
    data.nextId <- data.nextId + (nat 1)

  let%entry_point vote_lambda params =
    verify (contains sender data.members) ~msg:"You are not a member";
    verify (params >= data.inactiveBefore) ~msg:"The lambda is inactive";
    verify (contains params data.lambdas) ~msg:"Lambda not found";
    Set.add (Map.get data.votes params) sender;
    if (len (Map.get data.votes params)) >= data.required_votes then
      (
        List.iter (fun op ->
          operations <- op :: operations
        ) (() (Map.get data.lambdas params));
        data.inactiveBefore <- data.nextId
      )

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {inactiveBefore = nat; lambdas = big_map nat lambda unit (list operation); members = set address; nextId = nat; required_votes = nat; votes = big_map nat (set address)}]
      ~storage:[%expr
                 {inactiveBefore = nat 0;
                  lambdas = Map.make [];
                  members = Set.make([address "tz1Qi4ShtraJF1xagU5kRUjU6xzXYS5ufnXm"; address "tz1RnNX4gzQTy6wDFHNGV7ewuXS2Po7cpC3W"; address "tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx"]);
                  nextId = nat 0;
                  required_votes = nat 2;
                  votes = Map.make []}]
      [submit_lambda; vote_lambda]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())