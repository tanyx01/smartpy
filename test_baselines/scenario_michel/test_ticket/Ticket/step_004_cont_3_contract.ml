open SmartML

module Contract = struct
  let%entry_point ep1 () =
    with match_record(data, "data") as data:
      match_pair_test_ticket_61_fst, match_pair_test_ticket_61_snd = match_tuple(get_and_update (int 42) None data.m, "match_pair_test_ticket_61_fst", "match_pair_test_ticket_61_snd")
      data.m <- match_pair_test_ticket_61_snd

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {m = map int (ticket int)}]
      [ep1]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())