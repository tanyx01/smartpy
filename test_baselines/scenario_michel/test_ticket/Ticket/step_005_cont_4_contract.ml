open SmartML

module Contract = struct
  let%entry_point ep1 () =
    with match_record(data, "data") as data:
      match_pair_test_ticket_71_fst, match_pair_test_ticket_71_snd = match_tuple(get_and_update (int 42) None data.m, "match_pair_test_ticket_71_fst", "match_pair_test_ticket_71_snd")
      data.m <- match_pair_test_ticket_71_snd;
      data.x <- int 0

  let%entry_point ep2 params =
    with match_record(data, "data") as data:
      let%mutable ticket_78 = (ticket "a" (nat 1)) in ();
      let%mutable ticket_79 = (ticket "b" (nat 2)) in ();
      transfer (ticket_78, ticket_79) (tez 0) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {m = map int (ticket int); x = int}]
      [ep1; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())