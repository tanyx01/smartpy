open SmartML

module Contract = struct
  let%entry_point auto_call () =
    let%mutable ticket_9 = (ticket (int 1) (nat 43)) in ();
    transfer ticket_9 (tez 0) (self_entry_point "run")

  let%entry_point run params =
    set_type params (ticket int);
    ticket_test_ticket_14_data, ticket_test_ticket_14_copy = match_tuple(read_ticket_raw params, "ticket_test_ticket_14_data", "ticket_test_ticket_14_copy")
    ticket_test_ticket_14_ticketer, ticket_test_ticket_14_content, ticket_test_ticket_14_amount = match_tuple(ticket_test_ticket_14_data, "ticket_test_ticket_14_ticketer", "ticket_test_ticket_14_content", "ticket_test_ticket_14_amount")
    let%mutable ticket_15 = (ticket "abc" (nat 42)) in ();
    data.y <- some ticket_15;
    ticket1_test_ticket_16, ticket2_test_ticket_16 = match_tuple(open_some (split_ticket_raw ticket_test_ticket_14_copy (ticket_test_ticket_14_amount / (nat 3), open_some (is_nat (ticket_test_ticket_14_amount - (ticket_test_ticket_14_amount / (nat 3)))))), "ticket1_test_ticket_16", "ticket2_test_ticket_16")
    data.x <- some (open_some (join_tickets_raw (ticket2_test_ticket_16, ticket1_test_ticket_16)))

  let%entry_point run2 params =
    set_type params {t = ticket int; x = int};
    x_test_ticket_22, t_test_ticket_22 = match_record(params, "x", "t")
    verify (x_test_ticket_22 = (int 42));
    ticket_test_ticket_24_data, ticket_test_ticket_24_copy = match_tuple(read_ticket_raw t_test_ticket_22, "ticket_test_ticket_24_data", "ticket_test_ticket_24_copy")
    ticket_test_ticket_24_ticketer, ticket_test_ticket_24_content, ticket_test_ticket_24_amount = match_tuple(ticket_test_ticket_24_data, "ticket_test_ticket_24_ticketer", "ticket_test_ticket_24_content", "ticket_test_ticket_24_amount")
    let%mutable ticket_25 = (ticket "abc" (nat 42)) in ();
    data.y <- some ticket_25;
    ticket1_test_ticket_26, ticket2_test_ticket_26 = match_tuple(open_some (split_ticket_raw ticket_test_ticket_24_copy (ticket_test_ticket_24_amount / (nat 3), open_some (is_nat (ticket_test_ticket_24_amount - (ticket_test_ticket_24_amount / (nat 3)))))), "ticket1_test_ticket_26", "ticket2_test_ticket_26")
    data.x <- some (open_some (join_tickets_raw (ticket2_test_ticket_26, ticket1_test_ticket_26)))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = option (ticket int); y = option (ticket string)}]
      ~storage:[%expr
                 {x = None;
                  y = None}]
      [auto_call; run; run2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())