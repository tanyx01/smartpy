open SmartML

module Contract = struct
  let%entry_point run () =
    with modify(data, "t") as "t":
      ticket_test_ticket_36_data, ticket_test_ticket_36_copy = match_tuple(read_ticket_raw t, "ticket_test_ticket_36_data", "ticket_test_ticket_36_copy")
      ticket_test_ticket_36_ticketer, ticket_test_ticket_36_content, ticket_test_ticket_36_amount = match_tuple(ticket_test_ticket_36_data, "ticket_test_ticket_36_ticketer", "ticket_test_ticket_36_content", "ticket_test_ticket_36_amount")
      verify (ticket_test_ticket_36_content = "abc");
      ticket1_test_ticket_38, ticket2_test_ticket_38 = match_tuple(open_some (split_ticket_raw ticket_test_ticket_36_copy (ticket_test_ticket_36_amount / (nat 2), ticket_test_ticket_36_amount / (nat 2))), "ticket1_test_ticket_38", "ticket2_test_ticket_38")
      result (open_some (join_tickets_raw (ticket2_test_ticket_38, ticket1_test_ticket_38)))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ ticket string]
      [run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())