open SmartML

module Contract = struct
  let%entry_point ep params =
    verify ((params <= address "KT1XvNYseNDJJ6Kw27qhSEDF8ys8JhDopzfG") && (params >= address "KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT")) ~msg:"Not KT1";
    data.x <- params < address "KT1XvNYseNDJJ6Kw27qhSEDF8ys8JhDopzfG";
    data.y <- params >= address "KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT"

  let%entry_point test params =
    verify ((slice (pack (set_type_expr (fst params) address)) (nat 6) (nat 22)) = (slice (pack (set_type_expr (snd params) address)) (nat 6) (nat 22)))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = bool; y = bool}]
      ~storage:[%expr
                 {x = false;
                  y = false}]
      [ep; test]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())