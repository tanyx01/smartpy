open SmartML

module Contract = struct
  let%entry_point test_factorial () =
    verify ((5 self.factorial) = 120);
    verify (((int 5) self.factorial_rec) = (int 120));
    verify ((() self.factorial_five) = 120)

  let%entry_point test_failing () =
    let%mutable aaa = ("" self.failing) in ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [test_factorial; test_failing]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())