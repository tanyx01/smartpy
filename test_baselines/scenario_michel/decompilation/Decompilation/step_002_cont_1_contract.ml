open SmartML

module Contract = struct
  let%entry_point fifo params =
    set_type params (`Left unit + `Right int);
    if is_variant "Left" params then
      (
        verify (data.Left.Left < data.Left.Right);
        Map.delete data.Right data.Left.Left;
        data.Left.Left <- (int 1) + data.Left.Left
      )
    else
      (
        data.Left.Right <- (int 1) + data.Left.Right;
        Map.set data.Right data.Left.Right (open_variant params Right)
      )

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {Left = {Left = int; Right = int}; Right = map int int}]
      ~storage:[%expr
                 {Left = {Left = int 0; Right = int ((-1))};
                  Right = Map.make []}]
      [fifo]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())