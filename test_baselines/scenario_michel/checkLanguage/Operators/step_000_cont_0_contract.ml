open SmartML

module Contract = struct
  let%entry_point run () =
    List.iter (fun test ->
      name, loc_op, loc_tests = match_tuple(test, "name", "loc_op", "loc_tests")
      List.iter (fun test_ ->
        x, y, res = match_tuple(test_, "x", "y", "res")
        let%mutable z = ((x, y) loc_op) in ();
        verify (z = res) ~msg:(name, (x, (y, z, res)))
      ) loc_tests
    ) [("+", fun _x0 -> result ((fst _x0) + (snd _x0)), [(int 11, int 22, int 33); (int 11, int 28, int 39); (int 11, int ((-47)), int ((-36))); (int 11, int ((-2)), int 9); (int 123, int 22, int 145); (int 123, int 28, int 151); (int 123, int ((-47)), int 76); (int 123, int ((-2)), int 121); (int ((-15)), int 22, int 7); (int ((-15)), int 28, int 13); (int ((-15)), int ((-47)), int ((-62))); (int ((-15)), int ((-2)), int ((-17)))]); ("-", fun _x2 -> result (((int 0) + (fst _x2)) - (snd _x2)), [(int 11, int 22, int ((-11))); (int 11, int 28, int ((-17))); (int 11, int ((-47)), int 58); (int 11, int ((-2)), int 13); (int 123, int 22, int 101); (int 123, int 28, int 95); (int 123, int ((-47)), int 170); (int 123, int ((-2)), int 125); (int ((-15)), int 22, int ((-37))); (int ((-15)), int 28, int ((-43))); (int ((-15)), int ((-47)), int 32); (int ((-15)), int ((-2)), int ((-13)))]); ("*", fun _x4 -> result ((fst _x4) * (snd _x4)), [(int 11, int 22, int 242); (int 11, int 28, int 308); (int 11, int ((-47)), int ((-517))); (int 11, int ((-2)), int ((-22))); (int 123, int 22, int 2706); (int 123, int 28, int 3444); (int 123, int ((-47)), int ((-5781))); (int 123, int ((-2)), int ((-246))); (int ((-15)), int 22, int ((-330))); (int ((-15)), int 28, int ((-420))); (int ((-15)), int ((-47)), int 705); (int ((-15)), int ((-2)), int 30)])];
    List.iter (fun test ->
      name, loc_op, loc_tests = match_tuple(test, "name", "loc_op", "loc_tests")
      List.iter (fun test_ ->
        x, y, res = match_tuple(test_, "x", "y", "res")
        let%mutable z = ((x, y) loc_op) in ();
        verify (z = res) ~msg:(name, (x, (y, z, res)))
      ) loc_tests
    ) [("%", fun _x6 -> result ((fst _x6) % (snd _x6)), [(nat 11, nat 22, nat 11); (nat 11, nat 28, nat 11); (nat 123, nat 22, nat 13); (nat 123, nat 28, nat 11)]); ("//", fun _x8 -> result ((fst _x8) / (snd _x8)), [(nat 11, nat 22, nat 0); (nat 11, nat 28, nat 0); (nat 123, nat 22, nat 5); (nat 123, nat 28, nat 4)])];
    List.iter (fun test ->
      name, loc_op, loc_tests = match_tuple(test, "name", "loc_op", "loc_tests")
      List.iter (fun test_ ->
        x, y, res = match_tuple(test_, "x", "y", "res")
        let%mutable z = ((x, y) loc_op) in ();
        verify (z = res) ~msg:(name, (x, (y, z, res)))
      ) loc_tests
    ) [("&", fun _x10 -> result ((fst _x10) && (snd _x10)), [(true, true, true); (true, false, false); (false, true, false); (false, false, false)]); ("|", fun _x12 -> result ((fst _x12) || (snd _x12)), [(true, true, true); (true, false, true); (false, true, true); (false, false, false)])]

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())