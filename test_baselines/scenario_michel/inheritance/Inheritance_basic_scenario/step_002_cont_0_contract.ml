open SmartML

module Contract = struct
  let%entry_point default () =
    verify (sender = data.owner) ~msg:"This entrypoint can only be called by the owner.";
    data.timeout <- add_seconds now data.alive_delta

  let%entry_point withdraw params =
    verify (amount = (tez 0)) ~msg:"This entrypoint doesn't accept deposits.";
    if sender = data.heir then
      verify (now > data.timeout) ~msg:"The owner is still considered alive, you cannot withdraw."
    else
      verify (sender = data.owner) ~msg:"Only owner or heir can withdraw.";
    send params.receiver params.amount

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {alive_delta = int; heir = address; owner = address; timeout = timestamp}]
      ~storage:[%expr
                 {alive_delta = int 31622400;
                  heir = address "tz1YWjrGm1iz9xzt3HoVaoqu1Y7UvC164GKf";
                  owner = address "tz1cjpubPzgzeFbLo45hwHxMiuKqVNuhYeSc";
                  timeout = timestamp 31622400}]
      [default; withdraw]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())