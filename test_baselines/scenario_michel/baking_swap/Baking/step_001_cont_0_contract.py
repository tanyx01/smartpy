import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, collateral = sp.TMutez, duration = sp.TInt, ledger = sp.TMap(sp.TAddress, sp.TRecord(amount = sp.TMutez, due = sp.TTimestamp).layout(("amount", "due"))), rate = sp.TNat).layout(("admin", ("collateral", ("duration", ("ledger", "rate"))))))
    self.init(admin = sp.address('tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq'),
              collateral = sp.tez(0),
              duration = 365,
              ledger = {},
              rate = 700)

  @sp.entry_point
  def collateralize(self):
    sp.verify(sp.sender == self.data.admin)
    self.data.collateral += sp.amount

  @sp.entry_point
  def delegate(self, params):
    sp.verify(sp.sender == self.data.admin)
    sp.verify(sp.amount == sp.tez(0))
    sp.verify(sp.sender == sp.to_address(sp.implicit_account(params)))
    sp.set_delegate(sp.some(params))

  @sp.entry_point
  def deposit(self, params):
    sp.verify(self.data.rate >= params.rate)
    sp.verify(self.data.duration <= params.duration)
    sp.verify(~ (self.data.ledger.contains(sp.sender)))
    compute_baking_swap_105 = sp.local("compute_baking_swap_105", sp.split_tokens(sp.amount, self.data.rate, 10000))
    self.data.collateral -= compute_baking_swap_105.value
    self.data.ledger[sp.sender] = sp.record(amount = sp.amount + compute_baking_swap_105.value, due = sp.add_seconds(sp.now, self.data.duration * 86400))

  @sp.entry_point
  def set_offer(self, params):
    sp.verify(sp.sender == self.data.admin)
    sp.verify(sp.amount == sp.tez(0))
    self.data.rate = params.rate
    self.data.duration = params.duration

  @sp.entry_point
  def uncollateralize(self, params):
    sp.verify(sp.sender == self.data.admin)
    sp.verify(params.amount <= self.data.collateral, 'insufficient collateral')
    self.data.collateral -= params.amount
    sp.send(params.receiver, params.amount)

  @sp.entry_point
  def withdraw(self, params):
    sp.verify(sp.amount == sp.tez(0))
    compute_baking_swap_118 = sp.local("compute_baking_swap_118", self.data.ledger.get(sp.sender, message = 'NoDeposit'))
    sp.verify(sp.now >= compute_baking_swap_118.value.due)
    sp.send(params, compute_baking_swap_118.value.amount)
    del self.data.ledger[sp.sender]

sp.add_compilation_target("test", Contract())