import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(fif = sp.TRecord(first = sp.TInt, last = sp.TInt, saved = sp.TMap(sp.TInt, sp.TIntOrNat)).layout(("first", ("last", "saved"))), queue = sp.TRecord(first = sp.TInt, last = sp.TInt, saved = sp.TMap(sp.TInt, sp.TIntOrNat)).layout(("first", ("last", "saved")))).layout(("fif", "queue")))
    self.init(fif = sp.record(first = 0, last = -1, saved = {}),
              queue = sp.record(first = 0, last = -1, saved = {}))

  @sp.entry_point
  def pop(self):
    sp.verify(self.data.fif.first < self.data.fif.last)
    del self.data.fif.saved[self.data.fif.first]
    self.data.fif.first += 1

  @sp.entry_point
  def popQ(self):
    sp.verify(self.data.queue.first < self.data.queue.last)
    del self.data.queue.saved[self.data.queue.first]
    self.data.queue.first += 1

  @sp.entry_point
  def push(self, params):
    self.data.fif.last += 1
    self.data.fif.saved[self.data.fif.last] = params
    self.data.queue.last += 1
    self.data.queue.saved[self.data.queue.last] = params

sp.add_compilation_target("test", Contract())