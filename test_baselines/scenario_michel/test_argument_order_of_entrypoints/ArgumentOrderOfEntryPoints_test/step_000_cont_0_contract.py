import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TNat, b = sp.TString, c = sp.TBool).layout(("a", ("b", "c"))))
    self.init(a = 1,
              b = '',
              c = True)

  @sp.entry_point
  def ep1(self, params):
    self.data.b = params.b
    self.data.a = params.a
    self.data.c = params.c

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params.a, sp.TInt)
    sp.set_type(params.c, sp.TBool)
    sp.set_type(params.b, sp.TString)

  @sp.entry_point
  def ep3(self, params):
    self.data.b = params

sp.add_compilation_target("test", Contract())