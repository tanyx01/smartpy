import smartpy as sp

tstorage = sp.TRecord(a = sp.TNat, b = sp.TString, c = sp.TBool).layout(("a", ("b", "c")))
tparameter = sp.TVariant(ep1 = sp.TRecord(a = sp.TNat, b = sp.TString, c = sp.TBool).layout(("a", ("c", "b"))), ep2 = sp.TRecord(a = sp.TInt, b = sp.TString, c = sp.TBool).layout(("a", ("b", "c"))), ep3 = sp.TString).layout(("ep1", ("ep2", "ep3")))
tprivates = { }
tviews = { }
