open SmartML

module Contract = struct
  let%entry_point administrate params =
    verify (sender = data.admin) ~msg:"NOT ADMIN";
    set_type params (list (`setActive bool + `setAdmin address));
    List.iter (fun action ->
      match action with
        | `setActive setActive ->
          data.active <- setActive
        | `setAdmin setAdmin ->
          data.admin <- setAdmin

    ) params

  let%entry_point setValue params =
    verify data.active ~msg:"NOT ACTIVE";
    data.value <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {active = bool; admin = address; value = option int}]
      ~storage:[%expr
                 {active = false;
                  admin = address "tz1UyQDepgtUBnWjyzzonqeDwaiWoQzRKSP5";
                  value = None}]
      [administrate; setValue]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())