import smartpy as sp

tstorage = sp.TRecord(result = sp.TOption(sp.TMutez)).layout("result")
tparameter = sp.TVariant(decrease = sp.TMutez, store = sp.TMutez, sub = sp.TRecord(x = sp.TMutez, y = sp.TMutez).layout(("x", "y"))).layout(("decrease", ("store", "sub")))
tprivates = { }
tviews = { }
