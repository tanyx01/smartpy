open SmartML

module Contract = struct
  let%entry_point test params =
    verify ((fst (open_some ~message:() (ediv amount (mutez 1)))) = params);
    verify ((mul (set_type_expr params nat) (mutez 1)) = amount)

  let%entry_point test_diff params =
    let%mutable compute_test_mutez_11 = (params.x - params.y) in ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [test; test_diff]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())