open SmartML

module Contract = struct
  let%entry_point main () =
    ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ int]
      ~storage:[%expr int 123]
      [main]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())