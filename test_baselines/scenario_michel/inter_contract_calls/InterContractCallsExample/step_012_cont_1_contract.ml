open SmartML

module Contract = struct
  let%entry_point append_multiple_messages params =
    set_type params {messages = list string; separator = string};
    List.iter (fun message ->
      transfer {message = message; separator = params.separator} (tez 0) (open_some (contract {message = string; separator = string} data.worker_contract_address , entry_point='append_message'))
    ) params.messages

  let%entry_point store_single_message params =
    transfer params (tez 0) (open_some (contract string data.worker_contract_address , entry_point='set_message'))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {worker_contract_address = address}]
      ~storage:[%expr
                 {worker_contract_address = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"}]
      [append_multiple_messages; store_single_message]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())