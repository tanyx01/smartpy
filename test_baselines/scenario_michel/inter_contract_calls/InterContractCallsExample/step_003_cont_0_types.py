import smartpy as sp

tstorage = sp.TRecord(message = sp.TString).layout("message")
tparameter = sp.TVariant(append_message = sp.TRecord(message = sp.TString, separator = sp.TString).layout(("message", "separator")), set_message = sp.TString).layout(("append_message", "set_message"))
tprivates = { }
tviews = { }
