Comment...
 h1: test_owner-or-operator-transfer_Fungible_minimal
Table Of Contents

 test_owner-or-operator-transfer_Fungible_minimal
# Accounts
# FA2 with OwnerOrOperatorTransfer policy
# Alice can transfer
# Admin cannot transfer alices's token: FA2_NOT_OPERATOR
# Alice adds Bob as operator
# Bob can transfer Alice's token id 0
# Bob cannot transfer Alice's token id 1
# Alice can remove Bob as operator and add Charlie
# Bob cannot transfer Alice's token 0 anymore
# Charlie can transfer Alice's token
# Add then Remove in the same batch is transparent
# Remove then Add do add the operator
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Bob")])...
 => [sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edsk3SUiUcR33jiBmxRDke8MKfd18dxmq2fUbZWZFYoiEsTkpAz5F7')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edsk34XphRR5Rs6EeAGrxktxAhstbwPr5YZ4m7RMzjaed3n9g5JcBB')), sp.record(seed = 'Bob', address = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), public_key = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT'), public_key_hash = sp.key_hash('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), secret_key = sp.secret_key('edsk3Rg6sSnow8KiHdHbWBZVF4Xui8ucyxmujHeA35HaAgvwKuXWio'))]
Comment...
 h2: FA2 with OwnerOrOperatorTransfer policy
Comment...
 p: Owner or operators can transfer.
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_pre_michelson.michel 377
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_storage.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_storage.json 93
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_storage.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_types.py 7
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_metadata.metadata_base.json 142
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_contract.tz 388
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_contract.json 470
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_contract.py 60
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_006_cont_0_contract.ml 65
Comment...
 h2: Alice can transfer
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_008_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_008_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_008_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
Comment...
 h2: Admin cannot transfer alices's token: FA2_NOT_OPERATOR
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_010_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_010_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_010_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: ((transfer.from_ == sp.sender) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))) : sp.TBool) (templates/fa2_fungible_minimal.py, line 80)
Message: 'FA2_NOT_OPERATOR'
 (templates/fa2_fungible_minimal.py, line 79)
Comment...
 h2: Alice adds Bob as operator
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_012_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_012_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_012_cont_0_params.json 11
Executing update_operators(sp.list([variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" 0)) Unit} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
Verifying sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))...
 OK
Comment...
 h2: Bob can transfer Alice's token id 0
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_015_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_015_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_015_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" 0)) Unit} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
Comment...
 h2: Bob cannot transfer Alice's token id 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_017_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_017_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_017_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 1, amount = 1)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: ((transfer.from_ == sp.sender) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))) : sp.TBool) (templates/fa2_fungible_minimal.py, line 80)
Message: 'FA2_NOT_OPERATOR'
 (templates/fa2_fungible_minimal.py, line 79)
Comment...
 h2: Alice can remove Bob as operator and add Charlie
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_019_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_019_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_019_cont_0_params.json 20
Executing update_operators(sp.list([variant('remove_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)), variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Charlie").address), token_id = 0))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0)) Unit} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
Verifying ~ (sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)))...
 OK
Verifying sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Charlie").address), token_id = 0))...
 OK
Comment...
 h2: Bob cannot transfer Alice's token 0 anymore
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_023_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_023_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_023_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: ((transfer.from_ == sp.sender) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))) : sp.TBool) (templates/fa2_fungible_minimal.py, line 80)
Message: 'FA2_NOT_OPERATOR'
 (templates/fa2_fungible_minimal.py, line 79)
Comment...
 h2: Charlie can transfer Alice's token
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_025_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_025_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_025_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Charlie").address), token_id = 0, amount = 1)]))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 41; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42; Elt (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0) 1} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0)) Unit} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Charlie").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Comment...
 h2: Add then Remove in the same batch is transparent
Comment...
 p: TZIP-12: If two different commands in the list add and remove an
                operator for the same token owner and token ID, the last command
                in the list MUST take effect.
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_029_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_029_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_029_cont_0_params.json 20
Executing update_operators(sp.list([variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)), variant('remove_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 41; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42; Elt (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0) 1} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0)) Unit} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
Verifying ~ (sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)))...
 OK
Comment...
 h2: Remove then Add do add the operator
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_032_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_032_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_owner-or-operator-transfer_Fungible_minimal/step_032_cont_0_params.json 20
Executing update_operators(sp.list([variant('remove_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0)), variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 41; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42; Elt (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0) 1} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" 0)) Unit; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1dotQT4SrJ2rbQfea4PfSK3CoAHVbeM9mk" 0)) Unit} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
Verifying sp.contract_data(0).operators.contains(sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Bob").address), token_id = 0))...
 OK
