Comment...
 h1: test_core_interfaces_Fungible_minimal
Table Of Contents

 test_core_interfaces_Fungible_minimal
# Accounts
# FA2 contract
# Entrypoint: update_operators
# Entrypoint: transfer
# Entrypoint: balance_of
## Receiver contract
## Call to balance_of
# Storage: token_metadata
Comment...
 p: A call to all the standard entrypoints and off-chain views.
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Bob")])...
 => [sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edsk3SUiUcR33jiBmxRDke8MKfd18dxmq2fUbZWZFYoiEsTkpAz5F7')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edsk34XphRR5Rs6EeAGrxktxAhstbwPr5YZ4m7RMzjaed3n9g5JcBB')), sp.record(seed = 'Bob', address = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), public_key = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT'), public_key_hash = sp.key_hash('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), secret_key = sp.secret_key('edsk3Rg6sSnow8KiHdHbWBZVF4Xui8ucyxmujHeA35HaAgvwKuXWio'))]
Comment...
 h2: FA2 contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_pre_michelson.michel 377
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_storage.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_storage.json 93
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_storage.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_types.py 7
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_metadata.metadata_base.json 142
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_contract.tz 388
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_contract.json 470
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_contract.py 60
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_006_cont_0_contract.ml 65
Comment...
 h2: Entrypoint: update_operators
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_008_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_008_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_008_cont_0_params.json 11
Executing update_operators(sp.set_type_expr(sp.list([variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Alice").address), token_id = 0))]), sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator")))))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0)) Unit} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
Comment...
 h2: Entrypoint: transfer
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_010_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_010_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_010_cont_0_params.json 9
Executing transfer(sp.set_type_expr(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 1)]))]), sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0)) Unit} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
Comment...
 h2: Entrypoint: balance_of
Comment...
 h3: Receiver contract
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_pre_michelson.michel 67
 -> {}
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_storage.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_storage.json 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_sizes.csv 2
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_storage.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_types.py 7
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_contract.tz 72
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_contract.json 93
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_contract.py 17
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_013_cont_1_contract.ml 25
Comment...
 h3: Call to balance_of
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_015_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_015_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_testing/test_core_interfaces_Fungible_minimal/step_015_cont_0_params.json 6
Executing balance_of(sp.set_type_expr(sp.record(requests = sp.list([sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0)]), callback = sp.contract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))), sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'), entry_point='receive_balances').open_some()), sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback"))))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0)) Unit} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})}))))))
  + Transfer
     params: [sp.record(request = sp.record(owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), token_id = 0), balance = 42)]
     amount: sp.tez(0)
     to:     sp.contract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))), sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF%receive_balances')).open_some()
Executing (queue) receive_balances([sp.record(request = sp.record(owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), token_id = 0), balance = 42)])...
 -> {Elt "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42}}
Comment...
 h2: Storage: token_metadata
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).token_metadata[0], sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info")))) == sp.pack(sp.set_type_expr(sp.record(token_id = 0, token_info = {'decimals' : sp.bytes('0x31'), 'name' : sp.bytes('0x546f6b656e205a65726f'), 'symbol' : sp.bytes('0x546f6b30')}), sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info"))))...
 OK
