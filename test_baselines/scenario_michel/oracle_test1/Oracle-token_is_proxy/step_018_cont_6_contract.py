import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, escrow = sp.TAddress, last_request_id = sp.TNat, requester = sp.TAddress, value = sp.TOption(sp.TInt)).layout(("admin", ("escrow", ("last_request_id", ("requester", "value"))))))
    self.init(admin = sp.address('tz1QNqeGVRnwQ4YjKaB4n9QzS6PYuyd9ThbN'),
              escrow = sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H'),
              last_request_id = 0,
              requester = sp.address('KT1Tezooo4zzSmartPyzzSTATiCzzzyPVdv3'),
              value = sp.none)

  @sp.entry_point
  def set_value(self, params):
    sp.verify(sp.sender == self.data.escrow, 'ReceiverNotEscrow')
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")))
    request_oracle_495, result_oracle_495 = sp.match_record(params, "request", "result")
    ticket_oracle_497_data, ticket_oracle_497_copy = sp.match_tuple(sp.read_ticket_raw(request_oracle_495), "ticket_oracle_497_data", "ticket_oracle_497_copy")
    ticket_oracle_497_ticketer, ticket_oracle_497_content, ticket_oracle_497_amount = sp.match_tuple(ticket_oracle_497_data, "ticket_oracle_497_ticketer", "ticket_oracle_497_content", "ticket_oracle_497_amount")
    ticket_oracle_498_data, ticket_oracle_498_copy = sp.match_tuple(sp.read_ticket_raw(result_oracle_495), "ticket_oracle_498_data", "ticket_oracle_498_copy")
    ticket_oracle_498_ticketer, ticket_oracle_498_content, ticket_oracle_498_amount = sp.match_tuple(ticket_oracle_498_data, "ticket_oracle_498_ticketer", "ticket_oracle_498_content", "ticket_oracle_498_amount")
    sp.verify(ticket_oracle_497_ticketer == self.data.requester, 'ReceiverBadRequester')
    sp.verify(ticket_oracle_497_content.client_request_id > self.data.last_request_id, 'ReceiverBadRequestId')
    self.data.last_request_id = ticket_oracle_497_content.client_request_id
    self.data.value = sp.some(ticket_oracle_498_content.result.open_variant('int'))

  @sp.entry_point
  def setup(self, params):
    sp.verify(sp.sender == self.data.admin, 'ReceiverNotAdmin')
    self.data.requester = params.requester
    self.data.escrow = params.escrow

sp.add_compilation_target("test", Contract())