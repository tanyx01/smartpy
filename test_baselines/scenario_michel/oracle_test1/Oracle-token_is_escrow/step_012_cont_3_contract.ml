open SmartML

module Contract = struct
  let%entry_point cancel_value params =
    verify (sender = data.admin) ~msg:"RequesterNotAdmin";
    transfer {client_request_id = (open_some (is_nat (data.next_request_id - (nat 1)))); force = params.force; oracle = data.oracle} (tez 0) (open_some (contract {client_request_id = nat; force = bool; oracle = address} data.escrow , entry_point='cancel_request'))

  let%entry_point request_value params =
    verify (sender = data.admin) ~msg:"RequesterNotAdmin";
    let%mutable ticket_431 = (ticket {cancel_timeout = (add_seconds now (params.cancel_timeout_minutes * (int 60))); client_request_id = data.next_request_id; fulfill_timeout = (add_seconds now (params.fulfill_timeout_minutes * (int 60))); job_id = data.job_id; oracle = data.oracle; parameters = params.parameters; tag = "OracleRequest"; target = (to_address (open_some ~message:"RequesterTargetUnknown" (contract {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}} (open_some ~message:"RequesterTargetUnknown" data.receiver) , entry_point='set_value')))} (nat 1)) in ();
    transfer {amount = params.amount; request = ticket_431} (tez 0) (open_some (contract {amount = nat; request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}} data.escrow , entry_point='send_request'));
    data.next_request_id <- data.next_request_id + (nat 1)

  let%entry_point set_receiver params =
    verify (sender = data.admin) ~msg:"RequesterNotAdmin";
    data.receiver <- some params

  let%entry_point setup params =
    verify (sender = data.admin) ~msg:"RequesterNotAdmin";
    data.admin <- params.admin;
    data.escrow <- params.escrow;
    data.oracle <- params.oracle;
    data.job_id <- params.job_id;
    data.token <- params.token

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admin = address; escrow = address; job_id = bytes; next_request_id = nat; oracle = address; receiver = option address; token = address}]
      ~storage:[%expr
                 {admin = address "tz1NiCaziaWuZWMTYuiXX6ceM5A42Ckd6yop";
                  escrow = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  job_id = bytes "0x0001";
                  next_request_id = nat 1;
                  oracle = address "KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H";
                  receiver = None;
                  token = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"}]
      [cancel_value; request_value; set_receiver; setup]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())