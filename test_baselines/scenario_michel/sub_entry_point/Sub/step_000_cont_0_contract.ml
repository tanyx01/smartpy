open SmartML

module Contract = struct
  let%entry_point f () =
    let%mutable compute_sub_entry_point_25 = ((nat 5) self.a) in ();
    let%mutable compute_sub_entry_point_25i = ((nat 10) self.a) in ();
    data.z <- compute_sub_entry_point_25 + compute_sub_entry_point_25i

  let%entry_point g () =
    let%mutable compute_sub_entry_point_29 = ((nat 6) self.a) in ();
    data.z <- compute_sub_entry_point_29

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = nat; y = string; z = nat}]
      ~storage:[%expr
                 {x = nat 2;
                  y = "aaa";
                  z = nat 0}]
      [f; g]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())