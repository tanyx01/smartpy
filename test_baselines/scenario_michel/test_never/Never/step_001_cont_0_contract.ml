open SmartML

module Contract = struct
  let%entry_point entry_point_1 params =
    let x =
      match params with
        | `A A ->
          result (A + 12)
        | `B B ->
          never B

    in
    data.x <- x

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat}]
      ~storage:[%expr
                 {x = 0}]
      [entry_point_1]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())