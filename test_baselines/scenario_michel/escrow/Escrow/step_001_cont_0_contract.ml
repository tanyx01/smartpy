open SmartML

module Contract = struct
  let%entry_point addBalanceCounterparty () =
    verify (data.balanceCounterparty = (tez 0));
    verify (amount = data.fromCounterparty);
    data.balanceCounterparty <- data.fromCounterparty

  let%entry_point addBalanceOwner () =
    verify (data.balanceOwner = (tez 0));
    verify (amount = data.fromOwner);
    data.balanceOwner <- data.fromOwner

  let%entry_point claimCounterparty params =
    verify (now < data.epoch);
    verify (data.hashedSecret = (blake2b params.secret));
    verify (sender = data.counterparty);
    send data.counterparty (data.balanceOwner + data.balanceCounterparty);
    data.balanceOwner <- tez 0;
    data.balanceCounterparty <- tez 0

  let%entry_point claimOwner () =
    verify (data.epoch < now);
    verify (sender = data.owner);
    send data.owner (data.balanceOwner + data.balanceCounterparty);
    data.balanceOwner <- tez 0;
    data.balanceCounterparty <- tez 0

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {balanceCounterparty = mutez; balanceOwner = mutez; counterparty = address; epoch = timestamp; fromCounterparty = mutez; fromOwner = mutez; hashedSecret = bytes; owner = address}]
      ~storage:[%expr
                 {balanceCounterparty = tez 0;
                  balanceOwner = tez 0;
                  counterparty = address "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C";
                  epoch = timestamp 123;
                  fromCounterparty = tez 4;
                  fromOwner = tez 50;
                  hashedSecret = bytes "0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e";
                  owner = address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"}]
      [addBalanceCounterparty; addBalanceOwner; claimCounterparty; claimOwner]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())