import smartpy as sp

tstorage = sp.TRecord(admin = sp.TAddress, latestRoundData = sp.TOption(sp.TRecord(answer = sp.TNat, answeredInRound = sp.TNat, roundId = sp.TNat, startedAt = sp.TTimestamp, updatedAt = sp.TTimestamp).layout(("answer", ("answeredInRound", ("roundId", ("startedAt", "updatedAt")))))), proxy = sp.TAddress).layout(("admin", ("latestRoundData", "proxy")))
tparameter = sp.TVariant(getLatestRoundData = sp.TUnit, setLatestRoundData = sp.TRecord(answer = sp.TNat, answeredInRound = sp.TNat, roundId = sp.TNat, startedAt = sp.TTimestamp, updatedAt = sp.TTimestamp).layout(("answer", ("answeredInRound", ("roundId", ("startedAt", "updatedAt"))))), setup = sp.TRecord(admin = sp.TAddress, proxy = sp.TAddress).layout(("admin", "proxy"))).layout(("getLatestRoundData", ("setLatestRoundData", "setup")))
tprivates = { }
tviews = { }
