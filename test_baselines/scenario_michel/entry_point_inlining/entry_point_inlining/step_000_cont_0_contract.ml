open SmartML

module Contract = struct
  let%entry_point entry_point_1 () =
    data.x <- data.x + 12;
    data.x <- data.x + 15

  let%entry_point f params =
    data.x <- data.x + params

  let%entry_point g params =
    data.x <- data.x + params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat}]
      ~storage:[%expr
                 {x = 11}]
      [entry_point_1; f; g]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())