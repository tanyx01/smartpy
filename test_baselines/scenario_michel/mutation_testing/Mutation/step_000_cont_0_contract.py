import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat).layout("x"))
    self.init(x = 42)

  @sp.entry_point
  def entry_point_1(self):
    self.data.x += 1

  @sp.entry_point
  def entry_point_2(self, params):
    sp.failwith(sp.unit)

sp.add_compilation_target("test", Contract())