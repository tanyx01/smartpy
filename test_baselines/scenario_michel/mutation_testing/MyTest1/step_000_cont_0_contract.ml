open SmartML

module Contract = struct
  let%entry_point entry_point_1 () =
    data.x <- data.x + 1

  let%entry_point entry_point_2 params =
    verify (params < 2)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat}]
      ~storage:[%expr
                 {x = 42}]
      [entry_point_1; entry_point_2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())