import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep(self, params):
    sp.set_type(params, sp.TBounded(['abc', 'def'], t=sp.TString))

  @sp.entry_point
  def ep2(self):
    pass

sp.add_compilation_target("test", Contract())