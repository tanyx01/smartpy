open SmartML

module Contract = struct
  let%entry_point ep () =
    data.x <- 1

  let%entry_point ep2 () =
    data.x <- 2

  let%entry_point ep3 () =
    data.y <- unbound data.x

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = bounded([1, 2, 3], t=int); y = int}]
      ~storage:[%expr
                 {x = 1;
                  y = int 0}]
      [ep; ep2; ep3]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())