open SmartML

module Contract = struct
  let%entry_point play params =
    verify ((data.winner = (int 0)) && (not data.draw));
    verify ((params.i >= 0) && (params.i < 3));
    verify ((params.j >= 0) && (params.j < 3));
    verify (params.move = data.nextPlayer);
    verify ((Map.get (Map.get data.deck params.i) params.j) = (int 0));
    Map.set (Map.get data.deck params.i) params.j params.move;
    data.nbMoves <- data.nbMoves + 1;
    data.nextPlayer <- (int 3) - data.nextPlayer;
    if (((Map.get (Map.get data.deck params.i) 0) <> (int 0)) && ((Map.get (Map.get data.deck params.i) 0) = (Map.get (Map.get data.deck params.i) 1))) && ((Map.get (Map.get data.deck params.i) 0) = (Map.get (Map.get data.deck params.i) 2)) then
      data.winner <- Map.get (Map.get data.deck params.i) 0;
    if (((Map.get (Map.get data.deck 0) params.j) <> (int 0)) && ((Map.get (Map.get data.deck 0) params.j) = (Map.get (Map.get data.deck 1) params.j))) && ((Map.get (Map.get data.deck 0) params.j) = (Map.get (Map.get data.deck 2) params.j)) then
      data.winner <- Map.get (Map.get data.deck 0) params.j;
    if (((Map.get (Map.get data.deck 0) 0) <> (int 0)) && ((Map.get (Map.get data.deck 0) 0) = (Map.get (Map.get data.deck 1) 1))) && ((Map.get (Map.get data.deck 0) 0) = (Map.get (Map.get data.deck 2) 2)) then
      data.winner <- Map.get (Map.get data.deck 0) 0;
    if (((Map.get (Map.get data.deck 0) 2) <> (int 0)) && ((Map.get (Map.get data.deck 0) 2) = (Map.get (Map.get data.deck 1) 1))) && ((Map.get (Map.get data.deck 0) 2) = (Map.get (Map.get data.deck 2) 0)) then
      data.winner <- Map.get (Map.get data.deck 0) 2;
    if (data.nbMoves = 9) && (data.winner = (int 0)) then
      data.draw <- true

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {deck = map intOrNat (map intOrNat int); draw = bool; nbMoves = intOrNat; nextPlayer = int; winner = int}]
      ~storage:[%expr
                 {deck = Map.make [(0, Map.make [(0, int 0); (1, int 0); (2, int 0)]); (1, Map.make [(0, int 0); (1, int 0); (2, int 0)]); (2, Map.make [(0, int 0); (1, int 0); (2, int 0)])];
                  draw = false;
                  nbMoves = 0;
                  nextPlayer = int 1;
                  winner = int 0}]
      [play]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())