open SmartML

module Contract = struct
  let%entry_point check_view () =
    let%mutable compute_test_onchain_views_83 = view("abc", (), self_address, unit) in ()

  let%entry_point failing_entry_point () =
    failwith "This is a failure"

  let%entry_point store () =
    data.self_addr <- some self_address;
    let%mutable compute_test_onchain_views_66 = (open_some ~message:"Invalid view" view("view1", (), data.contract, {self_addr = address; z = nat})) in ();
    data.result2 <- view("a_really_good_name", (), self_address, {amount = mutez; balance = mutez; chain_id = chain_id; level = nat; now = timestamp; sender = address; source = address; total_voting_power = nat});
    data.result <- some compute_test_onchain_views_66.z;
    data.self_addr_2 <- some compute_test_onchain_views_66.self_addr;
    data.self_addr_3 <- some self_address;
    verify (data.self_addr = data.self_addr_3);
    verify (data.self_addr_2 = (some data.contract))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {contract = address; result = option nat; result2 = option {amount = mutez; balance = mutez; chain_id = chain_id; level = nat; now = timestamp; sender = address; source = address; total_voting_power = nat}; self_addr = option address; self_addr_2 = option address; self_addr_3 = option address}]
      ~storage:[%expr
                 {contract = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  result = None;
                  result2 = None;
                  self_addr = None;
                  self_addr_2 = None;
                  self_addr_3 = None}]
      [check_view; failing_entry_point; store]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())