import smartpy as sp

tstorage = sp.TRecord(contract = sp.TAddress, result = sp.TOption(sp.TNat), result2 = sp.TOption(sp.TRecord(amount = sp.TMutez, balance = sp.TMutez, chain_id = sp.TChainId, level = sp.TNat, now = sp.TTimestamp, sender = sp.TAddress, source = sp.TAddress, total_voting_power = sp.TNat).layout(("amount", ("balance", ("chain_id", ("level", ("now", ("sender", ("source", "total_voting_power"))))))))), self_addr = sp.TOption(sp.TAddress), self_addr_2 = sp.TOption(sp.TAddress), self_addr_3 = sp.TOption(sp.TAddress)).layout(("contract", ("result", ("result2", ("self_addr", ("self_addr_2", "self_addr_3"))))))
tparameter = sp.TVariant(check_view = sp.TUnit, failing_entry_point = sp.TUnit, store = sp.TUnit).layout(("check_view", ("failing_entry_point", "store")))
tprivates = { }
tviews = { "a_really_good_name": ((), sp.TRecord(amount = sp.TMutez, balance = sp.TMutez, chain_id = sp.TChainId, level = sp.TNat, now = sp.TTimestamp, sender = sp.TAddress, source = sp.TAddress, total_voting_power = sp.TNat).layout(("amount", ("balance", ("chain_id", ("level", ("now", ("sender", ("source", "total_voting_power"))))))))) }
