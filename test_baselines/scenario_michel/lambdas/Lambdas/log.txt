Comment...
 h1: Lambdas
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_pre_michelson.michel 617
 -> (Pair 0 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 0))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_storage.json 13
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_storage.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_types.py 7
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_contract.tz 757
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_contract.json 791
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_contract.py 143
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_0_contract.ml 94
 => test_baselines/scenario_michel/lambdas/Lambdas/step_002_cont_0_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_002_cont_0_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_002_cont_0_params.json 1
Executing f(sp.record())...
 -> (Pair 0 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 13))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_003_cont_0_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_003_cont_0_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_003_cont_0_params.json 1
Executing flambda(sp.record())...
 -> (Pair 0 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 1555371))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_004_cont_0_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_004_cont_0_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_004_cont_0_params.json 1
Executing i(sp.record())...
 -> (Pair 0 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_005_cont_0_params.json 1
Executing comp_test(sp.record())...
 -> (Pair 5 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_006_cont_0_params.json 1
Executing abs_test(5)...
 -> (Pair 5 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
Verifying sp.contract_data(0).abcd == 5...
 OK
 => test_baselines/scenario_michel/lambdas/Lambdas/step_008_cont_0_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_008_cont_0_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_008_cont_0_params.json 1
Executing abs_test(-42)...
 -> (Pair 42 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
Verifying sp.contract_data(0).abcd == 42...
 OK
 => test_baselines/scenario_michel/lambdas/Lambdas/step_010_cont_0_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_010_cont_0_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_010_cont_0_params.json 1
Executing fact(3)...
 -> (Pair 6 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
Verifying sp.contract_data(0).abcd == 6...
 OK
 => test_baselines/scenario_michel/lambdas/Lambdas/step_012_cont_0_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_012_cont_0_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_012_cont_0_params.json 1
Executing fact(5)...
 -> (Pair 120 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
Verifying sp.contract_data(0).abcd == 120...
 OK
