open SmartML

module Contract = struct
  let%entry_point calc params =
    data.value <- params data.logic

  let%entry_point updateLogic params =
    data.logic <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {logic = lambda bytes nat; value = nat}]
      ~storage:[%expr
                 {logic = lambda;
                  value = nat 0}]
      [calc; updateLogic]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())