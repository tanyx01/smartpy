import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(Help = sp.TList(sp.TString), formula = sp.TString, operations = sp.TList(sp.TString), result = sp.TInt, summary = sp.TString).layout(("Help", ("formula", ("operations", ("result", "summary"))))))
    self.init(Help = ['SmartPy Reverse Polish Calculator Template', 'Operations: +, -, *, ^', 'Example: 2 3 4 * - 3 ^'],
              formula = '',
              operations = [],
              result = 0,
              summary = '')

  @sp.entry_point
  def compute(self, params):
    def f_x12(_x12):
      result = sp.local("result", 1)
      sp.for i in sp.range(0, sp.fst(_x12)):
        result.value *= sp.snd(_x12)
      sp.result(result.value)
    bin_ops = sp.local("bin_ops", {'+' : sp.build_lambda(lambda _x6: sp.fst(_x6) + sp.snd(_x6)), '*' : sp.build_lambda(lambda _x8: sp.fst(_x8) * sp.snd(_x8)), '-' : sp.build_lambda(lambda _x10: sp.snd(_x10) - sp.fst(_x10)), '^' : sp.build_lambda(f_x12)})
    elements = sp.local("elements", self.string_split(sp.record(s = params, sep = ' ')))
    stack = sp.local("stack", sp.list([]), sp.TList(sp.TInt))
    formulas = sp.local("formulas", sp.list([]))
    sp.for element in elements.value:
      sp.if element != '':
        sp.if bin_ops.value.contains(element):
          with sp.match_cons(stack.value) as match_cons_67:
            with sp.match_cons(match_cons_67.tail) as match_cons_68:
              stack.value = match_cons_68.tail
              stack.value.push(bin_ops.value[element]((match_cons_67.head, match_cons_68.head)))
            else:
              sp.failwith('Bad formula: too many operators')
          else:
            sp.failwith('Bad formula: too many operators')
          with sp.match_cons(formulas.value) as match_cons_75:
            with sp.match_cons(match_cons_75.tail) as match_cons_76:
              formulas.value = match_cons_76.tail
              formulas.value.push(sp.concat(sp.list(['(', match_cons_76.head, element, match_cons_75.head, ')'])))
            else:
              pass
          else:
            pass
        sp.else:
          stack.value.push(self.nat_of_string(element))
          formulas.value.push(element)
    self.data.formula = params
    self.data.operations = elements.value
    length = sp.local("length", sp.len(stack.value))
    sp.verify(length.value == 1, ('Bad stack at the end of the computation. Length = ' + self.string_of_nat(length.value), stack.value))
    with sp.match_cons(stack.value) as match_cons_86:
      with sp.match_cons(formulas.value) as match_cons_87:
        self.data.result = match_cons_86.head
        sp.if match_cons_86.head >= 0:
          self.data.summary = (match_cons_87.head + ' = ') + self.string_of_nat(sp.as_nat(match_cons_86.head))
        sp.else:
          self.data.summary = (match_cons_87.head + ' = -') + self.string_of_nat(sp.as_nat(- match_cons_86.head))
      else:
        pass
    else:
      pass

  @sp.private_lambda()
  def nat_of_string(_x0):
    result = sp.local("result", 0)
    sp.for idx in sp.range(0, sp.len(_x0)):
      result.value = (10 * result.value) + {'0' : 0, '1' : 1, '2' : 2, '3' : 3, '4' : 4, '5' : 5, '6' : 6, '7' : 7, '8' : 8, '9' : 9}[sp.slice(_x0, idx, 1).open_some()]
    sp.result(result.value)

  @sp.private_lambda()
  def string_of_nat(_x2):
    x = sp.local("x", _x2)
    string_res = sp.local("string_res", sp.list([]))
    sp.if x.value == 0:
      string_res.value.push('0')
    sp.while x.value > 0:
      string_res.value.push({0 : '0', 1 : '1', 2 : '2', 3 : '3', 4 : '4', 5 : '5', 6 : '6', 7 : '7', 8 : '8', 9 : '9'}[x.value % 10])
      x.value //= 10
    sp.result(sp.concat(string_res.value))

  @sp.private_lambda()
  def string_split(_x4):
    prev_idx = sp.local("prev_idx", 0)
    res = sp.local("res", sp.list([]))
    sp.for idx in sp.range(0, sp.len(sp.set_type_expr(_x4.s, sp.TString))):
      sp.if sp.slice(sp.set_type_expr(_x4.s, sp.TString), idx, 1).open_some() == _x4.sep:
        res.value.push(sp.slice(sp.set_type_expr(_x4.s, sp.TString), prev_idx.value, sp.as_nat(idx - prev_idx.value)).open_some())
        prev_idx.value = idx + 1
    sp.if sp.len(sp.set_type_expr(_x4.s, sp.TString)) > 0:
      res.value.push(sp.slice(sp.set_type_expr(_x4.s, sp.TString), prev_idx.value, sp.as_nat(sp.len(sp.set_type_expr(_x4.s, sp.TString)) - prev_idx.value)).open_some())
    sp.result(res.value.rev())

sp.add_compilation_target("test", Contract())