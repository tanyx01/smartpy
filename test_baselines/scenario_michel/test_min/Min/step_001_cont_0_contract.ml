open SmartML

module Contract = struct
  let%entry_point ep1 () =
    let%mutable a = 1 in ();
    let%mutable b = 2 in ();
    data.r <- min a b

  let%entry_point ep1b params =
    let%mutable a = params in ();
    let%mutable b = 2 in ();
    data.r <- min a b

  let%entry_point ep2 () =
    let%mutable a = 3 in ();
    let%mutable b = 4 in ();
    data.r <- max a b

  let%entry_point ep2b params =
    let%mutable a = params in ();
    let%mutable b = 4 in ();
    data.r <- max a b

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {r = intOrNat}]
      ~storage:[%expr
                 {r = 0}]
      [ep1; ep1b; ep2; ep2b]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())