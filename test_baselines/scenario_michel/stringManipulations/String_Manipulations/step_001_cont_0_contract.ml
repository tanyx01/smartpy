open SmartML

module Contract = struct
  let%entry_point concatenating params =
    data.s0 <- some (concat params.s);
    data.b0 <- some (concat [params.b0; params.b1; concat params.sb])

  let%entry_point concatenating2 params =
    data.s0 <- some (params.s1 + params.s2);
    data.b0 <- some (params.b1 + params.b2)

  let%entry_point slicing params =
    data.s0 <- slice params.s (nat 2) (nat 5);
    data.b0 <- slice params.b (nat 1) (nat 2);
    data.l0 <- len params.s;
    with data.s0.match('Some') as Some:
      data.l0 <- data.l0 + (len Some);
    data.l1 <- len params.b

  let%entry_point test_nat_of_string params =
    let%mutable res = 0 in ();
    List.iter (fun idx ->
      res <- (10 * res) + (Map.get (Map.make [("0", 0); ("1", 1); ("2", 2); ("3", 3); ("4", 4); ("5", 5); ("6", 6); ("7", 7); ("8", 8); ("9", 9)]) (open_some (slice params idx (nat 1))))
    ) (range (nat 0) (len params) (nat 1));
    data.nat_of_string <- res

  let%entry_point test_split params =
    let%mutable prev_idx = (nat 0) in ();
    let%mutable res = [] in ();
    List.iter (fun idx ->
      if (open_some (slice params idx (nat 1))) = "," then
        (
          res <- (open_some (slice params prev_idx (open_some (is_nat (idx - prev_idx))))) :: res;
          prev_idx <- idx + (nat 1)
        )
    ) (range (nat 0) (len params) (nat 1));
    if (len params) > (nat 0) then
      res <- (open_some (slice params prev_idx (open_some (is_nat ((len params) - prev_idx))))) :: res;
    data.split <- List.rev res

  let%entry_point test_string_of_nat params =
    let%mutable x = params in ();
    let%mutable res = [] in ();
    if x = (nat 0) then
      res <- "0" :: res;
    while x > (nat 0) do
      res <- (Map.get (Map.make [(nat 0, "0"); (nat 1, "1"); (nat 2, "2"); (nat 3, "3"); (nat 4, "4"); (nat 5, "5"); (nat 6, "6"); (nat 7, "7"); (nat 8, "8"); (nat 9, "9")]) (x % (nat 10))) :: res;
      x <- x / (nat 10)
    done;
    data.string_of_nat <- concat res

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {b0 = option bytes; l0 = nat; l1 = nat; nat_of_string = intOrNat; s0 = option string; split = list string; string_of_nat = string}]
      ~storage:[%expr
                 {b0 = Some(bytes "0xaa");
                  l0 = nat 0;
                  l1 = nat 0;
                  nat_of_string = 0;
                  s0 = Some("hello");
                  split = [];
                  string_of_nat = ""}]
      [concatenating; concatenating2; slicing; test_nat_of_string; test_split; test_string_of_nat]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())