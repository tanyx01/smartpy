open SmartML

module Contract = struct
  let%entry_point ep1 () =
    data.xy <- (snd data.xy, fst data.xy)

  let%entry_point ep2 () =
    data.xy <- (snd (fst (data, ())).xy, fst (fst (data, ())).xy)

  let%entry_point ep3 () =
    data.xy <- (fst data.xy, fst data.xy)

  let%entry_point ep4 () =
    data.xy <- (snd data.xy, snd data.xy)

  let%entry_point ep5 params =
    set_type params nat;
    match ediv params (nat 2) with
      | `None None ->
        data.xy <- (0, 0)
      | `Some Some ->
        data.xy <- (1, 1)


  let%entry_point ep6 params =
    set_type params nat;
    match ediv params (nat 2) with
      | `Some Some ->
        data.xy <- (1, 1)
      | `None None ->
        data.xy <- (0, 0)


  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {xy = pair intOrNat intOrNat}]
      ~storage:[%expr
                 {xy = (0, 0)}]
      [ep1; ep2; ep3; ep4; ep5; ep6]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())