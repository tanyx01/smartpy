open SmartML

module Contract = struct
  let%entry_point ep () =
    data.f <- some (fun _x0 -> if _x0 = 0 then
  failwith "zero"
else
  result 1)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {f = option lambda intOrNat intOrNat}]
      ~storage:[%expr
                 {f = None}]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())