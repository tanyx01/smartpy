import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TString, sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat).layout(("a", "b"))), out = sp.TString).layout(("m", "out")))
    self.init(m = {'abc' : sp.record(a = 10, b = 20)},
              out = 'z')

  @sp.entry_point
  def ep1(self, params):
    match_pair_test_match_9_fst, match_pair_test_match_9_snd = sp.match_tuple(params, "match_pair_test_match_9_fst", "match_pair_test_match_9_snd")
    sp.verify(match_pair_test_match_9_fst == 'x')
    sp.verify(match_pair_test_match_9_snd == 2)

  @sp.entry_point
  def ep2(self, params):
    my_x, my_y, my_z = sp.match_tuple(params, "my_x", "my_y", "my_z")
    sp.verify(my_x == 'x')
    sp.verify(my_y == 2)
    sp.verify(my_z)

  @sp.entry_point
  def ep3(self, params):
    sp.set_type(params, sp.TRecord(x = sp.TString, y = sp.TInt, z = sp.TBool).layout(("x", ("y", "z"))))
    x_test_match_23, z_test_match_23 = sp.match_record(params, "x", "z")
    sp.verify(x_test_match_23 == 'x')
    sp.verify(z_test_match_23)

  @sp.entry_point
  def ep4(self, params):
    sp.set_type(params.x01, sp.TInt)
    sp.set_type(params.x02, sp.TKey)
    sp.set_type(params.x03, sp.TString)
    sp.set_type(params.x04, sp.TTimestamp)
    sp.set_type(params.x05, sp.TBytes)
    sp.set_type(params.x06, sp.TAddress)
    sp.set_type(params.x07, sp.TBool)
    sp.set_type(params.x08, sp.TKeyHash)
    sp.set_type(params.x09, sp.TSignature)
    sp.set_type(params.x10, sp.TMutez)
    x07_test_match_39, x03_test_match_39 = sp.match_record(params, "x07", "x03")
    sp.verify(x03_test_match_39 == 'x')
    sp.verify(x07_test_match_39)

  @sp.entry_point
  def ep5(self, params):
    a, b, c, d = sp.match_tuple(params, "a", "b", "c", "d")
    sp.verify(((a * b) + (c * d)) == 12)

  @sp.entry_point
  def ep6(self, params):
    a, b, c, d = sp.match_tuple(params, "a", "b", "c", "d")
    sp.set_type(c, sp.TInt)
    sp.verify(((a * b) + d) == 12)

  @sp.entry_point
  def ep7(self):
    k = sp.local("k", 'abc')
    with sp.match_record(self.data.m[k.value], "data") as data:
      k.value = 'xyz' + k.value
    self.data.out = k.value

sp.add_compilation_target("test", Contract())