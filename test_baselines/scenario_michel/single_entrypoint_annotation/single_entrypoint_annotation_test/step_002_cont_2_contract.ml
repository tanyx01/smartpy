open SmartML

module Contract = struct
  let%entry_point ep1 params =
    transfer (nat 1) (tez 0) (open_some ~message:"WRONG_INTERFACE_Contract1" (contract nat params.address1 ));
    transfer (nat 1) (tez 0) (open_some ~message:"WRONG_INTERFACE_Contract2" (contract nat params.address2 , entry_point='ep'))

  let%entry_point ep2 params =
    transfer (nat 1) (tez 0) (open_some ~message:"WRONG_INTERFACE_Contract1" (contract nat params.address1 ));
    transfer (nat 1) (tez 0) (open_some ~message:"WRONG_INTERFACE_Contract2" (contract nat params.address2 , entry_point='ep'))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep1; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())