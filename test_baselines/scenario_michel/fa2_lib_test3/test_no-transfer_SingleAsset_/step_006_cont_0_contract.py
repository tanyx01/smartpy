import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(last_token_id = sp.TNat, ledger = sp.TBigMap(sp.TAddress, sp.TNat), metadata = sp.TBigMap(sp.TString, sp.TBytes), supply = sp.TNat, token_metadata = sp.TBigMap(sp.TNat, sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info")))).layout(("last_token_id", ("ledger", ("metadata", ("supply", "token_metadata"))))))
    self.init(last_token_id = 1,
              ledger = {sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi') : 42},
              metadata = {'' : sp.bytes('0x697066733a2f2f6578616d706c65')},
              supply = 42,
              token_metadata = {0 : sp.record(token_id = 0, token_info = {'decimals' : sp.bytes('0x31'), 'name' : sp.bytes('0x546f6b656e205a65726f'), 'symbol' : sp.bytes('0x546f6b30')})})

  @sp.entry_point
  def balance_of(self, params):
    sp.set_type(params, sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")))
    sp.set_type(params.requests, sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))))
    def f_x0(_x0):
      sp.verify(self.data.token_metadata.contains(_x0.token_id), 'FA2_TOKEN_UNDEFINED')
      sp.result(sp.record(request = _x0, balance = self.data.ledger.get(_x0.owner, default_value = 0)))
    sp.transfer(params.requests.map(sp.build_lambda(f_x0)), sp.tez(0), params.callback)

  @sp.entry_point
  def transfer(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))))
    sp.failwith('FA2_TX_DENIED')

  @sp.entry_point
  def update_operators(self, params):
    sp.set_type(params, sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))))
    sp.failwith('FA2_OPERATORS_UNSUPPORTED')

sp.add_compilation_target("test", Contract())