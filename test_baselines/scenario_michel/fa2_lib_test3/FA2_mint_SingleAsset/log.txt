Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_pre_michelson.michel 483
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair 0 {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30})}))))))
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_storage.tz 1
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_storage.json 55
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_storage.py 1
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_types.py 7
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_metadata.metadata_base.json 168
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_contract.tz 551
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_contract.json 663
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_contract.py 87
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_000_cont_0_contract.ml 92
Comment...
 h1: FA2_mint_SingleAsset
Comment...
 h2: Mint entrypoint
Comment...
 h3: Single asset mint failure
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_004_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_004_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_004_cont_0_params.json 1
Executing mint(sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), amount = 1000)]))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sender == self.data.administrator : sp.TBool) (templates/fa2_lib.py, line 513)
Message: 'FA2_NOT_ADMIN'
 (templates/fa2_lib.py, line 665)
Comment...
 h3: Mint
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_006_cont_0_params.json 1
Executing mint(sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), amount = 1000)]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1000} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair 1000 {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30})}))))))
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 1000...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Bob").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 0...
 OK
Verifying sp.contract_view(0, "total_supply", sp.record(token_id = 0)).open_some(message = 'View total_supply is invalid!') == 1000...
 OK
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_010_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_010_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_test3/FA2_mint_SingleAsset/step_010_cont_0_params.json 5
Executing mint(sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), amount = 1000), sp.record(to_ = sp.resolve(sp.test_account("Bob").address), amount = 1000), sp.record(to_ = sp.resolve(sp.test_account("Bob").address), amount = 1000)]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C" 2000; Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2000} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair 4000 {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30})}))))))
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 2000...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Bob").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 2000...
 OK
Verifying sp.contract_view(0, "total_supply", sp.record(token_id = 0)).open_some(message = 'View total_supply is invalid!') == 4000...
 OK
