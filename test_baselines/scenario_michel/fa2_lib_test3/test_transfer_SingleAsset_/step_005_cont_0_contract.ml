open SmartML

module Contract = struct
  let%entry_point balance_of params =
    set_type params {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}};
    set_type params.requests (list {owner = address; token_id = nat});
    transfer (map (fun _x0 -> verify (contains _x0.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
result {request = _x0; balance = (Map.get ~default_value:(nat 0) data.ledger _x0.owner)}) params.requests) (tez 0) params.callback

  let%entry_point transfer params =
    set_type params (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}});
    List.iter (fun transfer ->
      List.iter (fun tx ->
        verify (contains tx.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
        verify ((sender = transfer.from_) || (contains {owner = transfer.from_; operator = sender; token_id = tx.token_id} data.operators)) ~msg:"FA2_NOT_OPERATOR";
        if tx.amount > (nat 0) then
          (
            Map.set data.ledger transfer.from_ (open_some ~message:"FA2_INSUFFICIENT_BALANCE" (is_nat ((Map.get ~default_value:(nat 0) data.ledger transfer.from_) - tx.amount)));
            Map.set data.ledger tx.to_ ((Map.get ~default_value:(nat 0) data.ledger tx.to_) + tx.amount)
          )
      ) transfer.txs
    ) params

  let%entry_point update_operators params =
    set_type params (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat}));
    List.iter (fun action ->
      match action with
        | `add_operator add_operator ->
          verify (add_operator.owner = sender) ~msg:"FA2_NOT_OWNER";
          Map.set data.operators add_operator ()
        | `remove_operator remove_operator ->
          verify (remove_operator.owner = sender) ~msg:"FA2_NOT_OWNER";
          Map.delete data.operators remove_operator

    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {last_token_id = nat; ledger = big_map address nat; metadata = big_map string bytes; operators = big_map {operator = address; owner = address; token_id = nat} unit; supply = nat; token_metadata = big_map nat {token_id = nat; token_info = map string bytes}}]
      ~storage:[%expr
                 {last_token_id = nat 1;
                  ledger = Map.make [(address "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi", nat 42)];
                  metadata = Map.make [("", bytes "0x697066733a2f2f6578616d706c65")];
                  operators = Map.make [];
                  supply = nat 42;
                  token_metadata = Map.make [(nat 0, {token_id = nat 0; token_info = Map.make [("decimals", bytes "0x31"); ("name", bytes "0x546f6b656e205a65726f"); ("symbol", bytes "0x546f6b30")]})]}]
      [balance_of; transfer; update_operators]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())