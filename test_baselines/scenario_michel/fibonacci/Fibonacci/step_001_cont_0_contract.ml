open SmartML

module Contract = struct
  let%entry_point compute params =
    data.counter <- 0;
    data.steps <- [];
    transfer params (tez 0) (self_entry_point "run")

  let%entry_point run params =
    data.steps <- params :: data.steps;
    if params > (int 1) then
      (
        transfer (params - (int 2)) (tez 0) (self_entry_point "run");
        transfer (params - (int 1)) (tez 0) (self_entry_point "run")
      )
    else
      data.counter <- data.counter + 1

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {counter = intOrNat; steps = list int}]
      ~storage:[%expr
                 {counter = 0;
                  steps = []}]
      [compute; run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())