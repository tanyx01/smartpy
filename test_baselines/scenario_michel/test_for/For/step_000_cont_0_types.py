import smartpy as sp

tstorage = sp.TRecord(ms = sp.TList(sp.TMap(sp.TString, sp.TIntOrNat)), rs = sp.TList(sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat).layout(("a", "b"))), x = sp.TIntOrNat, xxs = sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TNat)), xxxs = sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TIntOrNat)))).layout(("ms", ("rs", ("x", ("xxs", "xxxs")))))
tparameter = sp.TVariant(ep1 = sp.TUnit, ep2 = sp.TUnit, ep3 = sp.TUnit, ep4 = sp.TUnit, ep5 = sp.TUnit, ep6 = sp.TUnit, ep7 = sp.TUnit).layout((("ep1", ("ep2", "ep3")), (("ep4", "ep5"), ("ep6", "ep7"))))
tprivates = { }
tviews = { }
