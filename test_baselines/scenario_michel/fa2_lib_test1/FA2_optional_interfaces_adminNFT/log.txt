Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_pre_michelson.michel 494
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 0 (Pair {} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} {})))))
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_storage.tz 1
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_storage.json 22
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_storage.py 1
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_types.py 7
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_metadata.metadata_base.json 174
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_contract.tz 564
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_contract.json 689
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_contract.py 83
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_000_cont_0_contract.ml 90
Comment...
 h1: FA2_optional_interfaces_adminNFT
Comment...
 h2: Non admin cannot set admin
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_003_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_003_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_003_cont_0_params.json 1
Executing set_administrator(sp.resolve(sp.test_account("Alice").address))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sender == self.data.administrator : sp.TBool) (templates/fa2_lib.py, line 513)
Message: 'FA2_NOT_ADMIN'
 (templates/fa2_lib.py, line 518)
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_004_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_004_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_004_cont_0_params.json 1
Executing set_administrator(sp.resolve(sp.test_account("Administrator2").address))...
 -> (Pair "tz1VPsEA8g8zzNrv9Y3iCwwTLTuCDct2Pw15" (Pair 0 (Pair {} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} {})))))
Verifying ~ (sp.resolve(sp.test_account("Administrator").address) == sp.contract_data(0).administrator)...
 OK
Verifying sp.resolve(sp.test_account("Administrator2").address) == sp.contract_data(0).administrator...
 OK
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_007_cont_0_params.py 1
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_007_cont_0_params.tz 1
 => test_baselines/scenario_michel/fa2_lib_test1/FA2_optional_interfaces_adminNFT/step_007_cont_0_params.json 1
Executing set_administrator(sp.resolve(sp.test_account("Administrator").address))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 0 (Pair {} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} {})))))
