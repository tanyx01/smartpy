import smartpy as sp

tstorage = sp.TRecord(votes = sp.TList(sp.TRecord(sender = sp.TAddress, vote = sp.TString).layout(("sender", "vote")))).layout("votes")
tparameter = sp.TVariant(vote = sp.TRecord(vote = sp.TString).layout("vote")).layout("vote")
tprivates = { }
tviews = { }
