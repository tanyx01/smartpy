open SmartML

module Contract = struct
  let%entry_point build params =
    Map.set data.multisigs data.nbMultisigs params.contract;
    data.nbMultisigs <- data.nbMultisigs + 1

  let%entry_point sign params =
    verify (params.id = sender);
    set_type params.contractName string;
    verify (params.contractName = (Map.get data.multisigs params.contractId).name);
    set_type (Map.get data.multisigs params.contractId).weight int;
    set_type (Map.get data.multisigs params.contractId).groupsOK int;
    List.iter (fun group ->
      List.iter (fun participant ->
        if participant.id = params.id then
          (
            verify (not participant.hasVoted);
            participant.hasVoted <- true;
            set_type group.weight int;
            group.weight <- group.weight + participant.weight;
            group.voters <- group.voters + 1;
            if ((not group.ok) && (group.thresholdVoters <= group.voters)) && (group.thresholdWeight <= group.weight) then
              (
                group.ok <- true;
                (Map.get data.multisigs params.contractId).weight <- (Map.get data.multisigs params.contractId).weight + group.contractWeight;
                (Map.get data.multisigs params.contractId).groupsOK <- (Map.get data.multisigs params.contractId).groupsOK + (int 1);
                if ((not (Map.get data.multisigs params.contractId).ok) && ((Map.get data.multisigs params.contractId).thresholdGroupsOK <= (Map.get data.multisigs params.contractId).groupsOK)) && ((Map.get data.multisigs params.contractId).thresholdWeight <= (Map.get data.multisigs params.contractId).weight) then
                  (Map.get data.multisigs params.contractId).ok <- true
              )
          )
      ) group.participants
    ) (Map.get data.multisigs params.contractId).groups

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {multisigs = big_map intOrNat {groups = list {contractWeight = int; ok = bool; participants = list {hasVoted = bool; id = address; weight = int}; thresholdVoters = intOrNat; thresholdWeight = int; voters = intOrNat; weight = int}; groupsOK = int; name = string; ok = bool; thresholdGroupsOK = int; thresholdWeight = int; weight = int}; nbMultisigs = intOrNat}]
      ~storage:[%expr
                 {multisigs = Map.make [];
                  nbMultisigs = 0}]
      [build; sign]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())