open SmartML

module Contract = struct
  let%entry_point a params =
    data <- params

  let%entry_point b params =
    data.address <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {address = option int}]
      [a; b]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())