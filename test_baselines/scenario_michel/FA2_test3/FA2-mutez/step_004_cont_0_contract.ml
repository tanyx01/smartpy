open SmartML

module Contract = struct
  let%entry_point balance_of params =
    verify (not data.paused) ~msg:"FA2_PAUSED";
    set_type params {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}};
    let%mutable responses = (map (fun _x0 -> verify (contains _x0.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
if contains (set_type_expr _x0.owner address, set_type_expr _x0.token_id nat) data.ledger then
  result {request = {owner = (set_type_expr _x0.owner address); token_id = (set_type_expr _x0.token_id nat)}; balance = (Map.get data.ledger (set_type_expr _x0.owner address, set_type_expr _x0.token_id nat)).balance}
else
  result {request = {owner = (set_type_expr _x0.owner address); token_id = (set_type_expr _x0.token_id nat)}; balance = (nat 0)}) params.requests) in ();
    transfer responses (tez 0) (set_type_expr params.callback (contract (list {balance = nat; request = {owner = address; token_id = nat}})))

  let%entry_point mint params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    if contains (set_type_expr params.address address, set_type_expr params.token_id nat) data.ledger then
      (Map.get data.ledger (set_type_expr params.address address, set_type_expr params.token_id nat)).balance <- (Map.get data.ledger (set_type_expr params.address address, set_type_expr params.token_id nat)).balance + params.amount
    else
      Map.set data.ledger (set_type_expr params.address address, set_type_expr params.token_id nat) {balance = params.amount};
    if not (params.token_id < data.all_tokens) then
      (
        verify (data.all_tokens = params.token_id) ~msg:"Token-IDs should be consecutive";
        data.all_tokens <- params.token_id + (nat 1);
        Map.set data.token_metadata params.token_id {token_id = params.token_id; token_info = params.metadata}
      );
    Map.set data.total_supply params.token_id (params.amount + (Map.get ~default_value:(nat 0) data.total_supply params.token_id))

  let%entry_point mutez_transfer params =
    verify (sender = data.administrator);
    set_type params.destination address;
    set_type params.amount mutez;
    send params.destination params.amount

  let%entry_point set_administrator params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    data.administrator <- params

  let%entry_point set_metadata params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    Map.set data.metadata params.k params.v

  let%entry_point set_pause params =
    verify (sender = data.administrator) ~msg:"FA2_NOT_ADMIN";
    data.paused <- params

  let%entry_point transfer params =
    verify (not data.paused) ~msg:"FA2_PAUSED";
    set_type params (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}});
    List.iter (fun transfer ->
      List.iter (fun tx ->
        verify (((sender = data.administrator) || (transfer.from_ = sender)) || (contains (set_type_expr {owner = transfer.from_; operator = sender; token_id = tx.token_id} {operator = address; owner = address; token_id = nat}) data.operators)) ~msg:"FA2_NOT_OPERATOR";
        verify (contains tx.token_id data.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
        if tx.amount > (nat 0) then
          (
            verify ((Map.get data.ledger (set_type_expr transfer.from_ address, set_type_expr tx.token_id nat)).balance >= tx.amount) ~msg:"FA2_INSUFFICIENT_BALANCE";
            (Map.get data.ledger (set_type_expr transfer.from_ address, set_type_expr tx.token_id nat)).balance <- open_some (is_nat ((Map.get data.ledger (set_type_expr transfer.from_ address, set_type_expr tx.token_id nat)).balance - tx.amount));
            if contains (set_type_expr tx.to_ address, set_type_expr tx.token_id nat) data.ledger then
              (Map.get data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat)).balance <- (Map.get data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat)).balance + tx.amount
            else
              Map.set data.ledger (set_type_expr tx.to_ address, set_type_expr tx.token_id nat) {balance = tx.amount}
          )
      ) transfer.txs
    ) params

  let%entry_point update_operators params =
    set_type params (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat}));
    List.iter (fun update ->
      match update with
        | `add_operator add_operator ->
          verify ((add_operator.owner = sender) || (sender = data.administrator)) ~msg:"FA2_NOT_ADMIN_OR_OPERATOR";
          Map.set data.operators (set_type_expr {owner = add_operator.owner; operator = add_operator.operator; token_id = add_operator.token_id} {operator = address; owner = address; token_id = nat}) ()
        | `remove_operator remove_operator ->
          verify ((remove_operator.owner = sender) || (sender = data.administrator)) ~msg:"FA2_NOT_ADMIN_OR_OPERATOR";
          Map.delete data.operators (set_type_expr {owner = remove_operator.owner; operator = remove_operator.operator; token_id = remove_operator.token_id} {operator = address; owner = address; token_id = nat})

    ) params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {administrator = address; all_tokens = nat; ledger = big_map (pair address nat) {balance = nat}; metadata = big_map string bytes; operators = big_map {operator = address; owner = address; token_id = nat} unit; paused = bool; token_metadata = big_map nat {token_id = nat; token_info = map string bytes}; total_supply = big_map nat nat}]
      ~storage:[%expr
                 {administrator = address "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w";
                  all_tokens = nat 0;
                  ledger = Map.make [];
                  metadata = Map.make [("", bytes "0x68747470733a2f2f6578616d706c652e636f6d")];
                  operators = Map.make [];
                  paused = false;
                  token_metadata = Map.make [];
                  total_supply = Map.make []}]
      [balance_of; mint; mutez_transfer; set_administrator; set_metadata; set_pause; transfer; update_operators]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())