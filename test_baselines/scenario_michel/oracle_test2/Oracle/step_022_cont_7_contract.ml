open SmartML

module Contract = struct
  let%entry_point set_value params =
    verify (sender = data.escrow) ~msg:"ReceiverNotEscrow";
    set_type params {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}};
    request_oracle_495, result_oracle_495 = match_record(params, "request", "result")
    ticket_oracle_497_data, ticket_oracle_497_copy = match_tuple(read_ticket_raw request_oracle_495, "ticket_oracle_497_data", "ticket_oracle_497_copy")
    ticket_oracle_497_ticketer, ticket_oracle_497_content, ticket_oracle_497_amount = match_tuple(ticket_oracle_497_data, "ticket_oracle_497_ticketer", "ticket_oracle_497_content", "ticket_oracle_497_amount")
    ticket_oracle_498_data, ticket_oracle_498_copy = match_tuple(read_ticket_raw result_oracle_495, "ticket_oracle_498_data", "ticket_oracle_498_copy")
    ticket_oracle_498_ticketer, ticket_oracle_498_content, ticket_oracle_498_amount = match_tuple(ticket_oracle_498_data, "ticket_oracle_498_ticketer", "ticket_oracle_498_content", "ticket_oracle_498_amount")
    verify (ticket_oracle_497_ticketer = data.requester) ~msg:"ReceiverBadRequester";
    verify (ticket_oracle_497_content.client_request_id > data.last_request_id) ~msg:"ReceiverBadRequestId";
    data.last_request_id <- ticket_oracle_497_content.client_request_id;
    data.value <- some (open_variant ticket_oracle_498_content.result int)

  let%entry_point setup params =
    verify (sender = data.admin) ~msg:"ReceiverNotAdmin";
    data.requester <- params.requester;
    data.escrow <- params.escrow

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admin = address; escrow = address; last_request_id = nat; requester = address; value = option int}]
      ~storage:[%expr
                 {admin = address "tz1hzpcBRF2UGudeBX7d59VevQea2yQWazHT";
                  escrow = address "KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H";
                  last_request_id = nat 0;
                  requester = address "KT1Tezooo5zzSmartPyzzSTATiCzzzz48Z4p";
                  value = None}]
      [set_value; setup]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())