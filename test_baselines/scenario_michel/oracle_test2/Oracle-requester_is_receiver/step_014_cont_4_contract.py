import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, escrow = sp.TAddress, job_id = sp.TBytes, next_request_id = sp.TNat, oracle = sp.TAddress, token = sp.TAddress, value = sp.TOption(sp.TInt)).layout(("admin", ("escrow", ("job_id", ("next_request_id", ("oracle", ("token", "value"))))))))
    self.init(admin = sp.address('tz1NiCaziaWuZWMTYuiXX6ceM5A42Ckd6yop'),
              escrow = sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H'),
              job_id = sp.bytes('0x0001'),
              next_request_id = 1,
              oracle = sp.address('KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC'),
              token = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
              value = sp.none)

  @sp.entry_point
  def add_remove_escrow_operator(self, params):
    sp.if params.add_operator:
      sp.transfer(sp.list([variant('add_operator', sp.set_type_expr(sp.record(owner = sp.self_address, operator = self.data.escrow, token_id = 0), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))))]), sp.tez(0), sp.contract(sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))), params.token, entry_point='update_operators').open_some())
    sp.else:
      sp.transfer(sp.list([variant('remove_operator', sp.set_type_expr(sp.record(owner = sp.self_address, operator = self.data.escrow, token_id = 0), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))))]), sp.tez(0), sp.contract(sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))), params.token, entry_point='update_operators').open_some())

  @sp.entry_point
  def cancel_value(self, params):
    sp.verify(sp.sender == self.data.admin, 'RequesterNotAdmin')
    sp.transfer(sp.record(client_request_id = sp.as_nat(self.data.next_request_id - 1), force = params.force, oracle = self.data.oracle), sp.tez(0), sp.contract(sp.TRecord(client_request_id = sp.TNat, force = sp.TBool, oracle = sp.TAddress).layout(("client_request_id", ("force", "oracle"))), self.data.escrow, entry_point='cancel_request').open_some())

  @sp.entry_point
  def request_value(self, params):
    sp.verify(sp.sender == self.data.admin, 'RequesterNotAdmin')
    ticket_431 = sp.local("ticket_431", sp.ticket(sp.record(cancel_timeout = sp.add_seconds(sp.now, params.cancel_timeout_minutes * 60), client_request_id = self.data.next_request_id, fulfill_timeout = sp.add_seconds(sp.now, params.fulfill_timeout_minutes * 60), job_id = self.data.job_id, oracle = self.data.oracle, parameters = params.parameters, tag = 'OracleRequest', target = sp.self_entry_point_address('set_value')), 1))
    sp.transfer(sp.record(amount = params.amount, request = ticket_431.value), sp.tez(0), sp.contract(sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))))).layout(("amount", "request")), self.data.escrow, entry_point='send_request').open_some())
    self.data.next_request_id += 1

  @sp.entry_point
  def set_value(self, params):
    sp.verify(sp.sender == self.data.escrow, 'ReceiverNotEscrow')
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")))
    request_oracle_495, result_oracle_495 = sp.match_record(params, "request", "result")
    ticket_oracle_497_data, ticket_oracle_497_copy = sp.match_tuple(sp.read_ticket_raw(request_oracle_495), "ticket_oracle_497_data", "ticket_oracle_497_copy")
    ticket_oracle_497_ticketer, ticket_oracle_497_content, ticket_oracle_497_amount = sp.match_tuple(ticket_oracle_497_data, "ticket_oracle_497_ticketer", "ticket_oracle_497_content", "ticket_oracle_497_amount")
    ticket_oracle_498_data, ticket_oracle_498_copy = sp.match_tuple(sp.read_ticket_raw(result_oracle_495), "ticket_oracle_498_data", "ticket_oracle_498_copy")
    ticket_oracle_498_ticketer, ticket_oracle_498_content, ticket_oracle_498_amount = sp.match_tuple(ticket_oracle_498_data, "ticket_oracle_498_ticketer", "ticket_oracle_498_content", "ticket_oracle_498_amount")
    sp.verify(ticket_oracle_497_ticketer == sp.self_address, 'ReceiverBadRequester')
    sp.verify(ticket_oracle_497_content.client_request_id == sp.as_nat(self.data.next_request_id - 1), 'ReceiverBadRequester')
    self.data.value = sp.some(ticket_oracle_498_content.result.open_variant('int'))

  @sp.entry_point
  def setup(self, params):
    sp.verify(sp.sender == self.data.admin, 'RequesterNotAdmin')
    self.data.admin = params.admin
    self.data.escrow = params.escrow
    self.data.oracle = params.oracle
    self.data.job_id = params.job_id
    self.data.token = params.token

sp.add_compilation_target("test", Contract())