open SmartML

module Contract = struct
  let%entry_point add_remove_escrow_operator params =
    if params.add_operator then
      transfer [variant add_operator (set_type_expr {owner = self_address; operator = data.escrow; token_id = (nat 0)} {operator = address; owner = address; token_id = nat})] (tez 0) (open_some (contract (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat})) params.token , entry_point='update_operators'))
    else
      transfer [variant remove_operator (set_type_expr {owner = self_address; operator = data.escrow; token_id = (nat 0)} {operator = address; owner = address; token_id = nat})] (tez 0) (open_some (contract (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat})) params.token , entry_point='update_operators'))

  let%entry_point cancel_value params =
    verify (sender = data.admin) ~msg:"RequesterNotAdmin";
    transfer {client_request_id = (open_some (is_nat (data.next_request_id - (nat 1)))); force = params.force; oracle = data.oracle} (tez 0) (open_some (contract {client_request_id = nat; force = bool; oracle = address} data.escrow , entry_point='cancel_request'))

  let%entry_point request_value params =
    verify (sender = data.admin) ~msg:"RequesterNotAdmin";
    let%mutable ticket_431 = (ticket {cancel_timeout = (add_seconds now (params.cancel_timeout_minutes * (int 60))); client_request_id = data.next_request_id; fulfill_timeout = (add_seconds now (params.fulfill_timeout_minutes * (int 60))); job_id = data.job_id; oracle = data.oracle; parameters = params.parameters; tag = "OracleRequest"; target = (self_entry_point_address set_value)} (nat 1)) in ();
    transfer {amount = params.amount; request = ticket_431} (tez 0) (open_some (contract {amount = nat; request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}} data.escrow , entry_point='send_request'));
    data.next_request_id <- data.next_request_id + (nat 1)

  let%entry_point set_value params =
    verify (sender = data.escrow) ~msg:"ReceiverNotEscrow";
    set_type params {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}};
    request_oracle_495, result_oracle_495 = match_record(params, "request", "result")
    ticket_oracle_497_data, ticket_oracle_497_copy = match_tuple(read_ticket_raw request_oracle_495, "ticket_oracle_497_data", "ticket_oracle_497_copy")
    ticket_oracle_497_ticketer, ticket_oracle_497_content, ticket_oracle_497_amount = match_tuple(ticket_oracle_497_data, "ticket_oracle_497_ticketer", "ticket_oracle_497_content", "ticket_oracle_497_amount")
    ticket_oracle_498_data, ticket_oracle_498_copy = match_tuple(read_ticket_raw result_oracle_495, "ticket_oracle_498_data", "ticket_oracle_498_copy")
    ticket_oracle_498_ticketer, ticket_oracle_498_content, ticket_oracle_498_amount = match_tuple(ticket_oracle_498_data, "ticket_oracle_498_ticketer", "ticket_oracle_498_content", "ticket_oracle_498_amount")
    verify (ticket_oracle_497_ticketer = self_address) ~msg:"ReceiverBadRequester";
    verify (ticket_oracle_497_content.client_request_id = (open_some (is_nat (data.next_request_id - (nat 1))))) ~msg:"ReceiverBadRequester";
    data.value <- some (open_variant ticket_oracle_498_content.result int)

  let%entry_point setup params =
    verify (sender = data.admin) ~msg:"RequesterNotAdmin";
    data.admin <- params.admin;
    data.escrow <- params.escrow;
    data.oracle <- params.oracle;
    data.job_id <- params.job_id;
    data.token <- params.token

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admin = address; escrow = address; job_id = bytes; next_request_id = nat; oracle = address; token = address; value = option int}]
      ~storage:[%expr
                 {admin = address "tz1NiCaziaWuZWMTYuiXX6ceM5A42Ckd6yop";
                  escrow = address "KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H";
                  job_id = bytes "0x0001";
                  next_request_id = nat 1;
                  oracle = address "KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC";
                  token = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  value = None}]
      [add_remove_escrow_operator; cancel_value; request_value; set_value; setup]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())