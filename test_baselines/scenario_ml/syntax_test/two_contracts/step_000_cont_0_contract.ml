open SmartML

module Contract = struct
  let%entry_point replace params =
    set_type params {value = nat};
    set_type params {value = nat};
    set_type params.value nat;
    data.storedValue <- set_type_expr params.value nat

  let%entry_point sum params =
    set_type params {bar = {toto = nat}; foo = nat};
    set_type params.foo nat;
    set_type params.bar {toto = nat};
    set_type params.bar.toto nat;
    data.storedValue <- params.foo + params.bar.toto

  let%entry_point double () =
    data.storedValue <- data.storedValue * (nat 2);
    ()

  let%entry_point divide params =
    set_type params {divisor = nat};
    set_type params {divisor = nat};
    verify (params.divisor > (nat 5)) ~msg:"params.divisor too small";
    verify (params.divisor > (nat 6)) ~msg:42;
    data.storedValue <- data.storedValue / params.divisor

  let%entry_point misc params =
    set_type params.divisor int;
    set_type params.bar (`A nat + `B nat + `c never);
    set_type params.bar2 (`A nat + `B (`D never + `c nat));
    set_type params.bar3 (pair nat nat);
    verify ((fst params.bar3) = (snd params.bar3)) ~msg:"not equal";
    verify (params.divisor > (int 5)) ~msg:"params.divisor too small";
    verify (params.divisor > (int 6)) ~msg:42;
    trace params.divisor;
    match params.bar with
      | `A x ->
        failwith ((nat 1) + x)
      | `B x ->
        data.storedValue <- x
      | `c n ->
        never n
;
    if (2 + params.foo) < 42 then
      failwith params;
    if (1 + params.foo) < 14 then
      if (2 + params.foo) < 42 then
        failwith params
      else
        failwith "abc";
    while data.storedValue < (nat 15) do
      data.storedValue <- (nat 2) * data.storedValue
    done;
    List.iter (fun x ->
      data.storedValue <- data.storedValue + x
    ) params.l

  let%entry_point another () =
    data.storedValue <- data.storedValue * (nat 42)

  let%entry_point another_with_parameter params =
    data.storedValue <- data.storedValue * params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {storedValue = nat}]
      ~storage:[%expr
                 {storedValue = nat 123456}]
      [replace; sum; double; divide; misc; another; another_with_parameter]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())