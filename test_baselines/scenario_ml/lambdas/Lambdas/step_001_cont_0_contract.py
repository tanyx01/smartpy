import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(abcd = sp.TInt, value = sp.TIntOrNat).layout(("abcd", "value")))
    self.init(abcd = 0,
              value = 0)

  @sp.entry_point
  def f(self):
    toto = sp.local("toto", sp.build_lambda(lambda x: sp.fst(x.value) + sp.snd(x.value)))
    titi = sp.local("titi", toto.value.apply(5))
    self.data.value = titi.value(8)

  @sp.entry_point
  def abs_test(self, params):
    self.data.abcd = self.abs(params)

  @sp.private_lambda()
  def flam(x):
    sp.result(322 * x.value)

  @sp.private_lambda()
  def square_root(x):
    sp.verify(x.value >= 0, 'negative')
    y = sp.local("y", x.value)
    sp.while (y.value * y.value) > x.value:
      y.value = ((x.value // y.value) + y.value) // 2
    sp.result(y.value)

  @sp.private_lambda()
  def abs(x):
    sp.if x.value > 0:
      sp.result(x.value)
    sp.else:
      sp.result(- x.value)

sp.add_compilation_target("test", Contract())