open SmartML

module Contract = struct
  let%entry_point f () =
    let toto = (fun x -> result ((fst x) + (snd x))) in ();
    let titi = (apply_lambda 5 toto) in ();
    data.value <- 8 titi

  let%entry_point abs_test params =
    data.abcd <- params self.abs

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {abcd = int; value = intOrNat}]
      ~storage:[%expr
                 {abcd = int 0;
                  value = 0}]
      [f; abs_test]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())