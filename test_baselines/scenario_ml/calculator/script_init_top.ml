#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/calculator.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/scenario_ml/calculator/scenario.json"
else
  exit 1;;