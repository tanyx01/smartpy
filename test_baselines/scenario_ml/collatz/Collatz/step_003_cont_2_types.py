import smartpy as sp

tstorage = sp.TRecord(counter = sp.TIntOrNat, on_even = sp.TAddress, on_odd = sp.TAddress).layout(("counter", ("on_even", "on_odd")))
tparameter = sp.TVariant(reset = sp.TUnit, run = sp.TNat).layout(("reset", "run"))
tprivates = { }
tviews = { }
