open SmartML

module Contract = struct
  let%entry_point run params =
    if params > (nat 1) then
      (
        data.counter <- data.counter + 1;
        let p = {k = (self_entry_point "run"); x = params} in ();
        if (params % (nat 2)) = (nat 0) then
          transfer p (tez 0) (open_some (contract {k = contract nat; x = nat} data.on_even ))
        else
          transfer p (tez 0) (open_some (contract {k = contract nat; x = nat} data.on_odd ))
      )

  let%entry_point reset () =
    data.counter <- 0

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {counter = intOrNat; on_even = address; on_odd = address}]
      ~storage:[%expr
                 {counter = 0;
                  on_even = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  on_odd = address "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"}]
      [run; reset]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())