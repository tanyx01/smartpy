Comment...
 h1: Tic-Tac-Toe
Comment...
 h2: A sequence of interactions with a winner
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair 0 (Pair 1 0))))
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_002_cont_0_storage.json 41
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_002_cont_0_storage.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_002_cont_0_types.py 7
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_002_cont_0_contract.tz 769
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_002_cont_0_contract.json 546
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_002_cont_0_contract.py 33
Comment...
 h2: Message execution
Comment...
 h3: A first move in the center
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_005_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_005_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_005_cont_0_params.json 1
Executing play(sp.record(i = 1, j = 1, move = 1))...
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair 1 (Pair 2 0))))
Comment...
 h3: A forbidden move
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_007_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_007_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_007_cont_0_params.json 1
Executing play(sp.record(i = 1, j = 1, move = 2))...
 -> --- Expected failure in transaction --- Wrong condition: (self.data.deck[params.i][params.j] == 0 : sp.TBool) (python/templates/tictactoe.py, line 19)
 (python/templates/tictactoe.py, line 19)
Comment...
 h3: A second move
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_009_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_009_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_009_cont_0_params.json 1
Executing play(sp.record(i = 1, j = 2, move = 2))...
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair 2 (Pair 1 0))))
Comment...
 h3: Other moves
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_011_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_011_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_011_cont_0_params.json 1
Executing play(sp.record(i = 2, j = 1, move = 1))...
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 1; Elt 2 0}} (Pair False (Pair 3 (Pair 2 0))))
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_012_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_012_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_012_cont_0_params.json 1
Executing play(sp.record(i = 2, j = 2, move = 2))...
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 1; Elt 2 2}} (Pair False (Pair 4 (Pair 1 0))))
Verifying sp.contract_data(0).winner == 0...
 OK
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_014_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_014_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_014_cont_0_params.json 1
Executing play(sp.record(i = 0, j = 1, move = 1))...
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 1; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 1; Elt 2 2}} (Pair False (Pair 5 (Pair 2 1))))
Verifying sp.contract_data(0).winner == 1...
 OK
Comment...
 p: Player1 has won
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_017_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_017_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_017_cont_0_params.json 1
Executing play(sp.record(i = 0, j = 0, move = 2))...
 -> --- Expected failure in transaction --- Wrong condition: ((self.data.winner == 0) & (~ self.data.draw) : sp.TBool) (python/templates/tictactoe.py, line 15)
 (python/templates/tictactoe.py, line 15)
Comment...
 h2: A sequence of interactions with a draw
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair 0 (Pair 1 0))))
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_019_cont_1_storage.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_019_cont_1_storage.json 41
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_019_cont_1_sizes.csv 2
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_019_cont_1_storage.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_019_cont_1_types.py 7
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_019_cont_1_contract.tz 769
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_019_cont_1_contract.json 546
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_019_cont_1_contract.py 33
Comment...
 h2: Message execution
Comment...
 h3: A first move in the center
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_022_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_022_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_022_cont_1_params.json 1
Executing play(sp.record(i = 1, j = 1, move = 1))...
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair 1 (Pair 2 0))))
Comment...
 h3: A forbidden move
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_024_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_024_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_024_cont_1_params.json 1
Executing play(sp.record(i = 1, j = 1, move = 2))...
 -> --- Expected failure in transaction --- Wrong condition: (self.data.deck[params.i][params.j] == 0 : sp.TBool) (python/templates/tictactoe.py, line 19)
 (python/templates/tictactoe.py, line 19)
Comment...
 h3: A second move
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_026_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_026_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_026_cont_1_params.json 1
Executing play(sp.record(i = 1, j = 2, move = 2))...
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair 2 (Pair 1 0))))
Comment...
 h3: Other moves
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_028_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_028_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_028_cont_1_params.json 1
Executing play(sp.record(i = 2, j = 1, move = 1))...
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 1; Elt 2 0}} (Pair False (Pair 3 (Pair 2 0))))
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_029_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_029_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_029_cont_1_params.json 1
Executing play(sp.record(i = 2, j = 2, move = 2))...
 -> (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 1; Elt 2 2}} (Pair False (Pair 4 (Pair 1 0))))
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_030_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_030_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_030_cont_1_params.json 1
Executing play(sp.record(i = 0, j = 0, move = 1))...
 -> (Pair {Elt 0 {Elt 0 1; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 1; Elt 2 2}} (Pair False (Pair 5 (Pair 2 0))))
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_031_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_031_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_031_cont_1_params.json 1
Executing play(sp.record(i = 0, j = 1, move = 2))...
 -> (Pair {Elt 0 {Elt 0 1; Elt 1 2; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 1; Elt 2 2}} (Pair False (Pair 6 (Pair 1 0))))
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_032_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_032_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_032_cont_1_params.json 1
Executing play(sp.record(i = 0, j = 2, move = 1))...
 -> (Pair {Elt 0 {Elt 0 1; Elt 1 2; Elt 2 1}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 0; Elt 1 1; Elt 2 2}} (Pair False (Pair 7 (Pair 2 0))))
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_033_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_033_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_033_cont_1_params.json 1
Executing play(sp.record(i = 2, j = 0, move = 2))...
 -> (Pair {Elt 0 {Elt 0 1; Elt 1 2; Elt 2 1}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 2; Elt 1 1; Elt 2 2}} (Pair False (Pair 8 (Pair 1 0))))
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_034_cont_1_params.py 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_034_cont_1_params.tz 1
 => test_baselines/scenario_via_js/tictactoe/TicTacToe/step_034_cont_1_params.json 1
Executing play(sp.record(i = 1, j = 0, move = 1))...
 -> (Pair {Elt 0 {Elt 0 1; Elt 1 2; Elt 2 1}; Elt 1 {Elt 0 1; Elt 1 1; Elt 2 2}; Elt 2 {Elt 0 2; Elt 1 1; Elt 2 2}} (Pair True (Pair 9 (Pair 2 0))))
