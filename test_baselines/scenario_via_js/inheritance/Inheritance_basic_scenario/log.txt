Comment...
 h1: Basic scenario.
Comment...
 h2: Origination.
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair 31622400 (Pair "tz1YWjrGm1iz9xzt3HoVaoqu1Y7UvC164GKf" (Pair "tz1cjpubPzgzeFbLo45hwHxMiuKqVNuhYeSc" "1971-01-02T00:00:00Z")))
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_002_cont_0_storage.json 13
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_002_cont_0_storage.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_002_cont_0_types.py 7
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_002_cont_0_contract.tz 103
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_002_cont_0_contract.json 126
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_002_cont_0_contract.py 25
Comment...
 h2: Deposit.
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_004_cont_0_params.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_004_cont_0_params.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_004_cont_0_params.json 1
Executing default(sp.record())...
 -> (Pair 31622400 (Pair "tz1YWjrGm1iz9xzt3HoVaoqu1Y7UvC164GKf" (Pair "tz1cjpubPzgzeFbLo45hwHxMiuKqVNuhYeSc" "1971-01-02T00:00:00Z")))
Verifying sp.contract_balance(0) == sp.tez(1200)...
 OK
Verifying sp.contract_data(0).timeout == sp.add_seconds(sp.timestamp(0), 31622400)...
 OK
Comment...
 h2: Owner withdraw.
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_008_cont_0_params.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_008_cont_0_params.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_008_cont_0_params.json 1
Executing withdraw(sp.record(amount = sp.tez(200), receiver = sp.resolve(sp.test_account("owner").address)))...
 -> (Pair 31622400 (Pair "tz1YWjrGm1iz9xzt3HoVaoqu1Y7UvC164GKf" (Pair "tz1cjpubPzgzeFbLo45hwHxMiuKqVNuhYeSc" "1971-01-02T00:00:00Z")))
  + Transfer
     params: sp.unit
     amount: sp.tez(200)
     to:     sp.contract(sp.TUnit, sp.address('tz1cjpubPzgzeFbLo45hwHxMiuKqVNuhYeSc')).open_some()
Comment...
 h2: Alive confirmation.
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_010_cont_0_params.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_010_cont_0_params.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_010_cont_0_params.json 1
Executing default(sp.record())...
 -> (Pair 31622400 (Pair "tz1YWjrGm1iz9xzt3HoVaoqu1Y7UvC164GKf" (Pair "tz1cjpubPzgzeFbLo45hwHxMiuKqVNuhYeSc" "1971-12-28T00:01:00Z")))
Comment...
 h2: Heir withdraw.
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_012_cont_0_params.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_012_cont_0_params.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_basic_scenario/step_012_cont_0_params.json 1
Executing withdraw(sp.record(amount = sp.contract_balance(0), receiver = sp.resolve(sp.test_account("heir").address)))...
 -> (Pair 31622400 (Pair "tz1YWjrGm1iz9xzt3HoVaoqu1Y7UvC164GKf" (Pair "tz1cjpubPzgzeFbLo45hwHxMiuKqVNuhYeSc" "1971-12-28T00:01:00Z")))
  + Transfer
     params: sp.unit
     amount: sp.tez(1000)
     to:     sp.contract(sp.TUnit, sp.address('tz1YWjrGm1iz9xzt3HoVaoqu1Y7UvC164GKf')).open_some()
