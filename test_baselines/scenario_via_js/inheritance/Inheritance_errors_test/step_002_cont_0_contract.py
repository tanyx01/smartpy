import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(alive_delta = sp.TInt, heir = sp.TAddress, owner = sp.TAddress, timeout = sp.TTimestamp).layout(("alive_delta", ("heir", ("owner", "timeout")))))
    self.init(alive_delta = 31622400,
              heir = sp.address('tz1YWjrGm1iz9xzt3HoVaoqu1Y7UvC164GKf'),
              owner = sp.address('tz1cjpubPzgzeFbLo45hwHxMiuKqVNuhYeSc'),
              timeout = sp.timestamp(31622400))

  @sp.entry_point
  def default(self):
    sp.verify(sp.sender == self.data.owner, 'This entrypoint can only be called by the owner.')
    self.data.timeout = sp.add_seconds(sp.now, self.data.alive_delta)

  @sp.entry_point
  def withdraw(self, params):
    sp.verify(sp.amount == sp.tez(0), 'This entrypoint doesn't accept deposits.')
    sp.if sp.sender == self.data.heir:
      sp.verify(sp.now > self.data.timeout, 'The owner is still considered alive, you cannot withdraw.')
    sp.else:
      sp.verify(sp.sender == self.data.owner, 'Only owner or heir can withdraw.')
    sp.send(params.receiver, params.amount)

sp.add_compilation_target("test", Contract())