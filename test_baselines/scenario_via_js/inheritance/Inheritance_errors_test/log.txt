Comment...
 h1: Errors tests.
Comment...
 h2: Origination.
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair 31622400 (Pair "tz1YWjrGm1iz9xzt3HoVaoqu1Y7UvC164GKf" (Pair "tz1cjpubPzgzeFbLo45hwHxMiuKqVNuhYeSc" "1971-01-02T00:00:00Z")))
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_002_cont_0_storage.json 13
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_002_cont_0_storage.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_002_cont_0_types.py 7
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_002_cont_0_contract.tz 103
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_002_cont_0_contract.json 126
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_002_cont_0_contract.py 25
Comment...
 h2: Default: non-owner calling.
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_004_cont_0_params.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_004_cont_0_params.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_004_cont_0_params.json 1
Executing default(sp.record())...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sender == self.data.owner : sp.TBool) (python/templates/inheritance.py, line 47)
Message: 'This entrypoint can only be called by the owner.'
 (python/templates/inheritance.py, line 46)
Comment...
 h2: Withdraw: not allowed calling.
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_006_cont_0_params.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_006_cont_0_params.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_006_cont_0_params.json 1
Executing withdraw(sp.record(amount = sp.contract_balance(0), receiver = sp.resolve(sp.test_account("not_allowed").address)))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sender == self.data.owner : sp.TBool) (python/templates/inheritance.py, line 74)
Message: 'Only owner or heir can withdraw.'
 (python/templates/inheritance.py, line 74)
Comment...
 h2: Withdraw: sending tez.
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_008_cont_0_params.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_008_cont_0_params.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_008_cont_0_params.json 1
Executing withdraw(sp.record(amount = sp.contract_balance(0), receiver = sp.resolve(sp.test_account("owner").address)))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.amount == sp.tez(0) : sp.TBool) (python/templates/inheritance.py, line 67)
Message: 'This entrypoint doesn't accept deposits.'
 (python/templates/inheritance.py, line 67)
Comment...
 h2: Withdraw: heir withdraw before timeout.
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_010_cont_0_params.py 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_010_cont_0_params.tz 1
 => test_baselines/scenario_via_js/inheritance/Inheritance_errors_test/step_010_cont_0_params.json 1
Executing withdraw(sp.record(amount = sp.contract_balance(0), receiver = sp.resolve(sp.test_account("heir").address)))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.now > self.data.timeout : sp.TBool) (python/templates/inheritance.py, line 70)
Message: 'The owner is still considered alive, you cannot withdraw.'
 (python/templates/inheritance.py, line 69)
