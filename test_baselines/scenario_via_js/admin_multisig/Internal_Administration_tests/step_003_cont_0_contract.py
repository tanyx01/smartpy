import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(activeProposals = sp.TSet(sp.TNat), lastProposalId = sp.TNat, metadata = sp.TBigMap(sp.TString, sp.TBytes), proposals = sp.TBigMap(sp.TNat, sp.TRecord(actions = sp.TVariant(external = sp.TList(sp.TRecord(actions = sp.TBytes, target = sp.TAddress).layout(("actions", "target"))), internal = sp.TList(sp.TVariant(changeMetadata = sp.TPair(sp.TString, sp.TOption(sp.TBytes)), changeQuorum = sp.TNat, changeSigners = sp.TVariant(added = sp.TList(sp.TRecord(address = sp.TAddress, publicKey = sp.TKey).layout(("address", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout(("changeMetadata", ("changeQuorum", "changeSigners"))))).layout(("external", "internal")), endorsements = sp.TSet(sp.TAddress), initiator = sp.TAddress, startedAt = sp.TTimestamp).layout(("actions", ("endorsements", ("initiator", "startedAt"))))), quorum = sp.TNat, signers = sp.TMap(sp.TAddress, sp.TRecord(lastProposalId = sp.TOption(sp.TNat), publicKey = sp.TKey).layout(("lastProposalId", "publicKey")))).layout(("activeProposals", ("lastProposalId", ("metadata", ("proposals", ("quorum", "signers")))))))
    self.init(activeProposals = sp.set([]),
              lastProposalId = 0,
              metadata = {'' : sp.bytes('0x697066733a2f2f')},
              proposals = {},
              quorum = 1,
              signers = {sp.address('tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT') : sp.record(lastProposalId = sp.none, publicKey = sp.key('edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd')), sp.address('tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR') : sp.record(lastProposalId = sp.none, publicKey = sp.key('edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe'))})

  @sp.entry_point
  def aggregated_endorsement(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(proposalId = sp.TNat, signatures = sp.TList(sp.TRecord(signature = sp.TSignature, signerAddress = sp.TAddress).layout(("signature", "signerAddress")))).layout(("proposalId", "signatures"))))
    sp.for endorsment in params:
      sp.for signature in endorsment.signatures:
        sp.verify(self.data.signers.contains(signature.signerAddress), 'MULTISIG_SignerUnknown')
        sp.verify(sp.check_signature(self.data.signers[signature.signerAddress].publicKey, signature.signature, sp.pack(sp.record(contractAddress = sp.self_address, proposalId = endorsment.proposalId))), 'MULTISIG_Badsig')
        compute_admin_multisig_302 = sp.local("compute_admin_multisig_302", self.registerEndorsement(sp.record(proposalId = endorsment.proposalId, signerAddress = signature.signerAddress)))
      compute_admin_multisig_308 = sp.local("compute_admin_multisig_308", self.data.proposals[endorsment.proposalId])
      sp.if sp.len(compute_admin_multisig_308.value.endorsements) >= self.data.quorum:
        compute_admin_multisig_310 = sp.local("compute_admin_multisig_310", self.onApproved(sp.record(actions = compute_admin_multisig_308.value.actions, proposalId = endorsment.proposalId)))

  @sp.entry_point
  def aggregated_proposal(self, params):
    sp.set_type(params, sp.TRecord(actions = sp.TVariant(external = sp.TList(sp.TRecord(actions = sp.TBytes, target = sp.TAddress).layout(("actions", "target"))), internal = sp.TList(sp.TVariant(changeMetadata = sp.TPair(sp.TString, sp.TOption(sp.TBytes)), changeQuorum = sp.TNat, changeSigners = sp.TVariant(added = sp.TList(sp.TRecord(address = sp.TAddress, publicKey = sp.TKey).layout(("address", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout(("changeMetadata", ("changeQuorum", "changeSigners"))))).layout(("external", "internal")), proposalId = sp.TNat, signatures = sp.TList(sp.TRecord(signature = sp.TSignature, signerAddress = sp.TAddress).layout(("signature", "signerAddress")))).layout(("actions", ("proposalId", "signatures"))))
    sp.verify(self.data.signers.contains(sp.sender), 'MULTISIG_SignerUnknown')
    self.data.lastProposalId += 1
    sp.verify(self.data.lastProposalId == params.proposalId, 'MULTISIG_InvalidProposalId')
    compute_admin_multisig_232 = sp.local("compute_admin_multisig_232", sp.record(actions = params.actions, endorsements = sp.set([sp.sender]), initiator = sp.sender, startedAt = sp.now))
    sp.if self.data.signers[sp.sender].lastProposalId != sp.none:
      self.data.activeProposals.remove(self.data.signers[sp.sender].lastProposalId.open_some())
    self.data.signers[sp.sender].lastProposalId = sp.some(params.proposalId)
    self.data.activeProposals.add(params.proposalId)
    self.data.proposals[params.proposalId] = compute_admin_multisig_232.value
    compute_admin_multisig_251 = sp.local("compute_admin_multisig_251", sp.pack(sp.record(actions = params.actions, contractAddress = sp.self_address, proposalId = params.proposalId)))
    sp.for signature in params.signatures:
      sp.verify(self.data.signers.contains(signature.signerAddress), 'MULTISIG_SignerUnknown')
      sp.verify(sp.check_signature(self.data.signers[signature.signerAddress].publicKey, signature.signature, compute_admin_multisig_251.value), 'MULTISIG_Badsig')
      compute_admin_multisig_232.value.endorsements.add(signature.signerAddress)
    sp.if sp.len(compute_admin_multisig_232.value.endorsements) >= self.data.quorum:
      compute_admin_multisig_273 = sp.local("compute_admin_multisig_273", self.onApproved(sp.record(actions = compute_admin_multisig_232.value.actions, proposalId = params.proposalId)))

  @sp.entry_point
  def cancel_proposal(self, params):
    sp.verify(self.data.signers.contains(sp.sender), 'MULTISIG_SignerUnknown')
    sp.verify(self.data.proposals[params].initiator == sp.sender, 'MULTISIG_NotInitiator')
    self.data.activeProposals.remove(params)

  @sp.entry_point
  def endorsement(self, params):
    sp.verify(self.data.signers.contains(sp.sender), 'MULTISIG_SignerUnknown')
    sp.for pId in params:
      compute_admin_multisig_204 = sp.local("compute_admin_multisig_204", self.registerEndorsement(sp.record(proposalId = pId, signerAddress = sp.sender)))
      sp.if sp.len(self.data.proposals[pId].endorsements) >= self.data.quorum:
        compute_admin_multisig_214 = sp.local("compute_admin_multisig_214", self.onApproved(sp.record(actions = self.data.proposals[pId].actions, proposalId = pId)))

  @sp.entry_point
  def proposal(self, params):
    sp.verify(self.data.signers.contains(sp.sender), 'MULTISIG_SignerUnknown')
    sp.if self.data.signers[sp.sender].lastProposalId != sp.none:
      self.data.activeProposals.remove(self.data.signers[sp.sender].lastProposalId.open_some())
    self.data.lastProposalId += 1
    self.data.activeProposals.add(self.data.lastProposalId)
    self.data.proposals[self.data.lastProposalId] = sp.record(actions = params, endorsements = sp.set([sp.sender]), initiator = sp.sender, startedAt = sp.now)
    self.data.signers[sp.sender].lastProposalId = sp.some(self.data.lastProposalId)
    sp.if self.data.quorum < 2:
      compute_admin_multisig_187 = sp.local("compute_admin_multisig_187", self.onApproved(sp.record(actions = params, proposalId = self.data.lastProposalId)))

  @sp.private_lambda()
  def onApproved(_x0):
    with _x0.actions.match_cases() as arg:
      with arg.match('internal') as internal:
        sp.for action in internal:
          with action.match_cases() as arg:
            with arg.match('changeQuorum') as changeQuorum:
              self.data.quorum = changeQuorum
            with arg.match('changeMetadata') as changeMetadata:
              match_pair_admin_multisig_360_fst, match_pair_admin_multisig_360_snd = sp.match_tuple(changeMetadata, "match_pair_admin_multisig_360_fst", "match_pair_admin_multisig_360_snd")
              sp.if match_pair_admin_multisig_360_snd.is_some():
                self.data.metadata[match_pair_admin_multisig_360_fst] = match_pair_admin_multisig_360_snd.open_some()
              sp.else:
                del self.data.metadata[match_pair_admin_multisig_360_fst]
            with arg.match('changeSigners') as changeSigners:
              with changeSigners.match_cases() as arg:
                with arg.match('removed') as removed:
                  sp.for address in removed.elements():
                    sp.if self.data.signers.contains(address):
                      del self.data.signers[address]
                with arg.match('added') as added:
                  sp.for signer in added:
                    self.data.signers[signer.address] = sp.record(lastProposalId = sp.none, publicKey = signer.publicKey)


          sp.verify(self.data.quorum <= sp.len(self.data.signers), 'MULTISIG_MoreQuorumThanSigners')
        self.data.activeProposals = sp.set([])
      with arg.match('external') as external:
        sp.for action in external:
          sp.transfer(action.actions, sp.tez(0), sp.contract(sp.TBytes, action.target).open_some(message = 'MULTISIG_InvalidTarget'))

    self.data.activeProposals.remove(_x0.proposalId)

  @sp.private_lambda()
  def registerEndorsement(_x2):
    sp.verify(self.data.activeProposals.contains(_x2.proposalId), 'MULTISIG_ProposalUnknown')
    self.data.proposals[_x2.proposalId].endorsements.add(_x2.signerAddress)

sp.add_compilation_target("test", Contract())