import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(logic = sp.TLambda(sp.TBytes, sp.TNat), value = sp.TNat).layout(("logic", "value")))
    self.init(logic = sp.build_lambda(lambda _x0: sp.unpack(_x0, sp.TRecord(x = sp.TNat, y = sp.TNat).layout(("x", "y"))).open_some(message = 'Cannot UNPACK').x + sp.unpack(_x0, sp.TRecord(x = sp.TNat, y = sp.TNat).layout(("x", "y"))).open_some(message = 'Cannot UNPACK').y),
              value = 0)

  @sp.entry_point
  def calc(self, params):
    self.data.value = self.data.logic(params)

  @sp.entry_point
  def updateLogic(self, params):
    self.data.logic = params

sp.add_compilation_target("test", Contract())