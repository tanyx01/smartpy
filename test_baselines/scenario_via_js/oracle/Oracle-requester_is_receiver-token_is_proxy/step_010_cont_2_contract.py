import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(locked = sp.TBigMap(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat).layout(("client", "client_request_id")), sp.TRecord(amount = sp.TNat, cancel_timeout = sp.TTimestamp).layout(("amount", "cancel_timeout"))), token = sp.TAddress, token_id = sp.TNat).layout(("locked", ("token", "token_id"))))
    self.init(locked = {},
              token = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
              token_id = 0)

  @sp.entry_point
  def cancel_request(self, params):
    compute_oracle_304 = sp.local("compute_oracle_304", sp.record(client = sp.sender, client_request_id = params.client_request_id))
    sp.verify(self.data.locked.contains(compute_oracle_304.value), 'EscrowRequestIdUnknownForClient')
    sp.verify(sp.now >= self.data.locked[compute_oracle_304.value].cancel_timeout, 'EscrowCantCancelBeforeTimeout')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = sp.sender, token_id = self.data.token_id, amount = self.data.locked[compute_oracle_304.value].amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    del self.data.locked[compute_oracle_304.value]
    sp.if ~ params.force:
      sp.transfer(compute_oracle_304.value, sp.tez(0), sp.contract(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat).layout(("client", "client_request_id")), params.oracle, entry_point='cancel_request').open_some(message = 'EscrowOracleNotFound'))

  @sp.entry_point
  def force_fulfill_request(self, params):
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")))
    request_oracle_324, result_oracle_324 = sp.match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = sp.match_tuple(sp.read_ticket_raw(request_oracle_324), "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = sp.match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = sp.match_tuple(sp.read_ticket_raw(result_oracle_324), "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = sp.match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    compute_oracle_330 = sp.local("compute_oracle_330", sp.record(client = ticket_oracle_325_ticketer, client_request_id = ticket_oracle_325_content.client_request_id))
    sp.verify(self.data.locked.contains(compute_oracle_330.value), 'EscrowRequestUnknown')
    sp.verify(ticket_oracle_325_content.fulfill_timeout >= sp.now, 'EscrowCantFulfillAfterTimeout')
    sp.verify(ticket_oracle_325_content.tag == 'OracleRequest', 'TicketExpectedTag:OracleRequest')
    sp.verify(ticket_oracle_326_content.tag == 'OracleResult', 'TicketExpectedTag:OracleResult')
    sp.verify(ticket_oracle_325_ticketer == ticket_oracle_326_content.client, 'TicketClientNotMatch')
    sp.verify(ticket_oracle_326_ticketer == ticket_oracle_325_content.oracle, 'TicketOracleNotMatch')
    sp.verify(ticket_oracle_325_content.client_request_id == ticket_oracle_326_content.client_request_id, 'TicketClientRequestIdNotMatch')
    sp.verify(sp.sender == ticket_oracle_326_ticketer, 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = ticket_oracle_325_content.oracle, token_id = self.data.token_id, amount = self.data.locked[compute_oracle_330.value].amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    del self.data.locked[compute_oracle_330.value]

  @sp.entry_point
  def fulfill_request(self, params):
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")))
    request_oracle_324, result_oracle_324 = sp.match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = sp.match_tuple(sp.read_ticket_raw(request_oracle_324), "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = sp.match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = sp.match_tuple(sp.read_ticket_raw(result_oracle_324), "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = sp.match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    compute_oracle_330i = sp.local("compute_oracle_330i", sp.record(client = ticket_oracle_325_ticketer, client_request_id = ticket_oracle_325_content.client_request_id))
    sp.verify(self.data.locked.contains(compute_oracle_330i.value), 'EscrowRequestUnknown')
    sp.verify(ticket_oracle_325_content.fulfill_timeout >= sp.now, 'EscrowCantFulfillAfterTimeout')
    sp.verify(ticket_oracle_325_content.tag == 'OracleRequest', 'TicketExpectedTag:OracleRequest')
    sp.verify(ticket_oracle_326_content.tag == 'OracleResult', 'TicketExpectedTag:OracleResult')
    sp.verify(ticket_oracle_325_ticketer == ticket_oracle_326_content.client, 'TicketClientNotMatch')
    sp.verify(ticket_oracle_326_ticketer == ticket_oracle_325_content.oracle, 'TicketOracleNotMatch')
    sp.verify(ticket_oracle_325_content.client_request_id == ticket_oracle_326_content.client_request_id, 'TicketClientRequestIdNotMatch')
    sp.verify(sp.sender == ticket_oracle_326_ticketer, 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = ticket_oracle_325_content.oracle, token_id = self.data.token_id, amount = self.data.locked[compute_oracle_330i.value].amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    del self.data.locked[compute_oracle_330i.value]
    sp.transfer(sp.record(request = ticket_oracle_325_copy, result = ticket_oracle_326_copy), sp.tez(0), sp.contract(sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")), ticket_oracle_325_content.target).open_some(message = 'EscrowTargetNotFound'))

  @sp.entry_point
  def send_request(self, params):
    sp.set_type(params, sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), sender = sp.TAddress).layout(("amount", ("request", "sender"))))
    amount_oracle_270, request_oracle_270, sender_oracle_270 = sp.match_record(params, "amount", "request", "sender")
    ticket_oracle_276_data, ticket_oracle_276_copy = sp.match_tuple(sp.read_ticket_raw(request_oracle_270), "ticket_oracle_276_data", "ticket_oracle_276_copy")
    ticket_oracle_276_ticketer, ticket_oracle_276_content, ticket_oracle_276_amount = sp.match_tuple(ticket_oracle_276_data, "ticket_oracle_276_ticketer", "ticket_oracle_276_content", "ticket_oracle_276_amount")
    sp.verify(sp.sender == self.data.token, 'EscrowSenderNotToken')
    sp.verify(sender_oracle_270 == ticket_oracle_276_ticketer, 'EscrowSenderAndTicketerNotMatch')
    sp.verify(ticket_oracle_276_content.tag == 'OracleRequest', 'TicketExpectedTag:OracleRequest')
    compute_oracle_292 = sp.local("compute_oracle_292", sp.record(client = ticket_oracle_276_ticketer, client_request_id = ticket_oracle_276_content.client_request_id))
    sp.verify(~ (self.data.locked.contains(compute_oracle_292.value)), 'EscrowRequestIdAlreadyKnownForClient')
    self.data.locked[compute_oracle_292.value] = sp.record(amount = amount_oracle_270, cancel_timeout = ticket_oracle_276_content.cancel_timeout)
    sp.transfer(sp.record(amount = amount_oracle_270, request = ticket_oracle_276_copy), sp.tez(0), sp.contract(sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))))).layout(("amount", "request")), ticket_oracle_276_content.oracle, entry_point='create_request').open_some(message = 'EscrowOracleNotFound'))

sp.add_compilation_target("test", Contract())