import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(counterparty = sp.TAddress, epoch = sp.TTimestamp, hashedSecret = sp.TBytes, notional = sp.TMutez, owner = sp.TAddress).layout(("counterparty", ("epoch", ("hashedSecret", ("notional", "owner"))))))
    self.init(counterparty = sp.address('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP'),
              epoch = sp.timestamp(50),
              hashedSecret = sp.bytes('0x1f98c84caf714d00ede5d23142bc166d84f8cd42adc18be22c3d47453853ea49'),
              notional = sp.mutez(12),
              owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'))

  @sp.entry_point
  def allSigned(self):
    sp.verify(self.data.notional != sp.tez(0))
    sp.verify(self.data.owner == sp.sender)
    sp.send(self.data.counterparty, self.data.notional)
    self.data.notional = sp.tez(0)

  @sp.entry_point
  def cancelSwap(self):
    sp.verify(self.data.notional != sp.tez(0))
    sp.verify(self.data.owner == sp.sender)
    sp.verify(self.data.epoch < sp.now)
    sp.send(self.data.owner, self.data.notional)
    self.data.notional = sp.tez(0)

  @sp.entry_point
  def knownSecret(self, params):
    sp.verify(self.data.notional != sp.tez(0))
    sp.verify(self.data.counterparty == sp.sender)
    sp.verify(self.data.hashedSecret == sp.blake2b(params.secret))
    sp.send(self.data.counterparty, self.data.notional)
    self.data.notional = sp.tez(0)

sp.add_compilation_target("test", Contract())