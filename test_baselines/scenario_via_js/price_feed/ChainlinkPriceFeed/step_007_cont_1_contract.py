import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(active = sp.TBool, admin = sp.TAddress, decimals = sp.TNat, latestRoundId = sp.TNat, linkToken = sp.TAddress, maxSubmissions = sp.TNat, metadata = sp.TBigMap(sp.TString, sp.TBytes), minSubmissions = sp.TNat, oraclePayment = sp.TNat, oracles = sp.TMap(sp.TAddress, sp.TRecord(adminAddress = sp.TAddress, endingRound = sp.TNat, lastStartedRound = sp.TNat, startingRound = sp.TNat, withdrawable = sp.TNat).layout(("adminAddress", ("endingRound", ("lastStartedRound", ("startingRound", "withdrawable")))))), previousRoundDetails = sp.TRecord(activeOracles = sp.TSet(sp.TAddress), maxSubmissions = sp.TNat, minSubmissions = sp.TNat, submissions = sp.TMap(sp.TAddress, sp.TNat), timeout = sp.TNat).layout(("activeOracles", ("maxSubmissions", ("minSubmissions", ("submissions", "timeout"))))), recordedFunds = sp.TRecord(allocated = sp.TNat, available = sp.TNat).layout(("allocated", "available")), reportingRoundDetails = sp.TRecord(activeOracles = sp.TSet(sp.TAddress), maxSubmissions = sp.TNat, minSubmissions = sp.TNat, submissions = sp.TMap(sp.TAddress, sp.TNat), timeout = sp.TNat).layout(("activeOracles", ("maxSubmissions", ("minSubmissions", ("submissions", "timeout"))))), reportingRoundId = sp.TNat, restartDelay = sp.TNat, rounds = sp.TBigMap(sp.TNat, sp.TRecord(answer = sp.TNat, answeredInRound = sp.TNat, roundId = sp.TNat, startedAt = sp.TTimestamp, updatedAt = sp.TTimestamp).layout(("answer", ("answeredInRound", ("roundId", ("startedAt", "updatedAt")))))), timeout = sp.TNat).layout(("active", ("admin", ("decimals", ("latestRoundId", ("linkToken", ("maxSubmissions", ("metadata", ("minSubmissions", ("oraclePayment", ("oracles", ("previousRoundDetails", ("recordedFunds", ("reportingRoundDetails", ("reportingRoundId", ("restartDelay", ("rounds", "timeout"))))))))))))))))))
    self.init(active = True,
              admin = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'),
              decimals = 8,
              latestRoundId = 0,
              linkToken = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
              maxSubmissions = 6,
              metadata = {'' : sp.bytes('0x3c55524c3e')},
              minSubmissions = 3,
              oraclePayment = 1,
              oracles = {sp.address('KT1LLTzYhdhxTqKu7ByJz8KaShF6qPTdx5os') : sp.record(adminAddress = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'), endingRound = 4294967295, lastStartedRound = 0, startingRound = 0, withdrawable = 0), sp.address('KT1LhTzYhdhxTqKu7ByJz8KaShF6qPTdx5os') : sp.record(adminAddress = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'), endingRound = 4294967295, lastStartedRound = 0, startingRound = 0, withdrawable = 0), sp.address('KT1P7oeoKWHx5SXt73qpEanzkr8yeEKABqko') : sp.record(adminAddress = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'), endingRound = 4294967295, lastStartedRound = 0, startingRound = 0, withdrawable = 0), sp.address('KT1SCkxmTqTkmc7zoAP5uMYT9rp9iqVVRgdt') : sp.record(adminAddress = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'), endingRound = 4294967295, lastStartedRound = 0, startingRound = 0, withdrawable = 0)},
              previousRoundDetails = sp.record(activeOracles = sp.set([]), maxSubmissions = 0, minSubmissions = 0, submissions = {}, timeout = 0),
              recordedFunds = sp.record(allocated = 0, available = 0),
              reportingRoundDetails = sp.record(activeOracles = sp.set([]), maxSubmissions = 0, minSubmissions = 0, submissions = {}, timeout = 0),
              reportingRoundId = 0,
              restartDelay = 2,
              rounds = {},
              timeout = 10)

  @sp.entry_point
  def administrate(self, params):
    sp.verify(sp.sender == self.data.admin, 'Aggregator_NotAdmin')
    sp.set_type(params, sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeOracles = sp.TRecord(added = sp.TList(sp.TPair(sp.TAddress, sp.TRecord(adminAddress = sp.TAddress, endingRound = sp.TOption(sp.TNat), startingRound = sp.TNat).layout(("adminAddress", ("endingRound", "startingRound"))))), removed = sp.TList(sp.TAddress)).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = sp.TNat, minSubmissions = sp.TNat, oraclePayment = sp.TNat, restartDelay = sp.TNat, timeout = sp.TNat).layout(("maxSubmissions", ("minSubmissions", ("oraclePayment", ("restartDelay", "timeout")))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds")))))
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('changeActive') as changeActive:
          self.data.active = changeActive
        with arg.match('changeAdmin') as changeAdmin:
          self.data.admin = changeAdmin
        with arg.match('updateFutureRounds') as updateFutureRounds:
          sp.verify(sp.sender == self.data.admin, 'Aggregator_NotAdmin')
          compute_price_feed_672 = sp.local("compute_price_feed_672", sp.len(self.data.oracles))
          sp.verify(updateFutureRounds.maxSubmissions >= updateFutureRounds.minSubmissions, 'Aggregator_MaxInferiorToMin')
          sp.verify(compute_price_feed_672.value >= updateFutureRounds.maxSubmissions, 'Aggregator_MaxExceedActive')
          sp.verify((compute_price_feed_672.value == 0) | (compute_price_feed_672.value > updateFutureRounds.restartDelay), 'Aggregator_DelayExceedTotal')
          sp.verify((compute_price_feed_672.value == 0) | (updateFutureRounds.minSubmissions > 0), 'Aggregator_MinSubmissionsTooLow')
          sp.verify(self.data.recordedFunds.available >= ((updateFutureRounds.oraclePayment * compute_price_feed_672.value) * 2), 'Aggregator_InsufficientFundsForPayment')
          self.data.restartDelay = updateFutureRounds.restartDelay
          self.data.minSubmissions = updateFutureRounds.minSubmissions
          self.data.maxSubmissions = updateFutureRounds.maxSubmissions
          self.data.timeout = updateFutureRounds.timeout
          self.data.oraclePayment = updateFutureRounds.oraclePayment
        with arg.match('changeOracles') as changeOracles:
          sp.verify(sp.sender == self.data.admin, 'Aggregator_NotAdmin')
          sp.for oracleAddress in changeOracles.removed:
            del self.data.oracles[oracleAddress]
          sp.for oracle in changeOracles.added:
            match_pair_price_feed_696_fst, match_pair_price_feed_696_snd = sp.match_tuple(oracle, "match_pair_price_feed_696_fst", "match_pair_price_feed_696_snd")
            sp.set_type(match_pair_price_feed_696_snd.endingRound, sp.TOption(sp.TNat))
            endingRound = sp.local("endingRound", 4294967295)
            sp.if match_pair_price_feed_696_snd.endingRound.is_some():
              endingRound.value = match_pair_price_feed_696_snd.endingRound.open_some()
            self.data.oracles[match_pair_price_feed_696_fst] = sp.record(adminAddress = match_pair_price_feed_696_snd.adminAddress, endingRound = endingRound.value, lastStartedRound = 0, startingRound = match_pair_price_feed_696_snd.startingRound, withdrawable = 0)
            sp.if (self.data.reportingRoundId != 0) & (match_pair_price_feed_696_snd.startingRound <= self.data.reportingRoundId):
              self.data.reportingRoundDetails.activeOracles.add(match_pair_price_feed_696_fst)


  @sp.entry_point
  def decimals(self, params):
    sp.transfer(self.data.decimals, sp.tez(0), params)

  @sp.entry_point
  def forceBalanceUpdate(self):
    sp.transfer(sp.record(requests = sp.list([sp.record(owner = sp.self_address, token_id = 0)]), callback = sp.self_entry_point('updateAvailableFunds')), sp.tez(0), sp.contract(sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")), self.data.linkToken, entry_point='balance_of').open_some(message = 'Aggregator_InvalidTokenkInterface'))

  @sp.entry_point
  def latestRoundData(self, params):
    sp.transfer(self.data.rounds[self.data.latestRoundId], sp.tez(0), params)

  @sp.entry_point
  def submit(self, params):
    match_pair_price_feed_622_fst, match_pair_price_feed_622_snd = sp.match_tuple(params, "match_pair_price_feed_622_fst", "match_pair_price_feed_622_snd")
    sp.verify(self.data.active)
    sp.verify(self.data.oracles.contains(sp.sender), 'Aggregator_NotOracle')
    sp.verify(self.data.oracles[sp.sender].startingRound <= match_pair_price_feed_622_fst, 'Aggregator_NotYetEnabledOracle')
    sp.verify(self.data.oracles[sp.sender].endingRound > match_pair_price_feed_622_fst, 'Aggregator_NotLongerAllowedOracle')
    sp.verify((((match_pair_price_feed_622_fst + 1) == self.data.reportingRoundId) | (match_pair_price_feed_622_fst == self.data.reportingRoundId)) | (match_pair_price_feed_622_fst == (self.data.reportingRoundId + 1)), 'Aggregator_InvalidRound')
    sp.if (match_pair_price_feed_622_fst + 1) == self.data.reportingRoundId:
      sp.verify(~ (self.data.previousRoundDetails.submissions.contains(sp.sender)), 'Aggregator_SubmittedInCurrent')
      sp.verify(sp.len(self.data.previousRoundDetails.submissions) < self.data.previousRoundDetails.maxSubmissions, 'Aggregator_RoundMaxSubmissionExceed')
      self.data.previousRoundDetails.submissions[sp.sender] = match_pair_price_feed_622_snd
      sp.if sp.len(self.data.previousRoundDetails.submissions) >= self.data.previousRoundDetails.minSubmissions:
        self.data.rounds[sp.as_nat(self.data.reportingRoundId - 1)].answer = self.median(self.data.previousRoundDetails.submissions.values())
        self.data.rounds[sp.as_nat(self.data.reportingRoundId - 1)].updatedAt = sp.now
        self.data.rounds[sp.as_nat(self.data.reportingRoundId - 1)].answeredInRound = self.data.reportingRoundId
    sp.else:
      sp.if match_pair_price_feed_622_fst == self.data.reportingRoundId:
        sp.verify(~ (self.data.reportingRoundDetails.submissions.contains(sp.sender)), 'Aggregator_AlreadySubmittedForThisRound')
        sp.verify(sp.len(self.data.reportingRoundDetails.submissions) < self.data.reportingRoundDetails.maxSubmissions, 'Aggregator_RoundMaxSubmissionExceed')
        self.data.reportingRoundDetails.submissions[sp.sender] = match_pair_price_feed_622_snd
        sp.if sp.len(self.data.reportingRoundDetails.submissions) >= self.data.reportingRoundDetails.minSubmissions:
          self.data.rounds[self.data.reportingRoundId].answer = self.median(self.data.reportingRoundDetails.submissions.values())
          self.data.rounds[self.data.reportingRoundId].updatedAt = sp.now
          self.data.rounds[self.data.reportingRoundId].answeredInRound = self.data.reportingRoundId
          self.data.latestRoundId = self.data.reportingRoundId
      sp.else:
        sp.if self.data.reportingRoundId > 0:
          sp.verify((self.data.oracles[sp.sender].lastStartedRound == 0) | ((self.data.reportingRoundId + 1) > (self.data.oracles[sp.sender].lastStartedRound + self.data.restartDelay)), 'Aggregator_WaitBeforeInit')
          sp.verify((sp.now > sp.add_seconds(self.data.rounds[self.data.reportingRoundId].startedAt, sp.to_int(self.data.timeout) * 60)) | (self.data.rounds[self.data.reportingRoundId].answeredInRound == self.data.reportingRoundId), 'Aggregator_PreviousRoundNotOver')
        answer = sp.local("answer", 0)
        answeredInRound = sp.local("answeredInRound", 0)
        sp.if self.data.minSubmissions == 1:
          answer.value = match_pair_price_feed_622_snd
          answeredInRound.value = self.data.reportingRoundId + 1
        self.data.rounds[self.data.reportingRoundId + 1] = sp.record(answer = answer.value, answeredInRound = answeredInRound.value, roundId = self.data.reportingRoundId + 1, startedAt = sp.now, updatedAt = sp.now)
        self.data.previousRoundDetails = self.data.reportingRoundDetails
        compute_price_feed_588 = sp.local("compute_price_feed_588", self.getActiveOracles(self.data.reportingRoundId + 1))
        self.data.reportingRoundDetails = sp.record(activeOracles = compute_price_feed_588.value, maxSubmissions = self.data.maxSubmissions, minSubmissions = self.data.minSubmissions, submissions = {sp.sender : match_pair_price_feed_622_snd}, timeout = self.data.timeout)
        self.data.oracles[sp.sender].lastStartedRound = self.data.reportingRoundId + 1
        self.data.reportingRoundId += 1
    self.data.recordedFunds.available = sp.as_nat(self.data.recordedFunds.available - self.data.oraclePayment, message = 'Aggregator_OraclePaymentUnderflow')
    self.data.recordedFunds.allocated += self.data.oraclePayment
    self.data.oracles[sp.sender].withdrawable += self.data.oraclePayment

  @sp.entry_point
  def updateAvailableFunds(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))))
    sp.verify(sp.sender == self.data.linkToken, 'Aggregator_NotLinkToken')
    balance = sp.local("balance", 0)
    sp.for resp in params:
      sp.verify(resp.request.owner == sp.self_address)
      balance.value = resp.balance
    sp.if balance.value != self.data.recordedFunds.available:
      self.data.recordedFunds.available = balance.value

  @sp.entry_point
  def withdrawPayment(self, params):
    sp.verify(self.data.oracles[params.oracleAddress].adminAddress == sp.sender, 'Aggregator_NotOracleAdmin')
    sp.verify(self.data.oracles[params.oracleAddress].withdrawable >= params.amount, 'Aggregator_InsufficientWithdrawableFunds')
    self.data.oracles[params.oracleAddress].withdrawable = sp.as_nat(self.data.oracles[params.oracleAddress].withdrawable - params.amount)
    self.data.recordedFunds.allocated = sp.as_nat(self.data.recordedFunds.allocated - params.amount)
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = params.recipientAddress, token_id = 0, amount = params.amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.linkToken, entry_point='transfer').open_some(message = 'Aggregator_InvalidTokenkInterface'))
    sp.transfer(sp.record(requests = sp.list([sp.record(owner = sp.self_address, token_id = 0)]), callback = sp.self_entry_point('updateAvailableFunds')), sp.tez(0), sp.contract(sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")), self.data.linkToken, entry_point='balance_of').open_some(message = 'Aggregator_InvalidTokenkInterface'))

  @sp.private_lambda()
  def getActiveOracles(_x2):
    oracles = sp.local("oracles", sp.list([]), sp.TList(sp.TAddress))
    oracles.value = self.data.oracles.keys()
    activeOracles = sp.local("activeOracles", sp.set([]), sp.TSet(sp.TAddress))
    sp.for oracle in oracles.value:
      sp.if (self.data.oracles[oracle].endingRound >= _x2) & (self.data.oracles[oracle].startingRound <= _x2):
        activeOracles.value.add(oracle)
    sp.result(activeOracles.value)

  @sp.private_lambda()
  def median(_x4):
    result = sp.local("result", 0)
    half = sp.local("half", sp.len(_x4) // 2)
    hist = sp.local("hist", {})
    average = sp.local("average", ~ ((half.value * 2) != sp.len(_x4)))
    sp.for x in _x4:
      sp.if hist.value.contains(x):
        hist.value[x] += 1
      sp.else:
        hist.value[x] = 1
    i = sp.local("i", 0)
    sp.for x in hist.value.items():
      sp.if average.value:
        sp.if i.value < half.value:
          result.value = x.key
        sp.else:
          result.value += x.key
          result.value //= 2
          average.value = False
        i.value += x.value
      sp.else:
        sp.if i.value <= half.value:
          result.value = x.key
          i.value += x.value
    sp.result(result.value)

sp.add_compilation_target("test", Contract())