import smartpy as sp

tstorage = sp.TRecord(active = sp.TBool, admin = sp.TAddress, aggregator = sp.TOption(sp.TAddress)).layout(("active", ("admin", "aggregator")))
tparameter = sp.TVariant(administrate = sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeAggregator = sp.TAddress).layout(("changeActive", ("changeAdmin", "changeAggregator")))), decimals = sp.TPair(sp.TUnit, sp.TAddress), description = sp.TPair(sp.TUnit, sp.TAddress), latestRoundData = sp.TPair(sp.TUnit, sp.TAddress), version = sp.TPair(sp.TUnit, sp.TAddress)).layout((("administrate", "decimals"), ("description", ("latestRoundData", "version"))))
tprivates = { }
tviews = { }
