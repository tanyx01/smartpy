import smartpy as sp

tstorage = sp.TRecord(x = sp.TBounded(['abc', 'def', 'ghi', 'jkl'], t=sp.TString)).layout("x")
tparameter = sp.TVariant(ep = sp.TUnit, ep2 = sp.TBounded(['abc', 'def', 'ghi', 'jkl'], t=sp.TString)).layout(("ep", "ep2"))
tprivates = { }
tviews = { }
