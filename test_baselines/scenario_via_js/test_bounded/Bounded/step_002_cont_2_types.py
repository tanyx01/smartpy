import smartpy as sp

tstorage = sp.TRecord(x = sp.TBounded([1, 2, 3], t=sp.TInt), y = sp.TInt).layout(("x", "y"))
tparameter = sp.TVariant(ep = sp.TUnit, ep2 = sp.TUnit, ep3 = sp.TUnit).layout(("ep", ("ep2", "ep3")))
tprivates = { }
tviews = { }
