Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> 1234567
 => test_baselines/scenario_via_js/test_bitwise/test/step_000_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_000_cont_0_storage.json 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/test_bitwise/test/step_000_cont_0_storage.py 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_000_cont_0_types.py 7
 => test_baselines/scenario_via_js/test_bitwise/test/step_000_cont_0_contract.tz 45
 => test_baselines/scenario_via_js/test_bitwise/test/step_000_cont_0_contract.json 43
 => test_baselines/scenario_via_js/test_bitwise/test/step_000_cont_0_contract.py 28
 => test_baselines/scenario_via_js/test_bitwise/test/step_001_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_001_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_001_cont_0_params.json 1
Executing test_and(10)...
 -> 2
Verifying sp.contract_data(0).a == 2...
 OK
 => test_baselines/scenario_via_js/test_bitwise/test/step_003_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_003_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_003_cont_0_params.json 1
Executing test_or(10)...
 -> 10
Verifying sp.contract_data(0).a == 10...
 OK
 => test_baselines/scenario_via_js/test_bitwise/test/step_005_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_005_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_005_cont_0_params.json 1
Executing test_exclusive_or(9)...
 -> 3
Verifying sp.contract_data(0).a == 3...
 OK
 => test_baselines/scenario_via_js/test_bitwise/test/step_007_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_007_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_007_cont_0_params.json 1
Executing test_shift_left(10)...
 -> 3072
Verifying sp.contract_data(0).a == 3072...
 OK
 => test_baselines/scenario_via_js/test_bitwise/test/step_009_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_009_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_bitwise/test/step_009_cont_0_params.json 1
Executing test_shift_right(10)...
 -> 3
Verifying sp.contract_data(0).a == 3...
 OK
