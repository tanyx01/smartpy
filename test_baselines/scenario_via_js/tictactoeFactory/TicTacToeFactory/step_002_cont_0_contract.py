import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, boards = sp.TBigMap(sp.TString, sp.TRecord(deck = sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TInt)), draw = sp.TBool, metaData = sp.TMap(sp.TString, sp.TString), nbMoves = sp.TIntOrNat, nextPlayer = sp.TInt, player1 = sp.TAddress, player2 = sp.TAddress, winner = sp.TInt).layout(("deck", ("draw", ("metaData", ("nbMoves", ("nextPlayer", ("player1", ("player2", "winner"))))))))), metaData = sp.TMap(sp.TString, sp.TString), paused = sp.TBool).layout(("admin", ("boards", ("metaData", "paused")))))
    self.init(admin = sp.address('tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq'),
              boards = {},
              metaData = {},
              paused = False)

  @sp.entry_point
  def build(self, params):
    sp.verify((~ self.data.paused) | (sp.sender == self.data.admin))
    sp.verify(~ (self.data.boards.contains(params.game)))
    self.data.boards[params.game] = sp.record(deck = sp.set_type_expr({0 : {0 : 0, 1 : 0, 2 : 0}, 1 : {0 : 0, 1 : 0, 2 : 0}, 2 : {0 : 0, 1 : 0, 2 : 0}}, sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TInt))), draw = False, metaData = {}, nbMoves = 0, nextPlayer = 1, player1 = params.player1, player2 = params.player2, winner = 0)

  @sp.entry_point
  def deleteGame(self, params):
    sp.verify(sp.sender == self.data.admin)
    del self.data.boards[params.game]

  @sp.entry_point
  def play(self, params):
    sp.verify(~ self.data.paused)
    sp.verify((self.data.boards[params.game].winner == 0) & (~ self.data.boards[params.game].draw))
    sp.verify((params.i >= 0) & (params.i < 3))
    sp.verify((params.j >= 0) & (params.j < 3))
    sp.verify(params.move == self.data.boards[params.game].nextPlayer)
    sp.verify(self.data.boards[params.game].deck[params.i][params.j] == 0)
    sp.if params.move == 1:
      sp.verify(sp.sender == self.data.boards[params.game].player1)
    sp.else:
      sp.verify(sp.sender == self.data.boards[params.game].player2)
    self.data.boards[params.game].nextPlayer = 3 - self.data.boards[params.game].nextPlayer
    self.data.boards[params.game].deck[params.i][params.j] = params.move
    self.data.boards[params.game].nbMoves += 1
    sp.if ((self.data.boards[params.game].deck[params.i][0] != 0) & (self.data.boards[params.game].deck[params.i][0] == self.data.boards[params.game].deck[params.i][1])) & (self.data.boards[params.game].deck[params.i][0] == self.data.boards[params.game].deck[params.i][2]):
      self.data.boards[params.game].winner = self.data.boards[params.game].deck[params.i][0]
    sp.if ((self.data.boards[params.game].deck[0][params.j] != 0) & (self.data.boards[params.game].deck[0][params.j] == self.data.boards[params.game].deck[1][params.j])) & (self.data.boards[params.game].deck[0][params.j] == self.data.boards[params.game].deck[2][params.j]):
      self.data.boards[params.game].winner = self.data.boards[params.game].deck[0][params.j]
    sp.if ((self.data.boards[params.game].deck[0][0] != 0) & (self.data.boards[params.game].deck[0][0] == self.data.boards[params.game].deck[1][1])) & (self.data.boards[params.game].deck[0][0] == self.data.boards[params.game].deck[2][2]):
      self.data.boards[params.game].winner = self.data.boards[params.game].deck[0][0]
    sp.if ((self.data.boards[params.game].deck[0][2] != 0) & (self.data.boards[params.game].deck[0][2] == self.data.boards[params.game].deck[1][1])) & (self.data.boards[params.game].deck[0][2] == self.data.boards[params.game].deck[2][0]):
      self.data.boards[params.game].winner = self.data.boards[params.game].deck[0][2]
    sp.if (self.data.boards[params.game].nbMoves == 9) & (self.data.boards[params.game].winner == 0):
      self.data.boards[params.game].draw = True

  @sp.entry_point
  def setGameMetaData(self, params):
    sp.verify((sp.sender == self.data.admin) | (sp.sender == self.data.boards[params.game].player1))
    sp.set_type(params.name, sp.TString)
    sp.set_type(params.value, sp.TString)
    self.data.boards[params.game].metaData[params.name] = params.value

  @sp.entry_point
  def setMetaData(self, params):
    sp.verify(sp.sender == self.data.admin)
    sp.set_type(params.name, sp.TString)
    sp.set_type(params.value, sp.TString)
    self.data.metaData[params.name] = params.value

  @sp.entry_point
  def setPause(self, params):
    sp.verify(sp.sender == self.data.admin)
    self.data.paused = params

sp.add_compilation_target("test", Contract())