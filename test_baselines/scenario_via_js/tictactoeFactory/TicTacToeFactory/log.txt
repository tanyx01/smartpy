Comment...
 h1: Tic-Tac-Toe Games
Comment...
 h2: A sequence of interactions with a winner
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair "tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq" (Pair {} (Pair {} False)))
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_002_cont_0_storage.json 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_002_cont_0_storage.py 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_002_cont_0_types.py 7
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_002_cont_0_contract.tz 1555
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_002_cont_0_contract.json 1360
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_002_cont_0_contract.py 67
Comment...
 h2: Message execution
Comment...
 h3: Building a contract
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_005_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_005_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_005_cont_0_params.json 4
Executing build(sp.record(game = 'game 1', player1 = sp.resolve(sp.test_account("Alice").address), player2 = sp.resolve(sp.test_account("Robert").address)))...
 -> (Pair "tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq" (Pair {Elt "game 1" (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair {} (Pair 0 (Pair 1 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0)))))))} (Pair {} False)))
Comment...
 h3: A first move in the center
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_007_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_007_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_007_cont_0_params.json 1
Executing play(sp.record(game = 'game 1', i = 1, j = 1, move = 1))...
 -> (Pair "tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq" (Pair {Elt "game 1" (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair {} (Pair 1 (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0)))))))} (Pair {} False)))
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_008_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_008_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_008_cont_0_params.json 4
Executing build(sp.record(game = 'game 2', player1 = sp.resolve(sp.test_account("Alice").address), player2 = sp.resolve(sp.test_account("Robert").address)))...
 -> (Pair "tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq" (Pair {Elt "game 1" (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair {} (Pair 1 (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0))))))); Elt "game 2" (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair {} (Pair 0 (Pair 1 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0)))))))} (Pair {} False)))
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_009_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_009_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_009_cont_0_params.json 1
Executing deleteGame(sp.record(game = 'game 2'))...
 -> (Pair "tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq" (Pair {Elt "game 1" (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair {} (Pair 1 (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0)))))))} (Pair {} False)))
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_010_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_010_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_010_cont_0_params.json 1
Executing setMetaData(sp.record(name = 'toto', value = 'titi'))...
 -> (Pair "tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq" (Pair {Elt "game 1" (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair {} (Pair 1 (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0)))))))} (Pair {Elt "toto" "titi"} False)))
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_011_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_011_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_011_cont_0_params.json 1
Executing setGameMetaData(sp.record(game = 'game 1', name = 'toto', value = 'titi'))...
 -> (Pair "tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq" (Pair {Elt "game 1" (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair {Elt "toto" "titi"} (Pair 1 (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0)))))))} (Pair {Elt "toto" "titi"} False)))
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_012_cont_0_params.py 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_012_cont_0_params.tz 1
 => test_baselines/scenario_via_js/tictactoeFactory/TicTacToeFactory/step_012_cont_0_params.json 1
Executing setGameMetaData(sp.record(game = 'game 1', name = 'toto2', value = 'titi2'))...
 -> (Pair "tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq" (Pair {Elt "game 1" (Pair {Elt 0 {Elt 0 0; Elt 1 0; Elt 2 0}; Elt 1 {Elt 0 0; Elt 1 1; Elt 2 0}; Elt 2 {Elt 0 0; Elt 1 0; Elt 2 0}} (Pair False (Pair {Elt "toto" "titi"; Elt "toto2" "titi2"} (Pair 1 (Pair 2 (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0)))))))} (Pair {Elt "toto" "titi"} False)))
