import smartpy as sp

tstorage = sp.TUnit
tparameter = sp.TVariant(ep = sp.TList(sp.TVariant(V1 = sp.TRecord(a = sp.TBool, b = sp.TString).layout(("b", "a")), V2 = sp.TNat).layout(("V2", "V1")))).layout("ep")
tprivates = { }
tviews = { }
