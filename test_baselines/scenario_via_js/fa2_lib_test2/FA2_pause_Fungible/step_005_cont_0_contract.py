import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(administrator = sp.TAddress, last_token_id = sp.TNat, ledger = sp.TBigMap(sp.TPair(sp.TAddress, sp.TNat), sp.TNat), metadata = sp.TBigMap(sp.TString, sp.TBytes), operators = sp.TBigMap(sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), sp.TUnit), paused = sp.TBool, supply = sp.TBigMap(sp.TNat, sp.TNat), token_metadata = sp.TBigMap(sp.TNat, sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info")))).layout(("administrator", ("last_token_id", ("ledger", ("metadata", ("operators", ("paused", ("supply", "token_metadata")))))))))
    self.init(administrator = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
              last_token_id = 3,
              ledger = {(sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), 0) : 42, (sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), 1) : 42, (sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), 2) : 42},
              metadata = {'' : sp.bytes('0x697066733a2f2f6578616d706c65')},
              operators = {},
              paused = False,
              supply = {0 : 42, 1 : 42, 2 : 42},
              token_metadata = {0 : sp.record(token_id = 0, token_info = {'decimals' : sp.bytes('0x31'), 'name' : sp.bytes('0x546f6b656e205a65726f'), 'symbol' : sp.bytes('0x546f6b30')}), 1 : sp.record(token_id = 1, token_info = {'decimals' : sp.bytes('0x31'), 'name' : sp.bytes('0x546f6b656e204f6e65'), 'symbol' : sp.bytes('0x546f6b31')}), 2 : sp.record(token_id = 2, token_info = {'decimals' : sp.bytes('0x31'), 'name' : sp.bytes('0x546f6b656e2054776f'), 'symbol' : sp.bytes('0x546f6b32')})})

  @sp.entry_point
  def balance_of(self, params):
    sp.set_type(params, sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")))
    sp.set_type(params.requests, sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))))
    def f_x0(_x0):
      sp.verify(self.data.token_metadata.contains(_x0.token_id), 'FA2_TOKEN_UNDEFINED')
      sp.result(sp.record(request = _x0, balance = self.data.ledger.get((_x0.owner, _x0.token_id), default_value = 0)))
    sp.transfer(params.requests.map(sp.build_lambda(f_x0)), sp.tez(0), params.callback)

  @sp.entry_point
  def burn(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(amount = sp.TNat, from_ = sp.TAddress, token_id = sp.TNat).layout(("from_", ("token_id", "amount")))))
    sp.verify(True, 'FA2_TX_DENIED')
    sp.for action in params:
      sp.verify(self.data.token_metadata.contains(action.token_id), 'FA2_TOKEN_UNDEFINED')
      sp.verify(~ self.data.paused, ('FA2_TX_DENIED', 'FA2_PAUSED'))
      sp.verify((sp.sender == action.from_) | (self.data.operators.contains(sp.record(owner = action.from_, operator = sp.sender, token_id = action.token_id))), 'FA2_NOT_OPERATOR')
      self.data.ledger[(action.from_, action.token_id)] = sp.as_nat(self.data.ledger.get((action.from_, action.token_id), default_value = 0) - action.amount, message = 'FA2_INSUFFICIENT_BALANCE')
      compute_fa2_lib_742 = sp.local("compute_fa2_lib_742", sp.is_nat(self.data.supply.get(action.token_id, default_value = 0) - action.amount))
      with compute_fa2_lib_742.value.match_cases() as arg:
        with arg.match('Some') as Some:
          self.data.supply[action.token_id] = Some
        with arg.match('None') as None:
          self.data.supply[action.token_id] = 0


  @sp.entry_point
  def mint(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token = sp.TVariant(existing = sp.TNat, new = sp.TMap(sp.TString, sp.TBytes)).layout(("existing", "new"))).layout(("to_", ("token", "amount")))))
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    sp.for action in params:
      with action.token.match_cases() as arg:
        with arg.match('new') as new:
          compute_fa2_lib_634 = sp.local("compute_fa2_lib_634", self.data.last_token_id)
          self.data.token_metadata[compute_fa2_lib_634.value] = sp.record(token_id = compute_fa2_lib_634.value, token_info = new)
          self.data.supply[compute_fa2_lib_634.value] = action.amount
          self.data.ledger[(action.to_, compute_fa2_lib_634.value)] = action.amount
          self.data.last_token_id += 1
        with arg.match('existing') as existing:
          sp.verify(self.data.token_metadata.contains(existing), 'FA2_TOKEN_UNDEFINED')
          self.data.supply[existing] += action.amount
          self.data.ledger[(action.to_, existing)] = self.data.ledger.get((action.to_, existing), default_value = 0) + action.amount


  @sp.entry_point
  def set_administrator(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.administrator = params

  @sp.entry_point
  def set_metadata(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.metadata = params

  @sp.entry_point
  def set_pause(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.paused = params

  @sp.entry_point
  def transfer(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))))
    sp.for transfer in params:
      sp.for tx in transfer.txs:
        sp.verify(self.data.token_metadata.contains(tx.token_id), 'FA2_TOKEN_UNDEFINED')
        sp.verify(~ self.data.paused, ('FA2_TX_DENIED', 'FA2_PAUSED'))
        sp.verify((sp.sender == transfer.from_) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))), 'FA2_NOT_OPERATOR')
        sp.if tx.amount > 0:
          self.data.ledger[(transfer.from_, tx.token_id)] = sp.as_nat(self.data.ledger.get((transfer.from_, tx.token_id), default_value = 0) - tx.amount, message = 'FA2_INSUFFICIENT_BALANCE')
          self.data.ledger[(tx.to_, tx.token_id)] = self.data.ledger.get((tx.to_, tx.token_id), default_value = 0) + tx.amount

  @sp.entry_point
  def update_operators(self, params):
    sp.set_type(params, sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))))
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('add_operator') as add_operator:
          sp.verify(~ self.data.paused, ('FA2_OPERATORS_UNSUPPORTED', 'FA2_PAUSED'))
          sp.verify(add_operator.owner == sp.sender, 'FA2_NOT_OWNER')
          self.data.operators[add_operator] = sp.unit
        with arg.match('remove_operator') as remove_operator:
          sp.verify(~ self.data.paused, ('FA2_OPERATORS_UNSUPPORTED', 'FA2_PAUSED'))
          sp.verify(remove_operator.owner == sp.sender, 'FA2_NOT_OWNER')
          del self.data.operators[remove_operator]


  @sp.entry_point
  def withdraw_mutez(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    sp.send(params.destination, params.amount)

sp.add_compilation_target("test", Contract())