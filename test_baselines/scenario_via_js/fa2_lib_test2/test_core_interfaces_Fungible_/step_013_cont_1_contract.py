import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(last_known_balances = sp.TBigMap(sp.TAddress, sp.TMap(sp.TPair(sp.TAddress, sp.TNat), sp.TNat))).layout("last_known_balances"))
    self.init(last_known_balances = {})

  @sp.entry_point
  def receive_balances(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))))
    sp.for resp in params:
      sp.if self.data.last_known_balances.contains(sp.sender):
        self.data.last_known_balances[sp.sender][(resp.request.owner, resp.request.token_id)] = resp.balance
      sp.else:
        self.data.last_known_balances[sp.sender] = {(resp.request.owner, resp.request.token_id) : resp.balance}

sp.add_compilation_target("test", Contract())