Comment...
 h1: test_balance_of_Fungible_
Table Of Contents

 test_balance_of_Fungible_
# Accounts
# Contract
## Receiver contract
# FA2_TOKEN_UNDEFINED error
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Bob")])...
 => [sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edskRqFp3Z9AqoKrMNFb9bnWNwEsRzbjqjBhzmFMLF9UqB6VBmw7F8ppTiXaAnHtysmi6xFxoHf6rMUz6Y1ipiDz2EgwZQv3pa')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edskRijgcXx8gzqkq7SCBbrb6aDZQMmP6dznCQWgU1Jr4qPfJT1yFq5A39ja9G4wahS8uWtBurZy14Hy7GZkQh7WnopJTKtCQG')), sp.record(seed = 'Bob', address = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), public_key = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT'), public_key_hash = sp.key_hash('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), secret_key = sp.secret_key('edskRq1xuW7TCYzdFm1JQLi1Hz4MNDVP6ukQHVEEh3bVqyuzv7pXXjaGsXZuMbwtd3kQFp3LQ7GQzkLeprNEijKhQKzsxrYrUz'))]
Comment...
 h2: Contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair {Elt 0 42; Elt 1 42; Elt 2 42} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_005_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_005_cont_0_storage.json 91
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_005_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_005_cont_0_storage.py 1
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_005_cont_0_types.py 7
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_005_cont_0_metadata.metadata_base.json 149
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_005_cont_0_contract.tz 267
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_005_cont_0_contract.json 406
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_005_cont_0_contract.py 46
Comment...
 h3: Receiver contract
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 -> {}
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_007_cont_1_storage.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_007_cont_1_storage.json 1
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_007_cont_1_sizes.csv 2
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_007_cont_1_storage.py 1
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_007_cont_1_types.py 7
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_007_cont_1_contract.tz 72
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_007_cont_1_contract.json 101
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_007_cont_1_contract.py 17
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_008_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_008_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_008_cont_0_params.json 10
Executing balance_of(sp.record(requests = sp.list([sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0), sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 1)]), callback = sp.contract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))), sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'), entry_point='receive_balances').open_some()))...
 -> (Pair 3 (Pair {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 42} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair {Elt 0 42; Elt 1 42; Elt 2 42} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
  + Transfer
     params: [sp.record(request = sp.record(owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), token_id = 0), balance = 42), sp.record(request = sp.record(owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), token_id = 1), balance = 42)]
     amount: sp.tez(0)
     to:     sp.contract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))), sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF%receive_balances')).open_some()
Executing (queue) receive_balances([sp.record(request = sp.record(owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), token_id = 0), balance = 42), sp.record(request = sp.record(owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), token_id = 1), balance = 42)])...
 -> {Elt "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 42; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 42}}
Verifying sp.contract_data(1).last_known_balances[sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')][(sp.resolve(sp.test_account("Alice").address), 0)] == 42...
 OK
Verifying sp.contract_data(1).last_known_balances[sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')][(sp.resolve(sp.test_account("Alice").address), 1)] == 42...
 OK
Comment...
 h2: FA2_TOKEN_UNDEFINED error
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_012_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_012_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test2/test_balance_of_Fungible_/step_012_cont_0_params.json 10
Executing balance_of(sp.record(requests = sp.list([sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0), sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 5)]), callback = sp.contract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))), sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF'), entry_point='receive_balances').open_some()))...
 -> --- Expected failure in transaction --- Wrong condition: (self.data.token_metadata.contains(_x0.token_id) : sp.TBool) (templates/fa2_lib.py, line 195)
Message: 'FA2_TOKEN_UNDEFINED'
 (templates/fa2_lib.py, line 431)
