import smartpy as sp

tstorage = sp.TRecord(members = sp.TSet(sp.TAddress), proposals = sp.TBigMap(sp.TBytes, sp.TBool), required_votes = sp.TNat, votes = sp.TBigMap(sp.TBytes, sp.TSet(sp.TAddress))).layout(("members", ("proposals", ("required_votes", "votes"))))
tparameter = sp.TVariant(submit_proposal = sp.TBytes, vote_proposal = sp.TBytes).layout(("submit_proposal", "vote_proposal"))
tprivates = { }
tviews = { "is_voted": (sp.TBytes, sp.TBool) }
