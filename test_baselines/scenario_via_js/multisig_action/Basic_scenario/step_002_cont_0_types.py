import smartpy as sp

tstorage = sp.TRecord(inactiveBefore = sp.TNat, nextId = sp.TNat, proposals = sp.TBigMap(sp.TNat, sp.TList(sp.TRecord(actions = sp.TList(sp.TBytes), target = sp.TAddress).layout(("actions", "target")))), quorum = sp.TNat, signers = sp.TSet(sp.TAddress), votes = sp.TBigMap(sp.TNat, sp.TSet(sp.TAddress))).layout(("inactiveBefore", ("nextId", ("proposals", ("quorum", ("signers", "votes"))))))
tparameter = sp.TVariant(administrate = sp.TList(sp.TBytes), send_proposal = sp.TList(sp.TRecord(actions = sp.TList(sp.TBytes), target = sp.TAddress).layout(("actions", "target"))), vote = sp.TNat).layout(("administrate", ("send_proposal", "vote")))
tprivates = { }
tviews = { }
