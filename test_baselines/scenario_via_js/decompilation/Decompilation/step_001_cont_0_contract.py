import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(Left = sp.TIntOrNat, Right = sp.TIntOrNat).layout(("Left", "Right")))
    self.init(Left = 1,
              Right = 12)

  @sp.entry_point
  def welcome(self, params):
    sp.verify(self.data.Left <= 123)
    self.data.Left += params

sp.add_compilation_target("test", Contract())