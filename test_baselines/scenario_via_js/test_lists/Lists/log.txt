Comment...
 h1: Lists
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair None (Pair 0 (Pair "" (Pair 0 (Pair "" (Pair {} (Pair {} (Pair "no head" {"no tail"}))))))))
 => test_baselines/scenario_via_js/test_lists/Lists/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/test_lists/Lists/step_001_cont_0_storage.json 31
 => test_baselines/scenario_via_js/test_lists/Lists/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/test_lists/Lists/step_001_cont_0_storage.py 1
 => test_baselines/scenario_via_js/test_lists/Lists/step_001_cont_0_types.py 7
 => test_baselines/scenario_via_js/test_lists/Lists/step_001_cont_0_contract.tz 294
 => test_baselines/scenario_via_js/test_lists/Lists/step_001_cont_0_contract.json 398
 => test_baselines/scenario_via_js/test_lists/Lists/step_001_cont_0_contract.py 49
 => test_baselines/scenario_via_js/test_lists/Lists/step_002_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_lists/Lists/step_002_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_lists/Lists/step_002_cont_0_params.json 17
Executing test(sp.record(l = sp.list([1, 2, 3]), m = {'a' : ('aa', True), 'b' : ('bb', False), 'c' : ('cc', True)}, s = sp.set([1, 12, 100])))...
 -> (Pair (Some (Pair {1; 2; 3} (Pair {3; 2; 1} (Pair {Pair "a" (Pair "aa" True); Pair "b" (Pair "bb" False); Pair "c" (Pair "cc" True)} (Pair {Pair "c" (Pair "cc" True); Pair "b" (Pair "bb" False); Pair "a" (Pair "aa" True)} (Pair {"a"; "b"; "c"} (Pair {"c"; "b"; "a"} (Pair {Pair "aa" True; Pair "bb" False; Pair "cc" True} (Pair {Pair "cc" True; Pair "bb" False; Pair "aa" True} (Pair {1; 12; 100} {100; 12; 1})))))))))) (Pair 6 (Pair "abc" (Pair 113 (Pair "aacc" (Pair {16; 9; 4; 1; 0} (Pair {1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11} (Pair "no head" {"no tail"}))))))))
 => test_baselines/scenario_via_js/test_lists/Lists/step_003_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_lists/Lists/step_003_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_lists/Lists/step_003_cont_0_params.json 1
Executing test_match(sp.list(['1', '2', '3']))...
 -> (Pair (Some (Pair {1; 2; 3} (Pair {3; 2; 1} (Pair {Pair "a" (Pair "aa" True); Pair "b" (Pair "bb" False); Pair "c" (Pair "cc" True)} (Pair {Pair "c" (Pair "cc" True); Pair "b" (Pair "bb" False); Pair "a" (Pair "aa" True)} (Pair {"a"; "b"; "c"} (Pair {"c"; "b"; "a"} (Pair {Pair "aa" True; Pair "bb" False; Pair "cc" True} (Pair {Pair "cc" True; Pair "bb" False; Pair "aa" True} (Pair {1; 12; 100} {100; 12; 1})))))))))) (Pair 6 (Pair "abc" (Pair 113 (Pair "aacc" (Pair {16; 9; 4; 1; 0} (Pair {1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11} (Pair "1" {"2"; "3"}))))))))
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0), sp.TRecord(a = sp.TOption(sp.TRecord(l = sp.TList(sp.TIntOrNat), lr = sp.TList(sp.TIntOrNat), mi = sp.TList(sp.TRecord(key = sp.TString, value = sp.TPair(sp.TString, sp.TBool)).layout(("key", "value"))), mir = sp.TList(sp.TRecord(key = sp.TString, value = sp.TPair(sp.TString, sp.TBool)).layout(("key", "value"))), mk = sp.TList(sp.TString), mkr = sp.TList(sp.TString), mv = sp.TList(sp.TPair(sp.TString, sp.TBool)), mvr = sp.TList(sp.TPair(sp.TString, sp.TBool)), s = sp.TList(sp.TIntOrNat), sr = sp.TList(sp.TIntOrNat)).layout(("l", ("lr", ("mi", ("mir", ("mk", ("mkr", ("mv", ("mvr", ("s", "sr"))))))))))), b = sp.TIntOrNat, c = sp.TString, d = sp.TIntOrNat, e = sp.TString, f = sp.TList(sp.TIntOrNat), g = sp.TList(sp.TIntOrNat), head = sp.TString, tail = sp.TList(sp.TString)).layout(("a", ("b", ("c", ("d", ("e", ("f", ("g", ("head", "tail"))))))))))) == sp.pack(sp.set_type_expr(sp.record(a = sp.some(sp.record(l = sp.list([1, 2, 3]), lr = sp.list([3, 2, 1]), mi = sp.list([sp.record(key = 'a', value = ('aa', True)), sp.record(key = 'b', value = ('bb', False)), sp.record(key = 'c', value = ('cc', True))]), mir = sp.list([sp.record(key = 'c', value = ('cc', True)), sp.record(key = 'b', value = ('bb', False)), sp.record(key = 'a', value = ('aa', True))]), mk = sp.list(['a', 'b', 'c']), mkr = sp.list(['c', 'b', 'a']), mv = sp.list([('aa', True), ('bb', False), ('cc', True)]), mvr = sp.list([('cc', True), ('bb', False), ('aa', True)]), s = sp.list([1, 12, 100]), sr = sp.list([100, 12, 1]))), b = 6, c = 'abc', d = 113, e = 'aacc', f = sp.list([16, 9, 4, 1, 0]), g = sp.list([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]), head = '1', tail = sp.list(['2', '3'])), sp.TRecord(a = sp.TOption(sp.TRecord(l = sp.TList(sp.TIntOrNat), lr = sp.TList(sp.TIntOrNat), mi = sp.TList(sp.TRecord(key = sp.TString, value = sp.TPair(sp.TString, sp.TBool)).layout(("key", "value"))), mir = sp.TList(sp.TRecord(key = sp.TString, value = sp.TPair(sp.TString, sp.TBool)).layout(("key", "value"))), mk = sp.TList(sp.TString), mkr = sp.TList(sp.TString), mv = sp.TList(sp.TPair(sp.TString, sp.TBool)), mvr = sp.TList(sp.TPair(sp.TString, sp.TBool)), s = sp.TList(sp.TIntOrNat), sr = sp.TList(sp.TIntOrNat)).layout(("l", ("lr", ("mi", ("mir", ("mk", ("mkr", ("mv", ("mvr", ("s", "sr"))))))))))), b = sp.TIntOrNat, c = sp.TString, d = sp.TIntOrNat, e = sp.TString, f = sp.TList(sp.TIntOrNat), g = sp.TList(sp.TIntOrNat), head = sp.TString, tail = sp.TList(sp.TString)).layout(("a", ("b", ("c", ("d", ("e", ("f", ("g", ("head", "tail")))))))))))...
 OK
 => test_baselines/scenario_via_js/test_lists/Lists/step_005_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_lists/Lists/step_005_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_lists/Lists/step_005_cont_0_params.json 1
Executing test_match2(sp.list(['1', '2', '3']))...
 -> (Pair (Some (Pair {1; 2; 3} (Pair {3; 2; 1} (Pair {Pair "a" (Pair "aa" True); Pair "b" (Pair "bb" False); Pair "c" (Pair "cc" True)} (Pair {Pair "c" (Pair "cc" True); Pair "b" (Pair "bb" False); Pair "a" (Pair "aa" True)} (Pair {"a"; "b"; "c"} (Pair {"c"; "b"; "a"} (Pair {Pair "aa" True; Pair "bb" False; Pair "cc" True} (Pair {Pair "cc" True; Pair "bb" False; Pair "aa" True} (Pair {1; 12; 100} {100; 12; 1})))))))))) (Pair 6 (Pair "abc" (Pair 113 (Pair "aacc" (Pair {16; 9; 4; 1; 0} (Pair {1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11} (Pair "12" {"3"}))))))))
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).tail, sp.TList(sp.TString))) == sp.pack(sp.set_type_expr(sp.list(['3']), sp.TList(sp.TString)))...
 OK
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).head, sp.TString)) == sp.pack(sp.set_type_expr('12', sp.TString))...
 OK
