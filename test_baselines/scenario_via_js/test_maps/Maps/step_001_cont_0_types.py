import smartpy as sp

tstorage = sp.TRecord(m = sp.TMap(sp.TIntOrNat, sp.TString), x = sp.TString, y = sp.TOption(sp.TString), z = sp.TOption(sp.TString)).layout(("m", ("x", ("y", "z"))))
tparameter = sp.TVariant(test_get_and_update = sp.TUnit, test_map_get = sp.TMap(sp.TInt, sp.TString), test_map_get2 = sp.TMap(sp.TInt, sp.TString), test_map_get_default_values = sp.TMap(sp.TInt, sp.TString), test_map_get_missing_value = sp.TMap(sp.TInt, sp.TString), test_map_get_missing_value2 = sp.TMap(sp.TInt, sp.TString), test_map_get_opt = sp.TMap(sp.TInt, sp.TString), test_update_map = sp.TUnit).layout(((("test_get_and_update", "test_map_get"), ("test_map_get2", "test_map_get_default_values")), (("test_map_get_missing_value", "test_map_get_missing_value2"), ("test_map_get_opt", "test_update_map"))))
tprivates = { }
tviews = { }
