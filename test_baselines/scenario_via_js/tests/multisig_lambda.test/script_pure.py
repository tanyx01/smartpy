import smartpy as sp

MultisigLambda = sp.io.import_template("multisig_lambda.py").MultisigLambda

if "templates" not in __name__:

    class Administrated(sp.Contract):
        def __init__(self, admin):
            self.init(admin=admin, value=sp.int(0))

        @sp.entry_point
        def set_value(self, value):
            sp.verify(sp.sender == self.data.admin)
            self.data.value = value

    @sp.add_test(name="Full", is_default=True)
    def test():
        notMember = sp.test_account("notMember")
        member1 = sp.test_account("member1")
        member2 = sp.test_account("member2")
        member3 = sp.test_account("member3")
        members = [member1.address, member2.address, member3.address]

        sc = sp.test_scenario()
        sc.h1("Full test")
        sc.table_of_contents()
        sc.h2("Origination")
        sc.h3("MultisigLambda")
        c1 = MultisigLambda(members, required_votes=2)
        sc += c1
        sc.h3("Administrated")
        c2 = Administrated(c1.address)
        sc += c2

        sc.h2("MultisigLambda: submit_lambda")

        def set_42(params):
            administrated = sp.contract(sp.TInt, c2.address, entry_point="set_value")
            sp.transfer(sp.int(42), sp.tez(0), administrated.open_some())

        lambda_ = sp.utils.lambda_operations_only(set_42)
        c1.submit_lambda(lambda_).run(sender=member1)
        sc.verify(c1.data.nextId == 1)
        sc.h3("failure")
        c1.submit_lambda(lambda_).run(
            sender=notMember, valid=False, exception="You are not a member"
        )

        sc.h2("MultisigLambda: vote_lambda")
        sc.h3("failures")
        c1.vote_lambda(0).run(notMember, valid=False, exception="You are not a member")
        sc.h3("valid")
        c1.vote_lambda(0).run(member1)
        c1.vote_lambda(0).run(member2)
        sc.h3("failures")
        c1.vote_lambda(0).run(member3, valid=False, exception="The lambda is inactive")
        c1.vote_lambda(1).run(member3, valid=False, exception="Lambda not found")
        sc.verify(c2.data.value == 42)

    @sp.add_test(name="Mutation", is_default=False)
    def test():
        s = sp.test_scenario()
        with s.mutation_test() as mt:
            mt.add_scenario("Full")
