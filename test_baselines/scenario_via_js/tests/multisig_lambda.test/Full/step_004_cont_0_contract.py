import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(inactiveBefore = sp.TNat, lambdas = sp.TBigMap(sp.TNat, sp.TLambda(sp.TUnit, sp.TList(sp.TOperation))), members = sp.TSet(sp.TAddress), nextId = sp.TNat, required_votes = sp.TNat, votes = sp.TBigMap(sp.TNat, sp.TSet(sp.TAddress))).layout(("inactiveBefore", ("lambdas", ("members", ("nextId", ("required_votes", "votes")))))))
    self.init(inactiveBefore = 0,
              lambdas = {},
              members = sp.set([sp.address('tz1Qi4ShtraJF1xagU5kRUjU6xzXYS5ufnXm'), sp.address('tz1RnNX4gzQTy6wDFHNGV7ewuXS2Po7cpC3W'), sp.address('tz1W2YvpKeYqeSbeuJYc2i76ar6wAtKpnqbx')]),
              nextId = 0,
              required_votes = 2,
              votes = {})

  @sp.entry_point
  def submit_lambda(self, params):
    sp.verify(self.data.members.contains(sp.sender), 'You are not a member')
    self.data.lambdas[self.data.nextId] = params
    self.data.votes[self.data.nextId] = sp.set([])
    self.data.nextId += 1

  @sp.entry_point
  def vote_lambda(self, params):
    sp.verify(self.data.members.contains(sp.sender), 'You are not a member')
    sp.verify(params >= self.data.inactiveBefore, 'The lambda is inactive')
    sp.verify(self.data.lambdas.contains(params), 'Lambda not found')
    self.data.votes[params].add(sp.sender)
    sp.if sp.len(self.data.votes[params]) >= self.data.required_votes:
      sp.for op in self.data.lambdas[params](sp.unit):
        sp.operations().push(op)
      self.data.inactiveBefore = self.data.nextId

sp.add_compilation_target("test", Contract())