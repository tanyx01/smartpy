import smartpy as sp

tstorage = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d"))))
tparameter = sp.TVariant(ep1 = sp.TUnit, ep2 = sp.TUnit).layout(("ep1", "ep2"))
tprivates = { }
tviews = { }
