Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair {Elt "abc" (Pair 10 20)} "z")
 => test_baselines/scenario_via_js/test_match/Match/step_000_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/test_match/Match/step_000_cont_0_storage.json 1
 => test_baselines/scenario_via_js/test_match/Match/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/test_match/Match/step_000_cont_0_storage.py 1
 => test_baselines/scenario_via_js/test_match/Match/step_000_cont_0_types.py 7
 => test_baselines/scenario_via_js/test_match/Match/step_000_cont_0_contract.tz 209
 => test_baselines/scenario_via_js/test_match/Match/step_000_cont_0_contract.json 310
 => test_baselines/scenario_via_js/test_match/Match/step_000_cont_0_contract.py 63
 => test_baselines/scenario_via_js/test_match/Match/step_001_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_match/Match/step_001_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_match/Match/step_001_cont_0_params.json 1
Executing ep1(('x', 2))...
 -> (Pair {Elt "abc" (Pair 10 20)} "z")
 => test_baselines/scenario_via_js/test_match/Match/step_002_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_match/Match/step_002_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_match/Match/step_002_cont_0_params.json 1
Executing ep2(('x', 2, True))...
 -> (Pair {Elt "abc" (Pair 10 20)} "z")
 => test_baselines/scenario_via_js/test_match/Match/step_003_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_match/Match/step_003_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_match/Match/step_003_cont_0_params.json 1
Executing ep3(sp.record(x = 'x', y = 2, z = True))...
 -> (Pair {Elt "abc" (Pair 10 20)} "z")
 => test_baselines/scenario_via_js/test_match/Match/step_004_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_match/Match/step_004_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_match/Match/step_004_cont_0_params.json 1
Executing ep5((1, 6, 2, 3))...
 -> (Pair {Elt "abc" (Pair 10 20)} "z")
 => test_baselines/scenario_via_js/test_match/Match/step_005_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_match/Match/step_005_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_match/Match/step_005_cont_0_params.json 1
Executing ep6((1, 6, 2, 6))...
 -> (Pair {Elt "abc" (Pair 10 20)} "z")
 => test_baselines/scenario_via_js/test_match/Match/step_006_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_match/Match/step_006_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_match/Match/step_006_cont_0_params.json 1
Executing ep7(sp.record())...
 -> (Pair {Elt "abc" (Pair 10 20)} "xyzabc")
