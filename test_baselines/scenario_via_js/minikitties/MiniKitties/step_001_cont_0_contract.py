import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(creator = sp.TAddress, kitties = sp.TMap(sp.TInt, sp.TRecord(auction = sp.TTimestamp, borrowPrice = sp.TMutez, generation = sp.TIntOrNat, hatching = sp.TTimestamp, isNew = sp.TBool, kittyId = sp.TInt, owner = sp.TAddress, price = sp.TMutez).layout(("auction", ("borrowPrice", ("generation", ("hatching", ("isNew", ("kittyId", ("owner", "price")))))))))).layout(("creator", "kitties")))
    self.init(creator = sp.address('tz1ehmD34HoQEbSGb8sLsKeFKJXE5BRpakUs'),
              kitties = {})

  @sp.entry_point
  def breed(self, params):
    sp.verify(params.parent1 != params.parent2)
    sp.if False:
      sp.verify(sp.tez(0) < self.data.kitties[params.parent1].borrowPrice)
      sp.verify(self.data.kitties[params.parent1].borrowPrice < params.borrowPrice)
      sp.verify(sp.amount == params.borrowPrice)
      sp.send(self.data.kitties[params.parent1].owner, params.borrowPrice)
    sp.verify(self.data.kitties[params.parent1].auction < sp.now)
    sp.verify(self.data.kitties[params.parent1].hatching < sp.now)
    sp.if self.data.kitties[params.parent2].owner != sp.sender:
      sp.verify(sp.tez(0) < self.data.kitties[params.parent2].borrowPrice)
      sp.verify(self.data.kitties[params.parent2].borrowPrice < params.borrowPrice)
      sp.verify(sp.amount == params.borrowPrice)
      sp.send(self.data.kitties[params.parent2].owner, params.borrowPrice)
    sp.verify(self.data.kitties[params.parent2].auction < sp.now)
    sp.verify(self.data.kitties[params.parent2].hatching < sp.now)
    self.data.kitties[params.parent1].hatching = sp.add_seconds(sp.now, 100)
    self.data.kitties[params.parent2].hatching = sp.add_seconds(sp.now, 100)
    self.data.kitties[params.kittyId] = sp.record(auction = sp.timestamp(0), borrowPrice = sp.tez(0), generation = 1 + sp.max(self.data.kitties[params.parent1].generation, self.data.kitties[params.parent2].generation), hatching = sp.add_seconds(sp.now, 100), isNew = False, kittyId = params.kittyId, owner = sp.sender, price = sp.tez(0))

  @sp.entry_point
  def build(self, params):
    sp.verify(self.data.creator == sp.sender)
    sp.verify(params.kitty.isNew)
    sp.set_type(params.kitty.kittyId, sp.TInt)
    self.data.kitties[params.kitty.kittyId] = params.kitty

  @sp.entry_point
  def buy(self, params):
    sp.verify(sp.tez(0) < self.data.kitties[params.kittyId].price)
    sp.verify(self.data.kitties[params.kittyId].price <= params.price)
    sp.verify(sp.amount == params.price)
    sp.send(self.data.kitties[params.kittyId].owner, params.price)
    self.data.kitties[params.kittyId].owner = sp.sender
    sp.if self.data.kitties[params.kittyId].isNew:
      self.data.kitties[params.kittyId].isNew = False
      self.data.kitties[params.kittyId].auction = sp.add_seconds(sp.now, 10)
    sp.verify(sp.now <= self.data.kitties[params.kittyId].auction)
    sp.if sp.now <= self.data.kitties[params.kittyId].auction:
      self.data.kitties[params.kittyId].price = params.price + sp.mutez(1)

  @sp.entry_point
  def lend(self, params):
    sp.verify(sp.tez(0) <= params.price)
    sp.if False:
      sp.verify(sp.tez(0) < self.data.kitties[params.kittyId].borrowPrice)
      sp.verify(self.data.kitties[params.kittyId].borrowPrice < params.borrowPrice)
      sp.verify(sp.amount == params.borrowPrice)
      sp.send(self.data.kitties[params.kittyId].owner, params.borrowPrice)
    sp.verify(self.data.kitties[params.kittyId].auction < sp.now)
    sp.verify(self.data.kitties[params.kittyId].hatching < sp.now)
    self.data.kitties[params.kittyId].borrowPrice = params.price

  @sp.entry_point
  def sell(self, params):
    sp.verify(sp.tez(0) <= params.price)
    sp.if False:
      sp.verify(sp.tez(0) < self.data.kitties[params.kittyId].borrowPrice)
      sp.verify(self.data.kitties[params.kittyId].borrowPrice < params.borrowPrice)
      sp.verify(sp.amount == params.borrowPrice)
      sp.send(self.data.kitties[params.kittyId].owner, params.borrowPrice)
    sp.verify(self.data.kitties[params.kittyId].auction < sp.now)
    sp.verify(self.data.kitties[params.kittyId].hatching < sp.now)
    self.data.kitties[params.kittyId].price = params.price

sp.add_compilation_target("test", Contract())