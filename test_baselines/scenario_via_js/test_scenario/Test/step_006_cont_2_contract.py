import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep2(self):
    f = sp.local("f", sp.build_lambda(lambda _x0: sp.scenario_var(1)))
    sp.verify(f.value(0) == 43)

sp.add_compilation_target("test", Contract())