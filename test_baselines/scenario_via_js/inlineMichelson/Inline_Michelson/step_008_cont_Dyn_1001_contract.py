import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TInt)
    self.init(123)

  @sp.entry_point
  def main(self):
    pass

sp.add_compilation_target("test", Contract())