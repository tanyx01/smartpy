import smartpy as sp

tstorage = sp.TRecord(a = sp.TRecord(x1 = sp.TInt, x2 = sp.TInt).layout(("x1", "x2")), b = sp.TRecord(x1 = sp.TInt, x2 = sp.TInt).layout(("x1", "x2"))).layout(("a", "b"))
tparameter = sp.TVariant(swap = sp.TUnit).layout("swap")
tprivates = { }
tviews = { }
