import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def compare_eq(self):
    sp.verify(sp.compare(sp.unit, sp.unit) == 0)
    sp.verify(sp.compare(sp.set_type_expr(sp.none, sp.TOption(sp.TNat)), sp.set_type_expr(sp.none, sp.TOption(sp.TNat))) == 0)
    sp.verify(sp.compare(2, 1) == 1)

  @sp.entry_point
  def compare_inferior(self):
    sp.verify(sp.compare(1, 5) == (-1))
    sp.verify(sp.compare(1, 5) == (-1))
    sp.verify(sp.compare(sp.timestamp(1), sp.timestamp(5)) == (-1))
    sp.verify(sp.compare(sp.address('KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT'), sp.address('KT1XvNYseNDJJ6Kw27qhSEDF8ys8JhDopzfG')) == (-1))
    sp.verify(sp.compare(False, True) == (-1))
    sp.verify(sp.compare(sp.bytes('0x00'), sp.bytes('0x01')) == (-1))
    sp.verify(sp.compare(sp.chain_id('0x00'), sp.chain_id('0x01')) == (-1))
    sp.verify(sp.compare(sp.key('edpkuBknW28nW72KG6RoH'), sp.key('edpkuJqtDcA2m2muMxViS')) == (-1))
    sp.verify(sp.compare(sp.key_hash('tz1KqTpEZ7Yob7QbPE4Hy'), sp.key_hash('tz1XPTDmvT3vVE5Uunngm')) == (-1))
    sp.verify(sp.compare(sp.mutez(1), sp.mutez(5)) == (-1))
    sp.verify(sp.compare('a', 'e') == (-1))
    sp.verify(sp.compare((1, 5), (5, 1)) == (-1))
    sp.verify(sp.compare(sp.none, sp.some(sp.unit)) == (-1))
    sp.verify(sp.compare(sp.some(1), sp.some(5)) == (-1))
    sp.verify(sp.compare(sp.set_type_expr(variant('Left', 5), sp.TVariant(Left = sp.TNat, Right = sp.TNat).layout(("Left", "Right"))), variant('Right', 1)) == (-1))
    sp.verify(sp.compare(sp.set_type_expr(variant('Left', 1), sp.TVariant(Left = sp.TNat, Right = sp.TNat).layout(("Left", "Right"))), variant('Left', 5)) == (-1))
    sp.verify(sp.compare(sp.set_type_expr(variant('Left', 1), sp.TVariant(Left = sp.TNat, Right = sp.TNat).layout(("Left", "Right"))), variant('Left', 5)) == (-1))
    sp.verify(sp.compare(sp.set_type_expr(variant('Right', 1), sp.TVariant(Left = sp.TNat, Right = sp.TNat).layout(("Left", "Right"))), variant('Right', 5)) == (-1))

sp.add_compilation_target("test", Contract())