import smartpy as sp

tstorage = sp.TRecord(current = sp.TString, played = sp.TIntOrNat, rules = sp.TList(sp.TString), verse = sp.TIntOrNat).layout(("current", ("played", ("rules", "verse"))))
tparameter = sp.TVariant(sing = sp.TRecord(verses = sp.TIntOrNat).layout("verses")).layout("sing")
tprivates = { }
tviews = { }
