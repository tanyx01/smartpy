Comment...
 h1: String Manipulations
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair (Some 0xaa) (Pair 0 (Pair 0 (Pair 0 (Pair (Some "hello") (Pair {} ""))))))
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_001_cont_0_storage.json 24
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_001_cont_0_storage.py 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_001_cont_0_types.py 7
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_001_cont_0_contract.tz 360
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_001_cont_0_contract.json 441
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_001_cont_0_contract.py 63
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_002_cont_0_params.py 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_002_cont_0_params.tz 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_002_cont_0_params.json 1
Executing slicing(sp.record(b = sp.bytes('0xaa00bbcc'), s = '012345678901234567890123456789'))...
 -> (Pair (Some 0x00bb) (Pair 35 (Pair 4 (Pair 0 (Pair (Some "23456") (Pair {} ""))))))
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).s0, sp.TOption(sp.TString))) == sp.pack(sp.set_type_expr(sp.some('23456'), sp.TOption(sp.TString)))...
 OK
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).b0, sp.TOption(sp.TBytes))) == sp.pack(sp.set_type_expr(sp.some(sp.bytes('0x00bb')), sp.TOption(sp.TBytes)))...
 OK
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).l0, sp.TNat)) == sp.pack(sp.set_type_expr(35, sp.TNat))...
 OK
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).l1, sp.TNat)) == sp.pack(sp.set_type_expr(4, sp.TNat))...
 OK
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_007_cont_0_params.py 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_007_cont_0_params.tz 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_007_cont_0_params.json 1
Executing slicing(sp.record(b = sp.bytes('0xcc'), s = '01'))...
 -> (Pair None (Pair 2 (Pair 1 (Pair 0 (Pair None (Pair {} ""))))))
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).s0, sp.TOption(sp.TString))) == sp.pack(sp.set_type_expr(sp.none, sp.TOption(sp.TString)))...
 OK
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).b0, sp.TOption(sp.TBytes))) == sp.pack(sp.set_type_expr(sp.none, sp.TOption(sp.TBytes)))...
 OK
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_010_cont_0_params.py 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_010_cont_0_params.tz 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_010_cont_0_params.json 16
Executing concatenating(sp.record(b0 = sp.bytes('0x11'), b1 = sp.bytes('0x223344'), s = sp.list(['01', '234', '567', '89']), sb = sp.list([sp.bytes('0x1234'), sp.bytes('0x'), sp.bytes('0x4567aabbccdd')])))...
 -> (Pair (Some 0x1122334412344567aabbccdd) (Pair 2 (Pair 1 (Pair 0 (Pair (Some "0123456789") (Pair {} ""))))))
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).s0, sp.TOption(sp.TString))) == sp.pack(sp.set_type_expr(sp.some('0123456789'), sp.TOption(sp.TString)))...
 OK
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).b0, sp.TOption(sp.TBytes))) == sp.pack(sp.set_type_expr(sp.some(sp.bytes('0x1122334412344567aabbccdd')), sp.TOption(sp.TBytes)))...
 OK
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_013_cont_0_params.py 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_013_cont_0_params.tz 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_013_cont_0_params.json 1
Executing concatenating2(sp.record(b1 = sp.bytes('0xaaaa'), b2 = sp.bytes('0xab'), s1 = 'abc', s2 = 'def'))...
 -> (Pair (Some 0xaaaaab) (Pair 2 (Pair 1 (Pair 0 (Pair (Some "abcdef") (Pair {} ""))))))
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_014_cont_0_params.py 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_014_cont_0_params.tz 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_014_cont_0_params.json 1
Executing test_split('abc,def,ghi,,j')...
 -> (Pair (Some 0xaaaaab) (Pair 2 (Pair 1 (Pair 0 (Pair (Some "abcdef") (Pair {"abc"; "def"; "ghi"; ""; "j"} ""))))))
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_015_cont_0_params.py 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_015_cont_0_params.tz 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_015_cont_0_params.json 1
Executing test_string_of_nat(0)...
 -> (Pair (Some 0xaaaaab) (Pair 2 (Pair 1 (Pair 0 (Pair (Some "abcdef") (Pair {"abc"; "def"; "ghi"; ""; "j"} "0"))))))
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0).string_of_nat, sp.TString)) == sp.pack(sp.set_type_expr('0', sp.TString))...
 OK
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_017_cont_0_params.py 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_017_cont_0_params.tz 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_017_cont_0_params.json 1
Executing test_string_of_nat(12345678901234)...
 -> (Pair (Some 0xaaaaab) (Pair 2 (Pair 1 (Pair 0 (Pair (Some "abcdef") (Pair {"abc"; "def"; "ghi"; ""; "j"} "12345678901234"))))))
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_018_cont_0_params.py 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_018_cont_0_params.tz 1
 => test_baselines/scenario_via_js/stringManipulations/String_Manipulations/step_018_cont_0_params.json 1
Executing test_nat_of_string('12312345678901234123')...
 -> (Pair (Some 0xaaaaab) (Pair 2 (Pair 1 (Pair 12312345678901234123 (Pair (Some "abcdef") (Pair {"abc"; "def"; "ghi"; ""; "j"} "12345678901234"))))))
Computing sp.contract_data(0)...
 => sp.record(b0 = sp.some(sp.bytes('0xaaaaab')), l0 = 2, l1 = 1, nat_of_string = 12312345678901234123, s0 = sp.some('abcdef'), split = ['abc', 'def', 'ghi', '', 'j'], string_of_nat = '12345678901234')
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0), sp.TRecord(b0 = sp.TOption(sp.TBytes), l0 = sp.TNat, l1 = sp.TNat, nat_of_string = sp.TIntOrNat, s0 = sp.TOption(sp.TString), split = sp.TList(sp.TString), string_of_nat = sp.TString).layout(("b0", ("l0", ("l1", ("nat_of_string", ("s0", ("split", "string_of_nat"))))))))) == sp.pack(sp.set_type_expr(sp.record(b0 = sp.some(sp.bytes('0xaaaaab')), l0 = 2, l1 = 1, nat_of_string = 12312345678901234123, s0 = sp.some('abcdef'), split = sp.list(['abc', 'def', 'ghi', '', 'j']), string_of_nat = '12345678901234'), sp.TRecord(b0 = sp.TOption(sp.TBytes), l0 = sp.TNat, l1 = sp.TNat, nat_of_string = sp.TIntOrNat, s0 = sp.TOption(sp.TString), split = sp.TList(sp.TString), string_of_nat = sp.TString).layout(("b0", ("l0", ("l1", ("nat_of_string", ("s0", ("split", "string_of_nat")))))))))...
 OK
