import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TInt, sp.TTicket(sp.TInt))).layout("m"))

  @sp.entry_point
  def ep1(self):
    with sp.match_record(self.data, "data") as data:
      match_pair_test_ticket_61_fst, match_pair_test_ticket_61_snd = sp.match_tuple(sp.get_and_update(data.m, 42, sp.none), "match_pair_test_ticket_61_fst", "match_pair_test_ticket_61_snd")
      data.m = match_pair_test_ticket_61_snd

sp.add_compilation_target("test", Contract())