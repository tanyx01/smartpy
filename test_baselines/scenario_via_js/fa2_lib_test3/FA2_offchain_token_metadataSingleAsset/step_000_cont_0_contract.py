import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(administrator = sp.TAddress, last_token_id = sp.TNat, ledger = sp.TBigMap(sp.TAddress, sp.TNat), metadata = sp.TBigMap(sp.TString, sp.TBytes), operators = sp.TBigMap(sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), sp.TUnit), supply = sp.TNat, token_metadata = sp.TBigMap(sp.TNat, sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info")))).layout(("administrator", ("last_token_id", ("ledger", ("metadata", ("operators", ("supply", "token_metadata"))))))))
    self.init(administrator = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
              last_token_id = 1,
              ledger = {sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi') : 42},
              metadata = {'' : sp.bytes('0x697066733a2f2f6578616d706c65')},
              operators = {},
              supply = 42,
              token_metadata = {0 : sp.record(token_id = 0, token_info = {'decimals' : sp.bytes('0x31'), 'name' : sp.bytes('0x546f6b656e205a65726f'), 'symbol' : sp.bytes('0x546f6b30')})})

  @sp.entry_point
  def balance_of(self, params):
    sp.set_type(params, sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")))
    sp.set_type(params.requests, sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))))
    def f_x0(_x0):
      sp.verify(self.data.token_metadata.contains(_x0.token_id), 'FA2_TOKEN_UNDEFINED')
      sp.result(sp.record(request = _x0, balance = self.data.ledger.get(_x0.owner, default_value = 0)))
    sp.transfer(params.requests.map(sp.build_lambda(f_x0)), sp.tez(0), params.callback)

  @sp.entry_point
  def burn(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(amount = sp.TNat, from_ = sp.TAddress, token_id = sp.TNat).layout(("from_", ("token_id", "amount")))))
    sp.verify(True, 'FA2_TX_DENIED')
    sp.for action in params:
      sp.verify(self.data.token_metadata.contains(action.token_id), 'FA2_TOKEN_UNDEFINED')
      sp.verify((sp.sender == action.from_) | (self.data.operators.contains(sp.record(owner = action.from_, operator = sp.sender, token_id = action.token_id))), 'FA2_NOT_OPERATOR')
      self.data.ledger[action.from_] = sp.as_nat(self.data.ledger.get(action.from_, default_value = 0) - action.amount, message = 'FA2_INSUFFICIENT_BALANCE')
      compute_fa2_lib_782i = sp.local("compute_fa2_lib_782i", sp.is_nat(self.data.supply - action.amount))
      with compute_fa2_lib_782i.value.match_cases() as arg:
        with arg.match('Some') as Some:
          self.data.supply = Some
        with arg.match('None') as None:
          self.data.supply = 0


  @sp.entry_point
  def mint(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress).layout(("to_", "amount"))))
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    sp.for action in params:
      sp.verify(self.data.token_metadata.contains(0), 'FA2_TOKEN_UNDEFINED')
      self.data.supply += action.amount
      self.data.ledger[action.to_] = self.data.ledger.get(action.to_, default_value = 0) + action.amount

  @sp.entry_point
  def set_administrator(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.administrator = params

  @sp.entry_point
  def set_metadata(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.metadata = params

  @sp.entry_point
  def transfer(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))))
    sp.for transfer in params:
      sp.for tx in transfer.txs:
        sp.verify(self.data.token_metadata.contains(tx.token_id), 'FA2_TOKEN_UNDEFINED')
        sp.verify((sp.sender == transfer.from_) | (self.data.operators.contains(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id))), 'FA2_NOT_OPERATOR')
        sp.if tx.amount > 0:
          self.data.ledger[transfer.from_] = sp.as_nat(self.data.ledger.get(transfer.from_, default_value = 0) - tx.amount, message = 'FA2_INSUFFICIENT_BALANCE')
          self.data.ledger[tx.to_] = self.data.ledger.get(tx.to_, default_value = 0) + tx.amount

  @sp.entry_point
  def update_operators(self, params):
    sp.set_type(params, sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))))
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('add_operator') as add_operator:
          sp.verify(add_operator.owner == sp.sender, 'FA2_NOT_OWNER')
          self.data.operators[add_operator] = sp.unit
        with arg.match('remove_operator') as remove_operator:
          sp.verify(remove_operator.owner == sp.sender, 'FA2_NOT_OWNER')
          del self.data.operators[remove_operator]


  @sp.entry_point
  def withdraw_mutez(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    sp.send(params.destination, params.amount)

sp.add_compilation_target("test", Contract())