Comment...
 h1: FA2_pause_SingleAsset
Table Of Contents

 FA2_pause_SingleAsset
# Accounts
# FA2 Contract
# Transfer without pause
# Update_operator without pause
# Pause entrypoint
## Non admin cannot set pause
## Admin set pause
# Transfer fails with pause
# Update_operator fails with pause
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Bob")])...
 => [sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edskRqFp3Z9AqoKrMNFb9bnWNwEsRzbjqjBhzmFMLF9UqB6VBmw7F8ppTiXaAnHtysmi6xFxoHf6rMUz6Y1ipiDz2EgwZQv3pa')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edskRijgcXx8gzqkq7SCBbrb6aDZQMmP6dznCQWgU1Jr4qPfJT1yFq5A39ja9G4wahS8uWtBurZy14Hy7GZkQh7WnopJTKtCQG')), sp.record(seed = 'Bob', address = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), public_key = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT'), public_key_hash = sp.key_hash('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), secret_key = sp.secret_key('edskRq1xuW7TCYzdFm1JQLi1Hz4MNDVP6ukQHVEEh3bVqyuzv7pXXjaGsXZuMbwtd3kQFp3LQ7GQzkLeprNEijKhQKzsxrYrUz'))]
Comment...
 h2: FA2 Contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 42} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair False (Pair 42 {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30})})))))))
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_005_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_005_cont_0_storage.json 61
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_005_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_005_cont_0_storage.py 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_005_cont_0_types.py 7
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_005_cont_0_metadata.metadata_base.json 168
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_005_cont_0_contract.tz 610
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_005_cont_0_contract.json 838
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_005_cont_0_contract.py 97
Comment...
 h2: Transfer without pause
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_007_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_007_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_007_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 0)]))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 42} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair False (Pair 42 {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30})})))))))
Comment...
 h2: Update_operator without pause
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_009_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_009_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_009_cont_0_params.json 20
Executing update_operators(sp.list([variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Alice").address), token_id = 0)), variant('remove_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Alice").address), token_id = 0))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 42} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair False (Pair 42 {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30})})))))))
Comment...
 h2: Pause entrypoint
Comment...
 h3: Non admin cannot set pause
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_012_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_012_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_012_cont_0_params.json 1
Executing set_pause(True)...
 -> --- Expected failure in transaction --- Wrong condition: (sp.sender == self.data.administrator : sp.TBool) (templates/fa2_lib.py, line 513)
Message: 'FA2_NOT_ADMIN'
 (templates/fa2_lib.py, line 149)
Comment...
 h3: Admin set pause
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_014_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_014_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_014_cont_0_params.json 1
Executing set_pause(True)...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 (Pair {Elt "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 42} (Pair {Elt "" 0x697066733a2f2f6578616d706c65} (Pair {} (Pair True (Pair 42 {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30})})))))))
Comment...
 h2: Transfer fails with pause
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_016_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_016_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_016_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 0)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: (~ self.data.paused : sp.TBool) (templates/fa2_lib.py, line 155)
Message: ('FA2_TX_DENIED', 'FA2_PAUSED')
 (templates/fa2_lib.py, line 155)
Comment...
 h2: Update_operator fails with pause
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_018_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_018_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_test3/FA2_pause_SingleAsset/step_018_cont_0_params.json 20
Executing update_operators(sp.list([variant('add_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Alice").address), token_id = 0)), variant('remove_operator', sp.record(owner = sp.resolve(sp.test_account("Alice").address), operator = sp.resolve(sp.test_account("Alice").address), token_id = 0))]))...
 -> --- Expected failure in transaction --- Wrong condition: (~ self.data.paused : sp.TBool) (templates/fa2_lib.py, line 160)
Message: ('FA2_OPERATORS_UNSUPPORTED', 'FA2_PAUSED')
 (templates/fa2_lib.py, line 159)
