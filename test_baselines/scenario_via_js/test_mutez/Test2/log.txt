Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> None
 => test_baselines/scenario_via_js/test_mutez/Test2/step_000_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_000_cont_0_storage.json 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/test_mutez/Test2/step_000_cont_0_storage.py 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_000_cont_0_types.py 7
 => test_baselines/scenario_via_js/test_mutez/Test2/step_000_cont_0_contract.tz 44
 => test_baselines/scenario_via_js/test_mutez/Test2/step_000_cont_0_contract.json 44
 => test_baselines/scenario_via_js/test_mutez/Test2/step_000_cont_0_contract.py 20
 => test_baselines/scenario_via_js/test_mutez/Test2/step_001_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_001_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_001_cont_0_params.json 1
Executing store(sp.mutez(42))...
 -> (Some 42)
 => test_baselines/scenario_via_js/test_mutez/Test2/step_002_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_002_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_002_cont_0_params.json 1
Executing decrease(sp.mutez(22))...
 -> (Some 20)
 => test_baselines/scenario_via_js/test_mutez/Test2/step_003_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_003_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_003_cont_0_params.json 1
Executing decrease(sp.mutez(22))...
 -> --- Expected failure in transaction --- Invalid - operation on mutez (negative result)  sp.mutez(20) sp.mutez(22)
 => test_baselines/scenario_via_js/test_mutez/Test2/step_004_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_004_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_004_cont_0_params.json 1
Executing sub(sp.record(x = sp.mutez(1), y = sp.mutez(2)))...
 -> None
Verifying sp.contract_data(0).result == sp.none...
 OK
 => test_baselines/scenario_via_js/test_mutez/Test2/step_006_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_006_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_mutez/Test2/step_006_cont_0_params.json 1
Executing sub(sp.record(x = sp.mutez(100), y = sp.mutez(1)))...
 -> (Some 99)
Verifying sp.contract_data(0).result == sp.some(sp.mutez(99))...
 OK
