Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair 2 (Pair "aaa" 0))
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_000_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_000_cont_0_storage.json 1
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_000_cont_0_storage.py 1
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_000_cont_0_types.py 7
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_000_cont_0_contract.tz 171
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_000_cont_0_contract.json 205
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_000_cont_0_contract.py 37
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_001_cont_0_params.py 1
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_001_cont_0_params.tz 1
 => test_baselines/scenario_via_js/sub_entry_point/Sub/step_001_cont_0_params.json 1
Executing f(sp.record())...
 -> (Pair 4 (Pair "aaa" 55))
  + Transfer
     params: sp.unit
     amount: sp.tez(5)
     to:     sp.contract(sp.TUnit, sp.address('tz1h4EsGunH2Ue1T2uNs8mfKZ8XZoQji3HcK')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz1hJgZdhnRGvg5XD6pYxRCsbWh4jg5HQ476')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(10)
     to:     sp.contract(sp.TUnit, sp.address('tz1h4EsGunH2Ue1T2uNs8mfKZ8XZoQji3HcK')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(2)
     to:     sp.contract(sp.TUnit, sp.address('tz1hJgZdhnRGvg5XD6pYxRCsbWh4jg5HQ476')).open_some()
Verifying sp.contract_data(0) == sp.record(x = 4, y = 'aaa', z = 55)...
 OK
