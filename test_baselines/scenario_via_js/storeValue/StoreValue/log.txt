Comment...
 h1: Store Value
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> 12
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_001_cont_0_storage.json 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_001_cont_0_storage.py 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_001_cont_0_types.py 7
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_001_cont_0_contract.tz 51
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_001_cont_0_contract.json 51
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_001_cont_0_contract.py 21
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_002_cont_0_params.py 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_002_cont_0_params.tz 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_002_cont_0_params.json 1
Executing replace(sp.record(value = 15))...
 -> 15
Comment...
 p: Some computation
Computing sp.contract_data(0).storedValue * 12...
 => 180
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_005_cont_0_params.py 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_005_cont_0_params.tz 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_005_cont_0_params.json 1
Executing replace(sp.record(value = 25))...
 -> 25
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_006_cont_0_params.py 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_006_cont_0_params.tz 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_006_cont_0_params.json 1
Executing double(sp.record())...
 -> 50
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_007_cont_0_params.py 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_007_cont_0_params.tz 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_007_cont_0_params.json 1
Executing divide(sp.record(divisor = 2))...
 -> --- Expected failure in transaction --- Wrong condition: (params.divisor > 5 : sp.TBool) (python/templates/storeValue.py, line 19)
 (python/templates/storeValue.py, line 19)
Verifying sp.contract_data(0).storedValue == 50...
 OK
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_009_cont_0_params.py 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_009_cont_0_params.tz 1
 => test_baselines/scenario_via_js/storeValue/StoreValue/step_009_cont_0_params.json 1
Executing divide(sp.record(divisor = 6))...
 -> 8
Verifying sp.contract_data(0).storedValue == 8...
 OK
