Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> Unit
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_000_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_000_cont_0_storage.json 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_000_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_000_cont_0_storage.py 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_000_cont_0_types.py 7
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_000_cont_0_contract.tz 53
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_000_cont_0_contract.json 66
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_000_cont_0_contract.py 20
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_001_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_001_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_001_cont_0_params.json 1
Executing deposit(sp.record())...
 -> Unit
Verifying sp.contract_balance(0) == sp.mutez(1100)...
 OK
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_003_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_003_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_003_cont_0_params.json 1
Executing withdraw(sp.record(address = sp.resolve(sp.test_account("test").address), quantity = sp.mutez(200)))...
 -> Unit
  + Transfer
     params: sp.unit
     amount: sp.mutez(200)
     to:     sp.contract(sp.TUnit, sp.address('tz1WMLfXe5mw9Y3HfReqKk8Twz6s6ZqHiQCm')).open_some()
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_004_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_004_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_004_cont_0_params.json 1
Executing withdraw(sp.record(address = sp.resolve(sp.test_account("test").address), quantity = sp.mutez(2000)))...
 -> Unit
  + Transfer
     params: sp.unit
     amount: sp.mutez(2000)
     to:     sp.contract(sp.TUnit, sp.address('tz1WMLfXe5mw9Y3HfReqKk8Twz6s6ZqHiQCm')).open_some()
 -> --- Expected failure in transaction --- Negative balance for KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 (python/templates/test_transfers_to_accounts.py, line 26)
Verifying sp.contract_balance(0) == sp.mutez(900)...
 OK
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_006_cont_0_params.py 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_006_cont_0_params.tz 1
 => test_baselines/scenario_via_js/test_transfers_to_accounts/Test/step_006_cont_0_params.json 1
Executing withdraw_all(sp.resolve(sp.test_account("test").address))...
 -> Unit
  + Transfer
     params: sp.unit
     amount: sp.mutez(900)
     to:     sp.contract(sp.TUnit, sp.address('tz1WMLfXe5mw9Y3HfReqKk8Twz6s6ZqHiQCm')).open_some()
Verifying sp.contract_balance(0) == sp.tez(0)...
 OK
