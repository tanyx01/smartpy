import smartpy as sp

tstorage = sp.TRecord(worker_contract_address = sp.TAddress).layout("worker_contract_address")
tparameter = sp.TVariant(append_multiple_messages = sp.TRecord(messages = sp.TList(sp.TString), separator = sp.TString).layout(("messages", "separator")), store_single_message = sp.TString).layout(("append_multiple_messages", "store_single_message"))
tprivates = { }
tviews = { }
