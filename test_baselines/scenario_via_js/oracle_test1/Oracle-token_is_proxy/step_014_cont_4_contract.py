import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, escrow = sp.TAddress, job_id = sp.TBytes, next_request_id = sp.TNat, oracle = sp.TAddress, receiver = sp.TOption(sp.TAddress), token = sp.TAddress).layout(("admin", ("escrow", ("job_id", ("next_request_id", ("oracle", ("receiver", "token"))))))))
    self.init(admin = sp.address('tz1NiCaziaWuZWMTYuiXX6ceM5A42Ckd6yop'),
              escrow = sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H'),
              job_id = sp.bytes('0x0001'),
              next_request_id = 1,
              oracle = sp.address('KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC'),
              receiver = sp.none,
              token = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'))

  @sp.entry_point
  def cancel_value(self, params):
    sp.verify(sp.sender == self.data.admin, 'RequesterNotAdmin')
    sp.transfer(sp.record(client_request_id = sp.as_nat(self.data.next_request_id - 1), force = params.force, oracle = self.data.oracle), sp.tez(0), sp.contract(sp.TRecord(client_request_id = sp.TNat, force = sp.TBool, oracle = sp.TAddress).layout(("client_request_id", ("force", "oracle"))), self.data.escrow, entry_point='cancel_request').open_some())

  @sp.entry_point
  def request_value(self, params):
    sp.verify(sp.sender == self.data.admin, 'RequesterNotAdmin')
    ticket_431 = sp.local("ticket_431", sp.ticket(sp.record(cancel_timeout = sp.add_seconds(sp.now, params.cancel_timeout_minutes * 60), client_request_id = self.data.next_request_id, fulfill_timeout = sp.add_seconds(sp.now, params.fulfill_timeout_minutes * 60), job_id = self.data.job_id, oracle = self.data.oracle, parameters = params.parameters, tag = 'OracleRequest', target = sp.to_address(sp.contract(sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")), self.data.receiver.open_some(message = 'RequesterTargetUnknown'), entry_point='set_value').open_some(message = 'RequesterTargetUnknown'))), 1))
    sp.transfer(sp.record(amount = params.amount, escrow = self.data.escrow, request = ticket_431.value), sp.tez(0), sp.contract(sp.TRecord(amount = sp.TNat, escrow = sp.TAddress, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))))).layout(("amount", ("escrow", "request"))), self.data.token, entry_point='proxy').open_some())
    self.data.next_request_id += 1

  @sp.entry_point
  def set_receiver(self, params):
    sp.verify(sp.sender == self.data.admin, 'RequesterNotAdmin')
    self.data.receiver = sp.some(params)

  @sp.entry_point
  def setup(self, params):
    sp.verify(sp.sender == self.data.admin, 'RequesterNotAdmin')
    self.data.admin = params.admin
    self.data.escrow = params.escrow
    self.data.oracle = params.oracle
    self.data.job_id = params.job_id
    self.data.token = params.token

sp.add_compilation_target("test", Contract())