import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(administrator = sp.TAddress, all_tokens = sp.TNat, ledger = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TNat).layout("balance")), locked = sp.TBigMap(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat).layout(("client", "client_request_id")), sp.TRecord(amount = sp.TNat, cancel_timeout = sp.TTimestamp).layout(("amount", "cancel_timeout"))), metadata = sp.TBigMap(sp.TString, sp.TBytes), operators = sp.TBigMap(sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), sp.TUnit), paused = sp.TBool, token_metadata = sp.TBigMap(sp.TNat, sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info"))), total_supply = sp.TBigMap(sp.TNat, sp.TNat)).layout(("administrator", ("all_tokens", ("ledger", ("locked", ("metadata", ("operators", ("paused", ("token_metadata", "total_supply"))))))))))
    self.init(administrator = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
              all_tokens = 0,
              ledger = {},
              locked = {},
              metadata = {'' : sp.bytes('0x')},
              operators = {},
              paused = False,
              token_metadata = {},
              total_supply = {})

  @sp.entry_point
  def balance_of(self, params):
    sp.verify(~ self.data.paused, 'FA2_PAUSED')
    sp.set_type(params, sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")))
    def f_x0(_x0):
      sp.verify(self.data.token_metadata.contains(_x0.token_id), 'FA2_TOKEN_UNDEFINED')
      sp.if self.data.ledger.contains(sp.set_type_expr(_x0.owner, sp.TAddress)):
        sp.result(sp.record(request = sp.record(owner = sp.set_type_expr(_x0.owner, sp.TAddress), token_id = sp.set_type_expr(_x0.token_id, sp.TNat)), balance = self.data.ledger[sp.set_type_expr(_x0.owner, sp.TAddress)].balance))
      sp.else:
        sp.result(sp.record(request = sp.record(owner = sp.set_type_expr(_x0.owner, sp.TAddress), token_id = sp.set_type_expr(_x0.token_id, sp.TNat)), balance = 0))
    responses = sp.local("responses", params.requests.map(sp.build_lambda(f_x0)))
    sp.transfer(responses.value, sp.tez(0), sp.set_type_expr(params.callback, sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))))))

  @sp.entry_point
  def cancel_request(self, params):
    compute_oracle_304 = sp.local("compute_oracle_304", sp.record(client = sp.sender, client_request_id = params.client_request_id))
    sp.verify(self.data.locked.contains(compute_oracle_304.value), 'EscrowRequestIdUnknownForClient')
    sp.verify(sp.now >= self.data.locked[compute_oracle_304.value].cancel_timeout, 'EscrowCantCancelBeforeTimeout')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = sp.sender, token_id = 0, amount = self.data.locked[compute_oracle_304.value].amount)]))]), sp.tez(0), sp.self_entry_point('transfer'))
    del self.data.locked[compute_oracle_304.value]
    sp.if ~ params.force:
      sp.transfer(compute_oracle_304.value, sp.tez(0), sp.contract(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat).layout(("client", "client_request_id")), params.oracle, entry_point='cancel_request').open_some(message = 'EscrowOracleNotFound'))

  @sp.entry_point
  def force_fulfill_request(self, params):
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")))
    request_oracle_324, result_oracle_324 = sp.match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = sp.match_tuple(sp.read_ticket_raw(request_oracle_324), "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = sp.match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = sp.match_tuple(sp.read_ticket_raw(result_oracle_324), "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = sp.match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    compute_oracle_330 = sp.local("compute_oracle_330", sp.record(client = ticket_oracle_325_ticketer, client_request_id = ticket_oracle_325_content.client_request_id))
    sp.verify(self.data.locked.contains(compute_oracle_330.value), 'EscrowRequestUnknown')
    sp.verify(ticket_oracle_325_content.fulfill_timeout >= sp.now, 'EscrowCantFulfillAfterTimeout')
    sp.verify(ticket_oracle_325_content.tag == 'OracleRequest', 'TicketExpectedTag:OracleRequest')
    sp.verify(ticket_oracle_326_content.tag == 'OracleResult', 'TicketExpectedTag:OracleResult')
    sp.verify(ticket_oracle_325_ticketer == ticket_oracle_326_content.client, 'TicketClientNotMatch')
    sp.verify(ticket_oracle_326_ticketer == ticket_oracle_325_content.oracle, 'TicketOracleNotMatch')
    sp.verify(ticket_oracle_325_content.client_request_id == ticket_oracle_326_content.client_request_id, 'TicketClientRequestIdNotMatch')
    sp.verify(sp.sender == ticket_oracle_326_ticketer, 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = ticket_oracle_325_content.oracle, token_id = 0, amount = self.data.locked[compute_oracle_330.value].amount)]))]), sp.tez(0), sp.self_entry_point('transfer'))
    del self.data.locked[compute_oracle_330.value]

  @sp.entry_point
  def fulfill_request(self, params):
    sp.set_type(params, sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")))
    request_oracle_324, result_oracle_324 = sp.match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = sp.match_tuple(sp.read_ticket_raw(request_oracle_324), "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = sp.match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = sp.match_tuple(sp.read_ticket_raw(result_oracle_324), "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = sp.match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    compute_oracle_330i = sp.local("compute_oracle_330i", sp.record(client = ticket_oracle_325_ticketer, client_request_id = ticket_oracle_325_content.client_request_id))
    sp.verify(self.data.locked.contains(compute_oracle_330i.value), 'EscrowRequestUnknown')
    sp.verify(ticket_oracle_325_content.fulfill_timeout >= sp.now, 'EscrowCantFulfillAfterTimeout')
    sp.verify(ticket_oracle_325_content.tag == 'OracleRequest', 'TicketExpectedTag:OracleRequest')
    sp.verify(ticket_oracle_326_content.tag == 'OracleResult', 'TicketExpectedTag:OracleResult')
    sp.verify(ticket_oracle_325_ticketer == ticket_oracle_326_content.client, 'TicketClientNotMatch')
    sp.verify(ticket_oracle_326_ticketer == ticket_oracle_325_content.oracle, 'TicketOracleNotMatch')
    sp.verify(ticket_oracle_325_content.client_request_id == ticket_oracle_326_content.client_request_id, 'TicketClientRequestIdNotMatch')
    sp.verify(sp.sender == ticket_oracle_326_ticketer, 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = ticket_oracle_325_content.oracle, token_id = 0, amount = self.data.locked[compute_oracle_330i.value].amount)]))]), sp.tez(0), sp.self_entry_point('transfer'))
    del self.data.locked[compute_oracle_330i.value]
    sp.transfer(sp.record(request = ticket_oracle_325_copy, result = ticket_oracle_326_copy), sp.tez(0), sp.contract(sp.TRecord(request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target"))))))))), result = sp.TTicket(sp.TRecord(client = sp.TAddress, client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))), tag = sp.TString).layout(("client", ("client_request_id", ("result", "tag")))))).layout(("request", "result")), ticket_oracle_325_content.target).open_some(message = 'EscrowTargetNotFound'))

  @sp.entry_point
  def mint(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    sp.verify(params.token_id == 0, 'single-asset: token-id <> 0')
    sp.if self.data.ledger.contains(sp.set_type_expr(params.address, sp.TAddress)):
      self.data.ledger[sp.set_type_expr(params.address, sp.TAddress)].balance += params.amount
    sp.else:
      self.data.ledger[sp.set_type_expr(params.address, sp.TAddress)] = sp.record(balance = params.amount)
    sp.if ~ (params.token_id < self.data.all_tokens):
      sp.verify(self.data.all_tokens == params.token_id, 'Token-IDs should be consecutive')
      self.data.all_tokens = params.token_id + 1
      self.data.token_metadata[params.token_id] = sp.record(token_id = params.token_id, token_info = params.metadata)
    self.data.total_supply[params.token_id] = params.amount + self.data.total_supply.get(params.token_id, default_value = 0)

  @sp.entry_point
  def send_request(self, params):
    sp.set_type(params, sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))))).layout(("amount", "request")))
    amount_oracle_273, request_oracle_273 = sp.match_record(params, "amount", "request")
    ticket_oracle_276_data, ticket_oracle_276_copy = sp.match_tuple(sp.read_ticket_raw(request_oracle_273), "ticket_oracle_276_data", "ticket_oracle_276_copy")
    ticket_oracle_276_ticketer, ticket_oracle_276_content, ticket_oracle_276_amount = sp.match_tuple(ticket_oracle_276_data, "ticket_oracle_276_ticketer", "ticket_oracle_276_content", "ticket_oracle_276_amount")
    sp.verify(sp.sender == ticket_oracle_276_ticketer, 'EscrowSenderAndTicketerNotMatch')
    sp.transfer(sp.list([sp.record(from_ = sp.sender, txs = sp.list([sp.record(to_ = sp.self_address, token_id = 0, amount = amount_oracle_273)]))]), sp.tez(0), sp.self_entry_point('transfer'))
    sp.verify(ticket_oracle_276_content.tag == 'OracleRequest', 'TicketExpectedTag:OracleRequest')
    compute_oracle_292 = sp.local("compute_oracle_292", sp.record(client = ticket_oracle_276_ticketer, client_request_id = ticket_oracle_276_content.client_request_id))
    sp.verify(~ (self.data.locked.contains(compute_oracle_292.value)), 'EscrowRequestIdAlreadyKnownForClient')
    self.data.locked[compute_oracle_292.value] = sp.record(amount = amount_oracle_273, cancel_timeout = ticket_oracle_276_content.cancel_timeout)
    sp.transfer(sp.record(amount = amount_oracle_273, request = ticket_oracle_276_copy), sp.tez(0), sp.contract(sp.TRecord(amount = sp.TNat, request = sp.TTicket(sp.TRecord(cancel_timeout = sp.TTimestamp, client_request_id = sp.TNat, fulfill_timeout = sp.TTimestamp, job_id = sp.TBytes, oracle = sp.TAddress, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), tag = sp.TString, target = sp.TAddress).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))))).layout(("amount", "request")), ticket_oracle_276_content.oracle, entry_point='create_request').open_some(message = 'EscrowOracleNotFound'))

  @sp.entry_point
  def set_administrator(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.administrator = params

  @sp.entry_point
  def set_metadata(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.metadata[params.k] = params.v

  @sp.entry_point
  def set_pause(self, params):
    sp.verify(sp.sender == self.data.administrator, 'FA2_NOT_ADMIN')
    self.data.paused = params

  @sp.entry_point
  def transfer(self, params):
    sp.verify(~ self.data.paused, 'FA2_PAUSED')
    sp.set_type(params, sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))))
    sp.for transfer in params:
      sp.for tx in transfer.txs:
        sp.verify(tx.token_id == 0, 'single-asset: token-id <> 0')
        sp.verify((((sp.sender == self.data.administrator) | (transfer.from_ == sp.sender)) | (self.data.operators.contains(sp.set_type_expr(sp.record(owner = transfer.from_, operator = sp.sender, token_id = tx.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))))))) | (sp.sender == sp.self_address), 'FA2_NOT_OPERATOR')
        sp.verify(self.data.token_metadata.contains(tx.token_id), 'FA2_TOKEN_UNDEFINED')
        sp.if tx.amount > 0:
          sp.verify(self.data.ledger[sp.set_type_expr(transfer.from_, sp.TAddress)].balance >= tx.amount, 'FA2_INSUFFICIENT_BALANCE')
          self.data.ledger[sp.set_type_expr(transfer.from_, sp.TAddress)].balance = sp.as_nat(self.data.ledger[sp.set_type_expr(transfer.from_, sp.TAddress)].balance - tx.amount)
          sp.if self.data.ledger.contains(sp.set_type_expr(tx.to_, sp.TAddress)):
            self.data.ledger[sp.set_type_expr(tx.to_, sp.TAddress)].balance += tx.amount
          sp.else:
            self.data.ledger[sp.set_type_expr(tx.to_, sp.TAddress)] = sp.record(balance = tx.amount)

  @sp.entry_point
  def update_operators(self, params):
    sp.set_type(params, sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))))
    sp.for update in params:
      with update.match_cases() as arg:
        with arg.match('add_operator') as add_operator:
          sp.verify((add_operator.owner == sp.sender) | (sp.sender == self.data.administrator), 'FA2_NOT_ADMIN_OR_OPERATOR')
          self.data.operators[sp.set_type_expr(sp.record(owner = add_operator.owner, operator = add_operator.operator, token_id = add_operator.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))))] = sp.unit
        with arg.match('remove_operator') as remove_operator:
          sp.verify((remove_operator.owner == sp.sender) | (sp.sender == self.data.administrator), 'FA2_NOT_ADMIN_OR_OPERATOR')
          del self.data.operators[sp.set_type_expr(sp.record(owner = remove_operator.owner, operator = remove_operator.operator, token_id = remove_operator.token_id), sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))))]


sp.add_compilation_target("test", Contract())