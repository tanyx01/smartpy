import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def receive_and_check(self, params):
    sp.verify(params.sender_balance == sp.view("get_balance", sp.unit, sp.sender, sp.TMutez).open_some())
    sp.verify(params.receiver_balance == sp.balance)

sp.add_compilation_target("test", Contract())