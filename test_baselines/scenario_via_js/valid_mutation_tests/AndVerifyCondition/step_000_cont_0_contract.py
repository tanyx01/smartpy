import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TIntOrNat)
    self.init(0)

  @sp.entry_point
  def set_data(self, params):
    sp.if params.a & params.b:
      self.data += 1

sp.add_compilation_target("test", Contract())