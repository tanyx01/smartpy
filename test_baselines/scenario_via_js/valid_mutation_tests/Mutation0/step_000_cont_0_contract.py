import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TInt)
    self.init(0)

  @sp.entry_point
  def decrement(self):
    self.data -= 1

  @sp.entry_point
  def increment(self):
    sp.failwith(sp.unit)

sp.add_compilation_target("test", Contract())