import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TIntOrNat)
    self.init(0)

  @sp.entry_point
  def fail(self):
    sp.failwith(sp.unit)

sp.add_compilation_target("test", Contract())