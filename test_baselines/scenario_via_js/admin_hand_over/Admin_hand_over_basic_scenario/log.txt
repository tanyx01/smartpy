Comment...
 h1: Basic scenario.
Comment...
 h2: Origination.
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> {"tz1NajGagueXKGFqf9msG66qsAreKnPiFuEb"}
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_002_cont_0_storage.json 1
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_002_cont_0_storage.py 1
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_002_cont_0_types.py 7
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_002_cont_0_contract.tz 91
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_002_cont_0_contract.json 105
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_002_cont_0_contract.py 25
Comment...
 h2: An admin adds a new admin.
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_004_cont_0_params.py 1
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_004_cont_0_params.tz 1
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_004_cont_0_params.json 1
Executing add_admins(sp.list([sp.resolve(sp.test_account("new_admin").address)]))...
 -> {"tz1NajGagueXKGFqf9msG66qsAreKnPiFuEb"; "tz1ZEZr6xyjSiQFyCA7y3fVdz6fckLT9gJdG"}
Comment...
 h2: The new admin removes the older admin.
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_006_cont_0_params.py 1
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_006_cont_0_params.tz 1
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_006_cont_0_params.json 1
Executing remove_admins(sp.list([sp.resolve(sp.test_account("first_admin").address)]))...
 -> {"tz1ZEZr6xyjSiQFyCA7y3fVdz6fckLT9gJdG"}
Comment...
 h2: The new admin calls the protected entrypoint
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_008_cont_0_params.py 1
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_008_cont_0_params.tz 1
 => test_baselines/scenario_via_js/admin_hand_over/Admin_hand_over_basic_scenario/step_008_cont_0_params.json 1
Executing protected(sp.record())...
 -> {"tz1ZEZr6xyjSiQFyCA7y3fVdz6fckLT9gJdG"}
