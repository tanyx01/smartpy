import smartpy as sp

tstorage = sp.TRecord(admins = sp.TSet(sp.TAddress)).layout("admins")
tparameter = sp.TVariant(add_admins = sp.TList(sp.TAddress), protected = sp.TUnit, remove_admins = sp.TList(sp.TAddress)).layout(("add_admins", ("protected", "remove_admins")))
tprivates = { }
tviews = { }
