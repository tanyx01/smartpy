Comment...
 h1: Syntax
Comment...
 h2: Contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair {1; 2; 3} (Pair {Some 123; None} (Pair {Elt 0 (Some 123); Elt 1 None} (Pair "toto" (Pair True (Pair {0; 1; 2; 3; 4; 5; 6; 7; 8; 9} (Pair False (Pair 0x0000ab112233aa (Pair 7 (Pair {} (Pair 123 (Pair "tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk" (Pair "abc" "ABC")))))))))))))
 => test_baselines/scenario_via_js/syntax/Syntax/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_002_cont_0_storage.json 83
 => test_baselines/scenario_via_js/syntax/Syntax/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/syntax/Syntax/step_002_cont_0_storage.py 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_002_cont_0_types.py 7
 => test_baselines/scenario_via_js/syntax/Syntax/step_002_cont_0_contract.tz 625
 => test_baselines/scenario_via_js/syntax/Syntax/step_002_cont_0_contract.json 674
 => test_baselines/scenario_via_js/syntax/Syntax/step_002_cont_0_contract.py 107
Comment...
 h2: Message execution
 => test_baselines/scenario_via_js/syntax/Syntax/step_004_cont_0_params.py 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_004_cont_0_params.tz 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_004_cont_0_params.json 1
Executing comparisons(sp.record())...
 -> --- Expected failure in transaction --- Wrong condition: ((2 + self.data.i) == 12 : sp.TBool) (python/templates/syntax.py, line 26)
 (python/templates/syntax.py, line 26)
Comment...
 h2: Message execution
 => test_baselines/scenario_via_js/syntax/Syntax/step_006_cont_0_params.py 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_006_cont_0_params.tz 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_006_cont_0_params.json 1
Executing someComputations(sp.record(x = 'abcd', y = 12))...
 -> (Pair {1; 2; 3} (Pair {Some 123; None} (Pair {Elt 0 (Some 123); Elt 1 None} (Pair "abcd" (Pair True (Pair {0; 1; 2; 3; 4; 5; 6; 7; 8; 9} (Pair False (Pair 0x0000ab112233aa (Pair 99 (Pair {} (Pair 123 (Pair "tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk" (Pair "abc" "ABC")))))))))))))
Comment...
 h2: Message execution
 => test_baselines/scenario_via_js/syntax/Syntax/step_008_cont_0_params.py 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_008_cont_0_params.tz 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_008_cont_0_params.json 1
Executing localVariable(sp.record())...
 -> (Pair {1; 2; 3} (Pair {Some 123; None} (Pair {Elt 0 (Some 123); Elt 1 None} (Pair "abcd" (Pair True (Pair {0; 1; 2; 3; 4; 5; 6; 7; 8; 9} (Pair False (Pair 0x0000ab112233aa (Pair 198 (Pair {} (Pair 123 (Pair "tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk" (Pair "abc" "ABC")))))))))))))
 => test_baselines/scenario_via_js/syntax/Syntax/step_009_cont_0_params.py 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_009_cont_0_params.tz 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_009_cont_0_params.json 1
Executing iterations(sp.record())...
 -> (Pair {1; 2; 3} (Pair {Some 123; None} (Pair {Elt 0 (Some 123); Elt 1 None} (Pair "abcd" (Pair True (Pair {0; 1; 2; 3; 4; 5; 6; 7; 8; 9} (Pair False (Pair 0x0000ab112233aa (Pair 55 (Pair {} (Pair 123 (Pair "tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk" (Pair "abc" "ABC")))))))))))))
Comment...
 h2: Message execution
 => test_baselines/scenario_via_js/syntax/Syntax/step_011_cont_0_params.py 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_011_cont_0_params.tz 1
 => test_baselines/scenario_via_js/syntax/Syntax/step_011_cont_0_params.json 1
Executing myMessageName6(-18)...
 -> (Pair {1; 3; 12; 18} (Pair {Some 16; Some 123; None} (Pair {Elt 0 (Some 16); Elt 1 None} (Pair "abcd" (Pair True (Pair {0; 1; 2; 3; 4; 5; 6; 7; 8; 9} (Pair False (Pair 0x0000ab112233aa (Pair 55 (Pair {Elt 42 43} (Pair 123 (Pair "tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk" (Pair "abc" "ABC")))))))))))))
Verifying sp.contract_data(0).b...
 OK
Verifying sp.contract_data(0).i == 55...
 OK
Computing sp.contract_data(0)...
 => sp.record(aaa = sp.set([1, 3, 12, 18]), abc = [sp.some(16), sp.some(123), sp.none], abca = {0 : sp.some(16), 1 : sp.none}, acb = 'abcd', b = True, ddd = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], f = False, h = sp.bytes('0x0000ab112233aa'), i = 55, m = {42 : 43}, n = 123, pkh = sp.key_hash('tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk'), s = 'abc', toto = 'ABC')
Computing sp.contract_data(0)...
 => sp.record(aaa = sp.set([1, 3, 12, 18]), abc = [sp.some(16), sp.some(123), sp.none], abca = {0 : sp.some(16), 1 : sp.none}, acb = 'abcd', b = True, ddd = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], f = False, h = sp.bytes('0x0000ab112233aa'), i = 55, m = {42 : 43}, n = 123, pkh = sp.key_hash('tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk'), s = 'abc', toto = 'ABC')
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0), sp.TRecord(aaa = sp.TSet(sp.TNat), abc = sp.TList(sp.TOption(sp.TIntOrNat)), abca = sp.TMap(sp.TIntOrNat, sp.TOption(sp.TIntOrNat)), acb = sp.TString, b = sp.TBool, ddd = sp.TList(sp.TIntOrNat), f = sp.TBool, h = sp.TBytes, i = sp.TInt, m = sp.TMap(sp.TInt, sp.TInt), n = sp.TNat, pkh = sp.TKeyHash, s = sp.TString, toto = sp.TString).layout(("aaa", ("abc", ("abca", ("acb", ("b", ("ddd", ("f", ("h", ("i", ("m", ("n", ("pkh", ("s", "toto")))))))))))))))) == sp.pack(sp.set_type_expr(sp.record(aaa = sp.set([1, 3, 12, 18]), abc = sp.list([sp.some(16), sp.some(123), sp.none]), abca = sp.set_type_expr({0 : sp.some(16), 1 : sp.none}, sp.TMap(sp.TIntOrNat, sp.TOption(sp.TIntOrNat))), acb = 'abcd', b = True, ddd = sp.list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]), f = False, h = sp.bytes('0x0000ab112233aa'), i = 55, m = {42 : 43}, n = 123, pkh = sp.key_hash('tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk'), s = 'abc', toto = 'ABC'), sp.TRecord(aaa = sp.TSet(sp.TNat), abc = sp.TList(sp.TOption(sp.TIntOrNat)), abca = sp.TMap(sp.TIntOrNat, sp.TOption(sp.TIntOrNat)), acb = sp.TString, b = sp.TBool, ddd = sp.TList(sp.TIntOrNat), f = sp.TBool, h = sp.TBytes, i = sp.TInt, m = sp.TMap(sp.TInt, sp.TInt), n = sp.TNat, pkh = sp.TKeyHash, s = sp.TString, toto = sp.TString).layout(("aaa", ("abc", ("abca", ("acb", ("b", ("ddd", ("f", ("h", ("i", ("m", ("n", ("pkh", ("s", "toto"))))))))))))))))...
 OK
Comment...
 p: No interactive simulation available out of browser.
