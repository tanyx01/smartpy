import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(metadata = sp.TBigMap(sp.TString, sp.TBytes), x = sp.TIntOrNat).layout(("metadata", "x")))
    self.init(metadata = {'' : sp.bytes('0x697066733a2f2f516d65394c3479365a76507751746169734e4754554537566a55375052746e4a4673384e6a4e797a744533644754')},
              x = 1)

  @sp.entry_point
  def change_metadata(self, params):
    self.data.metadata[''] = params

  @sp.entry_point
  def incr(self):
    self.data.x += 1

sp.add_compilation_target("test", Contract())