import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat).layout("x"))
    self.init(x = 3)

  @sp.private_lambda()
  def sub(_x4):
    self.data.x = 3

sp.add_compilation_target("test", Contract())