import smartpy as sp

tstorage = sp.TRecord(x = sp.TOption(sp.TInt)).layout("x")
tparameter = sp.TVariant(map = sp.TLambda(sp.TInt, sp.TInt), set = sp.TOption(sp.TInt)).layout(("map", "set"))
tprivates = { }
tviews = { }
