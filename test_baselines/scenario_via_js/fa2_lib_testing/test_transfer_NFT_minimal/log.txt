Comment...
 h1: test_transfer_NFT_minimal
Table Of Contents

 test_transfer_NFT_minimal
# Accounts
# Contract
# Zero amount transfer
# Transfers Alice -> Bob
# Insufficient balance
# Same address transfer
# Not defined token
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Bob")])...
 => [sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edskRqFp3Z9AqoKrMNFb9bnWNwEsRzbjqjBhzmFMLF9UqB6VBmw7F8ppTiXaAnHtysmi6xFxoHf6rMUz6Y1ipiDz2EgwZQv3pa')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edskRijgcXx8gzqkq7SCBbrb6aDZQMmP6dznCQWgU1Jr4qPfJT1yFq5A39ja9G4wahS8uWtBurZy14Hy7GZkQh7WnopJTKtCQG')), sp.record(seed = 'Bob', address = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), public_key = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT'), public_key_hash = sp.key_hash('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'), secret_key = sp.secret_key('edskRq1xuW7TCYzdFm1JQLi1Hz4MNDVP6ukQHVEEh3bVqyuzv7pXXjaGsXZuMbwtd3kQFp3LQ7GQzkLeprNEijKhQKzsxrYrUz'))]
Comment...
 h2: Contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_005_cont_0_storage.tz 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_005_cont_0_storage.json 87
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_005_cont_0_sizes.csv 2
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_005_cont_0_storage.py 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_005_cont_0_types.py 7
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_005_cont_0_metadata.metadata_base.json 142
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_005_cont_0_contract.tz 304
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_005_cont_0_contract.json 449
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_005_cont_0_contract.py 52
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 1)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 2)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Bob").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 0...
 OK
Comment...
 h2: Zero amount transfer
Comment...
 p: TZIP-12: Transfers of zero amount MUST be treated as normal transfers.
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_012_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_012_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_012_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Bob").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 0)]))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Bob").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 0...
 OK
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_015_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_015_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_015_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Bob").address), token_id = 0, amount = 0)]))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt 0 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 1 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"; Elt 2 "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi"} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Bob").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 0...
 OK
Comment...
 h2: Transfers Alice -> Bob
Comment...
 p: TZIP-12: Each transfer in the batch MUST decrement token balance
                of the source (from_) address by the amount of the transfer and
                increment token balance of the destination (to_) address by the
                amount of the transfer.
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_020_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_020_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_020_cont_0_params.json 19
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Bob").address), token_id = 0, amount = 1), sp.record(to_ = sp.resolve(sp.test_account("Bob").address), token_id = 1, amount = 1)])), sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Bob").address), token_id = 2, amount = 1)]))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt 0 "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C"; Elt 1 "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C"; Elt 2 "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C"} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 0...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Bob").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 1)).open_some(message = 'View get_balance is invalid!') == 0...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Bob").address), token_id = 1)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Verifying sp.contract_data(0).ledger[0] == sp.resolve(sp.test_account("Bob").address)...
 OK
Comment...
 h2: Insufficient balance
Comment...
 p: TIP-12: If the transfer amount exceeds current token balance of
                the source address, the whole transfer operation MUST fail with
                the error mnemonic "FA2_INSUFFICIENT_BALANCE".
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_029_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_029_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_029_cont_0_params.json 19
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Bob").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = sp.scenario_var(0) + 1), sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 0)])), sp.record(from_ = sp.resolve(sp.test_account("Bob").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Alice").address), token_id = 0, amount = 0)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: ((tx.amount == 1) & (self.data.ledger[tx.token_id] == transfer.from_) : sp.TBool) (templates/fa2_nft_minimal.py, line 92)
Message: 'FA2_INSUFFICIENT_BALANCE'
 (templates/fa2_nft_minimal.py, line 91)
Comment...
 h2: Same address transfer
Comment...
 p: TZIP-12: Transfers with the same address (from_ equals to_) MUST
                be treated as normal transfers.
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_032_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_032_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_032_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Bob").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Bob").address), token_id = 0, amount = sp.scenario_var(0))]))]))...
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair {Elt 0 "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C"; Elt 1 "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C"; Elt 2 "tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C"} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair 3 (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e205a65726f; Elt "symbol" 0x546f6b30}); Elt 1 (Pair 1 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e204f6e65; Elt "symbol" 0x546f6b31}); Elt 2 (Pair 2 {Elt "decimals" 0x31; Elt "name" 0x546f6b656e2054776f; Elt "symbol" 0x546f6b32})})))))
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 0...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Bob").address), token_id = 0)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Alice").address), token_id = 1)).open_some(message = 'View get_balance is invalid!') == 0...
 OK
Verifying sp.contract_view(0, "get_balance", sp.record(owner = sp.resolve(sp.test_account("Bob").address), token_id = 1)).open_some(message = 'View get_balance is invalid!') == 1...
 OK
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_037_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_037_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_037_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Bob").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Bob").address), token_id = 0, amount = sp.scenario_var(0) + 1)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: ((tx.amount == 1) & (self.data.ledger[tx.token_id] == transfer.from_) : sp.TBool) (templates/fa2_nft_minimal.py, line 92)
Message: 'FA2_INSUFFICIENT_BALANCE'
 (templates/fa2_nft_minimal.py, line 91)
Comment...
 h2: Not defined token
Comment...
 p: TZIP-12: If one of the specified token_ids is not defined within
                the FA2 contract, the entrypoint MUST fail with the error
                mnemonic "FA2_TOKEN_UNDEFINED".
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_040_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_040_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_040_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Bob").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Bob").address), token_id = 4, amount = 0)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: (tx.token_id < self.data.next_token_id : sp.TBool) (templates/fa2_nft_minimal.py, line 78)
Message: 'FA2_TOKEN_UNDEFINED'
 (templates/fa2_nft_minimal.py, line 78)
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_041_cont_0_params.py 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_041_cont_0_params.tz 1
 => test_baselines/scenario_via_js/fa2_lib_testing/test_transfer_NFT_minimal/step_041_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.resolve(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.resolve(sp.test_account("Bob").address), token_id = 4, amount = 1)]))]))...
 -> --- Expected failure in transaction --- Wrong condition: (tx.token_id < self.data.next_token_id : sp.TBool) (templates/fa2_nft_minimal.py, line 78)
Message: 'FA2_TOKEN_UNDEFINED'
 (templates/fa2_nft_minimal.py, line 78)
