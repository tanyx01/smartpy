import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(contract = sp.TAddress, result = sp.TOption(sp.TNat), result2 = sp.TOption(sp.TRecord(amount = sp.TMutez, balance = sp.TMutez, chain_id = sp.TChainId, level = sp.TNat, now = sp.TTimestamp, sender = sp.TAddress, source = sp.TAddress, total_voting_power = sp.TNat).layout(("amount", ("balance", ("chain_id", ("level", ("now", ("sender", ("source", "total_voting_power"))))))))), self_addr = sp.TOption(sp.TAddress), self_addr_2 = sp.TOption(sp.TAddress), self_addr_3 = sp.TOption(sp.TAddress)).layout(("contract", ("result", ("result2", ("self_addr", ("self_addr_2", "self_addr_3")))))))
    self.init(contract = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
              result = sp.none,
              result2 = sp.none,
              self_addr = sp.none,
              self_addr_2 = sp.none,
              self_addr_3 = sp.none)

  @sp.entry_point
  def check_view(self):
    compute_test_onchain_views_83 = sp.local("compute_test_onchain_views_83", sp.view("abc", sp.unit, sp.self_address, sp.TUnit))

  @sp.entry_point
  def failing_entry_point(self):
    sp.failwith('This is a failure')

  @sp.entry_point
  def store(self):
    self.data.self_addr = sp.some(sp.self_address)
    compute_test_onchain_views_66 = sp.local("compute_test_onchain_views_66", sp.view("view1", sp.unit, self.data.contract, sp.TRecord(self_addr = sp.TAddress, z = sp.TNat).layout(("self_addr", "z"))).open_some(message = 'Invalid view'))
    self.data.result2 = sp.view("a_really_good_name", sp.unit, sp.self_address, sp.TRecord(amount = sp.TMutez, balance = sp.TMutez, chain_id = sp.TChainId, level = sp.TNat, now = sp.TTimestamp, sender = sp.TAddress, source = sp.TAddress, total_voting_power = sp.TNat).layout(("amount", ("balance", ("chain_id", ("level", ("now", ("sender", ("source", "total_voting_power")))))))))
    self.data.result = sp.some(compute_test_onchain_views_66.value.z)
    self.data.self_addr_2 = sp.some(compute_test_onchain_views_66.value.self_addr)
    self.data.self_addr_3 = sp.some(sp.self_address)
    sp.verify(self.data.self_addr == self.data.self_addr_3)
    sp.verify(self.data.self_addr_2 == sp.some(self.data.contract))

sp.add_compilation_target("test", Contract())