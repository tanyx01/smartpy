import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat, z = sp.TNat).layout(("x", "z"))
tparameter = sp.TUnit
tprivates = { }
tviews = { "view1": ((), sp.TRecord(self_addr = sp.TAddress, z = sp.TNat).layout(("self_addr", "z"))), "view2": ((), sp.TNat), "view3": (sp.TIntOrNat, sp.TIntOrNat) }
