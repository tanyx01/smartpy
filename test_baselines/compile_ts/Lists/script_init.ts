interface TStorage {
    list: TList<TString>;
}

@Contract
export class Lists {
    storage: TStorage = {
        list: [],
    };

    @EntryPoint
    push(value: TString): void {
        this.storage.list.push(value);
    }

    @EntryPoint
    concat(list: TList<TString>): void {
        this.storage.list.push(Sp.concat(list));
    }

    @EntryPoint
    reverse(): void {
        if (this.storage.list.size() === 4) {
            this.storage.list = this.storage.list.reverse();
        }
    }
}

Dev.test({ name: 'Lists' }, () => {
    const c1 = Scenario.originate(new Lists());
    Scenario.transfer(c1.push('String 1'));
    Scenario.transfer(c1.push('String 2'));
    Scenario.transfer(c1.push('String 3'));
    Scenario.transfer(c1.concat(['Hello', ' ', 'World']));
    Scenario.transfer(c1.reverse());
});

Dev.compileContract('Lists_compiled', new Lists());
