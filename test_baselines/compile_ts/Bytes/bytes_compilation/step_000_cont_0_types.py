import smartpy as sp

tstorage = sp.TRecord(bytes = sp.TOption(sp.TBytes), size = sp.TOption(sp.TNat)).layout(("bytes", "size"))
tparameter = sp.TVariant(concat = sp.TUnit, size = sp.TBytes, slice = sp.TUnit).layout(("concat", ("size", "slice")))
tprivates = { }
tviews = { }
