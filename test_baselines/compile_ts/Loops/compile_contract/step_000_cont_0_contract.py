import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def for(self):
    i = sp.local("i", 0)
    sp.while i.value < 10:
      i.value += 1
      i.value += 1

  @sp.entry_point
  def while(self):
    i = sp.local("i", 1)
    sp.while i.value == 1:
      i.value += 1

sp.add_compilation_target("test", Contract())