import smartpy as sp

tstorage = sp.TRecord(elements = sp.TOption(sp.TList(sp.TString)), set = sp.TSet(sp.TString)).layout(("elements", "set"))
tparameter = sp.TVariant(add = sp.TString, contains = sp.TString).layout(("add", "contains"))
tprivates = { }
tviews = { }
