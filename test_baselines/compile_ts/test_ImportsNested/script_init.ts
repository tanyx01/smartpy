import * as LocalModule from './Minimal';

export type StorageType = LocalModule.StorageSpec;

export const InitialStorage = LocalModule.InitialStorage;

export class Template extends LocalModule.Minimal {}
