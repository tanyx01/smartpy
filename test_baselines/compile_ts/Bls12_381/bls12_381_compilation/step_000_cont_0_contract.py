import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(checkResult = sp.TOption(sp.TBool), fr = sp.TBls12_381_fr, g1 = sp.TBls12_381_g1, g2 = sp.TBls12_381_g2, mulResult = sp.TOption(sp.TBls12_381_fr)).layout((("checkResult", "fr"), ("g1", ("g2", "mulResult")))))
    self.init(checkResult = sp.none,
              fr = sp.bls12_381_fr('0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221'),
              g1 = sp.bls12_381_g1('0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1'),
              g2 = sp.bls12_381_g2('0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801'),
              mulResult = sp.none)

  @sp.entry_point
  def add(self, params):
    sp.set_type(params.g1, sp.TBls12_381_g1)
    sp.set_type(params.g2, sp.TBls12_381_g2)
    sp.set_type(params.fr, sp.TBls12_381_fr)
    self.data.g1 += params.g1
    self.data.g2 += params.g2
    self.data.fr += params.fr

  @sp.entry_point
  def negate(self):
    self.data.g1 = - self.data.g1
    self.data.g2 = - self.data.g2
    self.data.fr = - self.data.fr

  @sp.entry_point
  def toInt(self):
    sp.verify(sp.to_int(self.data.fr) == 30425446867866594624074927213663410739214773393516557917510380800291188506624, 'Failed to cast field element Fr to Int')

  @sp.entry_point
  def mul(self, params):
    sp.set_type(params, sp.TPair(sp.TBls12_381_fr, sp.TBls12_381_fr))
    self.data.mulResult = sp.some(sp.mul(sp.fst(params), sp.snd(params)))
    self.data.g1 = sp.mul(self.data.g1, self.data.fr)
    self.data.g2 = sp.mul(self.data.g2, self.data.fr)
    self.data.fr = sp.mul(self.data.fr, self.data.fr)
    self.data.fr = sp.mul(self.data.fr, 1)
    self.data.fr = sp.mul(2, self.data.fr)

  @sp.entry_point
  def pairing_check(self, params):
    sp.set_type(params, sp.TList(sp.TPair(sp.TBls12_381_g1, sp.TBls12_381_g2)))
    self.data.checkResult = sp.some(sp.pairing_check(params))

sp.add_compilation_target("test", Contract())