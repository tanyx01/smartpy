import smartpy as sp

tstorage = sp.TRecord(value = sp.TOption(sp.TNat)).layout("value")
tparameter = sp.TVariant(switchCase1 = sp.TVariant(action1 = sp.TString, action2 = sp.TNat).layout(("action1", "action2")), switchCase2 = sp.TVariant(action1 = sp.TString, action2 = sp.TNat).layout(("action1", "action2")), switchCase3 = sp.TVariant(action1 = sp.TString, action2 = sp.TNat).layout(("action1", "action2"))).layout(("switchCase1", ("switchCase2", "switchCase3")))
tprivates = { }
tviews = { }
