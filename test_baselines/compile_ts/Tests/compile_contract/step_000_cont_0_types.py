import smartpy as sp

tstorage = sp.TRecord(storedValue = sp.TNat).layout("storedValue")
tparameter = sp.TVariant(add = sp.TRecord(value = sp.TNat, value2 = sp.TNat).layout(("value", "value2")), fail = sp.TUnit, replace = sp.TNat).layout(("add", ("fail", "replace")))
tprivates = { }
tviews = { }
