import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(storedValue = sp.TNat).layout("storedValue"))
    self.init(storedValue = 10)

  @sp.entry_point
  def replace(self, params):
    sp.set_type(params, sp.TNat)
    self.data.storedValue = params

  @sp.entry_point
  def add(self, params):
    sp.set_type(params.value, sp.TNat)
    sp.set_type(params.value2, sp.TNat)
    self.data.storedValue = params.value + params.value2

  @sp.entry_point
  def fail(self):
    sp.failwith('FAILED')

sp.add_compilation_target("test", Contract())