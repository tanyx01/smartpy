import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(value = sp.TOption(sp.TAddress)).layout("value"))
    self.init(value = sp.none)

  @sp.entry_point
  def createWithoutOrigination(self, params):
    sp.set_type(params, sp.TString)
    op = sp.local("op", create contract ...)
    self.data.value = sp.some(op.value.address)

sp.add_compilation_target("test", Contract())