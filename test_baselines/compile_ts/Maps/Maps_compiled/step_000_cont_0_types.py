import smartpy as sp

tstorage = sp.TRecord(entries = sp.TList(sp.TRecord(key = sp.TString, value = sp.TNat).layout(("key", "value"))), keys = sp.TList(sp.TString), map = sp.TMap(sp.TString, sp.TNat), value = sp.TNat, values = sp.TList(sp.TNat)).layout((("entries", "keys"), ("map", ("value", "values"))))
tparameter = sp.TVariant(remove = sp.TString, set = sp.TRecord(key = sp.TString, value = sp.TNat).layout(("key", "value"))).layout(("remove", "set"))
tprivates = { }
tviews = { }
