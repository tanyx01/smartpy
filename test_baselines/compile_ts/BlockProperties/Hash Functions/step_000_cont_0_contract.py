import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(amount = sp.TOption(sp.TMutez), balance = sp.TOption(sp.TMutez), chainId = sp.TOption(sp.TChainId), level = sp.TOption(sp.TNat), now = sp.TOption(sp.TTimestamp), sender = sp.TOption(sp.TAddress), source = sp.TOption(sp.TAddress), totalVotingPower = sp.TOption(sp.TNat), votingPower = sp.TOption(sp.TNat)).layout(((("amount", "balance"), ("chainId", "level")), (("now", "sender"), ("source", ("totalVotingPower", "votingPower"))))))
    self.init(amount = sp.none,
              balance = sp.none,
              chainId = sp.none,
              level = sp.none,
              now = sp.none,
              sender = sp.none,
              source = sp.none,
              totalVotingPower = sp.none,
              votingPower = sp.none)

  @sp.entry_point
  def ep(self):
    self.data.amount = sp.some(sp.amount)
    self.data.balance = sp.some(sp.balance)
    self.data.sender = sp.some(sp.sender)
    self.data.source = sp.some(sp.source)
    self.data.chainId = sp.some(sp.chain_id)
    self.data.now = sp.some(sp.now)
    self.data.level = sp.some(sp.level)
    self.data.totalVotingPower = sp.some(sp.total_voting_power)
    self.data.votingPower = sp.some(sp.voting_power(sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w')))

  @sp.entry_point
  def epFail(self):
    sp.failwith('FAIL ON PURPOSE')

sp.add_compilation_target("test", Contract())