import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(value = sp.TNat).layout("value"))
    self.init(value = 1)

  @sp.entry_point
  def ep1(self):
    i = sp.local("i", 0)
    sp.while i.value < 10:
      i.value += i.value + 1
      ii = sp.local("ii", 0)
      sp.while ii.value < 10:
        self.data.value += (i.value + ii.value) + 1
        ii.value += 1
      i.value += 1

  @sp.entry_point
  def ep2(self):
    sp.if (self.data.value + 1) > 2:
      self.data.value = 0

  @sp.entry_point
  def ep3(self):
    self.data.value = sp.fst(sp.ediv(sp.mutez(10), sp.mutez(1)).open_some(message = 'Failed to divide mutez'))

sp.add_compilation_target("test", Contract())