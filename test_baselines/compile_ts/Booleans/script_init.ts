interface TStorage {
    expr1: TBool;
    expr2: TBool;
}

@Contract
export class Booleans {
    constructor(public storage: TStorage) {}

    @EntryPoint
    ep(): void {
        const expr1 = this.storage.expr1;
        const expr2 = this.storage.expr2;
        Sp.verify(expr2 == expr2, 'expr1 && expr2');
        Sp.verify(expr1 && expr1, 'expr1 && expr2');
        Sp.verify(expr1 || expr2, 'expr1 || expr2');
        Sp.verify(expr1 !== expr2, 'expr1 !== expr2');

        this.storage.expr1 = expr1 && expr2;
    }
}

Dev.test({ name: 'Booleans' }, () => {
    const storage = { expr1: true, expr2: false };
    const c1 = Scenario.originate(new Booleans(storage));
    Scenario.transfer(c1.ep());
    Scenario.transfer(c1.ep(), { valid: false });
});

Dev.compileContract('booleans_compilation', new Booleans({ expr1: true, expr2: false }));
