class Test {
    storage = {
        value: 0,
    };

    @OffChainView
    other = (x: TNat): TNat => {
        Sp.verify(x < this.storage.value);
        return x * x;
    };

    @OffChainView
    square_exn = (x: TInt): TUnit => {
        Sp.failWith(x * x);
    };
}

Dev.test({ name: 'Test' }, () => {
    const c1 = Scenario.originate(new Test());
    const res = Scenario.compute(c1.other(-5));
    Scenario.show(res);
    Scenario.compute(c1.other(-1));
    Scenario.verify(Scenario.isFailing(c1.other(5)));
    Scenario.verify(!Scenario.isFailing(c1.other(-5)));
    Scenario.verify(Scenario.catchException(c1.other(5)) === Sp.some('WrongCondition: params < self.data.value'));
    Scenario.verify(
        Scenario.catchException<TString>(c1.other(5)) === Sp.some('WrongCondition: params < self.data.value'),
    );
    Scenario.verify(Scenario.catchException<TString>(c1.other(-5)) == Sp.none);
    Scenario.verify(Scenario.catchException<TInt>(c1.other(-5)) == Sp.none);
    Scenario.verify(Scenario.catchException<TInt>(c1.square_exn(-5)) == Sp.some(25));
});
