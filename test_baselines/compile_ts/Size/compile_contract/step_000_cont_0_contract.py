import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(bytes = sp.TBytes, list = sp.TList(sp.TNat), map = sp.TMap(sp.TString, sp.TBytes), set = sp.TSet(sp.TString), string = sp.TString).layout((("bytes", "list"), ("map", ("set", "string")))))
    self.init(bytes = sp.bytes('0x012345'),
              list = [1, 2],
              map = {'String' : sp.bytes('0x00')},
              set = sp.set(['String', 'String2']),
              string = 'A STRING')

  @sp.entry_point
  def checkSizes(self):
    sp.verify(sp.len(self.data.string) == 8, 'String size is not 8.')
    sp.verify(sp.len(self.data.bytes) == 3, 'Bytes size is not 3.')
    sp.verify(sp.len(self.data.map) == 1, 'Map size is not 1.')
    sp.verify(sp.len(self.data.set) == 2, 'Set size is not 2.')
    sp.verify(sp.len(self.data.list) == 2, 'List size is not 2.')

sp.add_compilation_target("test", Contract())