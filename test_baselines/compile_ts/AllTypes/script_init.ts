interface AllTypesStorage {
    address: TAddress;
    big_map: TBig_map<TNat, TBool>;
    bls12_381_fr: TBls12_381_fr;
    bls12_381_g1: TBls12_381_g1;
    bls12_381_g2: TBls12_381_g2;
    bool: TBool;
    bytes: TBytes;
    chain_id: TChain_id;
    int: TInt;
    key: TKey;
    key_hash: TKey_hash;
    lambda: TLambda<TNat, TNat>;
    list: TList<TString>;
    map: TMap<TInt, TString>;
    mutez: TMutez;
    nat: TNat;
    option: TOption<TAddress>;
    option2: TOption<TNat>;
    tuple: TTuple<[TNat, TNat, TNat]>;
    // sapling_state n
    // sapling_transaction n
    set: TSet<TNat>;
    signature: TSignature;
    string: TString;
    //ticket cty
    timestamp: TTimestamp;
    unit: TUnit;
    record: TRecord<
        {
            prop1: TInt;
            prop2: TString;
            prop3: TNat;
        },
        ['prop1', 'prop2', 'prop3']
    >;
    variant: TVariant<{ kind: 'A'; value: TString } | { kind: 'B'; value: TNat }, Layout.right_comb>;
    chest: TChest;
    chest_key: TChest_key;
}

export const storage: AllTypesStorage = {
    address: 'tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w',
    big_map: [[0, true]],
    bls12_381_fr: '0x01',
    bls12_381_g1: '0x01',
    bls12_381_g2: '0x00',
    bool: true,
    bytes: '0x00',
    chain_id: '0xedab',
    int: 1,
    key: 'edpk',
    key_hash: 'tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w',
    lambda: (value: TNat) => {
        return value + 1;
    },
    list: ['A String'],
    map: [[1, 'Something']],
    mutez: 1,
    nat: 0,
    // operation
    option: Sp.some<TAddress>('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
    option2: Sp.none,
    // or ty1 ty2
    tuple: [0, 1, 3],
    // sapling_state n
    // sapling_transaction n
    set: [1],
    signature: 'sig',
    string: 'A new String',
    // ticket cty
    timestamp: 1571659294,
    unit: Sp.unit,
    record: {
        prop1: 1,
        prop2: '2',
        prop3: 3,
    },
    variant: { kind: 'A', value: 'TEST' },
    chest: '0xbcdabdf6e0f9c4ff9883ba9adcd5c0aea7cd909199bbcba0a7878788dba08aecbc848bb9b3cb859df2a2ddd3acb7e39ee38fe2d2ae85c7caf58da4e984b5f6d8c39dd4bd0ab1ca8ef3ff8ec1cc8997bcb0f185c3f49ef98dcae695bfbabde8e0bda286c2e78f809bb98fa2e0acd5f18ad084c2a7f28ff9dcb49ef4c0b590e8c2cc99e493a3c2a7ef85e6bdb6c2ba87fd94b7faacc1c082d3fafef69bf9c3cd9ab3fb9ccaf7e0c58efbe1bab7fb8dc3a5e7f9e4f7d1d9dbd38ca891b994b5e09ab7ccad96d0d5ee8da5bce2b2c08d01563b4da16b60748081074010aee6b6b0f60c3a81e5afff3c000000211850a70bad6a333e9806ec3a2389e392181abd71f09f37447c226d8e4a6ab275aa',
    chest_key:
        '0x8caa8f96bd95b5d0bbfbb5b7c2d2c5f0ada0c3ced6b0cdbfb7a2ae9a8695d0eab59e89efd29d99ea999996c48efaaed3fcd38ff7ba8ffe86e19eddf9eae0a3bbddcba28acbbca7baaa98c7d99f80e7bbea91daa39ee592ebd58de0ac9efbbaaacdf8bcbd83e7e4d1da9aacecadcaef9ba7b8f1d4d6ef9dd8c7efe2e0ffe4ec95a0d3b6f9aeba9b831cdaf1b2be9493c3f58a8eeb8fbac5f88a81f6bb89d0f7c3d0b68bcefb81cfb09aaaf686cfcafba6fdee8edf8cbaa4bec6839ab5d4a3e9cdbaa1b4ef88bdc2b892e3c5bafcdbe7f5c4e3a7e38fd6dceba29981a2ff9dc5cfd5a59ba8aaef98afa0c8cbfc919893f1c487cc95deb7e6d5c3a5f08a86d089f2fec8d8eff9f1f6ebf489fce5a2e6caccf95d',
};

@Contract
class AllTypes {
    constructor(public storage: AllTypesStorage = storage) {}
}

Dev.test({ name: 'AllTypes' }, () => {
    Scenario.originate(new AllTypes(storage));
});

Dev.compileContract('compile_contract', new AllTypes(storage));
