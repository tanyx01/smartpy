import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(timestamp = sp.TTimestamp).layout("timestamp"))
    self.init(timestamp = sp.timestamp(1627302929))

  @sp.entry_point
  def addTime(self):
    self.data.timestamp = sp.add_seconds(self.data.timestamp, 1)
    self.data.timestamp = sp.add_seconds(self.data.timestamp, 1 * 60)
    self.data.timestamp = sp.add_seconds(self.data.timestamp, 1 * 3600)
    self.data.timestamp = sp.add_seconds(self.data.timestamp, 1 * 86400)
    sp.verify((sp.now - self.data.timestamp) > 0, 'Sp.now.minus(this.storage.timestamp) > 0')

sp.add_compilation_target("test", Contract())