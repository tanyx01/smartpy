import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(heir = sp.TAddress, owner = sp.TAddress, timeout = sp.TOption(sp.TTimestamp)).layout(("heir", ("owner", "timeout"))))
    self.init(heir = sp.address('tz1L3Pgin3YSeqZJb6UF9Fis9LBjp3L1YusM'),
              owner = sp.address('tz1M43YV5vVt7HALGJeWgQ1GRQfiGExA1Jbf'),
              timeout = sp.some(sp.timestamp(1672531200)))

  @sp.entry_point
  def alive(self):
    sp.verify(sp.sender == self.data.owner)
    self.data.timeout = sp.some(sp.add_seconds(sp.now, ((3600 * 24) * 365) * 2))

  @sp.entry_point
  def withdraw(self, params):
    sp.set_type(params.to_, sp.TAddress)
    sp.set_type(params.amount, sp.TMutez)
    sp.verify(sp.sender == self.data.owner)
    sp.verify(self.data.timeout.is_some())
    contract = sp.local("contract", sp.contract(sp.TUnit, params.to_, entry_point='default').open_some(message = 'Invalid Interface'))
    sp.transfer(sp.unit, params.amount, contract.value)
    self.data.timeout = sp.some(sp.add_seconds(sp.now, ((3600 * 24) * 365) * 2))

  @sp.entry_point
  def heirWithdraw(self, params):
    sp.set_type(params.to_, sp.TAddress)
    sp.set_type(params.amount, sp.TMutez)
    sp.verify(sp.sender == self.data.heir)
    sp.verify((~ self.data.timeout.is_some()) | (sp.now > self.data.timeout.open_some()))
    contract = sp.local("contract", sp.contract(sp.TUnit, params.to_, entry_point='default').open_some(message = 'Invalid Interface'))
    sp.transfer(sp.unit, params.amount, contract.value)
    self.data.timeout = sp.none

sp.add_compilation_target("test", Contract())