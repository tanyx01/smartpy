open SmartML

let%entry_point myEntryPoint params =
  data.a <- data.a + params.x;
  data.b <- data.b + params.y

let sub = Basics.build_contract ~storage:[%expr {a = 0; b = 0}] [myEntryPoint]

let%entry_point test_commands params =
  transfer 42 params.amount params.destination;
  set_delegate params.delegate;
  create_contract None (mutez 0) {a = 0; b = 0} sub

let%entry_point test_transfer params =
  add_operations [transfer_operation 42 params.amount params.destination]

let%entry_point test_set_delegate params =
  add_operations [set_delegate_operation params]

let%entry_point test_create_contract () =
  let create_op = create_contract_operation None (mutez 0) {a = 0; b = 0} sub in
  add_operations [create_op.operation]

let init =
  Basics.build_contract
    ~storage:[%expr {x = 0; y = 0}]
    [test_commands; test_transfer; test_create_contract; test_set_delegate]

let () =
  Target.register_test
    ~name:"Operations"
    [%actions
      h1 "Operations";
      let c = register_contract init in
      call c.test_create_contract ()]

let () = Target.register_compilation ~name:"operations" init
