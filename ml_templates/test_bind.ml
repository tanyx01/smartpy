open SmartML

module MyContract = struct
  let f p =
    [%command
      let* b = result (data.x + [%e p]) in
      result 42]

  let%entry_point call_f params =
    let* r = [%e f [%expr params]] in
    data.x <- r

  let init p1 p2 =
    let storage = [%expr {x = 12; y = 0}] in
    Basics.build_contract ~storage [call_f]
end

let () =
  Target.register_test
    ~name:"Test_bind"
    [%actions
      h1 "Test_bind";
      let c = register_contract (MyContract.init [%expr 12] [%expr 123]) in
      call c.call_f 42]
