open SmartML

module Calculator = struct
  let%entry_point add params = data.value <- params.x + params.y

  let%entry_point factorial params =
    data.value <- 1;
    List.iter (fun y -> data.value <- data.value * y) (range 1 (params + 1) 1)

  let%entry_point log2 params =
    data.value <- 0;
    let%mutable y = params in
    while y > 1 do
      data.value <- data.value + 1;
      y <- y / 2
    done

  let%entry_point multiply params = data.value <- params.x * params.y

  let%entry_point square params = data.value <- params * params

  let%entry_point squareRoot params =
    verify ~msg:"cannot take square root of negative number" (params >= 0);
    let%mutable y = params in
    while y * y > params do
      y <- ((params / y) + y) / 2
    done;
    verify
      (y * y <= params && params < (y + 1) * (y + 1))
      ~msg:"squareRoot error";
    data.value <- y

  let init () =
    Basics.build_contract
      ~storage:[%expr {value = 0}]
      [add; factorial; log2; multiply; square; squareRoot]
end

let () =
  Target.register_test
    ~name:"Calculator"
    [%actions
      h1 "Calculator";
      let c = register_contract (Calculator.init ()) in
      call c.multiply {x = 2; y = 5};
      call c.add {x = 2; y = 5};
      call c.add {x = 2; y = 5};
      call c.square 12;
      call c.squareRoot 0;
      call c.squareRoot 1234;
      call c.factorial 100;
      call c.log2 c.data.value;
      verify (c.data.value = 524)]

let () = Target.register_compilation ~name:"Calculator2" (Calculator.init ())
