import * as path from 'path';
import typescript from '@rollup/plugin-typescript';
import json from '@rollup/plugin-json';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';

export default {
    input: 'src/index.ts',
    output: [
        {
            file: pkg.module,
            globals: { crypto: 'crypto' },
            format: 'es',
            sourcemap: true,
            banner: `/*! === Version: ${pkg.version} === */`,
        },
        {
            file: pkg.main,
            format: 'cjs',
            sourcemap: true,
            globals: { crypto: 'crypto' },
            banner: `/*! === Version: ${pkg.version} === */`,
        },
    ],
    plugins: [
        typescript({
            tsconfig: path.resolve(__dirname, './tsconfig.build.json'),
        }),
        commonjs(),
        resolve(),
        json(),
        terser(),
    ],
};
