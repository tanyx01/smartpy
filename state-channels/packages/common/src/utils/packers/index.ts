import { MichelsonData, MichelsonType, packDataBytes, unpackDataBytes } from '@taquito/michel-codec';

export * as Channel from './channel';
export * as Game from './game';

export function unpackData(bytes: string, t?: MichelsonType): MichelsonData {
    return unpackDataBytes({ bytes }, t);
}

export function packData(v: MichelsonData, t?: MichelsonType): string {
    return packDataBytes(v, t).bytes;
}
