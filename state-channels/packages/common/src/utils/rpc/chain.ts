import { Constants } from '../../constants';
import Http from '../../services/http';

/**
 * @description Get chain identifier
 * @returns {string} chain identifier
 */
export const getChainID = async (): Promise<string> => (await Http.get(`${Constants.rpc}/chains/main/chain_id`)).data;
