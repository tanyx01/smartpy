import { blake2bHex } from 'blakejs';
import { GameID } from '../../@types/game';
import { Packers } from '..';

export function hashID(data: GameID): string {
    return blake2bHex(Buffer.from(Packers.Game.packID(data), 'hex'), undefined, 32);
}
