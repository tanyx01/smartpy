export * from './tokens';

export const Constants =
    process.env.NODE_ENV === 'development'
        ? {
              network: {
                  type: 'ghostnet',
              },
              rpc: 'https://ghostnet.smartpy.io',
              api: 'http://localhost:3005',
              graphQlHttp: 'http://localhost:8081/v1/graphql',
              graphQlWs: 'ws://localhost:8081/v1/graphql',
          }
        : {
              network: {
                  type: 'ghostnet',
              },
              rpc: 'https://ghostnet.smartpy.io',
              api: 'http://ghostnet.smartpy.io:3005',
              graphQlHttp: 'http://ghostnet.smartpy.io:8081/v1/graphql',
              graphQlWs: 'ws://ghostnet.smartpy.io:8081/v1/graphql',
          };
