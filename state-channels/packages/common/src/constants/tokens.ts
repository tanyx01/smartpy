import BigNumber from 'bignumber.js';

export enum TezUnit {
    uTez = 'uTez',
    tez = 'tez',
}

export const AmountUnit: Record<
    TezUnit,
    {
        char: string;
        unit: number;
    }
> = {
    [TezUnit.uTez]: { char: 'μꜩ', unit: 1 },
    [TezUnit.tez]: { char: 'ꜩ', unit: 1000000 },
};

/**
 * @description Convert amount number. (e.g. from Tez to uTez).
 *
 * @param value Value to be converted.
 * @param from origin unit.
 * @param to target unit.
 *
 * @returns {BigNumber} the converted value.
 */
export const convertUnitNumber = (value: BigNumber | string | number, from: TezUnit, to = TezUnit.uTez): BigNumber =>
    new BigNumber(value).dividedBy(AmountUnit[to].unit).multipliedBy(AmountUnit[from].unit);

/**
 * @description Convert amount and prettify. (e.g. from Tez to uTez).
 *
 * @param value Value to be converted.
 * @param from origin unit.
 * @param to target unit.
 *
 * @returns {string} the converted value.
 */
export const convertUnit = (value: BigNumber | string | number, from: TezUnit, to = TezUnit.uTez): string =>
    convertUnitNumber(value, from, to).toFormat(3, BigNumber.ROUND_FLOOR);

/**
 * @description Convert amount and include the unit symbol. (e.g. from Tez ꜩ to uTez μꜩ).
 *
 * @param value Value to be converted.
 * @param from origin unit.
 * @param to target unit.
 *
 * @returns {string} the converted value with the respective symbol.
 */
export const convertUnitWithSymbol = (value: BigNumber | string | number, from: TezUnit, to = TezUnit.uTez): string =>
    prettifyTezUnit(convertUnitNumber(value, from, to), to);

/**
 * @description Prettify amount with the unit symbol. (e.g. from Tez ꜩ to uTez μꜩ).
 *
 * @param value Value to be converted.
 * @param unit value unit.
 *
 * @returns {string} the amount with respective symbol.
 */
export function prettifyTezUnit(value: BigNumber | string | number, unit: TezUnit): string {
    return `${value} ${AmountUnit[unit].char}`;
}
