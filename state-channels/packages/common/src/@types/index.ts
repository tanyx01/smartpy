export * from './game';
export * from './channel';
export * from './model';
export * from './channel';

export interface SC_Token {
    id: number;
    metadata: Record<string, string>;
}

export interface SC_Platform {
    id: string;
    ledger: string;
    metadata: string;
}
