import { AxiosResponse } from 'axios';
import { GameSignature, NewGameRequest, NewGameStateRequest } from '../@types';
import Http from '../services/http';
import * as Routes from './routes';

/**
 * @description Post a new game
 */
export function newGame(data: NewGameRequest): Promise<AxiosResponse<any>> {
    return Http.post(Routes.GAMES, data);
}

/**
 * @description Post signatures to accept the game
 */
export function acceptGame(gameID: string, signature: GameSignature): Promise<AxiosResponse<any>> {
    return Http.post(Routes.ACCEPT_GAME.replace(':id', gameID), signature);
}

/**
 * @description Post a new game movement
 */
export function newGameMove(gameID: string, data: NewGameStateRequest): Promise<AxiosResponse<any>> {
    return Http.post(Routes.GAME_MOVE.replace(':id', gameID), data);
}

/**
 * @description Post signatures to accept game movement
 */
export function acceptGameMove(
    gameID: string,
    moveNumber: number,
    signature: GameSignature,
): Promise<AxiosResponse<any>> {
    return Http.post(
        Routes.ACCEPT_GAME_MOVE.replace(':gameID', gameID).replace(':moveNumber', String(moveNumber)),
        signature,
    );
}
