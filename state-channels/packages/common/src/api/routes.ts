import { Constants } from '../constants';

export const enum PATH {
    Games = '/games',
    AcceptGame = '/games/:id/accept',
    GameMove = '/games/:id/move',
    AcceptGameMove = '/games/:gameID/accept_move/:moveNumber',
}

export const GAMES = `${Constants.api}${PATH.Games}`;
export const ACCEPT_GAME = `${Constants.api}${PATH.AcceptGame}`;
export const GAME_MOVE = `${Constants.api}${PATH.GameMove}`;
export const ACCEPT_GAME_MOVE = `${Constants.api}${PATH.AcceptGameMove}`;
