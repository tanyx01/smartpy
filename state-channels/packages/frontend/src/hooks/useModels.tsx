import { useSubscription } from '@apollo/client';
import { SC_Model } from 'state-channels-common';

import { Subscription } from 'src/services/graphql';
import Logger from 'src/services/logger';
import useAppContext from './useAppContext';

function useModels(): Record<string, SC_Model> {
    const { platform } = useAppContext();
    const { error, data } = useSubscription<{ models: SC_Model[] }, { platform: string }>(Subscription.Model.MODELS, {
        variables: { platform: platform.id },
    });

    if (error) {
        Logger.debug(error);
    }

    return (
        data?.models.reduce<Record<string, SC_Model>>((acc, model) => {
            if (model.metadata?.name) {
                return { ...acc, [model.metadata.name]: model };
            }
            return acc;
        }, {}) || {}
    );
}

export default useModels;
