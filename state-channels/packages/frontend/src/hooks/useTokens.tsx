import { useSubscription } from '@apollo/client';
import { SC_Token } from 'state-channels-common';

import { Subscription } from 'src/services/graphql';
import Logger from 'src/services/logger';

function useTokens(): Record<number, SC_Token> {
    const { error, data } = useSubscription<{ tokens: SC_Token[] }>(Subscription.Token.TOKENS);

    if (error) {
        Logger.debug(error);
    }

    return (
        data?.tokens.reduce<Record<number, SC_Token>>(
            (acc, { id, metadata }) => ({ ...acc, [id]: { id, metadata } }),
            {},
        ) || {}
    );
}

export default useTokens;
