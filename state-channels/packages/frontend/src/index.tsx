import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from '@apollo/client';

import graphqlClient from './services/graphql';
import './index.css';
import reportWebVitals from './reportWebVitals';
import ThemeProvider from './context/ThemeProvider';
import Router from './Routes';
import AppProvider from './context/AppProvider.';

ReactDOM.render(
    <ThemeProvider>
        <ApolloProvider client={graphqlClient}>
            <AppProvider>
                <Router />
            </AppProvider>
        </ApolloProvider>
    </ThemeProvider>,
    document.getElementById('root'),
);

const onResize = () => {
    if (window.screen.width < 512) {
        document.querySelector('meta[name=viewport]')?.setAttribute('content', 'width=748');
    } else {
        document.querySelector('meta[name=viewport]')?.setAttribute('content', 'width=device-width, initial-scale=1');
    }
};
onResize();
window.addEventListener('resize', onResize);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
