import { gql } from '@apollo/client';

export const CHANNEL = gql`
    query Channel($id: String!) {
        channels: channel(where: { id: { _eq: $id } }) {
            id
            closed
            nonce
            withdrawDelay: withdraw_delay
            createdAt: created_at
            updatedAt: updated_at
            participants {
                publicKeyHash: public_key_hash
                publicKey: public_key
                withdrawID: withdraw_id
                withdraw
                bonds {
                    amount
                    id: bond_id
                }
            }
            games(order_by: { created_at: desc }, limit: 30) {
                id
                bonds
                nonce: game_nonce
                settlements
                state
                settled
                playDelay: play_delay
                onChain: on_chain
                players
                model {
                    metadata
                    outcomes
                }
                signatures {
                    publicKey: public_key
                    signature
                }
                createdAt: created_at
                updatedAt: updated_at
            }
        }
    }
`;
