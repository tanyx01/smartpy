import React from 'react';
import { ButtonProps } from '@mui/material/Button';
import { Link as RouterLink } from 'react-router-dom';

import { LocationDescriptor } from 'history';
import Button from './Button';

interface OwnProps extends ButtonProps {
    to: LocationDescriptor;
}

const Component: React.FC<OwnProps> = ({ to, ...props }) => (
    <Button
        {...props}
        component={React.forwardRef<HTMLAnchorElement>((linkProps, ref) => (
            <RouterLink {...linkProps} to={to} ref={ref} />
        ))}
    />
);

export default Component;
