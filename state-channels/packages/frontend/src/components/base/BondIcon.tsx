import React from 'react';

import type { ChipProps } from '@mui/material';
import { Chip, Tooltip, Avatar } from '@mui/material';

import { SC_Token } from 'state-channels-common';

interface OwnProps extends ChipProps {
    token?: SC_Token;
}

const BondIcon: React.FC<OwnProps> = ({ token, ...props }) => {
    if (token) {
        return (
            <Tooltip title={token.metadata['name']}>
                <>
                    <Chip
                        size="small"
                        sx={{ bgcolor: 'transparent', border: 'none' }}
                        avatar={
                            <Avatar
                                imgProps={{
                                    style: {
                                        objectFit: 'fill',
                                    },
                                }}
                                src={token.metadata['thumbnailUri']}
                            />
                        }
                        label={token.metadata['symbol']}
                        {...props}
                    />
                </>
            </Tooltip>
        );
    }

    return null;
};

export default BondIcon;
