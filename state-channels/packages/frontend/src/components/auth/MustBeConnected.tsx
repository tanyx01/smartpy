import React from 'react';
import { Grid, Typography } from '@mui/material';

import useWalletContext from '../../hooks/useWalletContext';
import BeaconButton from '../Beacon/Button';

const MustBeConnected: React.FC = ({ children }) => {
    const { pkh } = useWalletContext();

    if (pkh) {
        return <>{children}</>;
    }

    return (
        <Grid container direction="column" alignItems="center" justifyContent="center">
            <Typography variant="overline">Connect to a wallet first</Typography>
            <BeaconButton />
        </Grid>
    );
};

export default MustBeConnected;
