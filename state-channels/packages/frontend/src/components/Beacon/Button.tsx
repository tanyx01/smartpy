import React from 'react';

import { useTheme } from '@mui/material/styles';
import { useMediaQuery } from '@mui/material';

import Button from '../base/Button';
import useWalletContext from '../../hooks/useWalletContext';
import BeaconFullIcon from './icons/BeaconFull';
import Avatar from '../base/Avatar';

const BeaconButton: React.FC = () => {
    const { pkh, connectWallet } = useWalletContext();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('sm'));

    return (
        <Button
            onClick={connectWallet}
            endIcon={matches ? pkh ? <Avatar value={pkh} /> : <BeaconFullIcon /> : undefined}
        >
            {pkh ? 'Re-Connect' : 'Connect'}
        </Button>
    );
};

export default BeaconButton;
