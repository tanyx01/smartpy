import React from 'react';
import { SigningType } from '@airgap/beacon-sdk';

import { Alert, Chip, Divider, Grid, Paper, Rating, Theme, Typography } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import CircleIcon from '@mui/icons-material/CircleOutlined';
import CrossIcon from '@mui/icons-material/CloseOutlined';

import {
    Api,
    EntryPoints,
    GameConstants,
    OffchainViews,
    Packers,
    SC_Game,
    SC_GameMoveAction,
    SC_Platform,
} from 'state-channels-common';

import Wallet from 'src/services/wallet';
import Logger from 'src/services/logger';
import Button from 'src/components/base/Button';
import useWalletContext from 'src/hooks/useWalletContext';
import TicTacToeUtils from './TicTacToe.utils';
import { Errors } from '../../../../../common/src/utils/rpc';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        board: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            margin: 10,
            padding: 15,
        },
        cell: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: 64,
            height: 64,
            fontSize: '32pt',
        },
        divider: {
            margin: theme.spacing(1),
        },
    }),
);

interface GameHeaderProps {
    outcome?: Record<string, string>;
    currentPlayerNumber: number;
    localPlayerNumber: number;
    latestMove?: SC_GameMoveAction;
    pendingMove?: SC_GameMoveAction;
    onAcceptMovement?: () => void;
}

const GameHeader: React.FC<GameHeaderProps> = ({
    outcome,
    localPlayerNumber,
    currentPlayerNumber,
    pendingMove,
    latestMove,
    onAcceptMovement,
}) => {
    if (outcome) {
        for (const o in outcome) {
            switch (o) {
                case 'game_finished':
                    return (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            Game finished (
                            {outcome[o] === 'draw'
                                ? 'Draw'
                                : localPlayerNumber === Number(outcome[o].split('_')[1])
                                ? 'You Won!'
                                : 'You Lost!'}
                            )
                        </Alert>
                    );
                case 'game_abort':
                    return (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            Game aborted
                        </Alert>
                    );
                case 'player_inactive':
                    return (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            Player inactive ({localPlayerNumber !== Number(outcome[o]) ? 'You Won!' : 'You Lost!'})
                        </Alert>
                    );
                case 'player_double_played':
                    return (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            Player double played ({localPlayerNumber !== Number(outcome[o]) ? 'You Won!' : 'You Lost!'})
                        </Alert>
                    );
            }
        }
    }
    switch (latestMove?.outcome) {
        case 'player_1_won':
        case 'player_2_won':
            return (
                <Alert variant="outlined" severity="warning" icon={false}>
                    {localPlayerNumber === Number(latestMove.outcome.split('_')[1]) ? 'You Won!' : 'You Lost!'}
                </Alert>
            );
        case 'draw':
            return (
                <Alert variant="outlined" severity="warning" icon={false}>
                    The game ended with draw!
                </Alert>
            );
    }
    if (pendingMove && onAcceptMovement) {
        if (localPlayerNumber === pendingMove?.playerNumber) {
            return (
                <Paper>
                    {pendingMove.outcome ? (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            This move contains the following outcome: <Chip label={pendingMove.outcome} />
                        </Alert>
                    ) : (
                        <Alert variant="outlined" severity="info" icon={false}>
                            You must accept the opponent move before procceeding
                        </Alert>
                    )}
                    <Button fullWidth onClick={onAcceptMovement}>
                        Accept Oppenent Move
                    </Button>
                </Paper>
            );
        } else {
            return (
                <Alert variant="outlined" severity="info" icon={false}>
                    Waiting for the opponent to accept your move
                </Alert>
            );
        }
    }

    if (localPlayerNumber !== currentPlayerNumber) {
        return (
            <Alert variant="outlined" severity="info" icon={false}>
                It is not your turn yet
            </Alert>
        );
    }

    return (
        <Alert variant="outlined" severity="info" icon={false}>
            It is your turn
        </Alert>
    );
};

interface OwnProps {
    platform: SC_Platform;
    game: SC_Game;
    onError: (msg: string) => void;
    onSuccess: (msg: string) => void;
}

const TicTacToe: React.FC<OwnProps> = ({ platform, game, onError, onSuccess }) => {
    const classes = useStyles();
    const { pkh, publicKey } = useWalletContext();
    const [move, setMove] = React.useState<{ row: number; col: number }>();

    const pendingMove: SC_GameMoveAction | undefined = React.useMemo(
        () => (game?.pendingMove?.length ? game.pendingMove[0] : undefined),
        [game],
    );
    const latestMove: SC_GameMoveAction | undefined = React.useMemo(
        () => (game?.latestMove?.length ? game.latestMove[0] : undefined),
        [game],
    );
    const onChainBoard = React.useMemo(() => {
        if (game?.state) {
            return TicTacToeUtils.unpackState(game.state);
        }

        return [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ];
    }, [game]);
    const offChainBoard = React.useMemo(() => {
        if (pendingMove) {
            return TicTacToeUtils.unpackState(pendingMove.state);
        } else if (latestMove) {
            return TicTacToeUtils.unpackState(latestMove.state);
        }

        return onChainBoard;
    }, [onChainBoard, latestMove]);

    const playerNumber = React.useMemo(() => game.players[pkh!], [game, pkh]);

    const acceptOffchainMove = React.useCallback(async () => {
        try {
            if (game && pkh && publicKey && pendingMove) {
                const players = Object.entries(game.players).reduce<Record<number, string>>(
                    (acc, [address, number]) => ({ ...acc, [number]: address }),
                    {},
                );
                const moveData = Packers.packData(
                    {
                        prim: 'Pair',
                        args: [
                            {
                                int: String(pendingMove.move.row),
                            },
                            {
                                int: String(pendingMove.move.col),
                            },
                        ],
                    },
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'int', annots: ['%x'] },
                            { prim: 'int', annots: ['%y'] },
                        ],
                        annots: ['%move_data'],
                    },
                );
                const boardState: number[][] = JSON.parse(JSON.stringify(offChainBoard));
                boardState[pendingMove.move.row][pendingMove.move.col] = 0;
                const moveResult = await OffchainViews.offchain_compute_game_play(platform, {
                    player: players[latestMove?.playerNumber || game.currentPlayer],
                    moveBytes: moveData,
                    game: {
                        modelID: game.modelID,
                        initParams: game.initParams,
                        settled: game.settled,
                        current: {
                            player: latestMove?.playerNumber || game.currentPlayer,
                            moveNumber: latestMove?.moveNumber || game.moveNumber,
                            outcome: latestMove?.outcome ? { game_finished: latestMove.outcome } : game.outcome,
                        },
                        players,
                        state: TicTacToeUtils.packState(boardState),
                    },
                });

                if (!moveResult.state) {
                    return onError('Move is invalid');
                }

                const bytes = Packers.Game.packPlayAction(game.id, moveResult.current, moveResult.state, moveData);
                const signature = await Wallet.Beacon.signer.sign(bytes, undefined, SigningType.MICHELINE);

                await Api.Game.acceptGameMove(game.id, moveResult.current.moveNumber, {
                    publicKey,
                    signature: signature.prefixSig,
                });
            }
        } catch (e) {
            Logger.debug(e);
            onError(Errors.parseError(e));
        }
    }, [game, pkh, publicKey]);

    const submitOffchainMove = React.useCallback(async () => {
        try {
            if (game && pkh && publicKey && move) {
                const players = Object.entries(game.players).reduce<Record<number, string>>(
                    (acc, [address, number]) => ({ ...acc, [number]: address }),
                    {},
                );
                const moveBytes = Packers.packData(
                    {
                        prim: 'Pair',
                        args: [
                            {
                                int: String(move.row),
                            },
                            {
                                int: String(move.col),
                            },
                        ],
                    },
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'int', annots: ['%x'] },
                            { prim: 'int', annots: ['%y'] },
                        ],
                        annots: ['%move_data'],
                    },
                );
                const moveResult = await OffchainViews.offchain_compute_game_play(platform, {
                    player: pkh,
                    moveBytes,
                    game: {
                        modelID: game.modelID,
                        initParams: game.initParams,
                        settled: game.settled,
                        current: {
                            player: latestMove?.playerNumber || game.currentPlayer,
                            moveNumber: latestMove?.moveNumber || game.moveNumber,
                            outcome: latestMove?.outcome ? { game_finished: latestMove.outcome } : game.outcome,
                        },
                        players,
                        state: TicTacToeUtils.packState(offChainBoard),
                    },
                });

                if (!moveResult.state) {
                    return onError('Move is invalid');
                }

                const bytes = Packers.Game.packPlayAction(game.id, moveResult.current, moveResult.state, moveBytes);
                const signature = await Wallet.Beacon.signer.sign(bytes, undefined, SigningType.MICHELINE);

                await Api.Game.newGameMove(game.id, {
                    current: moveResult.current,
                    state: moveResult.state,
                    moveBytes,
                    move,
                    signature: {
                        publicKey,
                        signature: signature.prefixSig,
                    },
                });
            }
        } catch (e) {
            Logger.debug(e);
            onError(Errors.parseError(e));
        } finally {
            setMove(undefined);
        }
    }, [game, pkh, publicKey, move]);

    const playOnChain = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game && move && pkh && publicKey) {
            try {
                const players = Object.entries(game.players).reduce<Record<number, string>>(
                    (acc, [address, number]) => ({ ...acc, [number]: address }),
                    {},
                );
                const moveBytes = Packers.packData(
                    {
                        prim: 'Pair',
                        args: [
                            {
                                int: String(move.row),
                            },
                            {
                                int: String(move.col),
                            },
                        ],
                    },
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'int', annots: ['%x'] },
                            { prim: 'int', annots: ['%y'] },
                        ],
                        annots: ['%move_data'],
                    },
                );
                const moveResult = await OffchainViews.offchain_compute_game_play(platform, {
                    player: pkh,
                    moveBytes,
                    game: {
                        modelID: game.modelID,
                        initParams: game.initParams,
                        settled: game.settled,
                        current: {
                            player: game.currentPlayer,
                            moveNumber: game.moveNumber,
                            outcome: game.outcome,
                        },
                        players,
                        state: TicTacToeUtils.packState(onChainBoard),
                    },
                });

                if (!moveResult.state) {
                    return onError('Move is invalid');
                }

                const stateBytes = Packers.Game.packPlayAction(
                    game.id,
                    moveResult.current,
                    moveResult.state,
                    moveBytes,
                );
                const signature = await Wallet.Beacon.signer.sign(stateBytes, undefined, SigningType.MICHELINE);

                // Build "game_play" call
                await Wallet.Beacon.transfer(
                    platform.id,
                    EntryPoints.Game.play(game.id, moveBytes, moveResult.state, moveResult.current, [
                        {
                            publicKey,
                            signature: signature.prefixSig,
                        },
                    ]),
                );
            } catch (e) {
                Logger.debug(e);
                onError(Errors.parseError(e));
            } finally {
                setMove(undefined);
            }
        }
    }, [game, pkh, publicKey, move]);

    const updateGameStateOnChain = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game && latestMove) {
            try {
                const moveBytes = Packers.packData(
                    {
                        prim: 'Pair',
                        args: [
                            {
                                int: String(latestMove.move.row),
                            },
                            {
                                int: String(latestMove.move.col),
                            },
                        ],
                    },
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'int', annots: ['%x'] },
                            { prim: 'int', annots: ['%y'] },
                        ],
                        annots: ['%move_data'],
                    },
                );
                // Call "update_state" entrypoint
                await Wallet.Beacon.transfer(
                    platform.id,
                    EntryPoints.Game.play(
                        game.id,
                        moveBytes,
                        latestMove.state,
                        {
                            moveNumber: latestMove.moveNumber,
                            player: latestMove.playerNumber,
                            outcome: latestMove?.outcome ? { game_finished: latestMove.outcome } : game.outcome,
                        },
                        latestMove.signatures,
                    ),
                );
            } catch (e) {
                Logger.debug(e);
                onError(e.message);
            }
        }
    }, [game, latestMove]);

    const submitGameOnChain = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game) {
            try {
                // Call "new_game" entrypoint
                const constants: GameConstants = {
                    channelID: game.channelID,
                    gameNonce: game?.nonce,
                    modelID: game?.modelID,
                    playDelay: game?.playDelay,
                    settlements: game?.settlements,
                    bonds: game?.bonds,
                    players: game?.players,
                };
                await Wallet.Beacon.transfer(
                    platform.id,
                    EntryPoints.Game.newGame(game.initParams, constants, game.signatures),
                );
            } catch (e) {
                Logger.debug(e);
                onError(Errors.parseError(e));
            }
        }
    }, [game, latestMove]);

    const settleOnChain = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game) {
            try {
                // Call "settle" entrypoint
                await Wallet.Beacon.transfer(platform.id, EntryPoints.Game.settle(game.id));
            } catch (e) {
                Logger.debug(e);
                onError(Errors.parseError(e));
            }
        }
    }, [game]);

    const denounceDoublePlay = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game) {
            try {
                const opponent = Object.keys(game.players).find((addr) => addr !== pkh);
                if (!opponent) {
                    return onError('Could not get opponent number');
                }

                let sig = '';
                let state = '';
                if (pendingMove) {
                    sig = pendingMove.signatures.find((s) => !!s.publicKey)?.signature || '';
                    state = pendingMove.state;
                } else if (latestMove) {
                    sig = latestMove.signatures.find((s) => s.publicKey !== publicKey)?.signature || '';
                    state = latestMove.state;
                }

                // Call "dispute_double_signed" entrypoint
                await Wallet.Beacon.transfer(
                    platform.id,
                    EntryPoints.Game.disputeDoubleSigned(game.id, game.players[opponent], {
                        sig,
                        state,
                    }),
                );
            } catch (e) {
                Logger.debug(e);
                onError(Errors.parseError(e));
            }
        }
    }, []);

    const handleMoveSelection = (row: number, col: number) => {
        setMove({ row, col });
    };

    return (
        <Grid container alignItems="stretch" spacing={1}>
            <Grid xs={6} item display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                <Typography variant="overline">On-Chain State (Move Number: {game.moveNumber})</Typography>
                <Grid
                    container
                    justifyContent="stretch"
                    alignItems="stretch"
                    alignSelf="stretch"
                    justifySelf="stretch"
                    flexGrow={1}
                    sx={{ padding: 1 }}
                    component={Paper}
                >
                    <Grid item xs={12}>
                        <GameHeader
                            outcome={game?.outcome}
                            localPlayerNumber={game.players[pkh!]}
                            currentPlayerNumber={game.currentPlayer}
                        />
                        <div className={classes.divider} />
                    </Grid>
                    {onChainBoard.reduce<JSX.Element[]>(
                        (acc, cur, row) => [
                            ...acc,
                            ...cur.map((pos, col) => (
                                <Grid
                                    item
                                    xs={4}
                                    display="flex"
                                    justifyContent="center"
                                    alignItems="center"
                                    border="0.2px solid"
                                    key={`${row}-${col}`}
                                >
                                    <div className={classes.cell}>
                                        {pos ? (
                                            pos === playerNumber ? (
                                                <CrossIcon fontSize="inherit" />
                                            ) : (
                                                <CircleIcon fontSize="inherit" />
                                            )
                                        ) : (
                                            <Rating
                                                onChange={() => handleMoveSelection(row, col)}
                                                size="large"
                                                icon={<CrossIcon fontSize="inherit" />}
                                                emptyIcon={<CrossIcon fontSize="inherit" style={{ opacity: 0.4 }} />}
                                                value={move?.col === col && move?.row == row ? 1 : 0}
                                                max={1}
                                            />
                                        )}
                                    </div>
                                </Grid>
                            )),
                        ],
                        [],
                    )}
                    <Divider flexItem orientation="horizontal" sx={{ margin: 0.5 }} />
                    <Button fullWidth onClick={playOnChain} disabled={!!game?.outcome || !move}>
                        Submit move (On-Chain)
                    </Button>
                    <Divider flexItem orientation="horizontal" sx={{ margin: 0.3 }} />
                    <Button onClick={submitGameOnChain} fullWidth disabled={game.onChain}>
                        Submit game (On-Chain)
                    </Button>
                    <Divider flexItem orientation="horizontal" sx={{ margin: 0.3 }} />
                    <Button onClick={updateGameStateOnChain} fullWidth disabled={!game.onChain || !latestMove}>
                        Update game state (On-Chain)
                    </Button>
                    <Divider flexItem orientation="horizontal" sx={{ margin: 0.3 }} />
                    <Button onClick={settleOnChain} fullWidth disabled={game.settled || !game?.outcome}>
                        Settle game (On-Chain)
                    </Button>
                    <Divider flexItem orientation="horizontal" sx={{ margin: 0.3 }} />
                    {/* <Button onClick={denounceDoublePlay} fullWidth disabled={!!game?.outcome}>
                        Denounce double play
                    </Button> */}
                </Grid>
            </Grid>
            <Grid xs={6} item display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                <Typography variant="overline">
                    Off-Chain State (Move Number: {latestMove?.moveNumber || 'N/A'})
                </Typography>
                <Grid
                    container
                    justifyContent="stretch"
                    alignItems="stretch"
                    alignSelf="stretch"
                    justifySelf="stretch"
                    flexGrow={1}
                    sx={{ padding: 1 }}
                    component={Paper}
                >
                    <Grid item xs={12}>
                        <GameHeader
                            outcome={game?.outcome}
                            pendingMove={pendingMove}
                            latestMove={latestMove}
                            localPlayerNumber={game.players[pkh!]}
                            currentPlayerNumber={latestMove?.playerNumber || game.currentPlayer}
                            onAcceptMovement={acceptOffchainMove}
                        />
                        <div className={classes.divider} />
                    </Grid>
                    {offChainBoard.reduce<JSX.Element[]>(
                        (acc, cur, row) => [
                            ...acc,
                            ...cur.map((pos, col) => (
                                <Grid
                                    item
                                    xs={4}
                                    display="flex"
                                    justifyContent="center"
                                    alignItems="center"
                                    border="0.2px solid"
                                    key={`${row}-${col}`}
                                >
                                    <div className={classes.cell}>
                                        {pos ? (
                                            pos === playerNumber ? (
                                                <CrossIcon
                                                    fontSize="inherit"
                                                    color={
                                                        pendingMove?.move.row === row && pendingMove.move.col === col
                                                            ? 'warning'
                                                            : undefined
                                                    }
                                                />
                                            ) : (
                                                <CircleIcon
                                                    fontSize="inherit"
                                                    color={
                                                        pendingMove?.move.row === row && pendingMove.move.col === col
                                                            ? 'warning'
                                                            : undefined
                                                    }
                                                />
                                            )
                                        ) : (
                                            <Rating
                                                onChange={() => handleMoveSelection(row, col)}
                                                size="large"
                                                icon={<CrossIcon fontSize="inherit" />}
                                                emptyIcon={<CrossIcon fontSize="inherit" style={{ opacity: 0.4 }} />}
                                                value={move?.col === col && move?.row == row ? 1 : 0}
                                                max={1}
                                            />
                                        )}
                                    </div>
                                </Grid>
                            )),
                        ],
                        [],
                    )}
                    <Divider flexItem orientation="horizontal" sx={{ margin: 0.5 }} />
                    <Grid item display="flex" justifyContent="center" xs={12}>
                        <Button fullWidth onClick={submitOffchainMove} disabled={!move}>
                            Submit Move (Off-Chain)
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default TicTacToe;
