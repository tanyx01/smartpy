import { Encoders, Packers } from 'state-channels-common';

export interface HeadTailState {
    hash1?: string;
    revealed1?: string;
    revealed2?: boolean;
}

/**
 * @description Pack game state
 * @param {HeadTailState} state Game state
 * @returns {string} Packed game state
 */
export function packState(state: HeadTailState): string {
    return Packers.packData(
        {
            prim: 'Pair',
            args: [
                state.hash1
                    ? {
                          prim: 'Some',
                          args: [
                              {
                                  bytes: state.hash1,
                              },
                          ],
                      }
                    : { prim: 'None' },
                state.revealed1
                    ? {
                          prim: 'Some',
                          args: [
                              {
                                  int: state.revealed1,
                              },
                          ],
                      }
                    : { prim: 'None' },
                state.revealed2
                    ? {
                          prim: 'Some',
                          args: [
                              {
                                  int: state.revealed2 ? '1' : '0',
                              },
                          ],
                      }
                    : { prim: 'None' },
            ],
        },
        {
            prim: 'pair',
            args: [
                {
                    prim: 'option',
                    args: [
                        {
                            prim: 'bytes',
                        },
                    ],
                    annots: ['%hash1'],
                },
                {
                    prim: 'option',
                    args: [
                        {
                            prim: 'nat',
                        },
                    ],
                    annots: ['%revealed1'],
                },
                {
                    prim: 'option',
                    args: [
                        {
                            prim: 'nat',
                        },
                    ],
                    annots: ['%revealed2'],
                },
            ],
        },
    );
}

/**
 * @description Unpack game state
 * @param {string} state Packed game state
 * @returns {HeadTailState} Unpacked game state
 */
export function unpackState(state: string): HeadTailState {
    const type: any = {
        prim: 'pair',
        args: [
            {
                prim: 'option',
                args: [
                    {
                        prim: 'bytes',
                    },
                ],
                annots: ['%hash1'],
            },
            {
                prim: 'pair',
                args: [
                    {
                        prim: 'option',
                        args: [
                            {
                                prim: 'nat',
                            },
                        ],
                        annots: ['%revealed1'],
                    },
                    {
                        prim: 'option',
                        args: [
                            {
                                prim: 'nat',
                            },
                        ],
                        annots: ['%revealed2'],
                    },
                ],
            },
        ],
    };

    return Encoders.Michelson.translateMichelson(type, state);
}

const HeadTailUtils = {
    packState,
    unpackState,
};

export default HeadTailUtils;
