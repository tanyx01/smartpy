import React from 'react';
import RouterButton from '../components/base/RouterButton';
import { Container, Grid } from '@mui/material';
import PlatformSelection from 'src/components/base/PlatformSelection';

const Home: React.FC = () => {
    return (
        <Container>
            <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={1}
                sx={{ marginBottom: 10 }}
            >
                <Grid item xs={6}>
                    <PlatformSelection fullWidth />
                </Grid>
            </Grid>
            <Grid container direction="row" justifyContent="center" alignItems="center" spacing={1}>
                <Grid item xs={3}>
                    <RouterButton to="/channels" fullWidth>
                        Channels
                    </RouterButton>
                </Grid>
                <Grid item xs={3}>
                    <RouterButton to="/models" fullWidth>
                        Models
                    </RouterButton>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Home;
