import { AccountInfo } from '@airgap/beacon-sdk';
import React from 'react';
import { Constants } from 'state-channels-common';
import Logger from '../services/logger';
import WalletService from '../services/wallet';
import WalletContext from './WalletContext';

const WalletProvider: React.FC = (props) => {
    const [loading, setLoading] = React.useState(true);
    const [account, setAccount] = React.useState<AccountInfo>();

    React.useEffect(() => {
        WalletService.Beacon.getActiveAccount()
            .then((account) => {
                setAccount(account);
            })
            .finally(() => setLoading(false));
    }, []);

    const connectWallet = async () => {
        try {
            setLoading(true);
            await WalletService.Beacon.connect({ name: Constants.network.type, rpc: Constants.rpc });
            const account = await WalletService.Beacon.getActiveAccount();
            setAccount(account);
        } catch (e) {
            Logger.error(e);
        } finally {
            setLoading(false);
        }
    };

    return (
        <WalletContext.Provider
            value={{
                pkh: account?.address,
                publicKey: account?.publicKey,
                connectWallet,
            }}
        >
            {loading ? 'Loading' : props.children}
        </WalletContext.Provider>
    );
};

export default WalletProvider;
