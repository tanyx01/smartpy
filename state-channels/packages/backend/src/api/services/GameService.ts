import type { Request, Response } from 'express';
import {
    GameSignature,
    Hashers,
    NewGameRequest,
    NewGameStateRequest,
    Packers,
    Cryptography,
} from 'state-channels-common';
import { GameDAO } from '../../database/daos';
import errors from '../enums/errors';

/**
 * @description Handles new game requests
 * @param {Request<void, void, NewGameRequest>} request http request
 * @param {Response} response  http response
 * @returns {Promise<Response>} A response to the request
 */
async function newGame(req: Request<void, void, NewGameRequest>, res: Response): Promise<Response> {
    const { signature, initParams, constants } = req.body;

    try {
        const gameID = Hashers.Game.hashID({
            channelID: constants.channelID,
            gameNonce: constants.gameNonce,
            modelID: constants.modelID,
        });
        const bytes = Packers.Game.packNewGameAction(initParams, constants);
        // Validate signature
        const publicKeyHash = await Cryptography.hashPublicKey(signature.publicKey);
        // Fail if public key is not owned by one of the players
        if (!Object.keys(constants.players).includes(publicKeyHash)) {
            return res.status(400).json({
                error: {
                    kind: errors.game.INVALID,
                    msg: `The following public key is not owned by any player: ${signature.publicKey}`,
                },
            });
        }
        const isValid = await Cryptography.verifySignature(bytes, signature.signature, signature.publicKey);
        // Fail if the signature is invalid
        if (!isValid) {
            return res.status(400).json({
                error: {
                    kind: errors.game.INVALID,
                    msg: `The following signature is invalid: ${signature.signature}`,
                },
            });
        }
        // Reaching this point means that all signatures are valid.
        await GameDAO.persistGame(gameID, initParams, constants, signature);
        return res.status(200).json({ result: 'Game successfully submitted.' });
    } catch (e) {
        console.debug(e);
        return res.status(500).json({
            error: {
                kind: errors.game.UNEXPECTED_ERROR,
                msg: 'Could not insert the new game.',
            },
        });
    }
}

/**
 * @description Handles new game confirmation requests
 * @param {Request<GameSignature>} request http request
 * @param {Response} response  http response
 * @returns {Promise<Response>} A response to the request
 */
async function acceptGame(req: Request<{ id: string }, void, GameSignature>, res: Response): Promise<Response> {
    const signature = req.body;
    const { id: gameID } = req.params;

    try {
        const game = await GameDAO.getGame(gameID);
        if (game) {
            const bytes = Packers.Game.packNewGameAction(game.init_params, {
                bonds: game.bonds,
                channelID: game.channel_id,
                gameNonce: game.game_nonce,
                modelID: game.model_id,
                players: game.players,
                settlements: game.settlements,
                playDelay: game.play_delay,
            });

            // Validate signature
            const publicKeyHash = await Cryptography.hashPublicKey(signature.publicKey);
            // Fail if public key is not owned by one of the players
            if (!Object.keys(game.players).includes(publicKeyHash)) {
                return res.status(400).json({
                    error: {
                        kind: errors.game.INVALID,
                        msg: `The following public key is not owned by any player: ${signature.publicKey}`,
                    },
                });
            }
            const isValid = await Cryptography.verifySignature(bytes, signature.signature, signature.publicKey);
            // Fail if the signature is invalid
            if (!isValid) {
                return res.status(400).json({
                    error: {
                        kind: errors.game.INVALID,
                        msg: `The following signature is invalid: ${signature.signature}`,
                    },
                });
            }

            // The signature is valid, lets persist it.
            await GameDAO.persistGameSignature(gameID, signature);
            return res.status(200).json({ result: 'Game was successfully accepted.' });
        }

        return res.status(404).json({
            error: {
                kind: errors.game.NOT_FOUND,
                msg: `Game ${gameID} does not exist.`,
            },
        });
    } catch (e) {
        console.debug(e);
        return res.status(500).json({
            error: {
                kind: errors.game.UNEXPECTED_ERROR,
                msg: 'Could not accept game.',
            },
        });
    }
}

/**
 * @description Handles new game state requests
 * @param {Request<NewGameStateRequest>} request http request
 * @param {Response} response  http response
 * @returns {Promise<Response>} A response to the request
 */
async function gameMove(req: Request<{ id: string }, void, NewGameStateRequest>, res: Response): Promise<Response> {
    const { signature, current, move, state, moveBytes } = req.body;
    const { id: gameID } = req.params;

    try {
        const game = await GameDAO.getGame(gameID);
        if (game) {
            const bytes = Packers.Game.packPlayAction(gameID, current, state, moveBytes);

            // Validate signature
            const publicKeyHash = await Cryptography.hashPublicKey(signature.publicKey);
            // Fail if public key is not owned by one of the players
            if (!Object.keys(game.players).includes(publicKeyHash)) {
                return res.status(400).json({
                    error: {
                        kind: errors.game.INVALID,
                        msg: `The following public key is not owned by any player: ${signature.publicKey}`,
                    },
                });
            }
            const isValid = await Cryptography.verifySignature(bytes, signature.signature, signature.publicKey);
            // Fail if the signature is invalid
            if (!isValid) {
                return res.status(400).json({
                    error: {
                        kind: errors.game.INVALID,
                        msg: `The following signature is invalid: ${signature.signature}`,
                    },
                });
            }

            // The signature is valid, lets persist it.
            await GameDAO.persistGameMove(gameID, current, state, move, moveBytes, signature);
            return res.status(200).json({ result: 'Game movement was successfully submitted.' });
        }

        return res.status(404).json({
            error: {
                kind: errors.game.NOT_FOUND,
                msg: `Game ${gameID} does not exist.`,
            },
        });
    } catch (e) {
        console.debug(e);
        return res.status(500).json({
            error: {
                kind: errors.game.UNEXPECTED_ERROR,
                msg: 'Could not insert game movement.',
            },
        });
    }
}

/**
 * @description Handles the confirmation of new game state requests
 */
async function acceptGameMove(
    req: Request<{ gameID: string; moveNumber: string }, void, GameSignature>,
    res: Response,
): Promise<Response> {
    const signature = req.body;
    const { gameID, moveNumber } = req.params;
    try {
        const gameMove = await GameDAO.getGameMove(gameID, Number(moveNumber));
        if (gameMove) {
            const bytes = Packers.Game.packPlayAction(
                gameID,
                {
                    moveNumber: gameMove.move_number,
                    player: gameMove.player_number,
                    outcome: gameMove.outcome,
                },
                gameMove.state,
                gameMove.move_bytes,
            );

            // Validate signature
            const players = await GameDAO.getPlayers(gameID);
            const publicKeyHash = await Cryptography.hashPublicKey(signature.publicKey);
            // Fail if public key is not owned by one of the players
            if (!Object.keys(players).includes(publicKeyHash)) {
                return res.status(400).json({
                    error: {
                        kind: errors.game.INVALID,
                        msg: `The following public key is not owned by any player: ${signature.publicKey}`,
                    },
                });
            }
            const isValid = await Cryptography.verifySignature(bytes, signature.signature, signature.publicKey);
            // Fail if the signature is invalid
            if (!isValid) {
                return res.status(400).json({
                    error: {
                        kind: errors.game.INVALID,
                        msg: `The following signature is invalid: ${signature.signature}`,
                    },
                });
            }

            // The signature is valid, lets persist it.
            await GameDAO.persistGameMoveSignature(gameMove.id, signature);
            return res.status(200).json({ result: 'Game movement was successfully accepted.' });
        }

        return res.status(404).json({
            error: {
                kind: errors.game.NOT_FOUND,
                msg: `Game ${gameID} does not have any movement ${moveNumber}.`,
            },
        });
    } catch (e) {
        console.debug(e);
        return res.status(500).json({
            error: {
                kind: errors.game.UNEXPECTED_ERROR,
                msg: 'Could not accept game movement.',
            },
        });
    }
}

export default {
    newGame,
    acceptGame,
    gameMove,
    acceptGameMove,
};
