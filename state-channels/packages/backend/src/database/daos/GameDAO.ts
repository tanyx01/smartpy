import { GameConstants, GameCurrent, GameSignature, SC_Settlement } from 'state-channels-common';
import { runStatement, runTransaction } from '../index';

/**
 * @description Persist a game
 * @param {string} gameID Game identifier
 * @param {string} initParams Game initialization parameters
 * @param {GameConstants} constants Game constants
 * @param {GameSignature} signature Game signature
 * @returns {Promise<void>} A promise that resolves to nothing
 */
async function persistGame(
    gameID: string,
    initParams: string,
    constants: GameConstants,
    signature: GameSignature,
): Promise<void> {
    return runTransaction(async (client) => {
        await client.query(
            `
                INSERT INTO game(
                    id,
                    on_chain,
                    init_params,
                    players,
                    bonds,
                    channel_id,
                    game_nonce,
                    model_id,
                    play_delay,
                    settlements,
                    current_player,
                    move_nb
                ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);
            `,
            [
                gameID,
                false,
                initParams,
                JSON.stringify(constants.players),
                JSON.stringify(constants.bonds),
                constants.channelID,
                constants.gameNonce,
                constants.modelID,
                constants.playDelay,
                JSON.stringify(constants.settlements),
                1, // Current player always starts with player 1
                0, // Movement number starts on 0
            ],
        );

        await client.query({
            text: `
                    INSERT INTO game_signature(
                        game_id,
                        public_key,
                        signature
                    ) VALUES ($1, $2, $3);`,
            values: [gameID, signature.publicKey, signature.signature],
        });
    });
}

/**
 * @description Persist game signatures
 * @param {string} gameID Game identifier
 * @param {GameSignature} signature Game signature
 * @returns {Promise<void>} A promise that resolves to nothing
 */
async function persistGameSignature(gameID: string, signature: GameSignature): Promise<void> {
    return runTransaction(async (client) => {
        await client.query({
            text: `
                    INSERT INTO game_signature(
                        game_id,
                        public_key,
                        signature
                    ) VALUES ($1, $2, $3);`,
            values: [gameID, signature.publicKey, signature.signature],
        });
    });
}

/**
 * @description Persist game move
 * @param {string} gameID Game identifier
 * @param {GameCurrent} current Game current
 * @param {string} state Game state
 * @param {GameSignature} signature Game signature
 * @returns {Promise<void>} A promise that resolves to nothing
 */
async function persistGameMove(
    gameID: string,
    current: GameCurrent,
    state: string,
    move: JSON,
    moveBytes: string,
    signature: GameSignature,
): Promise<void> {
    return runTransaction(async (client) => {
        const [{ id }] = (
            await client.query(
                `
                INSERT INTO game_move(
                    game_id,
                    player_number,
                    move_number,
                    outcome,
                    move_data,
                    move_bytes,
                    state
                ) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id;
            `,
                [gameID, current.player, current.moveNumber, JSON.stringify(current.outcome), move, moveBytes, state],
            )
        ).rows;

        await client.query({
            text: `
                    INSERT INTO game_move_signature(
                        game_move_id,
                        public_key,
                        signature
                    ) VALUES ($1, $2, $3);`,
            values: [id, signature.publicKey, signature.signature],
        });
    });
}

/**
 * @description Persist game move signature
 * @param {string} gameMoveID Game movement identifier
 * @param {GameSignature} signature Game signature
 * @returns {Promise<void>} A promise that resolves to nothing
 */
async function persistGameMoveSignature(gameMoveID: number, signature: GameSignature): Promise<void> {
    return runTransaction(async (client) => {
        await client.query({
            text: `
                    INSERT INTO game_move_signature(
                        game_move_id,
                        public_key,
                        signature
                    ) VALUES ($1, $2, $3);`,
            values: [gameMoveID, signature.publicKey, signature.signature],
        });
    });
}

interface Game {
    id: string;
    state: string;
    init_params: string;
    bonds: Record<number, { [bound: number]: number }>;
    players: {
        [address: string]: number;
    };
    settlements: SC_Settlement[];
    current_player: number;
    channel_id: string;
    model_id: string;
    game_nonce: string;
    play_delay: number;
    move_nb: number;
    outcome: Record<string, string>;
}

/**
 * @description Get game
 * @param {string} gameID Game identifier
 * @returns {Game} Game movement row
 */
async function getGame(gameID: string): Promise<Game> {
    return runStatement(async (client) => {
        const [result] = (
            await client.query<Game>({
                text: 'SELECT * FROM game WHERE id = $1;',
                values: [gameID],
            })
        ).rows;
        return result;
    });
}

/**
 * @description Get game players
 * @param gameID Game identifier
 * @returns {{ [address: string]: number }} A dictionary with all players addresses and their numbers
 */
async function getPlayers(gameID: string): Promise<{ [address: string]: number }> {
    return runStatement(async (client) => {
        const [{ players }] = (
            await client.query<{ players: { [address: string]: number } }>({
                text: 'SELECT players FROM game WHERE id = $1;',
                values: [gameID],
            })
        ).rows;
        return players;
    });
}

interface GameMove {
    id: number;
    state: string;
    move_bytes: string;
    player_number: number;
    move_number: number;
    outcome: Record<string, string>;
}

/**
 * @description Get game movement
 * @param {string} gameID Game identifier
 * @param {number} gameMove Game movement number
 * @returns {GameMove} Game movement row
 */
async function getGameMove(gameID: string, gameMove: number): Promise<GameMove> {
    return runStatement(async (client) => {
        const [result] = (
            await client.query<GameMove>({
                text: 'SELECT * FROM game_move WHERE game_id = $1 AND move_number = $2;',
                values: [gameID, gameMove],
            })
        ).rows;
        return result;
    });
}

export default {
    persistGame,
    persistGameSignature,
    persistGameMove,
    persistGameMoveSignature,
    getGame,
    getPlayers,
    getGameMove,
};
