-- /////////////////
--
-- Stored Procedures
--
-- /////////////////

--
-- Get the total signatures of game_move
--
CREATE OR REPLACE FUNCTION public.game_move_signature_count(move game_move)
    RETURNS BIGINT
    LANGUAGE SQL
    STABLE
AS $function$
    SELECT COUNT(*) FROM game_move_signature sig WHERE sig.game_move_id = move.id
$function$
