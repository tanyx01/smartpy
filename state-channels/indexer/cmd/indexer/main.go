package main

import (
	IndexerConfig "github.com/romarq/tezplorer/internal/config/indexer"
	Crawler "github.com/romarq/tezplorer/internal/crawler"
	DB "github.com/romarq/tezplorer/internal/data"
	LOG "github.com/romarq/tezplorer/internal/logger"
	RPC "github.com/romarq/tezplorer/internal/rpc"
)

func main() {
	configuration := IndexerConfig.GetConfig()
	LOG.SetupLogger(configuration.Log.Location, configuration.Log.Level)

	LOG.Info("Initializing Tezplorer Indexer...")

	// Instantiate the RPC caller
	rpc, err := RPC.New(configuration.RPC.URL)
	if err != nil {
		LOG.Fatal("Could not connect to network: %v", err)
	}

	// Connect to PostgreSQL Database
	db, err := DB.New(configuration.DB.URL)
	if err != nil {
		LOG.Fatal("Could not connect to Database: %v", err)
	}
	defer db.Close()

	// Start crawler routine
	Crawler.Initialize(rpc, db, configuration)
}
