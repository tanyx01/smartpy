#!/bin/sh

DB_NAME=tezplorer
DB_SCHEMA_FILE=$(realpath $(dirname $0))/../docker/data/database.sql

# Clear old schema
psql -h localhost -U postgres -p 5432 <<EOF
CREATE DATABASE $DB_NAME;
--CREATE USER smartpy WITH ENCRYPTED PASSWORD '5martpy!';
GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO smartpy;
EOF

# Create schema
psql -h localhost -d $DB_NAME -U postgres -p 5432 -a -q -f $DB_SCHEMA_FILE 1>/dev/null
