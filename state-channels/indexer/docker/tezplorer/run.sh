#!/bin/sh

# Wait for boostapping to finish
sleep 20

# Run indexer
./indexer

# For debugging
while true
do
    sleep 5
    tail tezplorer-indexer.log
done
