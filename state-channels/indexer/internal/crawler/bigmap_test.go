package crawler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"

	"blockwatch.cc/tzgo/micheline"
	IndexerConfig "github.com/romarq/tezplorer/internal/config/indexer"
	"github.com/romarq/tezplorer/pkg/utils"
)

// Regex of the paths being mocked in tests
func pathContractScript(contract string) *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("/chains/main/blocks/[A-z0-9]+/context/contracts/%s/script/normalized", contract))
}

func Test_extractGameChannel(t *testing.T) {
	wd, _ := os.Getwd()

	scriptNormalizedFile, err := os.Open(path.Join(wd, "testdata/games_bigmap_script.json"))
	if err != nil {
		t.Fatal(err)
	}
	defer scriptNormalizedFile.Close()
	scriptNormalizedJSON, err := ioutil.ReadAll(scriptNormalizedFile)
	if err != nil {
		t.Fatal(err)
	}

	requestHandlers := []RequestHandler{
		{
			RegPath:  pathContractScript("KT1_platform"),
			Response: scriptNormalizedJSON,
		},
	}
	mockServer := httptest.NewServer(mockRequestHandler(requestHandlers))
	defer mockServer.Close()

	crawler := Crawler{
		Config: IndexerConfig.Config{
			RPC: IndexerConfig.RPCConfig{
				URL: mockServer.URL,
			},
			Chain: IndexerConfig.ChainConfig{
				Platforms: []string{"KT1_platform"},
			},
		},
	}

	t.Run("Exract game big_map difference",
		func(t *testing.T) {
			bigMapDiffFile, err := os.Open(path.Join(wd, "testdata/games_bigmap_diff.json"))
			if err != nil {
				t.Fatal(err)
			}
			defer bigMapDiffFile.Close()
			bigMapDiffJSON, err := ioutil.ReadAll(bigMapDiffFile)
			if err != nil {
				t.Fatal(err)
			}
			var bigMapDiff micheline.BigmapDiffElem
			err = json.Unmarshal(bigMapDiffJSON, &bigMapDiff)
			if err != nil {
				t.Fatal(err)
			}

			result, err := crawler.extractBigMapDiff(17615, "%games", "KT1_platform", bigMapDiff)
			if err != nil {
				t.Fatal(err)
			}
			fmt.Println(utils.PrettifyJSON(result))

			game, err := parseGame(result)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, game, game_type{
				ID:         "efe7a7a0bbfb3191fcc8415823650c2e6afceaf0b29b0c6a677198afd3171d0c",
				InitParams: "05030b",
				Settled:    false,
				State:      "",
				Metadata:   map[string]string{},
				// current
				Outcome:       json.RawMessage("null"),
				MoveNumber:    0,
				CurrentPlayer: 1,
				// constants
				Players: map[string]int64{
					"tz1NHco4L3SqhC8RboiSqXPZCRZsEgym3mff": int64(1),
					"tz1THWgwHiDfCBk2cjipYPLE3DA6hWqmgEBX": int64(2),
				},
				Bonds: map[int64]map[int64]int64{
					int64(1): {
						int64(0): int64(1),
					},
					int64(2): {
						int64(0): int64(1),
					},
				},
				ChannelID: "81d6e2fae5bf2d18dee5e6a17d1ee5b39a8be604726b9b6c868a7ae250799d48",
				Nonce:     "22425146978",
				ModelID:   "4261e0508a06644ceb9339d4b85bde0c96f6e14fb0c5c01044d37f477e70b8e3",
				PlayDelay: int64(10),
				Settlements: []gameSettlement{
					{
						Kind:      "game_finished",
						Value:     "player_1_own",
						Sender:    2,
						Recipient: 1,
						Bonds: map[int64]int64{
							int64(0): int64(2000000),
						},
					},
					{
						Kind:      "player_double_played",
						Value:     "1",
						Sender:    2,
						Recipient: 1,
						Bonds: map[int64]int64{
							int64(1): int64(4),
						},
					},
					{
						Kind:      "player_double_played",
						Value:     "2",
						Sender:    1,
						Recipient: 2,
						Bonds: map[int64]int64{
							int64(1): int64(4),
						},
					},
				},
				Timeouts: map[int64]int64{
					int64(1): int64(0),
					int64(2): int64(0),
				},
			})
		})

	t.Run("Extract (channels) big_map difference",
		func(t *testing.T) {
			bigMapDiffFile, err := os.Open(path.Join(wd, "testdata/channels_bigmap_diff.json"))
			if err != nil {
				t.Fatal(err)
			}
			defer bigMapDiffFile.Close()
			bigMapDiffJSON, err := ioutil.ReadAll(bigMapDiffFile)
			if err != nil {
				t.Fatal(err)
			}
			var bigMapDiff micheline.BigmapDiffElem
			err = json.Unmarshal(bigMapDiffJSON, &bigMapDiff)
			if err != nil {
				t.Fatal(err)
			}

			result, err := crawler.extractBigMapDiff(17615, "%channels", "KT1_platform", bigMapDiff)
			if err != nil {
				t.Fatal(err)
			}

			fmt.Println(utils.PrettifyJSON(result))

			channel, err := parseChannel(result)
			if err != nil {
				t.Fatal(err)
			}

			fmt.Println(utils.PrettifyJSON(channel))
		})
}

// ---------
//  Helpers
// ---------
type RequestHandler struct {
	RegPath  *regexp.Regexp
	Response []byte
}

func mockRequestHandler(rh []RequestHandler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		for _, req := range rh {
			if req.RegPath.MatchString(r.URL.String()) {
				w.Write(req.Response)
				return
			}
		}
	})
}
