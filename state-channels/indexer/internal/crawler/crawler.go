package crawler

import (
	"database/sql"
	"fmt"
	"time"

	"blockwatch.cc/tzgo/micheline"
	"blockwatch.cc/tzgo/tezos"
	"github.com/pkg/errors"
	LOG "github.com/romarq/tezplorer/internal/logger"
	"github.com/romarq/tezplorer/internal/monitor"

	IndexerConfig "github.com/romarq/tezplorer/internal/config/indexer"
	RPC "github.com/romarq/tezplorer/internal/rpc"
	"github.com/romarq/tezplorer/pkg/utils"
)

type ChainState struct {
	ChainID    string
	BlockHash  string
	BlockLevel int64
}

type PlatformStorage struct {
	Admins  []string
	Ledger  string
	BigMaps map[string]int64
}

type LedgerStorage struct {
	BigMaps map[string]int64
}

type Crawler struct {
	ChainState ChainState
	Platforms  map[string]*PlatformStorage
	Ledgers    map[string]LedgerStorage
	DB         *sql.DB
	RPC        *RPC.RPC
	Config     IndexerConfig.Config
}

func (c *Crawler) fetchChainState() error {
	err := c.DB.QueryRow("SELECT chain_id, l_block_level, l_block_hash FROM state LIMIT 1").Scan(
		&c.ChainState.ChainID,
		&c.ChainState.BlockLevel,
		&c.ChainState.BlockHash,
	)

	if err == sql.ErrNoRows {
		header, err := c.RPC.GetBlockHeaderByLevel(c.Config.Chain.Level - 1)
		if err != nil {
			return err
		}
		// Store state
		err = c.addState(header)
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}

	return nil
}

func (c *Crawler) addState(block *RPC.BlockHeader) error {
	if block.ChainId == nil {
		return fmt.Errorf("ChainId is nil")
	}
	if block.Hash == nil {
		return fmt.Errorf("BlockHash is nil")
	}
	chainId := block.ChainId.String()
	blockHash := block.Hash.String()

	insertStmt, err := c.DB.Prepare(`INSERT INTO state(chain_id, l_block_level, l_block_hash) VALUES($1, $2, $3)`)
	if err != nil {
		return errors.Wrap(err, "Could not prepate insert statement for table state.")
	}

	_, err = insertStmt.Exec(
		chainId,
		block.Level,
		blockHash,
	)
	if err != nil {
		return errors.Wrap(err, "Could not insert state.")
	}

	c.ChainState.ChainID = chainId
	c.ChainState.BlockLevel = block.Level
	c.ChainState.BlockHash = blockHash

	return nil
}

func (c *Crawler) updateState(block *RPC.Block) error {
	if c.ChainState.ChainID != block.ChainId.String() {
		return errors.Errorf("Chain identifier mismatch: '%s' != '%s'", c.ChainState.ChainID, block.ChainId.String())
	}
	if c.ChainState.BlockHash != block.Header.Predecessor.String() {
		LOG.Debug("Predecessor unexpected, expected '%s' but received '%s'", c.ChainState.BlockHash, block.Header.Predecessor.String())
		var err error
		if c.ChainState.BlockLevel >= 1000 {
			block, err = c.RPC.GetBlockByLevel(c.ChainState.BlockLevel - 1000)
		} else {
			block, err = c.RPC.GetBlockByLevel(0)
		}
		if err != nil {
			return errors.Wrap(err, "Could not reorg data.")
		}
	}
	chainId := block.ChainId.String()
	blockHash := block.Hash.String()

	updateStmt, err := c.DB.Prepare(`UPDATE state SET l_block_level = $1, l_block_hash = $2 WHERE chain_id = $3`)
	if err != nil {
		return errors.Wrap(err, "Could not prepare update statement for table (state).")
	}

	_, err = updateStmt.Exec(
		block.Header.Level,
		blockHash,
		chainId,
	)
	if err != nil {
		return errors.Wrap(err, "Could not update state.")
	}

	c.ChainState.BlockLevel = block.Header.Level
	c.ChainState.BlockHash = blockHash

	return nil
}

func (c *Crawler) addPlatform(address string, level int64) error {
	c.Platforms[address] = &PlatformStorage{
		Admins:  []string{},
		BigMaps: make(map[string]int64),
	}

	script, err := c.RPC.GetContractScriptAtLevel(address, level)
	if err != nil {
		return errors.Wrap(err, "Could not fetch platform contract script.")
	}

	storage := micheline.NewValue(script.StorageType(), script.Storage)
	if ledger, exists := storage.GetAddress("ledger"); exists {
		c.Platforms[address].Ledger = ledger.ContractAddress()
	}
	if adminsRaw, exist := storage.GetValue("admins"); exist {
		arr, ok := adminsRaw.([]interface{})
		if !ok {
			return errors.Errorf("Invalid storage, expected field (admins) to contain a value of type ([]interface {}) but received %T.", adminsRaw)
		}
		for _, v := range arr {
			c.Platforms[address].Admins = append(c.Platforms[address].Admins, fmt.Sprint(v))
		}
	} else {
		return errors.Errorf("Invalid storage, expected to find a field named (admins).")
	}

	bigMaps := script.BigmapsByName()
	for name, id := range bigMaps {
		c.Platforms[address].BigMaps[name] = id
	}

	var exists bool
	err = c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM platform WHERE id = $1)", address).Scan(&exists)
	if err != nil {
		return errors.Wrap(err, "Could not query table (platform).")
	}
	if !exists {
		insertStmt, err := c.DB.Prepare(`INSERT INTO platform (id, ledger) VALUES ($1, $2)`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare insert statement for table (platform).")
		}

		_, err = insertStmt.Exec(address, c.Platforms[address].Ledger)
		if err != nil {
			return errors.Wrap(err, "Could not insert new platform.")
		}
	}

	return nil
}

func (c *Crawler) addLedger(address string, level int64) error {
	c.Ledgers[address] = LedgerStorage{
		BigMaps: make(map[string]int64),
	}

	var exists bool
	err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM ledger WHERE id = $1)", address).Scan(&exists)
	if err != nil {
		return errors.Wrap(err, "Could not query table (ledger).")
	}
	if !exists {
		insertStmt, err := c.DB.Prepare(`INSERT INTO ledger (id) VALUES ($1)`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare insert statement for table (ledger).")
		}

		_, err = insertStmt.Exec(address)
		if err != nil {
			return errors.Wrap(err, "Could not insert new ledger.")
		}
	}

	script, err := c.RPC.GetContractScriptAtLevel(address, level)
	if err != nil {
		return errors.Wrap(err, "Could not fetch ledger contract script.")
	}

	bigMaps := script.BigmapsByName()
	for name, id := range bigMaps {
		c.Ledgers[address].BigMaps[name] = id
	}
	return nil
}

func (c *Crawler) applyBigMapDiff(bigMapEl micheline.BigmapDiffElem, address string, blockTime time.Time) error {
	if utils.Contains(c.Config.Chain.Platforms, address) {
		switch bigMapEl.Id {
		case c.Platforms[address].BigMaps["metadata"]:
			switch bigMapEl.Action {
			case micheline.DiffActionUpdate:
				metadata, err := c.extractBigMapDiff(
					bigMapEl.Id,
					"%metadata",
					address,
					bigMapEl,
				)
				if err != nil {
					return errors.Wrap(err, "Could not extract (metadata) big map difference")
				}

				ipfsLink, ok := metadata.Value.(string)
				if !ok {
					LOG.Debug("Platform metadata is invalid.")
				}

				updateStmt, err := c.DB.Prepare("UPDATE platform set metadata = $1 WHERE id = $2")
				if err != nil {
					return errors.Wrap(err, "Could not prepare update statement for table (platform).")
				}

				_, err = updateStmt.Exec(ipfsLink, address)
				if err != nil {
					return errors.Wrap(err, "Could not insert new platform.")
				}

				if err != nil {
					return errors.Wrap(err, "Could not update platform table")
				}
			default:
				LOG.Debug("unexpected action (%s) on channels big_map", bigMapEl.Action.String())
			}
		case c.Platforms[address].BigMaps["channels"]:
			switch bigMapEl.Action {
			case micheline.DiffActionUpdate:
				values, err := c.extractBigMapDiff(
					bigMapEl.Id,
					"%channels",
					address,
					bigMapEl,
				)
				if err != nil {
					return errors.Wrap(err, "Could not extract (channels) big map difference")
				}

				fmt.Println(utils.PrettifyJSON(values))

				channel, err := parseChannel(values)
				if err != nil {
					return errors.Wrap(err, "Could not parse channel entry")
				}

				fmt.Println(utils.PrettifyJSON(channel))

				err = c.updateChannel(channel, blockTime, address)
				if err != nil {
					return errors.Wrap(err, "Could not update channel table")
				}
			default:
				LOG.Debug("unexpected action (%s) on channels big_map", bigMapEl.Action.String())
			}
		case c.Platforms[address].BigMaps["model_metadata"]:
			switch bigMapEl.Action {
			case micheline.DiffActionUpdate:
				values, err := c.extractBigMapDiff(
					bigMapEl.Id,
					"%model_metadata",
					address,
					bigMapEl,
				)
				if err != nil {
					return errors.Wrap(err, "Could not extract (model_metadata) big map difference")
				}

				fmt.Println(utils.PrettifyJSON(values))
				err = c.updateModelMetadata(values, blockTime)
				if err != nil {
					return errors.Wrap(err, "Could not update (model) table")
				}
			default:
				LOG.Debug("unexpected action (%s) on models big_map", bigMapEl.Action.String())
			}
		case c.Platforms[address].BigMaps["models"]:
			switch bigMapEl.Action {
			case micheline.DiffActionUpdate:
				values, err := c.extractBigMapDiff(
					bigMapEl.Id,
					"%models",
					address,
					bigMapEl,
				)
				if err != nil {
					return errors.Wrap(err, "Could not extract (models) big map difference")
				}

				fmt.Println(utils.PrettifyJSON(values))

				model, err := parseModel(values)
				if err != nil {
					return errors.Wrap(err, "Could not parse model entry")
				}

				err = c.updateModel(model, blockTime, address)
				if err != nil {
					return errors.Wrap(err, "Could not update model table")
				}
			default:
				LOG.Debug("unexpected action (%s) on models big_map", bigMapEl.Action.String())
			}
		case c.Platforms[address].BigMaps["games"]:
			switch bigMapEl.Action {
			case micheline.DiffActionUpdate:
				values, err := c.extractBigMapDiff(
					bigMapEl.Id,
					"%games",
					address,
					bigMapEl,
				)
				if err != nil {
					return errors.Wrap(err, "Could not extract (games) big map difference")
				}

				fmt.Println(utils.PrettifyJSON(values))

				game, err := parseGame(values)
				if err != nil {
					return errors.Wrap(err, "Could not parse game entry")
				}

				fmt.Println(utils.PrettifyJSON(game))

				err = c.updateGame(game, blockTime)
				if err != nil {
					return errors.Wrap(err, "Could not update game table")
				}

			default:
				LOG.Debug("unexpected action (%s) on games big_map", bigMapEl.Action.String())
			}
		default:
			LOG.Debug("Discarding big_map (%d) update. %s", bigMapEl.Id, address)
		}
	} else if utils.Contains(c.Config.Chain.Ledgers, address) {
		switch bigMapEl.Id {
		case c.Ledgers[address].BigMaps["token_metadata"]:
			switch bigMapEl.Action {
			case micheline.DiffActionUpdate:
				values, err := c.extractBigMapDiff(
					bigMapEl.Id,
					"%token_metadata",
					address,
					bigMapEl,
				)
				if err != nil {
					return errors.Wrap(err, "Could not extract (token_metadata) big map difference")
				}

				fmt.Println(utils.PrettifyJSON(values))

				metadata, err := parseMetadata(values)
				if err != nil {
					return errors.Wrap(err, "Could not parse token_metadata entry")
				}

				fmt.Println(utils.PrettifyJSON(metadata))

				err = c.updateMetadata(metadata, blockTime, address)
				if err != nil {
					return errors.Wrap(err, "Could not update token_metadata table")
				}
			default:
				return fmt.Errorf("unexpected action (%s) on token_metadata big_map", bigMapEl.Action.String())
			}
		default:
			LOG.Debug("Discarding big_map (%d) update. %s", bigMapEl.Id, address)
		}
	}
	return nil
}

func (c *Crawler) addBlockOperations(block *RPC.Block) error {
	blockTime := block.Header.Timestamp
	for _, operations := range block.Operations {
		for _, operation := range operations {
			for _, content := range operation.Contents {
				switch content.OpKind() {
				case RPC.OpTypeOrigination:
					origination := content.(*RPC.Origination)

					// Skip if operation failed
					if origination.Metadata.Result.Status != tezos.OpStatusApplied {
						continue
					}

					// Check if we are monitoring this contract and store it if that is the case
					for _, address := range origination.Metadata.Result.OriginatedContracts {
						address := address.ContractAddress()
						if utils.Contains(c.Config.Chain.Ledgers, address) {
							if err := c.addLedger(address, block.GetLevel()); err != nil {
								LOG.Debug("Error when adding ledger: %v", err)
							}
						}
						if utils.Contains(c.Config.Chain.Platforms, address) {
							if err := c.addPlatform(address, block.GetLevel()); err != nil {
								LOG.Debug("Error when adding platform: %v", err)
							}
							LOG.Debug("- %v", c.Platforms[address].BigMaps)
						}
						for _, bigMapEl := range origination.Metadata.Result.BigmapDiff {
							if err := c.applyBigMapDiff(bigMapEl, address, blockTime); err != nil {
								LOG.Debug("Error when applying big map diff  %v", err)
							}
						}
					}

				case RPC.OpTypeTransaction:
					transaction := content.(*RPC.Transaction)
					destinationAddress := transaction.Destination.String()

					// Skip if operation failed
					if transaction.Metadata.Result.Status != tezos.OpStatusApplied {
						continue
					}

					// Apply big_map diffs contained in manager transactions
					for _, bigMapEl := range transaction.Metadata.Result.BigmapDiff {
						if err := c.applyBigMapDiff(bigMapEl, destinationAddress, blockTime); err != nil {
							LOG.Debug("Error when applying big map diff  %v", err)
						}
					}

					// Apply big_map diffs contained in internal transactions
					for _, op := range transaction.Metadata.InternalResults {
						switch op.OpKind() {
						case RPC.OpTypeTransaction:
							destinationAddress = op.Destination.ContractAddress()
							for _, bigMapEl := range op.Result.BigmapDiff {
								if err := c.applyBigMapDiff(bigMapEl, destinationAddress, blockTime); err != nil {
									LOG.Debug("Error when applying big map diff  %v", err)
								}
							}
						default:
							// Skip (only check transactions)
						}
					}
				}
			}
		}
	}

	return nil
}

func (c *Crawler) monitorHeads() error {
	heads := make(chan RPC.BlockHeader)

	blockMonitor := monitor.New(c.Config.RPC.URL + "/monitor/heads/main")
	go blockMonitor.MonitorHeads(heads)

	for {
		head, ok := <-heads
		if !ok {
			return errors.Errorf("Failed to monitor heads.")
		}

		if c.ChainState.BlockLevel >= int64(head.Level) {
			LOG.Debug("Level %d already processed, skipping...", head.Level)
			continue
		}

		LOG.Debug("Processing new block %s...", head.Hash)

		err := c.addBlock(head.Level)
		if err != nil {
			LOG.Debug("%v", err)
			return err
		}
	}
}

func (c *Crawler) addBlock(level int64) error {
	if c.ChainState.BlockLevel+1 != level {
		return errors.Errorf("Level too far, expected %d but received %d", c.ChainState.BlockLevel+1, level)
	}

	block, err := c.RPC.GetBlockByLevel(level)
	if err != nil {
		return errors.Wrap(err, "Failed to fetch block by level")
	}

	LOG.Info("Processing block: %d...", level)
	err = c.addBlockOperations(block)
	if err != nil {
		return err
	}

	err = c.updateState(block)
	if err != nil {
		return err
	}

	return nil
}

func (c *Crawler) synchronize() error {
	// This flag is turned [ON] when an error occurs and reset
	// to [OFF] at the begining of the loop after waiting a few seconds
	retry := false
	for {
		if retry {
			time.Sleep(5 * time.Second)
			retry = false
		}

		block, err := c.RPC.GetHeadBlockHeader()
		if err != nil {
			LOG.Debug("Could not get head block header: %v", err)
			retry = true
			continue
		}

		if c.ChainState.BlockLevel == block.Level {
			LOG.Info("Nonitoring new blocks...")
			err = c.monitorHeads()
			if err != nil {
				LOG.Debug("Failed when listening for new blocks: %v", err)
				retry = true
				continue
			}
		}

		LOG.Debug("Synchronyzing (%d of %d)", c.ChainState.BlockLevel, block.Level)
		for c.ChainState.BlockLevel < block.Level {
			err = c.addBlock(c.ChainState.BlockLevel + 1)
			if err != nil {
				LOG.Debug("%v", err)
				retry = true
				continue
			}

			// If the synchronization process completed go back to the beginning of the loop
			if c.ChainState.BlockLevel == block.Level {
				continue
			}
		}

	}
}

// Initialize the tezplorer crawler
func Initialize(rpc *RPC.RPC, db *sql.DB, configuration IndexerConfig.Config) Crawler {
	crawler := Crawler{
		DB:         db,
		RPC:        rpc,
		Config:     configuration,
		ChainState: ChainState{},
		Platforms:  make(map[string]*PlatformStorage),
		Ledgers:    make(map[string]LedgerStorage),
	}
	// Fetch current state
	err := crawler.fetchChainState()
	if err != nil {
		LOG.Error("Failed to fetch chain state. %v", err)
	}

	for _, platform := range configuration.Chain.Platforms {
		crawler.addPlatform(platform, crawler.ChainState.BlockLevel)
		if err != nil {
			LOG.Error("Failed to fetch platforms. %v", err)
		}
	}

	for _, ledger := range configuration.Chain.Ledgers {
		crawler.addLedger(ledger, crawler.ChainState.BlockLevel)
		if err != nil {
			LOG.Error("Failed to fetch ledgers. %v", err)
		}
	}

	err = crawler.synchronize()
	if err != nil {
		LOG.Error("Failed to update chain indexing. %v", err)
	}

	return crawler
}
