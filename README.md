# SmartPy and SmartPy.io

SmartPy and SmartPy.io are an Intuitive and Effective Smart Contract Language and Platform for Tezos.

## Links

### SmartPy.io

[SmartPy.io](https://SmartPy.io)

### Source code

Gitlab: https://gitlab.com/SmartPy/SmartPy

### On social media

Twitter: https://twitter.com/SmartPy_io

Medium: https://smartpy-io.medium.com

Telegram: https://t.me/SmartPy_io

## Installation

### Clone the SmartPy repository

You can clone the SmartPy repo by running:

```
git clone --recurse-submodules https://gitlab.com/SmartPy/SmartPy
```

If you forgot to specify `--recurse-submodules`, you can fix this with:

```
git submodule init
git submodule update
```

### Install environment and dependencies

The simplest and recommended way of building SmartPy is through
[Nix](https://nixos.org/explore.html).

For this, you first need to
[install Nix](https://nixos.org/nix/download.html), then activate the
"flakes" feature (details
[here](https://nixos.wiki/wiki/Flakes#Non-NixOS)) by running:

```
mkdir -p ~/.config/nix
echo 'experimental-features = nix-command flakes' >> ~/.config/nix/nix.conf
```

You can then run:

```
env/init
```


### Use the SmartPy environment

We can launch commands with the SmartPy environment by:

```
./with_env <command>
```

When using `make`, `./with_env` is optional, but will be slightly faster.

## First build

To build for the first time:

```
./with_env make full
```

## Regular build

To build afterwards:

```
./with_env make
```

## Getting started

To run the in-browser version locally, you can type:

It will be available at http://localhost:3000
```sh
$ make frontend-start
```

Build frontend for production:

The production bundle will be at [frontend/build](frontend/build)
```sh
$ make packages-build
```

Run frontend tests:

This will detect any file changes inside [src](src) and re-run the affected tests automatically.
```sh
$ make packages-test
```

### The frontend currently is composed by the following technologies:

| Tech        | Desc.                           |
| :---------- | :------------------------------ |
| ReactJS     | As web component framework      |
| Typescript  | Adds types on top of javascript |
| React Redux | As State Container              |
| Material UI | As UI framework                 |
| Axios       | As HTTP client                  |
| Jest        | As testing framework            |

#
### Front-end base structure

```bash
packages/frontend/public
├── static
│   ├── static
│   │   ├── python  # Python Scripts
│   │   ├── js      # Javascript Scripts (Brython, js_of_ocaml, ...)
│   │   └── img
│   └── ...
└── utils

packages/frontend/src
├── constants # Constant values should be stored here
├── features  # Features should be placed here (e.g. Pages[Home, Editor])
│   └── <feature>
│       ├── elements    # Icons, Buttons, ...
│       ├── containers  # Views Logic
│       ├── views       # Feature structure (A feature can have multiple views)
│       ├── components  # Composed elements (e.g. Menus)
│       ├── utils       # Util functions
│       └── ...
├── polyfills   # Polyfills should be placed here.
├── services    # Services (logger, toaster, ...)
├── store       # Redux Logic
├── __tests__   # Tests should be placed here
├── typings     # Type definitions for untyped modules should be placed here
└── utils       # Util functions should be placed here or in features/<feature>/utils
```


To get started with the command line version of SmartPy you can type

```
./with_env smartpy-cli/SmartPy.sh --help
```

## Testing, development and contributions

Full tests can be launched by typing

```
./with_env make test
```

A slightly less thorough but much quicker target is

```
./with_env make test-quick
```

When iterating on a template, we can launch an even quicker

```
./with_env make test-quick-incremental
```

Updating switch-state at `env/`

```sh
# Install the dependencies you need
opam install ...

# Update switch-state
cp _opam/.opam-switch/switch-state env/switch.export
```

`Update Typescript baselines`

```console
# Build SmartTS CLI
make update-ts-cli

# Updates jest snapshots at "packages/ts-syntax"
make update-ts-snapshots

# Updates SmartTS templates at "test_baselines"
make test-scenario-ts
```

`Versioning:`

```sh
# Patch
make patch-version
# Minor
make minor-version
# Major
make major-version
# With latest commit (short)
make with-rev-version

# Or a specific version
make custom-version VERSION=x.y.z-dev
```

Merge requests are welcome! Please launch tests before submitting.

## Update and deploy documentation

```sh
make send_docs
```

## License

Please check our [license](LICENSE). For https://gitlab.com/SmartPy/SmartPy, we use MIT.

## Tools

We acknowledge and appreciate using tools from third parties that all come with their respective licenses

### Some tools in [ext/](ext)

ace-builds
brython
eztz
flextesa
jquery
jquery-ui-themes-1.12.1
pako

### Other tools

We also integrate directly: [conseilJS](https://github.com/Cryptonomic/ConseilJS), [pace](https://github.hubspot.com/pace/docs/welcome/), [@taquito/michel-codec](https://github.com/ecadlabs/taquito/tree/master/packages/taquito-michel-codec).
