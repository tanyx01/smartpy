import sys

import subprocess
process = subprocess.Popen(['smartpy-cli/SmartPy.sh --version'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
out, err = process.communicate()
version = "v" + out.decode("utf8").split()[-1]

template = """
## VERSION
|   |   |
|---|---|
| Date | CURRENT_DATE |
| Commit  | CURRENT_COMMIT |
| Link  | [VERSION](RELEASE_ID) |

### Change Log

#### Breaking changes

#### Bug fixes
"""

filename = "packages/doc/docs/releases.md"

releases = open(filename, 'r').read()

prefix = releases.index("\n\n")

prefix, releases = releases[0:prefix], releases[prefix+2:]

open(filename, 'w').write("\n".join([prefix, template.replace("VERSION", version), releases]))
