#!/usr/bin/env node
import { execSync } from 'child_process';
import * as hyperquest from 'hyperquest';
import * as chalk from 'chalk';
import * as Commander from 'commander';
import * as path from 'path';
import * as semver from 'semver';
import * as tar from 'tar';
import * as https from 'https';
import * as tmp from 'tmp';
import * as fs from 'fs-extra';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageJson = require('../package.json');

const BASE_URL = 'https://smartpy.io/boilerplate';

enum SYNTAX {
    TYPESCRIPT = 'TYPESCRIPT',
    PYTHON = 'PYTHON',
}
const PACKAGES: Record<SYNTAX, string> = {
    [SYNTAX.TYPESCRIPT]: 'smartpy-boilerplate-typescript.tar.gz',
    [SYNTAX.PYTHON]: 'smartpy-boilerplate-python.tar.gz',
};

async function createProject(name: string, syntax: SYNTAX) {
    const root = path.resolve(name);
    const appName = path.basename(root);
    const folderExists = fs.existsSync(root);

    if (folderExists) {
        console.log(`Target folder ${chalk.green(root)} already exists. Delete it or provide a different name.`);
        process.exit(1);
    }

    // Download package
    const tmpdir = await unpackPackage(`${BASE_URL}/${PACKAGES[syntax]}`);

    if (syntax === SYNTAX.TYPESCRIPT) {
        const pkgPath = `${tmpdir}/package.json`;
        const pkg = await import(pkgPath);

        console.log();
        console.log(
            `Bootstrapping a new SmartPy project from version ${chalk.green(pkg.version)} of typescript boilerplate.`,
        );
        console.log();

        pkg.name = appName;
        fs.writeJSONSync(pkgPath, pkg, { spaces: '\t' });

        fs.moveSync(tmpdir, root);
    }

    // Move to root project folder
    process.chdir(path.resolve(root));

    // Install dependencies
    execSync('npm i');

    console.log();
    console.log(`Project ${chalk.green(appName)} was installed with success!`);
    console.log();
}

async function updateProject(name: string, syntax: SYNTAX) {
    const root = path.resolve(name);
    const appName = path.basename(root);
    const folderExists = fs.existsSync(root);

    if (!folderExists) {
        console.log(`Target folder ${chalk.green(root)} does not exists. Cannot update something that does not exist.`);
        process.exit(1);
    }

    // Download package
    const tmpdir = await unpackPackage(`${BASE_URL}/${PACKAGES[syntax]}`);

    if (syntax === SYNTAX.TYPESCRIPT) {
        const pkgPath = `${tmpdir}/package.json`;
        const pkg = await import(pkgPath);

        console.log();
        console.log(`Updating project...`);
        console.log();

        pkg.name = appName;
        fs.writeJSONSync(pkgPath, pkg, { spaces: '\t' });

        fs.moveSync(`${tmpdir}/package.json`, `${root}/package.json`, { overwrite: true });
        fs.moveSync(`${tmpdir}/tsconfig.json`, `${root}/tsconfig.json`, { overwrite: true });
        fs.moveSync(`${tmpdir}/templates`, `${root}/templates`, { overwrite: true });
        fs.moveSync(`${tmpdir}/scripts`, `${root}/scripts`, { overwrite: true });
    }

    // Move to root project folder
    process.chdir(path.resolve(root));

    // Install dependencies
    execSync('npm i');

    console.log();
    console.log(`Project ${chalk.green(appName)} was installed with success!`);
    console.log();
}

function getTemporaryDirectory(): Promise<string> {
    return new Promise((resolve, reject) => {
        tmp.dir({ unsafeCleanup: true }, (err, tmpdir) => {
            if (err) {
                reject(err);
            } else {
                resolve(tmpdir);
            }
        });
    });
}

function extractStream(stream: fs.ReadStream, dest: string): Promise<Error | undefined> {
    return new Promise((resolve, reject) => {
        stream.pipe(
            tar
                .extract({
                    C: dest,
                })
                .on('finish', resolve)
                .on('error', (err) => reject(err)),
        );
    });
}

function unpackPackage(installPackage: string): Promise<string> {
    if (installPackage.match(/^.+\.(tgz|tar\.gz)$/)) {
        return getTemporaryDirectory().then(async (tmpDir) => {
            const stream: fs.ReadStream = /^http/.test(installPackage)
                ? hyperquest(installPackage)
                : fs.createReadStream(installPackage);

            await extractStream(stream, tmpDir);
            return tmpDir;
        });
    }
    throw new Error(`Could not resolve ${installPackage}.`);
}

function checkForLatestVersion(): Promise<string> {
    return new Promise((resolve, reject) => {
        https
            .get('https://registry.npmjs.org/-/package/create-smartpy-project/dist-tags', (res) => {
                if (res.statusCode === 200) {
                    let body = '';
                    res.on('data', (data) => (body += data));
                    res.on('end', () => {
                        resolve(JSON.parse(body).latest);
                    });
                } else {
                    reject();
                }
            })
            .on('error', () => {
                reject();
            });
    });
}

const currentNodeVersion = process.versions.node;
const version = currentNodeVersion.split('.');
const major = Number(version[0]);

if (major < 10) {
    console.error(
        'You are running Node ' +
            currentNodeVersion +
            '.\n' +
            'create-smartpy-project requires Node 10 or higher. \n' +
            'Please update your version of Node.',
    );
    process.exit(1);
}

Commander.command('install')
    .arguments('<project-directory>')
    .usage(`${chalk.green('<project-directory>')} [options]`)
    .option('--typescript', 'bootstrap a typescript project')
    .action(async (projectName: string, args) => {
        if (!projectName) {
            console.error('Please specify the project directory:');
            console.log(`  ${chalk.cyan(Commander.name())} ${chalk.green('<project-directory>')}`);
            console.log();
            console.log('For example:');
            console.log(`  ${chalk.cyan(Commander.name())} ${chalk.green('smartpy-project')}`);
            console.log();
            console.log(`Run ${chalk.cyan(`${Commander.name()} --help`)} to see all options.`);
            process.exit(1);
        }

        await checkForLatestVersion()
            .catch(() => {
                try {
                    return execSync(`npm view ${packageJson.name} version`).toString().trim();
                } catch (e) {
                    return null;
                }
            })
            .then(async (latest: string | null) => {
                if (latest && semver.lt(packageJson.version, latest)) {
                    console.log();
                    console.error(
                        chalk.yellow(
                            `You are running \`${packageJson.name}\` ${packageJson.version}, which is behind the latest release (${latest}).`,
                        ),
                    );
                    console.log();
                    console.log(
                        'Please remove any global installs with one of the following commands:\n' +
                            `- npm uninstall -g ${packageJson.name}\n` +
                            `- yarn global remove ${packageJson.name}`,
                    );
                    console.log();
                    process.exit(1);
                } else {
                    try {
                        if (args.typescript) {
                            await createProject(projectName, SYNTAX.TYPESCRIPT);
                        } else {
                            Commander.help();
                        }
                    } catch (e) {
                        console.error(e);
                    }
                }
            });
    })
    .on('--help', () => {
        console.log();
        console.log(`Only ${chalk.green('<project-directory>')} is required.`);
        console.log();
    });

Commander.command('update')
    .option('--typescript', 'Update typescript boilerplate')
    .requiredOption('--path <path>')
    .action(async ({ path, typescript }) => {
        await checkForLatestVersion()
            .catch(() => {
                try {
                    return execSync(`npm view ${packageJson.name} version`).toString().trim();
                } catch (e) {
                    return null;
                }
            })
            .then(async (latest: string | null) => {
                if (latest && semver.lt(packageJson.version, latest)) {
                    console.log();
                    console.error(
                        chalk.yellow(
                            `You are running \`${packageJson.name}\` ${packageJson.version}, which is behind the latest release (${latest}).`,
                        ),
                    );
                    console.log();
                    console.log(
                        'Please remove any global installs with one of the following commands:\n' +
                            `- npm uninstall -g ${packageJson.name}\n` +
                            `- yarn global remove ${packageJson.name}`,
                    );
                    console.log();
                    process.exit(1);
                } else {
                    try {
                        if (typescript) {
                            await updateProject(path, SYNTAX.TYPESCRIPT);
                        } else {
                            Commander.help();
                        }
                    } catch (e) {
                        console.error(e);
                    }
                }
            });
    });

Commander.name(packageJson.name);
Commander.version(packageJson.version);
Commander.parse(process.argv);
