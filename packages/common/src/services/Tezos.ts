import { InMemorySigner } from '@taquito/signer';
import { TezosToolkit } from '@taquito/taquito';

const getWallet = async (tezosNode: string, secretKey: string): Promise<TezosToolkit> => {
    const signer = await InMemorySigner.fromSecretKey(secretKey);
    const Tezos = new TezosToolkit(tezosNode);
    Tezos.setSignerProvider(signer);

    return Tezos;
};

export default {
    getWallet,
};
