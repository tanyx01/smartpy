import { ContractAbstraction, TezosToolkit, Wallet } from '@taquito/taquito';

export async function originateContract(
    tezos: TezosToolkit,
    code: string | object[],
    initialStorage:  string | object,
    balance?:  string,
): Promise<ContractAbstraction<Wallet>> {
    const op = await tezos.wallet.originate({
        code: code,
        init: initialStorage,
        balance: balance,
    }).send();

    op.confirmation(1);

    return await op.contract();
}

export default {
    originateContract,
};
