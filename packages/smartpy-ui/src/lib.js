import SpValue from '../src/components/SpValue.vue'
import CodeMirror from '../src/components/CodeMirror/CodeMirror.ce.vue'
import { defineCustomElement } from 'vue'

export function defineSmartPyComponents() {
    customElements.define('sp-value', defineCustomElement(SpValue))
    customElements.define("sp-codemirror", defineCustomElement(CodeMirror));
}

