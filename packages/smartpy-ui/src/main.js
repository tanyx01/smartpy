import Dialog from 'primevue/dialog/sfc';
import Slider from 'primevue/slider';
import Button from 'primevue/button';
import InputText from 'primevue/inputtext';
import { createApp } from 'vue'
import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import './style.css'
import App from './App.vue'
import PrimeVue from 'primevue/config';

const app = createApp(App)
app.use(PrimeVue)
app.component('Dialog', Dialog);
app.component('InputText', InputText);
app.component('Button', Button);
app.component('Slider', Slider);

app.mount('#app')
