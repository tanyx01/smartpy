export const fa2LibExample = `import smartpy as sp
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/fa2_lib.py")

class SimpleNft(FA2.Fa2Nft):
    pass

@sp.add_test(name="My NFT")
def test():
    sc = sp.test_scenario()
    nft = SimpleNft(
        metadata = sp.utils.metadata_of_url("https://example.com"),
    )
    sc += nft
`;

export const pythonSyntax = `#Long line: Beautiful is better than ugly. Explicit is better than implicit. Simple is better than complex. Complex is better than complicated. Flat is better than nested. Sparse is better than dense. Readability counts. Special cases aren't special enough to break the rules. Although practicality beats purity. Errors should never pass silently. Unless explicitly silenced. In the face of ambiguity, refuse the temptation to guess. There should be one-- and preferably only one --obvious way to do it. Although that way may not be obvious at first unless you're Dutch. Now is better than never. Although never is often better than *right* now. If the implementation is hard to explain, it's a bad idea. If the implementation is easy to explain, it may be a good idea. Namespaces are one honking great idea -- let's do more of those!
# Literals
1234
0.0e101
.123
0b01010011100
0o01234567
0x0987654321abcdef
# Error Literals
.0b000
0.0e
0e

# String Literals
'For\''
"God\""
"""so loved
the world"""
'''that he gave
his only begotten\' '''
'that whosoever believeth \
in him'
''

# Identifiers
__a__
a.b
a.b.c
# Error Identifiers
a.

# Operators
+ - * / % & | ^ ~ < >
== != <= >= <> << >> // **
and or not in is

# Delimiters
() [] {} , : \` = ; @ . # At-signs and periods require context
+= -= *= /= %= &= |= ^=
//= >>= <<= **=

# Keywords
as assert break class continue def del elif else except
finally for from global if import lambda pass raise
return try while with yield

# Python 2 Keywords (otherwise Identifiers)
exec print

# Python 3 Keywords (otherwise Identifiers)
nonlocal

# Types
bool classmethod complex dict enumerate float frozenset int list object
property reversed set slice staticmethod str super tuple type

# Python 2 Types (otherwise Identifiers)
basestring buffer file long unicode xrange

# Python 3 Types (otherwise Identifiers)
bytearray bytes filter map memoryview open range zip

# Example Strict Errors
def doesNothing():
   pass # indentUnit is set to 4 but this line is indented 3

# Some Example code
import os
from package import ParentClass

@nonsenseDecorator
def doesNothing():
    pass

class ExampleClass(ParentClass):
    @staticmethod
    def example(inputStr):
        a = list(inputStr)
        a.reverse()
        return ''.join(a)

    def __init__(self, mixin = 'Hello'):
        self.mixin = mixin
`;