import CodeMirror from "/src/components/CodeMirror/CodeMirror.ce.vue";
import { defineCustomElement } from "vue";
import { fa2LibExample, pythonSyntax } from "./templateCodes";

if (!window.customElements.get(`sp-codemirror`)) {
  window.customElements.define(
    `sp-codemirror`,
    defineCustomElement(CodeMirror)
  );
}

export default {
  title: "IDE/CodeMirror",
  component: CodeMirror,
  parameters: {
    backgrounds: {
      default: "light",
      values: [
        { name: "light", value: "#ffffff" },
        { name: "dark", value: "#121212" },
      ],
    },
  },
  argTypes: {
    font_size: { control: "range" },
  },
};

const Template = (args) => ({
  components: { CodeMirror },
  setup() {
    return { args };
  },
  template: '<sp-codemirror v-bind="args" />',
});

const default_config = {
  disabled: "false",
  indent_with_tab: "true",
  tab_size: 4,
  autofocus: "true",
  language: "python",
  theme: "default",
  font_size: 14,
};

export const Empty = Template.bind({});
Empty.args = {
  ...default_config,
  code: "",
};

export const WhiteTheme = Template.bind({});
WhiteTheme.args = {
  ...default_config,
  code: fa2LibExample,
};

export const DarkTheme = Template.bind({});
DarkTheme.args = {
  ...default_config,
  code: fa2LibExample,
  theme: "oneDark",
};
DarkTheme.parameters = {
  backgrounds: { default: "dark" },
};

export const BigFont = Template.bind({});
BigFont.args = {
  ...default_config,
  code: fa2LibExample,
  font_size: 18,
};

const FlexElementTemplate = (args) => ({
  components: { CodeMirror },
  setup() {
    return { args };
  },
  template: `
    <div style="display: flex; height: 100vh; flex-direction: column;">
      <div
        style="
          display: flex;
          flex-grow: 1;
          overflow-y: auto;
        ">
        <sp-codemirror v-bind="args" style="flex-grow: 1;" />
      </div>
    </div>`,
});

export const AsFlexElement = FlexElementTemplate.bind({});
AsFlexElement.args = {
  ...default_config,
  code: "",
};

export const AsFlexElementWithCode = FlexElementTemplate.bind({});
AsFlexElementWithCode.args = {
  ...default_config,
  code: pythonSyntax,
};
