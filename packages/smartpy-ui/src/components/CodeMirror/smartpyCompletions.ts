import { CompletionContext, CompletionResult } from "@codemirror/autocomplete"
import completers from "./completers"

const decorators = completers
    .filter(({ detail }) => { return detail.includes("[SmartPy decorator]") })
    .map(({ label, detail, type }) => { return { label: `@sp.${label}`, type, detail }; });

const options = completers
    .filter(({ detail }) => { return !detail.includes("[SmartPy decorator]") })
    .map(({ label, detail, type }) => { return { label: `${label}`, type, detail, "info": "" }; })

export function smartPyCompletions(context: CompletionContext) {
    let decorator = context.tokenBefore(["Decorator"]);
    if (decorator) {
        return {
            from: decorator.from,
            options: decorators,
        };
    }
    let word = context.matchBefore(/\w*?/);
    if (word) {
        if (word.from == word.to && !context.explicit) {
            return null;
        }
        return {
            from: word.from,
            options: options,
        } as CompletionResult
    }
    return null;
}