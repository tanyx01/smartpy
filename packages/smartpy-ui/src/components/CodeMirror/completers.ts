const completers = [
    {
        "label": "TAddress",
        "caption": "sp.TAddress",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TBigMap",
        "caption": "sp.TBigMap",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TBls12_381_fr",
        "caption": "sp.TBls12_381_fr",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TBls12_381_g1",
        "caption": "sp.TBls12_381_g1",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TBls12_381_g2",
        "caption": "sp.TBls12_381_g2",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TBool",
        "caption": "sp.TBool",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TBytes",
        "caption": "sp.TBytes",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TChainId",
        "caption": "sp.TChainId",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TContract",
        "caption": "sp.TContract",
        "detail": "(t) [SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TInt",
        "caption": "sp.TInt",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TIntOrNat",
        "caption": "sp.TIntOrNat",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TKey",
        "caption": "sp.TKey",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TKeyHash",
        "caption": "sp.TKeyHash",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TList",
        "caption": "sp.TList",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TMap",
        "caption": "sp.TMap",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TMutez",
        "caption": "sp.TMutez",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TNat",
        "caption": "sp.TNat",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TOption",
        "caption": "sp.TOption",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TPair",
        "caption": "sp.TPair",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TRecord",
        "caption": "sp.TRecord",
        "detail": "(**fields) [SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TSaplingState",
        "caption": "sp.TSaplingState",
        "detail": "(memo_size = None) [SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TSaplingTransaction",
        "caption": "sp.TSaplingTransaction",
        "detail": "(memo_size = None) [SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TSecretKey",
        "caption": "sp.TSecretKey",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TSet",
        "caption": "sp.TSet",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TSignature",
        "caption": "sp.TSignature",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TString",
        "caption": "sp.TString",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TTimestamp",
        "caption": "sp.TTimestamp",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TUnit",
        "caption": "sp.TUnit",
        "detail": "[SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "TVariant",
        "caption": "sp.TVariant",
        "detail": "(**fields) [SmartPy type]",
        "score": 1000,
        "type": "type"
    },
    {
        "label": "address",
        "caption": "sp.address",
        "detail": "(\"tz... or KT...\") [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "big_map",
        "caption": "sp.big_map",
        "detail": "(l = ..., tkey = ..., tvalue = ...) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "bool",
        "caption": "sp.bool",
        "detail": "(b) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "bytes",
        "caption": "sp.bytes",
        "detail": "(\"0x...\") [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "chain_id_cst",
        "caption": "sp.chain_id_cst",
        "detail": "(\"0x9caecab9\") [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "int",
        "caption": "sp.int",
        "detail": "(i) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "key",
        "caption": "sp.key",
        "detail": "(\"tz...\") [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "key_hash",
        "caption": "sp.key_hash",
        "detail": "(\"tz...\") [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "list",
        "caption": "sp.list",
        "detail": "(l = ..., t = ...) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "map",
        "caption": "sp.map",
        "detail": "(l = ..., tkey = ..., tvalue = ...) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "nat",
        "caption": "sp.nat",
        "detail": "(n) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "record",
        "caption": "sp.record",
        "detail": "(field1 = value1, field2 = value2, .., ) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "sapling_test_transaction",
        "caption": "sp.sapling_test_transaction",
        "detail": "(source, target, amount, memo_size) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "set",
        "caption": "sp.set",
        "detail": "(l = ..., t = ...) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "string",
        "caption": "sp.string",
        "detail": "(s) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "tez",
        "caption": "sp.tez",
        "detail": "(...) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "timestamp",
        "caption": "sp.timestamp",
        "detail": "(...) [SmartPy literal]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "amount",
        "caption": "sp.amount",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "balance",
        "caption": "sp.balance",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "chain_id",
        "caption": "sp.chain_id",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "level",
        "caption": "sp.level",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "none",
        "caption": "sp.none",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "now",
        "caption": "sp.now",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "self",
        "caption": "sp.self",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "self_address",
        "caption": "sp.self_address",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "sender",
        "caption": "sp.sender",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "source",
        "caption": "sp.source",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "total_voting_power",
        "caption": "sp.total_voting_power",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "unit",
        "caption": "sp.unit",
        "detail": "[SmartPy constant]",
        "score": 1000,
        "type": "constant"
    },
    {
        "label": "add_compilation_target",
        "caption": "sp.add_compilation_target",
        "detail": "(name, contract, storage=None) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "add_expression_compilation_target",
        "caption": "sp.add_expression_compilation_target",
        "detail": "(name, expression) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "add_simulation_target",
        "caption": "sp.add_simulation_target",
        "detail": "(contract, name=\"Simulation\", shortname=None, profile=False, is_default=True) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "add_test",
        "caption": "sp.add_test",
        "detail": "(name, shortname=None, profile=False, is_default=True) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "as_nat",
        "caption": "sp.as_nat",
        "detail": "(i, message = None) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "big_map",
        "caption": "sp.big_map",
        "detail": "(l = ..., tkey = ..., tvalue = ...) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "blake2b",
        "caption": "sp.blake2b",
        "detail": "(value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "build_lambda",
        "caption": "sp.build_lambda",
        "detail": "(f) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "check_signature",
        "caption": "sp.check_signature",
        "detail": "(k, s, b) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "compute",
        "caption": "sp.compute",
        "detail": "(expression) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "concat",
        "caption": "sp.concat",
        "detail": "(l) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "contract",
        "caption": "sp.contract",
        "detail": "(t, address, entry_point = \"\") [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "create_contract",
        "caption": "sp.create_contract",
        "detail": "(contract, storage = None, amount = tez(0), baker = None) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "ediv",
        "caption": "sp.ediv",
        "detail": "(num, den) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "else",
        "caption": "sp.else",
        "detail": ": [SmartPy control]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "entry_point",
        "caption": "sp.entry_point",
        "detail": "[SmartPy decorator]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "failwith",
        "caption": "sp.failwith",
        "detail": "(message) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "for",
        "caption": "sp.for",
        "detail": " ... in ...: [SmartPy control]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "fst",
        "caption": "sp.fst",
        "detail": "(..) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "get_and_update",
        "caption": "sp.get_and_update",
        "detail": "(my_big_map, key, value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "global_lambda",
        "caption": "sp.global_lambda",
        "detail": "[SmartPy decorator]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "has_entry_point",
        "caption": "sp.has_entry_point",
        "detail": "(name) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "hash_key",
        "caption": "sp.hash_key",
        "detail": "(key) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "if",
        "caption": "sp.if",
        "detail": " ...: [SmartPy control]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "implicit_account",
        "caption": "sp.implicit_account",
        "detail": "(key_hash) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "io.import_script_from_script",
        "caption": "sp.io.import_script_from_script",
        "detail": "(name, script) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "io.import_script_from_url",
        "caption": "sp.io.import_script_from_url",
        "detail": "(url, name = None) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "io.import_stored_contract",
        "caption": "sp.io.import_stored_contract",
        "detail": "(name) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "io.import_template",
        "caption": "sp.io.import_template",
        "detail": "(template_name) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "is_nat",
        "caption": "sp.is_nat",
        "detail": "(i) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "join_tickets",
        "caption": "sp.join_tickets",
        "detail": "(ticket1, ticket2) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "join_tickets_raw",
        "caption": "sp.join_tickets_raw",
        "detail": "(tickets) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "keccak",
        "caption": "sp.keccak",
        "detail": "(value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "lambda_michelson",
        "caption": "sp.lambda_michelson",
        "detail": "(code, source = None, target = None) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "left",
        "caption": "sp.left",
        "detail": "(value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "len",
        "caption": "sp.len",
        "detail": "(e) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "list",
        "caption": "sp.list",
        "detail": "(l = ..., t = ...) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "make_signature",
        "caption": "sp.make_signature",
        "detail": "(secret_key, message, message_format = \"Raw\") [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "map",
        "caption": "sp.map",
        "detail": "(l = ..., tkey = ..., tvalue = ...) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "match_cons",
        "caption": "sp.match_cons",
        "detail": "(l) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "match_pair",
        "caption": "sp.match_pair",
        "detail": "(x) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "match_record",
        "caption": "sp.match_record",
        "detail": "(x, *fields) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "max",
        "caption": "sp.max",
        "detail": "(x, y) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "michelson",
        "caption": "sp.michelson",
        "detail": "(code, types_in, types_out) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "min",
        "caption": "sp.min",
        "detail": "(x, y) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "modify_record",
        "caption": "sp.modify_record",
        "detail": "(x, name = None) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "mul",
        "caption": "sp.mul",
        "detail": "(n, m) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "never",
        "caption": "sp.never",
        "detail": "(expression) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "offchain_view",
        "caption": "sp.offchain_view",
        "detail": "(pure = False, doc = None) [SmartPy decorator]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "pack",
        "caption": "sp.pack",
        "detail": "(x) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "pair",
        "caption": "sp.pair",
        "detail": "(e1, e2) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "pairing_check",
        "caption": "sp.pairing_check",
        "detail": "(pairs) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "private_lambda",
        "caption": "sp.private_lambda",
        "detail": "(with_operations=False, with_storage=False, wrap_call=False) [SmartPy decorator]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "range",
        "caption": "sp.range",
        "detail": "(x, y, step = 1) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "read_ticket",
        "caption": "sp.read_ticket",
        "detail": "(ticket) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "read_ticket_raw",
        "caption": "sp.read_ticket_raw",
        "detail": "(ticket) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "resolve",
        "caption": "sp.resolve",
        "detail": "(e) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "right",
        "caption": "sp.right",
        "detail": "(value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "sapling_empty_state",
        "caption": "sp.sapling_empty_state",
        "detail": "(memo_size) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "sapling_verify_update",
        "caption": "sp.sapling_verify_update",
        "detail": "(transaction) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "self_entry_point",
        "caption": "sp.self_entry_point",
        "detail": "(entry_point = \"\") [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "self_entry_point_address",
        "caption": "sp.self_entry_point_address",
        "detail": "(entry_point = \"\") [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "send",
        "caption": "sp.send",
        "detail": "(destination, amount, message = None) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "set",
        "caption": "sp.set",
        "detail": "(l = ..., t = ...) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "set_delegate",
        "caption": "sp.set_delegate",
        "detail": "(baker) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "set_entry_point",
        "caption": "sp.set_entry_point",
        "detail": "(name, entry_point) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "set_result_type",
        "caption": "sp.set_result_type",
        "detail": "(t) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "set_type",
        "caption": "sp.set_type",
        "detail": "(expression, t) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "set_type_expr",
        "caption": "sp.set_type_expr",
        "detail": "(expression, t) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "sha256",
        "caption": "sp.sha256",
        "detail": "(value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "sha3",
        "caption": "sp.sha3",
        "detail": "(value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "sha512",
        "caption": "sp.sha512",
        "detail": "(value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "slice",
        "caption": "sp.slice",
        "detail": "(expression, offset, length) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "snd",
        "caption": "sp.snd",
        "detail": "(..) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "some",
        "caption": "sp.some",
        "detail": "(e) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "split_ticket",
        "caption": "sp.split_ticket",
        "detail": "(ticket, q1, q2) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "split_ticket_raw",
        "caption": "sp.split_ticket_raw",
        "detail": "(ticket, q1, q2) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "split_tokens",
        "caption": "sp.split_tokens",
        "detail": "(amount, quantity, totalQuantity) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "sub_entry_point",
        "caption": "sp.sub_entry_point",
        "detail": "[SmartPy decorator]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "test_account",
        "caption": "sp.test_account",
        "detail": "(seed) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "test_scenario",
        "caption": "sp.test_scenario",
        "detail": "() [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "ticket",
        "caption": "sp.ticket",
        "detail": "(content, amount) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "timestamp_from_utc",
        "caption": "sp.timestamp_from_utc",
        "detail": "(year, month, day, hours, minutes, seconds) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "timestamp_from_utc_now",
        "caption": "sp.timestamp_from_utc_now",
        "detail": "() [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "to_address",
        "caption": "sp.to_address",
        "detail": "(contract) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "to_int",
        "caption": "sp.to_int",
        "detail": "(n) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "trace",
        "caption": "sp.trace",
        "detail": "(expression) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "transfer",
        "caption": "sp.transfer",
        "detail": "(arg, amount, destination) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "unpack",
        "caption": "sp.unpack",
        "detail": "(x, t = ...) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "update_map",
        "caption": "sp.update_map",
        "detail": "(my_big_map, key, value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "utils.bytes_of_string",
        "caption": "sp.utils.bytes_of_string",
        "detail": "(s) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "utils.metadata_of_url",
        "caption": "sp.utils.metadata_of_url",
        "detail": "(url) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "utils.mutez_to_nat",
        "caption": "sp.utils.mutez_to_nat",
        "detail": "(...) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "utils.nat_to_mutez",
        "caption": "sp.utils.nat_to_mutez",
        "detail": "(n)` and `sp.utils.nat_to_tez(n) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "utils.same_underlying_address",
        "caption": "sp.utils.same_underlying_address",
        "detail": "(addr1, addr2) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "utils.view",
        "caption": "sp.utils.view",
        "detail": "(t, message = None) [SmartPy decorator]",
        "score": 1000,
        "type": "keyword"
    },
    {
        "label": "utils.wrap_entry_point",
        "caption": "sp.utils.wrap_entry_point",
        "detail": "(name, entry_point) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "variant",
        "caption": "sp.variant",
        "detail": "(\"constructor\", value) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "verify",
        "caption": "sp.verify",
        "detail": "(condition, message = ...) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "verify_equal",
        "caption": "sp.verify_equal",
        "detail": "(v1, v2, message = ...) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "voting_power",
        "caption": "sp.voting_power",
        "detail": "(key_hash) [SmartPy function]",
        "score": 1000,
        "type": "function"
    },
    {
        "label": "while",
        "caption": "sp.while",
        "detail": " ...: [SmartPy control]",
        "score": 1000,
        "type": "keyword"
    }
];

export default completers;
