import React from 'react';
import SyntaxProvider from '../components/Syntax/SyntaxProvider';

function Root({ children }) {
    return (
        <SyntaxProvider>
            {children}
        </SyntaxProvider>
    );
}

export default Root;
