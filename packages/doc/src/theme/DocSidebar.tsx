import React from 'react';

import OriginalDocSidebar from '@theme-original/DocSidebar';
import SyntaxSelector from '../components/Syntax/Selector';
import './docSidebar.modules.css';

export default function DocSidebar(props) {
    return (
        <>
            <SyntaxSelector/>
            <OriginalDocSidebar {...props} />
        </>
    );
}