import React from 'react';

const CompilationOnly: React.FC = () => {
    return (
        <span
            style={{
                display: 'inline-flex',
                flexDirection: 'row',
                alignItems: 'center',
            }}
            className="badge badge--danger"
        >
            Only available at compilation
        </span>
    );
};

export default CompilationOnly;
