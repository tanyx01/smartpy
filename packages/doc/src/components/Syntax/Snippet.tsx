import React from 'react';
import useSyntaxContext, { SYNTAX } from '../hooks/useSyntaxContext';

interface OwnProps {
    syntax: SYNTAX;
}

const Snippet: React.FC<OwnProps> = ({ syntax: _syntax, children }) => {
    const { syntax } = useSyntaxContext();

    if (syntax === _syntax && children) {
        return children as any;
    }

    return null;
};

export { SYNTAX };

export default Snippet;
