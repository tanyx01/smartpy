import React, { useCallback, useEffect, useState } from 'react';
import { createStorageSlot } from '@docusaurus/theme-common';

import SyntaxContext from './SyntaxContext';

const SYNTAX_STORAGE_KEY = 'SYNTAX';
const storage = createStorageSlot(SYNTAX_STORAGE_KEY);

export enum SYNTAX {
    PY = 'Python',
    TS = 'Typescript',
    ML = 'Ocaml',
}

function SyntaxProvider(props): JSX.Element {
    const [syntax, setSyntax] = useState<SYNTAX>(SYNTAX.PY);

    const setSyntaxSyncWithLocalStorage = useCallback((syntax) => {
        storage.set(syntax);
    }, []);

    useEffect(() => {
        try {
            if (storage.get()) {
                setSyntax(createStorageSlot(SYNTAX_STORAGE_KEY).get() as SYNTAX);
            }
        } catch (err) {
            console.error(err);
        }
    }, []);

    return (
        <SyntaxContext.Provider
            value={{
                syntax,
                setSyntax: (syntax: SYNTAX) => {
                    setSyntaxSyncWithLocalStorage(syntax);
                    setSyntax(syntax);
                },
            }}
        >
            {props.children}
        </SyntaxContext.Provider>
    );
}

export default SyntaxProvider;
