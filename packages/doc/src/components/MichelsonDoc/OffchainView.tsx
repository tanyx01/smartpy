import React from 'react';

function OffchainView(props: any) {
    return (
        <section className="michelson">
            <section className="entrypoint">
                <h3>
                    OffchainView <span className="entrypointName">{props.name}</span>
                </h3>
                {props.children}
            </section>
        </section>
    );
}

export default OffchainView;
