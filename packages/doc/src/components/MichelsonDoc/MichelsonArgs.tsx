import React from 'react';
import replaceJSX from './utils/replaceJSX';
import variablename_replacements from './variableNameReplacements';

function MichelsonArgs(props: any) {
    const type = replaceJSX(props.type || 'unit', variablename_replacements);
    let name;
    if (!props.name) {
        name = <span className="entrypointVariableName">Parameter</span>;
    } else {
        name = <span className="variableName">{props.name}</span>;
    }
    return (
        <div className="michelsonArgsSection">
            <h4>
                {name}
                <span className="michelsonType">{type}</span>
            </h4>
            <div className="michelsonArgs">{props.children}</div>
        </div>
    );
}

export default MichelsonArgs;
