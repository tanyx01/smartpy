import React from 'react';

interface OwnProps {
    placeholder: string;
    url: string;
}

const PDocIcon: React.FC<OwnProps> = ({ placeholder, url }) => {
    return (
        <span
            style={{
                display: 'inline-flex',
                flexDirection: 'row',
                alignItems: 'center',
            }}
            className="badge badge--secondary"
        >
            <span>
                <a href={url}>{placeholder}</a>
            </span>
            <img
                width={16}
                height={16}
                src={require('@site/static/img/pdoc_logo.png').default}
                style={{ marginLeft: 5 }}
            />
        </span>
    );
};

export default PDocIcon;
