# Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern
static website generator.

## Installation

```bash
cd .. && lerna bootstrap`
```

Install all the package dependencies and links any cross-dependencies.
`npm install` may also work in the package directory but the result can be
differ from the CI expectation.

## Local Development

```bash
npx docusaurus start
```

This command starts a local development server and opens up a browser window.
Most changes are reflected live without having to restart the server.

## Build

```bash
npx docusaurus build
```

This command generates static content into the `build` directory and can be
served using any static contents hosting service.

## Deployment

```bash
cp -r packages/doc/build <destination>
```

Note that releases.md is managed by a SmartPy internal script.
Look for `prepare_distribution.sh`.