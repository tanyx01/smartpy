# Cryptography

import SyntaxSelector from '@site/src/components/Syntax/Selector';
import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

Some constructions are only available in tests, not in smart contracts.

**`sp.test_account(seed)`**

Creating a *deterministic* key-pair from a **seed** string:

```python
# seed is `Alice`
alice = sp.test_account("Alice")
```

It produces an object with the following properties:

| Property | Type  | Description  |
|-|:-:|:-:|
| alice.address | [sp.TAddress](/general/types#address) | Gives the public-key-hash |
| alice.public_key_hash | [sp.TKeyHash](/general/types#key_hash) | Gives the public-key-hash |
| alice.public_key | [sp.TKey](/general/types#key) | Gives the public-key |
| alice.secret_key | [sp.TString](/general/types#string) | Gives the secret-key |

<br/>

**`sp.make_signature(secret_key, message, message_format = 'Raw')`**

Forges a signature compatible with [sp.check_signature(...)](/types/signatures#check-signature).

`message_format` field accepts two values:

- **Hex** in which case the message will be interpreted as an hexadecimal string;
- **Raw** (*the default*) in which case the message is a [sp.TBytes](/general/types#bytes) value (usually the result of an `sp.pack` call).

:::info
`sp.test_account` methods and `sp.make_signature` are not available for compilation to Michelson (a smart contract cannot manipulate secret keys).
:::

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Some constructions are only available in tests, not in smart contracts.

**`Sp.testAccount(seed)`**

Creating a *deterministic* key-pair from a **seed** string:

```typescript
// seed is `Alice`
const alice = Sp.testAccount("Alice")
```

It produces an object with the following properties:

| Property | Type  | Description  |
|-|:-:|-:|
| alice.address | [TAddress](/general/types#address) | Gives the public-key-hash |
| alice.publicKeyHash | [TKeyHash](/general/types#key_hash) | Gives the public-key-hash |
| alice.publicKey | [TKey](/general/types#key) | Gives the public-key |
| alice.secretKey | [TString](/general/types#string) | Gives the secret-key |

<br/>

**`Sp.makeSignature(<secret_key>, <message>)`**

Forges a signature compatible with [Sp.checkSignature(...)](/types/signatures#check-signature).

`message` field is a [TBytes](/general/types#bytes) value (usually the result of an `Sp.pack` call).

:::info
`Sp.testAccount` methods and `Sp.makeSignature` are not available for compilation to Michelson (a smart contract cannot manipulate secret keys).
:::

</Snippet>
