# Testing Contracts

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

## Registering and displaying contracts

<Snippet syntax={SYNTAX.PY}>

```python
@sp.add_test(name = "A Test")
def test():
    # Create a scenario
    scenario = sp.test_scenario()
    # Instantiate a contract
    c1 = MyContract()

    # Add the contract to the scenario
    scenario += c1 # which is equivalent to `scenario.register(c1, show = True)`

    # To only register the smart contract but not show it
    scenario.register(c1)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Dev.test({
    name: "A Test"
}, () => {
    // Originates a contract (will be visible in the output panel)
    const c1 = Scenario.originate(new MyContract());

    // Originates another contract (will not be visible in the output panel)
    const c2 = Scenario.originate(new MyContract(), { show: false });
});
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


## Contract origination options

### Initial storage
Contract storages are typically determined in the [contract](/introduction/contracts) constructors but it's
possible to do it right before registering them inside scenarios.

<Snippet syntax={SYNTAX.PY}>

```python
    c2 = MyContract()
    c2.init_storage(sp.record(a = 12, b = True))
    scenario += c2
```

</Snippet>

### Initial balance

<Snippet syntax={SYNTAX.PY}>

Additionally to entry points, contracts have an additional method that can be called once, before origination.

```python
c1.set_initial_balance(expression)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Dev.test({
    name: "A Test"
}, () => {
    // Originates a contract with initial balance of 1 xtz
    const c2 = Scenario.originate(new MyContract(), { show: false, initialBalance: 10000000 });
});
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>



## Test accounts

Test accounts can be defined by calling `sp.test_account(seed)` where seed is a string.

A test account contains some fields:

- `<account>.address`
- `<account>.public_key_hash`
- `<account>.public_key`
- `<account>.secret_key`

See [Cryptography](./cryptography.md)

<Snippet syntax={SYNTAX.PY}>

```python
admin = sp.test_account("Administrator")
alice = sp.test_account("Alice")
bob   = sp.test_account("Robert")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Calls to entry points

<Snippet syntax={SYNTAX.PY}>

```python
# Call entry_point
c1.my_entrypoint(12)
# Call entry_point with customized block attributes.
c1.my_entrypoint(13).run(
                        sender          = None, # sp.TAddress
                        source          = None, # sp.TAddress
                        chain_id        = None, # sp.TChainId
                        level           = None, # sp.TNat
                        now             = None, # sp.TTimestamp
                        voting_powers   = None, # sp.TMap(TKeyHash, TNat)

                        amount          = None, # sp.TMutez
                        valid           = True, # sp.TBool
                        show            = True, # sp.TBool
                        exception       = None, # any
                    )
```

The **`.run(...)`** method and its parameters are all optional.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// Call entry_point
Scenario.transfer(c1.myEentrypoint(12))
// Call entry_point with customized block attributes.
Scenario.transfer(
    c1.myEentrypoint(12),
    {
        sender          : ..., // TAddress
        source          : ..., // TAddress
        amount          : ..., // TMutez
        now             : ..., // TTimestamp
        level           : ..., // TNat
        chainId         : ..., // TChain_id
        votingPowers    : ..., // Record<TKey_hash, TMutez>
        valid           : ..., // boolean
        show            : ..., // boolean
        exception       : ..., // any
    }
)
```

The second argument and its parameters are all optional.

| Parameter | Type | Description |
|-----------|------|-------------|
| sender | [TAddress](/general/types#address) | The simulated sender of the transaction. It populates `Sp.sender` and it can be either built by a `Sp.testAccount(...)` or an expression of type `TAddress`. |
| source | [TAddress](/general/types#address) | The simulated source of the transaction. It populates `Sp.source` and it can be either built by a `Sp.testAccount(...)` or an expression of type `TAddress`. |
| amount | [TMutez](/general/types#mutez) | The simulated amount sent. It populates `Sp.amount`. |
| now | [TTimestamp](/general/types#timestamp) | The simulated timestamp of the transaction. It populates `sp.now`. |
| level | [TNat](/general/types#nat) | The simulated level of the transaction. It populates `Sp.level`. |
| chainId | [TChain_id](/general/types#chain_id) | The simulated chain_id for the test. It populates `Sp.chainId` |
| votingPowers | Record<[TKey_hash](/general/types#key_hash), [TNat](/general/types#nat)> | The simulated voting powers for the test. It populates `Sp.totalVotingPower` and `Sp.votingPower` |
| valid | `True` or `False` | Tells the interpreter if the transaction is expected to fail or not. **True by default.** |
| show | `True` or `False` | Show or hide the transaction. **True by default.** |
| exception | `any type` | The expected exception raised by the transaction. **If present, valid must be False.** |


</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


### Calls optional arguments

<Snippet syntax={SYNTAX.PY}>

Generic context arguments are:

| Parameter | Type | Accessor | Description |
|-----------|------|-----------|------------|
| sender | [sp.TAddress](/general/types#address) or [sp.test_account](/scenarios/testing/#test-accounts) | `sp.sender` | The simulated sender of the transaction. <br/>Specific to call.|
| source | [sp.TAddress](/general/types#address) or [sp.test_account](/scenarios/testing/#test-accounts) | `sp.source` | The simulated source of the transaction. <br/>Specific to call.|
| chain_id | [sp.TChainId](/general/types#chain_id) | `sp.chain_id` | The simulated chain_id. <br/>Preserved until changed.|
| level | [sp.TNat](/general/types#nat) | `sp.level` | The simulated block level.  <br/>Preserved until changed.|
| now | [sp.TTimestamp](/general/types#timestamp) | `sp.now` | The simulated block timestamp.  <br/>Preserved until changed.|
| voting_powers | [sp.TMap](/general/types#map)([sp.TKeyHash](/general/types#key_hash), [sp.TNat](/general/types#nat)) | `sp.total_voting_power`, <br/> `sp.voting_power` | The simulated voting powers for the test. <br/>Preserved until changed.|

Specific context arguments are:

| Parameter | Type | Description |
|-----------|------|-------------|
| amount | [sp.TMutez](/general/types#mutez) | The simulated amount sent. It populates `sp.amount`. |
| valid | [sp.TBool](/general/types#bool) | Tells the interpreter if the transaction is expected to fail or not. **True by default.** |
| show | [sp.TBool](/general/types#bool) | Show or hide the transaction. **True by default.** |
| exception | any type | The expected exception raised by the transaction. **If present, valid must be False.** |

</Snippet>

## Exceptions

<Snippet syntax={SYNTAX.PY}>

**`sp.is_failing(<expression>)`**

Returns `True` when an expression *(views, lambdas, ...)* results in failure, and `False` when the expression succeeds.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.isFailing(<expression>)`**

Returns `true` when the expression *(views, lambdas, ...)* results in failure, and `false` when the expression succeeds.

</Snippet>

### Content of exceptions

<Snippet syntax={SYNTAX.PY}>

**`sp.catch_exception(<expression>, t = <type of the exception>)`**

`t` is optional, just using `sp.catch_exception(<expression>)` will be valid in most situations.

This method is used to test failing conditions of expressions *(views, lambdas, ...)*. It returns an [sp.TOption](/general/types#option) of type `t` that will contain `sp.some(<exception>)` when the expression fails or `sp.none` if it succeeds.

</Snippet>


<Snippet syntax={SYNTAX.TS}>

**`Sp.catchException<[exception type]>(<expression>)`**

`t` is optional, just using `Sp.catchException(<expression>)` will be valid in most situations.

This method is used to test failing conditions of expressions *(views, lambdas, ...)*. It returns an [TOption](/general/types#option) of type `t` that will contain `Sp.some(<exception>)` when the expression fails or `Sp.none` if it succeeds.

</Snippet>

## Views

Views, both off-chain or on-chain, can now be called from test scenarios the same way as entry points. The example below shows how to do it.

### Example

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self, param):
        self.init(param)

    @sp.offchain_view()
    def state(self, param):
        sp.verify(param < 5, "This is false: param > 5")
        sp.result(self.data * param)

@sp.add_test(name = "Minimal")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract(1)
    scenario += c1

    """ Test views """

    # Display the offchain call result
    scenario.show(c1.state(1))

    # Assert the view result
    scenario.verify(c1.state(2) == 2)
    # Assert call failures
    scenario.verify(sp.is_failing(c1.state(6)));    # Expected to fail
    scenario.verify(~ sp.is_failing(c1.state(1)));   # Not expected to fail

    # Assert exception result
    # catch_exception returns an option:
    #      sp.none if the call succeeds
    #      sp.some(<exception>) if the call fails
    e = sp.catch_exception(c1.state(7), t = sp.TString)
    scenario.verify(e == sp.some("This is false: param > 5"))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
export class OffChainViews {
    constructor(public storage: TInt = /* Default value */ 1) {}

    @OffChainView
    state = (param: TInt): TInt => {
        Sp.verify(param < 5, "This is false: param > 5")
        return this.storage * param;
    };
}

Dev.test({ name: 'test' }, () => {
    const c1 = Scenario.originate(new OffChainViews());

    /** Test views */

    // Display the offchain call result
    Scenario.show(c1.state(1));

    // Assert the view result
    Scenario.verify(c1.state(2) === 2);
    // Assert call failures
    Scenario.verify(Scenario.isFailing(c1.state(6)));    // Expected to fail
    Scenario.verify(!Scenario.isFailing(c1.state(1)));   // Not expected to fail

    // Assert exception result
    // catchException returns an option:
    //      Sp.none if the call succeeds
    //      Sp.some(<exception>) if the call fails
    const e = Scenario.catchException<TString>(c1.state(7));
    Scenario.verify(e === Sp.some("This is false: param > 5"))
});
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Document extra information

The following elements represent six levels of section headings.

`<h1>` is the highest section level and `<p>` is the lowest.

<Snippet syntax={SYNTAX.PY}>

```python
scenario.h1("a title")
scenario.h2("a subtitle")
scenario.h3('Equivalent to <h3> HTML tag.')
scenario.h4("Equivalent to <h4> HTML tag.")
scenario.p("Equivalent to <p> HTML tag.")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Scenario.h1('A title');
Scenario.h2('A subtitle');
Scenario.h3('Equivalent to <h3> HTML tag.');
Scenario.h4("Equivalent to <h4> HTML tag.");
Scenario.p("Equivalent to <p> HTML tag.");
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


## Expressions

### Showing expressions

<Snippet syntax={SYNTAX.PY}>

To show expressions, we use `scenario.show(expression, html = True, stripStrings = False)`.

| Parameter | Type | Description |
|-----------|------|------------|
| html | bool | `True` by default, `False` to export not in html but like in source code.
| stripStrings | bool | `False` by default, `True` to remove quotes around strings.


```python
scenario.show(expression, html = True, stripStrings = False)

scenario.show(c1.data.myParameter1 * 12)
scenario.show(c1.data)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

To show expressions, we use `Scenario.show(<expression>, { html = true, stripStrings = false })`.

| Parameter | Type | Description |
|-----------|------|------------|
| html | bool | `true` by default, `false` to export not in html but like in source code.
| stripStrings | bool | `false` by default, `true` to remove quotes around strings.

```typescript
const c1 = Scenario.originate(new MyContract());

Scenario.show(c1.storage)
Scenario.show(1 + 1000, { html = true, stripStrings = false })
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


### Computing expressions

<Snippet syntax={SYNTAX.PY}>

To compute expressions, we use `scenario.compute(<expression>, **context_args)`.

```python
x = scenario.compute(c1.data.myParameter1 * 12)
```

The variable x can now be used in the sequel of the scenario and its value is fixed.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

To compute expressions, we use `Scenario.compute(<expression>)`.

```typescript
const x = Scenario.compute(c1.storage.myParameter1 * 12)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


#### Compute optional arguments

<Snippet syntax={SYNTAX.PY}>

Generic context arguments are:

| Parameter | Type | Accessor | Description |
|-----------|------|-----------|------------|
| sender | [sp.TAddress](/general/types#address) or [sp.test_account](/scenarios/testing/#test-accounts) | `sp.sender` | The simulated sender of the computation. <br/>Specific to computation.|
| source | [sp.TAddress](/general/types#address) or [sp.test_account](/scenarios/testing/#test-accounts) | `sp.source` | The simulated source of the computation. <br/>Specific to computation.|
| chain_id | [sp.TChainId](/general/types#chain_id) | `sp.chain_id` | The simulated chain_id. <br/>Preserved until changed.|
| level | [sp.TNat](/general/types#nat) | `sp.level` | The simulated block level.  <br/>Preserved until changed.|
| now | [sp.TTimestamp](/general/types#timestamp) | `sp.now` | The simulated block timestamp.  <br/>Preserved until changed.|
| voting_powers | [sp.TMap](/general/types#map)([sp.TKeyHash](/general/types#key_hash), [sp.TNat](/general/types#nat)) | `sp.total_voting_power`, <br/> `sp.voting_power` | The simulated voting powers for the test. <br/>Preserved until changed.|

</Snippet>

## Data associated with contracts

<Snippet syntax={SYNTAX.PY}>

When a variable `c1` represents a contract in a scenario, we can access some associated data:

- `c1.data`

    Retrieve contract storage.

- `c1.balance`

    Retrieve contract  balance.

- `c1.baker`

    Retrieve its optional delegated baker.


- `c1.address`

    Retrieve its address within the scenario.

    In storage or similar circumstances, deployed contracts get addresses of the form:
    - `KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1`
    - `KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF`
    - `KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H`
    - `KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC`
    - `KT1Tezooo4zzSmartPyzzSTATiCzzzyPVdv3`
    - `KT1Tezooo5zzSmartPyzzSTATiCzzzz48Z4p`
    - `KT1Tezooo6zzSmartPyzzSTATiCzzztY1196`
    - `KT1Tezooo7zzSmartPyzzSTATiCzzzvTbG1z`
    - `KT1Tezooo8zzSmartPyzzSTATiCzzzzp29d1`
    - `KT1Tezooo9zzSmartPyzzSTATiCzzztdBMLX`
    - `KT1Tezoo1ozzSmartPyzzSTATiCzzzw8CmuY`
    - ...

- `c1.typed`

    Retrieve its testing typed contract value.

    To access entry points, one can use field notation:
    - `c1.typed.my_entry_point`: to access typed entry point my_entry_point of contract `c1`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

When a variable `c1` represents a contract in a scenario, we can access some associated data:

- `c1.storage`

    Retrieve contract storage.

- `c1.balance`

    Retrieve contract  balance.

- `c1.baker`

    Retrieve its optional delegated baker.

- `c1.address`

    Retrieve its address within the scenario.

    In storage or similar circumstances, deployed contracts get addresses of the form:
    - `KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1`
    - `KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF`
    - `KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H`
    - `KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC`
    - `KT1Tezooo4zzSmartPyzzSTATiCzzzyPVdv3`
    - `KT1Tezooo5zzSmartPyzzSTATiCzzzz48Z4p`
    - `KT1Tezooo6zzSmartPyzzSTATiCzzztY1196`
    - `KT1Tezooo7zzSmartPyzzSTATiCzzzvTbG1z`
    - `KT1Tezooo8zzSmartPyzzSTATiCzzzzp29d1`
    - `KT1Tezooo9zzSmartPyzzSTATiCzzztdBMLX`
    - `KT1Tezoo1ozzSmartPyzzSTATiCzzzw8CmuY`
    - ...

- `c1.typed`

    Retrieve its testing typed contract value.

    To access entry points, one can use field notation:

    - `c1.typed.my_entry_point`: to access typed entry point my_entry_point of contract `c1`.

</Snippet>

## Dynamic contracts

<Snippet syntax={SYNTAX.PY}>

See reference [Create Contract](https://smartpy.io/ide?template=create_contract.py) template.

Internally, SmartPy uses two types of contracts:

- **Static contracts** which appear explicitly in the scenarios.
- **Dynamic contacts** which are created in other contracts executed in the scenario (with `sp.create_contract`).

Declaring a dynamic contract of dynamic id (an integer) with the corresponding storage and full parameter types.
The first dynamically created contractId is `0`, then `1`, etc.

```python
dynamic_contract = scenario.dynamic_contract(contractId, tcontract, tparameter)
```

Return a dynamic contract that contains regular fields `data`, `balance`, `baker`, `address`, `typed` and a `call(...)` method.

Dynamic contracts addresses are of the form:
  - `KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU`
  - `KT1Tezooo1zzSmartPyzzDYNAMiCzztcr8AZ`
  - `KT1Tezooo2zzSmartPyzzDYNAMiCzzxyHfG9`
  - `KT1Tezooo3zzSmartPyzzDYNAMiCzzvqsJQk`
  - `KT1Tezooo4zzSmartPyzzDYNAMiCzzywTMhC`
  - `KT1Tezooo5zzSmartPyzzDYNAMiCzzvwBH3X`
  - `KT1Tezooo6zzSmartPyzzDYNAMiCzzvyu5w3`
  - `KT1Tezooo7zzSmartPyzzDYNAMiCzztDqbVQ`
  - `KT1Tezooo8zzSmartPyzzDYNAMiCzzq2URWu`
  - `KT1Tezooo9zzSmartPyzzDYNAMiCzzwMosaF`
  - `KT1Tezoo1ozzSmartPyzzDYNAMiCzzzknqsi`


**`Call method`**

Send the `parameter` to the dynamic contract `entry_point`.

It is also possible to use `.run(...)` on the generated call as described in [Registering and Displaying Calls to Entry Points](#registering-and-displaying-calls-to-entry-points).

```python
dynamic_contract.call(entry_point, parameter)
dynamic_contract.call(entry_point, parameter).run(
                        sender          = ..., # TAddress
                        source          = ..., # TAddress
                        amount          = ..., # TMutez
                        now             = ..., # TTimestamp
                        level           = ..., # TNat
                        chain_id        = ..., # TChainId
                        voting_powers   = ..., # Dict(TKeyHash, TNat)
                        valid           = ..., # True | False
                        show            = ..., # True | False
                        exception       = ..., # any
                    )
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

See reference [Create Contract](https://smartpy.io/ts-ide?template=createContract.ts) template.

Internally, SmartTS uses two types of contracts:

- **Static contracts** which appear explicitly in the scenarios.
- **Dynamic contacts** which are created in other contracts executed in the scenario (with `Sp.createContract`).

Declaring a dynamic contract of dynamic id (an integer) with the corresponding storage and full parameter types.
The first dynamically created contractId is `0`, then `1`, etc.

```typescript
Work in progress...
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Assertions


<Snippet syntax={SYNTAX.PY}>

To verify conditions, we use `scenario.verify(<condition>)`.

To verify an equality condition, we can also use `scenario.verify_equal(<left_expression>, <right_expression>)` which works on both comparable and non-comparable types.

```python
scenario.verify(c1.data.myParameter == 51)

scenario.verify_equal(c1.data.myList, [2, 3, 5, 7])
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

To verify conditions, we use `Scenario.verify(<condition>)`.

To verify an equality condition, we can also use `Scenario.verifyEqual(<left_expression>, <right_expression>)` which works on both comparable and non-comparable types.

```typescript
Scenario.verify(c1.storage.myParameter === 51)

Scenario.verifyEqual(c1.storage.myList, [2, 3, 5, 7])
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


## Interactive testing

<Snippet syntax={SYNTAX.PY}>

To test interactively a contract, we use `scenario.simulation(<contract>)`.

It also provides a step-by-step mode that is very useful to understand some computation.

```python
scenario.simulation(c1)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

To test interactively a contract, we use **`Scenario.simulation(<contract>)`**.

It also provides a step-by-step mode that is very useful to understand some computation.

```typescript
Scenario.simulation(c1)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
