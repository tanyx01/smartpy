# Documentation of the main templates

Here are some documented SmartPy templates, documentation file exported with the [CLI/doc target](/cli/#documentation).
Other examples in the template menu of the [editor](pathname:///../ide).

|                      Template                      |                     Documentation                      | Additional guide               |
| :------------------------------------------------: | :----------------------------------------------------: | ------------------------------ |
|      [admin_hand_over.py][py-admin_hand_over]      |      [admin_hand_over.html][pdoc-admin_hand_over]      |                                |
|       [admin_multisig.py][py-admin_multisig]       |       [admin_multisig.html][pdoc-admin_multisig]       |                                |
|          [baking_swap.py][py-baking_swap]          |          [baking_swap.html][pdoc-baking_swap]          |                                |
|              [fa2_lib.py][py-fa2_lib]              |              [fa2_lib.html][pdoc-fa2_lib]              | [FA2 lib guide][fa2-lib-guide] |
|        [fa2_lib_test1.py][py-fa2_lib_test1]        |        [fa2_lib_test1.html][pdoc-fa2_lib_test1]        |                                |
|        [fa2_lib_test2.py][py-fa2_lib_test2]        |        [fa2_lib_test2.html][pdoc-fa2_lib_test2]        |                                |
|        [fa2_lib_test3.py][py-fa2_lib_test3]        |        [fa2_lib_test3.html][pdoc-fa2_lib_test3]        |                                |
|      [fa2_nft_minimal.py][py-fa2_nft_minimal]      |      [fa2_nft_minimal.html][pdoc-fa2_nft_minimal]      |                                |
| [fa2_fungible_minimal.py][py-fa2_fungible_minimal] | [fa2_fungible_minimal.html][pdoc-fa2_fungible_minimal] |                                |
|       [fibonacci_view.py][py-fibonacci_view]       |       [fibonacci_view.html][pdoc-fibonacci_view]       |                                |
|          [inheritance.py][py-inheritance]          |          [inheritance.html][pdoc-inheritance]          |                                |
|             [multisig.py][py-multisig]             |             [multisig.html][pdoc-multisig]             |                                |
|      [multisig_action.py][py-multisig_action]      |      [multisig_action.html][pdoc-multisig_action]      |                                |
|      [multisig_lambda.py][py-multisig_lambda]      |      [multisig_lambda.html][pdoc-multisig_lambda]      |                                |
|        [multisig_view.py][py-multisig_view]        |        [multisig_view.html][pdoc-multisig_view]        |                                |
|     [mutation_testing.py][py-mutation_testing]     |        [mutation_testing.html][pdoc-mutation_testing]  | CLI only                       |

[py-admin_hand_over]: pathname:///../ide?template=admin_hand_over.py
[py-admin_multisig]: pathname:///../ide?template=admin_multisig.py
[py-baking_swap]: pathname:///../ide?template=baking_swap.py
[py-fa2_lib]: pathname:///../ide?template=fa2_lib.py
[py-fa2_lib_test1]: pathname:///../ide?template=fa2_lib_test1.py
[py-fa2_lib_test2]: pathname:///../ide?template=fa2_lib_test2.py
[py-fa2_lib_test3]: pathname:///../ide?template=fa2_lib_test3.py
[py-fa2_nft_minimal]: pathname:///../ide?template=fa2_nft_minimal.py
[py-fa2_fungible_minimal]: pathname:///../ide?template=fa2_fungible_minimal.py
[py-fibonacci_view]: pathname:///../ide?template=fibonacci_view.py
[py-inheritance]: pathname:///../ide?template=inheritance.py
[py-multisig]: pathname:///../ide?template=multisig.py
[py-multisig_action]: pathname:///../ide?template=multisig_action.py
[py-multisig_lambda]: pathname:///../ide?template=multisig_lambda.py
[py-multisig_view]: pathname:///../ide?template=multisig_view.py
[py-mutation_testing]: pathname:///../ide?template=mutation_testing.py

[pdoc-admin_hand_over]: pathname:///../pdoc/admin_hand_over.html
[pdoc-admin_multisig]: pathname:///../pdoc/admin_multisig.html
[pdoc-baking_swap]: pathname:///../pdoc/baking_swap.html
[pdoc-fa2_lib]: pathname:///../pdoc/fa2_lib.html
[pdoc-fa2_lib_test1]: pathname:///../pdoc/fa2_lib_test1.html
[pdoc-fa2_lib_test2]: pathname:///../pdoc/fa2_lib_test2.html
[pdoc-fa2_lib_test3]: pathname:///../pdoc/fa2_lib_test3.html
[pdoc-fa2_nft_minimal]: pathname:///../pdoc/fa2_nft_minimal.html
[pdoc-fa2_fungible_minimal]: pathname:///../pdoc/fa2_fungible_minimal.html
[pdoc-fibonacci_view]: pathname:///../pdoc/fibonacci_view.html
[pdoc-inheritance]: pathname:///../pdoc/inheritance.html
[pdoc-multisig]: pathname:///../pdoc/multisig.html
[pdoc-multisig_action]: pathname:///../pdoc/multisig_action.html
[pdoc-multisig_lambda]: pathname:///../pdoc/multisig_lambda.html
[pdoc-multisig_view]: pathname:///../pdoc/multisig_view.html
[pdoc-mutation_testing]: pathname:///../pdoc/mutation_testing.html

[fa2-lib-guide]: /guides/FA/FA2/overview/
