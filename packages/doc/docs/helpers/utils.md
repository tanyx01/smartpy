# Utils

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import CompilationOnly from '@site/src/components/CompilationOnly';

This section includes a few util functions that can help you to ease the development.

## Methods

<Snippet syntax={SYNTAX.PY}>

### Convert mutez to nat

Convert `TMutez` to `TNat`

```python
sp.utils.mutez_to_nat(...)
```

### Convert nat to mutez

Convert `TNat` to `TMutez`

```python
sp.utils.nat_to_mutez(n)    # sp.nat(1) => sp.mutez(1)
# or
sp.utils.nat_to_tez(n)      # sp.nat(1) => sp.mutez(1000000)
```

### Convert string to bytes <CompilationOnly />

Encode a constant string as sp.TBytes.

```python
sp.utils.bytes_of_string("A String") # => 0x4120537472696e67
```

### Convert URL to metadata <CompilationOnly />

A Simple alias for `sp.big_map({"" : sp.utils.bytes_of_string(url)})`.

```python
sp.utils.metadata_of_url("ipfs://...")
```

### Compare underlying address

It returns a boolean that informs if an address `A%foo` has the same underlying address as `A`.

```python
sp.verify(
    sp.utils.same_underlying_address(
        sp.address("KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF%foo"),
        sp.address("KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF")
    ),
    message = "Not the same underlying address"
)
```

### Wrap entry point

`sp.utils.wrap_entry_point(name, entry_point)`

A wrapper to prepare the a python function to be used as an update for entry point name (which must be a constant string).

```python
def f(self):
    sp.verify(self.data.valid, "NOT VALID")

sp.utils.wrap_entry_point("ep", f)
```

### Extract seconds from timestamp

Extract seconds of type [sp.TNat](/general/types#nat) from a value of type [sp.TTimestamp](/general/types#timestamp).

```python
sp.utils.seconds_of_timestamp(sp.timestamp(100)) # 100
```

### vector, matrix, cube

There is no array in SmartPy because they are missing in Michelson, we usually use maps instead.

There are three helper functions:

`sp.utils.vector(..)`, `sp.utils.matrix(..)` and `sp.utils.cube(..)` that take respectively a list, a list of lists and a list of lists of lists and return maps.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

### Extract seconds from timestamp

Extract seconds of type [TNat](/general/types#nat) from a value of type [TTimestamp](/general/types#timestamp).

```typescript
("2021-09-02T16:35:09.936Z" as TTimestamp).toSeconds(); // 1630600510
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>


<Snippet syntax={SYNTAX.PY}>

### Callback entry point

The decorator `@sp.utils.view` is used to define a [callback entry point](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-4/tzip-4.md#view-entrypoints). A sugared version of `@sp.entry_point` that receives a callback argument, then processes some logic to obtain the requested data and calls back the requester with a response.

#### Example

```python
@sp.utils.view(sp.TNat)
def getBalance(self, params):
    sp.result(self.data.balances[params].balance
```

This code above is a simpler version of the equivalent:

```python
@sp.entry_point
def getBalance(self, params):
    sp.set_type(sp.snd(params), sp.TContract(sp.TNat))
    response = self.data.balances[sp.fst(params)].balance
    sp.transfer(response, sp.tez(0), sp.snd(params))
```

</Snippet>