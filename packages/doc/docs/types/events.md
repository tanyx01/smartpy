# Events

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

A SmartPy contract can emit so-called events. An event can be emitted
with the following command:

**`sp.emit(event, with_type = True, tag = None)`**

The `event` argument can be of any (packable) type and represents the
event to be logged. The optional `tag` argument is a string that is
logged along with the event. The option `with_type` determines whether
the event's type is repeated in the generated `EMIT` instruction. Only
in some rare situations involving very large types you may want to set
it to `False`.

See the [Tezos
documentation](https://tezos.gitlab.io/kathmandu/event.html) for
further details.  <MichelsonDocLink placeholder="EMIT"
url="https://tezos.gitlab.io/michelson-reference/#instr-CREATE_CONTRACT"/>
