# Voting Power

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

Tezos is based on proof of stake, meaning that key hashes have some voting power corresponding on all the delegations these key hashes received.
`voting_powers` must be included in `.run(...)` as described in [Calls to Entry Points](/scenarios/testing#calls-to-entry-points).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`votingPowers` must be included in `Scenario.transfer(op, {...})` as described in [Calls to Entry Points](/scenarios/testing#calls-to-entry-points).

</Snippet>


## Global properties

### Total voting power

<Snippet syntax={SYNTAX.PY}>

**`sp.total_voting_power`** <br />
Return the total voting power as [sp.TNat](/general/types#nat) of all contracts.<br/>
The total voting power coincides with the sum of the rolls count of every contract in the voting listings.<br/>
The voting listings is calculated at the beginning of every voting period.

#### Example

```python
# type: TNat
t_voting_power = sp.total_voting_power
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.totalVotingPower`** <br />
Return the total voting power as [TNat](/general/types#nat) of all implicit accounts.<br/>
The total voting power coincides with the sum of the rolls count of every contract in the voting listings.<br/>
The voting listings is calculated at the beginning of every voting period.

#### Example

```typescript
const votingPower: TNat = Sp.totalVotingPower; # type of sp.TNat
```

</Snippet>

<MichelsonDocLink placeholder="TOTAL_VOTING_POWER" url="https://tezos.gitlab.io/michelson-reference/#instr-TOTAL_VOTING_POWER"/>

## Operations

### Get voting power of a specific key hash

<Snippet syntax={SYNTAX.PY}>

**`sp.voting_power(key_hash)`** <br />
Return the voting power of a given contract. This voting power coincides with the weight of the contract in the voting listings (i.e., the rolls count) which is calculated at the beginning of every voting period.

It expects a single argument of type [sp.TKeyHash](/general/types#key_hash) and returns the associated voting power of type [sp.TNat](/general/types#nat).

#### Example

```python
voting_power = sp.voting_power(sp.key_hash("tz1e5Ynvimf9ic6RG7YgAkQVXmdMZ3JzXdT7"))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.votingPower(key_hash)`** <br />
Return the voting power of a given contract. This voting power coincides with the weight of the contract in the voting listings (i.e., the rolls count) which is calculated at the beginning of every voting period.

It expects a single argument of type [TKey_hash](/general/types#key_hash) and returns the associated voting power of type [TNat](/general/types#nat).

#### Example

```typescript
const votingPower: TNat = Sp.votingPower("tz1e5Ynvimf9ic6RG7YgAkQVXmdMZ3JzXdT7" as TKey_hash)
```

</Snippet>


<MichelsonDocLink placeholder="VOTING_POWER" url="https://tezos.gitlab.io/michelson-reference/#instr-VOTING_POWER"/>
