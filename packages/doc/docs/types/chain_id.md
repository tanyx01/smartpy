# Chain Id

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type of chain identifiers in SmartPy is [sp.TChainId](/general/types#chain_id).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="chain_id" url="https://tezos.gitlab.io/michelson-reference/#type-chain_id"/>.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of chain identifiers in SmartPy is [TChainId](/general/types#chain_id).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="chain_id" url="https://tezos.gitlab.io/michelson-reference/#type-chain_id"/>.

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.chain_id_cst(<bytes>)`** <br />
Return a chain id of type [sp.TChainId](/general/types#chain_id) by its hexadecimal representation.

#### Example

```python
value = sp.chain_id_cst("0x9caecab9")
```

Check reference [Chain Id](https://smartpy.io/ide?template=chain_id.py) template.

:::note
Please note that chain ids are non comparable. Equality can be verified by using [sp.verify_equal](/general/checking_condition.md#verify_equal).
:::

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`0x9caecab9 as TChain_id`** <br />
Return a chain id of type [TChainId](/general/types#chain_id) by its hexadecimal representation.

```typescript
const value: TChain_id = "0x9caecab9";
```

:::note
Please note that chain ids are non comparable. Equality can be verified by using [sp.verify_equal](/general/checking_condition.md#verify_equal).
:::

</Snippet>


## Global property

<Snippet syntax={SYNTAX.PY}>

**`sp.chain_id`** <br />
Get the chain identifier of type [sp.TChainId](/general/types#chain_id) which represents the network currently evaluating the transaction.

#### Example

```python
@sp.entry_point
def ep(self):
    sp.verify(sp.chain_id == sp.chain_id_cst("0x9caecab9"))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`sp.chainId`** <br />
Get the chain identifier of type [TChainId](/general/types#chain_id) which represents the network currently evaluating the transaction.

#### Example

```typescript
@EntryPoint
ep() {
    const chainId: TChain_id = "0x9caecab9";
    Sp.verify(Sp.chainId === chainId)
}
```

</Snippet>

<MichelsonDocLink placeholder="CHAIN_ID" url="https://tezos.gitlab.io/michelson-reference/#instr-CHAIN_ID"/>
