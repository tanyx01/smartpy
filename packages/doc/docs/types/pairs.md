# Pairs

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

Pairs in SmartPy are of type [sp.TPair](/general/types#pair)(`t1`, `t2`).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Pairs in SmartTS are of type [TTuple](/general/types#pair)<[`t1`, `t2`]>.

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.pair(expr1, expr2)`** or **`(expr1, expr2)`** <br />
Define a pair of two elements.

#### Example

```python
aPair  = (1, "A String")
aPair2 = sp.pair(1, 2)
```

**`sp.tuple(expr1, expr2, expr3)`** or **`(expr1, expr2, expr3)`** <br />
Define a tuple with more than two elements.

#### Example

```python
aPair  = (1, "A String", 3)
aPair2 = sp.tuple(1, 2, "A String")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`[expr1, expr2, expr3] as TTuple<[t1, t2, t3]>`** <br />
Define a tuple of two or more elements.

#### Example

```typescript
const aPair: TTuple<[sp.TNat, sp.TString, sp.TNat]> = [1, "A String", 2];
```

</Snippet>

<MichelsonDocLink placeholder="PAIR" url="https://tezos.gitlab.io/michelson-reference/#instr-PAIR"/>

## Operations

### Get the first element

<Snippet syntax={SYNTAX.PY}>

**`sp.fst(..)`** <br />
Access the first element of a pair.

#### Example

```python
aPair = (1, "A String")
sp.fst(aPair) # 1
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<pair>.fst()`** <br />
Access the first element of a pair.

#### Example

```typescript
const aPair: TTuple<[TNat, TString]> = [1, "A String"]
aPair.fst() // 1
```

</Snippet>

### Get the second element

<Snippet syntax={SYNTAX.PY}>

**`sp.snd(..)`** <br />
Access the second element of a pair.

#### Example

```python
aPair = (1, "A String")
sp.snd(aPair) # A String
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<pair>.snd()`** <br />
Access the second element of a pair.

#### Example

```typescript
const aPair: TTuple<[TNat, TString]> = [1, "A String"]
aPair.snd() // A String
```

</Snippet>

<MichelsonDocLink placeholder="CAR" url="https://tezos.gitlab.io/michelson-reference/#instr-CAR"/> and <MichelsonDocLink placeholder="CDR" url="https://tezos.gitlab.io/michelson-reference/#instr-CDR"/>

### Match elements

**`sp.match_pair(x)`** <br />
If `x` is a SmartPy pair (i.e. a value of type [sp.TPair](/general/types#pair)(`t`, `u`)), this returns a Python pair of its components.

#### Example

```python
x1, x2 = sp.match_pair(p)
```

See reference [Match](https://smartpy.io/ide?template=test_match.py) template.
