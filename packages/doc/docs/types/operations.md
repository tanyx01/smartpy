# Operations and Transactions

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

Operations are usually handled through commands.
They can sometimes be handled in a more functional manner with expressions.

## Commands to Build Operations

This is the usual method to build operations and is documented other pages.

See [Contracts and Addresses](/types/contracts_addresses) for
transfers and contract creation. See [Keys](/types/keys) for
delegations. See [Events](/types/events) for events.

## Expressions to Build Operations
This is rarely in practice but proves to be useful.

Contrary to operations built in commands, operations built in
expressions need to be recorded explicitely within the contract.
Otherwise, they are dropped.

### Recording Operations
<Snippet syntax={SYNTAX.PY}>

**`sp.operations()`**

Return the current list of operations. This list can be manipulated
freely in an entry point and serves as the list of recorded
operations.

The compiler automatically reverses this list at the end of an entry
point to respect the typical creation order of operations.

**`sp.add_operations(l)`**

Pushes a list of operations on top of the current list of operations.

It's an short-end for:
```python
def add_operations(l):
    with sp.for_('op', l) as op:
        sp.operations().push(op)
```
</Snippet>

<Snippet syntax={SYNTAX.TS}>
Not supported yet.
</Snippet>

<Snippet syntax={SYNTAX.ML}>
Not supported yet.
</Snippet>

### Operations

<Snippet syntax={SYNTAX.PY}>

**`sp.transfer_operation(arg, amount, destination)`**

Return an expression that represents a transfer operation.

**`sp.set_delegate_operation(baker)`**

Return an expression that represents a set_delegate operation.

**`sp.create_contract_operation(contract, storage = None, amount =
tez(0), baker = None)`**

Return an expression that represents a create_contract operation.

**`sp.emit_operation(event, with_type = True, tag = None)`**

Return an expression that represents an emit operation.


In practice, SmartPy operation commands are built on top of these
expressions:
```python
def transfer(arg, amount, destination, line_no = None):
    operations().push(transfer_operation(arg, amount, destination, line_no))

def set_delegate(key_hash):
    operations().push(set_delegate_operation(key_hash))

def create_contract(contract, storage = None, amount = tez(0), baker = None):
    x = create_contract_operation(contract, storage, amount, baker)
    operations().push(x.operation)
    return x.address

def emit(event, tag = None, line_no = None):
    operations().push(emit_operation(event, tag, line_no))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>
Not supported yet.
</Snippet>

<Snippet syntax={SYNTAX.ML}>
Not supported yet.
</Snippet>
