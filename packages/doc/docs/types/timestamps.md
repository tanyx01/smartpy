# Timestamps

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type of timestamps in SmartPy is [sp.TTimestamp](/general/types#timestamp).

The corresponding type in Michelson is <MichelsonDocLink placeholder="timestamp" url="https://tezos.gitlab.io/michelson-reference/#type-timestamp"/>.

See reference [Timestamps](https://smartpy.io/ide?template=testTimestamp.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of timestamps in SmartTS is [TTimestamp](/general/types#timestamp).

The corresponding type in Michelson is <MichelsonDocLink placeholder="timestamp" url="https://tezos.gitlab.io/michelson-reference/#type-timestamp"/>.

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.timestamp(...)`** <br />
A literal timestamp is defined by doing `sp.timestamp(i)` where `i` is an integer representing the number of seconds since epoch `(January 1st 1970)`

#### Example

```python
timestamp1 = sp.timestamp(0)     # Thu, 01 Jan 1970 00:00:00 GMT
timestamp2 = sp.timestamp(10)    # Thu, 01 Jan 1970 00:00:10 GMT
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`i as TTimestamp`** <br />
A literal timestamp where `i` is an integer representing the number of seconds since epoch `(January 1st 1970)`

#### Example

```typescript
const timestamp1: TTimestamp = 0        // Thu, 01 Jan 1970 00:00:00 GMT
const timestamp2: TTimestamp = 10       // Thu, 01 Jan 1970 00:00:10 GMT
const timestamp3: TTimestamp = "2021-07-26T12:35:28.504Z";
```

</Snippet>

## Global properties

### Get block timestamp

<Snippet syntax={SYNTAX.PY}>

**`sp.now`** <br />
The minimal injection time on the stack for the current block/priority. For all reasonable purposes, this is a technical detail and `sp.now` should be understood as the timestamp of the block whose validation triggered the execution.

#### Example

```python
block_time = sp.now
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.now`** <br />
The minimal injection time on the stack for the current block/priority. For all reasonable purposes, this is a technical detail and `Sp.now` should be understood as the timestamp of the block whose validation triggered the execution.

#### Example

```typescript
const blockTime: TTimestamp = Sp.now;
```

</Snippet>

<MichelsonDocLink placeholder="NOW" url="https://tezos.gitlab.io/michelson-reference/#instr-NOW"/>

## Operations

### Subtraction

<Snippet syntax={SYNTAX.PY}>

**`expr1 - expr2`** <br />
Subtract two [sp.TTimestamp](/general/types#timestamp) values, `expr1` and `expr2`.

Return the difference in seconds of type [sp.TInt](/general/types#nat) between two timestamps.

#### Example

```python
value = sp.timestamp(10) - sp.timestamp(2)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<timestamp1>.minus(<timestamp2>)`** <br />
Subtract two [TTimestamp](/general/types#timestamp) values, `timestamp1` and `timestamp2`.

Return the difference in seconds of type [TInt](/general/types#nat) between two timestamps.

#### Example

```typescript
const value: TInt = (10 as TTimestamp).minus(2 as TTimestamp);
```

</Snippet>

<MichelsonDocLink placeholder="SUB" url="https://tezos.gitlab.io/michelson-reference/#instr-SUB"/>

### Add seconds

<Snippet syntax={SYNTAX.PY}>

**`<timestamp>.add_seconds(seconds)`** <br />
Return a timestamp with `seconds` added to `timestamp`, where `timestamp` must be a [sp.TTimestamp](/general/types#timestamp) and `seconds` a [sp.TInt](/general/types#int).

#### Example

```python
e.add_seconds(10)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<timestamp>.addSeconds(seconds)`** <br />
Return a timestamp with `seconds` added to `timestamp`, where `timestamp` must be a [TTimestamp](/general/types#timestamp) and `seconds` a [TInt](/general/types#int).

#### Example

```typescript
const timestamp: TTimestamp = "2021-07-26T12:35:28.000Z";
timestamp.addSeconds(10);
```

</Snippet>

<MichelsonDocLink placeholder="ADD" url="https://tezos.gitlab.io/michelson-reference/#instr-ADD"/>

### Add minutes

<Snippet syntax={SYNTAX.PY}>

**`<timestamp>.add_minutes(minutes)`** <br />
Return a timestamp with `minutes` added to `timestamp`, where `timestamp` must be a [sp.TTimestamp](/general/types#timestamp) and `minutes` a [sp.TInt](/general/types#int).

#### Example

```python
e.add_minutes(10)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<timestamp>.addMinutes(minutes)`** <br />
Return a timestamp with `minutes` added to `timestamp`, where `timestamp` must be a [TTimestamp](/general/types#timestamp) and `minutes` a [TInt](/general/types#int).

#### Example

```typescript
const timestamp: TTimestamp = "2021-07-26T12:35:28.000Z";
timestamp.addMinutes(1)
```

</Snippet>

<MichelsonDocLink placeholder="ADD" url="https://tezos.gitlab.io/michelson-reference/#instr-ADD"/>

### Add hours

<Snippet syntax={SYNTAX.PY}>

**`<timestamp>.add_hours(hours)`** <br />
Return a timestamp with `hours` added to `timestamp`, where `timestamp` must be a [sp.TTimestamp](/general/types#timestamp) and `hours` a [sp.TInt](/general/types#int).

#### Example

```python
e.add_hours(1)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<timestamp>.addHours(hours)`** <br />
Return a timestamp with `hours` added to `timestamp`, where `timestamp` must be a [TTimestamp](/general/types#timestamp) and `hours` a [TInt](/general/types#int).

#### Example

```typescript
const timestamp: TTimestamp = "2021-07-26T12:35:28.000Z";
timestamp.addHours(1)
```

</Snippet>

<MichelsonDocLink placeholder="ADD" url="https://tezos.gitlab.io/michelson-reference/#instr-ADD"/>

### Add days

<Snippet syntax={SYNTAX.PY}>

**`<timestamp>.add_days(days)`** <br />
Return a timestamp with `days` added to `timestamp`, where `timestamp` must be a [sp.TTimestamp](/general/types#timestamp) and `days` a [sp.TInt](/general/types#int).
#### Example

```python
e.add_days(1)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<timestamp>.addDays(days)`** <br />
Return a timestamp with `days` added to `timestamp`, where `timestamp` must be a [TTimestamp](/general/types#timestamp) and `days` a [TInt](/general/types#int).

#### Example

```typescript
const timestamp: TTimestamp = "2021-07-26T12:35:28.000Z";
timestamp.addDays(1)
```

</Snippet>


<MichelsonDocLink placeholder="ADD" url="https://tezos.gitlab.io/michelson-reference/#instr-ADD"/>


## Utils (meta-programming)

### Build a timestamp from UTC

<Snippet syntax={SYNTAX.PY}>

**`sp.timestamp_from_utc(year, month, day, hours, minutes, seconds)`** <br />
Compute a constant timestamp corresponding to an UTC datetime.

#### Example

```python
sp.timestamp_from_utc(2021, 12, 31, 23, 59, 59) # Fri, 31 Dec 2021 23:59:59 GMT
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress ...

</Snippet>

### Get current UTC timestamp

<Snippet syntax={SYNTAX.PY}>

**`sp.timestamp_from_utc_now()`** <br />
Compute a constant timestamp corresponding to now. This is fixed at contract or test evaluation time.

#### Example

```python
sp.timestamp_from_utc_now()
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress ...

</Snippet>
