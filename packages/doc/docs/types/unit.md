# Unit

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type of unit values in SmartPy is [sp.TUnit](/general/types#unit).

The corresponding type in Michelson is <MichelsonDocLink placeholder="unit" url="https://tezos.gitlab.io/michelson-reference/#type-unit"/>.<br/>
It is the return type of commands and the input types of entry points with empty parameters.

**`sp.unit`** <br />
The unique value of type [sp.TUnit](/general/types#unit).

#### Example

```python
unitValue = sp.unit
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>


The type of unit values in SmartTS is [TUnit](/general/types#unit).

The corresponding type in Michelson is <MichelsonDocLink placeholder="unit" url="https://tezos.gitlab.io/michelson-reference/#type-unit"/>[`unit`].<br/>
It is the return type of commands and the input types of entry points with empty parameters.

**`Sp.unit`** <br />
The unique value of type [TUnit](/general/types#unit).

#### Example

```typescript
const unitValue: TUnit = Sp.unit;
```

</Snippet>

<MichelsonDocLink placeholder="UNIT" url="https://tezos.gitlab.io/michelson-reference/#instr-UNIT"/>
