# Maps and Big Maps

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

## Maps

<Snippet syntax={SYNTAX.PY}>

Maps in SmartPy are of type [sp.TMap](/general/types#map)(`tKey`, `tValue`).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="map" url="https://tezos.gitlab.io/michelson-reference/#type-map"/>.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Maps in SmartTS are of type [TMap](/general/types#map)<`tKey`, `tValue`>.<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="map" url="https://tezos.gitlab.io/michelson-reference/#type-map"/>.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

See reference [Lists](https://smartpy.io/ide?template=testLists.py) and [Maps](https://smartpy.io/ide?template=test_maps.py) template.


### Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.map(l = ..., tkey = ..., tvalue = ...)`** or **`{ 0 : "aa", 12 :
"bb" }`** <br />
Define a map of (optional) elements in `l` (a Python dictionary) with optional key type `tkey` and optional value type `tvalue`.

#### Example

```python
my_map = sp.map(l = {0 : "aa", 12 : "bb" }, tkey = sp.TNat, tvalue = sp.TString)

# Standard Python dictionaries are also accepted
my_map = {0 : "aa", 12 : "bb" }
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`[ [0, "aa"], [0, "bb"] ] as TMap<TNat, TString>`** <br />
Define a map with keys of type `TNat` and values type `TString`.

```typescript
const my_map: TMap<TNat, TString> = [ [0, "aa"], [0, "bb"] ];
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

See <MichelsonDocLink placeholder="EMPTY_MAP" url="https://tezos.gitlab.io/michelson-reference/#instr-EMPTY_MAP"/> and <MichelsonDocLink placeholder="PUSH" url="https://tezos.gitlab.io/michelson-reference/#instr-PUSH"/>.

### Adding/Updating entries

<Snippet syntax={SYNTAX.PY}>

**`<my_map>[key] = <new_value>`** <br />
Set or replace an element in a map.

#### Example

```python
my_map[key] = value
```

**`sp.update_map(map, key, value)`** <br />
Return a new copy of map `my_map` where `key` has optional value
`value` (**`sp.none`** to delete). <br />
#### Example

```python
# Update entry with key `0` to have value `"cc"`
my_map = sp.update_map(my_map, 0, sp.some("cc"))
```

**`sp.get_and_update(map, key, value)`** <br />
Return two elements, the previous optional value (`sp.none` if missing) and a new map as `sp.update_map` would do.

#### Example

```python
(previous_value, new_map) = sp.get_and_update(my_map, 1, sp.some("dd"))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Set or replace an element in a map.

```typescript
// Update entry with key `0` to have value `"cc"`
my_map.set(0, "cc")
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="UPDATE" url="https://tezos.gitlab.io/michelson-reference/#instr-UPDATE"/>

### Deleting entries

<Snippet syntax={SYNTAX.PY}>

**`del my_map[key]`** <br />
Delete an element from a map.

#### Example

```python
my_map = sp.map({ 1: "aa", 2: "bb" })
del my_map[1] # my_map == { 2: "bb" }
```

**`sp.update_map(my_map, key, sp.none)`** <br />
Return a new copy of map `my_map` where `sp.none` value is used to remove the entry.

#### Example

```python
# Remove entry with key `1`
my_map = sp.update_map({ 1: "aa", 2: "bb" }, 1, sp.none) # my_map == { 2: "bb" }
```

**`sp.get_and_update(my_map, key, sp.none)`** <br />
Return two elements, the previous optional value (`sp.none` if missing) and a new map as `sp.update_map` would do.

#### Example

```python
# Remove entry with key `1` and return the previous and new state of the map
(previous_value, new_map) = sp.get_and_update({ 1: "aa", 2: "bb" }, 1, sp.none) # previous_value = { 1: "aa", 2: "bb" }, new_map == { 2: "bb" }
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_map>.remove(key, Sp.none)`** <br />
Delete an element from a map.

#### Example

```typescript
// Removing key `0`
my_map.remove(0, Sp.none)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

### Access entry value

<Snippet syntax={SYNTAX.PY}>

**`<my_map>[key]`** <br />
Look up an entry in a map. It fails if the entry is not found. `key` must have the type of its keys.

#### Example

```python
sp.map({ 1: "aa", 2: "bb" })[1] # "aa"
```

**`<my_map>.get(key, default_value = None, message = None)`** <br />
Same as `my_map[key]`. If `default_value` is specified and there is no entry for `key` in `my_map`, returns `default_value` instead of failing. If `default_value` is not specified and the is no entry for `key`, it fails with `message` if present.

#### Example

```python
sp.map({ 1: "aa", 2: "bb" }).get(1) # "aa"
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_map>.get(key)`** <br />
Look up an entry in a map. It fails if the entry is not found. `key` must have the type of its keys.

#### Example

```typescript
my_map.get(key)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<Snippet syntax={SYNTAX.PY}>

**`<my_map>.get_opt(key)`** <br />
Look up an entry in a map. It returns `sp.some(<value>)` if found, `sp.none` otherwise. `key` must have the type of its keys.

#### Example

```python
my_map.get_opt(key)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="GET" url="https://tezos.gitlab.io/michelson-reference/#instr-GET"/>

### Check key existence

<Snippet syntax={SYNTAX.PY}>

**`<my_map>.contains(key)`** <br />
Check whether the map `my_map` contains the `key`.

#### Example

```python
sp.map({ 1: "aa", 2: "bb" }).contains(1) # True
```

:::note
The syntax `x in m` does not work for SmartPy expressions because the `.. in ..` notation in Python cannot be overloaded.
:::

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_map>.hasKey(key)`** <br />
Check whether the map `my_map` contains the `key`.

#### Example

```typescript
my_map.hasKey(key)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="MEM" url="https://tezos.gitlab.io/michelson-reference/#instr-MEM"/>

### Get size

<Snippet syntax={SYNTAX.PY}>

**`sp.len(my_map)`** <br />
Return the size of the map `my_map`.

#### Example

```python
self.data.my_map = { 1: "aa", 2: "bb" }
sp.len(self.data.my_map) # 2
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_map>size()`** <br />
Return the size of the map `my_map`.

#### Example

```typescript
my_map.size()
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="SIZE" url="https://tezos.gitlab.io/michelson-reference/#instr-SIZE"/>

### Get entries

<Snippet syntax={SYNTAX.PY}>

**`<my_map>.items()`** <br />
Return the sorted list of key-value entries in a map. Each entry is rendered as record with the two fields `key` and `value`.

#### Example

```python
sp.map({ 1: "aa", 2: "bb" }).items() # [{ key: 1, value: "aa" }, { key: 2, value: "bb" }]
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_map>.entries()`** <br />
Return the sorted list of key-value entries in a map. Each entry is rendered as record with the two fields `key` and `value`.

#### Example

```typescript
const my_map: TMap<TNat, TString> = [ [1, "aa"], [2, "bb"] ];
my_map.entries() // [{ key: 1, value: "aa" }, { key: 2, value: "bb" }]
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

### Get Keys

<Snippet syntax={SYNTAX.PY}>

**`<my_map>.keys()`** <br />
Return the sorted list of keys of a map.

#### Example

```python
sp.map({ 1: "aa", 2: "bb" }).keys() # [1, 2]
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_map>.keys()`** <br />
Return the sorted list of keys of a map.

#### Example

```typescript
const my_map: TMap<TNat, TString> = [ [1, "aa"], [2, "bb"] ];
my_map.keys() // [1, 2]
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


### Get values

<Snippet syntax={SYNTAX.PY}>

**`<my_map>.values()`** <br />
Return the list of values of a map, sorted by keys.

#### Example

```python
sp.map({ 1: "aa", 2: "bb" }).values() # ["aa", "bb"]
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_map>.values()`** <br />
Return the list of values of a map, sorted by keys.

#### Example

```typescript
const my_map: TMap<TNat, TString> = [ [1, "aa"], [2, "bb"] ];
my_map.values() // ["aa", "bb"]
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


## Big maps

<Snippet syntax={SYNTAX.PY}>

Big maps, of type [sp.TBigMap](/general/types#big_map)(`key`, `value`)`, are lazy data structures that are only serialized and deserialized on demand.<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="big_map" url="https://tezos.gitlab.io/michelson-reference/#type-big_map"/>.

We cannot iterate on big maps or compute their sizes.

See reference [Lists](https://smartpy.io/ide?template=testLists.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Big maps, of type [TBig_map](/general/types#big_map)(`key`, `value`)`, are lazy data structures that are only serialized and deserialized on demand.<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="big_map" url="https://tezos.gitlab.io/michelson-reference/#type-big_map"/>.

We cannot iterate on big maps or compute their sizes.

</Snippet>


### Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.big_map(l = ..., tkey = ..., tvalue = ...)`** <br />
Define a big_map of (optional) elements in `l` (a Python dictionary) with optional key type `tkey` and optional value type `tvalue`.

#### Example

```python
my_big_map = sp.big_map(l = {0 : "aa", 12 : "bb" }, tkey = sp.TNat, tvalue = sp.TString)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`[ [0, "aa"], [0, "bb"] ] as TBig_map<TNat, TString>`** <br />
Define a big_map with keys of type `TNat` and values type `TString`.

#### Example

```typescript
const my_big_map: TBig_map<TNat, TString> = [ [0, "aa"], [0, "bb"] ];
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="EMPTY_BIG_MAP" url="https://tezos.gitlab.io/michelson-reference/#instr-EMPTY_BIG_MAP"/>

### Adding/Updating entries

<Snippet syntax={SYNTAX.PY}>

**`<my_big_map>[key] = <new_value>`** <br />
Set or replace an element in a big_map.

#### Example

```python
my_big_map[key] = value
```

**`sp.update_map(my_big_map, 0, sp.some("cc"))`** <br />
Return a new copy of big_map `my_big_map` where `key` has optional
value `value` (**`sp.none`** to delete). <br />
#### Example

```python
# Update entry with key `0` to have value `"cc"`
my_big_map = sp.update_map({ 0 : "aa", 1 : "bb" }, 0, sp.some("cc"))
```

**`sp.get_and_update(my_big_map, 1, sp.some("dd"))`** <br />
Return two elements, the previous optional value (`sp.none` if missing) and a new big_map as `sp.update_map` would do.

#### Example

```python
(previous_value, new_big_map) = sp.get_and_update({ 0 : "aa", 1 : "bb" }, 1, sp.some("dd"))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_big_map>.set(key, value)`** <br />
Set or replace an element in a big_map.

#### Example

```typescript
// Update entry with key `0` to have value `"cc"`
my_big_map.set(0, "cc")
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="UPDATE" url="https://tezos.gitlab.io/michelson-reference/#instr-UPDATE"/>

### Deleting entries

<Snippet syntax={SYNTAX.PY}>

**`del my_big_map[key]`** <br />
Delete an element from a big_map.

#### Example

```python
my_big_map = sp.big_map({ 0: "aa", 1: "bb" })
del my_big_map[0]
```

**`sp.update_map(my_big_map, 0, sp.none)`** <br />
Return a new copy of big_map `my_big_map` where `sp.none` value is used to remove the entry.

#### Example

```python
# Remove entry with key `0`
my_big_map = sp.update_map({ 0 : "aa", 1 : "bb" }, 0, sp.none)
```

**`sp.get_and_update(my_big_map, 0, sp.none)`** <br />
Return two elements, the previous optional value (`sp.none` if missing) and a new big_map as `sp.update_map` would do.

#### Example

```python
# Remove entry with key `1` and return the previous and new state of the big_map
(previous_value, new_map) = sp.get_and_update({ 0 : "aa", 1 : "bb" }, 1, sp.none)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_big_map>.remove(key, Sp.none)`** <br />
Delete an element from a big_map.

#### Example

```typescript
// Removing key `0`
my_big_map.remove(0, Sp.none)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

### Access entry value

<Snippet syntax={SYNTAX.PY}>

Look up an entry in a big_map. It fails if the entry is not found. `key` must have the type of its keys.

#### Example

```python
my_big_map[key]
```

**`<my_big_map>.get(key, default_value = None, message = None)`** <br />
Same as `my_big_map[key]`. If `default_value` is specified and there is no entry for `key` in `my_big_map`, returns `default_value` instead of failing. If `default_value` is not specified and the is no entry for `key`, it fails with `message` if present.

#### Example

```python
my_big_map.get(key, default_value = None)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_big_map>.get(key)`** <br />
Look up an entry in a big_map. It fails if the entry is not found. `key` must have the type of its keys.

#### Example

```typescript
my_big_map.get(key)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<Snippet syntax={SYNTAX.PY}>

**`<my_big_map>.get_opt(key)`** <br />
Look up an entry in a big_map. It returns `sp.some(<value>)` if found, `sp.none` otherwise. `key` must have the type of its keys.

#### Example

```python
my_big_map.get_opt(key)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="GET" url="https://tezos.gitlab.io/michelson-reference/#instr-GET"/>

### Check key existence

<Snippet syntax={SYNTAX.PY}>

**`<my_big_map>.contains(key)`** <br />
Check whether the big_map `my_big_map` contains the `key`.

#### Example

```python
my_big_map.contains(key)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_big_map>.hasKey(key)`** <br />
Check whether the big_map `my_big_map` contains the `key`.

#### Example

```typescript
my_big_map.hasKey(key)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="MEM" url="https://tezos.gitlab.io/michelson-reference/#instr-MEM"/>
