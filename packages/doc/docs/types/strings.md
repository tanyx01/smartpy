# Strings

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type of SmartPy strings is [sp.TString](/general/types#string).

The corresponding type in Michelson is <MichelsonDocLink placeholder="string" url="https://tezos.gitlab.io/michelson-reference/#type-string"/>.

See reference [Strings and Bytes](https://smartpy.io/ide?template=stringManipulations.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of SmartPy strings is [TString](/general/types#string).

The corresponding type in Michelson is <MichelsonDocLink placeholder="string" url="https://tezos.gitlab.io/michelson-reference/#type-string"/>.

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

**`"..."`**, **`'...'`** or **`sp.string(s)`** <br />
Strings in SmartPy are introduced by simply using regular Python strings of the form `"..."` or `'...'`, or by using `sp.string(s)` where `s` is a Python string.

#### Example

```python
aString1 = "Hello World"
aString2 = sp.string("Hello World")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`"..."`**, **`'...'`** <br />
Strings in SmartTS are introduced by simply using regular Typescript strings of the form `"..."` or `'...'`.

#### Example

```typescript
const sString: TString = "Hello World"
```

</Snippet>


## Operations

### Concatenation

<Snippet syntax={SYNTAX.PY}>

**`expr1 + expr2`** <br />
Concatenate two sequences of [sp.TString](/general/types#string).

#### Example

```python
"Hello" + " " + "World" # Hello World
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<expr1>.concat(<expr2>)`** <br />
Concatenate two sequences of [TString](/general/types#string).

```typescript
"Hello".concat(" ").concat("World"); // Hello World
```

</Snippet>


<Snippet syntax={SYNTAX.PY}>

**`sp.concat(<l>)`** <br />
Concatenate a list `l` of [sp.TString](/general/types#string).

#### Example

```python
sp.concat(["Hello", " ", "World"]) # Hello World
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.concat(<l>)`** <br />
Concatenate a list `l` of [TString](/general/types#string).

```typescript
Sp.concat(["Hello", " ", "World"]) // Hello World
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="CONCAT" url="https://tezos.gitlab.io/michelson-reference/#instr-CONCAT"/>


### Obtaining Size

<Snippet syntax={SYNTAX.PY}>

**`sp.len(<expression>)`** <br />
Return the length of `<expression>`.

#### Example

```python
size = sp.len("A String")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`<expression>.size()`

Return the length of `<expression>`.

#### Example

```typescript
const size: TNat = "A String".size()
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="SIZE" url="https://tezos.gitlab.io/michelson-reference/#instr-SIZE"/>


### Slicing

<Snippet syntax={SYNTAX.PY}>

**`sp.slice(<expression>, offset=<offset>, length=<length>)`** <br />
Slice `expression` of type [sp.TString](/general/types#string) from `offset` for `length` characters.

It returns an expression of type [sp.TOption](/general/types#option)([sp.TString](/general/types#string)).

#### Example

```python
sp.slice("Hello World", 0, 5) # sp.some("Hello")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<expr>.slice(<offset>, <length>)`** <br />
Slice `expr` of type [TString](/general/types#string) from `offset` for `length` characters.

It returns an expression of type [TOption](/general/types#option)([TString](/general/types#string)).

```typescript
"Hello World".slice(0, 5); // Sp.some("Hello")
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="SLICE" url="https://tezos.gitlab.io/michelson-reference/#instr-SLICE"/>
