# Sets

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

Sets in SmartPy are of type [sp.TSet](/general/types#set)(`element type`).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="set" url="https://tezos.gitlab.io/michelson-reference/#type-set"/>.

See reference [Lists](https://smartpy.io/ide?template=testLists.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Sets in SmartTS are of type [TSet](/general/types#set)(`element type`).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="set" url="https://tezos.gitlab.io/michelson-reference/#type-set"/>.

See reference [Sets](https://smartpy.io/ts-ide?template=Sets.ts) template.

</Snippet>


## Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.set(l = ..., t = ...)`** <br />
Define a set of (optional) elements in list `l` with optional element type `t`.<br/>

**`{1, 2, 3}`** <br />
Standard Python sets can also be used to define SmartPy sets.
It only works with non-SmartPy specific expressions.
For SmartPy expressions, we must use `sp.set([e1, e2, ..., en])`.

#### Example

```python
set1 = sp.set([1, 2, 3])
self.data.set2 = { 1, 2, 3 }
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`[...l] as TSet<t>`** <br />
Define a set of (optional) elements `l` with optional element type `t`.<br/>

#### Example

```typescript
const mySet: TSet<TNat> = [1, 2, 3];
```

</Snippet>

<MichelsonDocLink placeholder="EMPTY_SET" url="https://tezos.gitlab.io/michelson-reference/#instr-EMPTY_SET"/>

## Operations

### Get elements

<Snippet syntax={SYNTAX.PY}>

**`<my_set>.elements()`** <br />
Return the sorted list of elements in a set.

#### Example

```python
self.data.my_set = { 3, 2, 1 }
self.data.my_set.elements() # [3, 2, 1] of sp.TList<t>
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_set>.elements()`** <br />
Return the sorted list of elements in a set.

#### Example

```typescript
const mySet: TSet<TNat> = [ 3, 2, 1 ]
const myList: TList<TNat> = mySet.elements() // [3, 2, 1]
```

</Snippet>

### Add element

<Snippet syntax={SYNTAX.PY}>

**`<my_set>.add(element)`** <br />
Add an `element` from a set.

```python
self.data.my_set = { 1, 2 }
self.data.my_set.add(1) # { 1, 2, 3 }
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_set>.add(element)`** <br />
Add an `element` from a set.

```typescript
const mySet: TSet<TNat> = [ 1,  2 ]
mySet.add(3) // [ 1, 2, 3 ]
```

</Snippet>


<MichelsonDocLink placeholder="UPDATE" url="https://tezos.gitlab.io/michelson-reference/#instr-UPDATE"/>

### Remove element

<Snippet syntax={SYNTAX.PY}>

**`<my_set>.remove(element)`** <br />
Remove an `element` from a set.

```python
self.data.my_set = { 1, 2 }
self.data.my_set.remove(1) # { 2 }
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_set>.remove(element)`** <br />
Remove an `element` from a set.

```typescript
const mySet: TSet<TNat> = [ 1,  2 ]
mySet.remove(1) // [ 2 ]
```

</Snippet>

<MichelsonDocLink placeholder="UPDATE" url="https://tezos.gitlab.io/michelson-reference/#instr-UPDATE"/>

### Check if element exists

<Snippet syntax={SYNTAX.PY}>

**`<my_set>.contains(element)`** <br />
Check whether the set `my_set` contains the `element`.

```python
self.data.my_set = { 1, 2 }
self.data.my_set.contains(1) # True
```

:::note
The syntax `x in m` does not work for SmartPy expressions because the `.. in ..` notation in Python cannot be overloaded.
:::

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<my_set>.contains(element)`** <br />
Check whether the set `my_set` contains the `element`.

```typescript
const mySet: TSet<TNat> = [ 1,  2 ]
mySet.contains(1) // true
```

</Snippet>
