# Keys

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

## Public Key

<Snippet syntax={SYNTAX.PY}>

The type of public keys in SmartPy is [sp.TKey](/general/types#key).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="key" url="https://tezos.gitlab.io/michelson-reference/#type-key"/>.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of public keys in SmartTS is [TKey](/general/types#key).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="key" url="https://tezos.gitlab.io/michelson-reference/#type-key"/>.

</Snippet>

### Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.key(s)`** <br />
A literal key is of type [sp.TKey](/general/types#key) where `s` is a Python string.

#### Example

```python
akey = sp.key("edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`'edpk...' as TKey`** <br />
A literal key is of type [sp.TKey](/general/types#key)

#### Example

```typescript
const aKey: TKey = "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav";
```

</Snippet>

### Operations

<Snippet syntax={SYNTAX.PY}>

**`sp.hash_key(key)`** <br />
Compute the Base58Check of a public key.

It expects a single argument of type [sp.TKey](/general/types#key), and returns a value of type [sp.TKeyHash](/general/types#key_hash).

#### Example

```python
hashKey = sp.hash_key(sp.key("edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav"))
```

</Snippet>


<Snippet syntax={SYNTAX.TS}>

**`Sp.hashKey(key)`** <br />
Compute the Base58Check of a public key.

It expects a single argument of type [TKey](/general/types#key), and returns a value of type [TKey_hash](/general/types#key_hash).

#### Example

```typescript
const keyHash: TKey_hash = Sp.hashKey("edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav");
```

</Snippet>

<MichelsonDocLink placeholder="HASH_KEY" url="https://tezos.gitlab.io/michelson-reference/#instr-HASH_KEY"/>
<br/>
<br/>

<Snippet syntax={SYNTAX.PY}>

**`sp.check_signature(k, s, b)`** <br />
Verify that a byte sequence has been signed with a given key.

See [Signatures](/types/signatures)

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.checkSignature(k, s, b)`** <br />
Verify that a byte sequence has been signed with a given key.

See [Signatures](/types/signatures)

</Snippet>

<MichelsonDocLink placeholder="CHECK_SIGNATURE" url="https://tezos.gitlab.io/michelson-reference/#instr-CHECK_SIGNATURE"/>

## Key Hash

<Snippet syntax={SYNTAX.PY}>

The type of key hashes in SmartPy is [sp.TKeyHash](/general/types#key_hash).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="key_hash" url="https://tezos.gitlab.io/michelson-reference/#type-key_hash"/>.

See reference [Baking Swap](https://smartpy.io/ide?template=bakingSwap.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of key hashes in SmartTS is [TKey_hash](/general/types#key_hash).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="key_hash" url="https://tezos.gitlab.io/michelson-reference/#type-key_hash"/>.

</Snippet>


### Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.key_hash(s)`** <br />
A literal key hash is of type [sp.TKeyHash](/general/types#key_hash) where `s` is a Python string.

#### Example

```python
keyHash = sp.key_hash("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" as TKey_hash`** <br />
A literal key hash is of type [sp.TKey_hash](/general/types#key_hash).
_
#### Example

```typescript
const keyHash: TKey_hash = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
```

</Snippet>

### Operations

<Snippet syntax={SYNTAX.PY}>

**`sp.hash_key(key)`** <br />
Compute the Base58Check of a public key.

It expects a single argument of type [sp.TKey](/general/types#key), and returns a value of type [sp.TKeyHash](/general/types#key_hash).

#### Example

```python
hashKey = sp.hash_key(sp.key("edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav"))
```

</Snippet>


<Snippet syntax={SYNTAX.TS}>

**`Sp.hashKey(key)`** <br />
Compute the Base58Check of a public key.

It expects a single argument of type [TKey](/general/types#key), and returns a value of type [TKey_hash](/general/types#key_hash).

#### Example

```typescript
const keyHash: TKey_hash = Sp.hashKey("edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav");
```

</Snippet>

<MichelsonDocLink placeholder="HASH_KEY" url="https://tezos.gitlab.io/michelson-reference/#instr-HASH_KEY"/>
<br/>
<br/>


<Snippet syntax={SYNTAX.PY}>

**`sp.set_delegate(baker)`** <br />
Set or unset an optional `baker` of type [sp.TOption](/general/types#option)([sp.TKeyHash](/general/types#key_hash)).<br/>
In tests, a contract's baker is accessible through the `<contract>.baker` field.

#### Example

```python
# Set the delegation
sp.set_delegate(sp.some("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"))
# Unset the delegation
sp.set_delegate(sp.none)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.setDelegate(baker)`** <br />
Set or unset an optional `baker` of type [TOption](/general/types#option)<[TKey_hash](/general/types#key_hash)>.<br/>
In tests, a contract's baker is accessible through the `<contract>.baker` field.

#### Example

```typescript
// Set the delegation
Sp.setDelegate(Sp.some("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"))
// Unset the delegation
Sp.setDelegate(Sp.none)
```
</Snippet>

<MichelsonDocLink placeholder="SET_DELEGATE" url="https://tezos.gitlab.io/michelson-reference/#instr-SET_DELEGATE"/>
<br/>
<br/>

<Snippet syntax={SYNTAX.PY}>

**`sp.implicit_account(<key_hash>)`** <br />
Implicit accounts are contracts which always have type [sp.TUnit](/general/types#unit).

The instruction above converts a value of type [sp.TKeyHash](/general/types#key_hash) into [sp.TContract](/general/types#contract)([sp.TUnit](/general/types#unit)).

#### Example

```python
contract = sp.implicit_account(sp.key_hash("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.implicitAccount(<key_hash>)`** <br />
Implicit accounts are contracts which always have type [TUnit](/general/types#unit).

The instruction above converts a value of type [TKeyHash](/general/types#key_hash) into [TContract](/general/types#contract)([TUnit](/general/types#unit)).

#### Example

```typescript
const contract: TContract<TUnit> = Sp.implicitAccount("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx")
```

</Snippet>

<MichelsonDocLink placeholder="IMPLICIT_ACCOUNT" url="https://tezos.gitlab.io/michelson-reference/#instr-IMPLICIT_ACCOUNT"/>
<br/>
<br/>


<Snippet syntax={SYNTAX.PY}>

**`sp.voting_power(key_hash)`** <br />
Return the voting power of a given contract. This voting power coincides with the weight of the contract in the voting listings (i.e., the rolls count) which is calculated at the beginning of every voting period.

Input Type: [sp.TKeyHash](/general/types#key_hash)
Output Type: [sp.TNat](/general/types#nat)

#### Example

```python
hashKey = sp.key_hash("tz1eWtg7YQb5iLX2HvrHPGbhiCQZ8n98aUh5")
votingPower = sp.voting_power(hashKey)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`sp.votingPower(key_hash)`** <br />
Return the voting power of a given contract. This voting power coincides with the weight of the contract in the voting listings (i.e., the rolls count) which is calculated at the beginning of every voting period.

Input Type: [TKey_hash](/general/types#key_hash)
Output Type: [TNat](/general/types#nat)

#### Example

```typescript
const keyHash: TKey_hash = "tz1eWtg7YQb5iLX2HvrHPGbhiCQZ8n98aUh5";
const votingPower: TNat = Sp.votingPower(keyHash)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="VOTING_POWER" url="https://tezos.gitlab.io/michelson-reference/#instr-VOTING_POWER"/>

## Secret Key

<Snippet syntax={SYNTAX.PY}>

The type of secret keys in SmartPy is `sp.TSecretKey`.<br/>
There is no corresponding type in Michelson.

Secret keys are used in tests.
See [Cryptography in Test Scenarios](/scenarios/cryptography#cryptography) and [Signatures](/types/signatures).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of secret keys in SmartTS is `TSecretKey`.<br/>
There is no corresponding type in Michelson.

Secret keys are used in tests.
See [Cryptography in Test Scenarios](/scenarios/cryptography#cryptography) and [Signatures](/types/signatures).

</Snippet>
