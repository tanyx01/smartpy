# Options

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

Optional values in SmartPy are of type [sp.TOption](/general/types#option)(`t`).

The corresponding type in Michelson is <MichelsonDocLink placeholder="option" url="https://tezos.gitlab.io/michelson-reference/#type-option"/>. They represent values of type `t` or nothing.

Optional values are useful for accommodating missing data: e.g. if your contract has an optional expiry date, you can add a field `expiryDate = sp.none` to the constructor. Then, if you want to set the expiry date, you write `expiryDate = sp.some(sp.timestamp(1571761674))`.

Conversely, if you want to unset it again, you write `expiryDate = sp.none`. SmartPy automatically infers the type [sp.TOption](/general/types#option)([sp.TTimestamp](/general/types#timestamp)) for `x`, so you don't have to make it explicit.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Optional values in SmartPy are of type [TOption](/general/types#option)(`t`).

The corresponding type in Michelson is <MichelsonDocLink placeholder="option" url="https://tezos.gitlab.io/michelson-reference/#type-option"/>. They represent values of type `t` or nothing.

Optional values are useful for accommodating missing data: e.g. if your contract has an optional expiry date, you can add a field `const expiryDate: TOption<TTimestamp> = Sp.none` to the constructor. Then, if you want to set the expiry date, you write `const expiryDate: TOption<TTimestamp> = Sp.some(1571761674)`.

Conversely, if you want to unset it again, you write `const expiryDate: TOption<TTimestamp> = Sp.none`. SmartPy automatically infers the type [TOption](/general/types#option)([TTimestamp](/general/types#timestamp)) for `x`, so you don't have to make it explicit.

</Snippet>

### Define an empty optional value

<Snippet syntax={SYNTAX.PY}>

**`sp.none`** <br />
Define an optional value not containing any element.

#### Example

```python
optionalValue = sp.none
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.none`** <br />
Define an optional value not containing any element.

#### Example

```typescript
const optionalValue: TOption<TNat> = Sp.none;
```

</Snippet>

<MichelsonDocLink placeholder="NONE" url="https://tezos.gitlab.io/michelson-reference/#instr-NONE"/>

### Define an optional value

<Snippet syntax={SYNTAX.PY}>

**`sp.some(<expr>)`** <br />
Define an optional value containing an element `expr`.

#### Example

```python
optionalValue = sp.some(1)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.some(<expr>)`** <br />
Define an optional value containing an element `expr`.

#### Example

```typescript
const optionalValue: TOption<TNat> = Sp.some(1)
```

</Snippet>

<MichelsonDocLink placeholder="SOME" url="https://tezos.gitlab.io/michelson-reference/#instr-SOME"/>

### Check if an optional value exists

<Snippet syntax={SYNTAX.PY}>

**`expr.is_some()`** <br />
Check that an optional value contains an element, i.e., checks whether it is of the form `sp.some(...)`.

#### Example

```python
sp.if expr.is_some():
    # ...
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr.isSome()`** <br />
Check that an optional value contains an element, i.e., checks whether it is of the form `Sp.some(...)`.

#### Example

```typescript
if (expr.isSome()) {
    // ...
}
```

</Snippet>

<MichelsonDocLink placeholder="IF_NONE" url="https://tezos.gitlab.io/michelson-reference/#instr-IF_NONE"/>

### Get optional value

<Snippet syntax={SYNTAX.PY}>

**`expr.open_some(message = None)`** <br />
If `expr` is equal to `sp.some(x)`, return `x`; otherwise fail. `message` is the optional error raised in case of error.

#### Example

```python
# expr is of type sp.TOption<sp.TNat>
value = expr.open_some(message = "Value is None")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr.openSome(<message>)`** <br />
If `expr` is equal to `Sp.some(x)`, return `x`; otherwise fail. `message` is the optional error raised in case of error.

#### Example

```typescript
// expr is of type TOption<TNat>
const value: TNat = expr.openSome("Value is None")
```

</Snippet>

<MichelsonDocLink placeholder="IF_NONE" url="https://tezos.gitlab.io/michelson-reference/#instr-IF_NONE"/>
