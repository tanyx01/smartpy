# Michelson Code Inlining

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

See reference [Inline Michelson](https://smartpy.io/ide?template=inlineMichelson.py) template.

## Methods

### michelson

Inline arbitrary Michelson code.

`types_in` and `types_out` are lists of types, and `code` is some Michelson code using the [micheline](https://tezos.gitlab.io/shell/micheline.html) syntax.

Currently, in practice, only singleton **types_out** are supported.

**`sp.michelson(code, types_in, types_out)`**

```python
self.data.x = sp.michelson("ADD;", [sp.TInt, sp.TInt], [sp.TInt])(15, 16)
self.data.y = sp.michelson("DUP; DIG 2; ADD; MUL;", [sp.TInt, sp.TInt], [sp.TInt])(15, 16)
```

### lambda_michelson

A special case of `sp.michelson` where the code represents a lambda.

**`sp.lambda_michelson(code, source = None, target = None)`**

It is of type `sp.TLambda(type_in, type_out)`

```python
self.data.f = sp.lambda_michelson("DUP; PUSH int 2; ADD; MUL;", sp.TNat, sp.TInt)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress...
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
