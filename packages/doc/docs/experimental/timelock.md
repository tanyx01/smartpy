# Timelock

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

The feature documentation can be found [here](https://tezos.gitlab.io/alpha/timelock.html#timelock).

<Snippet syntax={SYNTAX.PY}>

See reference [Timelock](https://smartpy.io/ide?template=timelock.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

See reference [Timelock](https://smartpy.io/ts-ide?template=Timelock.ts) template.

</Snippet>

## Types

### Chest

Represents timelocked arbitrary bytes with the necessary public parameters to open it.<br/>

<Snippet syntax={SYNTAX.PY}>

See [sp.TChest](/general/types#chest) type.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

See [TChest](/general/types#chest) type.

</Snippet>


### Chest key

Represents the decryption key, alongside with a proof that the key is correct.<br/>

<Snippet syntax={SYNTAX.PY}>

See [sp.TChest_key](/general/types#chest_key) type.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

See [TChest_key](/general/types#chest_key) type.

</Snippet>

## Literals

### Chest key

<Snippet syntax={SYNTAX.PY}>

**`sp.chest("0x1234")`**

Create a literal of type [sp.TChest](/general/types#chest).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

In progress ...

</Snippet>

### Chest

<Snippet syntax={SYNTAX.PY}>

**`sp.chest_key("0x1234")`**

Create a literal of type [sp.TChest_key](/general/types#chest_key).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

In progress ...

</Snippet>

## Operations

### Opening the chest

<Snippet syntax={SYNTAX.PY}>

**`sp.open_chest(<chest_key>, <chest>, <time>)`**

The function takes a `<chest_key>` of type [sp.TChest_key](/general/types#chest_key), a `<chest>` of type [sp.TChest](/general/types#chest_key), a `<time> constant` of type [sp.TNat](/general/types#nat) and returns a value of type [sp.TVariant](/general/types#variant).

**Understanding the result:**

When the chest is successfully opened, the output is a `Left variant`. If something fails, the result is a `Right variant`.

`Left variant` is of type [sp.TBytes](/general/types#bytes) and is the underlying plaintext.

`Right variant` is of type [sp.TBool](/general/types#bool) and indicates that the chest or the chest key is malicious.

#### Example

```python
chest_key = sp.chest_key(
    "0x8caa8f96bd95b5d0bbfbb5b7c2d2c5f0ada0c3ced6b0cdbfb7a2ae9a8695d0eab59e89efd29d99ea999996c48efaaed3fcd38ff7ba8ffe86e19eddf9eae0a3bbddcba28acbbca7baaa98c7d99f80e7bbea91daa39ee592ebd58de0ac9efbbaaacdf8bcbd83e7e4d1da9aacecadcaef9ba7b8f1d4d6ef9dd8c7efe2e0ffe4ec95a0d3b6f9aeba9b831cdaf1b2be9493c3f58a8eeb8fbac5f88a81f6bb89d0f7c3d0b68bcefb81cfb09aaaf686cfcafba6fdee8edf8cbaa4bec6839ab5d4a3e9cdbaa1b4ef88bdc2b892e3c5bafcdbe7f5c4e3a7e38fd6dceba29981a2ff9dc5cfd5a59ba8aaef98afa0c8cbfc919893f1c487cc95deb7e6d5c3a5f08a86d089f2fec8d8eff9f1f6ebf489fce5a2e6caccf95d"
)
chest = sp.chest(
    "0xbcdabdf6e0f9c4ff9883ba9adcd5c0aea7cd909199bbcba0a7878788dba08aecbc848bb9b3cb859df2a2ddd3acb7e39ee38fe2d2ae85c7caf58da4e984b5f6d8c39dd4bd0ab1ca8ef3ff8ec1cc8997bcb0f185c3f49ef98dcae695bfbabde8e0bda286c2e78f809bb98fa2e0acd5f18ad084c2a7f28ff9dcb49ef4c0b590e8c2cc99e493a3c2a7ef85e6bdb6c2ba87fd94b7faacc1c082d3fafef69bf9c3cd9ab3fb9ccaf7e0c58efbe1bab7fb8dc3a5e7f9e4f7d1d9dbd38ca891b994b5e09ab7ccad96d0d5ee8da5bce2b2c08d01563b4da16b60748081074010aee6b6b0f60c3a81e5afff3c000000211850a70bad6a333e9806ec3a2389e392181abd71f09f37447c226d8e4a6ab275aa"
)
time  = 2000
result = sp.open_chestchest_key, chest, time)

with result.match_cases() as arg:
    with arg.match("Left") as success:
        # Left variant (Success)
        self.data.success = success
    with arg.match("Right") as failure:
        # Left variant (Something went wrong)
        self.data.failure = failure
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

In progress ...

</Snippet>
