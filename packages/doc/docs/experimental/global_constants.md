# Global Constants

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

The feature documentation can be found [here](https://tezos.gitlab.io/alpha/global_constants.html).

Global constants are placeholders that reference valid `Micheline` stored in a global table of constants. The `Micheline` replaces the placeholder before the contract gets parsed and type-checked.

This feature allows contracts to reuse existing code stored as constants, resulting in contracts of reduced size.

<Snippet syntax={SYNTAX.PY}>

See reference [Constants](https://smartpy.io/ide?template=constants.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

See reference [Constants](https://smartpy.io/ts-ide?template=Constants.ts) template.

</Snippet>

<Snippet syntax={SYNTAX.PY}>

:::caution

Note that using the [sp.unpack](/advanced/pack_unpack) operation to construct a lambda or
[sp.create_contract](/types/contracts_addresses/#create-a-new-contract) instruction that contains a constant reference is
not possible.

:::

</Snippet>

<Snippet syntax={SYNTAX.TS}>

:::caution

Note that using the [Sp.unpack](/advanced/pack_unpack) operation to construct a lambda or
[Sp.createContract](/types/contracts_addresses/#create-a-new-contract) instruction that contains a constant reference is
not possible.

:::

</Snippet>

## Using a constant as a value

<Snippet syntax={SYNTAX.PY}>

**`sp.constant("expr...", t = <type>)`**

Where `expr...` is the `hash` of the constant that points to some `Micheline` code stored on-chain, and `t` is an optional argument that explicitly informs the compiler about the constant type after it gets expanded.

```python
# The storage field `x` will be set to the value stored as constant `expr...`
self.data.x = sp.constant("expr...", t = sp.TNat)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.constant<t>("expr...")`**

Where `expr...` is the `hash` of the constant that points to some `Micheline` code stored on-chain, and `t` is an optional type that explicitly informs the compiler about the constant type after it gets expanded.

```typescript
// The storage field `x` will be set to the value stored as constant `expr...`
this.storage.x = Sp.constant<TNat>("expr...")
```

</Snippet>

## Constants in test scenarios

<Snippet syntax={SYNTAX.PY}>

**`<scenario>.prepare_constant_value(<some_value>, hash = <"expr...">)`**

Where **`<scenario>`** is a [test scenario](/scenarios/testing/#registering-and-displaying-contracts), **`<some_value>`** is the value that the constant will point to, and **`hash`** is an optional argument that, if specified, will override the real hash of the constant.

```python
@sp.add_test(name = "Testing with constants")
def test():
    # Create a scenario
    scenario = sp.test_scenario()
    # Create a constant from a value
    constant = scenario.prepare_constant_value(100)
    # Print the constant
    scenario.show(constant)
```

</Snippet>


<Snippet syntax={SYNTAX.TS}>


**`Scenario.prepareConstantValue(<some_value>, <constant_hash>)`**

Where **`<some_value>`** is the value that the constant will point to, and **`<constant_hash>`** is an optional argument that, if specified, will override the real hash of the constant.

```typescript
Dev.test({ name: "Testing with constants" }, () => {
    // Create a constant from a value
    const constant = Scenario.prepareConstantValue(100);
    // Print the constant
    Scenario.show(constant);
});
```

</Snippet>

#### Result

![Prepare constant value](/img/examples/prepare_constant_value.png)

## Complete example

The code below uses a constant `expruDpiV1FEzeYhWAQBVeCSEYMQbnUovq9Cbddqy6xLMeEe1zkGRA` to look up a string value.

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

class Constants(sp.Contract):
    def __init__(self, storage, constant1, constant2):
        self.constant1 = constant1
        self.constant2 = constant2
        self.init(storage)

    @sp.entry_point
    def ep(self, x):
        # Compute the new value from values stored as constants
        self.data = self.constant1(x) + self.constant2

@sp.add_test(name = "Test constants")
def test():
    # Create a scenario
    scenario = sp.test_scenario()

    # Create some lambda
    def some_lambda(x):
        sp.result(x * 10)

    # Create 2 constants from their values
    constant1 = scenario.prepare_constant_value(some_lambda)
    constant2 = scenario.prepare_constant_value(100)
    # Print the constants
    scenario.show(constant1)
    scenario.show(constant2)

    # Create contract
    contract = Constants(0, constant1 = constant1, constant2 = constant2)
    scenario += contract

    # Call the contract
    contract.ep(10)

sp.add_compilation_target(
    "constants_compilation",
    Constants(
        0,
        constant1 = sp.constant("c1", t = sp.TLambda(sp.TNat, sp.TNat)),
        constant2 = sp.constant("c2", t = sp.TNat)
    )
)
```

</Snippet>


<Snippet syntax={SYNTAX.TS}>

```typescript
class Constants {
    // The constant points to a lambda
    constant1 = Sp.constant<TLambda<TNat, TNat>>(
        'expru54tk2k4E81xQy63P6x3RijnTz51s2m7BV7pr3fDQH8YDqiYvR'
    )
    // The constant points to a nat
    constant2 = Sp.constant<TNat>(
        'exprtX5EDHZJaBbSmnq14wSwcadxvXJDk5Du7eUpbmMgQFPeGsaV2M'
    )

    constructor(public storage: TNat) { }

    @EntryPoint
    ep(x: TNat) {
        // Compute the new value from values stored as constants
        this.storage = this.constant1(x) + this.constant2;
    }
}

Dev.test({ name: 'Test constants' }, () => {
    // Define some lambda
    const someLambda = (x: TNat): TNat => {
        return x * 10;
    }
    // Create 2 constants from their values
    const constant1 = Scenario.prepareConstantValue(someLambda);
    const constant2 = Scenario.prepareConstantValue(100);
    // Print the constants
    Scenario.show(constant1);
    Scenario.show(constant2);

    // Create contract
    const contract = Scenario.originate(new Constants(0));

    // Call contract
    Scenario.transfer(contract.ep(10));
});

Dev.compileContract('constants_compilation', new Constants(0));
```

</Snippet>
