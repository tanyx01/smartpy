# Project Management

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import SelectPython from '@site/src/components/Syntax/SelectPython';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


### Installation

<Snippet syntax={SYNTAX.PY}>

`Dependencies`

|    Requirement    |        Version        |
|:-----------------:|:---------------------:|
| Python            | >= 3.7                |
| NodeJS            | >= 10.x               |

Download the [CLI](/cli).

```shell
sh <(curl -s https://smartpy.io/cli/install.sh)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`Dependencies`

|    Requirement    |        Version        |
|:-----------------:|:---------------------:|
| NodeJS            | >= 10.x               |


Bootstrap a project for `typescript` syntax.

<Tabs
    defaultValue="npx"
    values={[
        {label: 'npx', value: 'npx'},
        {label: 'npm', value: 'npm'},
        {label: 'yarn', value: 'yarn'}
    ]}
>
<TabItem value="npx">

```shell
npx create-smartpy-project@latest install <project-directory> --typescript
```
</TabItem>
<TabItem value="npm">

```shell
npm init smartpy-project@latest install <project-directory> --typescript
```

</TabItem>
<TabItem value="yarn">

```shell
yarn create smartpy-project@latest install <project-directory> --typescript
```

</TabItem>

</Tabs>

The boilerplate can then be updated anytime by running:

```shell
# Inside <project-directory>
npm run update
```

</Snippet>

### Directory structure


<Snippet syntax={SYNTAX.PY}>

```shell
smartpy-cli
│
├── templates
|   |
│   └── welcome.py              # A very simple template
│
├── ...
└── SmartPy.sh                  # Command line interface
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```shell
<project-directory>
│
├── build                       # Artifacts (compilations and tests)
│   │
│   ├── compilation
│   │   │
│   │   ├── Template.contract   # Contract compilation folder
│   │   └── ...
│   │
│   └── test
│       │
│       ├── Template.contract   # Contract testing folder
│       └── ...
│
├── src                         # Location for contract files (*.contract.ts)
|   |
│   ├── SubModule.ts            # Sub module example (it is imported into Template.contract.ts)
│   └── Template.contract.ts    # Template contract
│
├── templates                   # Various contract templates
|   |
│   ├── FA1_2.contract.ts       # FA1.2 template
│   └── FA2.contract.ts         # FA template
|
├── .eslintrc.js                # eslint configuration
├── .prettierrc.js              # prettier configuration
├── package.json
└── tsconfig.json               # It should not be modified.
```

</Snippet>

### Write a contract

<Snippet syntax={SYNTAX.PY}>

```shell
# Create a folder to store the contracts
mkdir ~/smartpy-cli/contracts
# Create a folder to store contract compilation artifacts
mkdir ~/smartpy-cli/compilation
# Create a folder to store contract test artifacts
mkdir ~/smartpy-cli/test

# Create a contract file
touch ~/smartpy-cli/contracts/my_contract.py
```

Edit `my_contract.py` and add your own logic or use the example below:

```python
import smartpy as sp

class MyContract(sp.Contract):
  def __init__(self, value):
      self.init(storedValue = value)

  @sp.entry_point
  def store(self, value):
    # Store a new value
    self.data.storedValue = value

@sp.add_test(name = "MyContract")
def test():
  scenario = sp.test_scenario()
  contract = MyContract(1)
  scenario += contract
  contract.store(2)

# A a compilation target (produces compiled code)
sp.add_compilation_target("my_contract_compiled", MyContract(1))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>


```shell
# Create a contract file
touch <project-directory>/src/MyContract.contract.ts
```

Edit `MyContract.contract.ts` and add your own logic or use the example below:

```typescript
interface TStorage {
    storedValue: TNat;
}

@Contract
export class MyContract {
    storage: TStorage = {
        storedValue: 1,
    };

    @EntryPoint
    store(value: TNat): void {
        // Store a new value
        this.storage.storedValue = value;
    }
}

Dev.test({ name: 'MyContract' }, () => {
    const c1 = Scenario.originate(new MyContract());
    Scenario.transfer(c1.store(2));
});

Dev.compileContract('my_contract_compiled', new MyContract());
```


</Snippet>

### Running compilations

<Snippet syntax={SYNTAX.PY}>

The command below will run the compilation targets in the file `my_contract.py` and store the generated compilation artifacts at `~/smartpy-cli/compilation`.

```shell
~/smartpy-cli/SmartPy.sh compile ~/smartpy-cli/contracts/my_contract.py ~/smartpy-cli/compilation
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The command below will iterate over all files in the `src` directory with postfix `*.contract.ts` and run all compilation targets.

```shell
npm run compile
```

</Snippet>

### Running tests

<Snippet syntax={SYNTAX.PY}>

The command below will run the test targets in the file `my_contract.py` and store the generated test artifacts at `~/smartpy-cli/test`.

```shell
~/smartpy-cli/SmartPy.sh test ~/smartpy-cli/contracts/my_contract.py ~/smartpy-cli/compilation
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The command below will iterate over all files in the `src` directory with postfix `*.contract.ts` and run all test targets.

```shell
npm run test
```

</Snippet>


### Deploy contracts

<Snippet syntax={SYNTAX.PY}>

By default, the originator will use a faucet account.<br/>
But you can provide your own private key by providing the argument `--private-key`

```shell
~/smartpy-cli/SmartPy.sh originate-contract --code ~/smartpy-cli/compilation/my_contract/<...>_contract.tz --storage ~/smartpy-cli/compilation/my_contract/<...>_storage.tz --rpc https://granadanet.smartpy.io --private-key <edsk...>
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

By default, the originator will use a faucet account.<br/>
But you can provide your own private key by providing the argument `--private-key`

```shell
npm run originate -- --code build/compilation/<...>_contract.tz --storage build/compilation/<...>_storage.tz --rpc https://granadanet.smartpy.io --private-key <edsk...>
```

</Snippet>
