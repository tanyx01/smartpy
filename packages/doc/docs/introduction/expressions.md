# Expressions

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

Like most languages, SmartPy has expressions.

For example, `self.data.x` represents the contract storage field **x** and `2` represents the number **2**, whereas <br/> `self.data.x + 2` represents their sum.

Inside a contract, when we write:

```python
y = self.data.x + 2
```

We declare `y` as an alias the SmartPy expression `self.data.x + 2`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Like most languages, SmartTS has expressions.

For example, `this.storage.data.x` represents the contract storage field **x** and `2` represents the number **2**, whereas <br/> `this.storage.x + 2` represents their sum.

Inside a contract, when we write:

```typescript
const y: TNat = this.storage.x + 2;
```

We declare `y` as an alias of expression `this.storage.x + 2`.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
