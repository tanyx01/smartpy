# Types

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

Types are usually automatically inferred and not explicitly needed. SmartPy types are all of the form `sp.T<TypeName>`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

SmartTS types are all of the form `T<TypeName>`.

</Snippet>


Have a look at the [Typing](/general/types) section.
