# Contracts

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

A contract consists of a state together with a single or multiple entry points.

<Snippet syntax={SYNTAX.PY}>

It is a class definition that inherits from `sp.Contract`. The constructor (`__init__`) makes a call to `self.init` and initializes fields that make up the contract’s state.

```python
class Hello(sp.Contract):
    def __init__(self):
        self.init(x = 0)

    @sp.entry_point
    def set_x(newX):
        self.data.x = newX

    @sp.entry_point
    def check_largerthan_x(p):
        sp.verify(p > self.data.x)
```

- **`self.init(..)`**

    Alias for `self.init_storage(arg = None, **kwargs)`

- <b>

  `self.init_storage(arg = None, **kwargs)`

  </b>

    Set the storage of a contract with no initial storage. `arg` and `kwargs` cannot be both present. <br/>
    `kwargs` is a dictionary of fields that are added to the storage (or removed when value is `None`).

    There are two different ways to call `self.init_storage` in a contract:
  - on a single value `self.init_storage(arg)` where `arg` is a SmartPy expression;
  - on a list of fields with values, `self.init_storage(**kwargs)` where `kwargs` is a dictionary.


- <b>

  `self.update_initial_storage(arg = None, **kwargs)`

  </b>

    Update the storage of a contract. `arg` and `kwargs` cannot be both present. `kwargs` is a dictionary of fields that are added to the storage (or removed when value is `None`).

    `self.update_initial_storage(e)` replaces the former storage while `self.update_initial_storage(**kwargs)` expects the former storage to be a record and adds, changes or removes (when value is `None`) former fields. This is very useful when we use complex inheritance patterns.

```python
class Hello(sp.Contract):
    def __init__(self):
        """ Alias for init_storage. """
        self.init(x = 0)
```

```python
class Hello(sp.Contract):
    def __init__(self):
        """ Initialize the storage to a record containing
            a single field x of value 0. """
        self.init_storage(x = 0)
```

```python
class Hello(sp.Contract):
    def __init__(self):
        """ It's allowed to call update_initial_storage even if no storage is defined. """
        self.update_initial_storage(x = 0)
```

```python
class Hello(sp.Contract):
    def __init__(self):
        self.init_storage(x = 0, y = 1)
```

```python
class Hello(sp.Contract):
    def __init__(self):
        """ Define x then add y. """
        self.init_storage(x = 0)
        self.update_initial_storage(y = 1)
```

```python
class Hello(sp.Contract):
    def __init__(self):
        """ Define x and t then remove x and add y. """
        self.init_storage(x = 0, t = 12)
        self.update_initial_storage(x = None, y = 1)
```
</Snippet>

<Snippet syntax={SYNTAX.TS}>

It is a class definition that is annotated by `@Contract`.

```typescript
interface TStorage {
    x: TNat
}

@Contract
class Hello {
    storage: TStorage = {
        x: 0
    };

    @EntryPoint
    setValue(newX: TNat) {
        this.storage.x = newX
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Setting storage type

<Snippet syntax={SYNTAX.PY}>

```python
class Hello(sp.Contract):
    def __init__(self):
        self.init_type(sp.TRecord(x = sp.TNat))
        self.init(x = 0)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The property `storage` defines the initial storage, it can be defined from the contract `constructor` when the initial storage needs to be specified from compilation/test targets.

Interfaces `TStorage` and `StorageType` are storage type definitions.

```typescript
interface TStorage {
    x: TNat
}

@Contract
class Hello {
    storage: TStorage = {
        x: 0
    };
}

// Or a more flexible approach

type StorageType = {
    x: TNat
}

@Contract
class World {
    constructor(public storage: StorageType = { x: 0 }) {}
}

Dev.compileContract("compilation", new World({ x: 10 }))
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

See [Setting a type constraint](/general/types#setting-a-type-constraint)
