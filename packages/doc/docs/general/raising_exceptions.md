# Raising Exceptions

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

Once an exception is raised, it cannot be caught.

:::caution
Error messages may take a lot of space in smart contracts.

Have a look at [Exception Optimization Levels](/general/flags#exception-optimization-levels).
:::

Exceptions cannot be caught, the whole operation is backtracked if an exception occurs.


### FAILWITH

<Snippet syntax={SYNTAX.PY}>

`sp.failwith` aborts the current transaction and raises a message of arbitrary type.

```python
@sp.entry_point
def ep(self):
    sp.if self.data.x > 10:
        sp.failwith("x is bigger than 10")
```

Besides `sp.failwith`, `sp.verify` and `sp.verify_equal`, exceptions can also be raised by other constructions:

- Accessing fields `my_map[x]`

    The exception raised is a pair containing the `(x, my_map)`.

- Opening variants `.open_some(<message>)`

    This may fail with `sp.unit`.

- Dividing by zero `1 / 0`

- Variable step in a range which is 0 `sp.for i in sp.range(1, 10, step = 0)`

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`Sp.failWith` aborts the current transaction and raises a message of arbitrary type.

```typescript
@EntryPoint
ep() {
    if (this.storage.x > 10) {
        Sp.failWith("x is bigger than 10");
    }
}
```

Besides `Sp.failWith`, `Sp.verify` and `Sp.verifyEqual`, exceptions can also be raised by other constructions:

- Accessing fields `my_map.get(x)`

    The exception raised is a pair containing the `(x, my_map)`.

- Opening variants `.openSome(<message>)`

    This may fail with `Sp.unit`.

- Dividing by zero `1 / 0`

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="FAILWITH" url="https://tezos.gitlab.io/michelson-reference/#instr-FAILWITH"/>
