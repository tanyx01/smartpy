# Variables

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

Local SmartPy variables can be defined as follows: `x = sp.local("x", 0)`

The first argument to `sp.local` is a string that will be used in error messages. It is advisable to use the same name that is used on the left of **=**.

Local variable values can be accessed to and updated with the **.value** field: `x.value = 1, x.value = 2 * x.value + 5`.

Local variables are necessary when the variable is mutated inside `if's` and `loop's`.

Local variables can be markd as immutable by specifying `mutable = False`.

:::caution
Local SmartPy variables are different from Python variables. The latter cannot be updated during contract execution.
:::

As an example, here is how we can compute a square root.

```python
@sp.entry_point
def squareRoot(self, x):
    sp.verify(x >= 0)
    y = sp.local('y', x)
    sp.while y.value * y.value > x:
        y.value = (x // y.value + y.value) // 2
    sp.verify((y.value * y.value <= x) & (x < (y.value + 1) * (y.value + 1)))
    self.data.value = y.value
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Variables can be defined as follows: `let x: TNat = 0`

```typescript
<var | let | const> <variable name>[: <type>] = <value>
```

**`var | let`** is used to define mutable variables.<br/>
**`const`** is used to define immutable variables.

As an example, here is how we can compute a square root.

```typescript
@EntryPoint
squareRoot(x: TNat) {
    Sp.verify(x >= 0)
    let y = x
    while (y * y > x) {
        y = x / y + y / 2
    }
    Sp.verify((y * y <= x) && (x < (y + 1) * (y + 1)))
    this.storage.value = y
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>
