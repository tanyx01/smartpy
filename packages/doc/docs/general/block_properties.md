# Global Properties

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';


<Snippet syntax={SYNTAX.TS}>

See Reference [BlockProperties](https://smartpy.io/ts-ide?template=BlockProperties.ts) template.

</Snippet>

## sender, source

Consider a scenario involving two contracts, a receiver and a proxy. The contract proxy, transfers any amount received in a call to receiver. Assume a third contract bootstrap2 creates a transaction to the proxy contract sending `99 ꜩ` . Then proxy creates an internal transaction that calls receiver:

:::note

![SENDER_SOURCE](/img/examples/sender-source.svg)

:::

<Snippet syntax={SYNTAX.PY}>

`sp.sender` and `sp.source` are of type [sp.TAddress](/general/types#address)

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(sender = sp.none, source = sp.none)

    @sp.entry_point
    def entry_point_1(self):
        self.data.sender = sp.some(sp.sender)
        self.data.source = sp.some(sp.source)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`Sp.sender` and `Sp.source` are of type [TAddress](/general/types#address)

```typescript
@Contract
class MyContract {
    storage: {
        sender: TOption<TAddress>;
        source: TOption<TAddress>;
    } = {
        sender: Sp.none,
        source: Sp.none
    }

    @EntryPoint
    entry_point() {
        this.storage.sender = Sp.some(Sp.sender)
        this.storage.source = Sp.some(Sp.source)
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="SENDER" url="https://tezos.gitlab.io/michelson-reference/#instr-SENDER"/> and <MichelsonDocLink placeholder="SOURCE" url="https://tezos.gitlab.io/michelson-reference/#instr-SOURCE"/>


## Get block timestamp

Push the minimal injection time for the current block, namely the block whose validation triggered this execution. The minimal injection time is 60 seconds after the timestamp of the predecessor block. This value does not change during the execution of the contract.

<Snippet syntax={SYNTAX.PY}>

It is of type [sp.TTimestamp](/general/types#timestamp).

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(timestamp = sp.none)

    @sp.entry_point
    def entry_point_1(self):
        self.data.timestamp = sp.some(sp.now)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

It is of type [TTimestamp](/general/types#timestamp).

```typescript
@Contract
class MyContract {
    storage: TOption<TTimestamp> = Sp.none;

    @EntryPoint
    entry_point() {
        this.storage = Sp.some(Sp.now)
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="NOW" url="https://tezos.gitlab.io/michelson-reference/#instr-NOW"/>

## Get block level

Gives the block level that included the operation.

<Snippet syntax={SYNTAX.PY}>

It is of type [sp.TNat](/general/types#nat).

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(level = sp.none)

    @sp.entry_point
    def entry_point_1(self):
        self.data.level = sp.some(sp.level)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

It is of type [TNat](/general/types#nat).

```typescript
@Contract
class MyContract {
    storage: TOption<TNat> = Sp.none;

    @EntryPoint
    entry_point() {
        this.storage = Sp.some(Sp.level)
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="LEVEL" url="https://tezos.gitlab.io/michelson-reference/#instr-LEVEL"/>

## Get chain identifier

This instruction pushes on the stack the identifier of the chain on which the smart contract is executed.

The chain identifier is designed to prevent replay attacks between the main chain and the test chain forked during amendment exploration, where contract addresses and storages are identical.

The value is derived from the genesis block hash and will thus be different on the main chains of different networks and on the test chains on which the value depends on the first block of the test-chain.

<Snippet syntax={SYNTAX.PY}>

It is of type [sp.TChainId](/general/types#chain_id).

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(id = sp.none)

    @sp.entry_point
    def entry_point_1(self):
        self.data.id = sp.some(sp.chain_id)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

It is of type [TChain_id](/general/types#chain_id).

```typescript
@Contract
class MyContract {
    storage: TOption<TChain_id> = Sp.none;

    @EntryPoint
    entry_point() {
        this.storage = Sp.some(Sp.chainId)
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="CHAIN_ID" url="https://tezos.gitlab.io/michelson-reference/#instr-CHAIN_ID"/>

## Get total voting power

Return the total voting power of all contracts. The total voting power coincides with the sum of the rolls count of every contract in the voting listings. The voting listings is calculated at the beginning of every voting period.

<Snippet syntax={SYNTAX.PY}>

It is of type [sp.TNat](/general/types#nat).

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(total_voting_power = sp.none)

    @sp.entry_point
    def entry_point_1(self):
        self.data.total_voting_power = sp.some(sp.total_voting_power)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

It is of type [TNat](/general/types#nat).

```typescript
@Contract
class MyContract {
    storage: TOption<TNat> = Sp.none;

    @EntryPoint
    entry_point() {
        this.storage = Sp.some(Sp.totalVotingPower)
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="TOTAL_VOTING_POWER" url="https://tezos.gitlab.io/michelson-reference/#instr-TOTAL_VOTING_POWER"/>
