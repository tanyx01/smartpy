# Typing

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import MichelsonDocLink from '@site/src/components/MichelsonDocLink';

## Type Inference

<Snippet syntax={SYNTAX.PY}>

Just like in Python, most of the time there is no need to specify the type of an object in SmartPy. For a number of reasons (e.g., because SmartPy's target language, Michelson, requires types), each SmartPy expression does however need a type. Therefore SmartPy uses type inference in order to determine each expressions type.

In practice, this means that information about an expression is gathered according to its usage: for example, when somewhere in your contract you write `self.data.x == "abc"`, SmartPy will automatically determine and remember that `self.data.x` is a string.

Note that SmartPy types are distinct from Python types: `self.data.x == "abc"` has the Python type `sp.Expr` (simply because it is a SmartPy expression), whereas it has the SmartPy type `sp.TBool` (see below).

While most of the time the user will not write many types explicitly it is beneficial to at least have a basic understanding of what they are. This also helps understanding error messages better.

</Snippet>
<Snippet syntax={SYNTAX.TS}>

Note that Michelson types are distinct from Typescript types: `const value = 2` implicitly has the Typescript type `number`, where it can be either a `nat` or an `int` in Michelson. (See below: [nat](#nat), [int](#int))

Depending on how `value` is used, SmartML will be able to automatically determine if the value is being used as a `nat` or an `int`.

It is a best practice to always explicitly specify the type. (e.g., `const value: TNat = 2`)

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>



## Basic Types



### nat

<Snippet syntax={SYNTAX.PY}>

`sp.TNat` is the type of non-negative integer values.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`TNat` is the type of non-negative integer values.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

`nat` is the type of non-negative integer values.

</Snippet>

See [Integers](/types/integers) documentation.
<MichelsonDocLink placeholder="nat" url="https://tezos.gitlab.io/michelson-reference/#type-nat"/>

### int

<Snippet syntax={SYNTAX.PY}>

`sp.TInt` is the type of signed integer values.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`TInt` is the type of signed integer values.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

`int` is the type of signed integer values.

</Snippet>

See [Integers](/types/integers) documentation.
<MichelsonDocLink placeholder="int" url="https://tezos.gitlab.io/michelson-reference/#type-int"/>


### int or nat {#int_or_nat}
<Snippet syntax={SYNTAX.PY}>

`sp.TIntOrNat` is the type of integer values when SmartPy doesn't know yet which type is applicable, either `sp.TInt` or `sp.TNat`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`TIntOrNat` is the type of integer values when SmartTS doesn't know yet which type is applicable, either `TInt` or `TNat`.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

`int_or_nat` is the type of integer values when SmartML doesn't know yet which type is applicable, either `int` or `nat`.

</Snippet>

See [Integers](/types/integers) documentation.

### string
The type of string values.

The current version of Michelson restricts strings to be the printable subset of `7-bit ASCII`, namely characters with codes from within **[32, 126]** range, plus the following escape characters `\n`, `\\`, `\"`. Unescaped line breaks (both `\n` and `\r`) cannot appear in a string.

<Snippet syntax={SYNTAX.PY}>

```python
"", '', "Hello World", 'Hello World'
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TString = "Hello World"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
let value = "Hello World"
```

</Snippet>

See [Strings](/types/strings) documentation.
<MichelsonDocLink placeholder="string" url="https://tezos.gitlab.io/michelson-reference/#type-string"/>


### bool
The type of boolean values.

<Snippet syntax={SYNTAX.PY}>

```python
True, False
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TBool = true
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
let value = true
```

</Snippet>


See [Booleans](/types/booleans) documentation.
<MichelsonDocLink placeholder="bool" url="https://tezos.gitlab.io/michelson-reference/#type-bool"/>

### bytes
The type of serialized data.

Bytes are used for serializing data, in order to check signatures and compute hashes on them. They can also be used to incorporate data from the wild and untyped outside world.

<Snippet syntax={SYNTAX.PY}>

```python
sp.bytes("0x"), sp.bytes("0x00"), sp.bytes("0x0001"), sp.bytes("0xff"), etc.
```

See reference [Strings and Bytes](https://smartpy.io/ide?template=stringManipulations.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
type bytes_type = TBool

// value assignment
let value: bytes_type = "0x00"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Bytes](/types/bytes) documentation.
<MichelsonDocLink placeholder="bytes" url="https://tezos.gitlab.io/michelson-reference/#type-bytes"/>


### mutez

<Snippet syntax={SYNTAX.PY}>

The type of tez amounts in SmartPy is [sp.TMutez](/general/types#mutez).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of tez amounts in SmartTS is [TMutez](/general/types#mutez).

</Snippet>

Mutez (micro-Tez) are internally represented by 64-bit signed integers. These are restricted to prevent creating a negative amount of mutez. Instructions are limited to prevent overflow and mixing them with other numerical types by mistake. They are also mandatorily checked for overflows.

<Snippet syntax={SYNTAX.PY}>

```python
sp.mutez(1000000)
# or
sp.tez(1)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
type mutez_type = TMutez

// value assignment
let value: mutez_type = 1000000
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>


See [Mutez](/types/mutez) documentation.
<MichelsonDocLink placeholder="mutez" url="https://tezos.gitlab.io/michelson-reference/#type-mutez"/>


### timestamp
A moment in time.

Literal timestamp is written either using [RFC3339](https://datatracker.ietf.org/doc/html/rfc3339) notation in a string (readable), or as the number of seconds since Epoch in a natural (optimized).

<Snippet syntax={SYNTAX.PY}>

```python
sp.timestamp(1571761674)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TTimestamp = 1571761674
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Timestamps](/types/timestamps) documentation.
<MichelsonDocLink placeholder="timestamp" url="https://tezos.gitlab.io/michelson-reference/#type-timestamp"/>


### address
An address of a contract or implicit account.

The address type merely gives the guarantee that the value has the form of a Tezos address, as opposed to [contract](#contract) that guarantees that the value is indeed a valid, existing account.

A valid Tezos address is a string prefixed by either tz1, tz2, tz3 or KT1 and followed by a Base58 encoded hash and terminated by a 4-byte checksum.

The prefix designates the type of address:

- `tz1` addresses are followed by a ed25519 public key hash
- `tz2` addresses are followed by a Secp256k1 public key hash
- `tz3` addresses are followed by a NIST p256r1 public key hash
- `KT1` addresses are followed by a contract hash

Addresses prefixed by `tz1`, `tz2` and `tz3` designate implicit accounts, whereas those prefixed `KT1` designate originated accounts.

Finally, addresses can specify an entrypoint by providing a `%<entrypoint_name>` suffix.

<Snippet syntax={SYNTAX.PY}>

```python
sp.address("tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
address_type = TAddress

// value assignment
let value: address_type = "tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Addresses and Contracts](/types/contracts_addresses) documentation.
<MichelsonDocLink placeholder="address" url="https://tezos.gitlab.io/michelson-reference/#type-address"/>


### key
A public cryptographic key.

**Supported Curves:**

- [ed25519](https://ed25519.cr.yp.to) prefix with `edpk`;
- [secp256k1](https://en.bitcoin.it/wiki/Secp256k1) prefix with `sppk`;
- [p256](https://neuromancer.sk/std/secg/secp256r1) prefix with `p2pk`;

<Snippet syntax={SYNTAX.PY}>

```python
sp.key("edpktx799pgw7M4z8551URER52VcENNCSZwE9f9cst4v6h5vCrQmJE")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
key_type = TKey

// value assignment
let value: key_type = "edpktx799pgw7M4z8551URER52VcENNCSZwE9f9cst4v6h5vCrQmJE"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Keys](/types/keys) documentation.
<MichelsonDocLink placeholder="key" url="https://tezos.gitlab.io/michelson-reference/#type-key"/>

### key_hash
A hash of a public cryptographic key.

<Snippet syntax={SYNTAX.PY}>

```python
sp.key_hash("tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
key_hash_type = TKey_hash

// value assignment
let value: key_hash_type = "tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Keys](/types/keys) documentation.
<MichelsonDocLink placeholder="key_hash" url="https://tezos.gitlab.io/michelson-reference/#type-key_hash"/>


### signature
A cryptographic signature.

<Snippet syntax={SYNTAX.PY}>

```python
sp.signature("edsigu3QszDjUpeqYqbvhyRxMpVFamEnvm9FYnt7YiiNt9nmjYfh8ZTbsybZ5WnBkhA7zfHsRVyuTnRsGLR6fNHt1Up1FxgyRtF")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
signature_type = TSignature

// value assignment
let value: signature_type = "edsigu3QszDjUpeqYqbvhyRxMpVFamEnvm9FYnt7YiiNt9nmjYfh8ZTbsybZ5WnBkhA7zfHsRVyuTnRsGLR6fNHt1Up1FxgyRtF"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Signatures](/types/signatures) documentation.
<MichelsonDocLink placeholder="signature" url="https://tezos.gitlab.io/michelson-reference/#type-signature"/>

### chain_id
A chain identifier.

An identifier for a chain, used to distinguish the test and the main chains.

The value is derived from the genesis block hash and will thus be different on the main chains of different networks and on the test chains for which the value depends on the first block of the test-chain.

Please note that chain ids are non comparable. Equality can be verified in test scenarios by using [verify_equal](/general/checking_condition.md#verify_equal).

<Snippet syntax={SYNTAX.PY}>

```python
sp.chain_id_cst("0x9caecab9")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
chain_id_type = TChain_id

// value assignment
let value: chain_id_type = "0x9caecab9"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Chain Id](/types/chain_id) documentation.
<MichelsonDocLink placeholder="chain_id" url="https://tezos.gitlab.io/michelson-reference/#type-chain_id"/>

### unit

The type whose only value is `Unit`, to use as a placeholder when some result or parameter is non-necessary. For instance, when the only goal of a contract is to update its storage.

<Snippet syntax={SYNTAX.PY}>

```python
sp.unit
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Sp.unit
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Unit](/types/unit) documentation.
<MichelsonDocLink placeholder="unit" url="https://tezos.gitlab.io/michelson-reference/#type-unit"/>

### bls12_381_g1
A point on the BLS12-381 curve G1.

Written as raw bytes, using a big-endian point encoding, as specified [here](https://docs.rs/bls12_381/latest/bls12_381/notes/serialization/index.html#bls12-381-serialization).

See reference [BLS12](https://smartpy.io/ide?template=bls12_381.py) template.

<Snippet syntax={SYNTAX.PY}>

```python
sp.bls12_381_g1("0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
bls12_381_g1_type = TBls12_381_g1

// value assignment
let value: TBls12_381_g1 = "0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [BLS12 381](/types/bls12-381) documentation.
<MichelsonDocLink placeholder="bls12_381_g1" url="https://tezos.gitlab.io/michelson-reference/#type-bls12_381_g1"/>

### bls12_381_g2
A point on the BLS12-381 curve G2.

Written as raw bytes, using a big-endian point encoding, as specified [here](https://docs.rs/bls12_381/latest/bls12_381/notes/serialization/index.html#bls12-381-serialization).

See reference [BLS12](https://smartpy.io/ide?template=bls12_381.py) template.

<Snippet syntax={SYNTAX.PY}>

```python
sp.bls12_381_g2("0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
bls12_381_g2_type = TBls12_381_g2

// value assignment
let value: bls12_381_g2_type = "0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [BLS12 381](/types/bls12-381) documentation.
<MichelsonDocLink placeholder="bls12_381_g2" url="https://tezos.gitlab.io/michelson-reference/#type-bls12_381_g2"/>

### bls12_381_fr
An element of the BLS12-381 scalar field Fr.

An element of the scalar field Fr, used for scalar multiplication on the BLS12-381 curves G1 and G2.

Written as raw bytes, using a big-endian point encoding, as specified [here](https://docs.rs/bls12_381/latest/bls12_381/notes/serialization/index.html#bls12-381-serialization).

See reference [BLS12](https://smartpy.io/ide?template=bls12_381.py) template.

<Snippet syntax={SYNTAX.PY}>

```python
sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
bls12_381_fr_type = TBls12_381_fr

// value assignment
let value: bls12_381_fr_type = "0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [BLS12 381](/types/bls12-381) documentation.
<MichelsonDocLink placeholder="bls12_381_fr" url="https://tezos.gitlab.io/michelson-reference/#type-bls12_381_fr"/>

### chest

Represents timelocked arbitrary bytes with the necessary public parameters to open it.

<Snippet syntax={SYNTAX.PY}>

```python
sp.chest("0x01234")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Timelock](/experimental/timelock) documentation.
<MichelsonDocLink placeholder="chest" url="https://tezos.gitlab.io/michelson-reference/#type-chest"/>

### chest_key

Represents the decryption key, alongside with a proof that the key is correct.

<Snippet syntax={SYNTAX.PY}>

```python
sp.chest_key("0x01234")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

See [Timelock](/experimental/timelock) documentation.
<MichelsonDocLink placeholder="chest_key" url="https://tezos.gitlab.io/michelson-reference/#type-chest_key"/>

## Container Types


### pair of type1 and type2 {#pair}
A pair or tuple of values.

The type `pair type1 type2` is the type of binary pairs composed of a left element of type `type1` and a right element of type `type2`.

<Snippet syntax={SYNTAX.PY}>

```python
(1, True)
# or
sp.pair(sp.nat(1), True)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
pair_type = TTuple<TNat, TBool>

// value assignment
let value: pair_type = [1, true];
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="pair" url="https://tezos.gitlab.io/michelson-reference/#type-pair"/>


### list of type {#list}

A single, immutable, homogeneous linked list, whose elements are of type `type`.

<Snippet syntax={SYNTAX.PY}>

```python
[1, 2, 3]
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
list_type = TList<TNat>

// value assignment
let value: list_type = [1];
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="list" url="https://tezos.gitlab.io/michelson-reference/#type-list"/>


### set of type {#set}
An immutable set of comparable values of type `type`.

<Snippet syntax={SYNTAX.PY}>

```python
sp.set([1, 2, 3])
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
set_type = TSet<TNat>

// value assignment
let value: set_type = [1];
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="set" url="https://tezos.gitlab.io/michelson-reference/#type-set"/>


### map of kty and vty {#map}
An immutable map from `kty` to `vty`.

Immutable maps from keys of type `kty` and values of type `vty`.

<Snippet syntax={SYNTAX.PY}>

```python
{ 1: "A String" }
# or
sp.map({ 1: "A String" }, tkey = sp.TNat, tvalue = sp.TString)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
map_type = TMap<TNat, TString>

// value assignment
let value: map_type = [[1, "A String"]];
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="map" url="https://tezos.gitlab.io/michelson-reference/#type-map"/>


### big_map of kty and vty {#big_map}
A lazily deserialized map from `kty` to `vty`.

Lazily deserialized maps from keys of type `kty` of values of type `vty`. These maps should be used if you intend to store large amounts of data in a map. Using `big_map` can reduce gas costs significantly compared to standard maps, as data is lazily deserialized. Note however that individual operations on `big_map` have higher gas costs than those over standard maps. A `big_map` also has a lower storage cost than a standard map of the same size, when large keys are used, since only the hash of each key is stored in a `big_map`.

The behavior of [GET](https://tezos.gitlab.io/michelson-reference/#instr-GET), [UPDATE](https://tezos.gitlab.io/michelson-reference/#instr-UPDATE), [GET_AND_UPDATE](https://tezos.gitlab.io/michelson-reference/#instr-GET_AND_UPDATE) and [MEM](https://tezos.gitlab.io/michelson-reference/#instr-MEM) is the same on `big_map` as on normal `map`, except that under the hood, elements are loaded and deserialized on demand.

Literal `big_map` cannot be pushed directly in contract code. Instead, they must be created using [EMPTY_BIG_MAP](https://tezos.gitlab.io/michelson-reference/#instr-EMPTY_BIG_MAP) and manipulated using [GET](https://tezos.gitlab.io/michelson-reference/#instr-GET), [UPDATE](https://tezos.gitlab.io/michelson-reference/#instr-UPDATE), [GET_AND_UPDATE](https://tezos.gitlab.io/michelson-reference/#instr-GET_AND_UPDATE) and [MEM](https://tezos.gitlab.io/michelson-reference/#instr-MEM).

Values of the `big_map` type cannot be serialized using [PACK](https://tezos.gitlab.io/michelson-reference/#instr-PACK).

<Snippet syntax={SYNTAX.PY}>

```python
sp.big_map({ 1 : "A String" }, tkey = sp.TNat, tvalue = sp.TString)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
big_map_type = TBig_map<TNat, TString>

// value assignment
let value: big_map_type = [[1, "A String"]];
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="big_map" url="https://tezos.gitlab.io/michelson-reference/#type-big_map"/>


### option of type {#option}
The type of an optional values.

They represent values of type `type` or nothing.

Optional values are useful for accommodating missing data, that will only be available after origination.

<Snippet syntax={SYNTAX.PY}>

```python
sp.some(1)
# or
sp.none
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
option_of_nat_type = TOption<TNat>

// value assignment
let value: option_of_nat_type = Sp.some<TNat>(1);
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="option" url="https://tezos.gitlab.io/michelson-reference/#type-option"/>


### contract of type {#contract}
<Snippet syntax={SYNTAX.PY}>

`sp.TContract(t)` is the type of contracts whose parameters are of type `t`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`TContract(t)` is the type of contracts whose parameters are of type `t`.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>


A value of type `contract type` is guaranteed to be a valid, existing account whose parameter type is `type`. This can be opposed to the `address` type, that merely gives the guarantee that the value has the form of a Tezos address. Values of the contract type cannot be stored. There are not literal values of type contract. Instead, such values are created using instructions such as **[CONTRACT](https://tezos.gitlab.io/michelson-reference/#instr-CONTRACT)** or **[IMPLICIT_ACCOUNT](https://tezos.gitlab.io/michelson-reference/#instr-IMPLICIT_ACCOUNT)**.

<Snippet syntax={SYNTAX.PY}>

```python
sp.contract(sp.TNat, "tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4", entry_point = "foo").open_some("InvalidInterface")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
// type definition
contract_type = TContract<TNat>

// value assignment
let value: contract_type = Sp.contract<TNat>("tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4", "foo").openSome("InvalidInterface");
// or
let value: contract_type = "tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4%foo";
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="contract" url="https://tezos.gitlab.io/michelson-reference/#type-contract"/>


### sapling_state of memo size {#sapling_state}

<Snippet syntax={SYNTAX.PY}>

`sp.TSaplingState(memo_size = size)` is the type of sapling states with memo size of `size`.

Example
```python
# A type
sp.TSaplingState(memo_size = 8)

# Some value
sp.sapling_empty_state(memo_size = 8)
```
</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript

`TSaplingState<size>`  is the type of sapling states with memo size of `size`.

```
</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>


Please see the [Sapling integration](https://tezos.gitlab.io/008/sapling.html) page for a more comprehensive description of the Sapling protocol.


<MichelsonDocLink placeholder="sapling_state" url="https://tezos.gitlab.io/michelson-reference/#type-sapling_state"/>

### sapling_transaction of memo size {#sapling_transaction}

<Snippet syntax={SYNTAX.PY}>

`sp.TSaplingTransaction(memo_size = size)` is the type of sapling transactions with memo size of `size`.

```python
## only for tests
sp.sapling_test_transaction(source, target, amount, memo_size)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`TSaplingTransaction<size>` is the type of sapling transactions with memo size of `size`.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

Please see the [Sapling integration](https://tezos.gitlab.io/008/sapling.html) page for a more comprehensive description of the Sapling protocol.

<MichelsonDocLink placeholder="sapling_transaction" url="https://tezos.gitlab.io/michelson-reference/#type-sapling_transaction"/>


## Record, Variant and Layout

In SmartPy, we can use custom data types called records and variants.
Records and variants are translated in Michelson into binary trees of pair and or with annotations corresponding to each field.

The geometry of these binary trees is described by a [layout](#layouts).

### Record

A record type represents a cartesian product of several types similar to a struct in `C` with a layout.

<Snippet syntax={SYNTAX.PY}>

`sp.TRecord(**fields)` A record type is introduced by enumerating the field names together with types

```python
sp.TRecord(x = sp.TInt, y = sp.TInt)
```
It uses the default layout as determined by SmartPy, and can be changed by specifying **default_record_layout** [flag](/general/flags#flags-with-arguments).

**`.layout(layout)`**

A record type, i.e. something of the form `sp.TRecord(...)`, can be used to define a record type with a layout by doing:
```python
t = sp.TRecord(owner = sp.TAddress, operator = sp.TAddress, token_id = sp.TString)
t_with_layout = t.layout(("owner", ("operator", "token_id")))
```

**`.right_comb()`**

Like **.layout(...)** but the geometry used is `(f1, (f2, .. (f_{k-1}, f_k)))))`, the list of fields is determined automatically and sorted alphabetically.

**`.with_fields(fields)`**

Adds some fields to the `TRecord(...)` where `fields` is a Python (string, type)-dictionary of fields.

**`.without_fields(fields)`**

Removes some fields from the `TRecord(...)` where `fields` is a Python list of fields.

</Snippet>


<Snippet syntax={SYNTAX.TS}>

`TRecord<{ ...fields }>` A record type is introduced by enumerating the field names together with types

```typescript
type aType = TRecord<{
    x: TInt;
    y: TNat;
}>
```

It uses the default layout as determined by SmartTS, and can be changed by specifying **default_record_layout** [flag](/general/flags#flags-with-arguments).

**`Layout`**

A record type, i.e. something of the form `TRecord<{ ...fields }, layout>`, can be used to define a record type with a layout by doing:

```typescript
type aType = TRecord<{
    owner   : TAddress;
    operator: TAddress;
    token_id: TString;
}, ["owner", ["operator", "token_id"]]>
```

**`Layout.right_comb`**

Like **["...", "..."]** but the geometry used is `(f1, (f2, .. (f_{k-1}, f_k)))))`, the list of fields is determined automatically and sorted alphabetically.

```typescript
type aType = TRecord<{
    owner   : TAddress;
    operator: TAddress;
    token_id: TString;
}, Layout.right_comb>
```

</Snippet>


<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

### Variant

A variant type represents a union of several choices, similar to a clean version of a struct with an enum/union pattern in `C`.

<Snippet syntax={SYNTAX.PY}>

`sp.TVariant(**fields)`
A variant type is introduced by enumerating the constructor names together with their inner types.

```python
# Example
sp.TVariant(default_choice = sp.TInt, alternative_choice = sp.TString)
```
It uses the default layout as determined by SmartPy, and can be changed by specifying **default_variant_layout** [flag](/general/flags#flags-with-arguments).

**`.layout(layout)`**

A variant type, i.e. something of the form `sp.TVariant(...)`, can be used to define a variant type with a layout by doing:
```python
t = sp.TVariant(choice_1 = sp.TAddress, choice_2 = sp.TString, choice_3 = sp.TString)
t_with_layout = t.layout(("choice_1", ("choice_2", "choice_3")))
```


**`.right_comb()`**

Like **.layout(...)** but the geometry used is `(f1, (f2, .. (f_{k-1}, f_k)))))`, the list of fields is determined automatically and sorted alphabetically.

</Snippet>


<Snippet syntax={SYNTAX.TS}>

`TVariant<Item1 | Item2>`
A variant type is introduced by enumerating the constructor names together with their inner types.

```typescript
type UpdateOperatorsParams = TVariant<
    | {
            kind: 'default_choice';
            value: TInt;
      }
    | {
            kind: 'alternative_choice';
            value: TString;
      },
>
```
It uses the default layout as determined by SmartTS, and can be changed by specifying **default_variant_layout** [flag](/general/flags#flags-with-arguments).


**`Layout`**

A record type, i.e. something of the form `TVariant<{ ...fields }, layout>`, can be used to define a record type with a layout by doing:

```typescript
type aType = TVariant<
    | {
            kind: 'default_choice';
            value: TInt;
      }
    | {
            kind: 'alternative_choice';
            value: TString;
      },
    ["default_choice", "alternative_choice"]
>
```

**`Layout.right_comb`**

Like **["...", "..."]** but the geometry used is `(f1, (f2, .. (f_{k-1}, f_k)))))`, the list of fields is determined automatically and sorted alphabetically.

```typescript
type aType = TVariant<
    | {
            kind: 'default_choice';
            value: TInt;
      }
    | {
            kind: 'alternative_choice';
            value: TString;
      },
    Layout.right_comb
>
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

### Layouts

<Snippet syntax={SYNTAX.PY}>

A layout is a Python expression listing all fields or constructors in a binary tree structure such as `("b", ("a", "c"))`.
There is no equivalent instruction in Michelson.

See reference [Data Type Layouts](https://smartpy.io/ide?template=layout.py) template.

The SmartPy compiler recognizes a special format **"**`[source]` as `[target]`**"** to translate field names between SmartPy and Michelson.<br/>
A specific case **"**`[source]` as**"** is also supported to generate annotation-less types.

See [FA1.2](https://smartpy.io/ide?template=FA1.2.py) template.

</Snippet>


<Snippet syntax={SYNTAX.TS}>

A layout is a typescript array listing all fields or constructors in a binary tree structure such as `("b", ("a", "c"))`.
There is no equivalent instruction in Michelson.

See reference [FA1.2](https://smartpy.io/ts-ide?template=FA1_2.ts) template.

</Snippet>


<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>


## Bounded types

See [Bounded types](/advanced/bounded-types) documentation.


## Setting a type constraint

<Snippet syntax={SYNTAX.PY}>

This is usually not needed for small contracts or prototypes but gets useful typically when interacting with the outside world, implementing a given interface, etc.

**`self.init_type(t)`**

Constrain the contract storage to be of type `t`  (called in the **`__init__`** constructor).<br/>
This is useful because it allows the user to make the storage type explicit or even to define storage-less contracts.<br/>
In a test, if not determined in the **`__init__`** method, the storage can be initialized by calling `c.init_storage(expression)`.<br/>

See reference [Init Type Only](https://smartpy.io/ide?template=init_type_only.py) template.<br/>
This is not mandatory but is appreciated by many in practice.

```python
class MyContract(sp.Contract):
def __init__(self):
    ## Possibly set the storage type
    self.init_type(sp.TRecord(a = sp.TInt, b = sp.TString))
    ## Possibly set the storage
    self.init(...)
```

**`sp.set_type(expression, t)`**

Constrain `expression` to be of type `t`. This can only be used as a command inside a contract.

There is no equivalent instruction in Michelson.<br/>
A usual pattern for big contracts is to explicitly setting types in the first lines of entry points.

This is usually not needed but is appreciated by many in practice.
```python
@sp.entry_point
def my_entry_point(self, x, y, z):
    ## First set types
    sp.set_type(x, sp.TInt)
    sp.set_type(y, sp.TString)
    sp.set_type(z, sp.TList(sp.TInt))
    ## Then do the computations
    ...
```

**`sp.set_result_type(t)`**

Wrap a block of commands and constrain its result type to `t`. This can only be used as a command inside a contract.

There is no equivalent instruction in Michelson.

This is useful for making types of failures explicit.
```python
@sp.private_lambda()
def oh_no(params):
    with sp.set_result_type(sp.TInt):
        sp.if params > 10:
            sp.failwith("too big")
        sp.else:
            sp.result(params)
```

**`sp.set_type_expr(expression, t)`**

Wrap `expression` to be of type `t`. This can only be used as an expression.

There is no equivalent instruction in Michelson.

A few words of caution about the differences between `sp.set_type` and `sp.set_type_expr`:
```python
# Inside a contract:

    @sp.entry_point
    def my_entry_point(self, params):
        ...
        ## Warning: this is not taken into account (the expression is simply dropped).
        sp.set_type_expr(params, sp.TInt)
        ...

    @sp.entry_point
    def my_entry_point(self, params):
        ...
        ## This is taken into account (when we call params afterward).
        params = sp.set_type_expr(params, sp.TInt)
        ... params ...

    @sp.entry_point
    def my_entry_point(self, params):
        ...
        ## This is taken into account (sp.set_type is a command).
        sp.set_type(params, sp.TInt) ## This is taken into account.
        ...

# Inside a test scenario:

    scenario += ...
    ## This is illegal (command outside of a contract).
    sp.set_type(..., ...)

    ## This is OK (usually useless nonetheless)
    x = sp.set_type_expr(..., ...)
    scenario += c.f(x)
```

Containers have built-in optional constraint arguments.

Define a map of (optional) elements in `l` with optional key type `tkey` and optional value type `tvalue`.
```python
sp.map(l = ..., tkey = ..., tvalue = ...)
```

Define a big_map of (optional) elements in `l` with optional key type `tkey` and optional value type `tvalue`.
```python
sp.big_map(l = ..., tkey = ..., tvalue = ...)
```

Defines a set of (optional) elements in list `l` with optional element type `t`.
```python
sp.set(l = ..., t = ...)`
```

Defines a list of (optional) elements in list `l` with optional element type `t`.
```python
sp.list(l = ..., t = ...)
```

</Snippet>


<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress
```

</Snippet>


<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>
