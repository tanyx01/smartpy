import SyntaxSelector from '@site/src/components/Syntax/Selector';
import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

# Flags


Several aspects of scenarios and the compilation of contracts can be controlled by the use of flags.

**SmartPy supports two sorts of flags:**
 - [Boolean flags](#boolean-flags)
 - [Flags with arguments](#flags-with-arguments)

### Boolean Flags

| Flag  | Description  | Default Value |
|:---------------------:|:--------------------------:|:--------------:|
| `contract-check-exception` | Fail in the interpreter when sp.contract(...) evaluates to none. | `True` |
| `view-check-exception` | Fail in the interpreter when sp.view(...) evaluates to none. | `True` |
| `disable-dup-check` | Remove the DUP protection on tickets | `False` |
| `dump-michel` | Dump Michel intermediate language | `False` |
| `erase-comments` | Remove compiler comments | `False` |
| `erase-var-annots` | Remove annotations (with RENAME) after `SENDER`, `BALANCE`, etc | `False` |
| `initial-cast` | Add an initial `CAST` to remove annotations first in the compiler | `False` |
| `pairn` | Use `PAIR n` instruction | `True` |
| `simplify` | Allow simplification after compilation | `True` |
| `simplify-via-michel` | Use Michel intermediate language | `False` |
| `single-entry-point-annotation` | Emit parameter annotations for single entry point contracts | `True` |
| `stop-on-error` | Scenarios stop on first error | `False` |
| `warn-unused` | Exceptions for unused entry point parameters | `True` |
| `decompile` | Decompile after compilation (for test purposes) | `False` |
| `default-check-no-incoming-transfer` | Default value for `check-no-incoming-transfer` parameter in entry points. | `False` |

### Flags with arguments

| Flag  | Description  | Options | Default Value |
|:---------------------:|:--------------------------:|:-----:|:--------------:|
| `protocol` | Protocol used in the scenario | `jakarta`, `kathmandu`, `lima` | `lima` |
| `lazy-entry-points` | Use big maps to store entry points |`none`, `single`, `multiple` | `none` |
| `exceptions` | Control exception generation in the compiler |`full-debug`, `debug-message`, `verify-or-line`, `default-line`, `line`, `default-unit`, `unit`. <br /> See [Exception Optimization Levels](/general/flags#exception-optimization-levels) | `verify-or-line` |
| `default_variant_layout` | Select default layout for variants |`tree`, `comb` | `tree` |
| `default_record_layout` | Select default layout for records |`tree`, `comb` | `tree` |

## Adding flags to a contract

Add a flag where flag is a string constant and args are potential arguments.

### Boolean flag

<Snippet syntax={SYNTAX.PY}>

```py
class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)
        # Enable flag
        self.add_flag("erase-comments")
        # Or disable flag
        self.add_flag("no-erase-comments")
```

</Snippet>
<Snippet syntax={SYNTAX.TS}>

```ts
@Contract({
    flags: [
        ["erase-comments"], // Enable erase-comments flag
        ["no-warn-unused"]  // Disable warn-unused flag
    ]
})
class MyContract {
    ...
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

### Flag with arguments

<Snippet syntax={SYNTAX.PY}>

```py
class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)

        self.add_flag("exceptions", "unit")
```

</Snippet>
<Snippet syntax={SYNTAX.TS}>

```ts
@Contract({
    flags: [
        ["exceptions", "unit"],
    ]
})
class MyContract {
    ...
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Adding flags to a scenario

Same as in [Adding flags to a contract](#adding-flags-to-a-contract), but instead provided in a test scenario.

<Snippet syntax={SYNTAX.PY}>

```py
@sp.add_test(name = "FlagExample")
def test():
    scenario = sp.test_scenario()
    scenario.add_flag("protocol", "edo")
```

</Snippet>
<Snippet syntax={SYNTAX.TS}>

```ts
Dev.test({
    name: 'FlagExample',
    flags: [
        ["protocol", "florence"],
    ]
}, () => {
    ...
});

```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Exception Optimization Levels

Defines the default exception behavior when [FAILWITH](https://tezos.gitlab.io/michelson-reference/#instr-FAILWITH) instructions get called.

<Snippet syntax={SYNTAX.PY}>

See reference [Exception Optimization](https://smartpy.io/ide?template=test_exception_optimization.py) template.

</Snippet>
<Snippet syntax={SYNTAX.TS}>

See reference [Exception Optimization](https://smartpy.io/ts-ide?template=ExceptionOptimization.ts) template.

</Snippet>

**The different levels are:**

| Level | Description |
|:----------------------------------------:|:-:|
| **`full-debug`** | This is extremely costly in terms of size and gas. Useful for debugging purposes. Type of failure, line number, some parameters |
| **`debug-message`** | This is still very costly in terms of size and gas |
| **`verify-or-line`** (default) | Puts messages for `verify` and `failwith`. Uses line numbers for other failures. |
| **`default-line`** | Puts messages for `verify` with custom messages and `failwith`. Uses line numbers for other failures. |
| **`line`** | Only puts line numbers everywhere |
| **`default-unit`** | Puts messages for `verify` with custom messages, and `failwith`. Uses `unit` for other failures. |
| **`unit`** | Always puts unit |

**Configuration Example**

<Snippet syntax={SYNTAX.PY}>

```py
class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)
        # Enable flag
        self.add_flag("exceptions", "full-debug")
```

</Snippet>
<Snippet syntax={SYNTAX.TS}>

```ts
@Contract({
    flags: [
        ["exceptions", "full-debug"],
    ]
})
class MyContract {
    ...
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
