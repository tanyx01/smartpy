---
sidebar_position: 100
title: FA1.2
---

Please see the SmartPy
[FA1.2](pathname://../../../../ide?template=FA1.2.py) template and
[TZIP-7](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-7/tzip-7.md) specifications.
