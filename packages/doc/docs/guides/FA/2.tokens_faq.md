---
sidebar_position: 2
title: Tokens - FAQ
---

# FAQ

## How does token work?

A smart contract (a little software that runs independently on the blockchain)
maintains a ledger. The ledger indicates who owns which token. It also maintains
a list of metadata associated with each token type. A metadata can specify the
token's name, symbol, number of decimals...

A token is identified by two values: the address of the smart contract and an
id number.

## What can we do with them?

Tokens can be minted (created from scratch), burned (definitively destroyed) and
transferred depending on rules written in the smart contract that maintains them.

## What do they represent?

Tokens can represent anything. The metadata associated with the tokens give you
the endless possibilities. Tokens can represent money, piece of art, permissions,
real world assets like houses or contracts.

## What are fungible tokens?

Fungible tokens represent a value that can be substitute by another.
For example, you wouldn't mind if someone replaces a $20 bill in your pocket
with another $20 bill because you don't distinguish two bills of the same value.

## What are NFTs or non-fungible tokens?

NFTs represent a unique value that can never be substitute by another.
NFTs are useful to represent real world assets that are unique like a painter
or a house. They can also represent a digital asset that is considered unique
like a digital painter, a music...

## How to create a token smart contract?

We recommend you to create a FA2 token and follow the [FA2 library guide](/guides/FA/FA2/overview).