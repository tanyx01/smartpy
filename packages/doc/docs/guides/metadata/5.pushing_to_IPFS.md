# Pushing to IPFS

Here we'll explain a method to push data on IPFS. Notice that a basic
understanding on what IPFS is and how it works is needed.

Remember that IPFS is fully public, like the blockchain and that it's not a
permanent storage.

A way to persist your data is called "pinning". Some services gives the ability
to push data on IPFS and pin them.

## Pinata

[Pinata cloud](https://pinata.cloud/) is a website that provides IPFS pinning
service. We are not affiliate to it in any way and do not endorse any
responsibility for its usage but we think it's an easy way to start interacting
with IPFS.

After having registered you can upload a file. Upload your JSON file or media
and then get its CID (it starts with "Qm"). This is the hash you need to
indicate in the [URI schemes](../uri_schemes).