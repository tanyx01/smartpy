---
sidebar_position: 1
---

import Michelson from '@site/src/components/MichelsonDoc/Michelson';
import MichelsonArg from '@site/src/components/MichelsonDoc/MichelsonArg';
import CodeTabs from '@site/src/components/CodeTabs';

# Overview

:::info
Version Alpha 0.2
A game platform contract (see [below](#contracts-and-examples)) has
been originated on *Hangzhounet*:
[KT1QW2haDVNiSaTW7NmL8ece16NwBGGpVLZh](https://better-call.dev/hangzhou2net/KT1QW2haDVNiSaTW7NmL8ece16NwBGGpVLZh).
The ledger: [KT19FUt5vfAGKAiG1rtibTzYnz5yDZxmhfum](https://better-call.dev/hangzhou2net/KT19FUt5vfAGKAiG1rtibTzYnz5yDZxmhfum).
:::

## What are state channels?

State channels combine the decentralization and security offered by
the Tezos blockchain with the throughput and low latency offered by
off-chain operations. As such, they provide a "layer two solution" that
overcomes scalability limits and reduces operational costs.

By way of analogy, an ordinary legal contract does not require a judge
to be present at every interaction of the involved parties. Indeed
this is only needed in the comparatively rare cases of disagreement or
foul play. The same is true for state channels: they provide a way for
parties to interact off-chain and only need to fall back to on-chain
operations to settle conflicts.

<div style={{textAlign: "center"}}>

![Build metadata](/img/guides/stateChannels/basic-interaction.png)

</div>

## Games!

To explain how state channels work, this guide describes a *game
platform*. It allows users to play games such as chess or
tic-tac-toe. A prize for the winner is agreed upon at the start and
settled at the end of a game. Actually, the provided framework is
slightly more general and it is easy to see how games generalize to
other types of contracts, e.g. financial ones.

## How does it work?

The game platform, represented on-chain by a contract, allows
participants to open channels between each other. To assure each other
that debt resulting from game outcomes will be settled, they then push
bonds that can only be withdrawn under certain conditions.  The next
step is to actually play the game: the players send each other signed
off-chain operations, without any chain interactions. In case of disagreement either player
can move the game to the blockchain.  This ensures that the players
can force each other to respect the rules of the game.  If all goes
well, very few operations take place on-chain, making game play
cheaper (off-chain operations have no associated gas cost) and
possibly faster.

The rules of a game are stipulated in a so-called *model*. (using
SmartPy or any other methods to generate Michelson).  Each instance of
a model is called a *game*.  In each game two players take turns in
making moves.

## Contracts and examples

The contracts for the platform, as well as the game models, can be
found under [smartpy.io/ide](https://smartpy.io/ide) > Templates >
Regular Templates > State Channels. In particular, there is:

- The main [platform
  contract](https://smartpy.io/ide?template=state_channel_games/game_platform.py):
  management of state channels, games and bonds.

- The [token
  ledger](https://smartpy.io/ide?template=state_channel_games/ledger.py):
  management of FA tokens

- A [model
  tester](https://smartpy.io/ide?template=state_channel_games/game_tester.py):
  test a single model before deploying it.

- A [set of example models](/guides/state_channels/models/models), together with scenarios
  and tests. Each model contains a scenario for the model tester.

- A [realistic scenario](/guides/state_channels/more/realistic_scenario) between two players using the testnet platform

## API: instantiating the ledger and platform

<CodeTabs>
    <Michelson name="ledger" type="sp.Contract">
        <MichelsonArg
            name="fa2config"
            type="$fa2config">
            See Python tab or <a href="https://smartpy.io/ide?template=FA2.py">FA2 template</a>
        </MichelsonArg>
        <MichelsonArg
            name="metadata"
            type="sp.TBigMap(sp.TString, sp.TBytes)">
            metadata of the contract
        </MichelsonArg>
        <MichelsonArg
            name="admin"
            type="sp.TAddress">
            Allowed to set token metadata/permissions and inactivate the contract.
        </MichelsonArg>
    </Michelson>
<block name="Python">

```python
import smartpy as sp
FA2 = sp.io.import_template("FA2.py")
lgr = sp.io.import_template("state_channel_games/ledger.py")

@sp.add_test(name="Ledger")
def test():
    sc = sp.test_scenario()
    admin = sp.test_account("admin")
    fa2_config = FA2.FA2_config(single_asset = False)
    metadata = sp.utils.metadata_of_url("https://example.com")

    wXTZ_metadata = FA2.FA2.make_metadata(
        name     = "Wrapped XTZ",
        symbol   = "WXTZ",
        decimals = 6,
    )
    wXTZ_permissions = lgr.build_token_permissions({
        "type"            : "NATIVE",
        "mint_cost"       : sp.mutez(1),
        "mint_permissions": sp.variant("allow_everyone", sp.unit)
    }, option = False)
    # `storage_overrides` is optionnal
    # In this example we define a wrapped XTZ that can be minted
    # By giving 1 mutez for 1 muWXTZ
    ledger = lgr.Ledger(
            fa2_config,
            admin = ledger_admin,
            metadata = metadata,
            storage_overrides = {
                "tokens_permissions": {0: wXTZ_permissions},
                "tokens_metadata": {0: wXTZ_metadata}
            },
        )
    sc += platform
```
</block>
</CodeTabs>

<CodeTabs>
    <Michelson name="gamePlatform" type="sp.Contract">
        <MichelsonArg
            name="admins"
            type="sp.TSet(sp.TAddress)">
            Set of admins of the platform. They are allowed to set token/model/games metada.
        </MichelsonArg>
        <MichelsonArg
            name="ledger"
            type="sp.TAddress">
            Address of the only contract authorized to indicate that bonds has be pushed.
        </MichelsonArg>
        <MichelsonArg
            name="platform_address"
            type="sp.TAddress">
            (optional) The address of the platform you want to replicate.
        </MichelsonArg>
    </Michelson>
<block name="Python">

```python
import smartpy as sp
gp = sp.io.import_template("state_channel_games/game_platform.py")

@sp.add_test(name="Game Platform")
def test():
    sc = sp.test_scenario()
    admin = sp.test_account("admin")
    platform_address = sp.address('KT1_ADDRESS_OF_THE_PLATFORM') # can be `None`
    platform = gp.GamePlatform(admins = sp.set([player1.address]), self_addr = platform_address)
    sc += platform
```
</block>
</CodeTabs>
