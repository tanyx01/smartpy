---
sidebar_position: 5
---

# 4️ New game

import SyntaxSelector from '@site/src/components/Syntax/Selector';
import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import Entrypoint from '@site/src/components/MichelsonDoc/Entrypoint';
import OffchainView from '@site/src/components/MichelsonDoc/OffchainView';
import MichelsonArg from '@site/src/components/MichelsonDoc/MichelsonArg';
import MichelsonArgs from '@site/src/components/MichelsonDoc/MichelsonArgs';
import Michelson from '@site/src/components/MichelsonDoc/Michelson';
import CodeTabs from '@site/src/components/CodeTabs';
import Code from '@site/src/components/CodeBlockWithTitle';

A game is a means to change the state of a channel. Each games has a
current state. This state evolves according to the rules defined by
its model. A game is attached to a channel.


## Structures of a game

### Outcomes

The outcome of a game. Partially controlled by the `apply_` lambda of
the model and partially controlled by the gamePlatform.

<Michelson name="t_outcome">
    <MichelsonArg
        name="$t_outcome_content"
        type="variant(game_finished = string, player_inactive = int, player_double_played = int)">
    </MichelsonArg>
    <MichelsonArg
        name="outcome"
        type="option(variant(pending = $t_outcome_content, final = $t_outcome_content))">
    </MichelsonArg>
</Michelson>
<br/>

If `outcome` is `none` or the outcome is pending, the game is still running.
Otherwise, (i.e. the outcome is variant "final") the game ended with one of the following results: <br/>

|         Variant          | Description                                                                                          | Value                                                                              |
| :----------------------: | ---------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
|    **game_finished**     | The game ended properly                                                                              | (string) Set by the model `apply_` lambda or by calling `game_set_outcome`. |
|     **game_aborted**     | The game was aborted                                                                                 | (unit) Set by calling `game_set_outcome`.                                     |
|   **player_inactive**    | One player timed out and the other called <span className="entrypointName">starved</span>.           | (nat) Id of the player who timed out                                               |
| **player_double_played** | One player double played and the other called <span className="entrypointName">double_signed</span>. | (nat) Id of the player who double-played                                           |

### Constants

The `constants` structure contains everything that was agreed by the
players at the beginning of the game.

<CodeTabs>
    <Michelson name="constants" type="pair">
        <MichelsonArg
            name="bonds"
            type="$game_bonds">
            See <a href="#game-bonds">game bonds</a>
        </MichelsonArg>
        <MichelsonArg
            name="channel_id"
            type="bytes">
            See <a href="/guides/state_channels/open_channel#build-the-channel_id">channel id calculation</a>.
        </MichelsonArg>
        <MichelsonArg
            name="game_nonce"
            type="string">
            A nonce that players gave to this game.
        </MichelsonArg>
        <MichelsonArg
            name="model_id"
            type="bytes">
            See <a href="/guides/state_channels/new_model#compute-the-model_id">model id calculation</a>.
        </MichelsonArg>
        <MichelsonArg
            name="play_delay"
            type="int">
            Seconds a player has to play on-chain when the other player calls <code>starving</code>.
        </MichelsonArg>
        <MichelsonArg
            name="players_addr"
            type="map (int, address)">
            Incremental player id starting at 1 => address
        </MichelsonArg>
        <MichelsonArg
            name="settlements"
            type="map ($outcome, list($transfer))">
            See <a href="#settlements">settlements</a>
        </MichelsonArg>
    </Michelson>
<block name="python">

```python
import smartpy as sp
t_constants = sp.record(
    model_id     = model_id,
    channel_id   = channel_id,
    players_addr = {1:player1.address, 2:player2.address},
    game_nonce   = "game1",
    play_delay   = 3600 * 24,
    settlements  = settlements,
    bonds        = {1:{0:20}, 2:{0:20}}
)
```
</block>
</CodeTabs>

### Transfers

A transfer describes a combination of tokens that is sent from a
sender to a receiver.

:::tip
You can use the player id `0` to set the platform as a sender.

In this case the game or the model must have special permissions from
the platform admin to perform the transfer.
:::

<CodeTabs>
    <Michelson
        name="transfer"
        type="pair">
        <MichelsonArg
            name="sender"
            type="int">
            Player ID. Use 0 to send from the platform.
        </MichelsonArg>
        <MichelsonArg
            name="receiver"
            type="int">
            Player ID of the receiver.
        </MichelsonArg>
        <MichelsonArg
            name="bonds"
            type="map (nat, nat)">
            Bonds (token id: amount) that will be transferred.
        </MichelsonArg>
    </Michelson>
<block name="python">

```python
transfer = sp.record(
    sender = 1,
    receiver = 2,
    bonds = sp.map({0: 10})
)
```
</block>
</CodeTabs>

### Settlements

<CodeTabs>
    <Michelson name="settlements" type="map ($outcome, list($transfer))"></Michelson>
<block name="python">

```python
import smartpy as sp
settlements = sp.map({
        # (Outcome, Outcome Value)                 : # List of transfers
        sp.variant("game_finished", "player_1_won"): [sp.record(sender = 2, receiver = 1, bonds = {0: 10})],
        sp.variant("game_finished", "player_2_won"): [sp.record(sender = 1, receiver = 2, bonds = {0: 10})],
        sp.variant("game_finished", "draw")        : [],
        sp.variant("player_inactive",            1): [sp.record(sender = 1, receiver = 2, bonds = {0: 15})],
        sp.variant("player_inactive",            2): [sp.record(sender = 2, receiver = 1, bonds = {0: 15})],
        sp.variant("player_double_played",       1): [sp.record(sender = 1, receiver = 2, bonds = {0: 20})],
        sp.variant("player_double_played",       2): [sp.record(sender = 2, receiver = 1, bonds = {0: 20})],
        sp.variant("game_aborted",         sp.unit): [],
    })
# In this example "player_1_won", "player_2_won" and "draw" are defined by the model tictactoe.
```
</block>
</CodeTabs>
<br/>

Settlements associates an [outcome](#outcome) to a list of transfers.

Each outcome defined by the model and each standard outcome (`player_inactive`, `player_double_play`, `game_aborted`)
**MUST** appear in the settlement.

:::tip
Keep `player_double_played` > `player_inactive` > `game_finished`.
:::

### Current

The current structure is managed by the game platform.

<Michelson name="current" type="pair">
    <MichelsonArg
        name="move_nb"
        type="nat">
        Incremental id of the move
    </MichelsonArg>
    <MichelsonArg
        name="player"
        type="int">
        Id of the current player
    </MichelsonArg>
    <MichelsonArg
        name="outcome"
        type="$outcome">
        See <a href="#outcome">outcome</a>
    </MichelsonArg>
</Michelson>

### Game

The game structure is managed by the game platform.

<Michelson name="game" type="pair">
    <MichelsonArg
        name="addr_players"
        type="map (address, int)">
    </MichelsonArg>
    <MichelsonArg
        name="constants"
        type="$constants">
    </MichelsonArg>
    <MichelsonArg
        name="current"
        type="$current">
    </MichelsonArg>
    <MichelsonArg
        name="state"
        type="bytes">
    </MichelsonArg>
    <MichelsonArg
        name="settled"
        type="bool">
    </MichelsonArg>
    <MichelsonArg
        name="timeouts"
        type="map (int, timestamp)">
    </MichelsonArg>
</Michelson>


### `game_bonds`

`game_bonds` defines what each player agreed to post as bond until the
game is settled.<br/>
The index corresponds to the player id (1 for player
1...). The value is a map of tokens and amount.

:::caution
For each player, the bonds corresponds to **the maximum that a player
could be asked to pay** by a <a href="#settlements">settlement</a> for each token.
:::


Example:

```python

settlements = sp.map({
    # (Outcome, Outcome Value)                 : # List of transfers
    sp.variant("game_finished", "player_1_won"): [sp.record(sender = 1, receiver = 2, bonds = {0: 10})],
    sp.variant("player_inactive",            1): [sp.record(sender = 1, receiver = 2, bonds = {0: 15}),
                                                  sp.record(sender = 1, receiver = 2, bonds = {0:  5})],
    sp.variant("player_double_played",       1): [sp.record(sender = 1, receiver = 2, bonds = {1: 42})],
})

bonds = {
    1: {
        0: 20, # For the outcome ("player_inactive", 1)
        1: 42  # For the outcome ("player_double_played", 1)
    },
    2: {}
}
```

<CodeTabs>
    <Michelson name="game_bonds" type="map(int, map(int, int))"></Michelson>
<block name="python">

```python
import smartpy as sp
bonds = {1:{0:20}, 2:{0:20}}
```
</block>
</CodeTabs>


## Start a new game

:::caution
Each player is responsible for verifying the other players' bonds
before signing a game (see [game bonds](#game_bonds)).
If this step is neglected, a player may not receive their settlements.

If the other player has a running `withdraw_request`: make sure he has
sufficient channel bonds to cover the withdraw + all the non-settled
game bonds.<br/> Otherwise, ask him to push new bonds or cancel the
`withdraw_request`.
:::

All players need to sign a `pack` of a pair of `"New Game"`,
`<constants>`, `<initParams>`.

`<constants>` game constants agreed by the players. <br/>
`<initParams>` params for the `init` lambda of the [model](/guides/state_channels/new_model).

The pair is of type `string $constants bytes`.

<Snippet syntax={SYNTAX.PY}>

In SmartPy this corresponds to a call to the `action_new_game` method
of `game_platform.py`

```python
def action_new_game(constants, params):
    constants = sp.set_type_expr(constants, types.t_constants)
    params    = sp.set_type_expr(params, sp.TBytes)
    return sp.pack(("New Game", constants, params))
```

</Snippet>

## Compute the game structure off-chain

:::note
More info about offchain views and signatures can be found in [offchain views and signatures](/guides/state_channels/more/offchain_views_signatures)
:::

<CodeTabs>
    <OffchainView name="offchain_new_game">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="constants"
                type="$constants">
                <a href="#constants">Constants</a> of the game
            </MichelsonArg>
            <MichelsonArg
                name="params"
                type="bytes">
                <a href="/guides/state_channels/new_model#what-init-do">Init params</a> for the <code>init</code> lambda of the model.
            </MichelsonArg>
            <MichelsonArg
                name="signatures"
                type="map (key, signature)">
                Signatures as explained in <a href="#start-a-new-game-offchain">How to start a new game offchain?</a>
            </MichelsonArg>
        </MichelsonArgs>
    </OffchainView>
<block name="python">

```python
import smartpy as sp

class TestView(sp.Contract):
    def __init__(self, c, f):
        self.c = c
        self.f = f.f
        self.init(result = sp.none)

    @sp.entry_point
    def compute(self, data, params):
        self.c.data = data
        b = sp.bind_block()
        with b:
            self.f(self.c, params)
        self.data.result = sp.some(b.value)

def make_signatures(p1, p2, x):
    # This function is illustrative.
    # In real life examples, other player's signature are received from them
    # You shouldn't know their secret_key'
    sig1 = sp.make_signature(p1.secret_key, x)
    sig2 = sp.make_signature(p2.secret_key, x)
    return sp.map({p1.public_key: sig1, p2.public_key: sig2})

@sp.add_test(name="New Game")
def test():
    sc = sp.test_scenario()

    # ... (scenario + platform origination + constants creation)

    def new_game_sigs(constants, params = sp.unit):
        new_game = gp.action_new_game(constants, sp.pack(params))
        return make_signatures(player1, player2, new_game)

    offchain_new_game = TestView(platform, platform.offchain_new_game)
    sc += offchain_new_game

    offchain_new_game.compute(sp.record(
        data   = platform.data,
        params = sp.record(
            constants  = constants,
            params     = sp.pack(sp.unit),
            signatures = new_game_signatures
        )
    )).run(sender = player1)
    game = sc.compute(offchain_new_game.data.result.open_some())
```
</block>
</CodeTabs>

## Push a new game on the platform

:::tip
Pushing a new game on-chain is not necessary if there is no
conflict between players
:::

<CodeTabs>
    <Entrypoint name="new_game">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="constants"
                type="$constants">
                <a href="#constants">Constants</a> of the game
            </MichelsonArg>
            <MichelsonArg
                name="params"
                type="bytes">
                <a href="/guides/state_channels/new_model#what-init-do">Init params</a> for the <code>init</code> lambda of the model.
            </MichelsonArg>
            <MichelsonArg
                name="signatures"
                type="map (key, signature)">
                Signatures as explained in <a href="#compute-the-game-structure-off-chain">Compute the game structure off-chain</a>
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
import smartpy as sp

# ... Build constants, gather other's signature and sign the new_game structure

platform.new_game(
    constants  = constants,
    params     = sp.pack(sp.unit),
    signatures = new_game_signatures
).run(sender = player2.address)
```
</block>
</CodeTabs>

## Compute the `game_id`

The game id is the `BLAKE2B` hash of the `sp.pair(channel_id, sp.pair(model_id, game_nonce))`.

```python
import smartpy as sp
gp = sp.io.import_template("state_channel_games/game_platform.py")

game_id = gp.compute_game_id(constants)
```

:::note

The `constants` contains the id of the channel which depends on the platform address.
:::
