---
sidebar_position: 2
---

# 1️ Open a channel

import SyntaxSelector from '@site/src/components/Syntax/Selector';
import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';
import Entrypoint from '@site/src/components/MichelsonDoc/Entrypoint';
import MichelsonArg from '@site/src/components/MichelsonDoc/MichelsonArg';
import MichelsonArgs from '@site/src/components/MichelsonDoc/MichelsonArgs';
import CodeTabs from '@site/src/components/CodeTabs';

In order to use the game platform to interact with another player, you
first need to open a state channel.

## What is a channel?

A channel is the central notion on the game platform. It is formed
between two players, each of which post bonds (tokens) into the
channel. The bonds are transferred via games that can be executed off-chain.

Several channels between the same two players can be open at the same
time. This may be desirable in order to have different parameters,
e.g. a different `withdraw_delay` (see [below](#opening-a-channel)).
Otherwise it is not necessary, as the same channel can be used for
several games.

## Opening a channel

Everyone can open a channel by calling the following entrypoint:

<CodeTabs>
    <Entrypoint name="new_channel">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="nonce"
                type="string">
                One time usage string. Can be anything as long as it is not reused it for another channel.
            </MichelsonArg>
            <MichelsonArg
                name="players"
                type="map (address, key)">
                Each party associated with its public key.
            </MichelsonArg>
            <MichelsonArg
                name="withdraw_delay"
                type="int">
                The number of **seconds** the parties have to challenge a player from <code>withdrawing</code>.
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
import smartpy as sp
gp = sp.io.import_template("state_channel_games/game_platform.py")

@sp.add_test(name="New channel")
def test():
    sc = sp.test_scenario()
    player1 = sp.test_account("player1")
    player2 = sp.test_account("player2")
    players = {player1.address: player1.public_key, player2.address: player2.public_key}
    platform_address = sp.address('KT1_ADDRESS_OF_THE_PLATFORM') # Can be None
    ledger_address = sp.address('KT1_ADDRESS_OF_THE_LEDGER')
    platform = gp.GamePlatform(
        admins = sp.set([player1.address]),
        ledger = ledger_address,
        self_addr = platform_address
    )
    sc += platform

    # New channel
    platform.new_channel(
        players = players,
        nonce = "Channel 1",
        withdraw_delay = 3600 * 24
    ).run(sender = player1)
```
</block>
</CodeTabs>

## Build the `channel_id`

The channel id will be used for any operation that is executed under the channel.

The channel id is the `BLAKE2B` hash of the `PACK` of a triple of:
- address of the platform
- players map (same as above)
- nonce (same as above)

:::info
The `channel_id` includes the platform address.
That's why the SmartPy template has an optionnal `self_addr` parameter to replicate an on-chain platform.
:::

```python
import smartpy as sp

channel_id = sp.blake2b(
    sp.pack(
        (
            sp.address('KT1_ADDRESS_OF_THE_PLATFORM'),
            players,
            nonce
        )
    )
)
```
