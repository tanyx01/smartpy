---
sidebar_position: 4
---

# 3️ New model

import SyntaxSelector from '@site/src/components/Syntax/Selector';
import Entrypoint from '@site/src/components/MichelsonDoc/Entrypoint';
import MichelsonArg from '@site/src/components/MichelsonDoc/MichelsonArg';
import MichelsonArgs from '@site/src/components/MichelsonDoc/MichelsonArgs';
import CodeTabs from '@site/src/components/CodeTabs';

> Note: this step is optional. It is only needed if you want to define
  a new game with new rules. To get started, it may be easier to use
  one of the pre-defined [example models](/guides/state_channels/models/models).

A model is a piece of code that determine the **rules of a
game**. They consist of two lambdas: `init` and `apply_`.

`init` takes a set of parameters and returns the initial state of the
game.  `apply_` takes a current state, a move, and a current state
metadata. It returns a new state and, if the game is finished, an
outcome.  The outcome can be mapped to a settlement, using a list
defined at game creation.

## Push a new model on the platform

:::tip
The `model_wrap` function of `model_wrap.py` returns the structure
expected by the `new_model` entrypoint.
:::

<CodeTabs>
    <Entrypoint name="admin_new_model">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="metadata"
                type="sp.TMap(sp.TString, sp.TBytes)">
                The metadata of the model.
            </MichelsonArg>
            <MichelsonArg
                name="model"
                type="pack($model)">
                The model.
            </MichelsonArg>
            <MichelsonArg
                name="permissions"
                type="sp.TMap(sp.TString, sp.TBytes)">
                The permissions of the model.
            </MichelsonArg>
            <MichelsonArg
                name="rewards"
                type="sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(('to_', ('token_id', 'amount'))))">
                A list of transfers from the platform to reward the creators of the model.
            </MichelsonArg>
        </MichelsonArgs>
        <MichelsonArgs name="model" type="pair">
            <MichelsonArg
                name="init"
                type="lambda(bytes, bytes)">
                The lambda that will be called when instantiating a new game.
            </MichelsonArg>
            <MichelsonArg
                name="apply_"
                type="lambda ($apply_input, $apply_output)">
                The lambda that will be called every time someone is playing.
            </MichelsonArg>
        </MichelsonArgs>
        <MichelsonArgs name="apply_input" type="pair">
            <MichelsonArg
                name="move_data"
                type="bytes">
                Move data given to the model
            </MichelsonArg>
            <MichelsonArg
                name="move_nb"
                type="nat">
                Incremental id of the move. (Managed by the platform.)
            </MichelsonArg>
            <MichelsonArg
                name="player"
                type="nat">
                Id of the current player. (Managed by the platform)
            </MichelsonArg>
            <MichelsonArg
                name="state"
                type="bytes">
                Current state of the game. (Last state returned by the model)
            </MichelsonArg>
        </MichelsonArgs>
        <MichelsonArgs name="apply_output" type="pair">
            <MichelsonArg
                name="messages"
                type="bytes">
                Messages returned by the game
            </MichelsonArg>
            <MichelsonArg
                name="new_state"
                type="bytes">
                New state of the game
            </MichelsonArg>
            <MichelsonArg
                name="outcome"
                type="option(string)">
                If <code>none</code>, the game continues. Otherwise the game ends with the given outcome.
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
import smartpy as sp
gp         = sp.io.import_template("state_channel_games/game_platform.py")
Tictactoe  = sp.io.import_template("state_channel_games/models/tictactoe.py").Tictactoe
model_wrap = sp.io.import_template("state_channel_games/model_wrap.py").model_wrap

@sp.add_test(name="New Model")
def test():
    sc = sp.test_scenario()
    player1 = sp.test_account("player1")
    platform_address = sp.address('KT1_ADDRESS_OF_THE_PLATFORM'),
    platform = gp.GamePlatform(admins = sp.set([player1.address]), self_addr = platform_address)
    sc += platform

    # New Model
    tictactoe = Tictactoe()
    model = sc.compute(model_wrap.model_wrap(tictactoe))
    model_id = sc.compute(model_wrap.model_id(model))
    platform.new_model(model).run(sender = player1.address)
```
 </block>
</CodeTabs>

## Compute the model_id

The model_id is a `BLAKE2B` hash of the parameters `model` given to <span className="entrypointName">new_model</span>.

``` python
import smartpy as sp
model_wrap   = sp.io.import_template("state_channel_games/model_wrap.py").model_wrap
Tictactoe    = sp.io.import_template("state_channel_games/models/tictactoe.py").Tictactoe

tictactoe = Tictactoe()
model     = sc.compute(model_wrap.model_wrap(tictactoe))
model_id  = sc.compute(model_wrap.model_id(model))
```

## How to code the model?

:::caution

You should never use blockchain operations instructions inside your lambda.

The complete list of instructions is available in the <a
href="https://tezos.gitlab.io/michelson-reference/#instructions-blockchain">Michelson
Reference</a>. It includes:<br/>
<code>AMOUNT</code>, <code>BALANCE</code>,
<code>CONTRACT</code>,
<code>CREATE_CONTRACT</code>, <code>IMPLICIT_ACCOUNT</code>,
<code>LEVEL</code>, <code>NOW</code>, <code>SELF</code>,
<code>SELF_ADDRESS</code>, <code>SENDER</code>,
<code>SET_DELEGATE</code>, <code>SOURCE</code>,
<code>TOTAL_VOTING_POWER</code>, <code>TRANSFER_TOKENS</code>,
<code>VOTING_POWER</code>.

You should never accept a game with a model that contains one of these
instructions.
[More examplanations here](/guides/state_channels/more/operation_instruction_problem)
:::

### Model class

A model is a python class that defines the following attributes:

+ `name`: A string with the name of the model
+ `t_init_input`: SmartPy type of the parameter of `init`
+ `t_game_state`: SmartPy type of the game state
+ `t_move_data`: SmartPy type of the parameter of a play move
+ `t_outcome`: list of the model outcomes

And two methods:

+ `apply_`: How the game acts after a play move`
+ `init`: How the game is initialized

### Model wrap

`model_wrap` from `model_wrap.py` is a helper that takes an instance of
your class and returns a record that you can give
to the <span className="entrypointName">new_model</span> entrypoint.

:::tip
`model_wrap` automatically [packs](/advanced/pack_unpack#packing-data)
and [unpacks](/advanced/pack_unpack#unpacking-data) your inputs and
outputs. So the game platform only stores [bytes](/types/bytes/).

The gameTester let you test your model without the wrapping process and channels complexity.
:::

### `init` lambda

The `init` lambda takes one parameter of the type defined in `t_init_input`.

It returns (by calling `sp.result`) a game state of the type defined in `t_game_state`.

### `apply_` lambda

The `apply_` lambda takes 4 parameters:

+ `move_data` of type t_move defined in t_move_data
+ `move_nb` of type nat: the id of the current move
+ `player` of type int: the id of the current player
+ `state` of type defined in t_game_state

It returns (by calling `sp.result`) a pair of

+ `new_state` of type defined in t_game_state
+ `outcome` (see [outcomes](/guides/state_channels/new_game#outcomes))

:::info
The outcome returned by `apply_` is surrounded by `sp.bounded` because
`model_wrap.py` verifies that it is listed in `self.t_outcome`.
:::

### Example

Example with the simplest model: `transfer.py`

``` python
import smartpy as sp

class Transfer:
    def __init__(self):
        self.name = "Transfer"
        self.t_init_input = sp.TUnit
        self.t_game_state = sp.TUnit
        self.t_move_data  = sp.TUnit
        self.t_outcome    = ["transferred"]

    def apply_(self, move_data, move_nb, player, state):
        sp.result(
            sp.pair(
                sp.unit,
                sp.some(sp.bounded("transferred"))
            )
        )

    def init(self, params):
        sp.result(sp.unit)
```

The rest is SmartPy code that you can learn and try on
[SmartPy.io](https://smartpy.io).<br/>

We've build a [lot of models](/guides/state_channels/models/models) that can be used as
examples. Also, you can always contact us on
[Telegram](https://t.me/SmartPy_io)

## Game tester

Game tester is a little platform that let you test your models
in a simple environment. Examples of its usage can be found in
the [tictactoe template](https://smartpy.io/ide?template=state_channel_games/models/tictactoe.py)

## Models permissions

Models can have permissions to mint platform tokens. see [tokens](/guides/state_channels/more/tokens#model-permissions)
