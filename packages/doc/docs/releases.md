# Releases

## v0.16.0
|   |   |
|---|---|
| Date | 2022-12-15 |
| Commit  | 8f134ebb649f5a7b37c44fca8f336f970f523565 |
| Link  | [v0.16.0](https://smartpy.io/releases/20221215-8f134ebb649f5a7b37c44fca8f336f970f523565) |

### Change Log

- Support for mutation testing, see
  [guide](/guides/mutation_testing). For performance reasons this
  feature is CLI-only.

- Support for the Lima protocol, activated by default. New constructs
  [`sp.build_lambda(..., recursive=True)`](/types/lambdas) and
  [`sp.private_lambda(..., recursive=True)`](/types/lambdas). See
  templates [lambdas](https://smartpy.io/ide?template=lambdas.py) and
  [test_private_lambda](https://smartpy.io/ide?template=test_private_lambda.py).
  Return type of `TICKET` is now an option.

#### Breaking changes

#### Bug fixes

## v0.15.0
|   |   |
|---|---|
| Date | 2022-10-26 |
| Commit  | 28e8c18e46035c353804eb5fd725573c5d434e8a |
| Link  | [v0.15.0](https://smartpy.io/releases/20221026-28e8c18e46035c353804eb5fd725573c5d434e8a) |

### Change Log

- Internal upgrades in our mockup tests, docker on MacOS.

- Improvements in the Wallet, new account management.

- Improvements in our Internal primitives.

#### Breaking changes

#### Bug fixes

- Fixing layout renamings.

- Fix automatic tab selection in editors.

## v0.14.0
|   |   |
|---|---|
| Date | 2022-09-26 |
| Commit  | 1c748c4572188f65a525792468e37da2182f18a2 |
| Link  | [v0.14.0](https://smartpy.io/releases/20220926-1c748c4572188f65a525792468e37da2182f18a2) |

### Change Log

- Kathmandu protocol is now by default

- Fix fees and gas computation with Kathmandu

- Restore tab stability in [Michelson IDE](https://smartpy.io/michelson)

- events handling [`sp.emit`](/types/events)

#### Breaking changes

#### Bug fixes

## v0.13.0
|   |   |
|---|---|
| Date | 2022-08-19 |
| Commit  | a1f12855a0f6348c6e5a7a209dfb48cba81b10db |
| Link  | [v0.13.0](https://smartpy.io/releases/20220819-a1f12855a0f6348c6e5a7a209dfb48cba81b10db) |

### Change Log

- New React version of the [explorer](pathname:///../explorer).

#### Breaking changes

#### Bug fixes

## v0.12.0
|   |   |
|---|---|
| Date | 20220802 |
| Commit  | a20c6dc7989710fe9adb666cb34f6ad5f1c9512f |
| Link  | [v0.12.0](https://smartpy.io/releases/20220802-a20c6dc7989710fe9adb666cb34f6ad5f1c9512f) |

### Change Log

- Lazy entry points. The [sp.entrypoint_map()](/introduction/entrypoints.md/#the-entrypoint-map) is now accessible.

- New Michelson-like [`sp.add`](/types/integers) and [`sp.compare`](/types/comparison_operators) instructions.

- New `parameter_type` argument in [entrypoints](/introduction/entrypoints.md).

- Documentation of several key [templates](/pdoc/).

- Ghostnet in explorer and origination.

#### Breaking changes

#### Bug fixes

- Types fixed in minimal FA2 examples.

- Fix bls-12-381 multiplication bug.

## v0.11.1
|   |   |
|---|---|
| Date | 2022-06-07 |
| Commit  | 708da61f52c9d66c88f593ffc2915c52545d6090 |
| Link  | [v0.11.1](https://smartpy.io/releases/20220607-708da61f52c9d66c88f593ffc2915c52545d6090) |

### Change Log

- Bring back search bar from documentation which had disappeared.
- Renaming and cleanup of the [baking swap template](https://smartpy.io/ide?template=baking_swap.py)

#### Breaking changes

#### Bug fixes

## v0.11.0
|   |   |
|---|---|
| Date | 2022-06-05 |
| Commit  | 75fe749c73b119afe90e60e27644c8fea64350fd |
| Link  | [v0.11.0](https://smartpy.io/releases/20220605-75fe749c73b119afe90e60e27644c8fea64350fd) |

### Change Log

- New `doc` target in SmartPy.sh.
- Fix of many bugs in Explorer and switch to TzKT for contract
  operations indexer.
- Many important internals improvements (typing, values, etc.).
- [FA2 library](/guides/FA/FA2/overview) is no longer experimental.
  Minor improvements in library and documentation.
- Support for Jakartanet and deprecation of Hangzhounet.

#### Breaking changes

- Changes in internal s-expr manipulations.

#### Bug fixes

## v0.10.1
|   |   |
|---|---|
| Date | 2022-04-05 |
| Commit  | 79018120fafa35774b674ec4de0aebd19409219d |
| Link  | [v0.10.1](https://smartpy.io/releases/20220405-79018120fafa35774b674ec4de0aebd19409219d) |

### Change Log

- Update Taquito to support Ithaca new operations in Origination.
- sp.sub_mutez documentation.

#### Breaking changes

#### Bug fixes

## v0.10.0
|   |   |
|---|---|
| Date | 2022-04-01 |
| Commit  | 6ddd6c3f7d2fb374406422d062e83c10b2ab3e91 |
| Link  | [v0.10.0](https://smartpy.io/releases/20220401-6ddd6c3f7d2fb374406422d062e83c10b2ab3e91) |

### Change Log

- Support for Ithaca protocol (on by default).
- Support for `SUB_MUTEZ` / `sp.sub_mutez`.
- Support for map on options.

#### Breaking changes

#### Bug fixes

- Fix generation of views in `sp.create_contract`.
- Fix explorer with no named entry point.

## v0.9.1
|   |   |
|---|---|
| Date | 2022-03-04 |
| Commit  | 1d99a0cdb3939bfa03709850769147324e16449f |
| Link  | [v0.9.1](https://smartpy.io/releases/20220304-1d99a0cdb3939bfa03709850769147324e16449f) |

### Change Log

- **Important.** New experimental [FA2 library](/guides/FA/FA2/overview) that will replace our
  legacy FA2 template.
- Entry points get a new argument `check_no_incoming_transfer` to
  determine whether to check for a zero `amount` or not.
- New `default-check-no-incoming-transfer` [flag](/general/flags.md)
  to determine the default value of `check_no_incoming_transfer` in
  entry points.
- Add `stop-on-error` [flag](/general/flags.md) to control early
  termination on errors.
- More explicit error reporting in contracts (specially for missing
  type information).
- Contract id and accessors such as `address`, `baker`, `data`, `typed`, and `balance`
  are now only visible once in a scenario.
- New ConseilJS version + better exception handling in [explorer](pathname:///../explorer.html).
- Automatically remove instructions after a
  [`sp.failwith`](/general/raising_exceptions/#failwith).

#### Breaking changes

#### Bug fixes
- The [nodes page](pathname:///../nodes) is better synchronized with SmartPy's nodes.

## v0.9.0
|   |   |
|---|---|
| Date | 2022-02-08 |
| Commit  | 48ec25e86a7834c3098dd72313c6d539acba201a |
| Link  | [v0.9.0](https://smartpy.io/releases/20220208-48ec25e86a7834c3098dd72313c6d539acba201a) |

### Change Log

- New code optimizer rule to slightly shrink lazy entry points code
  when multiple lazy entry points are defined.
- Removal of useless comments in generated Michelson code.
- We can now properly define entry points and views with the same
  names.
- Explorer / simulation. Parsing of parameters in micheline or JSON formats,
  with or without entry points.
- Explorer / simulation. Parsing of Lambdas in micheline format.
- Scenario [compute](/scenarios/testing#computing-expressions) can
  now change context (`sender`, `source`, `now`, `level`, etc.).

#### Breaking changes

#### Bug fixes

## v0.8.10
|   |   |
|---|---|
| Date | 2022-01-13 |
| Commit  | a3a64672bc25d41cc54d3c6b05240c6da1bdc5a6 |
| Link  | [v0.8.10](https://smartpy.io/releases/20220113-a3a64672bc25d41cc54d3c6b05240c6da1bdc5a6) |

### Change Log

- Fix [int / nat](/types/integers/#int--nat-operations) documentation.
- Wallet updates to recent versions to fix origination issues on the mainnet.
- Fix [TZComet](https://tzcomet.io) URL in explorer.

#### Breaking changes

#### Bug fixes

## v0.8.9
|   |   |
|---|---|
| Date | 2022-01-10 |
| Commit  | 9b78ca7747e956ef2f8dbbc5646d3e0e4fa0d6d4 |
| Link  | [v0.8.9](https://smartpy.io/releases/20220110-9b78ca7747e956ef2f8dbbc5646d3e0e4fa0d6d4) |

### Change Log

#### Breaking changes

#### Bug fixes

- Fix balance bug introduced in 0.8.8.

## v0.8.8
|   |   |
|---|---|
| Date | 2022-01-09 |
| Commit  | d2e97d98e428914eebdcfde0fef2d6bc6194bc16 |
| Link  | [v0.8.8](https://smartpy.io/releases/20220109-d2e97d98e428914eebdcfde0fef2d6bc6194bc16) |

### Change Log

#### Breaking changes

#### Bug fixes

- Fix balance computations between operations created by an entry point.

## v0.8.7
|   |   |
|---|---|
| Date | 2021-12-31 |
| Commit  | b9b4aa1c1fbad60844a51a1451abeaa03e1f432c |
| Link  | [v0.8.7](https://smartpy.io/releases/20211231-b9b4aa1c1fbad60844a51a1451abeaa03e1f432c) |

### Change Log

- Improvements in SmartML install procedure in the CLI.
- Improvements in error reporting, mostly in the SmartPy.io editor.

#### Breaking changes

#### Bug fixes

- Fix originator bug with lazy entry points (older taquito version was failing).

## v0.8.6
|   |   |
|---|---|
| Date | 2021-12-29 |
| Commit  | 3e3d4547dece8d65b8323184c00e8b0cb9ccd4b3 |
| Link  | [v0.8.6](https://smartpy.io/releases/20211229-3e3d4547dece8d65b8323184c00e8b0cb9ccd4b3) |

### Change Log

- Some typos fixed in documentation.
- Many improvements in state channel game platform (see
  documentation).

#### Breaking changes

#### Bug fixes

- Views: fix the code optimizer on views that were wrongly annotated as
  non failing.
- Views: fix a bug that mixed up view contracts with inheritance.
- Explorer: fix operations.

## v0.8.5
|   |   |
|---|---|
| Date | 2021-12-16 |
| Commit  | eaa92a2362bef28b282666334fac580b00fc03db |
| Link  | [v0.8.5](https://smartpy.io/releases/20211216-eaa92a2362bef28b282666334fac580b00fc03db) |

### Change Log

- Documentation of [operations and internals](/types/operations).
- Documentation of all three [view](/general/views) concepts (CPS,
  off-chain and on-chain).
- Documentation of [lambda](/types/lambdas) effects / contexts.
- Documentation of `sp.cons` in [lists](/types/lists#push-an-element-on-top-of-a-list).
- Links to new faucet [wallet](https://smartpy.io/wallet) faucet importer.
- Hangzhou is now the default protocol, introduction of Ithaca
  (equivalent to Hangzhou as of now).
- Remove Granadanet and Introduce Ithacanet in the UI (origination,
  wallet and explorer).
- New `view-check-exception` [flag](/general/flags.md) to control views error semantics.
- New [Admin Multisig template](https://smartpy.io/ide?template=admin_multisig.py).

#### Breaking changes

- The interpreter now checks that mutez don't become negative when
  substracting them.
- Layout information for record types is now checked.
- Cleaner way to change the name of a [view](/general/views).

#### Bug fixes

## v0.8.4
|   |   |
|---|---|
| Date | 2021-11-19 |
| Commit  | 23e4d3e4c53d5a934a14ca6d8da4c336ddf3fbf5 |
| Link  | [v0.8.4](https://smartpy.io/releases/20211121-23e4d3e4c53d5a934a14ca6d8da4c336ddf3fbf5)  |

### Change Log

- Support integer bit-wise operations `&` `|` on test interpreter;
- Remove double-rewriting of views;
- `CONCAT` is now resolved during type checking.

**`SmartTS:`**

- Add compilation diagnostics.


#### Breaking changes

- Decorators `@sp.global_lambda` and `@sp.sub_entry_point` were deprecated in favor of `@sp.private_lambda`, which centralizes both concepts (See [Lambdas](/types/lambdas/#define-a-private-lambda));

#### Bug fixes

- Properly load configuration flags in scenarios;
- Explorer Page - Call parameters now include the entry point name when using the SmartPy wallet.

## v0.8.3
|   |   |
|---|---|
| Date | 2021-11-06 |
| Commit  | 51985c20a6a77e31b6de0d3b0400cccce74e38ad |
| Link  | [v0.8.3](https://smartpy.io/releases/20211106-51985c20a6a77e31b6de0d3b0400cccce74e38ad)  |

### Change Log

- Remove deprecated test network (florencenet);
- Improve the precision of line numbers in error messages;
- Make evaluation order of all binops right-to-left;
- Reworks to `sp.contract` annotation and type checking. The default entry_point is better handled now;
- Adds new `contract-check-exception` [Flag](https://smartpy.io/docs/general/flags);
- Adds an [FA2 guide](https://smartpy.io/docs/guides/FA/FA2);
- Calling entry points in the explorer page now uses the entry point name instead of `default`.

`SmartTS`:
- Adds support for nested imports.

#### Breaking changes

#### Bug fixes

- Fix source and sender for follow-up calls;
- Fix comparison between address values:
  - `addresses of implicit accounts are strictly less than addresses of originated accounts`
  - `addresses of the same type are compared lexicographically`
- Fix a Brython bug for attributes called 'message';
- Adds missing `@smartpy/timelock` dependency.

`SmartTS`:

- Solves an issue related to variable resolution in test scenarios;


## v0.8.2
|   |   |
|---|---|
| Date | 2021-10-18 |
| Commit  | d17b6cf3713bf3fe927625398c202aa90a3a79cb |
| Link  | [v0.8.2](https://smartpy.io/releases/20211018-d17b6cf3713bf3fe927625398c202aa90a3a79cb) |

### Change Log

- `SmartML` dialect is now available in the [online IDE](https://smartpy.io/ml-ide); ([API documentation](https://smartpy.io/SmartML_docs))
- A few templates have been added for the SmartML dialect;
- Updates the `mint` entry point in [FA2 template](https://smartpy.io/ide?template=FA2.py);
- Adds the [timelock](/experimental/timelock) feature;
- In the online IDE, the editor and output panel can now be resizable. ([Feature Request](https://gitlab.com/SmartPy/smartpy/-/issues/15));
- `Hangzhou` primitives have been added to [Michelson IDE](https://smartpy.io/michelson).

#### Breaking changes

#### Bug fixes

- `DIP n {...}` is now valid when `n` is less or equal to the stack size;
- The test interpreter now supports single annotated entry points.

**`SmartTS`**

- Adds missing type resolutions for access expressions.

## v0.8.1
|   |   |
|---|---|
| Date | 2021-10-04 |
| Commit  | ea717461f93381b75961d6b3456cd114138f42c0 |
| Link  | [v0.8.1](https://smartpy.io/releases/20211004-ea717461f93381b75961d6b3456cd114138f42c0)  |

### Change Log

Improves frontend loading times.

#### Breaking changes

#### Bug fixes

## v0.8.0
|   |   |
|---|---|
| Date | 2021-09-29 |
| Commit  | ec4c2020b1e18201600a732d442303c6830f8995 |
| Link  | [v0.8.0](https://smartpy.io/releases/20210929-ec4c2020b1e18201600a732d442303c6830f8995)  |

### Change Log

- Adds support for [on-chain views](/general/views); (`SmartPy`, `SmartTS`)
- Adds support for [global constants](/experimental/global_constants) to be used as values; (`SmartPy`, `SmartTS`)
- Various improvements to SmartML dialect.

#### Breaking changes

- Method `sp.to_constant` was renamed to [`sp.resolve`](/introduction/constants_vs_expressions). This change is necessary to avoid confusion with the [constants feature](https://tezos.gitlab.io/protocols/alpha.html#global-constants) coming with `H proposal`.

#### Bug fixes

## v0.7.5
|   |   |
|---|---|
| Date | 2021-09-17 |
| Commit  | 545ba21ad167a0c2d9f7e99ce41a5015876cf62e |
| Link  | [v0.7.5](https://smartpy.io/releases/20210917-545ba21ad167a0c2d9f7e99ce41a5015876cf62e)  |

### Change Log

- An early version of the Ocaml dialect is available through the [CLI](https://smartpy.io/docs/cli);
- Introduces mechanisms to test off-chain views directly in test scenarios ([Documentation](/scenarios/testing#views));
- The [wallet page](https://smartpy.io/wallet) has been reworked;
- SmartPy.io frontend bundle received a few optimizations.

#### Breaking changes

#### Bug fixes

- Fix content forging in the origination page. (Strings with multiple spaces could cause the signature to be invalid.)

## v0.7.4
|   |   |
|---|---|
| Date | 2021-09-04 |
| Commit  | 98c3fb1314a5298a5000fe3801d0b57238469670 |
| Link  | [v0.7.4](https://smartpy.io/releases/20210904-98c3fb1314a5298a5000fe3801d0b57238469670)  |

### Change Log

#### Breaking changes

#### Bug fixes

- Adds support for `sp.TBounded` arguments in tests.

## v0.7.3
|   |   |
|---|---|
| Date | 2021-09-02 |
| Commit  | e593114259e42c0e429189cff0b967c0132fdc9a |
| Link  | [v0.7.3](https://smartpy.io/releases/20210902-e593114259e42c0e429189cff0b967c0132fdc9a)  |

### Change Log

- Removes `Edonet` node;
- Adds type annotations to the metadata builder;

**`Python`**

- Adds an helper method `sp.utils.seconds_of_timestamp(<timestamp>)` for extracting seconds of type [sp.TNat](/general/types#nat) from timestamps;
- Adds documentation for overload multiplication `sp.mul(<expr1>, <expr2>)`;

**`Typescript`**

- Adds `<int>.abs()` instruction;
- Makes origination parameters less restrictive in test scenarios;
- Adds an helper method `<timestamp>.toSeconds()` for extracting seconds of type [TNat](/general/types#nat) from timestamps;

#### Breaking changes

#### Bug fixes

**`Typescript`**

- Fixes a few exceptions related to variants;
- Fixes the documentation related to the `unpack` instruction;
- Fixes a few exceptions occurring in access expressions;

## v0.7.2
|   |   |
|---|---|
| Date | 2021-08-21  |
| Commit  | 7598ba9c6d786680318a38592d3f35178fb1dde5 |
| Link  | [v0.7.2](https://smartpy.io/releases/20210821-7598ba9c6d786680318a38592d3f35178fb1dde5)  |

### Change Log

**`Typescript`**

- Add [Sp.ediv](/types/mutez/#division) expression;
- Extend inlining support;
- Add update script to the [boilerplate](/introduction/project_management);
- Add better support for `as <type>` expressions;
- Add more type checks to the linter.

**`State channels`**

- Misc interface changes.

#### Breaking changes

#### Bug fixes

- Fix `TTimestamp` type-checking in Typescript dialect;
- Fix `TAddress` linting in Typescript dialect;
- Improve the messages of some cryptic errors.

## v0.7.1
|   |   |
|---|---|
| Date | 2021-08-18  |
| Commit  | 13167a59dea2dadd1e302825d0a83c8159ede071 |
| Link  | [v0.7.1](https://smartpy.io/releases/20210818-13167a59dea2dadd1e302825d0a83c8159ede071)  |

### Change Log

- Better error handling;

#### Breaking changes

#### Bug fixes

- Fix lambda packing/unpacking with records and variants;
- Fix `Sp.createContract` return type in the typescript dialect.

## v0.7.0
|   |   |
|---|---|
| Date | 2021-08-09  |
| Commit  | f661148f050609fa1075489aea5536f0a0c6fea8 |
| Link  | [v0.7.0](https://smartpy.io/releases/20210809-f661148f050609fa1075489aea5536f0a0c6fea8)  |

### Change Log

- Includes a beta version of the Typescript syntax called SmartTS;
- New [documentation](https://smartpy.io/docs), it now uses a new system to allow multiple syntaxes;
- Inlined Michelson in SmartPy is now interpreted.

#### Breaking changes

#### Bug fixes

## v0.6.11
|   |   |
|---|---|
| Date | 2021-07-10  |
| Commit  | 06baf4f9ae06f99dc60bab2a01cfee13f4a20c13  |
| Link  | [v0.6.11](https://smartpy.io/releases/20210710-06baf4f9ae06f99dc60bab2a01cfee13f4a20c13)  |

### Change Log

#### Breaking changes

#### Bug fixes

- Metadata builder now works when using lazy entry points;

- Option types now work as expected in explorer.html

## v0.6.10
|   |   |
|---|---|
| Date | 2021-07-08  |
| Commit  | 4662b0f8b1fe2186a243078f9f1ba0a4aa1c6f16  |
| Link  | [v0.6.10](https://smartpy.io/releases/20210708-4662b0f8b1fe2186a243078f9f1ba0a4aa1c6f16)  |

### Change Log

- The online IDE now uses monaco editor, it offers new features to ease the development experience;

- *Compiler:* The Michelson compiler no longer generates IF_SOME macros since they are not handled optimally by the Tezos client.

#### Breaking changes
- Contracts now enforce that initial flags do not appear in contracts (they were not taken into account but they could appear in contracts). They can only appear in the first steps of scenarios. [Flags](/general/flags.md#adding-flags-to-a-contract)
- `sp.now` and `sp.level` now keep the state from previous calls as default, instead of resetting to **zero**.

#### Bug fixes

- Fix `token_supply` configuration in the FA2 template;

## v0.6.9

|   |   |
|---|---|
| Date | 2021-06-30  |
| Commit  | d86b17adb6d0fb15c9dc009bf3a62de6665a9ae3  |
| Link  | [v0.6.9](https://smartpy.io/releases/20210630-d86b17adb6d0fb15c9dc009bf3a62de6665a9ae3)  |

### Change Log
  - Adds exception testing, users are now able to link:reference.html#_registering_and_displaying_calls_to_entry_points[test error messages] emitted by failed contract calls;
  - File names in error location;
  - Online IDE now divides tests and compilation targets into two distinct sections;
#### Bug Fixes
  - `originate-contract` CLI command can now be executed from any sub-folder;
  - `scenario.simulation` debugging feature is now working again;
  - Fixes an issue in the explorer page that would cause an exception when interpreting tuples;
  - Allow invalid `packed` bytes to be tested with `run(valid = False)`;
  - **(A work in progress)** New state channel based game platform templates:
    - [Game platform](https://smartpy.io/ide?template=state_channel_games/game_platform.py)
    - [Game tester](https://smartpy.io/ide?template=state_channel_games/game_tester.py)
    - [Game wrapper](https://smartpy.io/ide?template=state_channel_games/model_wrap.py)
    - [Game types](https://smartpy.io/ide?template=state_channel_games/types.py)
    - [Game: Head and Tail model](https://smartpy.io/ide?template=state_channel_games/models/head_tail.py)
    - [Game: Nim model](https://smartpy.io/ide?template=state_channel_games/models/nim.py)
    - [Game: tic-tac-toe model](https://smartpy.io/ide?template=state_channel_games/models/tictactoe.py)
    - [Game: transfer model](https://smartpy.io/ide?template=state_channel_games/models/transfer.py)

## v0.6.8

|   |   |
|---|---|
| Date | 2021-06-09  |
| Commit  | d64964633e98c1bd1fe8beb9f83138185cabdf90  |
| Link  | [v0.6.8](https://smartpy.io/releases/20210609-d64964633e98c1bd1fe8beb9f83138185cabdf90)  |

### Change Log

  - SmartPy CLI now shows all created operations in log.txt (it used to only show recursive operations).
  - More documentation and examples of lazy and updatable entry points.

#### Bug Fixes
  - Fix CLI originate-contract command.

## Previous Releases

View [previous releases](https://smartpy.io/releases.html).
