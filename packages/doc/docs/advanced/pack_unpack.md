# Pack and Unpack

import MichelsonDocLink from '@site/src/components/MichelsonDocLink';
import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

[Pack](/advanced/pack_unpack#packing-data) and [Unpack](/advanced/pack_unpack#unpacking-data) are used for serializing/deserializing pieces of data. It heavily relies on [sp.TBytes](/types/bytes) type.

- `Packing` Converts a given michelson value into [sp.TBytes](/general/types#bytes);

- `Unpacking` Does the inverse of **pack**, by deserializing [sp.TBytes](/general/types#bytes) back to the michelson value.

It is useful when building generic smart contracts with entry-points expecting [sp.TBytes](/general/types#bytes) to then deserialize it to an internal structure. Also necessary when verifying signed contents.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

[Pack](/advanced/pack_unpack#packing-data) and [Unpack](/advanced/pack_unpack#unpacking-data) are used for serializing/deserializing pieces of data. It heavily relies on [TBytes](/types/bytes) type.

- `Packing` Converts a given michelson value into [TBytes](/general/types#bytes);

- `Unpacking` Does the inverse of **pack**, by deserializing [TBytes](/general/types#bytes) back to the michelson value.

It is useful when building generic smart contracts with entry-points expecting [TBytes](/general/types#bytes) to then deserialize it to an internal structure. Also necessary when verifying signed contents.

See reference [Packing](https://smartpy.io/ts-ide?template=PackUnpack.ts) template.

</Snippet>


## Packing data

<Snippet syntax={SYNTAX.PY}>

`sp.pack(<data>)`

Serialize a piece of data `<data>` to its optimized binary representation of type [sp.TBytes](/general/types#bytes).

```python
someRecord = sp.record(x = 1)
bytes = sp.pack(someRecord)
```

Return an object of type [sp.TBytes](/general/types#bytes).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`Sp.pack(<data>)`

Serialize a piece of data `<data>` to its optimized binary representation of type [TBytes](/general/types#bytes).

```typescript
const someRecord = { x: 1 }
const bytes: TBytes = Sp.pack(someRecord)
```

Return an object of type [TBytes](/general/types#bytes).

</Snippet>

<MichelsonDocLink placeholder="PACK" url="https://tezos.gitlab.io/michelson-reference/#instr-PACK"/>

## Unpacking Data

<Snippet syntax={SYNTAX.PY}>

`sp.unpack(<bytes>, t=None)`

Parse the serialized data from its optimized binary representation of type [sp.TBytes](/general/types#bytes) into its original form.

There is an optional argument `t` to specify the type of its deserialized form.

```python
someRecord = sp.unpack(someRecord, sp.TNat)
```

Return an object of type `sp.TOption(sp.TNat)`

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`Sp.unpack<t>(<bytes>)`

Parse the serialized data from its optimized binary representation of type [TBytes](/general/types#bytes) into its original form.

There is an argument `t` to specify the type of its deserialized form.

```typescript
const someRecord: TOption<TNat> = Sp.unpack<TNat>(bytes)
```

Return an object of type `TOption<TNat>`

</Snippet>


<MichelsonDocLink placeholder="UNPACK" url="https://tezos.gitlab.io/michelson-reference/#instr-UNPACK"/>
