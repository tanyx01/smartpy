# Debugging Contracts

import Snippet, {SYNTAX} from '@site/src/components/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

## trace

Compute an expression and print its value to std::out (or the console in a web-browser).

```python
sp.trace(<expression>)
```

## simulation

Graphical user interface and step by step simulation in a web-browser.

```python
scenario.simulation(<contract>)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress...
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
