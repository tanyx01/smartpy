const baseURL = '/' + (process.env.BASE_URL || '');
const rootURL = baseURL.replace('docs/', '');

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
    trailingSlash: true,
    title: 'SmartPy Documentation',
    tagline: 'SmartPy Docs',
    url: 'https://smartpy.io',
    baseUrl: baseURL,
    onBrokenLinks: 'warn',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/favicon.ico',
    organizationName: 'SmartPy',
    projectName: 'SmartPy',
    themeConfig: {
        autoCollapseSidebarCategories: true,
        announcementBar: {
            id: 'old_docs',
            content:
                'Former documentation can be found <a target="_blank" rel="noopener noreferrer" href="https://smartpy.io/reference.html">here</a>.',
            backgroundColor: '#ffba00',
        },
        navbar: {
            logo: {
                alt: 'SmartPy',
                src: 'img/logo.svg',
                srcDark: 'img/logo_dark.svg',
                href: 'https://smartpy.io',
                target: '_blank',
            },
            items: [
                /* Uncomment once we have multiple versions
                {
                    type: 'docsVersionDropdown',
                    //// Optional
                    position: 'left',
                    // Add additional dropdown items at the beginning/end of the dropdown.
                    dropdownItemsBefore: [],
                    dropdownItemsAfter: [],
                    // Do not add the link active class when browsing docs.
                    dropdownActiveClassDisabled: true,
                },
                */
                {
                    type: 'doc',
                    docId: 'introduction/overview',
                    position: 'left',
                    label: 'Reference Manual',
                    docsPluginId: 'default',
                },
                {
                    type: 'doc',
                    docId: 'guides/index',
                    position: 'left',
                    label: 'Guides',
                },
                {
                    type: 'doc',
                    docId: 'pdoc',
                    position: 'left',
                    label: 'Templates',
                },
                /* Uncomment to share templates
                {
                    type: 'doc',
                    docId: 'templates/FA2',
                    position: 'left',
                    label: 'Templates',
                },
                */
                {
                    type: 'doc',
                    docId: 'releases',
                    position: 'right',
                    label: 'Releases',
                },
                {
                    label: 'CLI',
                    position: 'right',
                    to: '/cli',
                },
                {
                    href: 'pathname:///../ide',
                    target: '_blank',
                    label: 'IDE',
                    position: 'right',
                },
                {
                    href: 'https://gitlab.com/smartpy/smartpy',
                    label: 'Gitlab',
                    position: 'right',
                },
            ],
        },
        footer: {
            links: [
                {
                    title: 'Docs',
                    items: [
                        {
                            label: 'Docs',
                            to: '/',
                        },
                        {
                            label: 'Releases',
                            to: '/releases',
                        },
                    ],
                },
                {
                    title: 'Community',
                    items: [
                        {
                            label: 'Tezos Stack Exchange',
                            href: 'https://tezos.stackexchange.com/questions/ask?tags=smartpy',
                        },
                        {
                            label: 'Telegram',
                            href: 'https://t.me/SmartPy_io',
                        },
                        {
                            label: 'Twitter',
                            href: 'https://twitter.com/SmartPy_io',
                        },
                    ],
                },
                {
                    title: 'More',
                    items: [
                        {
                            label: 'Medium',
                            href: 'https://smartpy-io.medium.com',
                        },
                        {
                            label: 'GitLab',
                            href: 'https://gitlab.com/smartpy/smartpy',
                        },
                    ],
                },
            ],
            copyright: `Copyright © ${new Date().getFullYear()} SmartPy.`,
        },
        algolia: {
            apiKey: '3258d9cdfae64e48185f4a150e15aca1',
            indexName: 'smartpy',

            // Optional: see doc section below
            contextualSearch: false,

            // Optional: Algolia search parameters
            searchParameters: {},
        },
        prism: {
            theme: require('prism-react-renderer/themes/dracula'),
            additionalLanguages: ['ocaml', 'python', 'bash'],
        },
    },
    presets: [
        [
            '@docusaurus/preset-classic',
            {
                docs: {
                    routeBasePath: '/',
                    sidebarPath: require.resolve('./sidebars.js'),
                    editUrl: undefined,
                },
                theme: {
                    customCss: [
                        require.resolve('./src/css/custom.css'),
                        'prismjs/themes/prism-dark.css',
                    ],
                },
            },
        ],
    ],
    i18n: {
        defaultLocale: 'en',
        locales: ['en'],
    },
};
