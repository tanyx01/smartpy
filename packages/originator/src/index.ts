import Commander from './commands';
import info from '../package.json';

Commander.version(info.version);

Commander.parse(process.argv);
