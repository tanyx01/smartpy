import Commander from '../commands';

// Origination can take a few seconds
jest.setTimeout(92000);

const SECRET_KEY = 'edskRsk6poVViybe3kujNJafTxUef7HDcKE6wPGw7PTZZhYBULJXt8TX2aNRsujiw9Zf5tEqdxnirReK4o3qF4KABuUDxKvZXb';
const TEZOS_RPC = 'https://ghostnet.smartpy.io';

describe('Originate Contract', () => {
    it('Michelson format (.json)', async () => {
        Commander.parse([
            '',
            '',
            'originate-contract',
            '--code',
            './src/__tests__/code.json',
            '--storage',
            './src/__tests__/storage.json',
            '--rpc',
            TEZOS_RPC,
            '--private-key',
            SECRET_KEY,
        ]);

        // Await for the result (60 seconds)
        await new Promise((r) => setInterval(r, 30000));
    });
    it('Micheline format (.tz)', async () => {
        Commander.parse([
            '',
            '',
            'originate-contract',
            '--code',
            './src/__tests__/code.tz',
            '--storage',
            './src/__tests__/storage.tz',
            '--rpc',
            TEZOS_RPC,
            '--private-key',
            SECRET_KEY,
        ]);

        // Await for the result (60 seconds)
        await new Promise((r) => setInterval(r, 30000));
    });
    it('Load private key from faucet', async () => {
        Commander.parse([
            '',
            '',
            'originate-contract',
            '--code',
            './src/__tests__/code.tz',
            '--storage',
            './src/__tests__/storage.tz',
            '--rpc',
            TEZOS_RPC,
        ]);

        // Await for the result (60 seconds)
        await new Promise((r) => setInterval(r, 30000));
    });
});
