import React, { Suspense } from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import { createMemoryHistory, History } from 'history';
import { Router } from 'react-router-dom';

import { createStore } from 'src/store';
import RouterComponent from 'src/features/navigation/containers/Router';
import { HelmetProvider } from 'react-helmet-async';
import ThemeProvider from 'src/features/theme/context/ThemeProvider';

interface Options {
    route: string;
    history?: History;
}

const renderWithRouter = ({ route, history = createMemoryHistory({ initialEntries: [route] }) }: Options) => {
    const { store } = createStore(history);

    const Wrapper: React.FC = () => (
        <Provider store={store}>
            <HelmetProvider>
                <Router history={history}>
                    <Suspense fallback={<div />}>
                        <ThemeProvider>
                            <RouterComponent />
                        </ThemeProvider>
                    </Suspense>
                </Router>
            </HelmetProvider>
        </Provider>
    );

    return {
        ...render(<Wrapper />),
        // Added `history` to the returned utilities to allow us
        // to reference it in our tests.
        history,
    };
};

export default renderWithRouter;
