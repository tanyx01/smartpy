import React, { Suspense } from 'react';
import { act } from 'react-dom/test-utils';

import WalletView from 'src/features/wallet/views/Wallet';
import renderWithStore from '../test-helpers/renderWithStore';

describe('Wallet Page', () => {
    it('Wallet page renders correctly', () => {
        let container;
        act(() => {
            container = renderWithStore(
                <Suspense fallback={'...'}>
                    <WalletView />
                </Suspense>,
            ).container;
        });
        expect(container).toMatchSnapshot();
    });
});
