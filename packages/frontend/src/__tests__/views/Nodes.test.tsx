import React, { Suspense } from 'react';
import { act } from 'react-dom/test-utils';

import Nodes from '../../features/nodes/views/Nodes';
import renderWithStore from '../test-helpers/renderWithStore';

describe('Nodes Page', () => {
    it('Nodes page renders correctly', () => {
        let container;
        act(() => {
            container = renderWithStore(
                <Suspense fallback={'...'}>
                    <Nodes />
                </Suspense>,
            ).container;
        });
        expect(container).toMatchSnapshot();
    });
});
