import editorActions from '../features/py-ide/actions';
import tsEditorActions from '../features/ts-ide/actions';
import mlEditorActions from '../features/ml-ide/actions';
import michelsonEditorActions from '../features/mich-ide/actions';
import walletActions from '../features/wallet/actions';
import themeActions from '../features/theme/actions';
import i18nActions from '../features/i18n/actions';
import originationActions from '../features/origination/actions';
import explorerActions from '../features/explorer/actions';

const actions = {
    editor: editorActions,
    mlEditor: mlEditorActions,
    tsEditor: tsEditorActions,
    michelsonEditor: michelsonEditorActions,
    theme: themeActions,
    wallet: walletActions,
    origination: originationActions,
    explorer: explorerActions,
    i18n: i18nActions,
};

export default actions;
