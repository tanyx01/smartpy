import { combineReducers } from 'redux';
import { History } from 'history';

import editorReducer from '../features/py-ide/reducer';
import tsEditorReducer from '../features/ts-ide/reducer';
import mlEditorReducer from '../features/ml-ide/reducer';
import michelsonEditorReducer from '../features/mich-ide/reducer';
import walletReducer from '../features/wallet/reducer';
import themeReducer from '../features/theme/reducer';
import i18nReducer from '../features/i18n/reducer';
import originationReducer from '../features/origination/reducer';
import explorerReducer from '../features/explorer/reducer';

const rootReducer = (history: History) =>
    combineReducers({
        editor: editorReducer,
        tsIDE: tsEditorReducer,
        mlIDE: mlEditorReducer,
        michelsonEditor: michelsonEditorReducer,
        theme: themeReducer,
        wallet: walletReducer,
        origination: originationReducer,
        explorer: explorerReducer,
        i18n: i18nReducer,
    });

export default rootReducer;
