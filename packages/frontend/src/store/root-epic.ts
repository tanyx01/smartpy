import { combineEpics } from 'redux-observable';

import * as editorEpics from '../features/py-ide/epics';
import * as tsEditorEpics from '../features/ts-ide/epics';
import * as mlEditorEpics from '../features/ml-ide/epics';
import * as michelsonEditorEpics from '../features/mich-ide/epics';

export default combineEpics(
    ...[
        ...Object.values(editorEpics),
        ...Object.values(tsEditorEpics),
        ...Object.values(mlEditorEpics),
        ...Object.values(michelsonEditorEpics),
    ],
);
