import { RootAction, RootState, Services } from 'SmartPyTypes';
import { createStore as createReduxStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { createBrowserHistory } from 'history';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { composeEnhancers } from './utils';
import rootReducer from './root-reducer';
import rootEpic from './root-epic';
import services from '../services';
import { VERSION } from './version';

const persistConfig = {
    key: 'smartpy',
    storage,
    blacklist: ['explorer', 'mlIDE', 'tsIDE', 'editor', 'michelsonEditor', 'wallet', 'origination'],
    version: VERSION,
};

// Rehydrate state on app start
const initialState = {};

export const createStore = (history = createBrowserHistory({ basename: process.env.PUBLIC_URL })) => {
    const persistedReducer = persistReducer(persistConfig, rootReducer(history));

    const epicMiddleware = createEpicMiddleware<RootAction, RootAction, RootState, Services>({
        dependencies: services,
    });
    // Configure middlewares
    const middlewares = [epicMiddleware];
    // Compose enhancers
    const enhancer = composeEnhancers(applyMiddleware(...middlewares));

    const store = createReduxStore(persistedReducer, initialState, enhancer);
    const persistor = persistStore(store);

    epicMiddleware.run(rootEpic);

    return {
        store,
        history,
        persistor,
    };
};
