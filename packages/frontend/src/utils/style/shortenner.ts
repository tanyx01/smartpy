/**
 * @description Shorten text to fit in a max length.
 *
 * @param text text to shorten.
 * @param maxLength max length that the resultant output must have.
 *
 * @returns The shortened text in the following form <prefix>...<suffix>
 */
export const shortenText = (text: string, maxLength: number) => {
    if (text.length <= maxLength) {
        return text;
    }
    const realLength = maxLength - 3;
    const prefixLength = Math.floor(realLength * 0.6);
    const suffixLength = Math.floor(realLength * 0.4);

    const remaining = maxLength - (prefixLength + suffixLength + 3);

    return `${text.substr(0, prefixLength + remaining)}...${text.substr(text.length - suffixLength, suffixLength)}`;
};
