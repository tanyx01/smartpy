import logger from '../services/logger';

export interface ScriptProps {
    name: string;
    src: string;
    type: string;
    afterAppend?: () => void;
}

const asyncScriptInjector = async (
    scripts: ScriptProps[],
    onProgress: (percentage: number, loadedScript: string) => void,
    onFailure: (src: string) => void,
) => {
    let loadedScripts: string[] = [];
    const loadNewScript = async (script: ScriptProps) => {
        if (!loadedScripts.includes(script.name)) {
            await newScript(script)
                .then(() => {
                    loadedScripts = [...loadedScripts, script.name];
                    onProgress((loadedScripts.length * 100) / scripts.length, script.name);
                })
                .catch((e) => {
                    logger.debug(e);
                    onFailure(script.name);
                });
        }
    };

    for (const src of scripts) {
        await loadNewScript(src);
    }
};

const newScript = (script: ScriptProps) =>
    new Promise<void>((resolve, reject) => {
        if (findScript(script.name) === undefined) {
            const scriptElem = document.createElement('script');
            scriptElem.src = script.src;
            scriptElem.type = script.type;
            scriptElem.id = script.name;
            scriptElem.async = true;
            scriptElem.onload = () => resolve();
            scriptElem.onerror = reject;
            document.head.appendChild(scriptElem);
            script.afterAppend && script.afterAppend();
        } else {
            resolve();
        }
    });

/**
 * @summary Lookup <script id="..."/> in the HTML DOM.
 * @param {string} id Script Identifier.
 * @return {HTMLScriptElement} Script element.
 */
const findScript = (id: string) => Array.from(document.scripts).find((script) => script.id === id);

export default asyncScriptInjector;
