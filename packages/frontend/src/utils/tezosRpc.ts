import { Network } from '../constants/networks';
import * as RPC from '../constants/rpc';
import httpRequester from '../services/httpRequester';
import logger from '../services/logger';
import { CallError } from 'SmartPyModels';

/**
 * @description Get the RPC network.
 * @param {string} rpc - RPC address ( e.g. https://mainnet.smartpy.io )
 * @return {string} One of the following networks [MAINNET]
 */
export const getRpcNetwork = async (rpc: string): Promise<Network> => {
    const {
        network_version: { chain_name },
    } = await httpRequester
        .get(`${rpc}/version`)
        .then(({ data }) => data)
        .catch(async (e) => {
            logger.warn(e);
            return {
                network_version: await httpRequester.get(`${rpc}/network/version`),
            };
        });

    const network = chain_name.split('_')[1].replace('ITHACANET', 'GHOSTNET');
    if (!Object.keys(Network).includes(network)) {
        logger.error(`Unknown network: ${chain_name}.`);
        return Network.CUSTOM;
    }

    return Network[network as keyof typeof Network];
};

/**
 * @description Find in which network the contract is deployed.
 * @param {string} address Contract Address
 * @return {Promise<Network | void>} Network
 */
export const lookupContractNetwork = async (address: string): Promise<Network | void> => {
    const networks = Object.values(Network);
    for (const network of networks) {
        try {
            if (network in RPC.smartpy) {
                await httpRequester.get(
                    `${RPC.smartpy[network]}${RPC.Endpoint.CONTRACT_BALANCE.replace(':contract_id', address)}`,
                );
                return network;
            }
        } catch (e) {
            logger.debug(e);
        }
    }
    return Network.CUSTOM;
};

/**
 * @description Iterate over known RPC's to find a given contract by address
 * @param {string} address Contract address
 * @return {Promise<ContractResponse | void>} contract information
 */
export interface ContractResponse {
    rpc: string;
    contract: {
        balance: string;
        script: {
            code: unknown;
            storage: unknown;
        };
    };
}

export const lookupContract = async (address: string, rpc?: string): Promise<ContractResponse> => {
    const RPCs =
        rpc === undefined
            ? // Brute force lookup
              Object.values(Network)
                  .filter((rpc) => rpc in RPC.smartpy)
                  .map((network) => RPC.smartpy[network])
            : // Don't brute force if an RPC was explicitly provided
              [rpc];
    const errors: CallError[] = [];

    for (const rpc of RPCs) {
        try {
            return Promise.resolve(await fetchContract(address, rpc));
        } catch (e: any) {
            logger.debug(e);
            const message =
                e.response === undefined
                    ? 'Not found'
                    : e.response.data.includes('Cannot parse contract id')
                    ? 'Invalid contract address'
                    : e.response.statusText;
            errors.push({
                url: rpc,
                status: e.response?.status || 404,
                message: message,
            });
        }
    }
    return Promise.reject(errors);
};
/**
 * @description Fetch contract information.
 * @param {string} address Contract address
 * @param {string} rpc Node API
 * @return {Promise<ContractResponse>} contract information
 */
export const fetchContract = async (address: string, rpc: string): Promise<ContractResponse> => {
    const contract = (await httpRequester.get<any>(`${rpc}${RPC.Endpoint.CONTRACT.replace(':contract_id', address)}`))
        .data;

    return {
        contract,
        rpc,
    };
};
