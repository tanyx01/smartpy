export const getBase = (): string => process.env.PUBLIC_URL;

const url = {
    getBase,
};

export default url;
