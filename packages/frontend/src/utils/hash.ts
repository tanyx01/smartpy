import bs58 from 'bs58';
import blake from 'blakejs';

/**
 * @description Get blake2b hash from operation bytes.
 *
 * @param rawOpHex Hexadecimal representation of the operation bytes.
 *
 * @returns blake2b hash.
 */
export const getBlake2BHash = (rawOpHex: string): string => {
    const rawOp = Buffer.concat([Buffer.from([0x03]), Buffer.from(rawOpHex, 'hex')]);
    return bs58.encode(Buffer.from(blake.blake2b(rawOp, null, 32)));
};
