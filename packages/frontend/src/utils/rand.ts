/**
 * @summary Generate a random value to serve as identifier. (It generates 24 nibbles)
 * @return {string} Unique identifier with 24 hexadecimal characters.
 */
export const generateID = (): string => {
    return window.crypto
        .getRandomValues(new Uint16Array(10))
        .reduce((state, cur) => `${state}${cur.toString(16)}`, '')
        .slice(0, 24);
};
