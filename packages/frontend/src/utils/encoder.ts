import pako from 'pako';

/**
 * Compress data with zlib deflate algorithm.
 * @param {string} str - input string to compress.
 * @return {string} Zlib compressed data.
 */
export const compressString = (str: string) => {
    return btoa(pako.deflate(str, { to: 'string' }))
        .replace(/\+/g, '@')
        .replace(/\//g, '_')
        .replace(/=/g, '-');
};

/**
 * Decompress data with zlib inflate algorithm.
 * @param {string} encoded - input compressed data.
 * @return {string} Decompressed data.
 */
export const decompressString = (encoded: string) => {
    return pako.inflate(atob(encoded.replace(/@/g, '+').replace(/_/g, '/').replace(/-/g, '=')), { to: 'string' });
};
