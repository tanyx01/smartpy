import Explorer from 'src/constants/explorer';
import { Network } from 'src/constants/networks';
import httpRequester from 'src/services/httpRequester';
import Logger from 'src/services/logger';
import { lookupContractNetwork } from './tezosRpc';

export interface Operation {
    allocationFee: number;
    amount: number;
    bakerFee: number;
    block: string;
    counter: number;
    errors: { type: string }[];
    gasLimit: number;
    gasUsed: number;
    hasInternals: boolean;
    hash: string;
    id: number;
    level: number;
    originatedContract?: {
        address: string;
    };
    parameter: { entrypoint: string; value: string };
    parameters: string;
    sender: {
        address: string;
    };
    target: {
        address: string;
    };
    status: 'failed' | 'applied';
    storageFee: number;
    storageLimit: number;
    storageUsed: number;
    timestamp: string;
    type: 'transaction' | 'origination' | 'activation';
}

/**
 * @description Get contract operations from tzkt explorer
 * @param {string} address Contract Address
 * @return {Promise<Operation[]>} Network
 */
export const fetchContractOperations = async (address: string, network?: Network): Promise<Operation[]> => {
    try {
        network ||= (await lookupContractNetwork(address)) || undefined;
        if (network && network in Explorer.tzktAPI) {
            const { data } = await httpRequester.get<Operation[]>(
                `${Explorer.tzktAPI[network]}${Explorer.Endpoint.CONTRACT_OPERATIONS.replace(':contract_id', address)}`,
            );
            return data;
        }
    } catch (e) {
        Logger.debug(e);
    }

    return [];
};

const tzkt = {
    fetchContractOperations,
};

export default tzkt;
