import logger from '../services/logger';

export const prettifyJsonString = (jsonStr = '', space?: number) => {
    try {
        return jsonStr && JSON.stringify(JSON.parse(jsonStr), null, space);
    } catch (e) {
        logger.error('Failed to Prettify JSON', e);
    }
    return jsonStr;
};
