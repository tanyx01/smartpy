import logger from './logger';
import toast from './toast';
import ipfs from './ipfs';

const services = {
    logger,
    toast,
    ipfs,
};

export default services;
