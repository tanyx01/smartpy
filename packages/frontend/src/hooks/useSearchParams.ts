import { useLocation } from 'react-router-dom';
import queryString from 'query-string';

export const useSearchParams = <T>() => {
    const location = useLocation<T>();
    return queryString.parse(location.search);
};
