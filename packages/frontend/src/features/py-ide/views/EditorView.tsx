import React from 'react';
import { useDispatch } from 'react-redux';
import { KeyCode, KeyMod } from 'monaco-editor/esm/vs/editor/editor.api';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';
import type { OnMount } from '@monaco-editor/react';

// Material UI
import { Theme, useTheme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';

// State Management
import { IDESettings } from 'SmartPyModels';
import actions, { setVolatileContract, updateNewcomerGuideStep } from '../actions';
import { useNewcomerGuideStep, useSelectedContract } from '../selectors';

// Local Components
import ContractManagement from '../containers/ContractManagement';
import TemplatesMenuItem from '../components/TemplatesMenuItem';
import SettingsMenuItem from '../components/SettingsMenuItem';

// Local Utils
import { evalRun, evalTest } from '../utils/EditorUtils';
import toast from '../../../services/toast';
import useTranslation from '../../i18n/hooks/useTranslation';
import { downloadOutputPanel } from '../../common/utils/IDEUtils';
import EditorToolBar from '../../common/components/EditorToolBar';
import { SYNTAX } from '../../common/enums/ide';
import Logger from '../../../services/logger';
import Editor from '../../editor/components/Editor';
import EditorWithOutput from 'src/features/editor/components/EditorWithOutput';
import { Box } from '@mui/material';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 1,
        },
        editorSection: {
            display: 'flex',
            flexGrow: 1,
            borderTopWidth: 2,
            borderTopStyle: 'solid',
            borderTopColor: theme.palette.primary.light,
            // screen height - navBar - toolBar (percentage doesn't work here)
            height: 'calc(100vh - 80px - 60px)',
        },
        editor: {
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            flexGrow: 1,
        },
        noBorderRadius: {
            borderRadius: 0,
        },
    }),
);

interface OwnProps {
    clearOutputs: () => void;
    editorRef: React.MutableRefObject<editor.IStandaloneCodeEditor>;
    htmlOutput: { __html: string };
    settings: IDESettings;
    targets: { kind: string; name: string }[];
    showError: (error: string) => void;
}

const EditorView: React.FC<OwnProps> = (props) => {
    const classes = useStyles();
    const newcomerGuideStep = useNewcomerGuideStep();
    const outputPanelRef = React.useRef(null as unknown as HTMLDivElement);
    const theme = useTheme();
    const dispatch = useDispatch();
    const contract = useSelectedContract();
    const t = useTranslation();
    const { targets, clearOutputs, editorRef, htmlOutput, settings, showError } = props;
    const [compiling, setCompiling] = React.useState(false);

    const downloadOutput = React.useCallback(() => {
        if (contract && htmlOutput.__html) {
            downloadOutputPanel(contract.name, theme.palette.mode === 'dark', outputPanelRef.current);
        } else {
            toast.error(t('ide.errors.outputEmpty'));
        }
    }, [contract, htmlOutput.__html, t, theme.palette.mode]);

    const onInput = React.useCallback(
        (code = '') => {
            if (editorRef.current) {
                if (contract?.id && code !== contract?.code) {
                    dispatch(
                        actions.updateContract({
                            ...contract,
                            code,
                        }),
                    );
                } else if (!contract?.id) {
                    dispatch(
                        actions.addContract(
                            {
                                code,
                                shared: false,
                            },
                            false,
                        ),
                    );
                }
            }
        },
        [contract, dispatch, editorRef],
    );

    const compileContract = React.useCallback(
        async (withTests = true) => {
            setCompiling(true);
            try {
                await new Promise((r) => setTimeout(r, 500));
                await evalRun(withTests);
            } catch (error: any) {
                showError(error);
            }
            setCompiling(false);
        },
        [showError],
    );

    const runScenario = React.useCallback(
        async (testName: string) => {
            setCompiling(true);
            try {
                await evalTest(testName);
            } catch (error: any) {
                showError(error);
            }
            setCompiling(false);
        },
        [showError],
    );

    const handleEditorDidMount: OnMount = React.useCallback(
        (editor) => {
            if (!editor) {
                return Logger.error('Monaco Editor could not load, please notify the maintainer.');
            }
            editorRef.current = editor;
            editor.focus();
        },
        [editorRef],
    );

    /**
     * This method returns the editor
     */
    const IDE = React.useMemo(
        () => (
            <Box className={classes.editor} style={{ display: settings.layout === 'output-only' ? 'none' : 'flex' }}>
                <ContractManagement />
                <Editor
                    onMount={handleEditorDidMount}
                    language="tmPython"
                    value={contract?.code || ''}
                    options={{
                        automaticLayout: true,
                        selectOnLineNumbers: true,
                        fontSize: settings.fontSize,
                    }}
                    actions={[
                        {
                            // Unique identifier
                            id: 'run-code',
                            // A label of the action that will be presented to the user.
                            label: 'Run Code',
                            keybindings: [KeyMod.CtrlCmd | KeyCode.Enter],
                            // Custom section
                            contextMenuGroupId: 'utils',
                            contextMenuOrder: 1,
                            run: () => compileContract(),
                        },
                        {
                            // Unique identifier
                            id: 'run-code-no-tests',
                            // A label of the action that will be presented to the user.
                            label: 'Run Code Without Tests',
                            keybindings: [KeyMod.Shift | KeyMod.CtrlCmd | KeyCode.Enter],
                            // Custom section
                            contextMenuGroupId: 'utils',
                            contextMenuOrder: 2,
                            run: () => compileContract(false),
                        },
                    ]}
                    onChange={onInput}
                    compileContract={compileContract}
                />
            </Box>
        ),
        [
            classes.editor,
            compileContract,
            contract?.code,
            handleEditorDidMount,
            onInput,
            settings.fontSize,
            settings.layout,
        ],
    );

    return (
        <div className={classes.root}>
            <EditorToolBar
                targets={targets}
                clearOutputs={clearOutputs}
                downloadOutputPanel={downloadOutput}
                compileContract={compileContract}
                runScenario={runScenario}
                selectedContract={contract}
                baseUrl={`${window.location.origin}${process.env.PUBLIC_URL}/ide`}
                settingsMenu={<SettingsMenuItem />}
                templatesMenu={<TemplatesMenuItem />}
                updateNewcomerGuideStep={updateNewcomerGuideStep}
                setVolatileContract={setVolatileContract}
                newcomerGuideStep={newcomerGuideStep}
                syntax={SYNTAX.PYTHON}
            />
            <div className={classes.editorSection}>
                <EditorWithOutput
                    editor={IDE}
                    settings={settings}
                    htmlOutput={htmlOutput}
                    outputPanelRef={outputPanelRef}
                    compiling={compiling}
                />
            </div>
        </div>
    );
};

export default EditorView;
