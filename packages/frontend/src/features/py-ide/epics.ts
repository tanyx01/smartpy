import { RootAction, RootState, Services } from 'SmartPyTypes';
import { Epic } from 'redux-observable';
import { from, of } from 'rxjs';
import { filter, switchMap, map, catchError } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { loadTemplate, addContract, selectContract, showError, setVolatileContract } from './actions';
import { getLatestModifiedContract } from './selectors';
import { getTemplateCode } from '../common/utils/IDEUtils';

export const loadTemplateEpic: Epic<RootAction, RootAction, RootState, Services> = (action$, state$) =>
    action$.pipe(
        filter(isActionOf([loadTemplate])),
        switchMap(() =>
            from(getTemplateCode(state$.value.editor.template)).pipe(
                map((contract) => setVolatileContract(contract)),
                catchError((message: string) => of(showError(message))),
            ),
        ),
    );

export const addContractEpic: Epic<RootAction, RootAction, RootState, Services> = (action$, state$) =>
    action$.pipe(
        filter(isActionOf([addContract])),
        map(() => selectContract(getLatestModifiedContract(state$.value)?.id || '')),
        catchError((message: string) => of(showError(message))),
    );
