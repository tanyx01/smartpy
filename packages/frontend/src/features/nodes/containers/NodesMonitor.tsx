import React from 'react';
import httpRequester from '../../../services/httpRequester';

import Skeleton from '@mui/material/Skeleton';

import nodes, { Nodes } from '../constants/nodes';
import NodesMonitorComponent, { NodeStatus } from '../components/NodesMonitor';

const NodesMonitor = () => {
    const [ready, setReady] = React.useState(false);
    const intervalId = React.useRef(null as unknown as ReturnType<typeof setInterval>);
    const [nodesStatus, setNodesStatus] = React.useState<NodeStatus[]>([]);

    const updateNodeStatus = React.useCallback(async () => {
        const statusJson: NodeStatus[] = await httpRequester
            .get('https://status.smartpy.io/status.json', { timeout: 1000 })
            .then((data) => {
                return data.data
                    .filter((node: any): boolean => {
                        return node.spec.publicRpc === true;
                    })
                    .map((node: any): NodeStatus => {
                        const nodeStatus: NodeStatus = {
                            label: node.spec.label,
                            endpoint: node.spec.endpoint,
                            status: node.status,
                            timestamp: node.timestamp,
                            blockLevel: node.level,
                            error_description: node.description,
                        };
                        return nodeStatus;
                    });
            })
            .catch(() => {
                return [];
            });
        setNodesStatus(statusJson);
        setReady(true);
    }, []);

    React.useEffect(() => {
        updateNodeStatus();
        intervalId.current = setInterval(updateNodeStatus, nodes.healthCheckInterval);
        return () => {
            clearInterval(intervalId.current);
        };
    }, [updateNodeStatus]);

    return ready ? (
        <NodesMonitorComponent nodes={nodesStatus} />
    ) : (
        <div style={{ width: '100%' }}>
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
        </div>
    );
};

export default NodesMonitor;
