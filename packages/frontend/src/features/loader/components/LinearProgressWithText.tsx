import React from 'react';
// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import LinearProgress, { LinearProgressProps } from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';

import SmartPyIcon from '../../common/elements/icons/SmartPy';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            flexGrow: 1,
            backgroundColor: theme.palette.background.paper,
        },
        icon: {
            width: 100,
            height: 100,
            opacity: 0.8,
        },
        progress: {
            margin: 20,
            width: 200,
            height: 10,
        },
        text: {
            color: theme.palette.text.primary,
        },
    }),
);

interface OwnProps extends LinearProgressProps {
    msg?: string;
}

const LinearProgressWithText: React.FC<OwnProps> = ({ msg, ...props }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <SmartPyIcon className={classes.icon} />
            <LinearProgress className={classes.progress} {...props} />
            <Typography className={classes.text}>{msg || 'Loading...'}</Typography>
        </div>
    );
};

export default LinearProgressWithText;
