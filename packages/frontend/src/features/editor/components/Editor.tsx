import React from 'react';
import MonacoEditor from '@monaco-editor/react';
import type { EditorProps, OnMount } from '@monaco-editor/react';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';

import { Registry } from 'monaco-textmate'; // peer dependency
import { wireTmGrammars } from 'monaco-editor-textmate';
import { loader } from '@monaco-editor/react';
import { loadWASM } from 'onigasm'; // peer dependency of 'monaco-textmate'

import CodeMirror from './CodeMirror';

// React Ace IDE
import AceEditor from 'react-ace';
import 'ace-builds/src-min-noconflict/mode-python';
import 'ace-builds/src-min-noconflict/mode-typescript';
import 'ace-builds/src-min-noconflict/theme-chrome';
import 'ace-builds/src-min-noconflict/theme-monokai';

import { getBase } from 'src/utils/url';
import MichelsonLanguage from 'src/features/editor/language/michelson';
import PythonLanguage from 'src/features/editor/language/python';
import darkPlus from 'src/features/editor/themes/darkPlus';
import lightPlus from 'src/features/editor/themes/lightPlus';

import selectors from '../../../store/selectors';

const registry = new Registry({
    getGrammarDefinition: async (scopeName) => {
        if (scopeName.match(/.*.python/g)) {
            const pythonGrammar = await import('../grammars/MagicPython.tmLanguage.json');
            return {
                format: 'json',
                content: pythonGrammar.default,
            };
        }
        if (scopeName.match(/.*.ocaml/g)) {
            const ocamlGrammar = await import('../grammars/ocaml.tmLanguage.json');
            return {
                format: 'json',
                content: ocamlGrammar.default,
            };
        }
        if (scopeName.match(/.*.typescript/g)) {
            const typescriptGrammar = await import('../grammars/typescript.tmLanguage.json');
            return {
                format: 'json',
                content: typescriptGrammar.default,
            };
        }
        if (scopeName.match(/.*.michelson/g)) {
            const michelsonGrammar = await import('../grammars/michelson.tmLanguage.json');
            return {
                format: 'json',
                content: michelsonGrammar.default,
            };
        }
        return console.error('This should never happen.', scopeName) as never;
    },
});

interface OwnProps {
    actions?: editor.IActionDescriptor[];
    compileContract?: () => Promise<void>;
}

const Editor: React.FC<EditorProps & OwnProps> = ({ actions = [], ...props }) => {
    const isDarkMode = selectors.theme.useThemeMode();

    const onMount: OnMount = React.useCallback(
        async (editor, monaco) => {
            // Add Actions
            actions.forEach((action) => {
                editor.addAction(action);
            });
            // Propagate
            props.onMount?.(editor, monaco);
            window.editor = editor;
            editor.render(true);
        },
        [actions, props],
    );

    return (
        <>
            {props.language === 'tmPython' ? (
                <div style={{ display: 'flex', flexGrow: 1, overflow: 'auto' }}>
                    <CodeMirror
                        theme={isDarkMode ? 'oneDark' : 'default'}
                        value={props.value}
                        language={props.language}
                        onChange={props.onChange as any}
                        onMount={props.onMount as any}
                        fontSize={props.options?.fontSize}
                        compileContract={props.compileContract}
                    />
                </div>
            ) : (
                <div style={{ display: 'flex', flexGrow: 1, overflow: 'hidden' }}>
                    {/webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? (
                        <AceEditor
                            debounceChangePeriod={1000}
                            mode={props.language}
                            theme={isDarkMode ? 'monokai' : 'chrome'}
                            onLoad={(editor) => {
                                (props.onMount as any)?.(editor);
                            }}
                            setOptions={{
                                enableBasicAutocompletion: true,
                                enableLiveAutocompletion: false,
                                showLineNumbers: true,
                                tabSize: 4,
                                useWorker: true,
                                autoScrollEditorIntoView: true,
                                showPrintMargin: false,
                                useSoftTabs: true,
                                showGutter: true,
                                wrap: true,
                                highlightActiveLine: true,
                            }}
                            value={props.value}
                            editorProps={{ $blockScrolling: true }}
                            width="100%"
                            height="auto"
                            onChange={props.onChange as any}
                        />
                    ) : (
                        <MonacoEditor
                            {...{
                                ...props,
                                onMount,
                                theme: isDarkMode ? 'vs-code-dark' : 'vs-code-light',
                                options: {
                                    ...(props.options || {}),
                                    quickSuggestions: false,
                                },
                            }}
                        />
                    )}{' '}
                </div>
            )}
        </>
    );
};

// Load monaco static files (IMPORTANT)
loader.config({ paths: { vs: `${getBase()}/static/monaco-editor/0.25.2/min/vs` } });
(async () => {
    await loadWASM(`${getBase()}/static/wasm/onigasm.wasm`); // See https://www.npmjs.com/package/onigasm#light-it-up
    await loader.init().then(async (monaco) => {
        // monaco's built-in themes aren't powereful enough to handle TM tokens
        // https://github.com/Nishkalkashyap/monaco-vscode-textmate-theme-converter#monaco-vscode-textmate-theme-converter
        monaco.editor.defineTheme('vs-code-dark', darkPlus as any);
        monaco.editor.defineTheme('vs-code-light', lightPlus as any);

        monaco.languages.register({ id: 'ocaml' });
        monaco.languages.register({ id: 'tmPython' });
        monaco.languages.register({ id: 'michelson' });

        monaco.languages.setLanguageConfiguration('michelson', {
            comments: { lineComment: '#', blockComment: ['/*', '*/'] },
        });
        monaco.languages.setLanguageConfiguration('tmPython', {
            comments: { lineComment: '#', blockComment: ['"""', '"""'] },
        });
        monaco.languages.setLanguageConfiguration('ocaml', {
            comments: { blockComment: ['(*', '*)'] },
        });

        // monaco "language id's" to TextMate scopeNames
        const grammars = new Map();
        grammars.set('tmPython', 'source.python');
        grammars.set('ocaml', 'source.ocaml');
        grammars.set('michelson', 'source.michelson');
        grammars.set('typescript', 'source.typescript');

        await wireTmGrammars(monaco, registry, grammars);

        // Add michelson auto-completers
        MichelsonLanguage.register(monaco);
        // Add python auto-completers
        PythonLanguage.register(monaco);
    });
})();

export default Editor;
