import { CompletionItem, CompletionItemKind } from '../monacoEditor';

class MichelsonLanguage {
    static register(monaco: any) {
        // Register a completion item provider for the language
        // Register new completionItemProvider on Monaco's language import
        monaco.languages.registerCompletionItemProvider('michelson', {
            provideCompletionItems: (model: any, position: any) => {
                return {
                    suggestions: completers.reduce<CompletionItem[]>((acc, next) => {
                        const word = model.getWordUntilPosition(position);
                        const range = {
                            startLineNumber: position.lineNumber,
                            endLineNumber: position.lineNumber,
                            startColumn: word.startColumn,
                            endColumn: word.endColumn,
                        };
                        acc.push({
                            ...next,
                            range: range,
                        });
                        return acc;
                    }, []),
                };
            },
        });
    }
}

export default MichelsonLanguage;

const KEYWORKS = ['parameter', 'storage', 'code', 'view'];

const LITERALS = ['False', 'Elt', 'Left', 'None', 'Pair', 'Right', 'Some', 'True', 'Unit', 'constant'];

const TYPES = [
    'bool',
    'contract',
    'int',
    'key',
    'key_hash',
    'lambda',
    'list',
    'map',
    'big_map',
    'nat',
    'option',
    'or',
    'pair',
    'set',
    'signature',
    'string',
    'bytes',
    'mutez',
    'timestamp',
    'unit',
    'operation',
    'address',
    'chain_id',

    'chest',
    'chest_key',
];

const INSTRUCTIONS = [
    'PACK',
    'UNPACK',
    'BLAKE2B',
    'SHA256',
    'SHA512',
    'ABS',
    'ADD',
    'AMOUNT',
    'AND',
    'BALANCE',
    'CAR',
    'CDR',
    'CHECK_SIGNATURE',
    'COMPARE',
    'CONCAT',
    'CONS',
    'CREATE_ACCOUNT',
    'CREATE_CONTRACT',
    'IMPLICIT_ACCOUNT',
    'DIP',
    'DROP',
    'DUP',
    'EDIV',
    'EMIT',
    'EMPTY_MAP',
    'EMPTY_SET',
    'EQ',
    'EXEC',
    'FAILWITH',
    'GE',
    'GET',
    'GT',
    'HASH_KEY',
    'IF',
    'IF_CONS',
    'IF_LEFT',
    'IF_NONE',
    'INT',
    'LAMBDA',
    'LE',
    'LEFT',
    'LOOP',
    'LSL',
    'LSR',
    'LT',
    'MAP',
    'MEM',
    'MUL',
    'NEG',
    'NEQ',
    'NIL',
    'NONE',
    'NOT',
    'NOW',
    'OR',
    'PAIR',
    'PUSH',
    'RIGHT',
    'SIZE',
    'SOME',
    'SOURCE',
    'SENDER',
    'SELF',
    'STEPS_TO_QUOTA',
    'SUB',
    'SWAP',
    'TRANSFER_TOKENS',
    'SET_DELEGATE',
    'UNIT',
    'UPDATE',
    'XOR',
    'ITER',
    'LOOP_LEFT',
    'ADDRESS',
    'CONTRACT',
    'ISNAT',
    'CAST',
    'RENAME',
    'SLICE',
    'DIG',
    'DUG',
    'EMPTY_BIG_MAP',
    'APPLY',
    'CHAIN_ID',
    'LEVEL',
    'SELF_ADDRESS',

    'NEVER',
    'UNPAIR',
    'VOTING_POWER',
    'TOTAL_VOTING_POWER',
    'KECCAK',
    'SHA3',
    'PAIRING_CHECK',
    'bls12_381_g1',
    'bls12_381_g2',
    'bls12_381_fr',
    'SUBMIT_PROPOSALS',
    'SUBMIT_BALLOT',
    'SET_BAKER_ACTIVE',
    'TOGGLE_BAKER_DELEGATIONS',
    'SET_BAKER_CONSENSUS_KEY',
    'SET_BAKER_PVSS_KEY',
    'SAPLING_EMPTY_STATE',
    'SAPLING_VERIFY_UPDATE',

    'OPEN_CHEST',
    'VIEW',
];

const buildCompleter = (label: string, kind: CompletionItemKind) => ({
    label,
    kind,
    insertText: label,
});

const completers = [
    ...KEYWORKS.map((name) => buildCompleter(name, CompletionItemKind.Keyword)),
    ...TYPES.map((name) => buildCompleter(name, CompletionItemKind.TypeParameter)),
    ...INSTRUCTIONS.map((name) => buildCompleter(name, CompletionItemKind.Method)),
    ...LITERALS.map((name) => buildCompleter(name, CompletionItemKind.Constant)),
];
