import React from 'react';
import { useDispatch } from 'react-redux';

import { Light as SyntaxHighlighter } from 'react-syntax-highlighter';
import typescript from 'react-syntax-highlighter/dist/cjs/languages/hljs/typescript';
import a11yDark from 'react-syntax-highlighter/dist/cjs/styles/hljs/a11y-dark';
import github from 'react-syntax-highlighter/dist/cjs/styles/hljs/github';

import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import { useError } from '../selectors';
import { useThemeMode } from '../../theme/selectors';
import actions from '../actions';
import ErrorDialog from '../../common/components/ErrorDialog';
import OutputWrapper from '../../common/styles/OutputCss';

SyntaxHighlighter.registerLanguage('typescript', typescript);

const EditorErrorDialog = () => {
    const isDarkMode = useThemeMode();
    const error = useError();
    const dispatch = useDispatch();

    const handleClose = () => dispatch(actions.hideError());

    const gotoLine = (line: number, column: number, snippet = '') => {
        const rows = snippet.split('\n');
        const maxLength = rows.reduce<number>((p: number, c: string) => (p > c.length ? p : c.length), 0);
        window.showLine(line, column, line + rows.length - 1, column + maxLength);
        handleClose();
    };

    if (typeof error === 'string') {
        return (
            <ErrorDialog open={!!error} onClose={handleClose}>
                <OutputWrapper>
                    <div dangerouslySetInnerHTML={{ __html: error }}></div>
                </OutputWrapper>
            </ErrorDialog>
        );
    } else if (error instanceof Error) {
        if (error.name === 'ST_Error') {
            return (
                <ErrorDialog
                    open={!!error}
                    onClose={handleClose}
                    error={<Alert severity="error">{error.message}</Alert>}
                >
                    {error.snippet ? (
                        <>
                            <Button onClick={() => gotoLine(error.line.line, error.line.character, error.snippet)}>
                                <Typography variant="caption">{`Line: ${error.line.line}, Column: ${error.line.character}`}</Typography>
                            </Button>
                            <SyntaxHighlighter
                                startingLineNumber={error.line.line}
                                showLineNumbers
                                customStyle={{ borderRadius: 5 }}
                                style={isDarkMode ? a11yDark : github}
                                language="typescript"
                            >
                                {error.snippet}
                            </SyntaxHighlighter>
                        </>
                    ) : null}
                </ErrorDialog>
            );
        }
        return (
            <ErrorDialog open={!!error} onClose={handleClose}>
                <OutputWrapper>
                    <div dangerouslySetInnerHTML={{ __html: error.message }}></div>
                </OutputWrapper>
            </ErrorDialog>
        );
    }

    return null;
};

export default EditorErrorDialog;
