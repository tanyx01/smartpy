import React from 'react';

import { Theme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

// Local Components
import TemplatesDialog from '../views/TemplatesDialog';
// Local Elements
import TemplatesButton from '../../common/elements/TemplatesButton';
// State Management
import { useNewcomerMode, useVolatileContract } from '../selectors';
// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';

const TemplatesMenuItem: React.FC = () => {
    const volatileContract = useVolatileContract();
    const [open, setOpen] = React.useState(useNewcomerMode() && !Boolean(volatileContract));
    const t = useTranslation();

    const onlyIcon = useMediaQuery((theme: Theme) => theme.breakpoints.down('xl'));

    const handleDialogClose = () => {
        setOpen(false);
    };

    const handleDialogOpen = () => {
        setOpen(true);
    };

    return (
        <React.Fragment>
            <TemplatesButton
                label={t('ide.templatesMenu.label')}
                aria-controls="templates-menu"
                aria-haspopup="true"
                onClick={handleDialogOpen}
                onlyIcon={onlyIcon}
            />
            <TemplatesDialog open={open} onClose={handleDialogClose} />
        </React.Fragment>
    );
};

export default TemplatesMenuItem;
