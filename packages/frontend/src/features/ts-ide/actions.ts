import { createAction } from 'typesafe-actions';
import { IDEContract, IDESettings } from 'SmartPyModels';
import { ST_Error } from '@smartpy/ts-syntax/dist/types/lib/utils/Error';
import { IDENewcomerGuideSteps } from '../common/enums/ide';

export enum Actions {
    TS_IDE_UPDATE_CONTRACT = 'TS_IDE_UPDATE_CONTRACT',
    TS_IDE_ADD_CONTRACT = 'TS_IDE_ADD_CONTRACT',
    TS_IDE_REMOVE_CONTRACT = 'TS_IDE_REMOVE_CONTRACT',
    TS_IDE_SELECT_CONTRACT = 'TS_IDE_SELECT_CONTRACT',
    // VOLATILE CONTRACT
    TS_IDE_SET_VOLATILE_CONTRACT = 'TS_IDE_SET_VOLATILE_CONTRACT',
    // SETTINGS
    TS_IDE_UPDATE_SETTINGS = 'TS_IDE_UPDATE_SETTINGS',
    // ERROR DIALOG
    TS_IDE_SHOW_ERROR = 'TS_IDE_SHOW_ERROR',
    TS_IDE_HIDE_ERROR = 'TS_IDE_HIDE_ERROR',
    // SHORTCUTS DIALOG
    TS_IDE_SHOW_SHORTCUTS = 'TS_IDE_SHOW_SHORTCUTS',
    TS_IDE_HIDE_SHORTCUTS = 'TS_IDE_HIDE_SHORTCUTS',
    // TEMPLATES
    TS_IDE_TOGGLE_FAVORITE_TEMPLATE = 'TS_IDE_TOGGLE_FAVORITE_TEMPLATE',
    TS_IDE_LOAD_TEMPLATE = 'TS_IDE_LOAD_TEMPLATE',
    // NEWCOMER
    TS_IDE_UPDATE_NEWCOMER_GUIDE_STEP = 'TS_IDE_UPDATE_NEWCOMER_GUIDE_STEP',
    TS_IDE_TOGGLE_NEWCOMER_DIALOG = 'TS_IDE_TOGGLE_NEWCOMER_DIALOG',
}

// CONTRACTS

export const updateContract = createAction(Actions.TS_IDE_UPDATE_CONTRACT)<IDEContract>();
export const addContract = createAction(Actions.TS_IDE_ADD_CONTRACT)<IDEContract, boolean>();
export const removeContract = createAction(Actions.TS_IDE_REMOVE_CONTRACT)<string>();
export const selectContract = createAction(Actions.TS_IDE_SELECT_CONTRACT)<string>();

// VOLATILE CONTRACT

export const setVolatileContract = createAction(Actions.TS_IDE_SET_VOLATILE_CONTRACT)<string>();

// ERROR

export const showError = createAction(Actions.TS_IDE_SHOW_ERROR, (error: string | ST_Error) => error)<
    string | ST_Error
>();
export const hideError = createAction(Actions.TS_IDE_HIDE_ERROR)<void>();

// Shortcuts

export const showShortcuts = createAction(Actions.TS_IDE_SHOW_SHORTCUTS, (platform: string) => platform)<string>();
export const hideShortcuts = createAction(Actions.TS_IDE_HIDE_SHORTCUTS)<void>();

// Templates

export const toggleFavoriteTemplate = createAction(Actions.TS_IDE_TOGGLE_FAVORITE_TEMPLATE)<string>();
export const loadTemplate = createAction(Actions.TS_IDE_LOAD_TEMPLATE)<string>();

// SETTINGS

export const updateSettings = createAction(
    Actions.TS_IDE_UPDATE_SETTINGS,
    (settings: IDESettings) => settings,
)<IDESettings>();

// NEWCOMER
export const toggleNewcomerDialog = createAction(Actions.TS_IDE_TOGGLE_NEWCOMER_DIALOG)<boolean | void>();
export const updateNewcomerGuideStep = createAction(Actions.TS_IDE_UPDATE_NEWCOMER_GUIDE_STEP)<IDENewcomerGuideSteps>();

const actions = {
    updateContract,
    addContract,
    removeContract,
    updateSettings,
    selectContract,
    showError,
    hideError,
    showShortcuts,
    hideShortcuts,
    loadTemplate,
    updateNewcomerGuideStep,
    toggleNewcomerDialog,
    toggleFavoriteTemplate,
    setVolatileContract,
};

export default actions;
