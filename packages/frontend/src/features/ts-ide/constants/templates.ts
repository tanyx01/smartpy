import { getBase } from 'src/utils/url';

export interface TemplateProps {
    fileName: string;
    name: string;
    description?: string;
}

export type Templates = {
    [section: string]: TemplateProps[];
};

export type TemplatesObj = {
    [template: string]: TemplateProps;
};

export const syntaxTemplates: Templates = {
    'Syntax Examples': [
        {
            fileName: 'Minimal.ts',
            name: 'Minimal',
            description: 'A minimal template to start with.',
        },
        {
            fileName: 'Loops.ts',
            name: 'Loops',
            description: 'Example with loop statements.',
        },
        {
            fileName: 'BinaryExpressions.ts',
            name: 'Binary Expressions',
            description: 'Binary expressions examples.',
        },
        {
            fileName: 'PackUnpack.ts',
            name: 'Pack and Unpack',
            description: 'Example of packing and unpacking data.',
        },
        {
            fileName: 'HashFunctions.ts',
            name: 'Hash Functions',
            description: 'Example with all hash functions.',
        },
    ],
    'Templates per Type': [
        {
            fileName: 'Timelock.ts',
            name: 'Timelock',
            description: '<a href="https://tezos.gitlab.io/alpha/timelock.html">Documentation</a>',
        },
        {
            fileName: 'Constants.ts',
            name: 'Using Constants',
            description:
                '<a href="https://tezos.gitlab.io/protocols/011_hangzhou.html?highlight=global%20table%20constants#global-constants">Global table of constants</a>',
        },
        {
            fileName: 'OnChainViews.ts',
            name: 'Using on-chain views',
            description: `The documentation can be found <a href="${getBase()}/docs/experimental/onchain_views">here</a>.`,
        },
        {
            fileName: 'Lambdas.ts',
            name: 'Lambdas',
            description: 'Multiple examples with Lambdas.',
        },
        {
            fileName: 'createContract.ts',
            name: 'Contract Creation',
            description: 'An example on how to create a contract from another contract.',
        },
        {
            fileName: 'Signatures.ts',
            name: 'Signatures',
            description: 'Validate signatures.',
        },
        {
            fileName: 'Bls12_381.ts',
            name: 'BLS12 381',
            description:
                'Various BLS12 operations using types <span class="code">TBls12_381_g1</span>, <span class="code">TBls12_381_g2</span>, <span class="code">TBls12_381_fr</span>.',
        },
    ],
    'Misc Features': [
        {
            fileName: 'OffChainViews.ts',
            name: 'Metadata',
            description: 'Defining metadata for a contract.',
        },
        {
            fileName: 'Inlining.ts',
            name: 'Inline Functions',
            description: 'Inline expansion examples.',
        },
        {
            fileName: 'Inheritance.ts',
            name: 'Inheritance',
            description: 'Various ways to inherit from another contract.',
        },
    ],
};

export const contractTemplates: Templates = {
    'Token Contracts': [
        {
            fileName: 'FA1_2.ts',
            name: 'FA1.2: Fungible Assets',
            description:
                'FA1.2 refers to an ERC20-like fungible token standard for Tezos. Proposed in <a href="https://gitlab.com/tzip/tzip/blob/master/proposals/tzip-7/tzip-7.md" target="_blank">TZIP-7</a>.',
        },
        {
            fileName: 'FA2.ts',
            name: 'FA2: Fungible, Non-fungible, Multiple or Single Assets',
            description:
                'FA2 exposes a unified token contract interface, supporting a wide range of token types and implementations. Proposed in <a href="https://gitlab.com/tzip/tzip/blob/master/proposals/tzip-12/tzip-12.md" target="_blank">TZIP-12</a>.',
        },
    ],
    'Simple Examples': [
        {
            fileName: 'StoreValue.ts',
            name: 'Store Value',
            description:
                'Store some data in the storage of a contract and change its value by calling its entry points.',
        },
    ],
};

export const getAllTemplates = (): TemplatesObj => {
    const availableContractTemplates = Object.keys(contractTemplates).reduce(
        (state, key) => [...state, ...contractTemplates[key]],
        [] as TemplateProps[],
    );
    const availableSyntaxTemplates = Object.keys(syntaxTemplates).reduce(
        (state, key) => [...state, ...syntaxTemplates[key]],
        [] as TemplateProps[],
    );

    return [...availableContractTemplates, ...availableSyntaxTemplates].reduce(
        (state, template) => ({ ...state, [template.fileName]: template }),
        {},
    );
};
