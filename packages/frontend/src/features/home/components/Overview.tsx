import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';

//// Local Components
import MediumIcon from '../../common/elements/icons/Medium';
import { getBase } from '../../../utils/url';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            margin: 20,
        },
        image: {
            width: '100%',
        },
        card: {
            borderWidth: 2,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,

            boxShadow: `5px 5px 0 0 ${theme.palette.primary.main}`,
        },
        title: {
            color: theme.palette.primary.main,
            fontWeight: 'bold',
        },
        description: {
            color: theme.palette.primary.light,
        },
        chipIcon: {
            borderRadius: 8,
        },
    }),
);

const Overview: React.FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.root} id="overview">
            <Grid container spacing={2} alignItems="center" justifyContent="center">
                <Grid item xs={12} md={5}>
                    <img src={`${getBase()}/static/img/overview.svg`} alt="overview" className={classes.image} />
                </Grid>
                <Grid item xs={12} md={5}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
                                Overview
                            </Typography>
                            <Typography
                                variant="body1"
                                align="justify"
                                color="textPrimary"
                                className={classes.description}
                            >
                                Smart contracts are scripts stored on a blockchain that enable users to define custom
                                rules and protocols for various purposes. Some typical examples include escrow accounts,
                                insurance-like contracts, decentralized applications —  frequently called dApps,
                                multi-signatures (multi-sig) contracts, security tokens, and virtual or physical
                                tokenized assets.
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Chip
                                avatar={<MediumIcon className={classes.chipIcon} fontSize="small" />}
                                label="Introduction post on Medium"
                                clickable
                                component="a"
                                href="https://smartpy-io.medium.com/introducing-smartpy-and-smartpy-io-d4013bee7d4e"
                            />
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </div>
    );
};

export default Overview;
