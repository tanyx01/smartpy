import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { setDarkMode } from './actions';
import { ThemeSettings } from 'SmartPyModels';

const DEFAULT_SETTINGS: ThemeSettings = {
    darkMode: false,
};

const darkModeReducer = createReducer(DEFAULT_SETTINGS.darkMode).handleAction(
    setDarkMode,
    (_, action) => action.payload,
);

const themeReducer = combineReducers({
    darkMode: darkModeReducer,
});

export default themeReducer;
export type EditorState = ReturnType<typeof themeReducer>;
