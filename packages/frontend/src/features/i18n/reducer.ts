import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { SupportedLanguages } from '.';
import { changeLanguage } from './actions';

const languageReducer = createReducer(SupportedLanguages[0]).handleAction(changeLanguage, (state, action) =>
    SupportedLanguages.includes(action.payload) ? action.payload : state,
);

const i18nReducer = combineReducers({
    language: languageReducer,
});

export type EditorState = ReturnType<typeof i18nReducer>;
export default i18nReducer;
