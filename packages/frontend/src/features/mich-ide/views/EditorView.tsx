import React from 'react';
import { useDispatch } from 'react-redux';
import { KeyCode, KeyMod } from 'monaco-editor/esm/vs/editor/editor.api';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';
import type { OnMount } from '@monaco-editor/react';

// Material UI
import { Theme, useTheme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Alert from '@mui/material/Alert';

// State Management
import { MichelsonIDESettings } from 'SmartPyModels';
import { useSelectedContract } from '../selectors';

// Local Components
import ContractManagement from '../containers/ContractManagement';
// Local Utils
import { evalCode } from '../utils/EditorUtils';
import toast from '../../../services/toast';
import useTranslation from '../../i18n/hooks/useTranslation';
import { downloadOutputPanel } from '../../common/utils/IDEUtils';
import Logger from '../../../services/logger';
import Editor from '../../editor/components/Editor';
import EditorToolBar from '../../common/components/EditorToolBar';
import SettingsMenuItem from '../components/SettingsMenuItem';
import TemplatesMenuItem from '../components/TemplatesMenuItem';
import { SYNTAX } from '../../common/enums/ide';
import { addContract, setVolatileContract, updateContract } from '../actions';
import debounce from '../../../utils/debounce';
import { Box } from '@mui/material';
import EditorWithOutput from 'src/features/editor/components/EditorWithOutput';

const debouncer = debounce(1000);

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 1,
        },
        editorSection: {
            display: 'flex',
            flexGrow: 1,
            borderTopWidth: 2,
            borderTopStyle: 'solid',
            borderTopColor: theme.palette.primary.light,
            // screen height - navBar - toolBar (percentage doesn't work here)
            height: 'calc(100vh - 80px - 60px)',
        },
        editor: {
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            flexGrow: 1,
        },
        errorMessage: {
            marginBottom: 10,
        },
    }),
);

interface OwnProps {
    clearOutputs: () => void;
    editorRef: React.MutableRefObject<editor.IStandaloneCodeEditor>;
    htmlOutput: { __html: string };
    settings: MichelsonIDESettings;
    showError: (error: string) => void;
    firstMessage: string;
    shouldUpdateTabs?: boolean;
}

const EditorView: React.FC<OwnProps> = (props) => {
    const classes = useStyles();
    const outputPanelRef = React.useRef(null as unknown as HTMLDivElement);
    const theme = useTheme();
    const dispatch = useDispatch();
    const contract = useSelectedContract();
    const t = useTranslation();
    const { clearOutputs, editorRef, htmlOutput, settings, showError, firstMessage, shouldUpdateTabs } = props;
    const [compiling, setCompiling] = React.useState(false);

    const downloadOutput = React.useCallback(() => {
        if (contract && htmlOutput.__html) {
            downloadOutputPanel(contract.name, theme.palette.mode === 'dark', outputPanelRef.current);
        } else {
            toast.error(t('ide.errors.outputEmpty'));
        }
    }, [contract, htmlOutput.__html, t, theme.palette.mode]);

    const compileContract = React.useCallback(
        async () =>
            debouncer(() => {
                const code = editorRef.current?.getValue();
                if (code) {
                    setCompiling(true);
                    try {
                        evalCode(t, code, settings.protocol);
                    } catch (error: any) {
                        showError(error);
                    }
                    setCompiling(false);
                }
            }),
        [editorRef, settings.protocol, showError, t],
    );

    const onInput = React.useCallback(
        (code = '') => {
            if (editorRef.current) {
                if (contract?.id && code !== contract?.code) {
                    dispatch(
                        updateContract({
                            ...contract,
                            code,
                        }),
                    );
                } else if (!contract?.id) {
                    dispatch(
                        addContract(
                            {
                                code,
                                shared: false,
                            },
                            false,
                        ),
                    );
                }
                settings.electricEvaluation && compileContract();
            }
        },
        [compileContract, contract, dispatch, editorRef, settings.electricEvaluation],
    );

    const handleEditorDidMount: OnMount = React.useCallback(
        (editor) => {
            if (!editor) {
                return Logger.error('Monaco Editor could not load, please notify the maintainer.');
            }
            editorRef.current = editor;
            editor.focus();

            window.editor = editor;
            compileContract();
        },
        [compileContract, editorRef],
    );

    /**
     * This method returns the editor
     */
    const IDE = React.useMemo(
        () => (
            <Box className={classes.editor} style={{ display: settings.layout === 'output-only' ? 'none' : 'flex' }}>
                <ContractManagement onResize={() => null} />
                <Editor
                    onMount={handleEditorDidMount}
                    language="michelson"
                    value={contract?.code || ''}
                    options={{
                        automaticLayout: true,
                        selectOnLineNumbers: true,
                        tabSize: 4,
                        fontSize: settings.fontSize,
                    }}
                    actions={[
                        {
                            // An unique identifier of the contributed action.
                            id: 'run-code',
                            // A label of the action that will be presented to the user.
                            label: 'Run Code',
                            // An optional array of keybindings for the action.
                            keybindings: [KeyMod.CtrlCmd | KeyCode.Enter],

                            contextMenuGroupId: 'utils',

                            contextMenuOrder: 1.5,
                            run: compileContract,
                        },
                    ]}
                    onChange={onInput}
                    compileContract={compileContract}
                />
            </Box>
        ),
        [
            classes.editor,
            compileContract,
            contract?.code,
            handleEditorDidMount,
            onInput,
            settings.fontSize,
            settings.layout,
        ],
    );

    return (
        <div className={classes.root}>
            <EditorToolBar
                clearOutputs={clearOutputs}
                downloadOutputPanel={downloadOutput}
                compileContract={compileContract}
                selectedContract={contract}
                baseUrl={`${window.location.origin}${process.env.PUBLIC_URL}/michelson`}
                settingsMenu={<SettingsMenuItem />}
                templatesMenu={<TemplatesMenuItem />}
                setVolatileContract={setVolatileContract}
                syntax={SYNTAX.MICHELSON}
            />
            <div className={classes.editorSection}>
                <EditorWithOutput
                    editor={IDE}
                    settings={settings}
                    htmlOutput={htmlOutput}
                    outputPanelRef={outputPanelRef}
                    compiling={compiling}
                    alert={
                        <Alert
                            variant="outlined"
                            severity={firstMessage ? 'error' : 'info'}
                            className={classes.errorMessage}
                        >
                            {firstMessage || t('michelsonIde.noErrors')}
                        </Alert>
                    }
                    shouldUpdateTabs={shouldUpdateTabs}
                />
            </div>
        </div>
    );
};

export default EditorView;
