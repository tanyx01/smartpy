import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
// Material Icons
import SaveIcon from '@mui/icons-material/Save';

//LOcal Components
import CodeBlock from '../../common/components/CodeBlock';
// State Management
import actions from '../actions';
import { useVolatileContract } from '../selectors';
// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';
// Utils
import { generateID } from '../../../utils/rand';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        contractName: {
            marginBottom: 20,
        },
    }),
);

const CreateContractWithPreview = () => {
    const t = useTranslation();
    const volatileContract = useVolatileContract();
    const dispatch = useDispatch();
    const classes = useStyles();
    const textInputRef = React.useRef(null as unknown as HTMLInputElement);
    const [name, setName] = React.useState(`contract_${generateID()}`);

    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    };

    const handleSave = () => {
        dispatch(
            actions.addContract(
                {
                    name,
                    code: volatileContract,
                    shared: false,
                },
                false,
            ),
        );
        handleClose();
    };

    const handleClose = () => {
        setName(`unnamed_${generateID()}`);
        dispatch(actions.setVolatileContract(''));
    };

    return (
        <Dialog maxWidth="md" fullWidth open={!!volatileContract} onClose={handleClose}>
            <DialogTitle>{t('ide.contractManagement.importContract')}</DialogTitle>
            <DialogContent dividers>
                <div className={classes.contractName}>
                    <TextField
                        error={!name}
                        required
                        inputProps={{
                            maxLength: 32,
                            minLength: 1,
                        }}
                        ref={textInputRef}
                        fullWidth
                        label={t('common.contract.name')}
                        value={name}
                        variant="filled"
                        size="small"
                        onChange={handleNameChange}
                    />
                </div>
                <CodeBlock language="python" showLineNumbers text={volatileContract} />
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handleClose}>
                    {t('common.cancel')}
                </Button>
                <Button startIcon={<SaveIcon />} color="primary" onClick={handleSave} disabled={!name}>
                    {t('ide.contractManagement.importContract')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default CreateContractWithPreview;
