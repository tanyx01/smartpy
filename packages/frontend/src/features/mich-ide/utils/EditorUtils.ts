import { DefaultProtocol } from '../../../constants/protocol';

export const evalCode = (t: (str: string) => void, code: string, protocol = String(DefaultProtocol)) => {
    if (!code) {
        return;
    }
    try {
        window.smartmlCtx.call_exn_handler('update_michelson_view', code, protocol);
    } catch (error) {
        if (typeof window.smartpyContext.setFirstMessage === 'function') {
            window.smartpyContext?.setFirstMessage(`${t('michelsonIde.errorsWhileParsingMichelson')}\n${error}`);
        }
    }
};
