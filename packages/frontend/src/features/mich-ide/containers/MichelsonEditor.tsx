import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';

import { useContracts, useSettings } from '../selectors';
import actions from '../actions';

// Local Views
import ErrorDialog from '../components/ErrorDialog';
import EditorView from '../views/EditorView';
import Loading from '../../loader/components/LinearProgressWithText';
import CreateContractWithPreview from '../views/CreateContractWithPreview';
// Local Components
import HelpMenuItem from '../components/HelpMenuItem';
// Local Utils
import asyncScriptInjector from '../../../utils/asyncScriptInjector';
// Local Hooks
import SharedContractListener from '../../common/components/listeners/SharedContractListener';
// Local Polyfills
import '../../../polyfills';

import { getBase } from '../../../utils/url';
import { updateOriginationContract } from '../../origination/actions';
import Logger from '../../../services/logger';
import scripts from '../constants/scripts';

const EditorContainer = () => {
    // Refs
    const editorRef = React.useRef(null as unknown as editor.IStandaloneCodeEditor);
    const contractNextId = React.useRef(0);
    // States
    const [ready, setReady] = React.useState(false);
    const [percentage, setPercentage] = React.useState(0);
    const [latestScript, setLatestScript] = React.useState('');
    const [firstMessage, setFirstMessage] = React.useState('');
    const [htmlOutput, setHtmlOutput] = React.useState({ __html: '' });
    const contracts = useContracts();
    const settings = useSettings();
    const dispatch = useDispatch();

    /**
     * Set volatile contract (used when importing contracts)
     * @param {string} code - Contract code
     * @return {void}
     */
    const setVolatileContract = (code: string) => dispatch(actions.setVolatileContract(code));

    /**
     * @summary Show error callback
     * @param {string} error - Error message
     * @return {void}
     */
    const showError = useCallback((error: string) => dispatch(actions.showError(error)), [dispatch]);

    /**
     * Get the actual code in the editor.
     *
     * @return {string} Contract code currently in the editor.
     */
    const getEditorValue = () => editorRef.current?.getValue();

    /**
     * Set contract output (HTML formatted)
     *
     * @param {string} output - The Contract Output
     * @return {void}
     */
    const setOutput = React.useCallback((output: string) => {
        setHtmlOutput({ __html: output });
    }, []);

    /**
     * Append to the contract output (HTML formatted)
     *
     * @param {string} output - The Contract Output
     * @return {void}
     */
    const addOutput = (output: string) => {
        setHtmlOutput(({ __html }) => ({ __html: __html + output }));
    };

    /**
     * Clear Contract Outputs
     *
     * @returns {void}
     */
    const clearOutputs = () => {
        setHtmlOutput({ __html: '' });
        contractNextId.current = 0;
    };

    /**
     * @summary Get contract code.
     * @param {string} name - Name of the stored contract.
     * @return {string | undefined} The contract code or undefined if the contract was not found.
     */
    const getContractCode = useCallback(
        (name: string) => {
            for (const c of contracts) {
                if (c.name === name) {
                    return c.code;
                }
            }
        },
        [contracts],
    );

    const setSelectVisibility = (selectId: string, ...rest: string[]) => {
        for (let i = 0; i < rest.length; i += 2) {
            if (rest[i] === (document.getElementById(selectId) as any)?.value) {
                const el = document.getElementById(rest[i + 1]);
                if (el) el.style.display = 'block';
            } else {
                const el = document.getElementById(rest[i + 1]);
                if (el) el.style.display = 'none';
            }
        }
    };

    const setValueVisibility = (value: string, ...rest: string[]) => {
        for (let i = 0; i < rest.length; i += 2) {
            if (rest[i] === value) {
                const el = document.getElementById(rest[i + 1]);
                if (el) el.style.display = 'block';
            } else {
                const el = document.getElementById(rest[i + 1]);
                if (el) el.style.display = 'none';
            }
        }
    };

    const showLine = (line: number) => {
        editorRef.current?.revealPositionInCenter({ lineNumber: line, column: 0 });
        editorRef.current?.setPosition({ lineNumber: line, column: 0 });
        editorRef.current?.focus();
    };

    const gotoOrigination = React.useCallback(
        (codeJson, storageJson) => {
            dispatch(
                updateOriginationContract({
                    codeJson,
                    storageJson,
                }),
            );
            window.open(`${getBase()}/origination`);
        },
        [dispatch],
    );

    /**
     * Update loading state (in percentage %)
     */
    const onProgressUpdate = (percentage: number, loadedScript: string) => {
        setPercentage(percentage);
        setLatestScript(loadedScript);
        if (percentage === 100) {
            setReady(true);
        }
    };

    /**
     * Expose all callbacks needed by JS_of_ocaml scripts
     */
    React.useEffect(() => {
        window.setSelectVisibility = setSelectVisibility;
        window.setValueVisibility = setValueVisibility;
        window.showErrorPre = (error: string) => showError('<pre>' + error + '</pre>');
        window.showLine = showLine;
        window.smartpyContext = {
            ...(window.smartpyContext || {}),
            getContractCode,
            getEditorValue,
            addOutput,
            setOutput,
            clearOutputs,
            showError,
            setFirstMessage,
            gotoOrigination,
        };
    }, [getContractCode, setOutput, showError, gotoOrigination]);

    React.useEffect(() => {
        asyncScriptInjector(scripts, onProgressUpdate, (scriptName) =>
            Logger.error(`Failed to load script: ${scriptName}`),
        );

        const onResize = () => {
            if (window.screen.width < 768) {
                document.getElementById('viewport-meta')?.setAttribute('content', 'width=768');
            }
        };
        onResize();
        window.addEventListener('resize', onResize);

        return () => {
            window.removeEventListener('resize', onResize);
            document.getElementById('viewport-meta')?.setAttribute('content', 'width=device-width, initial-scale=1');
        };
    }, []);

    return (
        <React.Fragment>
            {ready ? (
                <EditorView
                    {...{
                        settings,
                        clearOutputs,
                        firstMessage,
                        editorRef,
                        htmlOutput,
                        showError,
                    }}
                    shouldUpdateTabs={true}
                />
            ) : (
                <Loading
                    msg={`Loading ${latestScript ? '(' + latestScript + ')' : ''} ...`}
                    variant="determinate"
                    value={percentage}
                />
            )}
            <ErrorDialog />
            <CreateContractWithPreview />
            <HelpMenuItem />
            <SharedContractListener setVolatileContract={setVolatileContract} />
        </React.Fragment>
    );
};

export default EditorContainer;
