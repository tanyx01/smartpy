import { RootState } from 'SmartPyTypes';
import { MichelsonIDESettings } from 'SmartPyModels';
import { useSelector } from 'react-redux';
import { DefaultProtocol, Protocol } from '../../../constants/protocol';

export const useSettings = () =>
    useSelector<RootState, MichelsonIDESettings>((state: RootState) => {
        return {
            ...state.michelsonEditor.settings,
            protocol: Object.values(Protocol).includes(state.michelsonEditor.settings.protocol as Protocol)
                ? state.michelsonEditor.settings.protocol
                : DefaultProtocol,
        } as MichelsonIDESettings;
    });
