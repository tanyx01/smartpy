/* eslint-disable react/jsx-no-undef */
import React from 'react';

import {
    styled,
    FormControl,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    TextField,
    Typography,
    Divider,
    Box,
    Button,
    Alert,
} from '@mui/material';
import EstimateIcon from '@mui/icons-material/Cached';

import WalletIcon from 'src/features/common/elements/icons/TezosWallet';
import CommandLineInterfaceIcon from 'src/features/common/elements/icons/CommandLineInterface';
import WalletWidget from 'src/features/wallet/containers/WalletWidget';
import { AppBar, Tab, Tabs } from '@mui/material';
import TabPanel from 'src/features/common/elements/TabPanel';
import CodeBlock from 'src/features/common/components/CodeBlock';
import CallContract from './CallContract';

import { useNetworkInfo } from 'src/features/wallet/selectors/network';
import { useAccountInfo } from 'src/features/wallet/selectors/account';
import { useContractInfo } from '../selectors';
import useTranslation from 'src/features/i18n/hooks/useTranslation';

import { RenderIfTrue } from 'src/utils/conditionalRender';
import { AmountUnit, convertUnitNumber, TezUnit } from 'src/utils/units';
import { TezosConstants } from 'src/services/conseil';
import Paper from 'src/features/common/components/Paper';
import { ExplorerSmartMLOutputs } from 'SmartPyModels';

const Container = styled(Box)(() => ({
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
}));

const MessageBuilder = styled(FormControl)(() => ({
    input: {
        '&[type="text"]': {
            height: '30px',
            width: '350px',
        },
    },
}));

export enum FIELDS {
    AMOUNT = 'amount',
    FEE = 'fee',
    GAS_LIMIT = 'gas_limit',
    STORAGE_LIMIT = 'storage_limit',
}

export const DEFAULT_PARAMETERS = {
    [FIELDS.AMOUNT]: 0.0,
    [FIELDS.FEE]: convertUnitNumber(
        TezosConstants.DefaultSimpleTransactionFee,
        AmountUnit.uTez,
        AmountUnit.tez,
    ).toNumber(),
    [FIELDS.GAS_LIMIT]: TezosConstants.DefaultTransactionGasLimit,
    [FIELDS.STORAGE_LIMIT]: TezosConstants.DefaultTransactionStorageLimit,
};

export type TransferParametersProps = {
    messageBuilder?: JSX.Element;
    parameters: typeof DEFAULT_PARAMETERS;
    handleParameterInput: (event: React.ChangeEvent<HTMLInputElement>) => void;
    estimateTransactionCost: () => Promise<void>;
    outputs: ExplorerSmartMLOutputs;
};

const TransferParameters: React.FC<TransferParametersProps> = ({
    messageBuilder,
    parameters,
    handleParameterInput,
    estimateTransactionCost,
    outputs,
}) => {
    const contractInfo = useContractInfo();
    const networkInfo = useNetworkInfo();
    const accountInfo = useAccountInfo();
    const t = useTranslation();

    const [tab2, setTab2] = React.useState(0);

    const handleTab2Change = (event: unknown, newValue: number) => {
        setTab2(newValue);
    };

    const command = React.useMemo(() => {
        const TEZOS_CLIENT_TRANSFER_TEMPLATE = `octez-client --endpoint {endpoint} transfer {amount} \\\n from {source} to {destination} \\\n --entrypoint {entrypoint} --arg '{arg}'`;
        let cmd = TEZOS_CLIENT_TRANSFER_TEMPLATE.replace('{endpoint}', networkInfo.rpc)
            .replace('{amount}', String(parameters[FIELDS.AMOUNT]))
            .replace('{source}', accountInfo.pkh || '$SOURCE')
            .replace('{destination}', contractInfo.address || '$DESTINATION')
            .replace('{entrypoint}', outputs['entrypoint'] || '$ENTRYPOINT')
            .replace('{arg}', outputs['paramsMicheline'] || '$ARG');

        // Add Fee
        if (Number(parameters[FIELDS.FEE])) {
            cmd = `${cmd} \\\n --fee ${parameters[FIELDS.FEE]}`;
        }
        // Add Gas Limit
        if (Number(parameters[FIELDS.GAS_LIMIT])) {
            cmd = `${cmd} \\\n --gas-limit ${parameters[FIELDS.GAS_LIMIT]}`;
        }
        // Add Storage Limit
        if (Number(parameters[FIELDS.STORAGE_LIMIT])) {
            cmd = `${cmd} \\\n --storage-limit ${parameters[FIELDS.STORAGE_LIMIT]}`;
        }
        return cmd;
    }, [accountInfo.pkh, contractInfo.address, networkInfo.rpc, outputs, parameters]);

    if (!contractInfo.address) {
        return null;
    }
    return (
        <RenderIfTrue isTrue={!!messageBuilder}>
            <Typography variant="h6">{t('explorer.section.contractInterface')}</Typography>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <Paper sx={{ padding: 2, borderColor: 'blue', border: '1px solid' }}>
                <Typography variant="overline" sx={{ fontSize: 18, marginLeft: 0.3, marginBottom: 2 }}>
                    {t('explorer.labels.callParameters')}
                </Typography>
                <MessageBuilder>{messageBuilder}</MessageBuilder>
                {!!outputs.entrypoint && (
                    <>
                        <p>Entrypoint: {outputs.entrypoint}</p>
                        <table className="recordList">
                            <tr>
                                <td>Micheline</td>
                                <td>{outputs.paramsMicheline}</td>
                            </tr>
                            <tr>
                                <td>JSON</td>
                                <td>{outputs.paramsJSON}</td>
                            </tr>
                            <tr>
                                <td>Micheline (Full Path)</td>
                                <td>{outputs.paramsMichelineFull}</td>
                            </tr>
                            <tr>
                                <td>JSON full (Full Path)</td>
                                <td>{outputs.paramsJSONFull}</td>
                            </tr>
                        </table>
                    </>
                )}
                {outputs.errors ? <Alert severity="error">{outputs.errors}</Alert> : null}
            </Paper>
            <RenderIfTrue isTrue={!!outputs.entrypoint}>
                <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
                <AppBar position="static" color="secondary">
                    <Tabs value={tab2} onChange={handleTab2Change} aria-label="Contract Menu" variant="fullWidth">
                        <Tab iconPosition="start" icon={<WalletIcon width={24} height={24} />} label="Wallet" />
                        <Tab
                            iconPosition="start"
                            icon={<CommandLineInterfaceIcon width={24} height={24} />}
                            label="CLI"
                        />
                    </Tabs>
                </AppBar>
                <TabPanel value={tab2} index={0}>
                    <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
                    <WalletWidget rpc={networkInfo.rpc} disableNetworkSelection={true} />
                </TabPanel>
                <RenderIfTrue isTrue={!!accountInfo.source || tab2 === 1}>
                    <Container>
                        <FormControl
                            fullWidth
                            variant="outlined"
                            margin="normal"
                            required
                            error={String(parameters[FIELDS.AMOUNT]) === ''}
                        >
                            <InputLabel htmlFor={FIELDS.AMOUNT}>{t('wallet.parameterLabels.amount')}</InputLabel>
                            <OutlinedInput
                                name={FIELDS.AMOUNT}
                                onChange={handleParameterInput}
                                id={FIELDS.AMOUNT}
                                fullWidth
                                type="number"
                                inputProps={{
                                    min: 0.0,
                                    step: 0.000001,
                                }}
                                label={t('wallet.parameterLabels.amount')}
                                startAdornment={
                                    <InputAdornment position="start">{AmountUnit[TezUnit.tez].char}</InputAdornment>
                                }
                                value={parameters[FIELDS.AMOUNT]}
                            />
                        </FormControl>
                        {tab2 === 0 && (
                            <>
                                <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
                                <Button
                                    color="primary"
                                    variant="outlined"
                                    startIcon={<EstimateIcon />}
                                    onClick={estimateTransactionCost}
                                >
                                    {t('origination.estimateCost')}
                                </Button>
                            </>
                        )}
                        <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
                        <FormControl
                            fullWidth
                            variant="outlined"
                            margin="normal"
                            required
                            error={String(parameters[FIELDS.FEE]) === ''}
                        >
                            <InputLabel htmlFor={FIELDS.FEE}>{t('wallet.parameterLabels.fee')}</InputLabel>
                            <OutlinedInput
                                name={FIELDS.FEE}
                                onChange={handleParameterInput}
                                id={FIELDS.FEE}
                                fullWidth
                                type="number"
                                inputProps={{
                                    min: 0,
                                    step: 0.000001,
                                }}
                                label={t('wallet.parameterLabels.fee')}
                                startAdornment={
                                    <InputAdornment position="start">{AmountUnit[TezUnit.tez].char}</InputAdornment>
                                }
                                value={parameters[FIELDS.FEE]}
                            />
                        </FormControl>
                        <TextField
                            onChange={handleParameterInput}
                            fullWidth
                            name={FIELDS.GAS_LIMIT}
                            id={FIELDS.GAS_LIMIT}
                            required
                            error={String(parameters[FIELDS.GAS_LIMIT]) === ''}
                            label={t('wallet.parameterLabels.gasLimit')}
                            type="number"
                            inputProps={{
                                min: 0,
                            }}
                            margin="normal"
                            variant="outlined"
                            value={parameters[FIELDS.GAS_LIMIT]}
                        />
                        <TextField
                            onChange={handleParameterInput}
                            fullWidth
                            name={FIELDS.STORAGE_LIMIT}
                            id={FIELDS.STORAGE_LIMIT}
                            required
                            error={String(parameters[FIELDS.STORAGE_LIMIT]) === ''}
                            type="number"
                            inputProps={{
                                min: 0,
                            }}
                            label={t('wallet.parameterLabels.storageLimit')}
                            margin="normal"
                            variant="outlined"
                            value={parameters[FIELDS.STORAGE_LIMIT]}
                        />
                    </Container>
                    <TabPanel value={tab2} index={1}>
                        <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
                        <CodeBlock showLineNumbers={false} language="bash" text={command} wrapLongLines />
                    </TabPanel>
                    {tab2 === 0 ? (
                        <>
                            <Divider sx={{ marginTop: 2, marginBottom: 2 }} />
                            <CallContract
                                parameters={{
                                    destination: contractInfo.address,
                                    amount: String(parameters.amount),
                                    fee: String(parameters.fee),
                                    parameters: {
                                        entrypoint: outputs.entrypoint,
                                        value: outputs.paramsJSON,
                                    },
                                    gasLimit: String(parameters.gas_limit),
                                    storageLimit: String(parameters.storage_limit),
                                }}
                            />
                        </>
                    ) : null}
                </RenderIfTrue>
            </RenderIfTrue>
        </RenderIfTrue>
    );
};

export default TransferParameters;
