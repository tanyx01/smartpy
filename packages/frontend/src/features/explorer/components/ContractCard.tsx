import React from 'react';

import {
    Alert,
    Chip,
    Divider,
    Grid,
    styled,
    Tooltip,
    useMediaQuery,
    Container as MuiContainer,
    Stack,
} from '@mui/material';

import CircularProgressWithText from '../../loader/components/CircularProgressWithText';
import { convertUnitWithSymbol, AmountUnit } from '../../../utils/units';
import AddressAvatar from '../../common/components/AddressAvatar';
import useTranslation from '../../i18n/hooks/useTranslation';
import { copyToClipboard } from '../../../utils/clipboard';
import { shortenText } from '../../../utils/style/shortenner';
import { useContractInfo } from '../selectors';
import { useNetworkInfo } from 'src/features/wallet/selectors/network';
import BasePaper from 'src/features/common/components/Paper';
import Fab from 'src/features/common/elements/Fab';
import Explorer from 'src/constants/explorer';
import { CallError } from 'SmartPyModels';

const Container = styled(MuiContainer)(() => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
}));

const AvatarSection = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    flexGrow: 1,
    borderRadius: 4,
    padding: 10,
    backgroundColor: theme.palette.background.paper,
}));

const Paper = styled(BasePaper)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'stretch',
    padding: theme.spacing(1),
}));

const FabButton = styled(Fab)<{ target?: string }>(({ theme }) => ({
    margin: 5,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: theme.palette.primary.main,
    [theme.breakpoints.down('sm')]: {
        flexGrow: 1,
    },
}));
const ContractInfo: React.FC = () => {
    const isMobile = useMediaQuery('(max-width:600px)');
    const contractInfo = useContractInfo();
    const networkInfo = useNetworkInfo();
    const t = useTranslation();

    const copyAddress = React.useCallback(() => {
        if (contractInfo.address) {
            copyToClipboard(contractInfo.address);
        }
    }, [contractInfo]);

    const buttons = React.useMemo(() => {
        if (networkInfo.network && contractInfo.address) {
            return (
                <Stack>
                    {!!Explorer.tzkt[networkInfo.network] && (
                        <FabButton
                            size="small"
                            variant="extended"
                            sx={{ borderColor: 'warning.light' }}
                            color="secondary"
                            aria-label={t('wallet.exploreWith.tzkt')}
                            href={`${Explorer.tzkt[networkInfo.network]}/${contractInfo.address}`}
                            target="_blank"
                        >
                            {t('wallet.exploreWith.tzkt')}
                        </FabButton>
                    )}
                    {!!Explorer.tzstats[networkInfo.network] && (
                        <FabButton
                            size="small"
                            variant="extended"
                            sx={{ borderColor: 'primary.light' }}
                            color="secondary"
                            aria-label={t('wallet.exploreWith.tzstats')}
                            href={`${Explorer.tzstats[networkInfo.network]}/${contractInfo.address}`}
                            target="_blank"
                        >
                            {t('wallet.exploreWith.tzstats')}
                        </FabButton>
                    )}
                    {!!Explorer.betterCallDev[networkInfo.network] && contractInfo.address.startsWith('KT') && (
                        <FabButton
                            size="small"
                            variant="extended"
                            sx={{ borderColor: 'success.dark' }}
                            color="secondary"
                            aria-label={t('wallet.exploreWith.betterCallDev')}
                            href={`${Explorer.betterCallDev[networkInfo.network]}/${contractInfo.address}`}
                            target="_blank"
                        >
                            {t('wallet.exploreWith.betterCallDev')}
                        </FabButton>
                    )}
                </Stack>
            );
        }
        return null;
    }, [contractInfo.address, networkInfo.network, t]);

    if (contractInfo.isLoading) {
        return <CircularProgressWithText size={64} margin={40} msg={t('common.loading')} />;
    }

    if (contractInfo.callErrors) {
        return (
            <>
                {contractInfo.callErrors?.map((error: CallError, index: number) => (
                    <Alert key={index} sx={{ marginTop: 2 }} severity="error">
                        {error.url}: {error.status} {error.message}
                    </Alert>
                ))}
            </>
        );
    }

    if (!contractInfo.valid || !contractInfo.address) {
        return null;
    }

    return (
        <>
            <Grid container justifyContent="center" alignItems="center" sx={{ marginTop: 2, marginBottom: 2 }}>
                <Grid item>
                    <Container>
                        <Paper>
                            <Grid container alignItems="stretch" justifyContent="center">
                                <Grid item xs={12} md={5} display="flex">
                                    <AvatarSection>
                                        <AddressAvatar address={contractInfo.address} size="large" />
                                        <Chip
                                            variant="outlined"
                                            color="primary"
                                            sx={{
                                                minHeight: 32,
                                                alignSelf: 'stretch',
                                            }}
                                            label={networkInfo.network}
                                        />
                                    </AvatarSection>
                                </Grid>
                                <Grid
                                    item
                                    xs={12}
                                    md={7}
                                    sx={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        flexDirection: 'column',
                                        padding: 2,
                                    }}
                                >
                                    <Tooltip title={t('common.copy') as string} aria-label="copy" placement="top">
                                        <Chip
                                            sx={{
                                                minHeight: 32,
                                                alignSelf: 'stretch',
                                            }}
                                            label={
                                                isMobile ? shortenText(contractInfo.address, 20) : contractInfo.address
                                            }
                                            color="primary"
                                            clickable
                                            onClick={copyAddress}
                                        />
                                    </Tooltip>
                                    <Divider sx={{ margin: 1 }} />
                                    <Chip
                                        sx={{
                                            minHeight: 32,
                                            alignSelf: 'stretch',
                                        }}
                                        label={convertUnitWithSymbol(
                                            contractInfo.balance || 0,
                                            AmountUnit.uTez,
                                            AmountUnit.tez,
                                        )}
                                    />
                                </Grid>
                            </Grid>
                        </Paper>
                        <p>
                            RPC:{' '}
                            <a href={`${networkInfo.rpc}/chains/main/blocks/head/header`} target="blank_">
                                {networkInfo.rpc}
                            </a>
                        </p>
                    </Container>
                </Grid>
                <Grid>{buttons}</Grid>
            </Grid>
        </>
    );
};

export default ContractInfo;
