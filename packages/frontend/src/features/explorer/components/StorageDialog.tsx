import React from 'react';
import { Link as LinkRouter } from 'react-router-dom';

import {
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Typography,
    Dialog,
    Slide,
    IconButton,
    SelectChangeEvent,
    Box,
} from '@mui/material';
import StorageIcon from '@mui/icons-material/Storage';

import Paper from 'src/features/common/components/Paper';
import useWalletContext from 'src/features/wallet/hooks/useWalletContext';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import { useDispatch } from 'react-redux';
import { updateContractInfo } from '../actions';

const Transition = React.forwardRef(function Transition(
    props: { children: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

interface OwnProps {
    updateURL: (address: string) => void;
}

const StorageDialog: React.FC<OwnProps> = ({ updateURL }) => {
    const [open, setOpen] = React.useState(false);
    const { contracts } = useWalletContext();
    const t = useTranslation();
    const dispatch = useDispatch();

    const loadStoredAccount = React.useCallback(
        (event: SelectChangeEvent<string>) => {
            const contract = contracts.find(({ id }) => id === event.target.value);
            if (contract?.address) {
                dispatch(updateContractInfo({ address: contract.address, valid: false }));
                updateURL(contract.address);
            }
        },
        [contracts, dispatch, updateURL],
    );

    const switchOpen = () => {
        setOpen((open) => !open);
    };

    return (
        <>
            <IconButton onClick={switchOpen}>
                <StorageIcon />
            </IconButton>

            <Dialog fullWidth TransitionComponent={Transition} open={open} onClose={switchOpen} hideBackdrop={true}>
                <Paper>
                    <FormControl variant="filled" fullWidth disabled={!contracts?.length}>
                        <InputLabel id="stored-contract-selector">
                            {t('explorer.labels.selectStoredContract')}
                        </InputLabel>
                        <Select
                            labelId="stored-contract-selector"
                            defaultValue=""
                            displayEmpty
                            onChange={loadStoredAccount}
                        >
                            {contracts.map((contract) => (
                                <MenuItem value={contract.id} key={contract.id} divider>
                                    <Typography variant="caption" noWrap>
                                        {contract.name}
                                    </Typography>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <Box sx={{ display: 'flex', justifyContent: 'end', marginTop: 1 }}>
                        <LinkRouter to="wallet/smart-contracts" target="_blank" style={{ textDecoration: 'none' }}>
                            {t('wallet.labels.manageContracts')}
                        </LinkRouter>
                    </Box>
                </Paper>
            </Dialog>
        </>
    );
};

export default StorageDialog;
