import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import {
    Alert,
    Divider,
    Typography,
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Rating,
    Box,
    Chip as MuiChip,
    styled,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ConfirmationIcon from '@mui/icons-material/VerifiedUser';

import OperationInformationDialog from 'src/features/wallet/views/OperationInformation';
import Paper from 'src/features/common/components/Paper';
import Button from 'src/features/common/elements/Button';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import { useAccountInfo } from 'src/features/wallet/selectors/account';
import WalletServices from 'src/features/wallet/services';
import Logger from 'src/services/logger';
import { showError } from '../actions';
import { OperationInformation } from 'SmartPyWalletTypes';
import { AmountUnit, convertUnitNumber } from 'src/utils/units';
import { Subscription } from 'rxjs';
import CircularProgressWithText from 'src/features/loader/components/CircularProgressWithText';
import CodeBlock from 'src/features/common/components/CodeBlock';
import SuccessIndicator from 'src/features/common/elements/SuccessIndicator';
import Fab from 'src/features/common/elements/Fab';
import Explorer from 'src/constants/explorer';
import { useNetworkInfo } from 'src/features/wallet/selectors/network';

const Chip = styled(MuiChip)(({ theme }) => ({
    backgroundColor: theme.palette.background.paper,
    margin: 10,
}));

const Container = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: theme.palette.background.default,
    padding: 10,
}));

type TransferResult = {
    hash: string;
    confirmations: number;
    address: string;
    operation: string;
};

type TransferError = {
    message: string;
    response: any;
};

const FabButton = styled(Fab)<{ target?: string }>(({ theme }) => ({
    margin: 5,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: theme.palette.primary.main,
    [theme.breakpoints.down('sm')]: {
        flexGrow: 1,
    },
}));

interface OwnProps {
    parameters: {
        destination: string;
        amount: string;
        fee: string;
        parameters: {
            entrypoint?: string;
            value?: string;
        };
        gasLimit: string;
        storageLimit: string;
    };
}

const CallContract: React.FC<OwnProps> = ({ parameters }) => {
    const t = useTranslation();
    const accountInformation = useAccountInfo();
    const networkInfo = useNetworkInfo();
    const dispatch = useDispatch();
    const [isCalling, setIsCalling] = React.useState(false);
    const [operationResult, setOperationResult] = React.useState<TransferResult>();
    const [operationError, setOperationError] = React.useState<TransferError>();
    const [transactionInformation, setTransactionInformation] = React.useState<OperationInformation>();
    const [resultExpanded, setResultExpanded] = React.useState(false);
    const subscription = React.useRef<Subscription>();

    const cleanOperationInformation = () => {
        setIsCalling(false);
        setOperationResult(undefined);
        setOperationError(undefined);
        setTransactionInformation(undefined);
    };

    const acceptOperationInformation = React.useCallback(async () => {
        // Reset operation information
        cleanOperationInformation();

        setIsCalling(true);

        try {
            subscription.current?.unsubscribe();
            const operation = await transactionInformation?.send();

            setOperationResult({
                hash: operation?.hash || '',
                confirmations: 0,
                address: operation?.contractAddress || '',
                operation: JSON.stringify(operation?.results, null, 4),
            });

            subscription.current = operation?.monitor?.subscribe((confirmations) => {
                setOperationResult((s) => (s ? { ...s, confirmations } : undefined));
            });
        } catch (e: any) {
            dispatch(showError(e.message));
            Logger.debug('Failed to call contract.', e);
            setOperationError({ message: e.message, response: JSON.parse(e.response) });
        }

        setIsCalling(false);
    }, [dispatch, transactionInformation]);

    const callContract = React.useCallback(async () => {
        setIsCalling(true);
        try {
            if (
                accountInformation?.source &&
                WalletServices[accountInformation.source] &&
                parameters.parameters.entrypoint &&
                parameters.parameters.value
            ) {
                const operation = await WalletServices[accountInformation.source].prepareTransfer({
                    destination: parameters.destination,
                    amount: convertUnitNumber(parameters.amount, AmountUnit.tez, AmountUnit.uTez).toFixed(),
                    fee: convertUnitNumber(parameters.fee, AmountUnit.tez, AmountUnit.uTez).toFixed(),
                    gas_limit: parameters.gasLimit,
                    storage_limit: parameters.storageLimit,
                    parameters: {
                        entrypoint: parameters.parameters.entrypoint,
                        value: JSON.parse(parameters.parameters.value),
                    },
                });
                setTransactionInformation(operation);
            }
        } catch (e: any) {
            dispatch(showError(e.message));
            Logger.debug('Failed to call contract.', e);
        }
        setIsCalling(false);
    }, [accountInformation.source, dispatch, parameters]);

    const pretty_operation_error = React.useMemo(() => {
        return (
            <>
                {operationError?.response?.map((o: any) => (
                    <>
                        {o?.contents?.map((c: any) => (
                            <>
                                {c?.metadata?.operation_result?.errors?.map((e: any, i: number) => {
                                    const with_ = e?.with?.string || e?.with?.int || e?.with?.nat;
                                    return (
                                        <Alert sx={{ marginTop: 2 }} severity="error" key={i}>
                                            {e?.id}
                                            <br />
                                            {e?.location ? (
                                                with_ ? (
                                                    <>
                                                        {t('explorer.errors.line', { line: e?.location })} {with_}
                                                    </>
                                                ) : (
                                                    <CodeBlock
                                                        language="json"
                                                        showLineNumbers
                                                        text={JSON.stringify(e.with, null, 4)}
                                                    />
                                                )
                                            ) : null}
                                            {e?.contract_handle && (
                                                <>{t('explorer.errors.contract', { address: e?.contract_handle })}</>
                                            )}
                                        </Alert>
                                    );
                                })}
                            </>
                        ))}
                    </>
                ))}
            </>
        );
    }, [t, operationError]);

    const result = React.useMemo(() => {
        if (isCalling) {
            return (
                <Paper>
                    <CircularProgressWithText size={64} margin={40} msg={'Calling Contract...'} />
                </Paper>
            );
        }
        if (operationResult) {
            return (
                <>
                    <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
                    <Container>
                        <Divider sx={{ marginTop: 1, marginBottom: 1 }} />

                        <Paper
                            sx={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                        >
                            <SuccessIndicator />

                            <Chip label={operationResult?.hash} variant="outlined" />
                        </Paper>

                        <Divider sx={{ marginTop: 1, marginBottom: 1 }} />

                        {networkInfo.network && Explorer.tzkt[networkInfo.network] ? (
                            <FabButton
                                variant="extended"
                                sx={{ borderColor: 'warning.light' }}
                                color="secondary"
                                aria-label={t('wallet.exploreWith.tzkt')}
                                href={`${Explorer.tzkt[networkInfo.network]}/${operationResult?.hash}`}
                                target="_blank"
                            >
                                {t('wallet.exploreWith.tzkt')}
                            </FabButton>
                        ) : null}

                        <Divider sx={{ marginTop: 1, marginBottom: 1 }} />

                        <Box
                            sx={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 10,
                            }}
                        >
                            <Typography variant="caption">{t('common.blockConfirmations')}</Typography>
                            <Rating
                                value={operationResult?.confirmations || 0}
                                max={10}
                                readOnly
                                emptyIcon={<ConfirmationIcon fontSize="small" />}
                                icon={<ConfirmationIcon fontSize="small" />}
                            />
                        </Box>
                    </Container>
                    <Accordion expanded={resultExpanded} onChange={() => setResultExpanded((s) => !s)}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>{t('explorer.section.callResult')}</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <CodeBlock language="json" showLineNumbers text={operationResult.operation} />
                        </AccordionDetails>
                    </Accordion>
                </>
            );
        } else if (operationError) {
            return (
                <>
                    {pretty_operation_error}
                    <Accordion
                        sx={{ marginTop: 2 }}
                        expanded={resultExpanded}
                        onChange={() => setResultExpanded((s) => !s)}
                    >
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>{t('explorer.section.rawCallError')}</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <CodeBlock
                                language="json"
                                showLineNumbers
                                text={JSON.stringify(operationError.response, null, 4)}
                            />
                        </AccordionDetails>
                    </Accordion>
                </>
            );
        }
        return null;
    }, [isCalling, operationResult, operationError, networkInfo.network, t, resultExpanded, pretty_operation_error]);

    return (
        <Paper>
            <Box display="flex">
                <Button
                    variant="contained"
                    color="primary"
                    onClick={callContract}
                    disabled={!accountInformation?.source || !parameters.parameters?.entrypoint}
                >
                    {t('explorer.section.callContract')}
                </Button>
            </Box>

            {result}
            <OperationInformationDialog
                operationInformation={transactionInformation}
                accept={acceptOperationInformation}
                cancel={cleanOperationInformation}
            />
        </Paper>
    );
};

export default CallContract;
