import React from 'react';
import { useDispatch } from 'react-redux';

import { Checkbox, Grid, IconButton, TextField as MuiTextField, Tooltip, Typography } from '@mui/material';
import SearchIcon from '@mui/icons-material/ManageSearch';
import SaveIcon from '@mui/icons-material/Save';

import actions, { updateContractInfo } from '../actions';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import { useContractInfo } from '../selectors';
import Button from 'src/features/common/elements/Button';
import * as TezosRPC from 'src/utils/tezosRpc';
import { useSearchParams } from 'src/hooks/useSearchParams';
import Logger from 'src/services/logger';
import { updateNetworkInfo } from 'src/features/wallet/actions';
import StorageDialog from './StorageDialog';
import AddContractDialog from 'src/features/wallet/components/AddContract';
import { Network } from 'src/constants/networks';
import Explorer from 'src/constants/explorer';
import httpRequester from 'src/services/httpRequester';
import useWalletContext from 'src/features/wallet/hooks/useWalletContext';
import { ContractResponse } from 'src/utils/tezosRpc';
import { operationsDetails } from 'SmartPyModels';
import { CallError } from 'SmartPyModels';

const updateURL = (address: string) => {
    window.history.pushState({}, `Explore - ${address}`, `${window.location.pathname}?address=${address}`);
};

const callNodeWithData = async (url: string) => {
    const { data } = await httpRequester.get(url);
    return data;
};

const getOperations = async (network: Network, address: string) => {
    const api = `${Explorer.tzktAPI[network]}/contracts/${address}`;
    const infos = await callNodeWithData(api);
    let n_ops = 0;
    if (infos) {
        n_ops =
            parseInt(infos['numTransactions']) |
            parseInt(infos['n_ops']) |
            (parseInt(infos['n_calls_success']) + parseInt(infos['n_calls_failed']));
    }
    const result = await callNodeWithData(
        `${Explorer.tzktAPI[network]}/operations/transactions/?target=${address}&micheline=3&select=storage,parameter,sender,target,timestamp,address,block,hash,status`,
    );

    return {
        ops: result,
        nOps: n_ops,
        firstSeen: infos['firstActivityTime'],
        lastSeen: infos['lastActivityTime'],
    };
};

interface OwnProps {
    resetSmartMLOutputs: () => void;
}

const ExplorerSetup: React.FC<OwnProps> = ({ resetSmartMLOutputs }) => {
    const isMounted = React.useRef(false);
    const [saveDialog, setSaveDialog] = React.useState(false);
    const { address } = useSearchParams();
    const t = useTranslation();
    const dispatch = useDispatch();
    const contractInfo = useContractInfo();
    const [rpc, setRPC] = React.useState('');
    const [useExplicitRPC, setUseExplicitRPC] = React.useState(false);
    const { contracts } = useWalletContext();

    const isContractStored = contracts.some(({ address }) => address === contractInfo.address);

    const switchSaveDialog = () => {
        setSaveDialog((open) => !open);
    };

    const onContractAddressChange = React.useCallback(
        (event: React.ChangeEvent<{ value: string }>) => {
            dispatch(actions.updateContractInfo({ address: event.target.value, valid: false, callErrors: undefined }));
            updateURL(event.target.value);
        },
        [dispatch],
    );

    const onRPCChange = (event: React.ChangeEvent<{ value: string }>) => {
        setRPC(event.target.value);
    };

    const exploreOperations = React.useCallback(
        async (address: string, network: Network, result: ContractResponse) => {
            dispatch(updateContractInfo({ isOperationsLoading: true }));
            let op_result = {} as operationsDetails;
            try {
                op_result = await getOperations(network, address);
                window.smartmlCtx.call_exn_handler(
                    'exploreOperations',
                    address,
                    JSON.stringify(result.contract),
                    JSON.stringify(op_result.ops),
                );
                dispatch(updateContractInfo({ isOperationsLoading: false, opDetails: op_result }));
            } catch (e: any) {
                Logger.debug(e);
                dispatch(
                    updateContractInfo({
                        isOperationsLoading: false,
                        operationsErrors: 'Could not find operations.',
                    }),
                );
            }
        },
        [dispatch],
    );

    const exploreContract = React.useCallback(
        async (address: string, rpc = '', useExplicitRPC = false) => {
            resetSmartMLOutputs();
            dispatch(updateContractInfo({ isLoading: true, valid: false, callErrors: undefined }));
            if (address) {
                let result: Promise<ContractResponse>;
                if (useExplicitRPC && rpc) {
                    result = TezosRPC.lookupContract(address, rpc);
                } else {
                    result = TezosRPC.lookupContract(address);
                }
                result
                    .then(async (response: ContractResponse) => {
                        TezosRPC.getRpcNetwork(response.rpc)
                            .then((network) => {
                                dispatch(updateNetworkInfo({ rpc: response.rpc, network }));

                                const storage: string = window.smartmlCtx.call(
                                    'parseStorage',
                                    JSON.stringify(response.contract),
                                );

                                dispatch(
                                    updateContractInfo({
                                        isLoading: false,
                                        balance: response.contract.balance,
                                        valid: true,
                                        storage: storage.toString(),
                                    }),
                                );

                                window.smartmlCtx.call_exn_handler(
                                    'exploreTwo',
                                    address,
                                    JSON.stringify(response.contract),
                                    network.toLowerCase(),
                                );

                                dispatch(updateContractInfo({ isLoading: false, valid: true }));

                                exploreOperations(address, network, response);
                            })
                            .catch((e) => {
                                Logger.debug(e);
                                const callError = [
                                    {
                                        url: e.url,
                                        status: e.response?.status || 404,
                                        message: e.response === undefined ? 'Not found' : e.response.statusText,
                                    },
                                ];
                                dispatch(
                                    actions.updateContractInfo({
                                        isLoading: false,
                                        callErrors: callError,
                                        valid: false,
                                    }),
                                );
                            });
                    })
                    .catch((errors: CallError[]) => {
                        dispatch(
                            updateContractInfo({
                                isLoading: false,
                                valid: false,
                                callErrors: errors,
                            }),
                        );
                    });
            }
        },
        [dispatch, exploreOperations, resetSmartMLOutputs],
    );

    const onExploreContract = React.useCallback(async () => {
        exploreContract(contractInfo.address || '', rpc, useExplicitRPC);
    }, [contractInfo.address, exploreContract, rpc, useExplicitRPC]);

    React.useEffect(() => {
        if (!isMounted.current && address) {
            exploreContract(address);
            dispatch(actions.updateContractInfo({ address }));
            isMounted.current = true;
        }
    }, [address, dispatch, exploreContract]);

    return (
        <>
            <Typography variant="h6" sx={{ marginBottom: 2 }}>
                {t('explorer.labels.search')}{' '}
                <Typography variant="caption">{t('explorer.labels.lookupDescription')}</Typography>
            </Typography>
            <Grid container justifyContent="flex-start" spacing={2}>
                <Grid item xs={12} sm={4} display="flex" justifyContent="start" alignItems="center">
                    <StorageDialog updateURL={updateURL} />
                    <MuiTextField
                        fullWidth
                        id="contractId"
                        label={t('explorer.labels.contractAddress') as string}
                        variant="outlined"
                        value={contractInfo.address || ''}
                        onChange={onContractAddressChange}
                        onKeyPress={(e) => {
                            if (e.key === 'Enter') {
                                onExploreContract();
                            }
                        }}
                        InputProps={{
                            endAdornment: (
                                <Tooltip
                                    title={t('explorer.labels.saveContract') as string}
                                    aria-label="update-contract"
                                    placement="left"
                                >
                                    <span>
                                        <IconButton color="primary" onClick={switchSaveDialog} size="large">
                                            <SaveIcon />
                                        </IconButton>
                                    </span>
                                </Tooltip>
                            ),
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={'auto'} display="flex" justifyContent="start" alignItems="center">
                    <Checkbox
                        value={useExplicitRPC}
                        sx={{ marginRight: 1 }}
                        onChange={() => setUseExplicitRPC((state) => !state)}
                    />
                    <Typography variant="caption">{t('explorer.labels.explicitRPC')}</Typography>
                </Grid>
                {useExplicitRPC && (
                    <Grid item xs={12} md={3} lg={4} display="flex" justifyContent="start" alignItems="center">
                        <MuiTextField
                            fullWidth
                            size="small"
                            label={t('explorer.labels.nodeRPC') as string}
                            variant="outlined"
                            value={rpc || ''}
                            error={!rpc && useExplicitRPC}
                            onChange={onRPCChange}
                        />
                    </Grid>
                )}
                <Grid item xs={12} md={4} lg={3} display="flex" justifyContent="end" alignItems="center">
                    <Button
                        fullWidth
                        size="large"
                        variant="contained"
                        disabled={!contractInfo.address}
                        onClick={() => onExploreContract()}
                    >
                        {t('explorer.labels.exploreContract')}
                        <SearchIcon sx={{ marginLeft: 1 }} />
                    </Button>
                </Grid>
            </Grid>
            <AddContractDialog defaultAddress={contractInfo.address} onClose={switchSaveDialog} open={saveDialog} />
        </>
    );
};

export default ExplorerSetup;
