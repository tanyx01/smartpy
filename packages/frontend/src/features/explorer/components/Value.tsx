import React, { useEffect } from 'react';

import { defineSmartPyComponents } from './smartPyGlue';

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace JSX {
        interface IntrinsicElements {
            'sp-value': any;
        }
    }
}

defineSmartPyComponents();

export default function SmartPyValueContainer(props: any) {
    return <sp-value initial-value={JSON.stringify(props.json)} editable={props.editable}></sp-value>;
}
