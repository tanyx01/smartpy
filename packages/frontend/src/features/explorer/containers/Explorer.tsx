import React from 'react';
import { useDispatch } from 'react-redux';

// Local Polyfills
import 'src/polyfills';

import ExplorerView from '../views/Explorer';
import Logger from 'src/services/logger';
import Loading from 'src/features/loader/components/LinearProgressWithText';
import actions from '../actions';
import asyncScriptInjector from 'src/utils/asyncScriptInjector';
import scripts from '../constants/scripts';
import { ExplorerSmartMLOutputs } from 'SmartPyModels';

window.setSelectVisibility = (selectId: string, ...rest: string[]) => {
    try {
        for (let i = 0; i < rest.length; i += 2) {
            const value = (document.getElementById(selectId) as HTMLInputElement)?.value;
            if (rest[i] === value) {
                (document.getElementById(rest[i + 1]) as HTMLInputElement).style.display = 'block';
            } else {
                (document.getElementById(rest[i + 1]) as HTMLInputElement).style.display = 'none';
            }
        }
    } catch (e) {
        Logger.debug(e);
    }
};

window.setValueVisibility = (value: string, ...rest: string[]) => {
    let i;
    for (i = 0; i < rest.length; i += 2) {
        if (rest[i] === value) {
            (document.getElementById(rest[i + 1]) as HTMLInputElement).style.display = 'block';
        } else {
            (document.getElementById(rest[i + 1]) as HTMLInputElement).style.display = 'none';
        }
    }
};

const ExplorerContainer = () => {
    const [outputs, setOutputs] = React.useState<ExplorerSmartMLOutputs>({});
    const dispatch = useDispatch();
    // States
    const [ready, setReady] = React.useState(false);
    const [percentage, setPercentage] = React.useState(0);
    const [latestScript, setLatestScript] = React.useState('');

    /**
     * Set explorer outputs
     *
     * @param {string} key - identifier
     * @param {string} output
     * @return {void}
     */
    const setExplorerOutput = (key: string, output: string) => {
        if (['entrypoint', 'paramsJSON', 'paramsMicheline', 'errors'].includes(key)) {
            setOutputs((outputs) => ({
                ...outputs,
                [key]: output || '',
            }));
        } else {
            setOutputs((outputs) => ({
                ...outputs,
                [key]: (
                    <div
                        dangerouslySetInnerHTML={{
                            __html: output,
                        }}
                    />
                ),
            }));
        }
    };

    /**
     * Reset outputs
     */
    const resetSmartMLOutputs = () => {
        setOutputs({});
    };

    /**
     * @summary Show error callback
     * @param {string} error - Error message
     * @return {void}
     */
    const showError = React.useCallback((error: string) => dispatch(actions.showError(error)), [dispatch]);

    /**
     * Update loading state (in percentage %)
     */
    const onProgressUpdate = (percentage: number, loadedScript: string) => {
        setPercentage(percentage);
        setLatestScript(loadedScript);
        if (percentage === 100) {
            setReady(true);
        }
    };

    React.useEffect(() => {
        (window as any).cleanMessages = () => {
            setOutputs((outputs) => ({
                ...outputs,
                errors: '',
            }));
        };
        window.showErrorPre = (error: string) => showError('<pre>' + error + '</pre>');
        window.smartpyContext = {
            ...(window.smartpyContext || {}),
            showError,
            setExplorerOutput,
        };
        asyncScriptInjector(scripts, onProgressUpdate, (scriptName) =>
            Logger.error(`Failed to load script: ${scriptName}`),
        );

        const onResize = () => {
            if (window.screen.width < 768) {
                document.getElementById('viewport-meta')?.setAttribute('content', 'width=768');
            }
        };
        onResize();
        window.addEventListener('resize', onResize);

        return () => {
            window.removeEventListener('resize', onResize);
            document.getElementById('viewport-meta')?.setAttribute('content', 'width=device-width, initial-scale=1');
        };
    }, [showError]);

    if (!ready) {
        return (
            <Loading
                msg={`Loading ${latestScript ? '(' + latestScript + ')' : ''} ...`}
                variant="determinate"
                value={percentage}
            />
        );
    }

    return <ExplorerView outputs={outputs} resetSmartMLOutputs={resetSmartMLOutputs} />;
};

export default ExplorerContainer;
