import React from 'react';
import { useDispatch } from 'react-redux';
import { Subscription } from 'rxjs';

import { TezosConstants } from '../../../services/conseil';

// Material UI
import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import { SelectChangeEvent } from '@mui/material/Select';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import CodeIcon from '@mui/icons-material/Code';
import Rating from '@mui/material/Rating';
import ConfirmationIcon from '@mui/icons-material/VerifiedUser';

// Local Components
import CodeBlock from '../../common/components/CodeBlock';
// Local Utils
import { prettifyJsonString } from '../../../utils/json';
// Local Widgets
import WalletWidget from '../../wallet/containers/WalletWidget';
import { useNetworkInfo } from '../../wallet/selectors/network';
import useTranslation from '../../i18n/hooks/useTranslation';
import { AmountUnit, convertUnitNumber } from '../../../utils/units';
import { useAccountInfo } from '../../wallet/selectors/account';
import WalletServices from '../../wallet/services';
import logger from '../../../services/logger';
import CircularProgressWithText from '../../loader/components/CircularProgressWithText';
import AddressAvatar from '../../common/components/AddressAvatar';
import { appendClasses } from '../../../utils/style/classes';

import TabPanel from '../../common/elements/TabPanel';
import SuccessIndicator from '../../common/elements/SuccessIndicator';
import TezosClient from './TezosClient';
import { jsonToMicheline } from '../util/michelson';
import OriginationSizes from '../components/ContractSizes';
import { useOriginationContract } from '../selectors/contract';
import ShareOrigination from '../components/ShareOrigination';
import { generateID } from '../../../utils/rand';
import { showError, updateOriginationContract } from '../actions';
import OperationInformationDialog from '../../wallet/views/OperationInformation';
import { OperationInformation } from 'SmartPyWalletTypes';
import { addContract } from '../../mich-ide/actions';
import OriginationParameters, { FIELDS } from '../components/OriginationParameters';
import { getBase } from '../../../utils/url';
import { NetworkProtocolHash } from '../../../constants/networks';
import NetworkSelection from 'src/features/common/components/NetworkSelection';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'space-around',
            justifyItems: 'space-around',
            alignContent: 'space-around',
            backgroundColor: theme.palette.background.paper,
            flexGrow: 1,
        },
        spacer: {
            margin: theme.spacing(2),
        },
        section: {
            padding: 10,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        accountSection: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            backgroundColor: theme.palette.background.default,
            padding: 10,
        },
        card: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        marginTop: {
            marginTop: 10,
        },
        marginBottom: {
            marginBottom: 10,
        },
        container: {
            backgroundColor: theme.palette.background.default,
            borderTopWidth: 0,
            color: theme.palette.getContrastText(theme.palette.background.default),

            boxShadow: '0 19px 38px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',

            padding: 20,
        },
        chip: {
            backgroundColor: theme.palette.background.paper,
            margin: 10,
        },
        input: {
            padding: '10px 12px 8px',
        },
        fullHeight: {
            height: '100%',
        },
        hidden: {
            display: 'none',
        },
    }),
);

type OriginationResult = {
    hash: string;
    confirmations: number;
    address: string;
    operation: string;
};

const DEFAULT_PARAMETERS = {
    [FIELDS.NAME]: `contract_${generateID()}`,
    [FIELDS.SOURCE]: '',
    [FIELDS.DELEGATE]: '',
    [FIELDS.BALANCE]: 0.0,
    [FIELDS.FEE]: convertUnitNumber(
        TezosConstants.DefaultAccountOriginationFee,
        AmountUnit.uTez,
        AmountUnit.tez,
    ).toNumber(),
    [FIELDS.GAS_LIMIT]: TezosConstants.DefaultAccountOriginationGasLimit,
    [FIELDS.STORAGE_LIMIT]: TezosConstants.DefaultAccountOriginationStorageLimit,
};

interface OwnProps {
    onRpcChange: (event: React.ChangeEvent<{ value: string }>) => void;
    onNetworkSelection: (event: SelectChangeEvent<string>) => void;
}

const OriginationView: React.FC<OwnProps> = (props) => {
    const classes = useStyles();
    const [tab1, setTab1] = React.useState(0);
    const [tab2, setTab2] = React.useState(0);
    const [isOriginating, setOrigination] = React.useState(false);
    const [resultExpanded, setResultExpanded] = React.useState(false);
    const [originationResult, setOriginationResult] = React.useState<OriginationResult>();
    const contract = useOriginationContract();
    const [parameters, setParameters] = React.useState(DEFAULT_PARAMETERS);
    const { rpc, network } = useNetworkInfo();
    const accountInformation = useAccountInfo();
    const subscription = React.useRef<Subscription>();
    const t = useTranslation();
    const dispatch = useDispatch();
    const [originationInformation, setOriginationInformation] = React.useState<OperationInformation>();
    const [editingCode, setEditingCode] = React.useState(false);
    const [editingStorage, setEditingStorage] = React.useState(false);

    const { onRpcChange } = props;

    React.useEffect(() => {
        dispatch(showError(''));
        return () => {
            // Unsubscribe on component dismount
            subscription.current?.unsubscribe();
        };
    }, [dispatch]);

    const codeMicheline = React.useMemo(
        () => jsonToMicheline(contract.codeJson, NetworkProtocolHash?.[network || '']),
        [network, contract.codeJson],
    );
    const storageMicheline = React.useMemo(
        () => jsonToMicheline(contract.storageJson, NetworkProtocolHash?.[network || '']),
        [network, contract.storageJson],
    );

    const handleTab1Change = (event: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setTab1(newValue);
    };

    const handleTab2Change = (event: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setTab2(newValue);
    };

    const switchEditingCode = () => setEditingCode((b) => !b);
    const switchEditingStorage = () => setEditingStorage((b) => !b);

    const openMichelsonEditor = () => {
        dispatch(addContract({ name: parameters[FIELDS.NAME], code: codeMicheline }, false));
        window.open(`${getBase()}/michelson`);
    };

    const updateStorage = (storage: string) => {
        if (storage) {
            dispatch(updateOriginationContract({ storageJson: storage }));
        }
    };

    const updateCode = (code: string) => {
        if (code) {
            dispatch(updateOriginationContract({ codeJson: code }));
        }
    };

    const handleParameterInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        setParameters((parameters) => ({ ...parameters, [event.target.id]: event.target.value }));
    };

    const reset = () => {
        setOriginationResult(undefined);
        setResultExpanded(false);
    };

    const estimateOriginationCosts = React.useCallback(async () => {
        try {
            if (accountInformation?.source && WalletServices[accountInformation.source]) {
                const estimation = await WalletServices[accountInformation.source].estimateOrigination({
                    code: JSON.parse(contract.codeJson || ''),
                    storage: JSON.parse(contract.storageJson || ''),
                });

                if (estimation) {
                    setParameters((state) => ({
                        ...state,
                        [FIELDS.FEE]: convertUnitNumber(estimation.fee, AmountUnit.uTez, AmountUnit.tez).toNumber(),
                        [FIELDS.GAS_LIMIT]: estimation.gasLimit,
                        [FIELDS.STORAGE_LIMIT]: estimation.storageLimit,
                    }));
                }
            }
        } catch (e: any) {
            const translation = t('origination.couldNotEstimateCosts');
            dispatch(showError(e.message));
            logger.warn(translation, e);
        }
    }, [accountInformation.source, contract.codeJson, contract.storageJson, dispatch, t]);

    const acceptOperationInformation = React.useCallback(async () => {
        // Reset operation information
        cleanOperationInformation();

        setOrigination(true);

        try {
            subscription.current?.unsubscribe();
            const operation = await originationInformation?.send();

            setOriginationResult({
                hash: operation?.hash || '',
                confirmations: 0,
                address: operation?.contractAddress || '',
                operation: JSON.stringify(operation?.results, null, 4),
            });

            subscription.current = operation?.monitor?.subscribe((confirmations) => {
                setOriginationResult((s) => (s ? { ...s, confirmations } : undefined));
            });
        } catch (e: any) {
            dispatch(showError(e.message));
            logger.debug('Failed to originate contract.', e);
        }

        setOrigination(false);
    }, [dispatch, originationInformation]);

    const cleanOperationInformation = () => {
        setOrigination(false);
        setOriginationInformation(undefined);
    };

    const originateContract = React.useCallback(async () => {
        reset();
        setOrigination(true);
        try {
            if (accountInformation?.source && WalletServices[accountInformation.source]) {
                const originationOperation = await WalletServices[accountInformation.source].prepareOrigination({
                    code: contract.codeJson,
                    storage: contract.storageJson,
                    ...parameters,
                });
                setOriginationInformation(originationOperation);
            }
        } catch (e: any) {
            dispatch(showError(e.message));
            logger.debug('Failed to originate contract.', e);
        }
        setOrigination(false);
    }, [accountInformation.source, contract.codeJson, contract.storageJson, parameters, dispatch]);

    const result = React.useMemo(() => {
        if (isOriginating) {
            return (
                <>
                    <div className={classes.spacer} />
                    <Paper className={classes.section}>
                        <CircularProgressWithText size={64} margin={40} msg={'Originating Contract...'} />
                    </Paper>
                </>
            );
        }
        if (originationResult) {
            return (
                <>
                    <div className={classes.spacer} />
                    <Paper className={classes.section}>
                        <Typography variant="h6">{t('origination.result')}</Typography>
                        <Divider className={classes.divider} />
                        <Box className={classes.accountSection}>
                            <div className={classes.spacer} />

                            <Paper className={appendClasses(classes.card, classes.section)}>
                                <SuccessIndicator />
                                <Typography className={classes.marginTop} variant="overline">
                                    {t('origination.successfulDeployment')}
                                </Typography>
                                <AddressAvatar address={originationResult?.address} size="large" />
                                <Chip label={originationResult?.address} className={classes.chip} variant="outlined" />
                            </Paper>

                            <div className={classes.spacer} />

                            <Grid container spacing={2} alignItems="center" justifyContent="center">
                                <Grid item>
                                    <Button
                                        href={`./explorer?address=${originationResult?.address}`}
                                        variant="outlined"
                                        target="_blank"
                                    >
                                        {t('origination.openExplorer')}
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Button
                                        href={`${getBase()}/wallet/smart-contracts?addAddress=${
                                            originationResult?.address
                                        }`}
                                        variant="outlined"
                                        target="_blank"
                                    >
                                        {t('origination.saveContract')}
                                    </Button>
                                </Grid>
                            </Grid>

                            <div className={classes.spacer} />

                            <div className={classes.card}>
                                <Typography variant="caption">{t('common.blockConfirmations')}</Typography>
                                <Rating
                                    value={originationResult?.confirmations || 0}
                                    max={10}
                                    readOnly
                                    emptyIcon={<ConfirmationIcon fontSize="small" />}
                                    icon={<ConfirmationIcon fontSize="small" />}
                                />
                            </div>
                        </Box>
                        <Accordion expanded={resultExpanded} onChange={() => setResultExpanded((s) => !s)}>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                                <Typography>{t('origination.result')}</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <CodeBlock language="json" showLineNumbers text={originationResult.operation} />
                            </AccordionDetails>
                        </Accordion>
                    </Paper>
                </>
            );
        }
    }, [resultExpanded, originationResult, isOriginating, classes, t]);

    /**
     * Deploy should be disable if no account was selected or parameters are incomplete
     */
    const isDeployDisabled = React.useMemo(() => {
        return !(
            accountInformation.pkh &&
            Object.keys(parameters)
                .filter((f) => f !== 'delegate')
                .every((v) => !!v)
        );
    }, [accountInformation, parameters]);

    const OriginationParams = React.useCallback(
        (isForTezosClient: boolean) => (
            <OriginationParameters
                parameters={parameters}
                handleParameterInput={handleParameterInput}
                isForTezosClient={isForTezosClient}
                estimateOriginationCosts={estimateOriginationCosts}
                estimateDisabled={Boolean(
                    !accountInformation?.pkh ||
                        accountInformation.activationRequired ||
                        accountInformation.revealRequired,
                )}
            />
        ),
        [accountInformation, estimateOriginationCosts, parameters],
    );

    return (
        <div className={classes.root}>
            <div>
                <AppBar position="static" color="secondary">
                    <Tabs value={tab1} onChange={handleTab1Change} aria-label="Origination Menu" variant="fullWidth">
                        <Tab label={t('origination.originateFromBrowser')} />
                        <Tab label={t('origination.withTezosClient')} />
                    </Tabs>
                </AppBar>
                <TabPanel className={classes.container} value={tab1} index={0}>
                    <Typography className={classes.marginBottom} variant="h4">
                        {t('origination.title')}
                    </Typography>
                    <Typography variant="body1">{t('origination.subTitle')}</Typography>

                    <div className={classes.spacer} />
                    <NetworkSelection />

                    <div className={classes.spacer} />
                    <Paper className={classes.section}>
                        <Typography variant="h6">{t('origination.sections.wallet')} </Typography>
                        <Divider className={classes.divider} />
                        <WalletWidget rpc={rpc} disableNetworkSelection={false} />
                    </Paper>

                    <div className={classes.spacer} />

                    {OriginationParams(false)}

                    <div className={classes.spacer} />

                    <Paper className={classes.section}>
                        <div>
                            <Typography variant="h6">
                                {t('origination.deployContract')} <ShareOrigination contract={contract} />
                            </Typography>
                        </div>
                        <Divider className={classes.divider} />
                        <Grid container spacing={2} alignItems="center">
                            <Grid item>
                                <Typography variant="body2">{t('origination.deployContractHint')}</Typography>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className={classes.fullHeight}
                                    onClick={originateContract}
                                    disabled={isDeployDisabled}
                                >
                                    {t('origination.deployContract')}
                                </Button>
                            </Grid>
                        </Grid>
                        <Divider className={classes.divider} />
                    </Paper>

                    {result}
                </TabPanel>
                <TabPanel className={classes.container} value={tab1} index={1}>
                    <TezosClient
                        storage={storageMicheline}
                        code={codeMicheline}
                        parameters={parameters}
                        handleParameterInput={handleParameterInput}
                        onRpcChange={onRpcChange}
                        rpc={rpc}
                        estimateOriginationCosts={estimateOriginationCosts}
                    >
                        {OriginationParams(true)}
                    </TezosClient>
                </TabPanel>
            </div>
            <div>
                <AppBar position="static" color="secondary">
                    <Tabs value={tab2} onChange={handleTab2Change} aria-label="Outputs Menu" variant="fullWidth">
                        <Tab label={t('common.sizes')} />
                        <Tab label={t('common.storage')} />
                        <Tab label={t('common.code')} />
                        <Tab label={`${t('common.storage')} JSON`} />
                        <Tab label={`${t('common.code')} JSON`} />
                    </Tabs>
                </AppBar>
                <TabPanel className={classes.container} value={tab2} index={0}>
                    <Typography className={classes.marginBottom} variant="h4">
                        {t('common.sizes')}
                    </Typography>
                    <OriginationSizes />
                </TabPanel>
                <TabPanel className={classes.container} value={tab2} index={1}>
                    <Typography className={classes.marginBottom} variant="h4">
                        {t('common.initialStorage')}
                    </Typography>
                    <CodeBlock language="michelson" showLineNumbers text={storageMicheline || ''} />
                </TabPanel>
                <TabPanel className={classes.container} value={tab2} index={2}>
                    <Typography className={classes.marginBottom} variant="h4">
                        {t('common.michelsonCode')}{' '}
                        <Button color="primary" endIcon={<CodeIcon />} onClick={openMichelsonEditor}>
                            {t('origination.openMichelsonEditor')}
                        </Button>
                    </Typography>
                    <CodeBlock language="michelson" showLineNumbers text={codeMicheline || ''} />
                </TabPanel>
                <TabPanel className={classes.container} value={tab2} index={3}>
                    <Typography className={classes.marginBottom} variant="h4">
                        {t('common.initialStorage')}
                    </Typography>
                    <CodeBlock
                        language="json"
                        showLineNumbers
                        text={prettifyJsonString(contract.storageJson, 4)}
                        editProps={{
                            open: editingStorage,
                            onOpenChange: switchEditingStorage,
                            onCodeChange: updateStorage,
                        }}
                    />
                </TabPanel>
                <TabPanel className={classes.container} value={tab2} index={4}>
                    <Typography className={classes.marginBottom} variant="h4">
                        {t('common.michelsonCode')}
                    </Typography>
                    <CodeBlock
                        language="json"
                        showLineNumbers
                        text={prettifyJsonString(contract.codeJson, 4)}
                        editProps={{
                            open: editingCode,
                            onOpenChange: switchEditingCode,
                            onCodeChange: updateCode,
                        }}
                    />
                </TabPanel>
            </div>
            <OperationInformationDialog
                operationInformation={originationInformation}
                accept={acceptOperationInformation}
                cancel={cleanOperationInformation}
            />
        </div>
    );
};

export default OriginationView;
