/* eslint-disable react/jsx-no-undef */
import React from 'react';

import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import FormControl from '@mui/material/FormControl';
import InputAdornment from '@mui/material/InputAdornment';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import EstimateIcon from '@mui/icons-material/Cached';

import { RenderIfTrue } from '../../../utils/conditionalRender';
import { OriginationParameters as ParametersType } from 'OriginationTypes';
import useTranslation from '../../i18n/hooks/useTranslation';
import { AmountUnit, TezUnit } from '../../../utils/units';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        section: {
            padding: 10,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        boxContainer: {
            borderRadius: 10,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
    }),
);

export enum FIELDS {
    ENDPOINT = 'endpoint',
    NAME = 'name',
    SOURCE = 'source',
    BALANCE = 'balance',
    DELEGATE = 'delegate',
    FEE = 'fee',
    GAS_LIMIT = 'gas_limit',
    STORAGE_LIMIT = 'storage_limit',
}

type OwnProps = {
    parameters: ParametersType;
    handleParameterInput: (event: React.ChangeEvent<HTMLInputElement>) => void;
    isForTezosClient?: boolean;
    estimateOriginationCosts: () => Promise<void>;
    estimateDisabled: boolean;
};

const OriginationParameters: React.FC<OwnProps> = ({
    parameters,
    handleParameterInput,
    isForTezosClient = false,
    estimateOriginationCosts,
    estimateDisabled,
}) => {
    const classes = useStyles();
    const t = useTranslation();

    return (
        <Paper className={classes.section}>
            <Typography variant="h6">{t('origination.sections.contractOriginationParameters')}</Typography>

            <Divider className={classes.divider} />
            <Box className={classes.boxContainer}>
                <RenderIfTrue isTrue={isForTezosClient}>
                    <FormControl fullWidth variant="outlined" margin="normal">
                        <InputLabel htmlFor={FIELDS.NAME}>{t('wallet.parameterLabels.contractName')}</InputLabel>
                        <OutlinedInput
                            required
                            name={FIELDS.NAME}
                            onChange={handleParameterInput}
                            id={FIELDS.NAME}
                            fullWidth
                            label={t('wallet.parameterLabels.contractName')}
                            value={parameters[FIELDS.NAME]}
                        />
                    </FormControl>
                    <FormControl fullWidth variant="outlined" margin="normal">
                        <InputLabel htmlFor={FIELDS.SOURCE}>{t('wallet.parameterLabels.source')}</InputLabel>
                        <OutlinedInput
                            required
                            name={FIELDS.SOURCE}
                            onChange={handleParameterInput}
                            id={FIELDS.SOURCE}
                            fullWidth
                            label={t('wallet.parameterLabels.source')}
                            value={parameters[FIELDS.SOURCE]}
                        />
                    </FormControl>
                </RenderIfTrue>
                <FormControl fullWidth variant="outlined" margin="normal">
                    <InputLabel htmlFor={FIELDS.DELEGATE}>{t('wallet.parameterLabels.delegate')}</InputLabel>
                    <OutlinedInput
                        name={FIELDS.DELEGATE}
                        onChange={handleParameterInput}
                        id={FIELDS.DELEGATE}
                        fullWidth
                        label={t('wallet.parameterLabels.delegate')}
                        value={parameters[FIELDS.DELEGATE]}
                    />
                </FormControl>
                <FormControl
                    fullWidth
                    variant="outlined"
                    margin="normal"
                    required
                    error={String(parameters[FIELDS.BALANCE]) === ''}
                >
                    <InputLabel htmlFor={FIELDS.BALANCE}>{t('wallet.parameterLabels.amount')}</InputLabel>
                    <OutlinedInput
                        name={FIELDS.BALANCE}
                        onChange={handleParameterInput}
                        id={FIELDS.BALANCE}
                        fullWidth
                        type="number"
                        inputProps={{
                            min: 0.0,
                            step: 0.000001,
                        }}
                        label={t('wallet.parameterLabels.amount')}
                        startAdornment={
                            <InputAdornment position="start">{AmountUnit[TezUnit.tez].char}</InputAdornment>
                        }
                        value={parameters[FIELDS.BALANCE]}
                    />
                </FormControl>
                <Divider className={classes.divider} />
                <Button
                    disabled={estimateDisabled}
                    color="primary"
                    variant="outlined"
                    startIcon={<EstimateIcon />}
                    onClick={estimateOriginationCosts}
                >
                    {t('origination.estimateCost')}
                </Button>
                <Divider className={classes.divider} />
                <FormControl
                    fullWidth
                    variant="outlined"
                    margin="normal"
                    required
                    error={String(parameters[FIELDS.FEE]) === ''}
                >
                    <InputLabel htmlFor={FIELDS.FEE}>{t('wallet.parameterLabels.fee')}</InputLabel>
                    <OutlinedInput
                        name={FIELDS.FEE}
                        onChange={handleParameterInput}
                        id={FIELDS.FEE}
                        fullWidth
                        type="number"
                        inputProps={{
                            min: 0,
                            step: 0.000001,
                        }}
                        label={t('wallet.parameterLabels.fee')}
                        startAdornment={
                            <InputAdornment position="start">{AmountUnit[TezUnit.tez].char}</InputAdornment>
                        }
                        value={parameters[FIELDS.FEE]}
                    />
                </FormControl>
                <TextField
                    onChange={handleParameterInput}
                    fullWidth
                    name={FIELDS.GAS_LIMIT}
                    id={FIELDS.GAS_LIMIT}
                    required
                    error={String(parameters[FIELDS.GAS_LIMIT]) === ''}
                    label={t('wallet.parameterLabels.gasLimit')}
                    type="number"
                    inputProps={{
                        min: 0,
                    }}
                    margin="normal"
                    variant="outlined"
                    value={parameters[FIELDS.GAS_LIMIT]}
                />
                <TextField
                    onChange={handleParameterInput}
                    fullWidth
                    name={FIELDS.STORAGE_LIMIT}
                    id={FIELDS.STORAGE_LIMIT}
                    required
                    error={String(parameters[FIELDS.STORAGE_LIMIT]) === ''}
                    type="number"
                    inputProps={{
                        min: 0,
                    }}
                    label={t('wallet.parameterLabels.storageLimit')}
                    margin="normal"
                    variant="outlined"
                    value={parameters[FIELDS.STORAGE_LIMIT]}
                />
            </Box>
            <Divider className={classes.divider} />
        </Paper>
    );
};

export default OriginationParameters;
