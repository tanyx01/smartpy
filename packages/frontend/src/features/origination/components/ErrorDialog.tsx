import React from 'react';
import { useDispatch } from 'react-redux';

import Alert from '@mui/material/Alert';

import { useError } from '../selectors';
import actions from '../actions';
import ErrorDialog from '../../common/components/ErrorDialog';

const OriginationErrorDialog = () => {
    const error = useError();
    const dispatch = useDispatch();

    const handleClose = () => dispatch(actions.hideError());

    return (
        <ErrorDialog open={!!error} onClose={handleClose}>
            <Alert severity="error">{error}</Alert>
        </ErrorDialog>
    );
};

export default OriginationErrorDialog;
