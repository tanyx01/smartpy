import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';

import { useContracts, useSettings } from '../selectors';
import actions from '../actions';

// Local Views
import ErrorDialog from '../components/ErrorDialog';
import EditorView from '../views/EditorView';
import Loading from '../../loader/components/LinearProgressWithText';
import CreateContractWithPreview from '../views/CreateContractWithPreview';
// Local Utils
import asyncScriptInjector from '../../../utils/asyncScriptInjector';
// Local Hooks
import SharedContractListener from '../../common/components/listeners/SharedContractListener';
// Local Polyfills
import '../../../polyfills';

import logger from '../../../services/logger';
import scripts from '../constants/scripts';
import { updateOriginationContract } from '../../origination/actions';
import { getBase } from '../../../utils/url';
import HelpMenuItem from '../../common/components/HelpMenuItem';

const EditorContainer = () => {
    // Refs
    const editorRef = React.useRef(null as unknown as editor.IStandaloneCodeEditor);
    const contractNextId = React.useRef(0);
    // States
    const [ready, setReady] = React.useState(false);
    const [percentage, setPercentage] = React.useState(0);
    const [latestScript, setLatestScript] = React.useState('');
    const [htmlOutput, setHtmlOutput] = React.useState({ __html: '' });
    const [targets, setTargets] = React.useState([] as { kind: string; name: string }[]);
    const contracts = useContracts();
    const settings = useSettings();
    const dispatch = useDispatch();

    /**
     * Set volatile contract (used when importing contracts)
     * @param {string} code - Contract code
     * @return {void}
     */
    const setVolatileContract = (code: string) => dispatch(actions.setVolatileContract(code));

    /**
     * @summary Show error callback
     * @param {string} error - Error message
     * @return {void}
     */
    const showError = useCallback((error: string) => dispatch(actions.showError(error)), [dispatch]);

    /**
     * Set contract output (HTML formatted)
     *
     * @param {string} output - The Contract Output
     * @return {void}
     */
    const setOutput = (output: string) => {
        setHtmlOutput({ __html: output });
    };

    /**
     * Append to the contract output (HTML formatted)
     *
     * @param {string} output - The Contract Output
     * @return {void}
     */
    const addOutput = (output: string) => {
        setHtmlOutput(({ __html }) => ({ __html: __html + output }));
    };

    /**
     * Clear Contract Outputs
     *
     * @returns {void}
     */
    const clearOutputs = () => {
        setHtmlOutput({ __html: '' });
        contractNextId.current = 0;
        setTargets([]);
    };

    /**
     * @summary Get contract code.
     * @param {string} name - Name of the stored contract.
     * @return {string | undefined} The contract code or undefined if the contract was not found.
     */
    const getContractCode = useCallback(
        (name: string) => {
            for (const c of contracts) {
                if (c.name === name) {
                    return c.code;
                }
            }
        },
        [contracts],
    );

    /**
     * Provide a distinct contract ID's to SmartML
     *
     * @returns {number} Contract ID
     */
    const nextId = () => contractNextId.current++;

    const setSelectVisibility = (selectId: string, ...rest: string[]) => {
        for (let i = 0; i < rest.length; i += 2) {
            if (rest[i] === (document.getElementById(selectId) as any)?.value) {
                const el = document.getElementById(rest[i + 1]);
                if (el) el.style.display = 'block';
            } else {
                const el = document.getElementById(rest[i + 1]);
                if (el) el.style.display = 'none';
            }
        }
    };

    const setValueVisibility = (value: string, ...rest: string[]) => {
        for (let i = 0; i < rest.length; i += 2) {
            if (rest[i] === value) {
                const el = document.getElementById(rest[i + 1]);
                if (el) el.style.display = 'block';
            } else {
                const el = document.getElementById(rest[i + 1]);
                if (el) el.style.display = 'none';
            }
        }
    };

    const gotoOrigination = React.useCallback(
        (
            contractSizes,
            storageCode,
            contractCode,
            storageCodeJson,
            contractCodeJson,
            storageCodeHtml,
            contractCodeHtml,
            storageCodeJsonHtml,
            contractCodeJsonHtml,
        ) => {
            dispatch(
                updateOriginationContract({
                    codeJson: contractCodeJson,
                    storageJson: storageCodeJson,
                }),
            );
            window.open(`${getBase()}/origination`);
        },
        [dispatch],
    );

    /**
     * Update loading state (in percentage %)
     */
    const onProgressUpdate = (percentage: number, loadedScript: string) => {
        setPercentage(percentage);
        setLatestScript(loadedScript);
        if (percentage === 100) {
            setReady(true);
        }
    };

    const showLine = (line: number, column = 0, endLine?: number, endColumn?: number) => {
        editorRef.current?.revealPositionInCenter({ lineNumber: line, column });
        editorRef.current?.setPosition({ lineNumber: line, column });
        if (endLine && endColumn) {
            editorRef.current?.setSelection({
                startLineNumber: line,
                startColumn: column,
                endLineNumber: endLine,
                endColumn: endColumn,
            });
        }
        editorRef.current?.focus();
    };

    /**
     * Expose all callbacks needed by JS_of_ocaml & Python scripts
     */
    React.useEffect(() => {
        window.setSelectVisibility = setSelectVisibility;
        window.setValueVisibility = setValueVisibility;
        window.showErrorPre = (error: string) => showError('<pre>' + error + '</pre>');
        window.showLine = showLine;
        window.smartpyContext = {
            ...(window.smartpyContext || {}),
            gotoOrigination,
            getContractCode,
            addOutput,
            setOutput,
            clearOutputs,
            nextId,
            showError,
        };
    }, [getContractCode, gotoOrigination, showError]);

    React.useEffect(() => {
        asyncScriptInjector(scripts, onProgressUpdate, (scriptName) =>
            logger.error(`Failed to load script: ${scriptName}`),
        );

        const onResize = () => {
            if (window.screen.width < 768) {
                document.getElementById('viewport-meta')?.setAttribute('content', 'width=768');
            }
        };
        onResize();
        window.addEventListener('resize', onResize);

        return () => {
            window.removeEventListener('resize', onResize);
            document.getElementById('viewport-meta')?.setAttribute('content', 'width=device-width, initial-scale=1');
        };
    }, []);

    return (
        <React.Fragment>
            {ready ? (
                <EditorView
                    {...{
                        settings,
                        clearOutputs,
                        editorRef,
                        htmlOutput,
                        targets,
                        setTargets,
                        showError,
                    }}
                />
            ) : (
                <Loading
                    msg={`Loading ${latestScript ? '(' + latestScript + ')' : ''} ...`}
                    variant="determinate"
                    value={percentage}
                />
            )}
            <ErrorDialog />
            <CreateContractWithPreview />
            <HelpMenuItem />
            <SharedContractListener setVolatileContract={setVolatileContract} />
        </React.Fragment>
    );
};

export default EditorContainer;
