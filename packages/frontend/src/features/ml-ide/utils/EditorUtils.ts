import pako from 'pako';

import { copyToClipboard } from '../../../utils/clipboard';
import toast from '../../../services/toast';

/**
 * Evaluate the code in the editor
 *
 * @param {string} code source code
 * @param {boolean} withTests - When true, the evaluation will run with tests.
 */
export const evalRun = (code: string, withTests = true): [string, string][] => {
    return JSON.parse(window.smartmlCtx.call('runSmartMLScript', code));
};

/**
 * Evaluate a specific test.
 *
 * @param {string} name - Name of the Tests to evaluate.
 */
export const evalScenario = async (name: string) => {
    try {
        return window.smartmlCtx.call('runSmartMLScriptScenarioName', name);
    } catch (error: any) {
        window.showErrorPre(error, String(error));
    }
};

export const getEmbeddedLink = (contract: string) => {
    const encoded = btoa(pako.deflate(contract, { to: 'string' }))
        .replace(/\+/g, '@')
        .replace(/\//g, '_')
        .replace(/=/g, '-');
    return `${window.location.origin}/ml-ide?code=${encoded}`;
};

export const handleTemplateShare = (fileName: string) => {
    copyToClipboard(`${window.location.origin}/ml-ide/?template=${fileName}`);
    toast.info('The template url was copied!');
};
