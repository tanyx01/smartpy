import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { alpha, Theme, useTheme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ShareOutlinedIcon from '@mui/icons-material/ShareOutlined';
import Tooltip from '@mui/material/Tooltip';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarIcon from '@mui/icons-material/Star';

// Local Components
import Loader from '../../loader/components/CircularProgressWithText';
// State Management
import actions from '../actions';
import { useFavoriteTemplates } from '../selectors';
import useTranslation from '../../i18n/hooks/useTranslation';
// Local Constants
import { TemplateProps, getAllTemplates } from '../constants/templates';
// Utils
import { handleTemplateShare } from '../utils/EditorUtils';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        secondaryText: {
            color: theme.palette.mode === 'dark' ? 'rgba(255, 255, 255, 0.54)' : 'rgba(0, 0, 0, 0.54)',
        },
        list: {
            width: '100%',
        },
        item: {
            display: 'flex',
            width: '100%',
        },
        favoriteButton: {
            width: 48,
            height: 48,
            alignSelf: 'center',
        },
        itemContainer: {
            width: '100%',
        },
        title: {
            flexGrow: 1,
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block',
            },
        },
        search: {
            position: 'relative',
            display: 'flex',
            alignItems: 'center',
            paddingLeft: 5,
            borderRadius: theme.shape.borderRadius,
            backgroundColor: alpha(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: alpha(theme.palette.common.white, 0.25),
            },
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(1),
                width: 'auto',
            },
        },
        searchIcon: {
            alignSelf: 'center',
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)})`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: '12ch',
                '&:focus': {
                    width: '20ch',
                },
            },
        },
        emptyListText: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: 20,
        },
    }),
);

const allTemplates = getAllTemplates();

const FavoriteContracts: React.FC = () => {
    const classes = useStyles();
    const theme = useTheme();
    const [search, setSearch] = React.useState('');
    const [favorites, setFavorites] = React.useState([] as TemplateProps[]);
    const [loading, setLoading] = React.useState(true);
    const favoriteTemplates = useFavoriteTemplates();
    const dispatch = useDispatch();
    const t = useTranslation();

    const handleListItemClick = async (fileName: string) => {
        dispatch(actions.loadTemplate(fileName));
    };

    const handleFavoriteToggle = (event: React.MouseEvent<HTMLButtonElement>, template: string) => {
        dispatch(actions.toggleFavoriteTemplate(template));
    };

    const updateFilteredTemplates = React.useCallback(
        () =>
            setFavorites(() => {
                setLoading(true);
                const newState = Object.keys(allTemplates).reduce<TemplateProps[]>((state, templateName) => {
                    const template = allTemplates[templateName];
                    if (
                        favoriteTemplates.includes(template.fileName) &&
                        (template.description?.match(search) || template.name.match(search))
                    ) {
                        state.push(template);
                    }
                    return state;
                }, []);

                setLoading(false);
                return newState;
            }),
        [favoriteTemplates, search],
    );

    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value);
    };

    React.useEffect(() => {
        updateFilteredTemplates();
    }, [updateFilteredTemplates]);

    return (
        <React.Fragment>
            <AppBar position="static" color={theme.palette.mode === 'dark' ? 'default' : 'primary'}>
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>
                        {t('ide.templatesMenu.favoriteTemplates')}
                    </Typography>
                    <div className={classes.search}>
                        <SearchIcon className={classes.searchIcon} />
                        <InputBase
                            defaultValue={search}
                            onChange={handleSearch}
                            placeholder={`${t('common.search')}…`}
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                </Toolbar>
            </AppBar>

            <Loader loading={loading} margin={50} />
            {favorites.length === 0 && !loading ? (
                <div className={classes.emptyListText}>
                    <Typography variant="overline">{t('ide.templatesMenu.emptyFavoritesList')}</Typography>
                </div>
            ) : (
                <List className={classes.list}>
                    {favorites.map((favorite) => (
                        <div key={favorite.name} className={classes.item}>
                            <IconButton
                                key={favorite.fileName}
                                className={classes.favoriteButton}
                                onClick={(e) => handleFavoriteToggle(e, favorite.fileName)}
                                size="large"
                            >
                                {favoriteTemplates.includes(favorite.fileName) ? <StarIcon /> : <StarBorderIcon />}
                            </IconButton>
                            <ListItem
                                button
                                onClick={() => handleListItemClick(favorite.fileName)}
                                classes={{ container: classes.itemContainer }}
                            >
                                <div>
                                    <ListItemText primary={favorite.name} />
                                    {favorite.description ? (
                                        <div
                                            className={classes.secondaryText}
                                            dangerouslySetInnerHTML={{ __html: favorite.description }}
                                        />
                                    ) : null}
                                </div>
                                <ListItemSecondaryAction onClick={() => handleTemplateShare(favorite.fileName)}>
                                    <Tooltip title="Share Template" aria-label="share-template" placement="left">
                                        <IconButton edge="end" size="large">
                                            <ShareOutlinedIcon />
                                        </IconButton>
                                    </Tooltip>
                                </ListItemSecondaryAction>
                            </ListItem>
                        </div>
                    ))}
                </List>
            )}
        </React.Fragment>
    );
};

export default FavoriteContracts;
