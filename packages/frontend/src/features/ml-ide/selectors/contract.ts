import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';
import { IDEContract } from 'SmartPyModels';

/**
 * @summary Get the latest contract to be modified.
 * @param {RootState} state - Redux state.
 * @return {IDEContract} the latest contract to be modified.
 */
export const getLatestModifiedContract = (state: RootState) =>
    state.mlIDE.contracts.reduce<IDEContract>((latest, cur) => {
        if (!latest.id) {
            latest = cur;
        } else if (new Date(cur.updatedAt as string) > new Date(latest.updatedAt as string)) {
            latest = cur;
        }
        return latest;
    }, {});

const getContract = (state: RootState, contractId?: string) => {
    let contractToReturn = state.mlIDE.contracts.find(({ id }) => id === contractId);

    if (!contractToReturn) {
        contractToReturn = getLatestModifiedContract(state);
    }

    return contractToReturn;
};

export const useContracts = () =>
    useSelector<RootState, IDEContract[]>((state) =>
        state.mlIDE.contracts.sort(
            ({ updatedAt: t1 }, { updatedAt: t2 }) =>
                new Date(t2 as string).getTime() - new Date(t1 as string).getTime(),
        ),
    );

export const useContract = (contractId: string) =>
    useSelector<RootState, IDEContract | undefined>((state) => getContract(state, contractId));

export const useSelectedContract = () =>
    useSelector<RootState, IDEContract | undefined>((state) => getContract(state, state.mlIDE.currentContract));

export const useVolatileContract = () => useSelector<RootState, string>((state) => state.mlIDE.volatileContract);
