import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Paper from '@mui/material/Paper';
import Chip from '@mui/material/Chip';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import AppBar from '@mui/material/AppBar';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';

// Local Components
import PopperWithArrow from '../../common/components/PopperWithArrow';
import NewcomerDialog from '../components/NewcomerDialog';
import FavoriteContracts from '../components/FavoriteContracts';
import TemplateContracts from '../components/TemplateContracts';
// State Management
import useTranslation from '../../i18n/hooks/useTranslation';
import { useNewcomerMode, useNewcomerGuideStep } from '../selectors';
import actions from '../actions';
// Local Constants
import { contractTemplates, syntaxTemplates } from '../constants/templates';
// Local Hooks
import useCommonStyles from '../../../hooks/useCommonStyles';
import { IDENewcomerGuideSteps } from '../../common/enums/ide';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            height: '70vh',
            overflowY: 'auto',
        },
    }),
);

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
    className: string;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && children}
        </div>
    );
}

/**
 * @summary Accessibility props (a11y)
 */
const a11yProps = (index: number) => {
    return {
        id: `tab-${index}`,
        'aria-controls': `tabpanel-${index}`,
    };
};

interface OwnProps {
    open: boolean;
    onClose: () => void;
}

const TemplatesDialog: React.FC<OwnProps> = (props) => {
    const classes = {
        ...useCommonStyles(),
        ...useStyles(),
    };
    const newcomersMode = useNewcomerMode();
    const [value, setValue] = React.useState(0);
    const newcomerGuideStep = useNewcomerGuideStep();
    const dispatch = useDispatch();
    const anchorRef = React.useRef<HTMLDivElement>(null);
    const t = useTranslation();

    const { onClose, open } = props;

    const handleClose = () => {
        onClose();
    };

    const handleChange = (event: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setValue(newValue);
    };

    const handleNewcomerInteraction = () => {
        if (newcomerGuideStep === IDENewcomerGuideSteps.TEMPLATES) {
            dispatch(actions.updateNewcomerGuideStep(IDENewcomerGuideSteps.NONE));
        }
    };

    return (
        <Dialog maxWidth="md" fullWidth onClose={handleClose} open={open}>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="Templates Menu"
                >
                    {newcomersMode ? <Tab label={t('ide.newcomer.newcomer')} {...a11yProps(0)} /> : null}
                    <Tab label={t('ide.templatesMenu.favorites')} {...a11yProps(newcomersMode ? 1 : 0)} />
                    <Tab
                        label={<div ref={anchorRef}>{t('ide.templatesMenu.regularTemplates')}</div>}
                        {...a11yProps(newcomersMode ? 2 : 1)}
                    />
                    <Tab label={t('ide.templatesMenu.referenceTemplates')} {...a11yProps(newcomersMode ? 3 : 2)} />
                </Tabs>
            </AppBar>

            {newcomersMode ? (
                <TabPanel value={value} index={0} className={classes.root}>
                    <DialogTitle id="newcomer-dialog-title">{t('ide.newcomer.dialog.title')}</DialogTitle>
                    <Divider />
                    <DialogContent>
                        <NewcomerDialog handleClose={handleClose} />
                    </DialogContent>
                </TabPanel>
            ) : null}
            <TabPanel value={value} index={newcomersMode ? 1 : 0} className={classes.root}>
                <DialogContent>
                    <FavoriteContracts />
                </DialogContent>
            </TabPanel>
            <TabPanel value={value} index={newcomersMode ? 2 : 1} className={classes.root}>
                <DialogContent>
                    <TemplateContracts title={t('ide.templatesMenu.regularTemplates')} templates={contractTemplates} />
                </DialogContent>
            </TabPanel>
            <TabPanel value={value} index={newcomersMode ? 3 : 2} className={classes.root}>
                <DialogContent>
                    <TemplateContracts title={t('ide.templatesMenu.referenceTemplates')} templates={syntaxTemplates} />
                </DialogContent>
            </TabPanel>
            <Divider />
            <DialogActions>
                <Button autoFocus color="primary" onClick={handleClose}>
                    {t('common.close')}
                </Button>
            </DialogActions>
            {/* Newcomer Guide Step (Templates - Used in newcomer mode) */}
            <PopperWithArrow
                open={newcomerGuideStep === IDENewcomerGuideSteps.TEMPLATES}
                withGlowAnimation
                anchorEl={anchorRef.current}
            >
                <Paper>
                    <Chip
                        className={classes.spacingTwo}
                        variant="outlined"
                        label={t('ide.newcomer.templatesStep.popperChip')}
                    />
                    <DialogActions>
                        <Button fullWidth variant="contained" onClick={handleNewcomerInteraction}>
                            {t('common.understood')}
                        </Button>
                    </DialogActions>
                </Paper>
            </PopperWithArrow>
        </Dialog>
    );
};

export default TemplatesDialog;
