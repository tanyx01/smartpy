import React from 'react';
import Button, { ButtonProps } from '@mui/material/Button';
import { Link as RouterLink } from 'react-router-dom';

import { LocationDescriptor } from 'history';

interface OwnProps extends ButtonProps {
    to: LocationDescriptor;
}

const RouterButton: React.FC<OwnProps> = ({ to, ...props }) => (
    <Button
        {...props}
        component={React.forwardRef((linkProps, ref) => (
            <RouterLink {...linkProps} to={to} />
        ))}
    />
);

export default RouterButton;
