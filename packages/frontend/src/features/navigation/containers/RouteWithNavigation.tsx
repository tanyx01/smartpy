import React from 'react';
import { Route, RouteProps } from 'react-router-dom';

import Navigation from '../components/Navigation';

const RouteWithNavigation: React.FC<RouteProps> = ({ children, ...props }) => (
    <Route {...props}>
        <Navigation />
        {children}
    </Route>
);

export default RouteWithNavigation;
