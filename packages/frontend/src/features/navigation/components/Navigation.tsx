import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter, RouteComponentProps } from 'react-router-dom';
// Material UI
import { styled } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Button from '@mui/material/Button';
import CodeIcon from '@mui/icons-material/Code';
import Search from '@mui/icons-material/Search';
import ContactSupport from '@mui/icons-material/ContactSupport';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import ButtonGroup from '@mui/material/ButtonGroup';

// Local Elements
import DarkLightSwitch from '../../theme/components/DarkLightSwitch';
import RouterButton from '../elements/RouterButton';
import Logo from '../../common/elements/Logo';

// Local Utils
import { version } from '../../../../package.json';
import { getBase } from '../../../utils/url';
import TypescriptIcon from '../../common/elements/icons/TypeScript';
import PythonIcon from '../../common/elements/icons/Python';
import OcamlIcon from '../../common/elements/icons/Ocaml';
import TezosWalletIcon from '../../common/elements/icons/TezosWallet';

const PREFIX = 'Navigation';

const classes = {
    appBar: `${PREFIX}-appBar`,
    logoSection: `${PREFIX}-logoSection`,
    middleSection: `${PREFIX}-middleSection`,
    gridMenu: `${PREFIX}-gridMenu`,
    paper: `${PREFIX}-paper`,
    button: `${PREFIX}-button`,
    buttonGroupItem: `${PREFIX}-buttonGroupItem`,
    groupedOutlinedHorizontal: `${PREFIX}-groupedOutlinedHorizontal`,
    groupedOutlinedDisabled: `${PREFIX}-groupedOutlinedDisabled`,
    lowOpacify: `${PREFIX}-lowOpacify`,
    activeButton: `${PREFIX}-activeButton`,
    logoParagraph: `${PREFIX}-logoParagraph`,
    logoLink: `${PREFIX}-logoLink`,
    version: `${PREFIX}-version`,
};

const Root = styled('div')(({ theme }) => ({
    [`& .${classes.appBar}`]: {
        position: 'relative',
        height: 80,
        backgroundColor: 'transparent',
        boxShadow: 'none',
    },

    [`& .${classes.logoSection}`]: {
        flexGrow: 1,
        display: 'flex',
        padding: 20,
    },

    [`& .${classes.middleSection}`]: {
        flexGrow: 1,
        display: 'flex',
        padding: 20,
    },

    [`& .${classes.gridMenu}`]: {
        width: 'auto',
    },

    [`& .${classes.paper}`]: {
        display: 'flex',
        backgroundColor: theme.palette.background.default,
        borderBottomWidth: 2,
        borderBottomStyle: 'solid',
        borderBottomColor: theme.palette.primary.light,
    },

    [`& .${classes.button}`]: {
        minWidth: 140,
        backgroundColor: theme.palette.background.paper,
        borderWidth: 2,
        '&:hover': {
            borderWidth: 2,
        },
    },

    [`& .${classes.buttonGroupItem}`]: {
        backgroundColor: theme.palette.background.paper,
        '&:not(:last-child)': {
            marginLeft: 0,
            border: '1px solid rgba(0, 123, 255, 0.5)',
        },
    },

    [`& .${classes.groupedOutlinedHorizontal}`]: {
        borderColor: 'inherit',
    },

    [`& .${classes.groupedOutlinedDisabled}`]: {
        borderWidth: 2,
        '&:disabled': {
            borderWidth: 2,
        },
    },

    [`& .${classes.lowOpacify}`]: {
        opacity: 0.6,
    },

    [`& .${classes.activeButton}`]: {
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.mode === 'dark' ? '#66aaff' : '#0160cc',
    },

    [`& .${classes.logoParagraph}`]: {
        margin: '-10px 0 0 40px',
        fontVariant: 'small-caps',
        fontFamily: 'Verdana, sans-serif',
        color: theme.palette.mode === 'dark' ? '#66aacc' : '#01608c',
    },

    [`& .${classes.logoLink}`]: {
        textDecoration: 'none',
    },

    [`& .${classes.version}`]: {
        position: 'absolute',
        left: 200,
        top: 28,
        fontSize: '8pt',
        color: theme.palette.mode === 'dark' ? '#FFF' : '#000',
        textDecoration: 'none',
    },
}));

const WalletIcon = styled(TezosWalletIcon)(({ theme }) => ({
    fill: theme.palette.primary.main,
    width: 24,
    height: 24,
}));

const menuButtons = [
    {
        component: RouterButton,
        props: {
            fullWidth: true,
            variant: 'outlined',
            size: 'large',
            startIcon: <CodeIcon />,
            to: '/ide',
            match: /.*(ide)|(michelson)|(ts-ide)|(ml-ide).*/,
        } as any,
        text: 'Editor',
    },
    {
        component: Button,
        props: {
            fullWidth: true,
            href: `${getBase()}/explorer`,
            variant: 'outlined',
            size: 'large',
            startIcon: <Search />,
            match: /.*(explorer).*/,
        } as any,
        text: 'Explorer',
    },
    {
        component: RouterButton,
        props: {
            fullWidth: true,
            variant: 'outlined',
            size: 'large',
            startIcon: <WalletIcon />,
            to: '/wallet',
            match: /.*(wallet).*/,
        } as any,
        text: 'Wallet',
    },
    {
        component: RouterButton,
        props: {
            fullWidth: true,
            variant: 'outlined',
            size: 'large',
            startIcon: <ContactSupport />,
            to: '/help',
            match: /.*(\/help).*/,
        } as any,
        text: 'Help',
    },
];

const Navigation: React.FC<RouteComponentProps> = (props) => {
    const showWideMenu = useMediaQuery('(min-width:1300px)');
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const {
        location: { pathname },
    } = props;

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const wideMenuItems = (
        <>
            {menuButtons.map((item, index) => {
                item.props.className = classes.button;
                if (item.props.match && pathname.match(item.props.match)) {
                    item.props.className = `${item.props.className} ${classes.activeButton}`;
                }
                return (
                    <Grid item key={index}>
                        <item.component {...item.props}>{item.text}</item.component>
                    </Grid>
                );
            })}
        </>
    );

    const mobileMenuItems = menuButtons.map((item, index) => {
        item.props.className = classes.button;
        if (item.props.match && pathname.match(item.props.match)) {
            item.props.className = `${item.props.className} ${classes.activeButton}`;
        }
        return (
            <MenuItem key={index}>
                <item.component {...item.props}>{item.text}</item.component>
            </MenuItem>
        );
    });

    const middleSection = React.useMemo(() => {
        const isPythonIDE = pathname.includes('/ide');
        const isTypescriptIDE = pathname.includes('/ts-ide');
        const isOcamlIDE = pathname.includes('/ml-ide');
        const isMichelsonIDE = pathname.includes('/michelson');
        if (isPythonIDE || isTypescriptIDE || isMichelsonIDE || isOcamlIDE) {
            return (
                <div className={classes.middleSection} aria-label="outlined button group">
                    <DarkLightSwitch />
                    <ButtonGroup variant="outlined">
                        <RouterButton
                            fullWidth
                            variant="outlined"
                            size="large"
                            to="/ide"
                            className={isPythonIDE ? '' : classes.lowOpacify}
                            classes={{ root: classes.buttonGroupItem }}
                        >
                            <PythonIcon />
                        </RouterButton>
                        <RouterButton
                            fullWidth
                            variant="outlined"
                            size="large"
                            to="/ts-ide"
                            className={isTypescriptIDE ? '' : classes.lowOpacify}
                            classes={{ root: classes.buttonGroupItem }}
                        >
                            <TypescriptIcon />
                        </RouterButton>
                        <RouterButton
                            fullWidth
                            variant="outlined"
                            size="large"
                            to="/ml-ide"
                            className={isOcamlIDE ? '' : classes.lowOpacify}
                            classes={{ root: classes.buttonGroupItem }}
                        >
                            <OcamlIcon />
                        </RouterButton>
                        <RouterButton
                            fullWidth
                            variant="outlined"
                            size="large"
                            to="/michelson"
                            className={isMichelsonIDE ? '' : classes.lowOpacify}
                            classes={{ root: classes.buttonGroupItem }}
                        >
                            <img alt="Michelson Editor" src={`${getBase()}/static/img/michelson_logo.png`} width={24} />
                        </RouterButton>
                    </ButtonGroup>
                </div>
            );
        }

        return <DarkLightSwitch />;
    }, [pathname]);

    return (
        <Root>
            <AppBar className={classes.appBar}>
                <Toolbar className={classes.paper}>
                    <div className={classes.logoSection}>
                        <Link to="/" className={classes.logoLink}>
                            <Logo />
                            <p className={classes.logoParagraph}>by Smart Chain Arena</p>
                        </Link>

                        <Typography
                            variant="caption"
                            className={classes.version}
                            component="a"
                            href={`${getBase()}/docs/releases`}
                        >
                            {version}
                        </Typography>
                    </div>
                    {middleSection}
                    {showWideMenu ? (
                        <Grid spacing={1} container className={classes.gridMenu}>
                            {wideMenuItems}
                        </Grid>
                    ) : (
                        <React.Fragment>
                            <IconButton
                                aria-label="menu"
                                aria-controls="menu"
                                aria-haspopup="true"
                                onClick={handleClick}
                            >
                                <MenuIcon fontSize="large" />
                            </IconButton>
                            <Menu
                                id="mobile-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                {mobileMenuItems}
                            </Menu>
                        </React.Fragment>
                    )}
                </Toolbar>
            </AppBar>
        </Root>
    );
};

export default withRouter(Navigation);
