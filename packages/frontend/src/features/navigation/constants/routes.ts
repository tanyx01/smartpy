import React from 'react';
import { Route, RouteProps } from 'react-router-dom';

import RouteWithNavigation from '../containers/RouteWithNavigation';

interface SmartPyRoute {
    title: string;
    Route: typeof Route | typeof RouteWithNavigation;
    routeProps: RouteProps;
    Component: React.FC<any>;
}

export default [
    // Root
    {
        title: 'Home - SmartPy',
        Route: Route,
        routeProps: {
            exact: true,
            path: '/',
        },
        Component: React.lazy(() => import('src/features/home/views/Home')),
    },
    // IDE
    {
        title: 'Editor - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/ide/:cid?' },
        Component: React.lazy(() => import('src/features/py-ide/containers/Editor')),
    },
    // Typescript IDE
    {
        title: 'Editor - SmartTS',
        Route: RouteWithNavigation,
        routeProps: { path: '/ts-ide/:cid?' },
        Component: React.lazy(() => import('src/features/ts-ide/containers/Editor')),
    },
    // Ocaml IDE
    {
        title: 'Editor - SmartML',
        Route: RouteWithNavigation,
        routeProps: { path: '/ml-ide/:cid?' },
        Component: React.lazy(() => import('src/features/ml-ide/containers/Editor')),
    },
    // Origination
    {
        title: 'Origination - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/origination' },
        Component: React.lazy(() => import('src/features/origination/containers/Origination')),
    },
    // Michelson Helper
    {
        title: 'Michelson - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/michelson' },
        Component: React.lazy(() => import('src/features/mich-ide/containers/MichelsonEditor')),
    },
    // Explorer
    {
        title: 'Explorer - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/explorer' },
        Component: React.lazy(() => import('src/features/explorer/containers/Explorer')),
    },
    // Help
    {
        title: 'Help - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/help' },
        Component: React.lazy(() => import('src/features/help/views/Help')),
    },
    // Wallet
    {
        title: 'Wallet - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/wallet', exact: true },
        Component: React.lazy(() => import('src/features/wallet/views/Wallet')),
    },
    // Wallet - Smart Contracts
    {
        title: 'Smart Contracts - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/wallet/smart-contracts', exact: true },
        Component: React.lazy(() => import('src/features/wallet/views/SmartContracts')),
    },
    // Wallet - Smart Contract
    {
        title: 'Smart Contract - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/wallet/smart-contracts/:address' },
        Component: React.lazy(() => import('src/features/wallet/views/SmartContract')),
    },
    // Wallet - Accounts
    {
        title: 'Accounts - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/wallet/accounts', exact: true },
        Component: React.lazy(() => import('src/features/wallet/containers/Accounts')),
    },
    // Wallet - Account
    {
        title: 'Account - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/wallet/accounts/:accountId' },
        Component: React.lazy(() => import('src/features/wallet/containers/Account')),
    },
    // Nodes Monitor
    {
        title: 'Nodes - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/nodes' },
        Component: React.lazy(() => import('../../nodes/views/Nodes')),
    },
    // Not Found Page
    {
        title: 'Nodes - Not Found',
        Route: RouteWithNavigation,
        routeProps: { path: '*' },
        Component: React.lazy(() => import('../../not-found/views/NotFound')),
    },
] as SmartPyRoute[];
