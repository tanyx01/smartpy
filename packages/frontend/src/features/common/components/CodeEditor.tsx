import React from 'react';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';
import type { OnMount } from '@monaco-editor/react';

// Material UI
import { Theme, useTheme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Editor from '../../editor/components/Editor';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            height: 300,
            boxShadow: 'inset 0 0 10px #000000',
        },
    }),
);

type OwnProps = {
    mode: string;
    value: string;
    onChange: (value: string) => void;
};

const CodeEditor: React.FC<OwnProps> = ({ onChange, value, mode }) => {
    const classes = useStyles();
    const theme = useTheme();
    const editorTheme = theme.palette.mode === 'dark' ? 'vs-dark' : 'vs-light';
    const textAreaRef = React.useRef(null as unknown as editor.IStandaloneCodeEditor);

    const onTextInput = () => {
        onChange(textAreaRef.current?.getValue());
    };

    const handleEditorDidMount: OnMount = (editor) => {
        textAreaRef.current = editor;
    };

    return (
        <Editor
            onMount={handleEditorDidMount}
            language={mode}
            theme={editorTheme}
            value={value}
            options={{
                automaticLayout: true,
                selectOnLineNumbers: true,
                minimap: {
                    enabled: false,
                },
            }}
            className={classes.paper}
            onChange={onTextInput}
        />
    );
};

export default CodeEditor;
