import React from 'react';

import {
    TableBody,
    TableCell,
    TableHead,
    Typography,
    TablePagination,
    TableSortLabel,
    TableCellProps,
    Chip,
} from '@mui/material';

import Table from 'src/features/common/components/Table';
import TableRow from 'src/features/common/components/TableRow';
import { StringMatchers, TimestampMatchers } from 'src/utils/matchers';
import { Operation } from 'src/utils/tzkt';
import { convertUnitWithSymbol, AmountUnit } from '../../../utils/units';
import { TezosAccount } from 'smartpy-wallet-storage';

enum Order {
    Asc = 'asc',
    Desc = 'desc',
}
enum Field {
    Type = 'type',
    Status = 'status',
    Timestamp = 'timestamp',
}

enum StatusColor {
    failed = 'error',
    applied = 'success',
}

const sortByField = (operations: Operation[], field: Field, order: Order) => {
    const _operations = [...operations];

    return _operations.sort((v1, v2) => {
        if (field === Field.Timestamp) {
            return (order === Order.Asc ? 1 : -1) * TimestampMatchers.compare(v1[field], v2[field]);
        }
        return (order === Order.Asc ? 1 : -1) * StringMatchers.compare(v1[field], v2[field]);
    });
};

const SortedTableCell = ({
    order,
    isSorted,
    onSort,
    fieldName,
    ...props
}: {
    order: Order;
    isSorted: boolean;
    onSort: (field: Field) => void;
    fieldName: Field;
} & TableCellProps) => (
    <TableCell {...props}>
        <TableSortLabel active={isSorted} direction={isSorted ? order : Order.Asc} onClick={() => onSort(fieldName)}>
            <Typography variant="overline">{fieldName}</Typography>
        </TableSortLabel>
    </TableCell>
);

interface OwnProps {
    rowsPerPage?: number;
    operations: Operation[];
    account?: TezosAccount;
}

const Operations: React.FC<OwnProps> = ({ operations, rowsPerPage: pageRows = 10, account }) => {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(pageRows);
    const [sortField, setSortField] = React.useState<Field>(Field.Timestamp);
    const [order, setOrder] = React.useState<Order>(Order.Desc);

    const onSort = React.useCallback(
        (field: Field) => {
            if (sortField === field) {
                setOrder(order === Order.Asc ? Order.Desc : Order.Asc);
            } else {
                setSortField(field);
                setOrder(Order.Desc);
            }
        },
        [order, sortField],
    );

    const filteredOperations = React.useMemo(() => {
        let _operations = operations.slice(page * rowsPerPage, (page + 1) * rowsPerPage);
        _operations = sortByField(_operations, sortField, order);

        return _operations;
    }, [operations, page, rowsPerPage, sortField, order]);

    return (
        <Table
            header={
                <TableHead>
                    <TableRow>
                        <SortedTableCell
                            order={order}
                            fieldName={Field.Type}
                            onSort={onSort}
                            isSorted={sortField === Field.Type}
                        />
                        <SortedTableCell
                            align="center"
                            order={order}
                            fieldName={Field.Status}
                            onSort={onSort}
                            isSorted={sortField === Field.Status}
                        />
                        <TableCell align="center">
                            <Typography variant="overline">Amount</Typography>
                        </TableCell>
                        <TableCell align="center">
                            <Typography variant="overline">Address</Typography>
                        </TableCell>
                        <SortedTableCell
                            align="right"
                            order={order}
                            fieldName={Field.Timestamp}
                            onSort={onSort}
                            isSorted={sortField === Field.Timestamp}
                        />
                    </TableRow>
                </TableHead>
            }
            body={
                <TableBody>
                    {filteredOperations.map((op) => {
                        const isReceiver = account && op.target?.address && op.target?.address === account.address;
                        const isSender = account && op.sender?.address && op.sender?.address === account.address;
                        const from = (
                            <>
                                <span>from</span>
                                <Chip label={op.sender?.address} sx={{ marginLeft: 1 }} />
                            </>
                        );
                        const to = (
                            <>
                                <span>to</span>
                                <Chip label={op.target?.address} sx={{ marginLeft: 1 }} />
                            </>
                        );
                        const toSelf = (
                            <>
                                <span>to</span>
                                <Chip label="self" sx={{ marginLeft: 1 }} />
                            </>
                        );
                        return (
                            <TableRow key={op.id}>
                                <TableCell>
                                    <Typography sx={{ fontWeight: 'bold' }}>{op.type}</Typography>
                                </TableCell>
                                <TableCell>
                                    {op.status ? (
                                        <Chip
                                            label={op.status}
                                            color={(StatusColor as any)[op.status]}
                                            sx={{ opacity: 0.8 }}
                                            size="small"
                                        />
                                    ) : null}
                                </TableCell>
                                <TableCell align="center">
                                    {op.amount ? (
                                        <Chip
                                            label={`${isReceiver ? '+' : isSender ? '-' : ''}${convertUnitWithSymbol(
                                                op.amount || 0,
                                                AmountUnit.uTez,
                                                AmountUnit.tez,
                                            )}`}
                                            color={isReceiver ? 'success' : isSender ? 'error' : undefined}
                                        />
                                    ) : null}
                                </TableCell>
                                <TableCell align="center">
                                    {op.type === 'origination' ? (
                                        <Chip label={op.originatedContract?.address} sx={{ marginLeft: 1 }} />
                                    ) : isSender && isReceiver ? (
                                        toSelf
                                    ) : (
                                        <>
                                            {isSender && (op.target?.address ? to : from)}
                                            {isReceiver && (op.sender?.address ? from : to)}
                                        </>
                                    )}
                                </TableCell>
                                <TableCell align="right"> {new Date(op.timestamp).toLocaleString()}</TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            }
            pagination={
                <TablePagination
                    rowsPerPageOptions={[pageRows, pageRows + 10]}
                    component="div"
                    count={operations.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={(_, page) => setPage(page)}
                    onRowsPerPageChange={(e) => setRowsPerPage(Number(e.target.value))}
                />
            }
        />
    );
};

export default Operations;
