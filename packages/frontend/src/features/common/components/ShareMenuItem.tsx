import React from 'react';

import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import LinkIcon from '@mui/icons-material/Link';

// Local Services
import toast from '../../../services/toast';
// Local Elements
import ShareButton from '../elements/ShareButton';
import IpfsIcon from '../../common/elements/icons/Ipfs';
import IpfsLinkShare from './IpfsLinkShare';
import { compressString } from '../../../utils/encoder';
// Local Utils
import { copyToClipboard } from '../../../utils/clipboard';
import useTranslation from '../../i18n/hooks/useTranslation';

const useStyles = makeStyles(() =>
    createStyles({
        listItemIcon: {
            minWidth: 30,
        },
    }),
);

interface OwnProps {
    baseUrl: string;
    code?: string;
    onlyIcon: boolean;
}

const ShareMenuItem: React.FC<OwnProps> = (props) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [ipfsShareOpen, setIpfsShareOpen] = React.useState(false);
    const anchorRef = React.useRef<HTMLButtonElement>(null);
    const { onlyIcon, code, baseUrl } = props;
    const t = useTranslation();

    const generateEmbeddedLink = () => {
        if (code) {
            copyToClipboard(`${baseUrl}?code=${compressString(code)}`);
            toast.info(`Embedded code link was copied!`);
        }
    };

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
            return;
        }
        setOpen(false);
    };

    const handleIpfsShareToggle = () => {
        setIpfsShareOpen((prev) => !prev);
    };

    return (
        <React.Fragment>
            <ShareButton
                ref={anchorRef}
                aria-controls="share-menu"
                aria-haspopup="true"
                onClick={handleToggle}
                onlyIcon={onlyIcon}
            />
            <Menu
                anchorEl={anchorRef.current}
                keepMounted
                open={Boolean(open)}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <MenuItem disabled={!code} onClick={generateEmbeddedLink}>
                    <ListItemIcon className={classes.listItemIcon}>
                        <LinkIcon />
                    </ListItemIcon>
                    <ListItemText primary="Share embedded code in link" />
                </MenuItem>
                <MenuItem disabled={!code} onClick={handleIpfsShareToggle}>
                    <ListItemIcon className={classes.listItemIcon}>
                        <IpfsIcon />
                    </ListItemIcon>
                    <ListItemText primary="Share with IPFS" />
                </MenuItem>
            </Menu>
            <IpfsLinkShare
                content={code}
                baseUrl={baseUrl}
                open={ipfsShareOpen}
                handleClose={handleIpfsShareToggle}
                title={t('common.shareContract')}
            />
        </React.Fragment>
    );
};

export default ShareMenuItem;
