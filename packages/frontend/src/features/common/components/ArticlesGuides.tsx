import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Divider from '@mui/material/Divider';
import Link from '@mui/material/Link';
import YouTubeIcon from '@mui/icons-material/YouTube';
import SchoolIcon from '@mui/icons-material/School';
import VideogameAssetIcon from '@mui/icons-material/VideogameAsset';

// Local Components
import MediumIcon from '../elements/icons/Medium';
// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';

type Materials = {
    section: string | JSX.Element;
    items: {
        title: string;
        date: string;
        link: string;
        icon: JSX.Element;
    }[];
}[];

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            margin: 20,
        },
        card: {
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,

            boxShadow: `5px 5px 0 0 ${theme.palette.primary.main}`,

            padding: 20,
        },
        nestedCard: {
            padding: 20,
        },
        title: {
            color: theme.palette.primary.main,
            fontWeight: 'bold',
            marginBottom: 20,
        },
        description: {
            fontWeight: 'bold',
            marginBottom: 20,
        },
        innerDescription: {
            fontWeight: 'bold',
            margin: 10,
        },
        list: {
            width: '100%',
            backgroundColor: theme.palette.background.default,
            padding: 0,
        },
        youtubeIcon: {
            color: 'red',
        },
    }),
);

const ArticlesGuides = () => {
    const classes = useStyles();
    const t = useTranslation();

    const SmartPyMaterials = React.useMemo<Materials>(
        () => [
            {
                section: t('common.mediumPosts'),
                items: [
                    {
                        title: 'Metadata in SmartPy',
                        date: 'Nov 28, 2020 · 3 min read',
                        link: 'https://smartpy-io.medium.com/metadata-in-smartpy-3eb09fcd1de7',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'SmartPy Focus: Importing SmartPy and Python code',
                        date: 'Oct 5, 2020 · 3 min read',
                        link: 'https://smartpy-io.medium.com/smartpy-focus-importing-smartpy-and-python-code-7c302eb9487',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'New Release for SmartPy and SmartPy.io: Open Source Version and Syntax, Testing, and Michelson Compiler Improvements',
                        date: 'Jan 13, 2020 · 3 min read',
                        link: 'https://smartpy-io.medium.com/new-release-for-smartpy-and-smartpy-io-89d11dc5146a',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'Introducting SmartPy and SmartPy.io, an Intiutive and Effective Language and Development Platform for Tezos Smart Contracts.',
                        date: 'Mar 5 2019 . 14 min read',
                        link: 'https://smartpy-io.medium.com/introducing-smartpy-and-smartpy-io-d4013bee7d4e',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'A First Encounter with SmartPy.',
                        date: 'Jun 27 2019 · 9 min read',
                        link: 'https://smartpy-io.medium.com/a-first-encounter-with-smartpy-70e28bfeed79',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'Basic Computations in SmartPy, Python and Michelson.',
                        date: 'Jul 25 2019 · 4 min read',
                        link: 'https://smartpy-io.medium.com/basic-computations-in-smartpy-python-and-michelson-bb69dccddaf2',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'Introducing smartpy-cli, a simple CLI to build Tezos smart contract in Python.',
                        date: 'Sep 13 2019 · 3 min read',
                        link: 'https://smartpy-io.medium.com/introducing-smartpybasic-a-simple-cli-to-build-tezos-smart-contract-in-python-f5bd8772b74a',
                        icon: <MediumIcon fontSize="large" />,
                    },
                ],
            },
            {
                section: t('common.videos'),
                items: [
                    {
                        title: 'SmartPy: The inner workings',
                        date: 'Fev 9, 2021 · 57 min video',
                        link: 'https://www.youtube.com/watch?v=u1duZUptIcc',
                        icon: <YouTubeIcon fontSize="large" className={classes.youtubeIcon} />,
                    },
                ],
            },
        ],
        [classes.youtubeIcon, t],
    );

    const OtherSmartPyMaterials = React.useMemo<Materials>(
        () => [
            {
                section: (
                    <>
                        {t('articlesGuides.b9labSection')} <Link href="https://tezos.b9lab.com">B9lab</Link>
                    </>
                ),
                items: [
                    {
                        title: 'Tezos Introduction',
                        date: 'April 8, 2021',
                        link: 'https://tezos.b9lab.com/blockchain-fundamentals/landingpage',
                        icon: <SchoolIcon fontSize="large" />,
                    },
                    {
                        title: 'Writing Smart Contracts',
                        date: 'April 8, 2021',
                        link: 'https://tezos.b9lab.com/smart-contracts',
                        icon: <SchoolIcon fontSize="large" />,
                    },
                ],
            },
            {
                section: (
                    <>
                        {t('articlesGuides.tzatzikitzSection')} <Link href="https://styts.com">tzatzikitz</Link>
                    </>
                ),
                items: [
                    {
                        title: 'Build your first DApp on Tezos in 2-4 hours',
                        date: 'Mar 15, 2021',
                        link: 'https://styts.com/your-first-tezos-dapp',
                        icon: <SchoolIcon fontSize="large" />,
                    },
                ],
            },
            {
                section: t('articlesGuides.digitalAssets'),
                items: [
                    {
                        title: 'FA2 Token Guide By Seb Mondet',
                        date: 'Sep 10, 2020',
                        link: 'https://assets.tqtezos.com/docs/token-contracts/fa2/1-fa2-smartpy',
                        icon: <SchoolIcon fontSize="large" />,
                    },
                    {
                        title: 'Introducing FA2: A Multi-Asset Interface for Tezos',
                        date: 'Mar 6, 2020 · 9 min read',
                        link: 'https://medium.com/tqtezos/introducing-fa2-a-multi-asset-interface-for-tezos-55173d505e5f',
                        icon: <MediumIcon fontSize="large" />,
                    },
                ],
            },
            {
                section: (
                    <>
                        {t('articlesGuides.cryptonomicSection')}{' '}
                        <Link href="https://cryptonomic.tech/">Cryptonomic</Link>
                    </>
                ),
                items: [
                    {
                        title: 'The future of Chainlink on Tezos',
                        date: 'Oct 8, 2020 · 4 min read',
                        link: 'https://medium.com/the-cryptonomic-aperiodical/the-future-of-chainlink-on-tezos-7f76c7bc64d5',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'Smart Contract Development Syllabus',
                        date: 'Aug 16, 2019 · 1 min read',
                        link: 'https://medium.com/the-cryptonomic-aperiodical/smart-contract-development-syllabus-f285a8463a4d',
                        icon: <MediumIcon fontSize="large" />,
                    },
                ],
            },
            {
                section: (
                    <>
                        {t('articlesGuides.buidllabsSection')} <Link href="https://buidllabs.io">BUIDL LABS</Link>
                    </>
                ),
                items: [
                    {
                        title: 'Learn to code Tezos blockchain DApps in SmartPy',
                        date: '2020-2021',
                        link: 'https://cryptocodeschool.in/tezos',
                        icon: <VideogameAssetIcon fontSize="large" />,
                    },
                ],
            },
            {
                section: (
                    <>
                        {t('articlesGuides.tezosindiaFoundationSection')}{' '}
                        <Link href="https://tezosindia.foundation">Tezos India Foundation</Link>
                    </>
                ),
                items: [
                    {
                        title: 'Factory Contract in SmartPy',
                        date: 'Sep 25, 2020 · 2 min read',
                        link: 'https://medium.com/tezos-india-foundation/factory-contract-in-smartpy-3f3eb1cce69d',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'Creating a Shopping Dapp with React.js and Temple Wallet',
                        date: 'Jul 29, 2020 · 4 min read',
                        link: 'https://medium.com/tezos-india-foundation/creating-a-shopping-dapp-with-react-js-and-thanos-wallet-67decaabaf0a',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'Inter-Contract Calling in SmartPy.',
                        date: 'May 25, 2020 · 3 min read',
                        link: 'https://medium.com/tezos-india-foundation/inter-contract-calling-in-smartpy-4e91dc7814bd',
                        icon: <MediumIcon fontSize="large" />,
                    },
                ],
            },
            {
                section: (
                    <>
                        {t('articlesGuides.blockmaticsSection')} <Link href="https://blockmatics.io/">Blockmatics</Link>
                    </>
                ),
                items: [
                    {
                        title: 'SmartPy - An Introduction to the smart contract language for Tezos',
                        date: 'Aug 8, 2019 · 12 min video',
                        link: 'https://www.youtube.com/watch?v=zXJNFVfSgdU',
                        icon: <YouTubeIcon fontSize="large" className={classes.youtubeIcon} />,
                    },
                    {
                        title: 'Tezos SmartPy Developer Course',
                        date: '',
                        link: 'https://training.blockmatics.io/p/tezos-smartpy-developer-course',
                        icon: <SchoolIcon fontSize="large" />,
                    },
                    {
                        title: 'An Escrow Smart Contract in SmartPy',
                        date: 'Aug 26, 2019 · 3 min read',
                        link: 'https://medium.com/@solled/an-escrow-smart-contract-in-smartpy-4ca80a0a28c6',
                        icon: <MediumIcon fontSize="large" />,
                    },
                ],
            },
            {
                section: (
                    <>
                        {t('articlesGuides.luizMilfontSection')}{' '}
                        <Link href="https://medium.com/@lmilfont">Luiz Milfont</Link>
                    </>
                ),
                items: [
                    {
                        title: 'How To Write Smart Contracts for Blockchain Using Python — Part One',
                        date: 'Jul 26, 2019 · 8 min read',
                        link: 'https://medium.com/better-programming/how-to-write-smart-contracts-for-blockchain-using-python-8f645d485bf7',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'How To Write Smart Contracts for Blockchain Using Python — Part Two',
                        date: 'Jul 27, 2019 · 5 min read',
                        link: 'https://medium.com/better-programming/how-to-write-smart-contracts-for-blockchain-using-python-part-2-99fc0cd43c37',
                        icon: <MediumIcon fontSize="large" />,
                    },
                    {
                        title: 'How to write Smart Contracts for Blockchain using Python (Part 3)',
                        date: 'Jul 29, 2019 · 4 min read',
                        link: 'https://medium.com/@lmilfont/how-to-write-smart-contracts-for-blockchain-using-python-part-3-af14e0d72ea8',
                        icon: <MediumIcon fontSize="large" />,
                    },
                ],
            },
        ],
        [classes.youtubeIcon, t],
    );

    return (
        <div className={classes.root} id="articles">
            <Typography variant="h4" className={classes.title}>
                {t('articlesGuides.blogPostsAndOtherMaterials')}
            </Typography>
            <Grid container spacing={2} alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <Paper className={classes.card}>
                        <Typography gutterBottom variant="h6" className={classes.description}>
                            {t('articlesGuides.someMaterialsBySmartPy')}
                        </Typography>
                        <Divider />
                        {SmartPyMaterials.map(({ section, items }, index) => (
                            <React.Fragment key={index}>
                                <Typography gutterBottom variant="body1" className={classes.innerDescription}>
                                    {section}
                                </Typography>
                                <Divider />
                                <List className={classes.list}>
                                    {items.map(({ title, date, link, icon }, index) => (
                                        <ListItem key={index} dense button divider href={link} component="a">
                                            <ListItemAvatar>{icon}</ListItemAvatar>
                                            <ListItemText primary={title} secondary={date} />
                                        </ListItem>
                                    ))}
                                </List>
                                <Divider />
                            </React.Fragment>
                        ))}
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.card}>
                        <Typography gutterBottom variant="h6" className={classes.description}>
                            {t('articlesGuides.someMaterialsByOtherTeams')}
                        </Typography>
                        <Divider />
                        {OtherSmartPyMaterials.map(({ section, items }, index) => (
                            <React.Fragment key={index}>
                                <Typography gutterBottom variant="body1" className={classes.innerDescription}>
                                    {section}
                                </Typography>
                                <Divider />
                                <List className={classes.list}>
                                    {items.map(({ title, date, link, icon }, index) => (
                                        <ListItem key={index} dense button divider href={link} component="a">
                                            <ListItemAvatar>{icon}</ListItemAvatar>
                                            <ListItemText primary={title} secondary={date} />
                                        </ListItem>
                                    ))}
                                </List>
                                <Divider />
                            </React.Fragment>
                        ))}
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
};

export default ArticlesGuides;
