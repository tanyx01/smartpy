import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
// Material Icons
import SaveIcon from '@mui/icons-material/Save';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';
import { IDEContract } from 'SmartPyModels';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        deleteButton: {
            backgroundColor: theme.palette.error.light,
            marginTop: 20,
        },
    }),
);

interface OwnProps {
    open: boolean;
    contract?: IDEContract;
    handleClose: () => void;
    handleDeleteContract: (contractId: string) => void;
    handleUpdateContract: (contractId: string, name: string) => void;
}

const EditContractDialog: React.FC<OwnProps> = ({
    open,
    contract,
    handleClose,
    handleDeleteContract,
    handleUpdateContract,
}) => {
    const classes = useStyles();
    const t = useTranslation();
    const textInputRef = React.useRef(null as unknown as HTMLInputElement);
    const [name, setName] = React.useState(contract?.name || '');

    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    };

    const handleSave = () => {
        contract?.id && handleUpdateContract(contract.id, name);
    };

    const handleDelete = () => contract?.id && handleDeleteContract(contract.id);

    return (
        <Dialog fullWidth open={open} onClose={handleClose}>
            <DialogTitle>{t('ide.contractManagement.editContract')}</DialogTitle>
            <DialogContent dividers>
                <TextField
                    inputProps={{
                        maxLength: 32,
                        minLength: 1,
                    }}
                    ref={textInputRef}
                    fullWidth
                    label={t('common.contract.name')}
                    defaultValue={contract?.name || ''}
                    variant="filled"
                    size="small"
                    onChange={handleNameChange}
                    InputProps={{
                        endAdornment: (
                            <Tooltip
                                title={t('ide.contractManagement.updateContract') as string}
                                aria-label="update-contract"
                                placement="left"
                            >
                                <span>
                                    <IconButton
                                        disabled={!contract || name === contract.name}
                                        color="primary"
                                        onClick={handleSave}
                                        size="large"
                                    >
                                        <SaveIcon />
                                    </IconButton>
                                </span>
                            </Tooltip>
                        ),
                    }}
                />
                <Button
                    onClick={handleDelete}
                    startIcon={<DeleteForeverIcon />}
                    autoFocus
                    variant="contained"
                    className={classes.deleteButton}
                >
                    {t('ide.contractManagement.deleteContract')}
                </Button>
            </DialogContent>
            <DialogActions>
                <Button autoFocus color="primary" onClick={handleClose}>
                    {t('common.close')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default EditContractDialog;
