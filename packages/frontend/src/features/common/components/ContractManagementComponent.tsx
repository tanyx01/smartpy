import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import withStyles from '@mui/styles/withStyles';
import useMediaQuery from '@mui/material/useMediaQuery';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';

// Local Components
import CreateContractDialog from './CreateContractDialog';
import EditContractDialog from './EditContractDialog';
import StoredContractsDialog from './StoredContractsDialog';
// Local Elements
import CreateContractButton from '../elements/CreateContractButton';
import EditContractButton from '../elements/EditContractButton';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';
import { IDEContract } from 'SmartPyModels';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        buttonContainer: {
            display: 'flex',
            justifyContent: 'space-evenly',
            padding: 8,
        },
    }),
);

const Accordion = withStyles({
    root: {
        border: '1px solid rgba(0, 0, 0, .125)',
        boxShadow: 'none',
        '&:not(:last-child)': {
            borderBottom: 0,
        },
        '&:before': {
            display: 'none',
        },
        '&$expanded': {
            margin: 0,
        },
    },
    expanded: {
        margin: 0,
    },
})(MuiAccordion);

const AccordionSummary = withStyles({
    root: {
        backgroundColor: 'rgba(0, 0, 0, .03)',
        borderBottom: '1px solid rgba(0, 0, 0, .125)',
        marginBottom: -1,
        minHeight: 56,
        '&$expanded': {
            minHeight: 56,
        },
    },
    content: {
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        '&$expanded': {
            margin: '12px 0',
        },
    },
    expanded: {},
})(MuiAccordionSummary);

interface OwnProps {
    onResize?: () => void;
    contract?: IDEContract;
    contracts: IDEContract[];
    handleSelectContract: (contractId: string) => void;
    handleDeleteContract: (contractId: string) => void;
    handleUpdateContract: (contractId: string, name: string) => void;
    handleCreateContract: (name: string) => void;
}

const ContractManagement: React.FC<OwnProps> = ({
    onResize,
    contract,
    contracts,
    handleSelectContract,
    handleDeleteContract,
    handleUpdateContract,
    handleCreateContract,
}) => {
    const classes = useStyles();
    const t = useTranslation();
    const [expanded, setExpanded] = React.useState(false);
    const onlyIcon = useMediaQuery('(max-width:1200px)');
    const [edit, setEdit] = React.useState(false);
    const [create, setCreate] = React.useState(false);

    const handleCreateOpen = () => {
        setCreate(true);
    };

    const handleCreateClose = () => {
        setCreate(false);
    };

    const handleEditOpen = () => {
        setEdit(true);
    };

    const handleEditClose = () => {
        setEdit(false);
    };

    const toggleExpand = () => {
        setExpanded((state) => !state);
        onResize && setTimeout(onResize, 500);
    };

    return (
        <Accordion square expanded={expanded} onChange={toggleExpand}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography>{t('ide.contractManagement.title')}</Typography>
                <Chip
                    variant="outlined"
                    size="small"
                    label={contract?.name || t('ide.contractManagement.contractNotInitiated')}
                />
            </AccordionSummary>
            <MuiAccordionDetails className={classes.buttonContainer}>
                <CreateContractButton onlyIcon={onlyIcon} disabled={!contract} onClick={handleCreateOpen} />
                <EditContractButton onlyIcon={onlyIcon} disabled={!contract} onClick={handleEditOpen} />
                <StoredContractsDialog
                    onlyIcon={onlyIcon}
                    contracts={contracts}
                    handleDeleteContract={handleDeleteContract}
                    handleUpdateContract={handleUpdateContract}
                    handleSelectContract={handleSelectContract}
                />
            </MuiAccordionDetails>
            <EditContractDialog
                open={edit && !!contract?.id}
                contract={contract}
                handleDeleteContract={handleDeleteContract}
                handleUpdateContract={handleUpdateContract}
                handleClose={handleEditClose}
            />
            <CreateContractDialog
                open={create}
                handleClose={handleCreateClose}
                handleCreateContract={handleCreateContract}
            />
        </Accordion>
    );
};

export default ContractManagement;
