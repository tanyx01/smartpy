import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Typography from '@mui/material/Typography';

// Local Components
import SmartPyIcon from '../elements/icons/SmartPy';
import OutputCss from '../styles/OutputCss';
import CircularProgress from '../../loader/components/CircularProgressWithText';

// Local hooks
import useTranslation from '../../i18n/hooks/useTranslation';

// Local Utils
import debounce from '../../../utils/debounce';

// Debounce helpers
const onScrollDebouncer = debounce(1000);

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            height: '100%',
            display: 'flex',
        },
        overflow: {
            position: 'relative',
            overflowY: 'auto',
            height: '100%',
            lineHeight: 1.5,
        },
        emptyBackground: {
            position: 'absolute',
            width: '50%',
            height: '50%',
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -50%)',
            opacity: 0.5,
        },
        header: {
            margin: 10,
            color: theme.palette.text.primary,
        },
    }),
);

interface OwnProps {
    isContractCompiling: boolean;
    output: { __html: string };
    shouldUpdateTabs?: boolean;
}

const OutputPanel: React.FC<OwnProps> = ({ output, isContractCompiling, shouldUpdateTabs = false }) => {
    const outputPanelRef = React.useRef(null as unknown as HTMLDivElement);
    const [scrollTop, setScrollTop] = React.useState(null as number | null);
    const t = useTranslation();
    const classes = useStyles();
    const latestUpperTab = React.useRef('');
    const latestLowerTab = React.useRef('');

    const onScroll = () => {
        onScrollDebouncer(setScrollTop, outputPanelRef.current?.scrollTop);
    };

    React.useEffect(() => {
        if (scrollTop && outputPanelRef.current) {
            outputPanelRef.current.scrollTo({ top: scrollTop });
        }
    }, [output, scrollTop]);

    React.useEffect(() => {
        const originalOpenTab = (window as any).openTab;
        (window as any).openTab = (evt: any, name: any) => {
            if (evt?.currentTarget?.parentNode?.firstElementChild?.innerText === 'Output') {
                latestUpperTab.current = evt.currentTarget.innerText;
            } else {
                latestLowerTab.current = evt.currentTarget.innerText;
            }

            originalOpenTab(evt, name);
        };
        return () => {
            (window as any).openTab = originalOpenTab;
            latestUpperTab.current = '';
            latestLowerTab.current = '';
        };
    }, []);

    if (isContractCompiling) {
        return (
            <div className={classes.root}>
                <CircularProgress />
            </div>
        );
    }
    /**
     * Hacky function that re-opens the previous selected tabs. (This is needed because the HTML is generated from ocaml)
     */
    const updateTabs = () => {
        if (shouldUpdateTabs) {
            setImmediate(() => {
                const upperParentTab = document.getElementsByClassName('tab')[0];

                let upperTabIndex = Array.from(upperParentTab?.childNodes || []).findIndex(
                    ({ innerText }: any) => innerText === latestUpperTab.current,
                );

                if (upperTabIndex === -1 && latestLowerTab.current) {
                    upperTabIndex = 1;
                }

                if (upperTabIndex > 0) {
                    upperParentTab.childNodes.forEach(({ classList }: any, index) => {
                        upperTabIndex === index ? classList.add('active') : classList.remove('active');
                    });
                    upperParentTab?.parentElement?.childNodes?.forEach(
                        ({ style, className, childNodes }: any, index) => {
                            if (upperTabIndex === index) {
                                style.display = 'block';
                                const lowerParentTab = childNodes[0]?.childNodes[0];

                                if (lowerParentTab && lowerParentTab?.classList?.contains('tab')) {
                                    const lowerTabIndex = Array.from(lowerParentTab?.childNodes || []).findIndex(
                                        ({ innerText }: any) => innerText === latestLowerTab.current,
                                    );
                                    if (lowerTabIndex > 0) {
                                        Array.from(lowerParentTab?.childNodes || []).forEach(
                                            ({ classList }: any, index) => {
                                                lowerTabIndex === index
                                                    ? classList.add('active')
                                                    : classList.remove('active');
                                            },
                                        );
                                        Array.from(lowerParentTab?.parentElement?.childNodes || []).forEach(
                                            ({ style, className }: any, index) => {
                                                if (lowerTabIndex === index) {
                                                    style.display = 'block';
                                                } else if (className === 'tabcontent') {
                                                    style.display = 'none';
                                                }
                                            },
                                        );
                                    }
                                } else {
                                    latestLowerTab.current = '';
                                }
                            } else if (className === 'tabcontent') {
                                style.display = 'none';
                            }
                        },
                    );
                } else {
                    latestUpperTab.current = '';
                }
            });
        }
        return null;
    };

    return output.__html.length === 0 ? (
        <React.Fragment>
            <SmartPyIcon className={classes.emptyBackground} />
            <Typography className={classes.header} variant="caption" component="h2">
                {t('ide.outputPanel')}
            </Typography>
        </React.Fragment>
    ) : (
        <div className={classes.overflow} onScroll={onScroll} ref={outputPanelRef}>
            <OutputCss>
                <div dangerouslySetInnerHTML={output} />
                {updateTabs()}
            </OutputCss>
        </div>
    );
};

export default OutputPanel;
