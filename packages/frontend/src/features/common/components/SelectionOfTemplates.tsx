import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Divider from '@mui/material/Divider';

// Local Components
import SmartPyIcon from '../elements/icons/SmartPy';

// State Management
import useTranslation from '../../i18n/hooks/useTranslation';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        card: {
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,
            boxShadow: `5px 5px 0 0 ${theme.palette.primary.main}`,
        },
        marginBottom: {
            marginBottom: 20,
        },
        innerDescription: {
            fontWeight: 'bold',
            margin: 10,
        },
        list: {
            width: '100%',
            backgroundColor: theme.palette.background.default,
            padding: 0,
        },
        icon: {
            marginTop: 5,
            width: 32,
            height: 32,
        },
    }),
);

const SelectionOfTemplates: React.FC = ({ children }) => {
    const classes = useStyles();
    const t = useTranslation();

    return (
        <Paper className={`${classes.card} ${classes.marginBottom}`}>
            {/* TEMPLATES */}
            <Divider />
            <Typography gutterBottom variant="body1" className={classes.innerDescription}>
                {t('commonComponents.selectionOfTemplates.title')}
            </Typography>
            <Divider />
            <List className={classes.list}>
                <ListItem dense divider button to="ide?template=calculator.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon className={classes.icon} />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.calculatorDescription')} />
                </ListItem>
                <ListItem dense divider button to="ide?template=storeValue.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon className={classes.icon} />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.storeValueDescription')} />
                </ListItem>
                <ListItem dense button to="ide?template=chess.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon className={classes.icon} />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.chessDescription')} />
                </ListItem>
                <Divider />
                <ListItem dense divider button to="ide?template=FA2.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon className={classes.icon} />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.fa2Description')} />
                </ListItem>
                <ListItem dense button to="ide?template=oracle.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon className={classes.icon} />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.chainLinkDescription')} />
                </ListItem>
            </List>
            {children}
        </Paper>
    );
};

export default SelectionOfTemplates;
