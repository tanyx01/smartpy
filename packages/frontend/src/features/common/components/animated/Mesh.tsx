import React from 'react';

const defaultOptions = {
    delay: 200,
    particleColor: 'rgb(144, 202, 249)',
    lineColor: 'rgb(144, 202, 249)',
    rgb: 'rgb(144, 202, 249)'.match(/\d+/g) as RegExpMatchArray,
    particleAmount: 30,
    defaultSpeed: 1,
    variantSpeed: 2,
    defaultRadius: 1,
    variantRadius: 2,
    linkRadius: 150,
};

const checkDistance = (x1: number, y1: number, x2: number, y2: number) => {
    return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
};

const calculateStrokeStyle = (opacity: number) => {
    return `rgba(${defaultOptions.rgb[0]}, ${defaultOptions.rgb[1]}, ${defaultOptions.rgb[2]}, ${opacity})`;
};

interface Vector {
    x: number;
    y: number;
}

class Particle {
    private width: number;
    private height: number;
    public x: number;
    public y: number;
    private speed: number;
    private directionAngle: number;
    private color: string;
    private radius: number;
    private vector: Vector;

    constructor(xPos: number, yPos: number) {
        this.width = xPos;
        this.height = yPos;
        this.x = Math.random() * xPos;
        this.y = Math.random() * yPos;
        this.speed = defaultOptions.defaultSpeed + Math.random() * defaultOptions.variantSpeed;
        this.directionAngle = Math.floor(Math.random() * 360);
        this.color = defaultOptions.particleColor;
        this.radius = defaultOptions.defaultRadius + Math.random() * defaultOptions.variantRadius;
        this.vector = {
            x: Math.cos(this.directionAngle) * this.speed,
            y: Math.sin(this.directionAngle) * this.speed,
        };
    }

    public update = () => {
        this.border();
        this.x += this.vector.x;
        this.y += this.vector.y;
    };

    public border = () => {
        if (this.x >= this.width || this.x <= 0) {
            this.vector.x *= -1;
        }
        if (this.y >= this.height || this.y <= 0) {
            this.vector.y *= -1;
        }
        if (this.x > this.width) this.x = this.width;
        if (this.y > this.height) this.y = this.height;
        if (this.x < 0) this.x = 0;
        if (this.y < 0) this.y = 0;
    };

    public draw = (canvas: CanvasRenderingContext2D) => {
        canvas.beginPath();
        canvas.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        canvas.closePath();
        canvas.fillStyle = this.color;
        canvas.fill();
    };
}

interface Props {
    className: string;
}

const DelayedMeshAnimation: React.FC<Props> = (props) => {
    const [isReady, setReady] = React.useState(false);
    React.useEffect(() => {
        const timeout = setTimeout(() => setReady(true), 1000);
        return () => clearTimeout(timeout);
    }, []);

    return isReady ? <MeshAnimation {...props} /> : <React.Fragment />;
};

const MeshAnimation: React.FC<Props> = (props) => {
    const isMounted = React.useRef(false);
    const timeoutId = React.useRef(null as unknown as ReturnType<typeof setTimeout>);
    const animationId = React.useRef(null as unknown as ReturnType<typeof requestAnimationFrame>);
    const canvasRef = React.useRef(null as unknown as HTMLCanvasElement);
    const parentRef = React.useRef(null as unknown as HTMLDivElement);
    const particles = React.useRef([] as Particle[]);
    const canvas = React.useRef(null as unknown as CanvasRenderingContext2D);
    const [width, setWidth] = React.useState(0);
    const [height, setHeight] = React.useState(0);

    const resizeReset = React.useCallback(() => {
        if (parentRef.current && canvasRef.current) {
            canvasRef.current.width = parentRef.current.clientWidth;
            canvasRef.current.height = parentRef.current.clientHeight;
            setWidth(parentRef.current.clientWidth);
            setHeight(parentRef.current.clientHeight);
        }
    }, []);

    const deBouncer = React.useCallback(() => {
        timeoutId.current && clearTimeout(timeoutId.current);
        timeoutId.current = setTimeout(resizeReset, defaultOptions.delay);
    }, [resizeReset]);

    const linkPoints = (point1: Particle) => {
        particles.current.forEach((particle) => {
            const distance = checkDistance(point1.x, point1.y, particle.x, particle.y);
            const opacity = 1 - distance / defaultOptions.linkRadius;
            if (opacity > 0) {
                canvas.current.lineWidth = 0.5;
                canvas.current.strokeStyle = calculateStrokeStyle(opacity);
                canvas.current.beginPath();
                canvas.current.moveTo(point1.x, point1.y);
                canvas.current.lineTo(particle.x, particle.y);
                canvas.current.closePath();
                canvas.current.stroke();
            }
        });
    };

    const loopCallback = React.useCallback(() => {
        canvas.current.clearRect(0, 0, width, height);
        particles.current.forEach((particle) => {
            particle.update();
            particle.draw(canvas.current);
            linkPoints(particle);
        });
        animationId.current = requestAnimationFrame(loopCallback);
    }, [width, height]);

    React.useEffect(() => {
        if (canvasRef.current) {
            const context = canvasRef.current.getContext('2d');
            if (context) {
                canvas.current = context;
                window.addEventListener('resize', deBouncer);
                resizeReset();
            }
        }
        return () => {
            window.removeEventListener('resize', deBouncer);
        };
    }, [deBouncer, resizeReset]);

    React.useEffect(() => {
        if (canvasRef.current) {
            loopCallback();
        }
        return () => {
            if (animationId.current) {
                cancelAnimationFrame(animationId.current);
            }
        };
    }, [loopCallback]);

    React.useEffect(() => {
        if (canvas.current && !isMounted.current) {
            isMounted.current = true;
            // Generate Particles
            (async () => {
                const collection: Particle[] = [];
                for (let i = 0; i < defaultOptions.particleAmount; i++) {
                    await new Promise((resolve) => setImmediate(resolve));
                    collection.push(new Particle(width, height));
                }
                particles.current = collection;
            })();
        }
        return () => {
            isMounted.current = false;
        };
    }, [height, width]);

    return (
        <div ref={parentRef} {...props}>
            <canvas ref={canvasRef} />
        </div>
    );
};

export default DelayedMeshAnimation;
