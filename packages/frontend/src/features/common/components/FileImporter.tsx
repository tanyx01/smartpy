import React from 'react';

import { useDropzone, FileWithPath } from 'react-dropzone';

import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Chip from '@mui/material/Chip';
import Fade from '@mui/material/Fade';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';

import useTranslation from '../../i18n/hooks/useTranslation';

const useStyles = makeStyles(() =>
    createStyles({
        paper: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: 100,
            borderWidth: 2,
            borderRadius: 2,
            borderColor: 'gray',
            borderStyle: 'dashed',
        },
    }),
);

interface OwnProps {
    open: boolean;
    onClose: () => void;
    onComplete: (file: File) => void;
    accept: string;
}

const FileImporter: React.FC<OwnProps> = (props) => {
    const classes = useStyles();
    const [file, setFile] = React.useState(null as unknown as FileWithPath);
    const t = useTranslation();
    const { getRootProps, getInputProps } = useDropzone({
        accept: props.accept,
        noDrag: false,
        multiple: false,
        getFilesFromEvent: async (event: any) => {
            const files = event.dataTransfer ? event.dataTransfer.files : event.target.files;
            setFile(files.length > 0 ? files[0] : null);
            return [];
        },
    });

    const { open, onClose, onComplete } = props;

    const handleClose = () => {
        onClose();
        handleClear();
    };

    /**
     * Clear selected files.
     */
    const handleClear = () => {
        setFile(null as unknown as FileWithPath);
    };

    /**
     * Pass value to callback Handle Complete (This will send the file to the parent component)
     */
    const handleDone = () => {
        onComplete(file);
        handleClear();
    };

    return (
        <Dialog
            open={open}
            keepMounted
            onClose={handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle id="alert-dialog-slide-title">{'Import a contract'}</DialogTitle>
            <DialogContent>
                <Fade in={open}>
                    <div>
                        <div
                            {...getRootProps({
                                className: classes.paper,
                            })}
                        >
                            <input {...getInputProps()} />
                            <p>{t('fileImporter.dragAndDrop')}</p>
                        </div>
                        {file ? (
                            <aside>
                                <h4>Contract File:</h4>
                                <Chip size="small" label={`${file.name} - ${file.size} Bytes`} onDelete={handleClear} />
                            </aside>
                        ) : null}
                    </div>
                </Fade>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Cancel
                </Button>
                <Button disabled={!file} onClick={handleDone} color="primary">
                    Done
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default FileImporter;
