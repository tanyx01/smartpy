export enum IDENewcomerGuideSteps {
    NONE,
    TEMPLATES,
    RUN_CODE,
}

export enum SYNTAX {
    PYTHON = 'PYTHON',
    TYPESCRIPT = 'TYPESCRIPT',
    OCAML = 'OCAML',
    MICHELSON = 'MICHELSON',
}
