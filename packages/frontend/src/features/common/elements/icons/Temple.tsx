import React from 'react';

import { useTheme } from '@mui/material/styles';

const TempleIcon = () => {
    const textColor = useTheme().palette.text.primary;
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0px"
            y="0px"
            viewBox="0 0 1615 512"
            xmlSpace="preserve"
            height="42px"
            fill={textColor}
            stroke="none"
        >
            <path d="M475.8,198.7v-41.1h193.9v41.1h-72.3v194.9h-49.3V198.7H475.8z"></path>
            <path d="M750,397c-18.2,0-33.9-3.7-47-11.1c-13.1-7.4-23.2-17.9-30.3-31.6c-7.1-13.6-10.6-29.8-10.6-48.5 c0-18.2,3.5-34.2,10.6-47.9c7.1-13.8,17-24.5,29.9-32.2c12.9-7.7,28-11.5,45.4-11.5c11.7,0,22.6,1.9,32.7,5.6 c10.1,3.7,18.9,9.3,26.5,16.8c7.6,7.5,13.5,16.8,17.7,28.1c4.2,11.3,6.3,24.4,6.3,39.5v13.5H681.8v-30.4h103.3 c0-7.1-1.5-13.3-4.6-18.8c-3.1-5.5-7.3-9.7-12.7-12.9c-5.4-3.1-11.7-4.7-18.8-4.7c-7.5,0-14,1.7-19.8,5.1 c-5.7,3.4-10.2,8-13.4,13.7c-3.2,5.7-4.9,12.1-5,19.1v28.9c0,8.8,1.6,16.3,4.9,22.7c3.3,6.4,7.9,11.3,13.9,14.8 c6,3.5,13.1,5.2,21.3,5.2c5.5,0,10.4-0.8,15-2.3c4.5-1.5,8.4-3.8,11.6-6.9c3.2-3.1,5.7-6.8,7.4-11.3l45.4,3 c-2.3,10.9-7,20.4-14.1,28.5c-7.1,8.1-16.3,14.4-27.4,18.9C777.5,394.8,764.6,397,750,397z"></path>
            <path d="M863.3,393.6v-177h46.8v31.2h2.1c3.7-10.4,9.8-18.6,18.4-24.5c8.6-6,18.9-9,30.9-9c12.1,0,22.5,3,31,9 c8.5,6,14.2,14.2,17.1,24.5h1.8c3.6-10.1,10.2-18.3,19.7-24.4c9.5-6.1,20.7-9.2,33.7-9.2c16.5,0,29.9,5.2,40.3,15.7 c10.3,10.5,15.5,25.3,15.5,44.5v119.1h-49V284.2c0-9.8-2.6-17.2-7.8-22.1c-5.2-4.9-11.8-7.4-19.6-7.4c-8.9,0-15.9,2.8-20.9,8.5 c-5,5.6-7.5,13.1-7.5,22.3v108.1h-47.6V283.2c0-8.7-2.5-15.6-7.4-20.7c-5-5.1-11.5-7.7-19.5-7.7c-5.5,0-10.4,1.4-14.7,4.1 s-7.8,6.5-10.3,11.4c-2.5,4.9-3.8,10.6-3.8,17.1v106.3H863.3z"></path>
            <path d="M1159.3,460V216.5h48.4v29.7h2.2c2.2-4.8,5.3-9.6,9.4-14.6c4.1-5,9.5-9.1,16.1-12.4c6.6-3.3,14.9-5,24.8-5 c12.9,0,24.8,3.4,35.7,10.1c10.9,6.7,19.6,16.8,26.2,30.3c6.5,13.5,9.8,30.4,9.8,50.7c0,19.7-3.2,36.4-9.5,50 c-6.3,13.6-14.9,23.8-25.8,30.8c-10.9,7-23,10.4-36.5,10.4c-9.5,0-17.6-1.6-24.3-4.7c-6.6-3.1-12.1-7.1-16.3-11.9 c-4.2-4.8-7.5-9.7-9.7-14.6h-1.5V460H1159.3z M1207.3,305.1c0,10.5,1.5,19.7,4.4,27.5c2.9,7.8,7.1,13.9,12.7,18.3 c5.5,4.3,12.3,6.5,20.2,6.5c8,0,14.8-2.2,20.3-6.6c5.5-4.4,9.7-10.6,12.6-18.4c2.9-7.9,4.3-17,4.3-27.3c0-10.2-1.4-19.2-4.3-27 c-2.8-7.8-7-13.8-12.6-18.2c-5.5-4.4-12.3-6.6-20.4-6.6c-8,0-14.7,2.1-20.2,6.3c-5.5,4.2-9.7,10.2-12.6,18 C1208.8,285.4,1207.3,294.5,1207.3,305.1z"></path>
            <path d="M1413.7,157.5v236h-49.1v-236H1413.7z"></path>
            <path d="M1533.5,397c-18.2,0-33.9-3.7-47-11.1c-13.1-7.4-23.2-17.9-30.3-31.6c-7.1-13.6-10.6-29.8-10.6-48.5 c0-18.2,3.5-34.2,10.6-47.9c7.1-13.8,17-24.5,29.9-32.2c12.9-7.7,28-11.5,45.4-11.5c11.7,0,22.6,1.9,32.7,5.6 c10.1,3.7,18.9,9.3,26.5,16.8c7.6,7.5,13.5,16.8,17.7,28.1c4.2,11.3,6.3,24.4,6.3,39.5v13.5h-149.5v-30.4h103.3 c0-7.1-1.5-13.3-4.6-18.8c-3.1-5.5-7.3-9.7-12.7-12.9c-5.4-3.1-11.7-4.7-18.8-4.7c-7.5,0-14,1.7-19.8,5.1 c-5.7,3.4-10.2,8-13.4,13.7c-3.2,5.7-4.9,12.1-5,19.1v28.9c0,8.8,1.6,16.3,4.9,22.7c3.3,6.4,7.9,11.3,13.9,14.8 c6,3.5,13.1,5.2,21.3,5.2c5.5,0,10.4-0.8,15-2.3c4.5-1.5,8.4-3.8,11.6-6.9c3.2-3.1,5.7-6.8,7.4-11.3l45.4,3 c-2.3,10.9-7,20.4-14.1,28.5c-7.1,8.1-16.3,14.4-27.4,18.9C1561,394.8,1548.1,397,1533.5,397z"></path>
            <g>
                <linearGradient
                    id="SVGID_1_"
                    gradientUnits="userSpaceOnUse"
                    x1="39.3846"
                    y1="236.3078"
                    x2="354.4615"
                    y2="236.3078"
                >
                    <stop offset="0.001880787" stopColor="rgb(252, 195, 60)"></stop>
                    <stop offset="1" stopColor="rgb(255, 238, 80)"></stop>
                </linearGradient>
                <polygon
                    fill="url(#SVGID_1_)"
                    points="275.7,157.5 354.5,157.5 315.1,0 39.4,0 78.8,157.5 157.5,157.5 196.9,315.1 147.7,315.1  167.4,393.8 216.6,393.8 236.3,472.6 354.5,472.6  "
                ></polygon>
                <linearGradient
                    id="SVGID_2_"
                    gradientUnits="userSpaceOnUse"
                    x1="354.4615"
                    y1="177.2308"
                    x2="275.6923"
                    y2="177.2308"
                >
                    <stop offset="0" stopColor="rgb(251, 152, 40)"></stop>
                    <stop offset="1" stopColor="rgb(249, 108, 19)"></stop>
                </linearGradient>
                <polygon fill="url(#SVGID_2_)" points="315.1,196.9 285.5,196.9 275.7,157.5 354.5,157.5  "></polygon>
                <linearGradient
                    id="SVGID_3_"
                    gradientUnits="userSpaceOnUse"
                    x1="290.4854"
                    y1="68.9231"
                    x2="93.5385"
                    y2="68.9231"
                >
                    <stop offset="0" stopColor="rgb(248, 66, 0)"></stop>
                    <stop offset="1" stopColor="rgb(249, 108, 19)"></stop>
                </linearGradient>
                <polygon fill="url(#SVGID_3_)" points="275.7,78.8 98.5,78.8 93.5,59.1 290.5,59.1  "></polygon>
                <linearGradient
                    id="SVGID_4_"
                    gradientUnits="userSpaceOnUse"
                    x1="16.2722"
                    y1="5.7626"
                    x2="62.4971"
                    y2="191.1605"
                >
                    <stop offset="0" stopColor="rgb(249, 108, 19)"></stop>
                    <stop offset="1" stopColor="rgb(251, 152, 40)"></stop>
                </linearGradient>
                <polygon fill="url(#SVGID_4_)" points="39.4,196.9 0,39.4 39.4,0 78.8,157.5  "></polygon>
                <linearGradient
                    id="SVGID_5_"
                    gradientUnits="userSpaceOnUse"
                    x1="127.9997"
                    y1="413.5384"
                    x2="216.6157"
                    y2="413.5384"
                >
                    <stop offset="0" stopColor="rgb(248, 66, 0)"></stop>
                    <stop offset="1" stopColor="rgb(249, 108, 19)"></stop>
                </linearGradient>
                <polygon fill="url(#SVGID_5_)" points="216.6,393.8 167.4,393.8 128,433.2 177.2,433.2  "></polygon>
                <linearGradient
                    id="SVGID_6_"
                    gradientUnits="userSpaceOnUse"
                    x1="39.3846"
                    y1="177.2308"
                    x2="157.5385"
                    y2="177.2308"
                >
                    <stop offset="0" stopColor="rgb(248, 66, 0)"></stop>
                    <stop offset="1" stopColor="rgb(249, 108, 19)"></stop>
                </linearGradient>
                <polygon fill="url(#SVGID_6_)" points="118.2,196.9 39.4,196.9 78.8,157.5 157.5,157.5  "></polygon>
                <linearGradient
                    id="SVGID_7_"
                    gradientUnits="userSpaceOnUse"
                    x1="173.711"
                    y1="320.8643"
                    x2="134.426"
                    y2="163.301"
                >
                    <stop offset="0" stopColor="rgb(249, 108, 19)"></stop>
                    <stop offset="1" stopColor="rgb(251, 152, 40)"></stop>
                </linearGradient>
                <polygon fill="url(#SVGID_7_)" points="196.9,315.1 147.7,315.1 118.2,196.9 157.5,157.5  "></polygon>
                <linearGradient
                    id="SVGID_8_"
                    gradientUnits="userSpaceOnUse"
                    x1="151.1373"
                    y1="427.4619"
                    x2="124.5549"
                    y2="320.8458"
                >
                    <stop offset="0" stopColor="rgb(251, 152, 40)"></stop>
                    <stop offset="1" stopColor="rgb(249, 108, 19)"></stop>
                </linearGradient>
                <polygon fill="url(#SVGID_8_)" points="167.4,393.8 147.7,315.1 108.3,354.5 128,433.2  "></polygon>
                <linearGradient
                    id="SVGID_9_"
                    gradientUnits="userSpaceOnUse"
                    x1="220.0605"
                    y1="506.231"
                    x2="193.4781"
                    y2="399.615"
                >
                    <stop offset="0" stopColor="rgb(249, 108, 19)"></stop>
                    <stop offset="1" stopColor="rgb(251, 152, 40)"></stop>
                </linearGradient>
                <polygon fill="url(#SVGID_9_)" points="236.3,472.6 216.6,393.8 177.2,433.2 196.9,512  "></polygon>
                <linearGradient
                    id="SVGID_10_"
                    gradientUnits="userSpaceOnUse"
                    x1="299.3167"
                    y1="488.3636"
                    x2="208.1513"
                    y2="122.7192"
                >
                    <stop offset="0" stopColor="rgb(249, 108, 19)"></stop>
                    <stop offset="1" stopColor="rgb(248, 66, 0)"></stop>
                </linearGradient>
                <polygon
                    fill="url(#SVGID_10_)"
                    points="315.1,472.6 226.5,118.2 210.7,133.9 295.4,472.6 279.6,488.4 299.3,488.4  "
                ></polygon>
                <linearGradient
                    id="SVGID_11_"
                    gradientUnits="userSpaceOnUse"
                    x1="297.4457"
                    y1="126.4734"
                    x2="282.7152"
                    y2="67.3927"
                >
                    <stop offset="0" stopColor="rgb(248, 66, 0)"></stop>
                    <stop offset="1" stopColor="rgb(249, 108, 19)"></stop>
                </linearGradient>
                <polygon fill="url(#SVGID_11_)" points="305.2,118.2 285.5,118.2 275.7,78.8 290.5,59.1  "></polygon>
                <linearGradient
                    id="SVGID_12_"
                    gradientUnits="userSpaceOnUse"
                    x1="213.4426"
                    y1="492.3077"
                    x2="331.1172"
                    y2="492.3077"
                >
                    <stop offset="0" stopColor="rgb(248, 66, 0)"></stop>
                    <stop offset="1" stopColor="rgb(249, 108, 19)"></stop>
                </linearGradient>
                <polygon
                    fill="url(#SVGID_12_)"
                    points="315.1,472.6 299.3,488.4 279.6,488.4 295.4,472.6 236.3,472.6 196.9,512 315.1,512 354.5,472.6  "
                ></polygon>
            </g>
        </svg>
    );
};

export default TempleIcon;
