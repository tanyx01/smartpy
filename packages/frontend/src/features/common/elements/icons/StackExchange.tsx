import React from 'react';
import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon';

const StackExchangeIcon = (props: SvgIconProps) => (
    <SvgIcon {...props} width="36" height="36" viewBox="0 0 16 16">
        <path d="M0 3c0-1.1.9-2 2-2h8a2 2 0 012 2H0z" fill="#8FD8F7"></path>
        <path d="M12 10H0c0 1.1.9 2 2 2h5v3l3-3a2 2 0 002-2z" fill="#155397"></path>
        <path fill="#46A2D9" d="M0 4h12v2H0z"></path>
        <path fill="#2D6DB5" d="M0 7h12v2H0z"></path>
    </SvgIcon>
);

export default StackExchangeIcon;
