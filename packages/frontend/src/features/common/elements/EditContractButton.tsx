import React from 'react';

// Material UI
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
// Material Icons
import EditIcon from '@mui/icons-material/Edit';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';

interface OwnProps {
    onlyIcon: boolean;
    onClick: () => void;
    disabled: boolean;
}

const EditContractButton: React.FC<OwnProps> = ({ onlyIcon, ...props }) => {
    const t = useTranslation();

    return (
        <Tooltip
            title={t('ide.contractManagement.editContract') as string}
            aria-label="edit-contract"
            placement="bottom"
        >
            <span>
                <Button {...props} variant="outlined">
                    {onlyIcon ? <EditIcon /> : t('ide.contractManagement.editContract')}
                </Button>
            </span>
        </Tooltip>
    );
};

export default EditContractButton;
