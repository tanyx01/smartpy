import React from 'react';
import { RenderIfTrue } from 'src/utils/conditionalRender';

interface OwnProps {
    className?: string;
    children?: React.ReactNode;
    index: number;
    value: number;
}
const TabPanel: React.FC<OwnProps> = ({ children, value, index, ...other }) => (
    <RenderIfTrue isTrue={value === index}>
        <div role="tabpanel" id={`tabpanel-${index}`} {...other}>
            {value === index && children}
        </div>
    </RenderIfTrue>
);

export const TabPanelInnerHTML: React.FC<OwnProps> = ({ children, value, index, ...other }) => (
    <div
        {...other}
        style={{
            visibility: value === index ? 'visible' : 'hidden',
            height: value === index ? '100%' : 0,
        }}
    >
        {children}
    </div>
);

export default TabPanel;
