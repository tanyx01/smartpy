import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import SuccessIcon from '@mui/icons-material/Check';
import Avatar from '@mui/material/Avatar';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        successIcon: {
            width: 48,
            height: 48,
            backgroundColor: 'rgb(131, 179, 0)',
        },
    }),
);

const SuccessIndicator = () => {
    const classes = useStyles();

    return (
        <Avatar className={classes.successIcon}>
            <SuccessIcon fontSize="large" />
        </Avatar>
    );
};

export default SuccessIndicator;
