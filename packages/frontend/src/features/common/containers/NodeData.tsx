import React from 'react';

import NodeDataView, { NodeData } from '../views/NodeData';

// Local Constants
import { Endpoint } from '../../../constants/rpc';

// Local Services
import HttpRequester from '../../../services/httpRequester';
import Logger from '../../../services/logger';
import { useNetworkInfo } from '../../wallet/selectors/network';
import debounce from '../../../utils/debounce';

// Debounce helpers
const getNodeDataDebouncer = debounce(1000);

const NodeDataContainer: React.FC = () => {
    const isMounted = React.useRef(false);
    const [nodeData, setNodeData] = React.useState({} as unknown as NodeData);
    const { rpc } = useNetworkInfo();

    const updateNodeData = (json: Record<string, any>) => {
        isMounted.current && setNodeData((prev) => ({ ...prev, ...json }));
    };

    const getNodeData = React.useCallback(async () => {
        if (rpc) {
            HttpRequester.get(`${rpc}${Endpoint.HEADER}`)
                .then(({ data: header }) => updateNodeData({ header }))
                .catch((e) => {
                    Logger.error('[RPC] - Failed to fetch header.', e);
                });
            HttpRequester.get(`${rpc}${Endpoint.VERSION}`)
                .then(({ data: version }) => updateNodeData({ version }))
                .catch((e) => {
                    Logger.error('[RPC] - Failed to fetch version.', e);
                });
            HttpRequester.get(`${rpc}${Endpoint.ERRORS}`)
                .then(({ data: errors }) => updateNodeData({ errors }))
                .catch((e) => {
                    Logger.error('[RPC] - Failed to fetch errors.', e);
                });
            HttpRequester.get(`${rpc}${Endpoint.CONSTANTS}`)
                .then(({ data: constants }) => updateNodeData({ constants }))
                .catch((e) => {
                    Logger.error('[RPC] - Failed to fetch constants.', e);
                });
            HttpRequester.get(`${rpc}${Endpoint.METADATA}`)
                .then(({ data: metadata }) => updateNodeData({ metadata }))
                .catch((e) => {
                    Logger.error('[RPC] - Failed to fetch metadata.', e);
                });
            HttpRequester.get(`${rpc}${Endpoint.CHECKPOINT}`)
                .then(({ data: checkpoint }) => updateNodeData({ checkpoint }))
                .catch((e) => {
                    Logger.error('[RPC] - Failed to fetch checkpoint.', e);
                });
        }
    }, [rpc]);

    React.useEffect(() => {
        isMounted.current = true;

        getNodeDataDebouncer(getNodeData);

        return () => {
            isMounted.current = false;
        };
    }, [getNodeData]);

    return <NodeDataView {...{ rpc, nodeData }} />;
};

export default NodeDataContainer;
