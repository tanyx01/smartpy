import React from 'react';

import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { styled } from '@mui/styles';
import {
    SelectChangeEvent,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Container as MuiContainer,
    Divider,
    Box,
    Alert,
} from '@mui/material';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';

import RouterFab from 'src/features/navigation/elements/RouterFab';
import { Network } from 'src/constants/networks';
import * as RPC from 'src/constants/rpc';

import { fetchContractOperations, Operation } from 'src/utils/tzkt';
import useWalletContext from '../hooks/useWalletContext';
import { useNetworkInfo } from '../selectors/network';
import AccountView from '../views/Account';
import { updateAccountInfo, updateNetworkInfo } from '../actions';
import WalletService from '../services';
import Logger from 'src/services/logger';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import TextField from 'src/features/common/elements/TextField';
import { getRpcNetwork } from 'src/utils/tezosRpc';
import DecryptAccount from '../views/DecryptAccount';
import { TezosAccount } from 'smartpy-wallet-storage';
import NotFound from 'src/features/not-found/views/NotFound';

const Container = styled(MuiContainer)(() => ({
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
}));

const TopDiv = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
}));

const Account = () => {
    const { accountId } = useParams<{ accountId: string }>();
    const { accounts, updateAccountName, updateAccountPreferredRpc } = useWalletContext();
    const networkInfo = useNetworkInfo();
    const [operations, setOperations] = React.useState<Operation[]>([]);
    const t = useTranslation();
    const dispatch = useDispatch();

    const network = React.useMemo(() => networkInfo?.network as Network, [networkInfo?.network]);
    const encryptedAccount = React.useMemo(() => accounts.find(({ id }) => id === accountId), [accounts, accountId]);

    const [account, setAccount] = React.useState<TezosAccount | undefined>(encryptedAccount);

    const handleRpcChange = React.useCallback(
        async (rpc: string) => {
            if (account) {
                updateAccountPreferredRpc(account.id, rpc);
            }
            dispatch(updateNetworkInfo({ rpc }));
            try {
                const network = RPC.Nodes[rpc] || (await getRpcNetwork(rpc));
                dispatch(updateNetworkInfo({ rpc, network }));
            } catch (e) {
                Logger.debug(e);
            }
        },
        [account, dispatch, updateAccountPreferredRpc],
    );

    const onRpcChange = async (event: React.ChangeEvent<{ value: string }>) => {
        handleRpcChange(event.target.value);
    };

    if (
        encryptedAccount &&
        account &&
        (account.name !== encryptedAccount.name || account.preferredRpc !== encryptedAccount.preferredRpc)
    ) {
        setAccount({
            ...account,
            name: encryptedAccount.name,
            preferredRpc: encryptedAccount.preferredRpc,
        });
    }

    React.useEffect(() => {
        if (account && account?.preferredRpc && account?.preferredRpc !== networkInfo.rpc) {
            handleRpcChange(account.preferredRpc);
        }
    }, [account, handleRpcChange, networkInfo.rpc]);

    const handleUpdateName = React.useCallback(
        (name: string) => {
            if (account && account.id) {
                updateAccountName(account.id, name);
            }
        },
        [account, updateAccountName],
    );

    const fetchInformation = React.useCallback(async () => {
        if (account?.privateKey) {
            try {
                await WalletService.SMARTPY_SECRET_KEY.import(networkInfo.rpc, { secretKey: account.privateKey });
                const accountInfo = await WalletService.SMARTPY_SECRET_KEY.getInformation();
                dispatch(updateAccountInfo(accountInfo));
                fetchContractOperations(account.address, network).then(setOperations);
            } catch (e) {
                Logger.debug(e);
            }
        }
    }, [account, dispatch, network, networkInfo.rpc]);

    React.useEffect(() => {
        fetchInformation();
    }, [fetchInformation]);

    const handleNetworkSelection = (event: SelectChangeEvent<string>) => {
        const _network = event.target.value as Network;
        const rpc = RPC.smartpy[_network];
        if (rpc) {
            if (account) {
                updateAccountPreferredRpc(account.id, rpc);
            }
            dispatch(
                updateNetworkInfo({
                    rpc: rpc,
                    network: _network,
                }),
            );
        }
    };

    return (
        <Container>
            <TopDiv>
                <RouterFab color="primary" to="/wallet/accounts">
                    <ArrowBackOutlinedIcon />
                    {t('wallet.labels.goBackToAccounts')}
                </RouterFab>
                {account?.privateKey && (
                    <Box sx={{ display: 'flex' }}>
                        <TextField size="small" value={networkInfo?.rpc} onChange={onRpcChange}></TextField>
                        <Divider sx={{ margin: 1 }} />
                        <FormControl size="small" sx={{ width: 200 }}>
                            <InputLabel id="network">{t('wallet.labels.network')}</InputLabel>
                            <Select
                                name="network"
                                labelId="network"
                                label={t('wallet.labels.network')}
                                value={network || Network.MAINNET}
                                onChange={handleNetworkSelection}
                            >
                                {(Object.values(Network) || []).map((_network) => {
                                    return (
                                        <MenuItem
                                            disabled={_network === Network.CUSTOM}
                                            value={_network}
                                            key={_network}
                                        >
                                            {_network}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                    </Box>
                )}
            </TopDiv>
            {account ? (
                <DecryptAccount account={account} setAccount={setAccount}>
                    <AccountView
                        operations={operations}
                        network={network}
                        updateName={handleUpdateName}
                        account={account}
                    />
                </DecryptAccount>
            ) : (
                <>
                    <Alert severity="error">Account not found</Alert>
                    <NotFound />
                </>
            )}
        </Container>
    );
};

export default Account;
