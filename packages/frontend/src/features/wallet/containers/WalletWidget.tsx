import React from 'react';
import { useDispatch } from 'react-redux';

import { WalletStatus } from 'SmartPyWalletTypes';
import logger from '../../../services/logger';
import { getRpcNetwork } from '../../../utils/tezosRpc';
import { clearAccountInfo, loadingAccountInfo, updateAccountInfo } from '../actions';
import { AccountSource } from '../constants/sources';
import WalletServices from '../services';
import WalletWidget from '../views/WalletWidget';

interface OwnProps {
    disableNetworkSelection: boolean;
    rpc: string;
}

const WalletWidgetContainer: React.FC<OwnProps> = ({ rpc, disableNetworkSelection }) => {
    const isMounted = React.useRef(false);
    const [walletStatus, setWalletStatus] = React.useState<WalletStatus>({ temple: false });
    const dispatch = useDispatch();

    const onTempleClick = React.useCallback(async () => {
        // Reset Account Information
        dispatch(clearAccountInfo());

        dispatch(loadingAccountInfo(true));
        try {
            const network = await getRpcNetwork(rpc);
            if (network) {
                await WalletServices[AccountSource.TEMPLE].connect(
                    { name: network.toLowerCase(), rpc },
                    { forcePermission: true },
                );
                const accountInformation = await WalletServices[AccountSource.TEMPLE].getInformation();
                dispatch(updateAccountInfo(accountInformation));
            }
        } catch (e: any) {
            logger.error(e);
            dispatch(
                updateAccountInfo({
                    errors: e.message,
                }),
            );
        } finally {
            dispatch(loadingAccountInfo(false));
        }
    }, [dispatch, rpc]);

    const onBeaconClick = React.useCallback(async () => {
        // Reset Account Information
        dispatch(clearAccountInfo());

        dispatch(loadingAccountInfo(true));
        try {
            const network = await getRpcNetwork(rpc);
            if (network) {
                await WalletServices[AccountSource.BEACON].connect({ name: network.toLowerCase(), rpc });
                const accountInformation = await WalletServices[AccountSource.BEACON].getInformation();
                dispatch(updateAccountInfo(accountInformation));
            }
        } catch (e: any) {
            logger.error(e);
            dispatch(
                updateAccountInfo({
                    errors: e.message,
                }),
            );
        } finally {
            dispatch(loadingAccountInfo(false));
        }
    }, [dispatch, rpc]);

    const getWalletStatus = async () => {
        let temple = false;

        try {
            temple = await WalletServices[AccountSource.TEMPLE].isAvailable();
        } catch (e) {
            logger.trace(e);
        }

        if (isMounted.current) {
            setWalletStatus({
                temple,
            });
        }
    };

    React.useEffect(() => {
        isMounted.current = true;
        getWalletStatus();
        return () => {
            isMounted.current = false;
        };
    }, []);

    return (
        <WalletWidget
            walletStatus={walletStatus}
            onTempleClick={onTempleClick}
            onBeaconClick={onBeaconClick}
            disableNetworkSelection={disableNetworkSelection}
        />
    );
};

export default WalletWidgetContainer;
