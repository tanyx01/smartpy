import { createContext } from 'react';

import { OriginatedContract } from 'smartpy-wallet-storage/dist/types/services/contract/storage';
import { FaucetAccount } from 'smartpy-wallet-storage/dist/types/services/faucet/storage';

import { TezosAccount } from 'smartpy-wallet-storage';

export interface IWalletContext {
    loading: boolean;
    contracts: OriginatedContract[];
    faucetAccounts: FaucetAccount[];
    accounts: TezosAccount[];
    removeContracts: (ids: string[]) => void;
    removeFaucetAccounts: (ids: string[]) => void;
    removeAccounts: (ids: string[]) => void;
    addContract: (contract: { name: string; address: string }) => void;
    addFaucetAccount: (contract: FaucetAccount) => void;
    addAccount: (account: Omit<TezosAccount, 'id'>, password?: string) => void;
    getDecryptedAccount: (account: TezosAccount, password: string) => Promise<TezosAccount>;
    updateContract: (contract: OriginatedContract) => void;
    updateFaucetAccount: (contract: FaucetAccount) => void;
    updateAccountName: (id: string, name: string) => void;
    updateAccountPreferredRpc: (id: string, preferredRpc: string) => void;
    resetAccounts: () => void;
}

const contextStub = {
    loading: true,
    contracts: [],
    faucetAccounts: [],
    accounts: [],
    removeContracts: () => null,
    removeFaucetAccounts: () => null,
    removeAccounts: () => null,
    addContract: () => null,
    addFaucetAccount: () => null,
    addAccount: () => null,
    getDecryptedAccount: () => Promise.reject(),
    updateContract: () => null,
    updateFaucetAccount: () => null,
    updateAccountName: () => null,
    updateAccountPreferredRpc: () => null,
    resetAccounts: () => null,
};

const WalletContext = createContext<IWalletContext>(contextStub);

export default WalletContext;
