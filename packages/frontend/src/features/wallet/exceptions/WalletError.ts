import TempleErrorCodes from './TempleErrorCodes';
import BeaconErrorCodes from './BeaconErrorCodes';
import LedgerErrorCodes from './LedgerErrorCodes';
import SecretKeyErrorCodes from './SecretKeyErrorCodes';

export enum WalletGenericErrorCodes {
    NOT_ABLE_TO_ESTIMATE_OPERATION = 'wallet.generic.not_able_to_estimate_operation',
    NOT_ABLE_TO_GET_SOURCE = 'wallet.generic.not_able_to_get_source',
    NOT_ABLE_TO_GET_RPC = 'wallet.generic.not_able_to_get_rpc',
    NOT_ABLE_TO_ORIGINATE_CONTRACT = 'wallet.generic.not_able_to_originate_contract',
    NOT_ABLE_TO_CALL_CONTRACT = 'wallet.generic.not_able_to_call_contract',
    NOT_ABLE_TO_PREPARE_ORIGINATION = 'wallet.generic.not_able_to_prepare_origination',
    NOT_ABLE_TO_GET_BALANCE = 'wallet.generic.not_able_to_balance',
    NOT_ABLE_TO_REVEAL_ACCOUNT = 'wallet.generic.not_able_to_reveal_account',
    NOT_ABLE_TO_PREPARE_TRANSACTION = 'wallet.generic.not_able_to_prepare_transaction',
}

export type WalletErrorCodes =
    | WalletGenericErrorCodes
    | TempleErrorCodes
    | BeaconErrorCodes
    | LedgerErrorCodes
    | SecretKeyErrorCodes;

/**
 * @class Generic Wallet Error
 *
 * @param {WalletErrorCodes} message Error code or message
 */
export default class WalletError extends Error {
    constructor(message: WalletErrorCodes) {
        super(message); // 'Error' breaks prototype chain here

        Object.setPrototypeOf(this, new.target.prototype); // Restore prototype chain
    }
}
