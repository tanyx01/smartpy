import React from 'react';

import {
    styled,
    Typography,
    Divider,
    Paper,
    AppBar,
    Tabs,
    Tab,
    TextField,
    Tooltip,
    OutlinedInput,
    InputAdornment,
    IconButton,
    FormControl,
    InputLabel,
} from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import PlayArrowOutlinedIcon from '@mui/icons-material/PlayArrowOutlined';
import ListAltOutlinedIcon from '@mui/icons-material/ListAltOutlined';
import InfoIcon from '@mui/icons-material/Info';

import Fab from 'src/features/common/elements/Fab';
import { Operation } from 'src/utils/tzkt';
import { Network } from 'src/constants/networks';
import * as Explorer from 'src/constants/explorer';
import TabPanel from 'src/features/common/elements/TabPanel';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import AccountInfo from '../components/AccountInfo';
import TransactionView from './Transaction';
import Operations from 'src/features/common/components/Operations';
import { TezosAccount } from 'smartpy-wallet-storage';

const NameDiv = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: 300,
}));

const TopButtons = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    margin: 20,
    [theme.breakpoints.down('sm')]: {
        display: 'flex',
        flexWrap: 'wrap',
    },
}));

const FabButton = styled(Fab)<{ target?: string }>(({ theme }) => ({
    margin: 5,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: theme.palette.primary.main,
    [theme.breakpoints.down('sm')]: {
        flexGrow: 1,
    },
}));

interface AccountViewProps {
    network: Network;
    operations: Operation[];
    updateName: (name: string) => void;
    account: TezosAccount;
}

const AccountView: React.FC<AccountViewProps> = ({ network, account, operations, updateName }) => {
    const [tab, setTab] = React.useState(0);
    const [showSecret, setShowSecret] = React.useState(false);
    const [editName, setEditName] = React.useState(false);
    const t = useTranslation();

    const handleTabChange = (_: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setTab(newValue);
    };

    const handleNameUpdate = (e: React.FocusEvent<HTMLInputElement>) => {
        updateName(e.target.value);
        setEditName(false);
    };

    const handleKeyDownCapture = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            updateName((e.target as any).value);
            setEditName(false);
        }
    };

    const handleClickShowSecret = () => {
        setShowSecret(!showSecret);
    };

    const handleMouseDownShow = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    return (
        <>
            <NameDiv>
                {editName ? (
                    <TextField
                        fullWidth
                        autoFocus={editName}
                        defaultValue={account.name}
                        onBlur={handleNameUpdate}
                        onKeyDownCapture={handleKeyDownCapture}
                        inputProps={{
                            autoComplete: 'off',
                        }}
                        size="small"
                    />
                ) : (
                    <Tooltip title={t('common.edit') as string} placement="top">
                        <>
                            <Typography
                                variant="caption"
                                textAlign="center"
                                sx={{
                                    fontSize: '1em',
                                    padding: 1,
                                    borderRadius: 3,
                                    border: '2px solid',
                                    borderColor: 'transparent',
                                    ':hover': { borderColor: 'primary.dark' },
                                }}
                                onClick={() => setEditName(true)}
                            >
                                {account.name}
                            </Typography>
                            <EditOutlinedIcon fontSize="small" onClick={() => setEditName(true)} />
                        </>
                    </Tooltip>
                )}
            </NameDiv>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <AccountInfo disableNetworkSelection skipTitles />
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <TopButtons>
                <FabButton
                    variant="extended"
                    sx={{ borderColor: 'warning.light' }}
                    color="secondary"
                    aria-label={t('wallet.exploreWith.tzkt')}
                    href={`${Explorer.tzkt[network]}/${account.address}`}
                    target="_blank"
                >
                    {t('wallet.exploreWith.tzkt')}
                </FabButton>
                <FabButton
                    variant="extended"
                    color="secondary"
                    aria-label={t('wallet.exploreWith.tzstats')}
                    href={`${Explorer.tzstats[network]}/${account.address}`}
                    target="_blank"
                >
                    {t('wallet.exploreWith.tzstats')}
                </FabButton>
            </TopButtons>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <Paper sx={{ flexGrow: 1 }}>
                <AppBar position="static" color="default">
                    <Tabs value={tab} onChange={handleTabChange} aria-label="Faucet Menu" variant="fullWidth">
                        <Tab label={t('wallet.labels.operations')} icon={<ListAltOutlinedIcon />} />
                        <Tab label={t('wallet.labels.interact')} icon={<PlayArrowOutlinedIcon />} />
                        <Tab label={t('wallet.labels.accountInfo')} icon={<InfoIcon />} />
                    </Tabs>
                </AppBar>

                <TabPanel value={tab} index={0}>
                    <Operations operations={operations} account={account} />
                </TabPanel>
                <TabPanel value={tab} index={1}>
                    <TransactionView />
                </TabPanel>
                <TabPanel value={tab} index={2}>
                    <Paper sx={{ padding: 4 }}>
                        <FormControl fullWidth variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-public">{t('wallet.labels.publicKey')}</InputLabel>
                            <OutlinedInput
                                disabled
                                autoComplete="off"
                                id="outlined-adornment-public"
                                label={t('wallet.labels.publicKey')}
                                value={account.publicKey}
                            />
                        </FormControl>
                        <FormControl fullWidth sx={{ marginTop: 2 }} variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-secret">{t('wallet.labels.privateKey')}</InputLabel>
                            <OutlinedInput
                                disabled
                                autoComplete="off"
                                id="outlined-adornment-secret"
                                type={showSecret ? 'text' : 'password'}
                                label={t('wallet.labels.privateKey')}
                                value={account.privateKey}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowSecret}
                                            onMouseDown={handleMouseDownShow}
                                            edge="end"
                                        >
                                            {showSecret ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Paper>
                </TabPanel>
            </Paper>
        </>
    );
};

export default AccountView;
