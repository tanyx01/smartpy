import React from 'react';

// Material UI
import { styled, Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Dialog from '@mui/material/Dialog';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import AppBar from '@mui/material/AppBar';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import CancelIcon from '@mui/icons-material/Cancel';
import Slide from '@mui/material/Slide';

// Utils
import { prettifyJsonString } from '../../../utils/json';
// Components
import CodeBlock from '../../common/components/CodeBlock';
// State Management
import useTranslation from '../../i18n/hooks/useTranslation';

import { OperationInformation } from 'SmartPyWalletTypes';
import TezosWalletIcon from '../../common/elements/icons/TezosWallet';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: theme.spacing(2, 0, 2, 0),
            overflowY: 'auto',
        },
        tabContent: {
            padding: 20,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        spacer: {
            margin: theme.spacing(2),
        },
        fullHeight: {
            height: '100%',
        },
        redButton: {
            backgroundColor: theme.palette.error.main,
        },
    }),
);

const WalletIcon = styled(TezosWalletIcon)(({ theme }) => ({
    fill: theme.palette.primary.contrastText,
    width: 24,
    height: 24,
}));

const Transition = React.forwardRef(function Transition(
    props: { children: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="down" ref={ref} {...props} />;
});

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
    className: string;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && children}
        </div>
    );
}

interface OwnProps {
    operationInformation?: OperationInformation;
    cancel: () => void;
    accept: () => void;
}

const OperationInformationDialog: React.FC<OwnProps> = ({ operationInformation, cancel, accept }) => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const t = useTranslation();

    const handleChange = (_: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setValue(newValue);
    };

    const open = React.useMemo(() => !!operationInformation?.bytes, [operationInformation]);

    return (
        <Dialog maxWidth="md" fullWidth onClose={cancel} open={open} TransitionComponent={Transition}>
            <DialogTitle id="operation-information-title">{t('wallet.operation.title')}</DialogTitle>
            <DialogContent dividers>
                <AppBar position="static" color="default">
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="Operation Information Dialog"
                    >
                        <Tab label={t('wallet.operation.hashes')} key={0} />
                        {(operationInformation?.operationContents as any)?.script ? (
                            <Tab label={t('wallet.operation.jsonEncodingNoScript')} key={1} />
                        ) : undefined}
                        <Tab
                            label={t('wallet.operation.jsonEncoding')}
                            key={(operationInformation?.operationContents as any)?.script ? 2 : 1}
                        />
                        <Tab
                            label={t('wallet.operation.bytesEncoding')}
                            key={(operationInformation?.operationContents as any)?.script ? 3 : 2}
                        />
                    </Tabs>
                </AppBar>

                <TabPanel value={value} index={0} className={classes.root}>
                    <Paper className={classes.tabContent}>
                        <TextField
                            variant="outlined"
                            disabled
                            fullWidth
                            label={t('wallet.operation.blake2BHash')}
                            value={operationInformation?.blake2bHash || ''}
                        />
                        <Divider className={classes.spacer} />
                        <TextField
                            variant="outlined"
                            disabled
                            fullWidth
                            label={t('wallet.operation.blockHash')}
                            value={operationInformation?.blockHash || ''}
                        />
                    </Paper>
                </TabPanel>
                {(operationInformation?.operationContents as any)?.script ? (
                    <TabPanel value={value} index={1} className={classes.root}>
                        <CodeBlock
                            language="json"
                            showLineNumbers
                            text={
                                prettifyJsonString(
                                    JSON.stringify({
                                        ...(operationInformation?.operationContents || {}),
                                        script: undefined,
                                    }),
                                    4,
                                ) || ''
                            }
                        />
                    </TabPanel>
                ) : undefined}
                <TabPanel
                    value={value}
                    index={(operationInformation?.operationContents as any)?.script ? 2 : 1}
                    className={classes.root}
                >
                    <CodeBlock
                        language="json"
                        showLineNumbers
                        text={
                            prettifyJsonString(JSON.stringify(operationInformation?.operationContents || {}), 4) || ''
                        }
                    />
                </TabPanel>
                <TabPanel
                    value={value}
                    index={(operationInformation?.operationContents as any)?.script ? 3 : 2}
                    className={classes.root}
                >
                    <Paper className={classes.tabContent}>
                        <TextField
                            variant="outlined"
                            disabled
                            fullWidth
                            multiline
                            minRows={15}
                            label={t('wallet.operation.bytesEncoding')}
                            value={operationInformation?.bytes || ''}
                        />
                    </Paper>
                </TabPanel>
            </DialogContent>
            <DialogActions>
                <Button onClick={cancel} variant="contained" className={classes.redButton} endIcon={<CancelIcon />}>
                    {t('common.cancel')}
                </Button>
                <Button autoFocus onClick={accept} variant="contained" endIcon={<WalletIcon />}>
                    {t('common.accept')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default OperationInformationDialog;
