import React from 'react';

import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';

import { WalletStatus } from 'SmartPyWalletTypes';
import Typography from '@mui/material/Typography';
import WalletSectionPopup from './WalletSelectionPopup';

import AccountInfo from '../components/AccountInfo';
import { useAccountInfo } from '../selectors/account';
import useTranslation from '../../i18n/hooks/useTranslation';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        button: {
            borderWidth: 2,
            backgroundColor: theme.palette.background.default,
        },
        leftContainer: {
            display: 'flex',
            alignItems: 'flex-start',
            flexDirection: 'column',
            padding: 10,
        },
    }),
);

interface OwnProps {
    disableNetworkSelection: boolean;
    walletStatus: WalletStatus;
    onTempleClick: () => void;
    onBeaconClick: () => void;
}

const WalletWidget: React.FC<OwnProps> = ({ walletStatus, onTempleClick, onBeaconClick, disableNetworkSelection }) => {
    const classes = useStyles();
    const accountInformation = useAccountInfo();
    const t = useTranslation();

    const buttonLabel = accountInformation.pkh ? t('origination.switchAccount') : t('origination.selectAccount');

    return (
        <div className={classes.leftContainer}>
            {accountInformation.pkh && <Typography variant="h6">{t('origination.accountLoaded')} </Typography>}
            <AccountInfo disableNetworkSelection skipTitles />
            <WalletSectionPopup
                variant="outlined"
                className={classes.button}
                onBeaconClick={onBeaconClick}
                onTempleClick={onTempleClick}
                label={buttonLabel}
                walletStatus={walletStatus}
                disableNetworkSelection={disableNetworkSelection}
            />
        </div>
    );
};

export default WalletWidget;
