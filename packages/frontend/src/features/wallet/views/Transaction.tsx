import React from 'react';

import {
    Alert,
    Box,
    Chip,
    Divider,
    FormControl,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    Paper,
    Rating,
    Typography,
} from '@mui/material';
import ConfirmationIcon from '@mui/icons-material/VerifiedUser';

import TextField from 'src/features/common/elements/TextField';
import { AmountUnit, convertUnitNumber, TezUnit } from 'src/utils/units';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import Button from 'src/features/common/elements/Button';
import WalletService from '../services';
import Logger from 'src/services/logger';
import CircularProgressWithText from 'src/features/loader/components/CircularProgressWithText';
import OperationInformationDialog from './OperationInformation';
import { OperationInformation } from 'SmartPyWalletTypes';
import { Subscription } from 'rxjs';
import { TezosConstants } from 'src/services/conseil';

export enum TransactionFields {
    AMOUNT = 'amount',
    FEE = 'fee',
    GAS_LIMIT = 'gas_limit',
    STORAGE_LIMIT = 'storage_limit',
    DESTINATION = 'destination',
}

interface ResultProps {
    t: (key: string) => string;
    submitting: boolean;
    result?: { hash: string; confirmations: number };
}

const Result = ({ t, submitting, result }: ResultProps) => {
    if (submitting) {
        return (
            <Paper>
                <CircularProgressWithText size={64} margin={40} />
            </Paper>
        );
    }
    if (result) {
        return (
            <Paper
                sx={{
                    borderWidth: 2,
                    borderStyle: 'solid',
                    borderColor: 'success.dark',
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 2,
                }}
            >
                <Chip
                    label={
                        <Typography variant="caption" textAlign="center">
                            {result?.hash}
                        </Typography>
                    }
                />
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', padding: 2 }}>
                    <Typography variant="caption" sx={{ marginRight: 2 }}>
                        {t('common.blockConfirmations')}
                    </Typography>
                    <Rating
                        value={result?.confirmations || 0}
                        max={10}
                        readOnly
                        emptyIcon={<ConfirmationIcon fontSize="small" />}
                        icon={<ConfirmationIcon fontSize="small" />}
                    />
                </Box>
            </Paper>
        );
    }
    return null;
};

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface OwnProps {}

const TransactionView: React.FC<OwnProps> = () => {
    const t = useTranslation();
    const [submitting, setSubmitting] = React.useState(false);
    const [operationInformation, setOperationInformation] = React.useState<OperationInformation>();
    const subscription = React.useRef<Subscription>();
    const [error, setError] = React.useState('');
    const [operationResult, setOperationResult] = React.useState<{ hash: string; confirmations: number }>();
    const [parameters, setParameters] = React.useState<Record<string, string>>({
        [TransactionFields.AMOUNT]: '0',
    });

    React.useEffect(
        () => () => {
            // Unsubscribe on component dismount
            subscription.current?.unsubscribe();
        },
        [],
    );

    const acceptOperationInformation = React.useCallback(async () => {
        // Reset operation information
        cleanOperationInformation();

        setSubmitting(true);

        try {
            subscription.current?.unsubscribe();
            const operation = await operationInformation?.send();

            setOperationResult({
                hash: operation?.hash || '',
                confirmations: 0,
            });

            subscription.current = operation?.monitor?.subscribe((confirmations) => {
                setOperationResult((s) => (s ? { ...s, confirmations } : undefined));
            });
        } catch (e: any) {
            setError(e?.message);
            Logger.debug(e);
        }

        setSubmitting(false);
    }, [operationInformation]);

    const handleParameterInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        setError('');
        setParameters((parameters) => ({ ...parameters, [event.target.id]: event.target.value }));
    };

    const sendTransaction = async () => {
        setSubmitting(true);
        setError('');

        try {
            if (!parameters[TransactionFields.DESTINATION]) {
                throw new Error(t('wallet.errors.missingField.destination'));
            }
            const destination = parameters[TransactionFields.DESTINATION];
            const amount = String(convertUnitNumber(parameters[TransactionFields.AMOUNT], AmountUnit.tez));
            const result = await WalletService.SMARTPY_SECRET_KEY.prepareTransfer({
                destination,
                amount,
                fee: String(TezosConstants.DefaultSimpleTransactionFee),
                storage_limit: String(TezosConstants.DefaultTransactionStorageLimit),
                gas_limit: String(TezosConstants.DefaultTransactionGasLimit),
            });
            setOperationInformation(result);
        } catch (e: any) {
            setError(e?.message);
            Logger.debug(e);
        }

        setSubmitting(false);
    };

    const cleanOperationInformation = () => {
        setSubmitting(false);
        setError('');
        setOperationInformation(undefined);
        setOperationResult(undefined);
    };

    return (
        <>
            <Paper sx={{ padding: 4 }}>
                {error ? <Alert severity="error">{error}</Alert> : null}
                <Result t={t} result={operationResult} submitting={submitting} />
                <Divider />
                <TextField
                    inputProps={{
                        autoComplete: 'off',
                    }}
                    onChange={handleParameterInput}
                    fullWidth
                    name={TransactionFields.DESTINATION}
                    id={TransactionFields.DESTINATION}
                    required
                    error={String(parameters[TransactionFields.DESTINATION]) === ''}
                    label={t('wallet.parameterLabels.destination')}
                    margin="normal"
                    variant="outlined"
                    value={parameters[TransactionFields.DESTINATION]}
                />
                <FormControl
                    fullWidth
                    variant="outlined"
                    margin="normal"
                    required
                    error={String(parameters[TransactionFields.AMOUNT]) === ''}
                >
                    <InputLabel htmlFor={TransactionFields.AMOUNT}>{t('wallet.parameterLabels.amount')}</InputLabel>
                    <OutlinedInput
                        name={TransactionFields.AMOUNT}
                        onChange={handleParameterInput}
                        id={TransactionFields.AMOUNT}
                        fullWidth
                        type="number"
                        inputProps={{
                            min: 0,
                            step: 1,
                        }}
                        label={t('wallet.parameterLabels.amount')}
                        startAdornment={
                            <InputAdornment position="start">{AmountUnit[TezUnit.tez].char}</InputAdornment>
                        }
                        value={parameters[TransactionFields.AMOUNT]}
                    />
                </FormControl>
                <Divider sx={{ marginTop: 1, marginBottom: 2 }} />
                <Box sx={{ display: 'flex', justifyContent: 'end' }}>
                    <Button onClick={sendTransaction}>{t('wallet.labels.sendTransaction')}</Button>
                </Box>
            </Paper>

            <OperationInformationDialog
                operationInformation={operationInformation}
                accept={acceptOperationInformation}
                cancel={cleanOperationInformation}
            />
        </>
    );
};

export default TransactionView;
