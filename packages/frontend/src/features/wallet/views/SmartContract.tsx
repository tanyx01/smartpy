import React from 'react';

import { styled, Container as MuiContainer, Typography, Divider, Tooltip } from '@mui/material';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';

import useTranslation from 'src/features/i18n/hooks/useTranslation';
import { useParams } from 'react-router-dom';
import { getBase } from 'src/utils/url';
import Fab from 'src/features/common/elements/Fab';
import { fetchContractOperations, Operation } from 'src/utils/tzkt';
import RouterFab from 'src/features/navigation/elements/RouterFab';
import useWalletContext from '../hooks/useWalletContext';
import TextField from 'src/features/common/elements/TextField';
import Operations from 'src/features/common/components/Operations';

const Container = styled(MuiContainer)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'center',
    height: 780,
    padding: 20,
}));

const NameDiv = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: 300,
}));

const TopButtons = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    margin: 20,
    [theme.breakpoints.down('sm')]: {
        display: 'flex',
        flexWrap: 'wrap',
    },
}));

const FabButton = styled(Fab)<{ target?: string }>(({ theme }) => ({
    margin: 5,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: theme.palette.primary.main,
    [theme.breakpoints.down('sm')]: {
        flexGrow: 1,
    },
}));

const SmartContractView = () => {
    const isMounted = React.useRef(false);
    const { address } = useParams<{ address: string }>();
    const [operations, setOperations] = React.useState<Operation[]>([]);
    const t = useTranslation();
    const { updateContract, contracts } = useWalletContext();
    const [editName, setEditName] = React.useState(false);

    const contract = React.useMemo(() => contracts.find((c) => c.address === address), [address, contracts]);
    React.useEffect(() => {
        isMounted.current = true;
        fetchContractOperations(address).then((ops) => {
            if (isMounted.current) {
                setOperations(ops);
            }
        });
        return () => {
            isMounted.current = false;
        };
    }, [address]);

    const handleNameUpdate = (e: React.FocusEvent<HTMLInputElement>) => {
        if (contract) {
            updateContract({
                ...contract,
                name: e.target.value,
            });
            setEditName(false);
        }
    };

    const handleKeyDownCapture = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter' && contract) {
            updateContract({
                ...contract,
                name: (e.target as any).value,
            });
            setEditName(false);
        }
    };

    return (
        <Container>
            <div>
                <RouterFab
                    color="primary"
                    aria-label="Smart Contracts"
                    to="/wallet/smart-contracts"
                    sx={{ marginBottom: 3 }}
                >
                    <ArrowBackOutlinedIcon />
                    {t('wallet.contractView.goBack')}
                </RouterFab>
            </div>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <NameDiv>
                {editName ? (
                    <TextField
                        fullWidth
                        autoFocus={editName}
                        defaultValue={contract?.name || ''}
                        onBlur={handleNameUpdate}
                        onKeyDownCapture={handleKeyDownCapture}
                        inputProps={{
                            autoComplete: 'off',
                        }}
                        size="small"
                    />
                ) : (
                    <Tooltip title={t('common.edit') as string} placement="top">
                        <>
                            <Typography
                                variant="caption"
                                textAlign="center"
                                sx={{
                                    fontSize: '1em',
                                    padding: 1,
                                    borderRadius: 3,
                                    border: '2px solid',
                                    borderColor: 'transparent',
                                    ':hover': { borderColor: 'primary.dark' },
                                }}
                                onClick={() => setEditName(true)}
                            >
                                {contract?.name || ''}
                            </Typography>
                            <EditOutlinedIcon fontSize="small" />
                        </>
                    </Tooltip>
                )}
            </NameDiv>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <TopButtons>
                <FabButton
                    variant="extended"
                    color="secondary"
                    aria-label={t('wallet.exploreWith.smartpy')}
                    href={`${getBase()}/explorer?address=${address}`}
                    target="_blank"
                >
                    {t('wallet.exploreWith.smartpy')}
                </FabButton>
                <FabButton
                    variant="extended"
                    color="secondary"
                    sx={{ borderColor: '#abda82' }}
                    aria-label={t('wallet.exploreWith.betterCallDev')}
                    href={`https://better-call.dev/search?text=${address}`}
                    target="_blank"
                >
                    {t('wallet.exploreWith.betterCallDev')}
                </FabButton>
            </TopButtons>
            <Typography variant="overline">{t('wallet.labels.operations')}</Typography>
            <Divider sx={{ marginTop: 1, marginBottom: 1 }} />
            <Operations operations={operations} />
        </Container>
    );
};

export default SmartContractView;
