import React from 'react';
import { useDispatch } from 'react-redux';

import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Dialog from '@mui/material/Dialog';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import AppBar from '@mui/material/AppBar';
import DialogActions from '@mui/material/DialogActions';
import Button, { ButtonProps } from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import KeyIcon from '@mui/icons-material/VpnKey';
import TezosWalletIcon from 'src/features/common/elements/icons/TezosWallet';
import Slide from '@mui/material/Slide';

import useTranslation from '../../i18n/hooks/useTranslation';
import { clearAccountInfo } from '../actions';
import LedgerIcon from '../../common/elements/icons/Ledger';
import LedgerForm from '../components/LedgerForm';
import AccountForm from '../components/AccountForm';
import SecretKeyForm from '../components/SecretKeyForm';
import { useAccountInfo } from '../selectors/account';
import BeaconFull from 'src/features/common/elements/icons/BeaconFull';
import TempleIcon from 'src/features/common/elements/icons/Temple';
import AccountInfo from '../components/AccountInfo';
import { WalletStatus } from 'SmartPyWalletTypes';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        panel: {
            padding: theme.spacing(2, 0, 2, 0),
            overflowY: 'auto',
        },
        fullHeight: {
            height: '100%',
        },
        marginBottom: {
            marginBottom: 10,
        },
        hidden: {
            display: 'none',
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        input: {
            padding: '10px 12px 8px',
        },
        spacer: {
            margin: theme.spacing(2),
        },
        section: {
            padding: 20,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        centralSection: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            backgroundColor: theme.palette.background.paper,
            padding: 10,
        },
    }),
);

const Transition = React.forwardRef(function Transition(
    props: { children: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
    className: string;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && children}
        </div>
    );
}

/**
 * @summary Accessibility props (a11y)
 */
const a11yProps = (index: number) => {
    return {
        id: `tab-${index}`,
        'aria-controls': `tabpanel-${index}`,
    };
};

interface OwnProps extends ButtonProps {
    disableNetworkSelection: boolean;
    walletStatus: WalletStatus;
    label: string;
    onBeaconClick: () => void;
    onTempleClick: () => void;
}

const WalletSectionPopup: React.FC<OwnProps> = ({
    disableNetworkSelection,
    onBeaconClick,
    onTempleClick,
    label,
    walletStatus,
    ...buttonProps
}) => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [open, setOpen] = React.useState(false);
    const t = useTranslation();
    const dispatch = useDispatch();
    const accountInformation = useAccountInfo();

    const handleOpen = () => {
        setOpen((isTrue) => {
            if (!isTrue) {
                // Reset Account Information
                dispatch(clearAccountInfo());
            }
            return !isTrue;
        });
    };

    const handleChange = (_: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setValue((state) => {
            if (state !== newValue) {
                dispatch(clearAccountInfo());
            }
            return newValue;
        });
    };

    const onTempleClickIfAvailable = () => {
        if (walletStatus.temple) {
            onTempleClick();
        }
    };

    return (
        <>
            <Button {...buttonProps} onClick={handleOpen} endIcon={<TezosWalletIcon height="0.875em" />}>
                {label}
            </Button>
            <Dialog maxWidth="md" fullWidth onClose={handleOpen} open={open} TransitionComponent={Transition}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        scrollButtons="auto"
                        aria-label="SmartPy Wallet Dialog"
                        variant="fullWidth"
                    >
                        <Tab {...a11yProps(0)} icon={<TezosWalletIcon width={24} />} label={t('Account')} />
                        <Tab {...a11yProps(1)} icon={<LedgerIcon />} label={t('Ledger')} />
                        <Tab {...a11yProps(2)} icon={<KeyIcon />} label={t('Secret Key')} />
                        <Tab
                            {...a11yProps(3)}
                            icon={<TempleIcon />}
                            label={walletStatus.temple ? '' : t('wallet.labels.notAvailable')}
                            onClick={onTempleClickIfAvailable}
                        />
                        <Tab {...a11yProps(3)} icon={<BeaconFull />} onClick={onBeaconClick} />
                    </Tabs>
                </AppBar>

                <TabPanel value={value} className={classes.panel} index={0}>
                    <AccountForm disableNetworkSelection={disableNetworkSelection} />
                </TabPanel>
                <TabPanel value={value} className={classes.panel} index={1}>
                    <LedgerForm />
                </TabPanel>
                <TabPanel value={value} className={classes.panel} index={2}>
                    <SecretKeyForm disableNetworkSelection={disableNetworkSelection} />
                </TabPanel>
                <TabPanel value={value} className={classes.panel} index={3}>
                    {!accountInformation?.pkh &&
                        (!accountInformation?.isLoading ? (
                            walletStatus.temple ? (
                                <Typography style={{ textAlign: 'center' }}>
                                    Click on the Temple icon to select an account.
                                </Typography>
                            ) : (
                                <Typography style={{ textAlign: 'center' }}>
                                    Temple is not available. Make sure the browser plugin is installed and try to
                                    refresh the page.
                                </Typography>
                            )
                        ) : (
                            <Typography style={{ textAlign: 'center' }}>
                                A Temple window is opened. Choose your wallet on the window.
                            </Typography>
                        ))}
                    <AccountInfo skipTitles disableNetworkSelection={disableNetworkSelection} />
                </TabPanel>
                <TabPanel value={value} className={classes.panel} index={4}>
                    <AccountInfo skipTitles disableNetworkSelection={false} />
                    {!accountInformation?.pkh && (
                        <Typography style={{ textAlign: 'center' }}>
                            Click on the Beacon icon to select an account.
                        </Typography>
                    )}
                </TabPanel>

                <Divider />
                <DialogActions>
                    {accountInformation?.pkh ? (
                        <Button autoFocus color="primary" onClick={handleOpen}>
                            {t('common.validate')}
                        </Button>
                    ) : (
                        <Button autoFocus color="primary" onClick={handleOpen}>
                            {t('common.cancel')}
                        </Button>
                    )}
                </DialogActions>
            </Dialog>
        </>
    );
};

export default WalletSectionPopup;
