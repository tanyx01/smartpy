import React from 'react';

import {
    styled,
    Container as MuiContainer,
    TableBody,
    TableCell,
    TableHead,
    Typography,
    Divider,
    TablePagination,
    TableSortLabel,
    Box,
    TableCellProps,
    Checkbox,
    Toolbar as MuiToolbar,
    alpha,
    Tooltip,
    IconButton,
    Popper,
    Stack,
    TextField,
    Paper,
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import FilterListIcon from '@mui/icons-material/FilterList';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import AddIcon from '@mui/icons-material/Add';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';

import useTranslation from 'src/features/i18n/hooks/useTranslation';
import Table from 'src/features/common/components/Table';
import TableRow from 'src/features/common/components/TableRow';
import useWalletContext from 'src/features/wallet/hooks/useWalletContext';
import { OriginatedContract } from 'smartpy-wallet-storage/dist/types/services/contract/storage';
import StringMatcher from 'src/utils/matchers/String';
import Fab from 'src/features/common/elements/Fab';
import { lookupContractNetwork } from 'src/utils/tezosRpc';
import { useSearchParams } from 'src/hooks/useSearchParams';
import RouterFab from 'src/features/navigation/elements/RouterFab';
import { Network } from 'src/constants/networks';
import AddContractDialog from '../components/AddContract';

enum Order {
    Asc = 'asc',
    Desc = 'desc',
}
enum Field {
    Name = 'name',
    Address = 'address',
    Network = 'network',
}

const sortByField = (contracts: OriginatedContract[], field: Field, order: Order) => {
    const _contracts = [...contracts];

    return _contracts.sort((v1, v2) => (order === Order.Desc ? 1 : -1) * StringMatcher.compare(v1[field], v2[field]));
};

const TopDiv = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-between',
}));

const Container = styled(MuiContainer)(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'center',
    height: 780,
    padding: 20,
}));

interface TableToolbarProps {
    t: (s: string) => string;
    numSelected: number;
    onFilter: (field: Field, value: string) => void;
    onDelete: () => void;
}

const TableToolbar = (props: TableToolbarProps) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const { t, numSelected, onFilter, onDelete } = props;

    const handleFilterClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(anchorEl ? null : event.currentTarget);
    };

    const handleFilterUpdate = (e: React.ChangeEvent<HTMLInputElement>) => {
        switch (e.target.name) {
            case Field.Name:
                return onFilter(Field.Name, e.target.value);
            case Field.Address:
                return onFilter(Field.Address, e.target.value);
            case Field.Network:
                return onFilter(Field.Network, e.target.value);
        }
    };

    return (
        <MuiToolbar
            disableGutters={true}
            style={{
                minHeight: 16,
            }}
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 },
                ...(numSelected > 0 && {
                    bgcolor: (theme) => alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
                }),
            }}
        >
            <div style={{ flex: '1 1 100%' }}>
                {numSelected > 0 ? (
                    <Typography sx={{ flex: '1 1 100%' }} color="inherit" variant="overline" component="div">
                        {numSelected} selected
                    </Typography>
                ) : null}
            </div>
            {numSelected > 0 ? (
                <Tooltip title="Delete">
                    <IconButton onClick={onDelete}>
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
            ) : (
                <Tooltip title="Filter list">
                    <IconButton onClick={handleFilterClick}>
                        <FilterListIcon />
                    </IconButton>
                </Tooltip>
            )}
            <Popper open={Boolean(anchorEl)} placement="left-end" disablePortal={false} anchorEl={anchorEl}>
                <Box component={Paper} sx={{ padding: 2 }}>
                    <Stack spacing={2}>
                        <Typography>Filters</Typography>
                        <Divider flexItem />
                        <TextField
                            name={Field.Name}
                            label={t(`wallet.contracts.${Field.Name}`)}
                            onChange={handleFilterUpdate}
                        />
                        <Divider flexItem />
                        <TextField
                            name={Field.Address}
                            label={t(`wallet.contracts.${Field.Address}`)}
                            onChange={handleFilterUpdate}
                        />
                        <Divider flexItem />
                        <TextField
                            name={Field.Network}
                            label={t(`wallet.contracts.${Field.Network}`)}
                            onChange={handleFilterUpdate}
                        />
                    </Stack>
                </Box>
            </Popper>
        </MuiToolbar>
    );
};

const SortedTableCell = ({
    order,
    isSorted,
    onSort,
    fieldName,
    ...props
}: {
    order: Order;
    isSorted: boolean;
    onSort: (field: Field) => void;
    fieldName: Field;
} & TableCellProps) => (
    <TableCell {...props}>
        <TableSortLabel active={isSorted} direction={isSorted ? order : Order.Asc} onClick={() => onSort(fieldName)}>
            <Typography variant="overline">{fieldName}</Typography>
        </TableSortLabel>
    </TableCell>
);

interface ContractRowProps {
    contract: OriginatedContract;
    checked: boolean;
    selectRow: (id: string) => void;
}

const ContractRow = ({ contract, checked, selectRow }: ContractRowProps) => {
    const isMounted = React.useRef(false);
    const [network, setNetwork] = React.useState(contract.network);

    React.useEffect(() => {
        isMounted.current = true;
        if ((!network || network === Network.CUSTOM) && contract.address) {
            lookupContractNetwork(contract.address).then(
                (network) => isMounted.current && network && setNetwork(network),
            );
        }
        return () => {
            isMounted.current = false;
        };
    }, [contract, network]);

    return (
        <TableRow key={contract.id}>
            <TableCell padding="checkbox">
                <Checkbox color="primary" checked={checked} onChange={() => selectRow(contract.id || '')} />
            </TableCell>
            <TableCell>{contract.name}</TableCell>
            <TableCell align="right">{contract.address}</TableCell>
            <TableCell align="right">{network}</TableCell>

            <TableCell align="right">
                <RouterFab size="small" to={`/wallet/smart-contracts/${contract.address}`}>
                    <SearchOutlinedIcon />
                    Open
                </RouterFab>
            </TableCell>
        </TableRow>
    );
};

const SmartContractsView = () => {
    const { addAddress } = useSearchParams<{ addAddress?: string }>();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [sortField, setSortField] = React.useState<Field>(Field.Name);
    const [order, setOrder] = React.useState<Order>(Order.Desc);
    const [selectedRows, setSelectedRows] = React.useState<string[]>([]);
    const [filters, setFilters] = React.useState<Record<string, string>>({});
    const [addingContract, setAddingContract] = React.useState(!!addAddress);
    const { contracts, removeContracts } = useWalletContext();
    const t = useTranslation();

    const onSort = React.useCallback(
        (field: Field) => {
            if (sortField === field) {
                setOrder(order === Order.Asc ? Order.Desc : Order.Asc);
            } else {
                setSortField(field);
                setOrder(Order.Asc);
            }
        },
        [order, sortField],
    );

    const filteredContracts = React.useMemo(() => {
        let _contracts = sortByField(contracts, sortField, order);

        _contracts = _contracts.filter((c) => {
            const keys = Object.keys(filters).filter((field) => !!filters[field]) as Field[];
            return keys.every((field) => (field in c && c[field]?.includes(filters[field])) || false);
        });
        _contracts = _contracts.slice(page * rowsPerPage, (page + 1) * rowsPerPage);

        return _contracts;
    }, [contracts, page, rowsPerPage, sortField, order, filters]);

    const onSelectAll = React.useCallback(() => {
        if (selectedRows.length) {
            setSelectedRows([]);
        } else {
            setSelectedRows(filteredContracts.map(({ id }) => id || ''));
        }
    }, [selectedRows, filteredContracts]);

    const selectRow = React.useCallback(
        (id: string) => {
            if (selectedRows.includes(id)) {
                setSelectedRows((selected) => selected.filter((s) => s !== id));
            } else {
                setSelectedRows((selected) => [...selected, id]);
            }
        },
        [selectedRows],
    );

    const handleDelete = React.useCallback(() => {
        removeContracts(selectedRows);
        setSelectedRows([]);
    }, [removeContracts, selectedRows]);

    const handleFilter = (field: Field, value: string) => {
        setFilters((filters) => ({ ...filters, [field]: value }));
    };

    return (
        <Container>
            <TopDiv>
                <RouterFab
                    color="primary"
                    aria-label={t('wallet.labels.goBackToMenu')}
                    to="/wallet"
                    sx={{ marginBottom: 3 }}
                >
                    <ArrowBackOutlinedIcon />
                    {t('wallet.labels.goBackToMenu')}
                </RouterFab>
                <Fab
                    color="secondary"
                    sx={{ borderWidth: 2, borderStyle: 'solid', borderColor: 'primary.main' }}
                    onClick={() => setAddingContract(true)}
                >
                    <AddIcon /> {t('wallet.labels.addContract')}
                </Fab>
            </TopDiv>
            <Typography textAlign="center" variant="overline">
                {t('wallet.labels.smartContracts')}
            </Typography>
            <Divider sx={{ margin: 1 }} />
            <Table
                sx={{ flexGrow: 1 }}
                header={
                    <TableHead>
                        <TableRow>
                            <TableCell colSpan={5}>
                                <TableToolbar
                                    t={t}
                                    numSelected={selectedRows.length}
                                    onFilter={handleFilter}
                                    onDelete={handleDelete}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell padding="checkbox">
                                <Checkbox
                                    color="primary"
                                    indeterminate={selectedRows.length > 0 && selectedRows.length < rowsPerPage}
                                    checked={rowsPerPage > 0 && selectedRows.length === rowsPerPage}
                                    onChange={onSelectAll}
                                    inputProps={{
                                        'aria-label': 'Select all contracts',
                                    }}
                                />
                            </TableCell>
                            <SortedTableCell
                                order={order}
                                fieldName={Field.Name}
                                onSort={onSort}
                                isSorted={sortField === Field.Name}
                            />
                            <SortedTableCell
                                align="right"
                                order={order}
                                fieldName={Field.Address}
                                onSort={onSort}
                                isSorted={sortField === Field.Address}
                            />
                            <SortedTableCell
                                align="right"
                                order={order}
                                fieldName={Field.Network}
                                onSort={onSort}
                                isSorted={sortField === Field.Network}
                            />
                            {/* Open button will appear in this cell */}
                            <TableCell align="right" />
                        </TableRow>
                    </TableHead>
                }
                body={
                    <TableBody>
                        {filteredContracts.map((contract) => (
                            <ContractRow
                                key={contract.id}
                                checked={selectedRows.includes(contract.id || '')}
                                contract={contract}
                                selectRow={selectRow}
                            />
                        ))}
                    </TableBody>
                }
                pagination={
                    <TablePagination
                        rowsPerPageOptions={[5, 10]}
                        component="div"
                        count={contracts.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={(_, page) => setPage(page)}
                        onRowsPerPageChange={(e) => setRowsPerPage(Number(e.target.value))}
                    />
                }
            />
            <AddContractDialog
                fullWidth
                open={addingContract}
                defaultAddress={addAddress}
                onClose={() => setAddingContract(false)}
                aria-labelledby="add-contract-dialog-title"
            />
        </Container>
    );
};

export default SmartContractsView;
