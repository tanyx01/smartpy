import React from 'react';

import useTranslation from 'src/features/i18n/hooks/useTranslation';

import Logger from 'src/services/logger';

import { TezosAccount } from 'smartpy-wallet-storage';
import useWalletContext from '../hooks/useWalletContext';

import { Alert, Button, InputLabel, Box, FormControl, OutlinedInput, InputAdornment, IconButton } from '@mui/material';

import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import LockOpenOutlined from '@mui/icons-material/LockOpenOutlined';

interface DecryptAccountProps {
    account: TezosAccount;
    children: JSX.Element;
    setAccount: (account: TezosAccount) => void;
}

const DecryptAccount: React.FC<DecryptAccountProps> = ({ account, children, setAccount }) => {
    const t = useTranslation();

    const [password, setPassword] = React.useState('');
    const [showPassword, setShowPassword] = React.useState(false);
    const [error, setError] = React.useState('');
    const { getDecryptedAccount } = useWalletContext();
    const handleUnlock = async () => {
        const encodedPassword = btoa(password);
        setPassword('');
        setError('');
        await getDecryptedAccount(account, encodedPassword)
            .then((decryptedAccount: TezosAccount) => setAccount(decryptedAccount))
            .catch((e: any) => {
                setError(e?.message);
                Logger.debug(e);
            });
    };
    const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(e.target.value);
    };

    const handleKeyDownCapture = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            handleUnlock();
        }
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownShow = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    if (account.privateKey) {
        return children;
    }
    return (
        <>
            {error && (
                <Alert severity="error" sx={{ marginBottom: 3 }}>
                    {error}
                </Alert>
            )}
            <InputLabel>
                Account &quot;
                <Box component="span" fontWeight="fontWeightMedium">
                    {account.name}
                </Box>
                &quot; is locked.
            </InputLabel>
            <FormControl sx={{ marginTop: 2 }} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-password">{t('common.password')}</InputLabel>
                <OutlinedInput
                    required
                    autoFocus
                    id="outlined-adornment-password"
                    type={showPassword ? 'text' : 'password'}
                    name="password"
                    label={t('common.password')}
                    onChange={handleTextChange}
                    onKeyDownCapture={handleKeyDownCapture}
                    value={password}
                    autoComplete="off"
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                                aria-label={t('common.aria.togglePasswordVisibility')}
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownShow}
                                edge="end"
                            >
                                {showPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                        </InputAdornment>
                    }
                />
            </FormControl>
            <Button color="primary" onClick={handleUnlock} endIcon={<LockOpenOutlined />}>
                {t('common.unlock')}
            </Button>
        </>
    );
};

export default DecryptAccount;
