import React from 'react';

import {
    TableBody,
    TableCell,
    TableHead,
    Typography,
    Divider,
    TablePagination,
    TableSortLabel,
    Box,
    TableCellProps,
    Checkbox,
    Toolbar as MuiToolbar,
    alpha,
    Tooltip,
    IconButton,
    Popper,
    Stack,
    TextField,
    Paper,
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import FilterListIcon from '@mui/icons-material/FilterList';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';

import useTranslation from 'src/features/i18n/hooks/useTranslation';
import Table from 'src/features/common/components/Table';
import TableRow from 'src/features/common/components/TableRow';
import StringMatcher from 'src/utils/matchers/String';
import RouterFab from 'src/features/navigation/elements/RouterFab';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

enum Order {
    Asc = 'asc',
    Desc = 'desc',
}
enum Field {
    Name = 'name',
    Address = 'address',
}

const sortByField = (
    contracts: { name: string; address: string; id?: string; encrypted: boolean }[],
    field: Field,
    order: Order,
) => {
    const _contracts = [...contracts];

    return _contracts.sort((v1, v2) => (order === Order.Desc ? 1 : -1) * StringMatcher.compare(v1[field], v2[field]));
};

interface TableToolbarProps {
    t: (s: string) => string;
    numSelected: number;
    onFilter: (field: Field, value: string) => void;
    onDelete: () => void;
}

const TableToolbar = (props: TableToolbarProps) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const { t, numSelected, onFilter, onDelete } = props;

    const handleFilterClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(anchorEl ? null : event.currentTarget);
    };

    const handleFilterUpdate = (e: React.ChangeEvent<HTMLInputElement>) => {
        switch (e.target.name) {
            case Field.Name:
                return onFilter(Field.Name, e.target.value);
            case Field.Address:
                return onFilter(Field.Address, e.target.value);
        }
    };

    return (
        <MuiToolbar
            disableGutters={true}
            style={{
                minHeight: 16,
            }}
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 },
                ...(numSelected > 0 && {
                    bgcolor: (theme) => alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
                }),
            }}
        >
            <div style={{ flex: '1 1 100%' }}>
                {numSelected > 0 ? (
                    <Typography sx={{ flex: '1 1 100%' }} color="inherit" variant="overline" component="div">
                        {numSelected} selected
                    </Typography>
                ) : null}
            </div>
            {numSelected > 0 ? (
                <Tooltip title="Delete">
                    <IconButton onClick={onDelete}>
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
            ) : (
                <Tooltip title="Filter list">
                    <IconButton onClick={handleFilterClick}>
                        <FilterListIcon />
                    </IconButton>
                </Tooltip>
            )}
            <Popper open={Boolean(anchorEl)} placement="left-end" disablePortal={false} anchorEl={anchorEl}>
                <Box component={Paper} sx={{ padding: 2 }}>
                    <Stack spacing={2}>
                        <Typography>Filters</Typography>
                        <Divider flexItem />
                        <TextField
                            name={Field.Name}
                            label={t(`wallet.labels.${Field.Name}`)}
                            onChange={handleFilterUpdate}
                        />
                        <Divider flexItem />
                        <TextField
                            name={Field.Address}
                            label={t(`wallet.labels.${Field.Address}`)}
                            onChange={handleFilterUpdate}
                        />
                    </Stack>
                </Box>
            </Popper>
        </MuiToolbar>
    );
};

const SortedTableCell = ({
    order,
    isSorted,
    onSort,
    fieldName,
    ...props
}: {
    order: Order;
    isSorted: boolean;
    onSort: (field: Field) => void;
    fieldName: Field;
} & TableCellProps) => (
    <TableCell {...props}>
        <TableSortLabel active={isSorted} direction={isSorted ? order : Order.Asc} onClick={() => onSort(fieldName)}>
            <Typography variant="overline">{fieldName}</Typography>
        </TableSortLabel>
    </TableCell>
);

interface AccountRowProps {
    account: { name: string; id?: string; address: string; encrypted: boolean };
    checked: boolean;
    selectRow: (id: string) => void;
}

const AccountRow = ({ account, checked, selectRow }: AccountRowProps) => {
    const t = useTranslation();

    return (
        <TableRow key={account.id}>
            <TableCell padding="checkbox">
                <Checkbox color="primary" checked={checked} onChange={() => selectRow(account.id || '')} />
            </TableCell>
            <TableCell>{account.name}</TableCell>
            <TableCell align="left">{account.address}</TableCell>
            <TableCell>{account.encrypted && <LockOutlinedIcon sx={{ marginRight: 1 }} />}</TableCell>

            <TableCell align="right">
                <RouterFab size="small" to={`/wallet/accounts/${account.id}`}>
                    <SearchOutlinedIcon />
                    Open
                </RouterFab>
            </TableCell>
        </TableRow>
    );
};

interface OwnProps {
    accounts: { name: string; id: string; address: string; encrypted: boolean }[];
    removeAccounts: (ids: string[]) => void;
}

const AccountsView: React.FC<OwnProps> = ({ removeAccounts, accounts }) => {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [sortField, setSortField] = React.useState<Field>(Field.Name);
    const [order, setOrder] = React.useState<Order>(Order.Desc);
    const [selectedRows, setSelectedRows] = React.useState<string[]>([]);
    const [filters, setFilters] = React.useState<Record<string, string>>({});
    const t = useTranslation();

    const onSort = React.useCallback(
        (field: Field) => {
            if (sortField === field) {
                setOrder(order === Order.Asc ? Order.Desc : Order.Asc);
            } else {
                setSortField(field);
                setOrder(Order.Asc);
            }
        },
        [order, sortField],
    );

    const filteredAccounts = React.useMemo(() => {
        let _accounts = sortByField(accounts, sortField, order);

        _accounts = _accounts.filter((c) => {
            const keys = Object.keys(filters).filter((field) => !!filters[field]) as Field[];
            return keys.every((field) => (field in c && c[field]?.includes(filters[field])) || false);
        });

        _accounts = _accounts.slice(page * rowsPerPage, (page + 1) * rowsPerPage);

        return _accounts;
    }, [accounts, page, rowsPerPage, sortField, order, filters]);

    const onSelectAll = React.useCallback(() => {
        if (selectedRows.length) {
            setSelectedRows([]);
        } else {
            setSelectedRows(filteredAccounts.map(({ id }) => id || ''));
        }
    }, [selectedRows, filteredAccounts]);

    const selectRow = React.useCallback(
        (id: string) => {
            if (selectedRows.includes(id)) {
                setSelectedRows((selected) => selected.filter((s) => s !== id));
            } else {
                setSelectedRows((selected) => [...selected, id]);
            }
        },
        [selectedRows],
    );

    const handleDelete = React.useCallback(() => {
        removeAccounts(selectedRows);
        setSelectedRows([]);
    }, [removeAccounts, selectedRows]);

    const handleFilter = (field: Field, value: string) => {
        setFilters((filters) => ({ ...filters, [field]: value }));
    };

    return (
        <Table
            sx={{ flexGrow: 1 }}
            header={
                <TableHead>
                    <TableRow>
                        <TableCell colSpan={5}>
                            <TableToolbar
                                t={t}
                                numSelected={selectedRows.length}
                                onFilter={handleFilter}
                                onDelete={handleDelete}
                            />
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell padding="checkbox">
                            <Checkbox
                                color="primary"
                                indeterminate={selectedRows.length > 0 && selectedRows.length < rowsPerPage}
                                checked={rowsPerPage > 0 && selectedRows.length === rowsPerPage}
                                onChange={onSelectAll}
                                inputProps={{
                                    'aria-label': 'Select all accounts',
                                }}
                            />
                        </TableCell>
                        <SortedTableCell
                            order={order}
                            fieldName={Field.Name}
                            onSort={onSort}
                            isSorted={sortField === Field.Name}
                        />
                        <SortedTableCell
                            align="left"
                            order={order}
                            fieldName={Field.Address}
                            onSort={onSort}
                            isSorted={sortField === Field.Address}
                        />
                        {/* Open button will appear in this cell */}
                        <TableCell align="right" />
                    </TableRow>
                </TableHead>
            }
            body={
                <TableBody>
                    {filteredAccounts.map((account) => (
                        <AccountRow
                            key={account.id}
                            checked={selectedRows.includes(account.id || '')}
                            account={account}
                            selectRow={selectRow}
                        />
                    ))}
                </TableBody>
            }
            pagination={
                <TablePagination
                    rowsPerPageOptions={[5, 10]}
                    component="div"
                    count={accounts.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={(_, page) => setPage(page)}
                    onRowsPerPageChange={(e) => setRowsPerPage(Number(e.target.value))}
                />
            }
        />
    );
};

export default AccountsView;
