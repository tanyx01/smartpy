import React from 'react';
import WalletStorage from 'smartpy-wallet-storage';

import {
    Alert,
    Dialog,
    DialogActions,
    DialogContent,
    DialogProps,
    Divider,
    TextField,
    Button,
    DialogTitle,
    InputLabel,
    FormControlLabel,
    Checkbox,
    InputAdornment,
    IconButton,
    OutlinedInput,
    FormControl,
} from '@mui/material';

import useTranslation from '../../i18n/hooks/useTranslation';
import KeyIcon from '@mui/icons-material/VpnKey';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

import WalletServices from '../services';
import { useNetworkInfo } from '../selectors/network';
import { AccountSource } from '../constants/sources';
import useWalletContext from '../hooks/useWalletContext';
import Logger from 'src/services/logger';

interface AddAccountDialogProps extends DialogProps {
    onClose: () => void;
}

const AddAccountDialog: React.FC<AddAccountDialogProps> = ({ ...props }) => {
    const t = useTranslation();
    const { addAccount } = useWalletContext();
    const [name, setName] = React.useState('');
    const [secretKey, setSecretKey] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [usePassword, setUsePassword] = React.useState(false);
    const [showPassword, setShowPassword] = React.useState(false);
    const [showSecret, setShowSecret] = React.useState(false);
    const [error, setError] = React.useState('');
    const { rpc } = useNetworkInfo();

    const createAccount = async () => {
        try {
            if (name && secretKey && (password || !usePassword)) {
                await WalletServices[AccountSource.SMARTPY_SECRET_KEY].import(rpc, { secretKey });
                const privateKeyHash = await WalletServices[AccountSource.SMARTPY_SECRET_KEY].publicKeyHash();
                const publicKey = await WalletServices[AccountSource.SMARTPY_SECRET_KEY].publicKey();
                privateKeyHash &&
                    publicKey &&
                    addAccount(
                        {
                            address: privateKeyHash,
                            accountType: WalletStorage.constants.accountTypes.NORMAL,
                            name: name,
                            privateKey: secretKey,
                            publicKey,
                        },
                        password,
                    );
                handleClose();
            }
        } catch (e: any) {
            setError(e?.message);
            Logger.debug(e);
        }
    };

    const generatePrivateKey = async () => {
        const secretKey = await WalletServices[AccountSource.SMARTPY_SECRET_KEY].generateSecretKey();
        setSecretKey(secretKey);
    };

    const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        switch (e.target.name) {
            case 'name':
                return setName(e.target.value);
            case 'secretKey':
                return setSecretKey(e.target.value);
            case 'password':
                return setPassword(e.target.value);
        }
    };

    const handleClickShowSecret = () => {
        setShowSecret(!showSecret);
    };
    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };
    const handleMouseDownShow = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const handleUsePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.checked) {
            setUsePassword(true);
        } else {
            setUsePassword(false);
            setPassword('');
        }
    };

    const handleClose = () => {
        setName('');
        setSecretKey('');
        setPassword('');
        setUsePassword(false);
        props?.onClose();
    };

    return (
        <Dialog {...props} onClose={handleClose} fullWidth>
            <DialogTitle>{t('wallet.labels.addAccount')}</DialogTitle>
            <DialogContent dividers>
                {error ? (
                    <Alert severity="error" sx={{ marginBottom: 3 }}>
                        {error}
                    </Alert>
                ) : null}
                <TextField
                    autoFocus
                    fullWidth
                    required
                    name="name"
                    label={t('common.name')}
                    value={name}
                    onChange={handleTextChange}
                    autoComplete="off"
                />
                <Divider sx={{ marginTop: 2, marginBottom: 2 }} />
                <Button variant="contained" endIcon={<KeyIcon />} onClick={generatePrivateKey}>
                    {t('wallet.labels.generatePrivateKey')}
                </Button>
                <FormControl fullWidth sx={{ marginTop: 2 }} variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-secret">{`${t('common.secret')} (edsk)`}</InputLabel>
                    <OutlinedInput
                        required
                        id="outlined-adornment-secret"
                        type={showSecret ? 'text' : 'password'}
                        name="secretKey"
                        label={`${t('common.secret')} (edsk)`}
                        onChange={handleTextChange}
                        value={secretKey}
                        autoComplete="new-password"
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label={t('common.aria.toggleSecretKeyVisibility')}
                                    onClick={handleClickShowSecret}
                                    onMouseDown={handleMouseDownShow}
                                    edge="end"
                                >
                                    {showSecret ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <Divider sx={{ marginTop: 2, marginBottom: 2 }} />
                <InputLabel>{t('wallet.addAccountDialog.usePasswordExplanation')}</InputLabel>
                <FormControlLabel
                    control={<Checkbox onChange={handleUsePasswordChange} checked={usePassword} />}
                    label={t('wallet.addAccountDialog.usePassword')}
                />
                {usePassword && (
                    <FormControl fullWidth sx={{ marginTop: 2 }} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password">{t('common.password')}</InputLabel>
                        <OutlinedInput
                            required
                            id="outlined-adornment-password"
                            type={showPassword ? 'text' : 'password'}
                            name="password"
                            label={t('common.password')}
                            onChange={handleTextChange}
                            value={password}
                            autoComplete="new-password"
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label={t('common.aria.togglePasswordVisibility')}
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownShow}
                                        edge="end"
                                    >
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                )}
            </DialogContent>

            <DialogActions>
                <Button autoFocus color="primary" onClick={createAccount}>
                    {t('wallet.labels.createAccount')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default AddAccountDialog;
