import { combineReducers } from 'redux';
import { AccountInformation, NetworkInformation } from 'SmartPyModels';
import { createReducer } from 'typesafe-actions';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

import { Network } from '../../constants/networks';
import { smartpy } from '../../constants/rpc';

import { updateNetworkInfo, updateAccountInfo, clearAccountInfo, loadingAccountInfo } from './actions';

// Network

const networkReducer = createReducer<NetworkInformation>({
    network: Network.MAINNET as string,
    rpc: smartpy[Network.MAINNET],
}).handleAction(updateNetworkInfo, (state, { payload }) => ({
    ...state,
    ...payload,
}));

// Account

const accountReducer = createReducer<AccountInformation>({ isLoading: false })
    .handleAction(updateAccountInfo, (state, { payload }) => ({
        ...state,
        ...payload,
    }))
    .handleAction(clearAccountInfo, () => ({ isLoading: false }))
    .handleAction(loadingAccountInfo, (state, { payload }) => ({
        ...state,
        isLoading: payload,
    }));

const persistConfig = {
    key: 'wallet2',
    storage: storage,
    whitelist: [],
};

const walletReducer = combineReducers({
    networkInfo: networkReducer,
    accountInfo: accountReducer,
});

export default persistReducer(persistConfig, walletReducer);
