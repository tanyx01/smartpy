declare module 'SmartPyModels' {
    export type AccountInformation = {
        isLoading?: boolean;
        revealRequired?: boolean;
        revealing?: boolean;
        activating?: boolean;
        activationRequired?: boolean;
        errors?: string;
        source?: import('./constants/sources').AccountSource;
        pkh?: string;
        balance?: number;
        balanceTimestamp?: timestamp;
    };
    export type NetworkInformation = {
        network?: string;
        rpc: string;
    };
}

declare module 'SmartPyWalletTypes' {
    export type WalletStatus = {
        temple: boolean;
    };
    export type ScriptParams = {
        code: string;
        storage: string;
    };
    export type CallParams = {
        entrypoint: string;
        value: Record<string, unknown>;
    };
    export type OperationCostsEstimation = {
        fee: number;
        gasLimit: number;
        storageLimit: number;
    };
    export type OperationResults = Promise<{
        hash: string;
        results: import('conseiljs').AlphaOperationContentsAndResult[];
        contractAddress?: string;
        monitor: import('rxjs').Observable<number>;
    }>;
    export type OperationInformation = {
        bytes: string;
        blockHash: string;
        operationContents: import('conseiljs').Origination | import('conseiljs').Transaction;
        blake2bHash: string;
        send: () => Promise<OperationResults>;
    };
}
