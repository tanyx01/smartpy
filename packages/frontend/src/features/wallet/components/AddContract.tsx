import React from 'react';

import {
    Alert,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogProps,
    DialogTitle,
    Divider,
    Stack,
    TextField,
    Typography,
} from '@mui/material';

import useTranslation from 'src/features/i18n/hooks/useTranslation';
import useWalletContext from '../hooks/useWalletContext';

interface AddContractDialogProps extends DialogProps {
    defaultAddress?: string;
    onClose: () => void;
}

const AddContractDialog = ({ defaultAddress, ...props }: AddContractDialogProps) => {
    const t = useTranslation();
    const [addContractValues, setAddContractValues] = React.useState<Record<string, string>>({});
    const { addContract } = useWalletContext();
    const [error, setError] = React.useState('');

    const handleAddContract = React.useCallback(() => {
        if (!addContractValues.name) {
            return setError('You must provide the contract name.');
        }
        if (!addContractValues.address && !defaultAddress) {
            return setError('You must provide the contract address.');
        }
        addContract({
            name: addContractValues.name,
            address: addContractValues.address || defaultAddress || '',
        });
        props.onClose();
    }, [addContract, addContractValues.address, addContractValues.name, defaultAddress, props]);

    const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAddContractValues((values) => ({ ...values, [e.target.name]: e.target.value }));
    };

    return (
        <Dialog {...props} fullWidth>
            <DialogTitle id={props['aria-labelledby']}>{t('wallet.labels.addContract')}</DialogTitle>
            <DialogContent dividers>
                {error ? (
                    <Alert variant="outlined" severity="error" onClose={() => setError('')}>
                        <Typography>{error}</Typography>
                    </Alert>
                ) : null}
                <div style={{ margin: 5 }} />
                <Stack spacing={2}>
                    <TextField fullWidth name="name" label={t(`wallet.labels.name`)} onChange={handleTextChange} />
                    <Divider flexItem />
                    <TextField
                        fullWidth
                        name="address"
                        defaultValue={defaultAddress || ''}
                        label={t(`wallet.labels.address`)}
                        onChange={handleTextChange}
                    />
                </Stack>
            </DialogContent>
            <DialogActions>
                <Button
                    disabled={!addContractValues.name || (!addContractValues.address && !defaultAddress)}
                    onClick={handleAddContract}
                    autoFocus
                >
                    {t('wallet.labels.addContract')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default AddContractDialog;
