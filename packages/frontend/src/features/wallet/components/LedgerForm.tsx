import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import MenuItem from '@mui/material/MenuItem';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import ResetIcon from '@mui/icons-material/RotateLeft';
import Typography from '@mui/material/Typography';
import Alert from '@mui/material/Alert';

import useTranslation from '../../i18n/hooks/useTranslation';
import { clearAccountInfo, loadingAccountInfo, updateAccountInfo } from '../actions';
import { Curve } from '../services/smartpy/LedgerSigner';
import WalletServices from '../services';
import { getRpcNetwork } from '../../../utils/tezosRpc';
import { useNetworkInfo } from '../selectors/network';
import ledgerInfo from '../constants/ledger';
import AccountInfo from './AccountInfo';
import { usingSafari } from '../../../utils/browser';
import logger from '../../../services/logger';
import { AccountSource } from '../constants/sources';
import { Divider } from '@mui/material';
import { useAccountInfo } from '../selectors/account';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: 5,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
        },
        fullHeight: {
            height: '100%',
        },
        section: {
            padding: 20,
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        alert: {
            margin: theme.spacing(2, 0, 2, 0),
            padding: 20,
        },
    }),
);

enum FormField {
    CURVE = 'curve',
    DERIVATION_PATH = 'derivationPath',
}

const DEFAULT_FORM_VALUES = {
    [FormField.CURVE]: Curve.ED25519,
    [FormField.DERIVATION_PATH]: ledgerInfo.defaultDerivationPath,
};

const LedgerForm: React.FC = () => {
    const classes = useStyles();
    const [values, setValues] = React.useState(DEFAULT_FORM_VALUES);
    const t = useTranslation();
    const dispatch = useDispatch();
    const { rpc } = useNetworkInfo();
    const { isLoading } = useAccountInfo();

    const resetDerivationPath = () => {
        setValues((state) => ({ ...state, [FormField.DERIVATION_PATH]: ledgerInfo.defaultDerivationPath }));
    };

    const handleInputChange = (
        event: SelectChangeEvent<string> | React.ChangeEvent<{ name: string; value: string }>,
    ) => {
        const { name, value } = event.target;
        if (name) {
            setValues((state) => ({ ...state, [name]: value }));
        }
    };

    const onConnect = async () => {
        // Reset Account Information
        dispatch(clearAccountInfo());

        dispatch(loadingAccountInfo(true));
        try {
            const network = await getRpcNetwork(rpc);
            if (network) {
                await WalletServices[AccountSource.SMARTPY_LEDGER].connect(
                    { name: network.toLowerCase(), rpc },
                    { curve: values[FormField.CURVE], path: values[FormField.DERIVATION_PATH] },
                );
                const accountInformation = await WalletServices[AccountSource.SMARTPY_LEDGER].getInformation();
                dispatch(updateAccountInfo(accountInformation));
            }
        } catch (e) {
            logger.error(e);
            dispatch(
                updateAccountInfo({
                    errors: e.message,
                }),
            );
        } finally {
            dispatch(loadingAccountInfo(false));
        }
    };

    return (
        <div className={classes.root}>
            <Typography variant="overline">{t('wallet.ledgerForm.title')}</Typography>
            <Paper className={classes.section}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <FormControl variant="filled" fullWidth>
                            <InputLabel>{t('wallet.curve')}</InputLabel>
                            <Select
                                name={FormField.CURVE}
                                value={String(values[FormField.CURVE])}
                                onChange={handleInputChange}
                            >
                                <MenuItem value={Curve.ED25519}>
                                    <em>ed25519 (tz1)</em>
                                </MenuItem>
                                <MenuItem value={Curve.SECP256K1}>
                                    <em>secp256k1 (tz2)</em>
                                </MenuItem>
                                <MenuItem value={Curve.P256}>
                                    <em>p256 (tz3)</em>
                                </MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            name={FormField.DERIVATION_PATH}
                            fullWidth
                            variant="filled"
                            label={t('wallet.derivationPath')}
                            value={values[FormField.DERIVATION_PATH]}
                            onChange={handleInputChange}
                            InputProps={{
                                endAdornment: (
                                    <Tooltip
                                        title={t('wallet.resetDerivationPath') as string}
                                        aria-label="reset-derivation-path"
                                        placement="left"
                                    >
                                        <span>
                                            <IconButton
                                                disabled={
                                                    values[FormField.DERIVATION_PATH] ===
                                                    ledgerInfo.defaultDerivationPath
                                                }
                                                color="primary"
                                                onClick={resetDerivationPath}
                                                size="large"
                                            >
                                                <ResetIcon />
                                            </IconButton>
                                        </span>
                                    </Tooltip>
                                ),
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.fullHeight}
                            onClick={onConnect}
                            disabled={usingSafari()}
                        >
                            {t('Connect')}
                        </Button>
                    </Grid>
                </Grid>
            </Paper>
            <Divider sx={{ margin: 1 }} />
            {isLoading ? <Typography variant="overline">Connect your ledger and go to the Tezos App</Typography> : null}

            {usingSafari() ? (
                <Alert severity="error" className={classes.alert}>
                    {t('wallet.ledgerForm.safariNotSupported')}
                </Alert>
            ) : (
                <AccountInfo />
            )}
        </div>
    );
};

export default LedgerForm;
