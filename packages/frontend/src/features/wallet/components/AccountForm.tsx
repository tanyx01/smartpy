import React from 'react';
import { useDispatch } from 'react-redux';
import { Link as LinkRouter } from 'react-router-dom';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import { Typography, Box } from '@mui/material';
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';

import useTranslation from 'src/features/i18n/hooks/useTranslation';
import AccountInfo from './AccountInfo';

import Paper from 'src/features/common/components/Paper';

import { clearAccountInfo, loadingAccountInfo, updateAccountInfo } from '../actions';
import WalletServices from '../services';
import { useNetworkInfo } from '../selectors/network';
import logger from 'src/services/logger';
import { AccountSource } from '../constants/sources';
import useWalletContext from 'src/features/wallet/hooks/useWalletContext';
import DecryptAccount from '../views/DecryptAccount';
import { TezosAccount } from 'smartpy-wallet-storage';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: 5,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
        },
        paper: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: 300,
            flexGrow: 1,
            boxShadow: 'inset 0 0 10px #000000',
        },
        fullHeight: {
            height: '100%',
        },
        section: {
            padding: 20,
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        storedFaucet: {
            paddingTop: theme.spacing(2),
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
        },
        input: {
            padding: '10px 12px 8px',
        },
    }),
);

interface OwnProps {
    disableNetworkSelection?: boolean;
}

const AccountForm: React.FC<OwnProps> = ({ disableNetworkSelection = true }) => {
    const classes = useStyles();
    const t = useTranslation();
    const { rpc } = useNetworkInfo();
    const dispatch = useDispatch();
    const { accounts } = useWalletContext();
    const [account, setAccount] = React.useState<TezosAccount | undefined>(undefined);

    const selectAccount = async (event: SelectChangeEvent<string>) => {
        const account = accounts[parseInt(event.target.value)];
        await loadStoredAccount(account);
    };

    const setAndLoadAccount = (account: TezosAccount) => {
        setAccount(account);
        loadStoredAccount(account);
    };

    const loadStoredAccount = async (account: TezosAccount) => {
        // Reset Account Information
        dispatch(clearAccountInfo());

        dispatch(loadingAccountInfo(true));

        setAccount(account);
        try {
            const secretKey = account.privateKey;
            if (secretKey) {
                await WalletServices[AccountSource.SMARTPY_SECRET_KEY].import(rpc, { secretKey });
                const accountInformation = await WalletServices[AccountSource.SMARTPY_SECRET_KEY].getInformation();
                dispatch(updateAccountInfo(accountInformation));
            }
        } catch (e: any) {
            logger.error(e);
            dispatch(
                updateAccountInfo({
                    errors: e.message,
                }),
            );
        } finally {
            dispatch(loadingAccountInfo(false));
        }
    };

    return (
        <div className={classes.root}>
            <Container fixed maxWidth="sm">
                <Paper className={classes.section}>
                    <FormControl variant="filled" fullWidth disabled={accounts.length === 0}>
                        <InputLabel id="stored-account-selector">
                            {t('wallet.accountForm.selectStoredAccount')}
                        </InputLabel>

                        <Select labelId="stored-account-selector" defaultValue="" displayEmpty onChange={selectAccount}>
                            {Object.entries(accounts).map(([acc, { name }]) => (
                                <MenuItem value={acc} key={acc} divider>
                                    <Typography variant="caption" noWrap>
                                        {name}
                                    </Typography>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <Box sx={{ display: 'flex', justifyContent: 'end' }}>
                        <LinkRouter to="wallet/accounts" target="_blank">
                            {t('wallet.accountForm.manageAccounts')}
                        </LinkRouter>
                    </Box>
                </Paper>
            </Container>
            <Divider className={classes.divider} />
            {account && (
                <DecryptAccount account={account} setAccount={setAndLoadAccount}>
                    <AccountInfo disableNetworkSelection={disableNetworkSelection} />
                </DecryptAccount>
            )}
        </div>
    );
};

export default AccountForm;
