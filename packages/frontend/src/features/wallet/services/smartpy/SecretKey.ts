import { InMemorySigner } from '@taquito/signer';
import { AccountSource } from '../../constants/sources';
import WalletError from '../../exceptions/WalletError';
import SecretKeyErrorCodes from '../../exceptions/SecretKeyErrorCodes';
import AbstractWallet from '../AbstractWallet';
import LocalSigner from './LocalSigner';
import * as bip39 from 'bip39';
import sodium from 'libsodium-wrappers-sumo';
import base58check from 'bs58check';

export type SecretKeyParams = {
    secretKey: string;
    password?: string;
};

class SecretKeyWallet extends AbstractWallet {
    constructor() {
        super(AccountSource.SMARTPY_SECRET_KEY);
    }

    /**
     * @description Connect to the wallet.
     *
     * @param rpc RPC address.
     * @param params The secret seed and an optional password.
     *
     * @returns A promise that resolves to void;
     */
    public import = async (rpc: string, params: SecretKeyParams) => {
        this.signer = await LocalSigner.fromSecretKey(params.secretKey, params.password);

        if (!this.signer) {
            throw new WalletError(SecretKeyErrorCodes.NOT_ABLE_TO_IMPORT_ACCOUNT);
        }

        this.rpc = rpc;
        this.pkh = await this.signer.publicKeyHash();
    };

    public generateSecretKey = async (): Promise<string> => {
        const mnemonic = bip39.generateMnemonic(256);
        const seed = await bip39.mnemonicToSeed(mnemonic, '');
        await sodium.ready;
        const keys = sodium.crypto_sign_seed_keypair(seed.slice(0, 32), 'hex');
        return base58check.encode(Buffer.from('2bf64e07' + keys.privateKey, 'hex'));
    };
}

export default SecretKeyWallet;
