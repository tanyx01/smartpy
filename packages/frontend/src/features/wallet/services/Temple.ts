import { TempleWallet } from '@temple-wallet/dapp';

import info from '../../../constants/info';
import { AccountSource } from '../constants/sources';
import AbstractWallet from './AbstractWallet';
import TempleSigner from './TempleSigner';

class Temple extends AbstractWallet {
    private client;

    constructor() {
        super(AccountSource.TEMPLE);
        this.client = new TempleWallet(info.name);
        this.signer = new TempleSigner(this.client);
    }

    /**
     * @description Callback that monitors the wallet availability.
     *
     * @returns A promise that resolved to true if the wallet is available or false otherwise.
     */
    public onAvailabilityChange = TempleWallet.onAvailabilityChange;

    /**
     * @description Verifies if the wallet extension is installed and available.
     *
     * @returns A promise that resolved to true if the wallet is available or false otherwise.
     */
    public isAvailable = TempleWallet.isAvailable;

    /**
     * @description Connect to the wallet.
     *
     * @param network A Tezos network.
     * @param options Options passed to the wallet.
     *
     * @returns A promise that resolves to void;
     */
    public connect = async (
        network: {
            name: string;
            rpc: string;
        },
        opts?:
            | {
                  forcePermission: boolean;
              }
            | undefined,
    ) => {
        await this.client.connect(network, opts);

        this.rpc = network.rpc;
        this.pkh = await this.client.getPKH();
    };
}

export default Temple;
