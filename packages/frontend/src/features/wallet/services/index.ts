import Temple from './Temple';
import Beacon from './Beacon';
import Ledger from './smartpy/Ledger';
import SecretKey from './smartpy/SecretKey';
import { AccountSource } from '../constants/sources';

const WalletService = {
    [AccountSource.TEMPLE]: new Temple(),
    [AccountSource.BEACON]: new Beacon(),
    [AccountSource.SMARTPY_LEDGER]: new Ledger(),
    [AccountSource.SMARTPY_SECRET_KEY]: new SecretKey(),
};

export default WalletService;
