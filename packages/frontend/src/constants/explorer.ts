import { Network } from './networks';

export const tzktAPI: Record<string, string> = {
    [Network.MAINNET]: 'https://api.mainnet.tzkt.io/v1',
    [Network.GHOSTNET]: 'https://api.ghostnet.tzkt.io/v1',
    [Network.KATHMANDUNET]: 'https://api.kathmandunet.tzkt.io/v1',
    [Network.LIMANET]: 'https://api.limanet.tzkt.io/v1',
};

export const tzkt: Record<string, string> = {
    [Network.MAINNET]: 'https://tzkt.io',
    [Network.GHOSTNET]: 'https://ghostnet.tzkt.io',
    [Network.KATHMANDUNET]: 'https://kathmandunet.tzkt.io',
    [Network.LIMANET]: 'https://limanet.tzkt.io',
};

export const tzstatsAPI: Record<string, string> = {
    [Network.MAINNET]: 'https://api.tzstats.com/explorer',
    [Network.GHOSTNET]: 'https://api.ghost.tzstats.com/explorer',
    [Network.KATHMANDUNET]: 'https://api.kathmandu.tzstats.com/explorer',
    [Network.LIMANET]: 'https://api.lima.tzstats.com/explorer',
};

export const tzstats: Record<string, string> = {
    [Network.MAINNET]: 'https://tzstats.com',
    [Network.GHOSTNET]: 'https://ghost.tzstats.com',
    [Network.KATHMANDUNET]: 'https://kathmandu.tzstats.com',
    [Network.LIMANET]: 'https://lima.tzstats.com',
};

export const betterCallDev: Record<string, string> = {
    [Network.MAINNET]: 'https://better-call.dev/mainnet',
    [Network.GHOSTNET]: 'https://better-call.dev/ghostnet',
    [Network.KATHMANDUNET]: 'https://better-call.dev/kathmandunet',
    [Network.LIMANET]: 'https://better-call.dev/limanet',
};

export enum Endpoint {
    CONTRACT_OPERATIONS = '/accounts/:contract_id/operations',
}

const Explorer = {
    betterCallDev,
    tzstats,
    tzstatsAPI,
    tzkt,
    tzktAPI,
    Endpoint,
};

export default Explorer;
