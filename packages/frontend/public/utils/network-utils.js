const Networks = {
    MAINNET: 'Mainnet',
    GHOSTNET: 'Ghostnet',
    JAKARTANET: 'Jakartanet',
    KATHMANDUNET: 'Kathmandunet',
    LIMANET: 'Limanet',
};

const EXPLORER = {
    MAINNET: {
        tzstats: 'https://tzstats.com',
        tzkt: 'https://tzkt.io',
        bcd: 'https://better-call.dev',
    },
    GHOSTNET: {
        tzstats: 'https://ghost.tzstats.com/',
        tzkt: 'https://ghostnet.tzkt.io',
        bcd: 'https://better-call.dev',
    },
    JAKARTANET: {
        tzstats: 'https://jakarta.tzstats.com/',
        tzkt: 'https://jakartanet.tzkt.io',
        bcd: 'https://better-call.dev/jakartanet',
    },
    KATHMANDUNET: {
        tzstats: 'https://kathmandu.tzstats.com/',
        tzkt: 'https://kathmandunet.tzkt.io',
        bcd: 'https://better-call.dev/kathmandunet',
    },
    LIMANET: {
        tzstats: 'https://lima.tzstats.com/',
        tzkt: 'https://limanet.tzkt.io',
        bcd: 'https://better-call.dev/limanet',
    },
};

const API = {
    MAINNET: {
        tzkt: 'https://api.mainnet.tzkt.io/v1',
        tzstats: 'https://api.tzstats.com',
    },
    GHOSTNET: {
        tzkt: 'https://api.ghostnet.tzkt.io/v1',
        tzstats: 'https://api.ghost.tzstats.com',
    },
    JAKARTANET: {
        tzkt: 'https://api.jakartanet.tzkt.io/v1',
        tzstats: 'https://api.jakarta.tzstats.com',
    },
    KATHMANDUNET: {
        tzkt: 'https://api.kathmandunet.tzkt.io/v1',
        tzstats: 'https://api.kathmandu.tzstats.com',
    },
    LIMANET: {
        tzkt: 'https://api.limanet.tzkt.io/v1',
        tzstats: 'https://api.lima.tzstats.com',
    },
};

const smartPyNodes = {
    'https://mainnet.smartpy.io': Networks.MAINNET,
    'https://ghostnet.smartpy.io': Networks.GHOSTNET,
    'https://jakartanet.smartpy.io': Networks.JAKARTANET,
    'https://kathmandunet.smartpy.io': Networks.KATHMANDUNET,
    'https://limanet.smartpy.io': Networks.LIMANET,
};

const gigaNodes = {
    'https://mainnet-tezos.giganode.io': Networks.MAINNET,
};

const networkFilterByFeature = {
    faucet: ['mainnet'],
};

/**
 * Filter network by feature
 *
 * @param {string} feature
 * @param {string} node
 *
 * @returns true to skip, false otherwise
 */
const shouldSkipNetwork = (feature, node) => {
    const featureFilter = networkFilterByFeature[feature];
    return featureFilter && featureFilter.some((filter) => node.toLowerCase().includes(filter));
};

/**
 *  GET Request
 */
const requestGET = (url) =>
    new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.timeout = 2000;
        req.onreadystatechange = () => {
            if (req.readyState === 4) {
                if (req.status === 200) {
                    resolve(JSON.parse(req.response));
                } else {
                    reject(req.responseText);
                }
            }
        };
        req.open('GET', url, true);
        req.send();
    });

/**
 * Get the RPC network.
 * @param {string} rpcAddress - RPC address ( e.g. https://mainnet.smartpy.io )
 * @return {string} One of the following networks [MAINNET]
 */
const getRpcNetwork = async (rpcAddress) => {
    const {
        network_version: { chain_name },
    } = await requestGET(`${rpcAddress}/version`).catch(async (e) => {
        console.warn(e);
        return {
            network_version: await requestGET(`${rpcAddress}/network/version`),
        };
    });

    const network = chain_name.split('_')[1].replace('ITHACANET', 'GHOSTNET');
    if (!Object.keys(Networks).includes(network)) {
        console.error(`Unknown network: ${network}.`);
    }

    return network?.replace(Networks.EDO2NET, Networks.EDONET);
};
