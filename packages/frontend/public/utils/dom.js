// Sanitize user inputs before inserting them into the DOM
const sanitize_user_input = (input) => input?.replace(/<(|\/|[^>\/bi]|\/[^>bi]|[^\/>][^>]+|\/[^>][^>]+)>/g, '');
