/**
 * Tezos Base Derivation Path
 *
 * 44   - BIP-0044
 * 1729 - Registered coin type (XTZ)
 *
 * @see https://github.com/satoshilabs/slips/blob/master/slip-0044.md
 */
const BaseDerivationPath = '44h/1729h';

const KnownDerivationPaths = {
  'octez-client': `${BaseDerivationPath}/0h/0h`,
};

const Curves = {
  tz1: 0x00,
  tz2: 0x01,
  tz3: 0x02,
};

const CurveNames = {
  tz1: 'ed25519',
  tz2: 'secp256k1',
  tz3: 'p256',
};
