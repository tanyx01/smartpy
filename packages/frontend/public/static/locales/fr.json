{
  "michelsonIde": {
    "noErrors": "No Errors",
    "electricEvaluation": "Electric Evaluation",
    "errorsWhileParsingMichelson": "Errors while parsing the Michelson script",
    "helpMenu": {
      "michelsonReferenceByNomadicLabs": "Michelson Reference",
      "openReferenceManual": "SmartPy Reference Manual"
    },
    "toolBar": {
      "runCode": "Run Code (Ctrl-Enter / Cmd-Enter)",
      "clearOutputs": "Clear Outputs"
    }
  },
  "help": {
    "referenceManual": "Reference Manual",
    "referenceManualSubTitle": "Along with blogs, tutorials and templates, the best resource for SmartPy documentation is its ",
    "faqAcron": "FAQ",
    "faq": "Frequently Asked Questions",
    "installCLI": "Install the command line interface",
    "releases": "SmartPy releases",
    "moduleCode": "Module Code",
    "internalModulesHeader": "Access to internal modules",
    "internalModulesSubHeader": "SmartPy relies heavily on two Python modules:",
    "smartpyModuleDescription": "The SmartPy language." ,
    "smartpyioModuleDescription": "Helper python module used by the online editor.",
    "smartpyReliesOn": "SmartPy also relies on SmartML, see our ",
    "openSourceDistribution": "open source distribution."
  },
  "ide": {
    "typescript": {
      "disclaimer": "This is a beta version, the Typescript syntax is still under development."
    },
    "editor": "IDE",
    "output": {
      "targets": "Targets",
      "tests": "Tests",
      "compilations": "Compilations"
    },
    "settings": {
      "fontSize": "Font Size",
      "protocol": "Protocol",
      "layout": "Layout",
      "sideBySide": "Side By Side",
      "stacked": "Stacked",
      "editorOnly": "Editor Only",
      "outputOnly": "Output Only",
      "helpers": "Helpers",
      "newcomersMode": "Newcomers Mode"
    },
    "outputPanel": "Output Panel",
    "toolBar": {
      "runCode": "Run Code (Ctrl-Enter / Cmd-Enter)",
      "clearOutputs": "Clear Outputs"
    },
    "helpMenu": {
      "openReferenceManual": "SmartPy Reference Manual",
      "installSmartpyCli": "SmartPy CLI",
      "viewReleases": "Releases"
    },
    "newcomer": {
      "newcomer": "Newcomer",
      "dialog": {
        "title": "Welcome to SmartPy IDE!",
        "Stay in Newcomer Mode": "Stay in Newcomer Mode",
        "This popup presents a few templates for newcomers": "This popup presents a few templates for newcomers.",
        "SmartPy IDE allows you to": "SmartPy IDE allows you to:",
        "selectTemplate": "Select a template",
        "showMeWhere": "Show me where!",
        "editAndRun": "Edit and Run the code",
        "observeAndDeploy": "Observe the code output and deploy the generated michelson contract on a testnet or on the Tezos mainnet.",
        "andMuchMore": "And much more...",
        "someTemplates": "A few templates:",
        "beginnerTemplates": "Beginner Templates",
        "advancedTemplates": "Advanced Templates",
        "expertTemplates": "Expert Templates",
        "calculator": "Calculator",
        "storeValue": "Storage Value",
        "tictactoe": "TicTacToe Game",
        "minikitties": "Mini Kitties",
        "FA1-2": "FA1.2 token standard",
        "FA2": "FA2 token standard",
        "oracle": "Chainlink Oracles",
        "stateChannels": "State Channels"
      },
      "templatesStep": {
        "popperChip": "Click in the menu to select a template"
      },
      "runCodeStep": {
        "popperChip": "Click in the button to run the code"
      }
    },
    "contractManagement": {
      "title": "Contract Management",
      "createContract": "Create Contract",
      "editContract": "Edit Contract",
      "updateContract": "Update Contract",
      "deleteContract": "Delete Contract",
      "importContract": "Import Contract",
      "contractNotInitiated": "Contract not initiated",
      "listDialog": {
        "listStoredContracts": "List Stored Contracts",
        "title": "Stored Contracts",
        "clickToUse": "Click on the contract to start using it.",
        "contractName": "Name: ",
        "elapsedTime": "Modified {{elapsedTime}} ago",
        "emptyList": "No Contracts yet!"
      },
      "editDialog": {
        "title": "Edit Contract {{name}}"
      }
    },
    "templatesMenu": {
      "label": "Templates",
      "favorites": "Favorites",
      "emptyList": "No Contracts Found!",
      "favoriteTemplates": "Favorite Templates",
      "emptyFavoritesList": "No Favorites!",
      "referenceTemplates": "Reference Templates",
      "regularTemplates": "Regular Templates",
      "contractTemplates": "Contract Templates"
    },
    "errors": {
      "outputEmpty": "The output panel is currently empty."
    }
  },
  "commonComponents": {
    "selectionOfTemplates": {
      "title": "A very small selection of templates that shows how to use SmartPy",
      "calculatorDescription": "Calculator - Computations and loops",
      "storeValueDescription": "Store Value - A simple data storage",
      "chessDescription": "Chess - Matrices, loops, local variables",
      "fa2Description": "FA2 - Fungible asset templates",
      "chainLinkDescription": "Chainlink Oracles - Experimental support of Chainlink oracles in Tezos"
    }
  },
  "wallet": {
    "labels": {
      "smartContracts": "Smart Contracts",
      "faucet": "Faucet",
      "operations": "Operations"
    },
    "exploreWith": {
      "smartpy": "Explore with SmartPy",
      "betterCallDev": "Explorer with Better Call Dev"
    },
    "contracts": {
      "addContract": "Add Contract",
      "address": "Address",
      "name": "Name",
      "network": "Network"
    },
    "ledgerForm": {
      "title": "Ledger Form",
      "safariNotSupported": "Ledger is not supported on Safari."
    },
    "secretKeyForm": {
      "title": "Secret Key Form",
      "secretKey": "Secret Key (edsk, spsk, p2sk)",
      "passphrase": "Passphrase",
      "showPassphrase": "Show Passphrase",
      "hidePassphrase": "Hide Passphrase"
    },
    "accountInfo": {
      "loadedWithSuccess": "Account loaded with success",
      "title": "Account Information",
      "reveal": "Reveal Account",
      "revealing": "Revealing Account",
      "activate": "Activate Account",
      "activating": "Activating Account"
    },
    "resetDerivationPath": "Reset Derivation Path",
    "derivationPath": "Derivation Path (Default)",
    "curve": "Elliptic Curve",
    "faucetForm": {
      "selectStoredFaucet": "Select a stored account",
      "storage": "Storage",
      "manageFaucet": "Manage your faucet accounts",
      "title": "Faucet Form",
      "clickToAccessFaucet": "Click here to access the faucet",
      "invalid": "The faucet is invalid",
      "saveFaucet": "Save Faucet"
    },
    "operation": {
      "title": "Pre-Signature Information",
      "hashes": "Hashes",
      "jsonEncodingNoScript": "Parameters (no script)",
      "jsonEncoding": "Parameters (Full)",
      "bytesEncoding": "Bytes Encoding",
      "blake2BHash": "Blake 2B Hash",
      "blockHash": "Block Hash"
    }
  },
  "fileImporter": {
    "dragAndDrop": "Drag/Drop the file here, or click to open file selection.",
    "dragAndDropTab": "Drag & Drop",
    "copyAndPasteTab": "Copy & Paste"
  },
  "origination": {
    "estimateCost": "Estimate cost from RPC",
    "originationFailed": "Contract origination failed!",
    "originatingContract": "Originating Contract...",
    "deployContract": "Deploy Contract",
    "deployContractHint": "Once everything looks good, you can deploy your contract:",
    "nodeAndNetwork": "Node and Network",
    "useYourOwnNode": "(You can use your own node)",
    "blockConfirmations": "Block Confirmations",
    "successfulDeployment": "Contract originated successfully!",
    "result": "Origination Result",
    "downloadContract": "Download Contract",
    "tezosClientCommand": "Tezos Client Command",
    "loadAccount": "(Load Account)",
    "sections": {
      "wallet": "Wallet",
      "contractOriginationParameters": "Origination Parameters"
    },
    "parameterLabels": {
      "endpoint": "RPC Endpoint",
      "contractName": "Contract Name",
      "source": "Source",
      "delegate": "Delegate (Optional)",
      "amount": "Amount in tez ꜩ",
      "fee": "Fee in tez ꜩ",
      "gasLimit": "Gas Limit",
      "storageLimit": "Storage Limit"
    },
    "openMichelsonEditor": "Open on Michelson editor",
    "linkCopied": "Origination link copied!",
    "shareButton": "Share Origination",
    "openExplorer": "Open Explorer",
    "originateFromBrowser": "Originate from Browser",
    "withTezosClient": "With Tezos Client",
    "title": "Direct Network Contract Origination",
    "subTitle": "Please choose a node on a Tezos network, a signer, and some origination parameters to originate the contract.",
    "saveContract": "Save Contract",
    "failedToOriginateContract": "Contract origination failed.",
    "couldNotEstimateCosts": "Not able to estimate origination costs.",
    "selectWalletFirst": "Select a wallet first."
  },
  "articlesGuides": {
    "b9labSection": "B9lab Materials",
    "tzatzikitzSection": "DApp tutorial by",
    "blogPostsAndOtherMaterials": "Blog Posts and Other Materials",
    "someMaterialsBySmartPy": "Some Materials by the SmartPy Team",
    "someMaterialsByOtherTeams": "Some Materials by Other Teams",
    "digitalAssets": "Digital Assets",
    "buidllabsSection": "Interactive code school that teaches you to write smart contracts in SmartPy by",
    "blockmaticsSection": "Tutorials and other teaching materials by",
    "cryptonomicSection": "A series about smart contract development in SmartPy by",
    "luizMilfontSection": "A series, How To Write Smart Contracts for Blockchain Using Python, by",
    "tezosindiaFoundationSection": "A tutorial by Pawan Dhanwani from"
  },
  "common": {
    "warning": "Warning",
    "videos": "Videos",
    "mediumPosts": "Medium Posts",
    "connect": "Connect",
    "loading": "Loading...",
    "copied": "Copied!",
    "generateLink": "Generate Link",
    "shareContract": "Share Contract",
    "sizes": "Sizes",
    "storage": "Storage",
    "code": "Code",
    "initialStorage": "Initial Storage",
    "michelsonCode": "Michelson Code",
    "customRPC": "Custom RPC",
    "shortcuts": "Shortcuts",
    "couldNotLoadFile": "Could not load file.",
    "copy": "Copy",
    "search": "Search",
    "close": "Close",
    "understood": "Understood",
    "cancel": "Cancel",
    "accept": "Accept",
    "ok": "Ok",
    "save": "Save",
    "editMode": "Edit Mode",
    "viewMode": "View Mode",
    "contract": {
      "name": "Contract Name"
    }
  }
}
