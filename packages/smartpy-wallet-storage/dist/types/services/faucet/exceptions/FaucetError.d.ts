export default class FaucetError extends Error {
    constructor(message: FaucetErrorCodes);
}
export declare enum FaucetErrorCodes {
    FAUCET_ACCOUNT_ALREADY_EXISTS = "implicit_account.already_exists",
    FAUCET_ACCOUNT_NOT_FOUND = "implicit_account.not_found",
    FAUCET_ACCOUNT_NULL = "implicit_account.must_not_be_null",
    FAUCET_ACCOUNT_ADDRESS_NULL = "implicit_account.fields.address.null"
}
