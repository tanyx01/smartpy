import { FaucetAccount } from './storage';
declare const FaucetService: {
    /**
     *  Find all faucet accounts.
     *  @returns {{ [address: string]: FaucetAccount }}
     */
    findAll: (searchTerm?: string) => {
        [address: string]: FaucetAccount;
    };
    /**
     *  Find faucet account by identifier.
     *  @param {string} id account identifier.
     *  @returns {FaucetAccount}
     */
    findById: (id: string) => FaucetAccount;
    /**
     *  Persist a faucet account.
     *  @param {FaucetAccount} account faucet account.
     */
    persist: (account: FaucetAccount) => void;
    /**
     *  Update a faucet account.
     *  @param {FaucetAccount} account faucet account.
     */
    merge: (account: FaucetAccount) => void;
    /**
     *  Remove a faucet account.
     *  @param {string} id account identifier.
     */
    removeById: (id: string) => void;
    /**
     *  Select faucet account.
     *  @param {string} id account identifier.
     */
    select: (id: string) => void;
    /**
     *  Get selected faucet account.
     *  @returns {FaucetAccount}
     */
    getSelected: () => FaucetAccount | undefined;
    /**
     *  Clear the storage.
     */
    clear: () => void;
};
export default FaucetService;
