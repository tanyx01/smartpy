export default class ImplicitAccountError extends Error {
    constructor(message: ImplicitAccountErrorCodes);
}
export declare enum ImplicitAccountErrorCodes {
    ACCOUNT_ALREADY_EXISTS = "implicit_account.already_exists",
    ACCOUNT_NOT_FOUND = "implicit_account.not_found",
    ACCOUNT_NULL = "implicit_account.must_not_be_null",
    ACCOUNT_ADDRESS_NULL = "implicit_account.fields.address.null",
    FIND_ALL_INVALID_ACCOUNT_TYPES_PARAMETER = "implicit_account.find_all.parameters.account_types.invalid",
    CANNOT_ENCRYPT_NOT_PROTECTED = "implicit_account.encrypt.not_protected",
    CANNOT_ENCRYPT_WITHOUT_PASSWORD = "implicit_account.encrypt.no_password",
    CANNOT_DECRYPT_WITHOUT_PASSWORD = "implicit_account.decrypt.no_password",
    INCORRECT_PASSWORD = "implicit_account.decrypt.wrong_password"
}
