export interface TezosAccount {
    id: string;
    name: string;
    accountType: any;
    address: string;
    publicKey: string;
    derivationPath?: string;
    privateKey?: string;
    encryptedContent?: string;
    preferredRpc?: string;
}
export default class AccountService {
    private accounts;
    LOCAL_STORAGE_KEY: string;
    constructor();
    /**
     *  Find all implicit accounts.
     *  @returns {{ [address: string]: TezosAccount }}
     */
    findAll: (accountTypes?: string[], searchTerm?: string) => {
        [address: string]: TezosAccount;
    };
    /**
     *  Find implicit account by identifier.
     *  @param {string} id account identifier.
     *  @returns {TezosAccount}
     */
    findById: (id: string) => TezosAccount;
    /**
     *  Add an implicit account.
     *  @param {TezosAccount} account tezos implicit account.
     *  @param {string?} password an optional password encoded with btoa to encrypt the account.
     */
    addAccount: (account: TezosAccount, password?: string) => void;
    /**
     *  Update a implicit account's name.
     *  @param {string} id account's identifier.
     *  @param {string} name account's new name.
     */
    updateName: (id: string, name: string) => void;
    /**
     *  Update a implicit account's name.
     *  @param {string} id account's identifier.
     *  @param {string} preferredRpc account's preferred rpc.
     */
    updatePreferredRpc: (id: string, preferredRpc: string) => void;
    /**
     *  Remove a implicit account.
     *  @param {string} id Account identifier.
     */
    removeById: (id: string) => void;
    /**
     * Return the decrypted account.
     *  @param {TezosAccount} account tezos implicit account.
     *  @param {string?} password an optional password encoded with btoa to decrypt the account.
     *  @returns {TezosAccount}
     */
    getDecryptedAccount: (account: TezosAccount, password: string) => TezosAccount;
    /**
     *  Clear the storage.
     */
    clear: () => void;
    /**
     * Persist to local storage
     */
    private persistLocalStorage;
    /**
     * Encrypt an account content or return the encrypted content
     */
    private getEncryptedAccount;
    /**
     * Reload the whole accounts from local storage.
     */
    reloadFromStorage: () => void;
}
