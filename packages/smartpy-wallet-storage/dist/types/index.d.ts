import AccountService from './services/account';
declare const Wallet: {
    contract: {
        findAll: (network?: string | undefined, deployedOnly?: boolean | undefined, searchTerm?: string | undefined) => {
            [address: string]: import("./services/contract/storage").OriginatedContract;
        };
        findById: (id: string) => import("./services/contract/storage").OriginatedContract;
        findByAddress: (address: string) => import("./services/contract/storage").OriginatedContract;
        persist: (contract: import("./services/contract/storage").OriginatedContract) => void;
        merge: (contract: import("./services/contract/storage").OriginatedContract) => void;
        removeById: (id: string) => void;
        select: (id: string) => void;
        getSelected: () => import("./services/contract/storage").OriginatedContract | undefined;
        clear: () => void;
    };
    account: AccountService;
    faucet: {
        findAll: (searchTerm?: string | undefined) => {
            [address: string]: import("./services/faucet/storage").FaucetAccount;
        };
        findById: (id: string) => import("./services/faucet/storage").FaucetAccount;
        persist: (account: import("./services/faucet/storage").FaucetAccount) => void;
        merge: (account: import("./services/faucet/storage").FaucetAccount) => void;
        removeById: (id: string) => void;
        select: (id: string) => void;
        getSelected: () => import("./services/faucet/storage").FaucetAccount | undefined;
        clear: () => void;
    };
    constants: SmartPyConstants;
};
export default Wallet;
export { TezosAccount } from './services/account';
