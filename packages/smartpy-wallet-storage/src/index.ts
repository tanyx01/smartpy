import ContractService from './services/contract';
import AccountService from './services/account';
import Constants from './constants';
import FaucetService from './services/faucet';

import pkg from '../package.json';

window.localStorage.setItem(
    'wallet-metadata2',
    JSON.stringify({
        version: pkg.version,
    }),
);

const Wallet = {
    contract: ContractService,
    account: new AccountService(),
    faucet: FaucetService,
    constants: Constants,
};

export default Wallet;

export { TezosAccount } from './services/account';

