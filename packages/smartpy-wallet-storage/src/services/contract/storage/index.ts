export interface ContractsStorageFormat {
    selectedContract?: OriginatedContract;
    contracts: {
        [id: string]: OriginatedContract;
    };
}

export interface OriginatedContract {
    id?: string;
    name: string;
    address?: string;
    network?: string;
    storage?: string;
    code?: string;
}

export default class ContractsStorage {
    public CONTRACTS_STORAGE_KEY = 'contracts2';
    private contractsStorage = initialContractsStorage;

    constructor() {
        // Initiate faucet storage if empty
        if (!window.localStorage.getItem(this.CONTRACTS_STORAGE_KEY)) {
            const oldContracts = window.localStorage.getItem('contracts');
            if (oldContracts) {
                window.localStorage.setItem(this.CONTRACTS_STORAGE_KEY, oldContracts);
            } else {
                window.localStorage.setItem(this.CONTRACTS_STORAGE_KEY, JSON.stringify(this.contractsStorage));
            }
        }
    }

    /**
     *  Get Contracts Storage.
     *  @returns {ContractsStorageFormat}
     */
    public get = (): ContractsStorageFormat => {
        const storage = window.localStorage.getItem(this.CONTRACTS_STORAGE_KEY);
        return storage ? JSON.parse(storage) : {};
    };

    /**
     *  Persist new storage.
     *  @param {ContractsStorageFormat} storage new storage.
     */
    public persist = (storage: ContractsStorageFormat): void => {
        this.contractsStorage = JSON.parse(JSON.stringify(storage));
        window.localStorage.setItem(this.CONTRACTS_STORAGE_KEY, JSON.stringify(this.contractsStorage));
    };

    /**
     *  Clear storage.
     */
    public clear = (): void => {
        this.contractsStorage = initialContractsStorage;
        window.localStorage.setItem(this.CONTRACTS_STORAGE_KEY, JSON.stringify(this.contractsStorage));
    };
}

const initialContractsStorage: ContractsStorageFormat = {
    contracts: {},
};
