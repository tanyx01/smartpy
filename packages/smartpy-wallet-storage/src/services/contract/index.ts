import OriginatedContractError, { OriginatedContractErrorCodes } from './exceptions/OriginatedContractError';
import ContractsStorage, { ContractsStorageFormat, OriginatedContract } from './storage';
import { generateID } from '../../utils/rand';

const contractsStorage = new ContractsStorage();

const ContractService = {
    /**
     *  Find all contracts.
     *  @param {boolean} deployedOnly answer with originated contracts only.
     *  @param {string} network
     *  @param {string} contractNameTerm search term
     *  @returns {{ [address: string]: OriginatedContract }}
     */
    findAll: (
        network?: string,
        deployedOnly?: boolean,
        searchTerm?: string,
    ): { [address: string]: OriginatedContract } => {
        let contracts = contractsStorage.get().contracts;

        // Apply contract (name U address) filter if provided
        if (searchTerm) {
            contracts = Object.keys(contracts).reduce<{ [address: string]: OriginatedContract }>((prev, cur) => {
                if (
                    contracts[cur].name.toLowerCase().startsWith(searchTerm.toLowerCase()) ||
                    contracts[cur].address?.toLowerCase()?.startsWith(searchTerm.toLowerCase())
                ) {
                    prev[cur] = contracts[cur];
                }
                return prev;
            }, {});
        }
        // Apply network filter if provided
        if (network) {
            contracts = Object.keys(contracts).reduce((pState, key) => {
                if (contracts[key].network == network) {
                    pState = {
                        ...pState,
                        [key]: contracts[key],
                    };
                }
                return pState;
            }, {});
        }

        return deployedOnly
            ? contracts
            : Object.keys(contracts).reduce((pState, key) => {
                  if (contracts[key].address) {
                      pState = {
                          ...pState,
                          [key]: contracts[key],
                      };
                  }
                  return pState;
              }, {});
    },
    /**
     *  Find originated contract by identifier.
     *  @param {string} id contract identifier
     *  @returns {OriginatedContract}
     */
    findById: (id: string): OriginatedContract => {
        // Get storage
        const storage = contractsStorage.get();
        // Contract must exist
        throwIfContractNotFound(storage, id);
        return storage.contracts[id];
    },
    /**
     *  Find originated contract by address.
     *  @param {string} address contract address.
     *  @returns {OriginatedContract}
     */
    findByAddress: (address: string): OriginatedContract => {
        // Get storage
        const storage = contractsStorage.get();
        // Contract must exist
        for (const id of Object.keys(storage.contracts)) {
            if (storage.contracts[id].address == address) {
                return storage.contracts[id];
            }
        }
        throw new OriginatedContractError(OriginatedContractErrorCodes.CONTRACT_NOT_FOUND);
    },
    /**
     *  Persist the originated contract.
     *  @param {OriginatedContract} contract originated contract.
     */
    persist: (contract: OriginatedContract): void => {
        validateContractInput(contract);
        // Get storage
        const storage = contractsStorage.get();
        contract.id = generateID();
        // Save mutation
        storage.contracts = {
            ...storage.contracts,
            [contract.id]: contract,
        };
        contractsStorage.persist(storage);
    },
    /**
     *  Update a originated contract.
     *  @param {OriginatedContract} contract originated contract.
     */
    merge: (contract: OriginatedContract): void => {
        validateContractInput(contract);
        if (contract.id) {
            // Get storage
            const storage = contractsStorage.get();
            // Contract must exist
            throwIfContractNotFound(storage, contract.id);
            // Save mutation
            storage.contracts = {
                ...storage.contracts,
                [contract.id]: contract,
            };
            contractsStorage.persist(storage);
        }
    },
    /**
     *  Remove a originated contract.
     *  @param {string} id contract identifier
     */
    removeById: (id: string): void => {
        // Get storage
        const storage = contractsStorage.get();
        // Contract must exist
        throwIfContractNotFound(storage, id);
        // Delete contract and persist
        delete storage.contracts[id];
        contractsStorage.persist(storage);
    },
    /**
     *  Select originated contract.
     *  @param {string} id contract identifier
     */
    select: (id: string): void => {
        // Get storage
        const storage = contractsStorage.get();
        // Contract must exist
        throwIfContractNotFound(storage, id);
        storage.selectedContract = storage.contracts[id];
        contractsStorage.persist(storage);
    },
    /**
     *  Get selected contract.
     *  @returns {OriginatedContract}
     */
    getSelected: (): OriginatedContract | undefined => {
        return contractsStorage.get().selectedContract;
    },
    /**
     *  Clear the storage.
     */
    clear: (): void => contractsStorage.clear(),
};

/**
 *  Validate contract input.
 *  @param {OriginatedContract} contract originated contract.
 */
const validateContractInput = (contract: OriginatedContract) => {
    // Contract must not be null
    if (!contract?.address) {
        throw new OriginatedContractError(OriginatedContractErrorCodes.CONTRACT_NULL);
    }
    // Contract must have an address
    validateContractAddress(contract.address);
};

/**
 *  Validate contract address.
 *  @param {string} address contract address.
 */
const validateContractAddress = (address: string) => {
    // Contract address must not be null
    if (!address) {
        throw new OriginatedContractError(OriginatedContractErrorCodes.CONTRACT_ADDRESS_NULL);
    }
};

/**
 *  Throw if the contract doesn't exist.
 *  @param {ContractsStorageFormat} storage storage state.
 *  @param {string} id contract identifier
 *  @throws {OriginatedContractError} if the contract doesn't exist.
 */
const throwIfContractNotFound = (storage: ContractsStorageFormat, id: string) => {
    if (!storage.contracts[id]) {
        throw new OriginatedContractError(OriginatedContractErrorCodes.CONTRACT_NOT_FOUND);
    }
};

export default ContractService;
