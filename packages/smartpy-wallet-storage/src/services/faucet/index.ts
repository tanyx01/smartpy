import FaucetError, { FaucetErrorCodes } from './exceptions/FaucetError';
import FaucetStorage, { FaucetAccount } from './storage';
import { generateID } from '../../utils/rand';

const faucetStorage = new FaucetStorage();

const FaucetService = {
    /**
     *  Find all faucet accounts.
     *  @returns {{ [address: string]: FaucetAccount }}
     */
    findAll: (searchTerm?: string): { [address: string]: FaucetAccount } => {
        let { faucet } = faucetStorage.get();

        // Apply account (name U address) filter if provided
        if (searchTerm) {
            faucet = Object.keys(faucet).reduce<{ [address: string]: FaucetAccount }>((prev, cur) => {
                if (
                    faucet[cur].name.toLowerCase().startsWith(searchTerm.toLowerCase()) ||
                    faucet[cur].address.toLowerCase().startsWith(searchTerm.toLowerCase())
                ) {
                    prev[cur] = faucet[cur];
                }
                return prev;
            }, {});
        }
        return faucet;
    },
    /**
     *  Find faucet account by identifier.
     *  @param {string} id account identifier.
     *  @returns {FaucetAccount}
     */
    findById: (id: string): FaucetAccount => {
        // Get storage
        const { faucet } = faucetStorage.get();
        // Faucet must exist
        throwIfFaucetNotFound(faucet, id);
        return faucet[id];
    },
    /**
     *  Persist a faucet account.
     *  @param {FaucetAccount} account faucet account.
     */
    persist: (account: FaucetAccount): void => {
        validateFaucetInput(account);
        // Get faucet storage
        const storage = faucetStorage.get();
        account.id = generateID();
        // Save mutation
        storage.faucet = {
            ...storage.faucet,
            [account.id]: account,
        };
        faucetStorage.persist(storage);
    },
    /**
     *  Update a faucet account.
     *  @param {FaucetAccount} account faucet account.
     */
    merge: (account: FaucetAccount): void => {
        validateFaucetInput(account);
        if (account.id) {
            // Get storage
            const storage = faucetStorage.get();
            // Faucet must exist
            throwIfFaucetNotFound(storage.faucet, account.id);
            // Save mutation
            storage.faucet = {
                ...storage.faucet,
                [account.id]: account,
            };
            faucetStorage.persist(storage);
        }
    },
    /**
     *  Remove a faucet account.
     *  @param {string} id account identifier.
     */
    removeById: (id: string): void => {
        // Get storage
        const storage = faucetStorage.get();
        // Faucet must exist
        throwIfFaucetNotFound(storage.faucet, id);
        // Delete faucet account and persist
        delete storage.faucet[id];
        faucetStorage.persist(storage);
    },
    /**
     *  Select faucet account.
     *  @param {string} id account identifier.
     */
    select: (id: string): void => {
        // Get storage
        const storage = faucetStorage.get();
        // Faucet must exist
        throwIfFaucetNotFound(storage.faucet, id);
        storage.selectedFaucet = storage.faucet[id];
        faucetStorage.persist(storage);
    },
    /**
     *  Get selected faucet account.
     *  @returns {FaucetAccount}
     */
    getSelected: (): FaucetAccount | undefined => {
        return faucetStorage.get().selectedFaucet;
    },
    /**
     *  Clear the storage.
     */
    clear: (): void => faucetStorage.clear(),
};

/**
 *  Validate account input.
 *  @param {FaucetAccount} account faucet account.
 */
const validateFaucetInput = (account: FaucetAccount) => {
    // Faucet account must not be null
    if (!account) {
        throw new FaucetError(FaucetErrorCodes.FAUCET_ACCOUNT_NULL);
    }
    // Faucet must have an address
    validateFaucetAddress(account.address);
};

/**
 *  Validate faucet account address.
 *  @param {string} address account address.
 */
const validateFaucetAddress = (address: string) => {
    // Faucet address must not be null
    if (!address) {
        throw new FaucetError(FaucetErrorCodes.FAUCET_ACCOUNT_ADDRESS_NULL);
    }
};

/**
 *  Throw if the faucet account doesn't exist.
 *  @param {Map<string, FaucetAccount>} faucet faucet accounts.
 *  @param {string} id account identifier.
 *  @throws {FaucetError} if the faucet account doesn't exist.
 */
const throwIfFaucetNotFound = (faucet: Record<string, FaucetAccount>, id: string) => {
    if (!faucet[id]) {
        throw new FaucetError(FaucetErrorCodes.FAUCET_ACCOUNT_NOT_FOUND);
    }
};

export default FaucetService;
