export interface FaucetStorageFormat {
    selectedFaucet?: FaucetAccount;
    faucet: {
        [id: string]: FaucetAccount;
    };
}

export interface FaucetAccount {
    id?: string;
    name: string;
    address: string;
    publicKey: string;
    privateKey: string;
    secret: string;
}

export default class FaucetStorage {
    public FAUCET_STORAGE_KEY = 'faucet2';
    private faucetStorage = initialFaucetStorage;

    constructor() {
        // Initiate faucet storage if empty
        if (!window.localStorage.getItem(this.FAUCET_STORAGE_KEY)) {
            const oldFaucet = window.localStorage.getItem('faucet');
            if (oldFaucet) {
                window.localStorage.setItem(this.FAUCET_STORAGE_KEY, oldFaucet);
            } else {
                window.localStorage.setItem(this.FAUCET_STORAGE_KEY, JSON.stringify(this.faucetStorage));
            }
        }
    }

    /**
     *  Get Faucet Storage.
     *  @returns {FaucetStorageFormat}
     */
    public get = (): FaucetStorageFormat => {
        const storage = window.localStorage.getItem(this.FAUCET_STORAGE_KEY);
        return storage ? JSON.parse(storage) : {};
    };

    /**
     *  Persist new storage.
     *  @param {FaucetStorageFormat} storage new storage.
     */
    persist = (storage: FaucetStorageFormat): void => {
        this.faucetStorage = JSON.parse(JSON.stringify(storage));
        window.localStorage.setItem(this.FAUCET_STORAGE_KEY, JSON.stringify(this.faucetStorage));
    };

    /**
     *  Clear storage.
     */
    clear = (): void => {
        this.faucetStorage = initialFaucetStorage;
        window.localStorage.setItem(this.FAUCET_STORAGE_KEY, JSON.stringify(this.faucetStorage));
    };
}

const initialFaucetStorage: FaucetStorageFormat = {
    faucet: {},
};
