export default class FaucetError extends Error {
    constructor(message: FaucetErrorCodes) {
        super(message); // 'Error' breaks prototype chain here

        Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
        this.name = 'FaucetError';
    }
}

export enum FaucetErrorCodes {
    FAUCET_ACCOUNT_ALREADY_EXISTS = 'implicit_account.already_exists',
    FAUCET_ACCOUNT_NOT_FOUND = 'implicit_account.not_found',
    FAUCET_ACCOUNT_NULL = 'implicit_account.must_not_be_null',
    FAUCET_ACCOUNT_ADDRESS_NULL = 'implicit_account.fields.address.null',
}
