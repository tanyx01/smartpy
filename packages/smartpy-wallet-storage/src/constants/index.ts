import { TezosNetwork } from './Networks';
import { AccountType } from './AccountTypes';

const Constants: SmartPyConstants = {
    networks: TezosNetwork,
    accountTypes: AccountType,
};

export default Constants;
