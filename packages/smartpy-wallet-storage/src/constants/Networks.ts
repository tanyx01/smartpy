export enum TezosNetwork {
    MAINNET = 'Mainnet',
    ZERONET = 'Zeronet',
    BABYLONNET = 'Babylonnet',
    CARTHAGENET = 'Carthagenet',
    LABNET = 'Labnet',
}
