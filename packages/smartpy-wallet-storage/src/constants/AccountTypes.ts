export enum AccountType {
    NORMAL = 'Normal',
    LEDGER = 'Ledger',
    KEYLESS = 'Keyless',
}
