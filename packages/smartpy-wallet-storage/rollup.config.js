import path from 'path';
import typescript from '@rollup/plugin-typescript';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';

export default {
    input: 'src/index.ts',
    output: [
        {
            file: `${pkg.main}`,
            format: 'cjs',
            exports: 'named',
            banner: `/* === Version: ${pkg.version} === */`,
        },
        {
            file: `${pkg.module}`,
            format: 'es',
            exports: 'named',
            banner: `/* === Version: ${pkg.version} === */`,
        },
        {
            name: 'SmartpyWallet',
            file: `dist/index.min.js`,
            format: 'iife',
            sourcemap: 'inline',
            compact: true,
            banner: `/* === Version: ${pkg.version} === */`,
            globals: { crypto: 'crypto' },
        },
    ],
    plugins: [
        typescript({
            tsconfig: path.resolve(__dirname, './tsconfig.build.json'),
        }),
        terser(),
        resolve({ preferBuiltins: true, browser: true }),
        commonjs(),
        json(),
    ],
};
