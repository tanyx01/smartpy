import TezosWallet from '../../src/index';
import ImplicitAccountError, {
    ImplicitAccountErrorCodes,
} from '../../src/services/account/exceptions/ImplicitAccountError';
import { AccountType } from '../../src/constants/AccountTypes';
import AccountService, { TezosAccount } from '../../src/services/account';

const DUMMY_PASSWORD = 'PASSWORD';
const DUMMY_PRIVATE_KEY = 'edskRxyk39iQ62KbSPwapZDsqEZSQPqBB21SRVjRem1kBjAfSiUUpDoz9ywsj4uJPEoeJnzAC2WfxKXEHKC2hP8MaqeK3Y6nnG';
const STORAGE_KEY = 'wallet';

const dummyImplicitAccount = (overrides: any = {}): TezosAccount => {
    const rand =
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15);
    return {
        name: `ImplicitAccount_${rand}`,
        address: rand,
        accountType: AccountType.NORMAL,
        publicKey: 'edpkuwfvN4wxaFHraKHY3mjqdLT9N8jmU3n4nvdiN5FCuDqYgXLy85',
        ...overrides,
    };
};

beforeEach(() => {
    TezosWallet.account.clear();
});

describe('Account Service', () => {
    /**
     *  Reuse an already existent storage.
     */
    it('Reuse an already existent storage.', () => {
        TezosWallet.account = new AccountService();
    });
    /**
     *  Persist implicit account.
     */
    it('Add implicit account.', () => {
        const account = dummyImplicitAccount();
        TezosWallet.account.addAccount(account);

        expect(TezosWallet.account.findById(account.id)).toStrictEqual(account);
    });
    /**
     *  Persist a null account (Expect Exception).
     */
    it('Add a null account (Expect Exception).', () => {
        expect(() => TezosWallet.account.addAccount(null as any)).toThrow(
            new ImplicitAccountError(ImplicitAccountErrorCodes.ACCOUNT_NULL),
        );
    });
    /**
     *  Validate account input (Expect Exception).
     */
    it('Validate account input (Expect Exception).', () => {
        const account = dummyImplicitAccount() as any;
        delete account.address;
        expect(() => TezosWallet.account.addAccount(account)).toThrow(
            new ImplicitAccountError(ImplicitAccountErrorCodes.ACCOUNT_ADDRESS_NULL),
        );
    });
    /**
     *  Update implicit account.
     */
    it('Update account name.', () => {
        TezosWallet.account.addAccount(dummyImplicitAccount());
        const accounts = TezosWallet.account.findAll();
        const account = accounts[Object.keys(accounts)[0]];

        expect(TezosWallet.account.findById(account.id)).toStrictEqual(account);

        TezosWallet.account.updateName(account.id, "update_name");

        expect(TezosWallet.account.findById(account.id).name).toStrictEqual("update_name");
    });
    /**
     *  Find all implicit accounts.
     */
    it('Find all implicit accounts.', () => {
        TezosWallet.account.addAccount(dummyImplicitAccount());
        expect(Object.keys(TezosWallet.account.findAll())).toHaveLength(1);
        TezosWallet.account.addAccount(dummyImplicitAccount());
        expect(Object.keys(TezosWallet.account.findAll())).toHaveLength(2);
    });
    /**
     *  Find all filtered implicit accounts.
     */
    it('Find all filtered implicit accounts.', () => {
        const dummy_type_normal = dummyImplicitAccount();
        const dummy_type_normal2 = dummyImplicitAccount();
        const dummy_type_ledger = dummyImplicitAccount({
            accountType: AccountType.LEDGER,
        });

        TezosWallet.account.addAccount(dummy_type_normal);
        TezosWallet.account.addAccount(dummy_type_normal2);
        TezosWallet.account.addAccount(dummy_type_ledger);

        expect(Object.keys(TezosWallet.account.findAll())).toHaveLength(3);

        expect(Object.keys(TezosWallet.account.findAll([AccountType.NORMAL]))).toHaveLength(2);

        // Filter By account type and account name
        expect(Object.keys(TezosWallet.account.findAll([AccountType.NORMAL], dummy_type_normal.name))).toHaveLength(1);

        // Filter By account type and account name
        expect(Object.keys(TezosWallet.account.findAll([AccountType.NORMAL], dummy_type_normal.address))).toHaveLength(
            1,
        );
    });
    /**
     *  Remove implicit account by address.
     */
    it('Remove implicit account by address.', () => {
        const account = dummyImplicitAccount();
        TezosWallet.account.addAccount(account);
        expect(Object.keys(TezosWallet.account.findAll())).toHaveLength(1);
        TezosWallet.account.removeById(account.id);
        expect(Object.keys(TezosWallet.account.findAll())).toHaveLength(0);
    });
    /**
     *  Remove nonexistent implicit account by address (Expect Exception).
     */
    it('Remove nonexistent implicit account by address (Expect Exception).', () => {
        expect(() => TezosWallet.account.removeById(dummyImplicitAccount().id)).toThrow(
            new ImplicitAccountError(ImplicitAccountErrorCodes.ACCOUNT_NOT_FOUND),
        );
    });
    /**
     *  Add account with password.
     */
    it('Add account with password.', () => {
        TezosWallet.account.clear();
        TezosWallet.account.addAccount(
            dummyImplicitAccount({ privateKey: DUMMY_PRIVATE_KEY }), DUMMY_PASSWORD);
        const accounts = TezosWallet.account.findAll();
        const account = accounts[Object.keys(accounts)[0]];
        expect(account.privateKey).toBeUndefined;
        expect(account.encryptedContent).not.toBeUndefined;
    });
    /**
     *  Get decrypted account with correct password.
     */
    it('Get decrypted account storage with correct password.', () => {
        TezosWallet.account.clear();
        TezosWallet.account.addAccount(
            dummyImplicitAccount({ privateKey: DUMMY_PRIVATE_KEY }), DUMMY_PASSWORD);
        const accounts = TezosWallet.account.findAll();
        const account = accounts[Object.keys(accounts)[0]];
        const decryptedAccount = TezosWallet.account.getDecryptedAccount(account, DUMMY_PASSWORD);
        expect(decryptedAccount.privateKey).toStrictEqual(DUMMY_PRIVATE_KEY);
    });
    /**
     *  Get decrypted account with incorrect password.
     */
    it('Get decrypted account with incorrect password.', () => {
        TezosWallet.account.clear();
        TezosWallet.account.addAccount(
            dummyImplicitAccount({ privateKey: DUMMY_PRIVATE_KEY }), DUMMY_PASSWORD);
        const accounts = TezosWallet.account.findAll();
        const account = accounts[Object.keys(accounts)[0]]
        expect(() => TezosWallet.account.getDecryptedAccount(account, "INCORRECT_PASSWORD")).toThrow(
            new ImplicitAccountError(ImplicitAccountErrorCodes.INCORRECT_PASSWORD),
        );
    });
    /**
     *  Get decrypted account without password.
     */
    it('Get decrypted account without password.', () => {
        TezosWallet.account.clear();
        TezosWallet.account.addAccount(
            dummyImplicitAccount({ privateKey: DUMMY_PRIVATE_KEY }), DUMMY_PASSWORD);
        const accounts = TezosWallet.account.findAll();
        const account = accounts[Object.keys(accounts)[0]]
        expect(() => TezosWallet.account.getDecryptedAccount(account, '')).toThrow(
            new ImplicitAccountError(ImplicitAccountErrorCodes.CANNOT_DECRYPT_WITHOUT_PASSWORD),
        );
    });
});
