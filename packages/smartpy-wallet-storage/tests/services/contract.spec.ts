import TezosWallet from '../../src';
import OriginatedContractError, {
    OriginatedContractErrorCodes,
} from '../../src/services/contract/exceptions/OriginatedContractError';
import { OriginatedContract } from '../../src/services/contract/storage';

const dummyOriginatedContract = (): OriginatedContract => {
    const rand =
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15);
    return {
        name: `OriginatedContract_${rand}`,
        address: rand,
    };
};

beforeEach(() => {
    TezosWallet.contract.clear();
});

describe('Contract Service', () => {
    /**
     *  Save originated contract.
     */
    it('Persist originated contract.', () => {
        TezosWallet.contract.persist(dummyOriginatedContract());
        const contracts = TezosWallet.contract.findAll();
        const contract = contracts[Object.keys(contracts)[0]];

        expect(TezosWallet.contract.findById(contract.id as any)).toStrictEqual(contract);
    });
    /**
     *  Persist a null contract (Expect Exception).
     */
    it('Persist a null contract (Expect Exception).', () => {
        expect(() => TezosWallet.contract.persist(null as any)).toThrow(
            new OriginatedContractError(OriginatedContractErrorCodes.CONTRACT_NULL),
        );
    });
    /**
     *  Validate contract input (Expect Exception).
     */
    it('Validate contract input (Expect Exception).', () => {
        const contract = dummyOriginatedContract();
        delete contract.address;
        expect(() => TezosWallet.contract.persist(contract)).toThrow(
            new OriginatedContractError(OriginatedContractErrorCodes.CONTRACT_NULL),
        );
    });
    /**
     *  Update originated contract.
     */
    it('Update originated contract.', () => {
        TezosWallet.contract.persist(dummyOriginatedContract());
        const contracts = TezosWallet.contract.findAll();
        const contract = contracts[Object.keys(contracts)[0]];

        expect(TezosWallet.contract.findById(contract.id as any)).toStrictEqual(contract);

        const updatedContract = dummyOriginatedContract();
        updatedContract.id = contract.id;
        TezosWallet.contract.merge(updatedContract);

        expect(TezosWallet.contract.findById(contract.id as any)).toStrictEqual(updatedContract);
    });
    /**
     *  Find all originated contracts.
     */
    it('Find all originated contracts.', () => {
        TezosWallet.contract.persist(dummyOriginatedContract());
        expect(Object.keys(TezosWallet.contract.findAll())).toHaveLength(1);
        TezosWallet.contract.persist(dummyOriginatedContract());
        expect(Object.keys(TezosWallet.contract.findAll())).toHaveLength(2);
    });
    /**
     *  Remove originated contract by identifier.
     */
    it('Remove originated contract by identifier.', () => {
        TezosWallet.contract.persist(dummyOriginatedContract());
        const contracts = TezosWallet.contract.findAll();
        const contract = contracts[Object.keys(contracts)[0]];
        expect(Object.keys(TezosWallet.contract.findAll())).toHaveLength(1);
        TezosWallet.contract.removeById(contract.id as any);
        expect(Object.keys(TezosWallet.contract.findAll())).toHaveLength(0);
    });
    /**
     *  Remove nonexistent originated contract by identifier (Expect Exception).
     */
    it('Remove nonexistent originated contract by identifier (Expect Exception).', () => {
        expect(() => TezosWallet.contract.removeById('DUMMY')).toThrow(
            new OriginatedContractError(OriginatedContractErrorCodes.CONTRACT_NOT_FOUND),
        );
    });
    /**
     *  Select and get selected contract.
     */
    it('Select and get selected contract.', () => {
        TezosWallet.contract.persist(dummyOriginatedContract());
        const contracts = TezosWallet.contract.findAll();
        const contract = contracts[Object.keys(contracts)[0]];
        TezosWallet.contract.select(contract.id as any);
        expect(TezosWallet.contract.getSelected()).toStrictEqual(contract);
    });
});
