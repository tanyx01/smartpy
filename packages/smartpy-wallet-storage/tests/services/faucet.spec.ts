import TezosWallet from '../../src/index';
import FaucetError, { FaucetErrorCodes } from '../../src/services/faucet/exceptions/FaucetError';
import { FaucetAccount } from '../../src/services/faucet/storage';

const dummyFaucetAccount = (overrides: any = {}): FaucetAccount => {
    const rand =
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15);
    return {
        name: `ImplicitAccount_${rand}`,
        address: rand,
        publicKey: 'edpkuwfvN4wxaFHraKHY3mjqdLT9N8jmU3n4nvdiN5FCuDqYgXLy85',
        ...overrides,
    };
};

beforeEach(() => {
    TezosWallet.faucet.clear();
});

describe('Faucet Service', () => {
    /**
     *  Persist faucet account.
     */
    it('Persist implicit account.', () => {
        TezosWallet.faucet.persist(dummyFaucetAccount());
        const accounts = TezosWallet.faucet.findAll();
        const account = accounts[Object.keys(accounts)[0]];

        expect(TezosWallet.faucet.findById(account.id as any)).toStrictEqual(account);
    });
    /**
     *  Persist a null faucet account (Expect Exception).
     */
    it('Persist a null faucet account (Expect Exception).', () => {
        expect(() => TezosWallet.faucet.persist(null as any)).toThrow(
            new FaucetError(FaucetErrorCodes.FAUCET_ACCOUNT_NULL),
        );
    });
    /**
     *  Validate faucet account input (Expect Exception).
     */
    it('Validate faucet account input (Expect Exception).', () => {
        const account = dummyFaucetAccount() as any;
        delete account.address;
        expect(() => TezosWallet.faucet.persist(account)).toThrow(
            new FaucetError(FaucetErrorCodes.FAUCET_ACCOUNT_ADDRESS_NULL),
        );
    });
    /**
     *  Update faucet account.
     */
    it('Update faucet account.', () => {
        TezosWallet.faucet.persist(dummyFaucetAccount());
        const accounts = TezosWallet.faucet.findAll();
        const account = accounts[Object.keys(accounts)[0]];

        expect(TezosWallet.faucet.findById(account.id as any)).toStrictEqual(account);

        const updatedAccount = dummyFaucetAccount();
        updatedAccount.id = account.id;
        TezosWallet.faucet.merge(updatedAccount);

        expect(TezosWallet.faucet.findById(account.id as any)).toStrictEqual(updatedAccount);
    });
    /**
     *  Find all faucet accounts.
     */
    it('Find all faucet accounts.', () => {
        TezosWallet.faucet.persist(dummyFaucetAccount());
        expect(Object.keys(TezosWallet.faucet.findAll())).toHaveLength(1);
        TezosWallet.faucet.persist(dummyFaucetAccount());
        expect(Object.keys(TezosWallet.faucet.findAll())).toHaveLength(2);
    });
    /**
     *  Find all filtered faucet accounts.
     */
    it('Find all filtered faucet accounts.', () => {
        const dummy_account1 = dummyFaucetAccount();
        const dummy_account2 = dummyFaucetAccount();

        TezosWallet.faucet.persist(dummy_account1);
        TezosWallet.faucet.persist(dummy_account2);

        expect(Object.keys(TezosWallet.faucet.findAll())).toHaveLength(2);

        expect(Object.keys(TezosWallet.faucet.findAll("I DON'T EXIST"))).toHaveLength(0);

        expect(Object.keys(TezosWallet.faucet.findAll(dummy_account1.name))).toHaveLength(1);
    });
    /**
     *  Remove faucet account by address.
     */
    it('Remove faucet account by address.', () => {
        TezosWallet.faucet.persist(dummyFaucetAccount());
        const accounts = TezosWallet.faucet.findAll();
        const account = accounts[Object.keys(accounts)[0]];
        expect(Object.keys(TezosWallet.faucet.findAll())).toHaveLength(1);
        TezosWallet.faucet.removeById(account.id as any);
        expect(Object.keys(TezosWallet.faucet.findAll())).toHaveLength(0);
    });
    /**
     *  Remove nonexistent faucet account by address (Expect Exception).
     */
    it('Remove nonexistent faucet account by address (Expect Exception).', () => {
        expect(() => TezosWallet.faucet.removeById('DUMMY')).toThrow(
            new FaucetError(FaucetErrorCodes.FAUCET_ACCOUNT_NOT_FOUND),
        );
    });
    /**
     *  Select and get selected faucet account.
     */
    it('Select and get selected faucet account.', () => {
        TezosWallet.faucet.persist(dummyFaucetAccount());
        const accounts = TezosWallet.faucet.findAll();
        const account = accounts[Object.keys(accounts)[0]];
        TezosWallet.faucet.select(account.id as any);
        expect(TezosWallet.faucet.getSelected()).toStrictEqual(account);
    });
});
