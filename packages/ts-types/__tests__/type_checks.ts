export type Expect<T extends true> = T
export type ExpectTrue<T extends true> = T
export type ExpectFalse<T extends false> = T
export type IsTrue<T extends true> = T
export type IsFalse<T extends false> = T

export type Equal<X, Y> =
  (<T>() => T extends X ? 1 : 2) extends
  (<T>() => T extends Y ? 1 : 2) ? true : false
export type NotEqual<X, Y> = true extends Equal<X, Y> ? false : true

type type_checks = [
    // TInt
    Expect<Equal<TInt, TInt>>,
    Expect<NotEqual<TInt, TNat>>,
    Expect<NotEqual<TInt, TMutez>>,
    Expect<NotEqual<TInt, TTimestamp>>,
    Expect<NotEqual<TInt, TBls12_381_fr>>,
    // TNat
    Expect<Equal<TNat, TNat>>,
    Expect<NotEqual<TNat, TInt>>,
    Expect<NotEqual<TNat, TMutez>>,
    Expect<NotEqual<TNat, TTimestamp>>,
    Expect<NotEqual<TNat, TBls12_381_fr>>,
];

/**
 * TNat
 */
const nat: TNat = 10;
// Minus
let nat_minus_int: TInt = nat.minus(1 as TInt);
let nat_minus_nat: TInt = nat.minus(1 as TNat);
// Multiply
let nat_multiply_int: TInt = nat.multiply(1 as TInt);
let nat_multiply_nat: TNat = nat.multiply(1 as TNat);
let nat_multiply_mutez: TMutez = nat.multiply(1 as TMutez);
let nat_multiply_bls12_381_fr_1: TBls12_381_fr = nat.multiply(1 as TBls12_381_fr);
let nat_multiply_bls12_381_fr_2: TBls12_381_fr = nat.multiply("0x00" as TBls12_381_fr);
// Plus
let nat_plus_nat: TNat = nat.plus(1 as TNat);
let nat_plus_int: TInt = nat.plus(1 as TInt);
// toInt
let nat_to_int: TInt = nat.toInt();
// toMutez
let nat_to_mutez: TMutez = nat.toMutez();

/**
 * TInt
 */
const int: TInt = 10;
// Minus
let int_minus_int: TInt = int.minus(1 as TInt);
let int_minus_nat: TInt = int.minus(1 as TNat);
// Multiply
let int_multiply_int: TInt = int.multiply(1 as TInt);
let int_multiply_nat: TInt = int.multiply(1 as TNat);
let int_multiply_mutez: TMutez = int.multiply(1 as TMutez);
let int_multiply_bls12_381_fr_1: TBls12_381_fr = int.multiply(1 as TBls12_381_fr);
let int_multiply_bls12_381_fr_2: TBls12_381_fr = int.multiply("0x00" as TBls12_381_fr);
// Plus
let int_plus_nat: TInt = int.plus(1 as TNat);
let int_plus_int: TInt = int.plus(1 as TInt);
// toInt
let int_to_nat: TNat = int.toNat();
// abs
let int_abs: TNat = int.abs();
