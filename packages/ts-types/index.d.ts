/* eslint-disable @typescript-eslint/no-empty-interface */
/* eslint-disable @typescript-eslint/ban-types */

// + Typescript CORE

type ReturnType<T> = T extends (...args: any[]) => infer R ? R : any;
type Equal<X, Y> = (<T>() => T extends X ? 1 : 2) extends (<T>() => T extends Y ? 1 : 2) ? true : false;

interface SymbolConstructor {
    /**
     * A method that returns the default iterator for an object. Called by the semantics of the
     * for-of statement.
     */
    readonly iterator: unique symbol;
}

interface IteratorYieldResult<TYield> {
    done?: false;
    value: TYield;
}

interface IteratorReturnResult<TReturn> {
    done: true;
    value: TReturn;
}

type IteratorResult<T, TReturn = any> = IteratorYieldResult<T> | IteratorReturnResult<TReturn>;

interface Iterator<T, TReturn = any, TNext = undefined> {
    // NOTE: 'next' is defined using a tuple to ensure we report the correct assignability errors in all places.
    next(...args: [] | [TNext]): IteratorResult<T, TReturn>;
    return?(value?: TReturn): IteratorResult<T, TReturn>;
    throw?(e?: any): IteratorResult<T, TReturn>;
}

interface Iterable<T> {
    [Symbol.iterator](): Iterator<T>;
}

interface IterableIterator<T> extends Iterator<T> {
    [Symbol.iterator](): IterableIterator<T>;
}

interface ArrayLike<T> {
    readonly length: number;
    readonly [n: number]: T;
}

interface Array<T> {
    /** Iterator */
    [Symbol.iterator](): IterableIterator<T>;

}

interface Boolean {}
interface CallableFunction {}
interface NewableFunction {}
interface Object {}
interface Boolean {}
interface Number {}
interface Function {}
interface RegExp {}
interface IArguments {}
interface TypedPropertyDescriptor<_T> {}

type Exclude<T, U> = T extends U ? never : T;
type Extract<T, U> = T extends U ? T : never;
type Pick<T, K extends keyof T> = {
    [P in K]: T[P];
};
type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
type InstanceType<T> = T extends new (...args: any[]) => infer R ? R : any;


// - Typescript CORE
interface Number {}
interface String {
    i: any;
    size: () => TNat;
    plus: (v: any) => any;
    negate: () => any;
    toInt: () => TInt;
    multiply: (v: any) => any;
    concat: (v: any) => any;
    slice: (offset: any, length: any) => any;
}
interface TAddress extends Pick<string, never> {}
interface TSignature extends Pick<string, never> {}
interface TString extends Pick<string, never> {
    size: () => TNat;
    concat: (v: TBytes) => TBytes;
    slice: (offset: TNat, length: TNat) => TOption<TBytes>;
}
interface TBytes extends Pick<String, never> {
    size: () => TNat;
    concat: (v: TBytes) => TBytes;
    slice: (offset: TNat, length: TNat) => TOption<TBytes>;
}
interface TChain_id extends Omit<String, 'size'> {}
interface TKey_hash extends Omit<String, 'size'> {}
interface TKey extends Omit<String, 'size'> {}
interface TChest extends Pick<String, never> {}
interface TChest_key extends Pick<String, never> {}
interface TBls12_381_g1 extends smartTS.TBls12Generic<String, TBls12_381_g1, 'TBls12_381_g1'> {}
interface TBls12_381_g2 extends smartTS.TBls12Generic<String, TBls12_381_g2, 'TBls12_381_g2'> {}

type TBls12_381_fr = (string | number) & {
    toInt?: () => TInt;
    plus?: (v: TBls12_381_fr) => TBls12_381_fr;
    negate?: () => TBls12_381_fr;
    multiply?: <T>(v: T) =>
        T extends TNat | TInt | TBls12_381_fr
        ? TBls12_381_fr
        : T extends TBls12_381_g1
        ? TBls12_381_g1
        : T extends TBls12_381_g2
        ? TBls12_381_g2
        : never;
};

interface Array<T> {
    size: () => TNat;
    hasKey: (key: any) => TBool;
    get: (key: any, ...obj: any) => any;
    set: (key: any, value: any) => TUnit;
    entries: () => any;
    keys: () => any;
    values: () => any;
    remove: (key: any) => TUnit;
    fst: () => any;
    snd: () => any;
    push: (arg: any) => void;
    elements: () => TList<any>;
    contains: (v: any) => TBool;
    add: (v: any) => TUnit;
    reverse: () => any;
};

interface TBig_map<K, V> extends Pick<Array<[K, V]>, never> {
    hasKey: (key: K) => TBool;
    get: (key: K, defaultValue?: V) => V;
    set: (key: K, value: V) => void;
    remove: (key: K) => void;
};

interface TMap<K, V> extends Pick<Array<[K, V]>, never> {
    size: () => TNat;
    hasKey: (key: K) => TBool;
    get: (key: K, defaultValue?: V) => V;
    set: (key: K, value: V) => void;
    remove: (key: K) => void;
    entries: () => TList<{ key: K, value: V }>;
    keys: () => TList<K>;
    values: () => TList<V>;
};

interface TList<T> extends Pick<[T], never> {
    [Symbol.iterator](): Iterator<T>;
    size: () => TNat;
    push: (arg: T) => void;
    reverse: () => TList<T>;
};

interface TSet<T> extends Pick<[T], never> {
    size: () => TNat;
    elements: () => TList<T>;
    contains: (v: T) => TBool;
    add: (v: T) => TUnit;
    remove: (v: T) => TUnit;
};

/**
 * @description A Tuple type
 */
type TTuple<T extends unknown[]> = smartTS.LengthOfTuple<T> extends 2
    // A tuple with 2 elements
    ? Pick<[...T], never> & {
        /**
         * @description Get first element
         */
        fst: () => T extends [infer R, ...any[]] ? R : never;
        /**
         * @description Get second element
         */
        snd: () => T extends [...any[], infer R] ? R : never;
    }
    // A tuple with more than 2 elements
    : Pick<[...T], never> & {

    };

type TBool = boolean;

type TTimestamp = (string | number) & {
    addSeconds?: (s: TNat) => TTimestamp;
    addMinutes?: (s: TNat) => TTimestamp;
    addHours?: (s: TNat) => TTimestamp;
    addDays?: (s: TNat) => TTimestamp;
    toSeconds?: () => TNat;
    minus?: <T>(v: T) => T extends TInt ? TTimestamp : T extends TTimestamp ? TInt : never;
};
type TMutez = number & {
    /**
     * @deprecated
     */
    _mutez?: never;
    toNat?: () => TNat;
    plus?: (v: TMutez) => TMutez;
    minus?: (v: TMutez) => TMutez;
    multiply?: (v: TNat) => TMutez;
};
type TNat = number & {
    /**
     * @deprecated
     */
    _nat?: never;
    toMutez?: () => TNat;
    toInt?: () => TInt;
    plus?: <T>(v: T) => T extends TNat ? TNat : T extends TInt ? TInt : never;
    minus?: <T>(v: T) => T extends TNat | TInt ? TInt : never;
    multiply?: <T>(v: T) => T extends TMutez | TNat | TInt | TBls12_381_fr ? T : never;
};
type TInt = number & {
    abs?: () => TNat;
    toNat?: () => TNat;
    plus?: <T>(v: T) => T extends TNat | TInt ? TInt : never;
    minus?: <T>(v: T) => T extends TNat | TInt ? TInt : never;
    multiply?: <T>(v: T) => T extends TNat | TInt ? TInt : T extends TBls12_381_fr ? TBls12_381_fr : never;
};
type TNever = never;
type TLambda<_I, O> = (...input: any[]) => O;

interface TOption<T> {
    isSome: () => TBool;
    openSome: (message?: unknown) => T;
};

interface TContract<T>  {
    /**
     * @deprecated
     */
     _: T
};
interface TSaplingState<_T> {};
interface TSaplingTransaction<_T> {};
interface TOperation {};
type TUnit = void;

interface Object {
    isVariant: (kind: any) => any;
    openVariant: (kind: any) => any;
};
type TRecord<
    T extends { [key: string]: any },
    _L extends ((T extends T ? keyof T : never) | (T extends T ? keyof T : never)[])[] | Layout = Layout.right_comb,
> = T;
type TVariant<
    T extends {
        kind: string;
        value: any;
    },
    _L extends (T['kind'] | T['kind'][])[] | Layout = Layout.right_comb,
> = T & {
    isVariant: (kind: T['kind']) => TBool;
    openVariant: <K extends T['kind']>(kind: K, errorMsg: any) => Extract<T, { kind: K }>['value'];
};

// Enums
declare enum Layout {
    right_comb = 'right_comb',
};

// Modules
declare const Sp: smartTS.Sp;
declare const Dev: smartTS.Dev;
declare const Scenario: smartTS.Scenario;

// Decorators
declare const EntryPoint: smartTS.EntryPoint;
declare const OffChainView: smartTS.OffChainView;
declare const OnChainView: smartTS.OnChainView;
declare const Contract: smartTS.Contract;
declare const MetadataBuilder: smartTS.MetadataBuilder;
declare const PrivateLambda: smartTS.PrivateLambda;
declare const Inline: smartTS.Inline;

declare namespace smartTS {
    interface ImplicitAccount {
        address: TAddress;
        publicKeyHash: TKey_hash;
        publicKey: TKey;
        secretKey: string;
    }

    interface EntryPoint {
        <T = void>(target: any, propertyKey: any): TypedPropertyDescriptor<(...args: any[]) => void>; // enforce void return
        <T = void>(args?: { name?: string; lazy?: boolean; lazy_no_code?: boolean; mock?: boolean }): (
            target: any,
            property: any,
        ) => TypedPropertyDescriptor<(...args: any[]) => void>; // enforce void return
    }

    interface OffChainView {
        (target: any, propertyKey: any): any;
        (args: { name?: string; pure?: boolean; description?: string }): (target: any, property: any) => any;
    }

    interface OnChainView {
        (target: any, propertyKey: any): any;
        (args: { name?: string; }): (target: any, property: any) => any;
    }

    interface PrivateLambda {
        (target?: any, propertyKey?: any): any;
        (args?: { withOperations?: boolean, withStorage?: 'read-only' | 'read-write' }): (target: any, property: any) => any;
    }

    interface Contract {
        (constructor: any): any;
        (_: { flags: string[][] }): (_: any) => any;
    }

    type Metadata =
        | {
              name?: string;
              description?: string;
              decimals?: number;
              symbol?: string;
              version?: string;
              interfaces?: string[];
              authors?: string[];
              homepage?: string;
              source?: {
                  [key: string]: string | string[];
              };
              views?: ((...args: any) => any)[];
          }
        | {
              [key: string]: string | boolean | number | (string | boolean | number)[] | Metadata;
          };

    interface MetadataBuilder {
        (target: { metadata: Metadata }, propertyKey: 'metadata'): void;
    }

    interface Inline {
        (target: any, propertyKey: any): any;
    }

    interface Sp {
        // Type wrappers
        bigMap: <T>(elements: unknown[]) => T;
        // Option
        some: <T>(arg: T) => TOption<T>;
        none: TOption<any>;
        //
        source: TAddress;
        sender: TAddress;
        amount: TMutez;
        balance: TMutez;
        chainId: TChain_id;
        now: TTimestamp;
        level: TNat;
        totalVotingPower: TNat;
        //
        unit: void;
        variant: <T extends { kind: unknown, value: unknown }>(kind: PickPropType<T, 'kind'>, value: PickPropType<T, 'value'>) => T;
        votingPower: (keyHash: TKey_hash) => TNat;
        never: (arg: any) => TNever;
        verify: (condition: boolean, message?: any) => void;
        checkSignature: (pubKey: TKey_hash, signature: TSignature, content: TBytes) => TBool;
        createContract: <C>(contract: C, storage?: PickContractStorage<InstanceType<C>>, amount?: TMutez, baker?: TKey_hash) => TAddress;
        createContractOperation: <C>(contract: C, storage?: PickContractStorage<InstanceType<C>>, amount?: TMutez, baker?: TKey_hash) => TRecord<{ address: TAddress; operation: TOperation}>;
        verifyEqual: (left: any, right: any, message?: any) => void;
        transfer: <T>(params: T, mutez: TMutez, contract: TContract<T>) => void;
        setDelegate: (keyHash: TOption<TKey_hash>) => void;
        pack: (arg: any) => TBytes;
        unpack: <T>(bytes: TBytes) => TOption<T>;
        concat: <T>(list: TList<T>) => T;
        constant: <T = any>(hash: string) => T;
        view: <T = TUnit>(name: string, address: TAddress, param?: any) => TOption<T>;
        ediv: <T1, T2>(v1: T1, v2: T2) => TOption<TTuple<[
            T1 extends TNat
                ? T2 extends TNat
                    ? TNat // v1: TNat, v2: TNat
                    : T2 extends TInt
                        ? TInt  // v1: TNat, v2: TInt
                        : never // v1: TNat, v2: unknown
                : T1 extends TMutez
                    ? T2 extends TMutez
                        ? TNat  // v1: TMutez, v2: TMutez
                        : never // v1: TMutez, v2: unknown
                    : never,    // v1: unknown, v2: unknown
            T1 extends TNat | TInt
                ? TNat  // v1: TNat | TInt, v2: unknown
                : T1 extends TMutez
                    ? TMutez // v1: TMutez | TInt, v2: unknown
                    : never  // v1: unknown, v2: unknown
        ]>>;
        failWith: <T>(msg: T) => void;
        openChest: (chest_key: TChest_key, chest: TChest, time: TNat) => TVariant<
            | {
                    kind: 'Left';
                    value: TBytes;
                }
            | {
                    kind: 'Right';
                    value: TBool;
                },
            ['Left', 'Right']
        >;
        // Contract Addresses
        selfAddress: TAddress;
        self: TContract<unknown>;
        /**
         * @description Compute the Base58Check of a public key.
         */
        hashKey: (key: TKey) => TKey_hash;
        implicitAccount: (keyHash: TKey_hash) => TContract<TUnit>;
        contract: <T>(address: TAddress, entryPoint?: string) => TOption<TContract<T>>;
        selfEntryPoint: <T>(entryPoint?: string) => TContract<T>;
        selfEntryPointAddress: (entryPoint?: string) => TAddress;
        toAddress: (contract: TContract<unknown>) => TAddress;
        // BLS12-381
        pairingCheck: (pairs: TList<TTuple<[TBls12_381_g1, TBls12_381_g2]>>) => TBool;
        // Hash Functions
        blake2b: (bytes: TBytes) => TBytes;
        sha256: (bytes: TBytes) => TBytes;
        sha512: (bytes: TBytes) => TBytes;
        sha3: (bytes: TBytes) => TBytes;
        keccak: (bytes: TBytes) => TBytes;
    }

    interface Dev {
        test: (args: { name: string, shortName?: string; profile?: boolean; enabled?: boolean, flags?: string[][] }, test: (scenario: any) => void) => void;
        compileContract: (name: string, contract: unknown) => void;
    }

    interface Scenario {
        // Loggers
        h1: (msg: string) => void;
        h2: (msg: string) => void;
        h3: (msg: string) => void;
        h4: (msg: string) => void;
        p: (msg: string) => void;
        tableOfContents: () => void;
        show: (expr: any, opt?: { compile?: boolean, html?: boolean, stripStrings?: boolean }) => void;
        //
        transfer: (
            contract: void,
            options?: {
                amount?: TMutez;
                chainId?: TChain_id;
                level?: TNat;
                now?: TTimestamp;
                votingPowers?: TMap<TKey_hash, TMutez>;
                sender?: TAddress | ImplicitAccount;
                source?: TAddress | ImplicitAccount;
                valid?: boolean;
                show?: boolean;
                exception?: any;
            }) => void;
        originate: <C>(contract: C, options?: { show?: boolean, initialBalance?: TMutez }) => PickContractProps<C> & {
            balance: TMutez;
            baker: TKey_hash;
            address: TAddress;
            typed: PickContractProps<C>
        };
        testAccount: (seed: TString) => ImplicitAccount;
        makeSignature: (secretKey: string, content: TBytes) => TSignature;
        // Verify
        verify: (condition: TBool) => void;
        verifyEqual: (left: any, right: any) => void;
        compute: (expr: any) => any;
        isFailing: (call: any) => TBool;
        catchException: <T>(call:any) => TOption<T>;
        prepareConstantValue: (expr: any, hash?: string) => any;
    }

    // Not exposed
    type PickPropType<TObj, TProp extends keyof TObj> = TObj[TProp];
    type PickContractProps<T> = Pick<T, { [Key in keyof T]: T[Key] extends Function ? Key : Key extends 'storage' ? Key : never }[keyof T]>;
    type PickContractStorage<T> = 'storage' extends keyof T ? T['storage'] : never;
    type LengthOfTuple<T extends any[]> = T extends { length: infer L } ? L : never;

    interface TBls12Generic<B, T, ID> extends Pick<B, never> {
        /**
         * @deprecated Don't use this field.
         */
        i: ID,
        plus: (v: T) => T;
        negate: () => T;
        multiply: (v: TBls12_381_fr) => T;
    }
}
