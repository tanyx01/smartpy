type Token = TRecord<{ balance: TNat }>;

type Storage = {
    tokens: TBig_map<TNat, Token>;
}

// A contract that serves as storage and provides information to other contracts through an on-chain view
class Provider {
    constructor(public storage: Storage) {}

    @OnChainView
    getTokenById = (tokenID: TNat): Token => {
        Sp.verify(this.storage.tokens.hasKey(tokenID), "Token doesn't exist")
        return this.storage.tokens.get(tokenID);
    }
}

// Contract that will call the view defined in the consumer above
class Consumer {
    @OnChainView
    checkToken = (params: { tokenID: TNat, providerAddress: TAddress}): TUnit => {
        const token = Sp.view<Token>(
            "getTokenById",
            params.providerAddress,
            params.tokenID
        ).openSome("Invalid view");
        Sp.verify(token.balance >= 10, "Token balance is lower than 10");
    }
}

Dev.test({name: "test"}, () => {
    const provider = Scenario.originate(new Provider({
        tokens: [
            [0, { balance: 11 }]
        ]
    }));
    const consumer = Scenario.originate(new Consumer());

    Scenario.verify(!Scenario.isFailing(
        consumer.checkToken({ tokenID: 0, providerAddress: provider.address })
    ));
});

Dev.compileContract('compilation', new Consumer());
