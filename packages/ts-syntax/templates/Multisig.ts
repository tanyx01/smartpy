/* ======== */
// Contract parameters

const quorum: TNat = 3;

const alice: TAddress = 'tz1M43YV5vVt7HALGJeWgQ1GRQfiGExA1Jbf';
const alice_key: TKey = 'edpkvF1HmWAdtpqPAc27pNRpNaYoF3vp9cgWXbhmC45RHN1JWh1f4z';
const bob: TAddress = 'tz1L3Pgin3YSeqZJb6UF9Fis9LBjp3L1YusM';
const bob_key: TKey = 'edpkub2iNvM3KnaccBaHujEz93Naj3ySBvAoEFLELjaLXk21otyTyT';

const signers: TSigners = [
    [alice as TAddress, { publicKey: alice_key, lastProposalId: Sp.none as TOption<TNat> }],
    [bob as TAddress, { publicKey: bob_key, lastProposalId: Sp.none as TOption<TNat> }],
];

/* ======== */

/* Types */

type TSigners = TMap<TAddress, TRecord<{ publicKey: TKey; lastProposalId: TOption<TNat> }>>;

type TChangeSigners = TVariant<
    { kind: 'removed'; value: TSet<TAddress> } | { kind: 'added'; value: TList<{ address: TAddress; publicKey: TKey }> }
>;

// This is used to change the contract's storage
type TInternalAdminAction = TVariant<
    { kind: 'changeSigners'; value: TChangeSigners } | { kind: 'changeQuorum'; value: TNat }
>;

// This is used to act on the target contract
type TExternalAdminAction = {
    target: TAddress;
    actions: TBytes;
};

// Proposal action type specification
type TProposalAction = TVariant<
    { kind: 'internal'; value: TList<TInternalAdminAction> } | { kind: 'external'; value: TList<TExternalAdminAction> }
>;

// How a proposal is stored
type TProposal = {
    startedAt: TTimestamp;
    initiator: TAddress;
    endorsements: TSet<TAddress>;
    actions: TProposalAction;
};

interface TStorage {
    quorum: TNat;
    lastProposalId: TNat;
    signers: TSigners;
    proposals: TBig_map<TNat, TProposal>;
    activeProposals: TSet<TNat>;
}

/* =========== */

// Contract

@Contract
export class MultisigAdmin {
    storage: TStorage = {
        quorum: quorum,
        lastProposalId: 0,
        signers: signers,
        proposals: [],
        activeProposals: [],
    };

    @Inline
    failIfNotSigner = (sender: TAddress): void => Sp.verify(this.storage.signers.hasKey(sender), 'Not_Signer');

    @EntryPoint
    proposal(actions: TProposalAction): void {
        /*
            Each user can have at most one proposal active at a time.
            Submitting a new proposal overrides the previous one.
        */
        // Proposals can only be submitted by registered signers
        this.failIfNotSigner(Sp.sender);

        // If the proposal initiator has an active proposal,
        // then replace that proposal with the new one
        const signerLastProposalId = this.storage.signers.get(Sp.sender).lastProposalId;
        if (signerLastProposalId != Sp.none) {
            this.storage.activeProposals.remove(signerLastProposalId.openSome());
        }

        // Increment proposal counter
        this.storage.lastProposalId += 1;
        const proposalId = this.storage.lastProposalId;
        // Store new proposal
        this.storage.activeProposals.add(proposalId);
        this.storage.proposals.set(proposalId, {
            startedAt: Sp.now,
            initiator: Sp.sender,
            endorsements: [Sp.sender] as TSet<TAddress>,
            actions: actions,
        });

        // Update signer's last proposal
        this.storage.signers.get(Sp.sender).lastProposalId = Sp.some(proposalId);

        // Approve the proposal if quorum only requires 1 vote
        if (this.storage.quorum < 2) {
            this.onApproved(actions);
        }
    }

    /* Apply administrative actions to the multisig contract */
    @Inline
    selfAdmin = (action: TInternalAdminAction): void => {
        switch (action.kind) {
            case 'changeQuorum':
                {
                    this.storage.quorum = action.value;
                }
                break;
            case 'changeSigners': {
                // TODO
                const changeSignersAction = action.value;
                switch (changeSignersAction.kind) {
                    case 'removed':
                        {
                            const removeSet = changeSignersAction.value;
                            for (const address of removeSet.elements()) {
                                // Remove signer
                                this.storage.signers.remove(address);
                                // We don't remove signer[address].lastProposalId
                                // because we remove all activeProposals after it.
                            }
                        }
                        break;
                    case 'added': {
                        const addList = changeSignersAction.value;
                        for (const signer of addList) {
                            this.storage.signers.set(signer.address, {
                                publicKey: signer.publicKey,
                                lastProposalId: Sp.none,
                            });
                        }
                    }
                }
            }
        }
    };

    /* ************** */
    /* Global Lambdas */
    /* ************** */

    @PrivateLambda({ withOperations: true, withStorage: 'read-write' })
    onApproved = (actions: TProposalAction): void => {
        switch (actions.kind) {
            case 'internal':
                {
                    // Will be executed
                    for (const action of actions.value) {
                        this.selfAdmin(action);
                        // Ensure that the contract never requires more quorum than the total of signers.
                        Sp.verify(this.storage.quorum <= this.storage.signers.size(), 'MoreQuorumThanSigners');
                    }
                    // Removes all active proposals after an administrative change.
                    this.storage.activeProposals = [] as TSet<TNat>;
                }
                break;
            case 'external': {
                for (const action of actions.value) {
                    const target: TContract<TBytes> = Sp.contract<TBytes>(action.target).openSome('Invalid Interface');
                    Sp.transfer(action.actions, 0, target);
                }
            }
        }
    };
}

/* =========== */

// Tests

Dev.test({ name: 'MultisigAdmin contract' }, () => {
    const c1 = Scenario.originate(new MultisigAdmin());
    Scenario.transfer(c1.proposal(Sp.variant('internal', [])), { sender: alice });
});

Dev.compileContract('compile_contract', new MultisigAdmin());
