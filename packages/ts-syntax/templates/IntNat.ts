interface StorageSpec {
    int: TInt;
    nat: TNat;
}

@Contract
export class IntNat {
    storage: StorageSpec = {
        int: 0,
        nat: 0,
    };

    @EntryPoint
    ep(): void {
        this.storage.int = this.storage.nat.toInt();
        this.storage.nat = this.storage.int.toNat();
        this.storage.nat = this.storage.int.abs();
    }
}

Dev.test({ name: 'intNat' }, () => {
    const c1 = Scenario.originate(new IntNat());
    Scenario.transfer(c1.ep());
});

Dev.compileContract('intNat', new IntNat());
