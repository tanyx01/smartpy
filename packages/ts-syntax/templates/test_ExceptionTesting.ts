export class ExceptionTesting {
    constructor(public storage: TInt = /* Default value */ 1) {}

    @EntryPoint
    ep(param: TInt) {
        Sp.verify(param < 5, 'This is false: param > 5');
    }

    @OnChainView
    state = (param: TInt): TInt => {
        Sp.verify(param < 5, 'This is false: param > 5');
        return this.storage * param;
    };
}

Dev.test({ name: 'ExceptionTesting_test' }, () => {
    const c1 = Scenario.originate(new ExceptionTesting());

    Scenario.transfer(c1.ep(6), { valid: false, exception: 'This is false: param > 5' });

    /** Test views */

    // Display the offchain call result
    Scenario.show(c1.state(1));

    // Assert the view result
    Scenario.verify(c1.state(2) === 2);
    // Assert call failures
    Scenario.verify(Scenario.isFailing(c1.state(6))); // Expected to fail
    Scenario.verify(!Scenario.isFailing(c1.state(1))); // Not expected to fail

    // Assert exception result
    // catchException returns an option:
    //      Sp.none if the call succeeds
    //      Sp.some(<exception>) if the call fails
    const e = Scenario.catchException<TString>(c1.state(7));
    Scenario.verify(e === Sp.some('This is false: param > 5'));
});

Dev.compileContract('ExceptionTesting_compilation', new ExceptionTesting());
