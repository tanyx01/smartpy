interface TStorage {
    storedValue: TNat;
}

type ReplaceParams = {
    value: TNat;
};

type DivideParams = {
    divisor: TNat;
};

@Contract
export class StoreValue {
    storage: TStorage = {
        storedValue: 1,
    };

    @EntryPoint
    replace(params: ReplaceParams): void {
        this.storage.storedValue = params.value;
    }

    @EntryPoint
    double(): void {
        this.storage.storedValue *= 2;
    }

    @EntryPoint
    divide(params: DivideParams): void {
        this.storage.storedValue /= params.divisor;
    }
}

Dev.test({ name: 'StoreValue' }, () => {
    const c1 = Scenario.originate(new StoreValue());
    Scenario.transfer(c1.replace({ value: 1 }));
    Scenario.transfer(c1.double());
    Scenario.transfer(c1.divide({ divisor: 1 }));
});

Dev.compileContract('compile_contract', new StoreValue());
