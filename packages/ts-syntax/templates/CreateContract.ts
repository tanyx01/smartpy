interface TCreatedStorage {
    value: TString;
}

@Contract
export class Created {
    storage: TCreatedStorage = {
        value: '',
    };

    @EntryPoint
    updateValue(value: TString): void {
        this.storage.value = value;
    }
}

interface TCreatorStorage {
    value: TOption<TAddress>;
}

@Contract
export class Creator {
    storage: TCreatorStorage = {
        value: Sp.none,
    };

    @EntryPoint
    create(value: TString): void {
        // Create contract without storing its address
        Sp.createContract(Created, { value: value });
        // Creat contract and store its address
        this.storage.value = Sp.some(
            Sp.createContract(Created, { value: value }, 10, 'tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
        );
    }
}

Dev.test({ name: 'Create Record' }, () => {
    const c1 = Scenario.originate(new Creator());
    Scenario.transfer(c1.create('Hello World'));
});

Dev.compileContract('create_record_compilation', new Creator());
