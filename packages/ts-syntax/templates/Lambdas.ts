interface TStorage {
    storedValue: TNat;
    transformer: TLambda<TNat, TNat>;
    increment: TLambda<TNat, TNat>;
    decrement: TLambda<TNat, TInt>;
}

const transformer: TLambda<TNat, TNat> = (value: TNat): TNat => {
    if (value > 5) {
        return 1;
    } else {
        return value;
    }
};

@Contract
export class Lambdas {
    storage: TStorage = {
        storedValue: 1,
        transformer: transformer,
        increment: (value: TNat) => {
            return value + 1;
        },
        decrement: (value: TNat): TInt => value - 1,
    };

    @EntryPoint
    store(value: TNat, transformer2: TLambda<TNat, TNat>): void {
        const transformer3: TLambda<TNat, TNat> = (value: TNat): TNat => {
            if (value > 5) {
                return 2;
            } else {
                return value;
            }
        };
        this.storage.storedValue = this.storage.increment(value);
        this.storage.storedValue = this.storage.transformer(this.storage.storedValue);
        this.storage.storedValue = transformer(this.storage.storedValue);
        this.storage.storedValue = transformer2(this.storage.storedValue);
        this.storage.storedValue = transformer3(this.storage.storedValue);
        this.storage.storedValue = this.transformer4(this.storage.storedValue);
        this.storage.storedValue = this.transformer5(this.storage.storedValue);
        this.storage.storedValue = this.storage.decrement(this.storage.storedValue) as TNat;

        Sp.verify(this.callback() === Sp.unit);
    }

    @EntryPoint
    default(): void {
        this.storage.storedValue = 10;
    }

    @PrivateLambda({})
    transformer4 = (value: TNat): TNat => {
        return value + 5;
    };

    @PrivateLambda({ withStorage: 'read-only' })
    transformer5 = (value: TNat): TNat => {
        return value + this.storage.storedValue;
    };

    @PrivateLambda({ withOperations: true })
    callback = (): TUnit => {
        const contract = Sp.contract<TUnit>(Sp.selfAddress, 'default').openSome();
        Sp.transfer(Sp.unit, 0, contract);
    };
}

Dev.test({ name: 'Lambdas' }, () => {
    const c1 = Scenario.originate(new Lambdas());
    Scenario.transfer(c1.store(1, (v: TNat) => v + 1));
});

Dev.compileContract('compile_contract', new Lambdas());
