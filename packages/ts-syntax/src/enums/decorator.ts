export enum ST_DecoratorKind {
    Contract = 'Contract',
    EntryPoint = 'EntryPoint',
    View = 'View',
    MetadataBuilder = 'MetadataBuilder',
    PrivateLambda = 'PrivateLambda',
    Inline = 'Inline',
}

export enum ST_ContractDecoratorProp {
    flags = 'flags',
}

export enum ST_EntryPointDecoratorProp {
    name = 'name',
    lazy = 'lazy',
    lazy_no_code = 'lazy_no_code',
    check_no_incoming_transfer = 'check_no_incoming_transfer',
    mock = 'mock',
}

export enum ST_ViewDecoratorProp {
    name = 'name',
    pure = 'pure',
    description = 'description',
}

export enum ST_PrivateLambdaDecoratorProp {
    withStorage = 'withStorage',
    withOperations = 'withOperations',
}
