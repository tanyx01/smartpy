export enum ST_ScenarioActionKind {
    HTML = 'html',
    NEW_CONTRACT = 'newContract',
    MESSAGE = 'message',
    SHOW = 'show',
    FLAG = 'flag',
    VERIFY = 'verify',
    COMPUTE = 'compute',
    CONSTANT = 'constant',
}

export enum ST_ScenarioKind {
    Compilation = 'compilation',
    Test = 'test',
}
