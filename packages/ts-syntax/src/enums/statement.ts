export enum ST_StatementKind {
    Expression = 'Expression',
    VariableStatement = 'VariableStatement',
    IfStatement = 'IfStatement',
    ForStatement = 'ForStatement',
    ForOfStatement = 'ForOfStatement',
    WhileStatement = 'WhileStatement',
    SwitchStatement = 'SwitchStatement',
    Bind = 'Bind',
    Result = 'Result',
}

export enum ST_VariableValueKind {
    Literal = 'Literal',
    PropertyPath = 'PropertyPath',
}

export enum ST_StatementKeywords {
    forGroup = 'for_group',
    whileBlock = 'while_block',
    ifBlock = 'if_block',
    elseBlock = 'else_block',
}
