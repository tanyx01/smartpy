import type { ST_ScopeKind } from '../enums/scope';
import type { FileLineInfo } from './common';
import type { Report, ST_Class, ST_ClassScope, ST_Contract } from './output';
import type { ST_Scenario } from './scenario';
import type { ST_TypeDefs } from './type';

export interface ST_Module {
    contracts: Record<string, ST_Contract>;
    scope: ST_Scope;
    scenarios: ST_Scenario[];
    warnings: Report[];
}

export interface ST_ImportedModule extends ST_Module {
    name: string;
    scope: ST_Scope;
    line: FileLineInfo;
}

export interface ST_Scope extends ST_ClassScope {
    kind: ST_ScopeKind;
    typeDefs: ST_TypeDefs;
    classes: Record<string, ST_Class>;
}

export type ST_Modules = Record<string, ST_Module>;
