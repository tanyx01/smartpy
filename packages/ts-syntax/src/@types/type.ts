import type { FrontendType, Layout } from '../enums/type';
import type { FileLineInfo } from './common';

export type ST_Layout = (string | ST_Layout)[];
export type ST_TypeDef = (
    | {
          type: FrontendType.TRecord;
          properties: ST_TypeDefs;
          layout?: ST_Layout | Layout;
      }
    | {
          type: FrontendType.TVariant;
          properties: ST_TypeDefs;
          layout?: ST_Layout | Layout;
      }
    | {
          type: FrontendType.TOption;
          none: boolean;
          innerType: ST_TypeDef;
      }
    | {
          type: FrontendType.TOption;
          innerType: ST_TypeDef;
      }
    | {
          type: FrontendType.TList | FrontendType.TSet | FrontendType.TTuple;
          innerTypes: ST_TypeDef[];
      }
    | {
          type: FrontendType.TMap | FrontendType.TBig_map;
          keyType: ST_TypeDef;
          valueType: ST_TypeDef;
      }
    | {
          type: FrontendType.TFunction | FrontendType.TLambda;
          inputTypes: Record<string, ST_TypeDef & { index: number }>;
          outputType: ST_TypeDef;
      }
    | {
          type: FrontendType.TContract;
          inputType: ST_TypeDef;
      }
    | {
          type:
              | FrontendType.TAddress
              | FrontendType.TBls12_381_fr
              | FrontendType.TBls12_381_g1
              | FrontendType.TBls12_381_g2
              | FrontendType.TBool
              | FrontendType.TBytes
              | FrontendType.TChain_id
              | FrontendType.TInt
              | FrontendType.TKey
              | FrontendType.TKey_hash
              | FrontendType.TMutez
              | FrontendType.TNat
              | FrontendType.TNever
              | FrontendType.TSignature
              | FrontendType.TString
              | FrontendType.TTimestamp
              | FrontendType.TOperation
              | FrontendType.TUnit
              | FrontendType.TChest
              | FrontendType.TChest_key
              | FrontendType.TUnknown
              | FrontendType.TIntOrNat;
      }
) & {
    line?: FileLineInfo;
};

export type ST_TypeDefs = Record<string, ST_TypeDef>;
