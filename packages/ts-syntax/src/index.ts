import { Transpiler } from './lib/Transpiler';
import typings from './ide.generated';
import type { ST_Scenario } from './@types/scenario';
import type { FileInfo } from './@types/common';
import { fetchImportedFiles } from './lib/CompilerHost';

// Expose types at the root
export type { ST_Error } from './lib/utils/Error';

export { setFetchFromDisk, setFetchFromURL } from './lib/utils/file/FileUtils';

const SmartTS = {
    typings,
    Transpiler,
    transpile: async (sourceCode: FileInfo): Promise<ST_Scenario[]> => {
        const sourceFiles = await fetchImportedFiles(sourceCode);
        const transpiler = new Transpiler(sourceCode, sourceFiles);
        // Perform code analysis
        transpiler.generateDiagnostics();
        return transpiler.transpile();
    },
};

export default SmartTS;
