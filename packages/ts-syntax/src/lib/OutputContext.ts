import {
    ArrowFunction,
    ClassDeclaration,
    FunctionDeclaration,
    HeritageClause,
    ImportDeclaration,
    isIdentifier,
    isPropertyAccessExpression,
    MethodDeclaration,
    Node,
    NodeArray,
    SourceFile,
    SyntaxKind,
} from 'typescript';
import type { Nullable } from '../@types/common';
import type { ST_Property_Decorators, ST_Class_Decorator } from '../@types/decorator';
import type { ST_Expression, ST_TypedExpression } from '../@types/expression';
import type { ST_ImportedModule, ST_Module } from '../@types/module';
import type { ST_Class, ST_Flag } from '../@types/output';
import type { ST_FunctionProperty, ST_Properties } from '../@types/property';
import type { ST_CompilationTarget, ST_Scenario, ST_ScenarioAction } from '../@types/scenario';
import type { ST_Statement, ST_VariableStatement } from '../@types/statement';
import type { ST_TypeDefs, ST_TypeDef } from '../@types/type';
import type { FrontendType } from '../enums/type';
import { ST_DecoratorKind } from '../enums/decorator';
import { ST_ExpressionKind } from '../enums/expression';
import { ST_Modifier } from '../enums/Modifiers';
import { ST_ScenarioActionKind, ST_ScenarioKind } from '../enums/scenario';
import { ST_ScopeKind } from '../enums/scope';
import MetadataTranslator from './translator/metadata';
import { staticId } from './translator/misc';
import type { Translators } from './Transpiler';
import ExpressionBuilder from './utils/builders/expression';
import TypeBuilder from './utils/builders/type';
import ErrorUtils from './utils/Error';
import { nonFalsy } from './utils/filters/falsy';
import guards from './utils/guard';
import IdentifierUtils from './utils/Identifier';
import LineUtils from './utils/Line';
import ModifiersUtils from './utils/Modifiers';

class Scope {
    iterators: Record<string, Extract<ST_Expression, { kind: ST_ExpressionKind.ForIterator }>> = {};
    properties: ST_Properties = {};
    statements: ST_Statement[] = [];
    functions: Record<string, ST_FunctionProperty> = {};
    typeDefs: ST_TypeDefs = {};
    classes: Record<string, ST_Class> = {};
    varCounter = 0;
    switchCase: Nullable<{
        variant: string;
        accessExpr: Extract<ST_Expression, { kind: ST_ExpressionKind.VariantAccess }>;
    }>;

    constructor(public kind: ST_ScopeKind, public parent?: Scope) {}

    public addIterator = (name: string, expr: Extract<ST_Expression, { kind: ST_ExpressionKind.ForIterator }>) =>
        (this.iterators[name] = expr);
    public removeIterator = (name: string) => delete this.iterators[name];

    /**
     * @description Records a statement.
     * @param {ST_Statement} statement
     */
    public emitStatement = (statement: ST_Statement): void => {
        this.statements = [...this.statements, statement];
    };

    /**
     * @description Generates a variable identifier.
     * @return {string} Variable identifier.
     */
    get generateVarID(): string {
        return `var_${this.varCounter++}`;
    }
}

class BaseContext {
    importing: Nullable<string>;
    declaringClass: Nullable<string>;
    declaringMethod: Nullable<string>;
    modules: Record<string, ST_ImportedModule> = {};
    exported: {
        typeDefs: string[];
        properties: string[];
        functions: string[];
        classes: string[];
    } = {
        typeDefs: [],
        properties: [],
        functions: [],
        classes: [],
    };
    scopes: Scope[] = [];
    result: ST_Module = {
        contracts: {},
        scope: {
            kind: ST_ScopeKind.Global,
            typeDefs: {},
            properties: {},
            functions: {},
            classes: {},
        },
        scenarios: [],
        warnings: [],
    };

    constructor(public sourceFile: SourceFile, public translators: Translators) {}

    get scope(): Scope {
        return this.scopes?.[this.scopes.length - 1];
    }

    /**
     * @description Filter all non exported (classes, properties, type definition) and return the module scope
     */
    get exportedModule(): ST_Module {
        // Remove any class that was not exported
        Object.keys(this.result.scope.classes).forEach((key) => {
            if (!this.exported.classes.includes(key)) {
                delete this.result.scope.classes[key];
                // Also delete the contract class reference if any
                delete this.result.contracts[key];
            }
        });
        // Remove any property that was not exported
        Object.keys(this.result.scope.properties).forEach((key) => {
            if (!this.exported.properties.includes(key)) {
                delete this.result.scope.properties[key];
            }
        });
        // Remove any type definition that was not exported
        Object.keys(this.result.scope.typeDefs).forEach((key) => {
            if (!this.exported.typeDefs.includes(key)) {
                delete this.result.scope.typeDefs[key];
            }
        });
        // Remove any function that was not exported
        Object.keys(this.result.scope.functions).forEach((key) => {
            if (!this.exported.functions.includes(key)) {
                delete this.result.scope.functions[key];
            }
        });

        return this.result;
    }

    /**
     * @description Add imported module.
     * @param {string} name Module name
     * @param {ST_Module} module Module scope
     * @param {ImportDeclaration} node Import declaration node
     */
    public addModule = (name: string, module: ST_Module, node: ImportDeclaration) => {
        // Re-generate the class identifier for the current file
        module.scope.classes = Object.entries(module.scope.classes).reduce(
            (classes, [name, value]) => ({
                ...classes,
                [name]: {
                    ...value,
                    id: this.nextClassCounter,
                },
            }),
            {},
        );
        this.modules = {
            ...this.modules,
            [name]: {
                ...module,
                name,
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            },
        };
    };

    /**
     * @description Getter that returns a frozen context of the imported modules
     */
    get importedModules() {
        return Object.freeze(this.modules);
    }

    get classes(): Record<string, ST_Class> {
        return this.result.scope.classes;
    }

    /**
     * @description Get global properties.
     * @returns {ST_Properties} ROOT properties
     */
    get globalProperties(): ST_Properties {
        return this.result.scope.properties;
    }

    /**
     * @description Get Global type definitions.
     * @returns {ST_TypeDefs} ROOT type definitions.
     */
    get globalTypeDefs(): ST_TypeDefs {
        return this.result.scope.typeDefs;
    }

    lambdaCounter = 0;
    get nextLambdaId(): number {
        return this.lambdaCounter++;
    }

    contractClassCounter = 0;
    get nextClassCounter(): number {
        return this.contractClassCounter++;
    }

    /**
     * Counter used to generate identifiers for scenario variables;
     */
    scenarioVarCounter = 0;
    get nextScenarioVarId(): number {
        return this.scenarioVarCounter++;
    }
}

export class OutputContext extends BaseContext {
    /**
     * @description Initialize new scope.
     */
    enterScope(kind: ST_ScopeKind = ST_ScopeKind.Generic): void {
        this.scopes.push(new Scope(kind, /* Parent scope */ this.scope));
        if (kind === ST_ScopeKind.Global) {
            if (this.scopes.length !== 1) {
                ErrorUtils.failWith('Can only exist a single global scope.');
            }
            this.result.scope = this.scope;
        }
    }

    /**
     * @description Remove previous scope.
     */
    exitScope(): void {
        this.scopes.pop();
    }

    /**
     * @description Get properties by class.
     * @param {string} className
     * @returns {ST_Properties} properties
     */
    getClassProperties(className: string): ST_Properties {
        return this.result.scope.classes[className].scope.properties;
    }

    /**
     * @description Get class method
     * @param {ST_Class} contractClass
     * @param {string} methodName
     * @returns {ST_FunctionProperty} The method
     */
    getClassMethod(contractClass: ST_Class, methodName: string): ST_FunctionProperty {
        return contractClass.scope.functions[methodName];
    }

    /**
     * @description Get current class
     * @returns {ST_Class} class being declared
     */
    get currentClass(): ST_Class {
        if (this.declaringClass) {
            return this.result.scope.classes[this.declaringClass];
        }
        return ErrorUtils.failWith('Currently, there is no class being declared.');
    }

    /**
     * @description Get current method being interpreted.
     * @returns {ST_FunctionProperty} The method
     */
    get currentMethod(): ST_FunctionProperty {
        if (this.declaringMethod) {
            if (
                this.declaringClass &&
                this.result.scope.classes[this.declaringClass].scope.functions[this.declaringMethod]
            ) {
                return this.result.scope.classes[this.declaringClass].scope.functions[this.declaringMethod];
            }

            for (const scope of [...this.scopes].reverse()) {
                if (this.declaringMethod in scope.functions) {
                    return scope.functions[this.declaringMethod];
                }
            }
        }
        return ErrorUtils.failWith('Currently, there is no method being declared.');
    }

    /**
     * @description Get current scenario being interpreted.
     * @returns {ST_Scenario} The scenario
     */
    get currentScenario(): ST_Scenario {
        if (this.result.scenarios.length > 0) {
            return [...this.result.scenarios].reverse()[0];
        }
        return ErrorUtils.failWith('Currently, there is no scenario being interpreted.');
    }

    /**
     * @description Record class declaration;
     * @param {ClassDeclaration} node Class declaration node.
     * @returns {string} Class name.
     */
    emitClassDeclaration = (node: ClassDeclaration): string => {
        // Get class name
        const className = IdentifierUtils.extractName(node);
        if (className) {
            // Get Heritage
            const getExtendedClass = (nodes: NodeArray<HeritageClause>): ST_Class => {
                for (const node of nodes) {
                    if (node.token === SyntaxKind.ExtendsKeyword) {
                        // It is an extends heritage
                        const expression = node.types[0].expression;
                        if (isIdentifier(expression)) {
                            const name = IdentifierUtils.extractName(expression);
                            if (!name) {
                                return this.emitError(node, 'Could not extract class name.');
                            }
                            return this.classes[name];
                        }
                        if (isPropertyAccessExpression(expression)) {
                            const moduleName = IdentifierUtils.extractName(expression.expression);
                            if (!moduleName) {
                                return this.emitError(node, 'Could not extract module name.');
                            }
                            const extendedClassName = IdentifierUtils.extractName(expression);
                            if (!extendedClassName) {
                                return this.emitError(node, 'Could not extract class name.');
                            }

                            const module = this.modules[moduleName];
                            if (!module) {
                                return this.emitError(expression, `Cannot find any module named (${moduleName}).`);
                            }

                            const extendedClass = this.modules[moduleName].scope.classes[extendedClassName];
                            if (!extendedClass) {
                                return this.emitError(
                                    expression,
                                    `Cannot find any class named (${extendedClassName}) in module (${moduleName}).`,
                                );
                            }

                            return extendedClass;
                        }
                    }
                }
                // This should never happen, but we have the logs if it happens.
                return this.emitError(nodes[0], 'Expected an extend heritage, but could not find any.');
            };

            if (node.heritageClauses) {
                // Extract scope to avoid cycle references when cloning class object.
                const { scope, ...classObj } = getExtendedClass(node.heritageClauses);
                this.scope.properties = scope.properties;
                this.scope.functions = scope.functions;
                this.classes[className] = {
                    ...JSON.parse(JSON.stringify(classObj)),
                    id: this.nextClassCounter,
                    scope: this.scope,
                    name: className,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            } else {
                this.classes[className] = {
                    name: className,
                    id: this.nextClassCounter,
                    constructorArgs: {},
                    decorators: {},
                    entry_points: {},
                    views: {},
                    global_lambdas: {},
                    metadata: {},
                    scope: this.scope,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
        } else {
            return this.emitError(node, 'Anonymous classes are not allowed.');
        }

        if (ModifiersUtils.extractModifiers(node).includes(ST_Modifier.Export)) {
            this.exported.classes = [...this.exported.classes, className];
        }

        return className;
    };

    /**
     * @description Record type definition.
     * @param {string} name Type name
     * @param {ST_TypeDef} typeDef Type specification
     * @param {ST_Modifier[]} modifiers type modifiers (export, etc...)
     */
    emitTypeDef = (name: string, typeDef: ST_TypeDef, modifiers: ST_Modifier[]): void => {
        // Add type definition to the current scope
        this.scope.typeDefs[name] = typeDef;

        if (modifiers.includes(ST_Modifier.Export)) {
            this.exported.typeDefs = [...this.exported.typeDefs, name];
        }
    };

    /**
     * @description Record variable declaration.
     * @param {ST_VariableStatement} variable
     */
    emitVariableDeclaration = (variable: ST_VariableStatement): void => {
        if (this.scope.properties[variable.name] && !this.declaringClass) {
            return ErrorUtils.failWithInfo({
                msg: `Duplicated variable (${variable.name}).`,
                line: variable.line,
            });
        }
        // Add property to the current scope
        this.scope.properties[variable.name] = variable;

        // Interpret as exported every variable declaration with export modifier and declared on the root scope
        if (variable.modifiers.includes(ST_Modifier.Export) && this.scope.kind === ST_ScopeKind.Global) {
            this.exported.properties = [...this.exported.properties, variable.name];
        }
    };

    /**
     * @description Record method declaration.
     * @param {string} name method name
     * @param {ST_TypeDef} type method signature
     * @param {ST_Modifier[]} modifiers method modifiers (export, etc...)
     * @param {Node} node function node
     */
    emitFunctionDeclaration = (
        name: string,
        type: Extract<ST_TypeDef, { type: FrontendType.TFunction | FrontendType.TLambda }>,
        modifiers: ST_Modifier[],
        node: FunctionDeclaration | MethodDeclaration | ArrowFunction,
        decorators: ST_Property_Decorators,
    ): void => {
        const line = LineUtils.getLineAndCharacter(this.sourceFile, node);
        const id = this.nextLambdaId;

        // Check if PrivateLambda decorator exists and if it is configured to include operations and storage
        const withOperations = decorators.PrivateLambda ? decorators.PrivateLambda.withOperations : false;
        this.emitPropertyDecorators(decorators);
        // Add function to the current scope
        if (this.scope.parent) {
            this.scope.parent.functions[name] = {
                name,
                type,
                id,
                typeDefs: {},
                properties: {},
                statements: {},
                withStorage: decorators.PrivateLambda?.withStorage,
                withOperations,
                scope: this.scope,
                decorators,
                line,
            };
        }

        if (modifiers.includes(ST_Modifier.Export)) {
            this.exported.functions = [...this.exported.functions, name];
        }
    };

    /**
     * @description Record method statement.
     * @param {ST_Statement} statement method statement
     */
    emitFunctionStatement = (statement: ST_Statement): void => {
        if (!this.declaringMethod) {
            return ErrorUtils.failWithInfo({
                msg: 'Cannot emit function statements outside of a function block.',
                line: statement.line,
                fileName: this.sourceFile.fileName,
            });
        }
        this.currentMethod.statements = {
            ...this.currentMethod.statements,
            [Object.keys(this.currentMethod.statements).length]: statement,
        };
    };

    /**
     * @description Record class decorator.
     * @param name Class name
     * @param decorator Class decorator
     */
    emitClassDecorator = (className: string, decorator: ST_Class_Decorator): void => {
        if (decorator.kind === ST_DecoratorKind.Contract) {
            // Means that current class is a contract
            this.result.contracts[className] = {
                name: className,
                classRef: className,
            };
        }
        this.classes[className].decorators[decorator.kind] = decorator;
    };

    /**
     * @description Record property decorator.
     * @param decorator Class decorator
     * @param propertyName Property name
     */
    emitPropertyDecorators = (decorators: ST_Property_Decorators): void => {
        Object.values(decorators).forEach((decorator) => {
            if (decorator && this.declaringClass) {
                switch (decorator.kind) {
                    case ST_DecoratorKind.EntryPoint:
                        // Means that the current property is an entry_point
                        this.currentClass.entry_points[decorator.functionRef] = decorator;
                        break;
                    case ST_DecoratorKind.View:
                        // Means that the current property is a view
                        this.currentClass.views[decorator.functionRef] = decorator;
                        break;
                    case ST_DecoratorKind.PrivateLambda:
                        // Means that the current property is an global_lambda view
                        this.currentClass.global_lambdas[decorator.functionRef] = decorator;
                        break;
                    case ST_DecoratorKind.MetadataBuilder:
                        this.currentClass.metadata[decorator.name] = decorator.properties;
                        break;
                }
            }
        });
    };

    /**
     * @description Record compilation target.
     * @param {string} name compilation target name
     * @param {string} className Contract className
     * @param {ST_Expression[]} args arguments expected by the class constructor
     */
    emitCompilationTarget = (name: string, contractClass: ST_Class, args: ST_TypedExpression[]): void => {
        this.result.scenarios = [
            ...this.result.scenarios,
            {
                kind: ST_ScenarioKind.Compilation,
                name,
                scenario: [
                    this.getNewContractAction(
                        {
                            name,
                            className: contractClass.name,
                            args,
                        },
                        contractClass,
                    ),
                ],
                longname: name,
                shortname: name,
                show: false,
            },
        ];
    };

    /**
     * @description Emit scenario.
     * @param {ST_Scenario} scenario
     */
    emitScenario = (scenario: ST_Scenario): void => {
        this.result.scenarios = [...this.result.scenarios, scenario];
    };

    /**
     * @description Emit a scenario action.
     * @param {ST_ScenarioAction} action Scenario Action
     */
    emitScenarioAction = (action: ST_ScenarioAction): void => {
        this.currentScenario.scenario = [...this.currentScenario.scenario, action];
    };

    emitError = (node: Node, msg: string): never => {
        return ErrorUtils.failWithInfo({
            msg,
            line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            text: node.getText(this.sourceFile),
            fileName: this.sourceFile.fileName,
        });
    };

    emitWarning = (node: Node, msg: string): void => {
        this.result.warnings = [
            ...this.result.warnings,
            {
                msg,
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            },
        ];
    };

    getResult(): ST_Scenario[] {
        return this.result.scenarios;
    }

    public exportContract = (
        contract: ST_Class,
        storage: ST_Expression & { type?: ST_TypeDef },
        initialBalance = '0',
    ): string => {
        const templateId = staticId(contract.id, LineUtils.getLineNumber(contract.line));
        const storageString = `${this.translators.Expression.translateExpression(storage)}`;
        const storageType = `${this.translators.Type.translateType(storage?.type || TypeBuilder.unknown())}`;
        const messages = `${this.buildMessages(contract)}`;
        const views = `${this.buildViews(contract)}`;
        const globals = `${this.buildGlobals(contract)}`;
        const initial_metadata = `${this.buildMetadata(contract)}`;

        return `(
            template_id ${templateId}
            storage ${storageString}
            storage_type (${storageType})
            entry_points (${messages})
            flags (${this.stringifyFlags(contract)})
            privates (${globals})
            views (${views})
            entry_points_layout ()
            initial_metadata (${initial_metadata})
            balance ${this.translators.Expression.translateExpression(
                ExpressionBuilder.mutezLiteral(initialBalance, contract.line),
            )}
        )`;
    };

    public getNewContractAction = (
        compilation: ST_CompilationTarget,
        contractClass: ST_Class,
        show = true,
        initialBalance = '0',
    ): ST_ScenarioAction => {
        const storage: ST_Expression = compilation.args?.[contractClass.constructorArgs?.['storage']?.index] ||
            contractClass.constructorArgs['storage']?.initializer ||
            contractClass.scope.properties['storage']?.expression || ExpressionBuilder.unit(contractClass.line);
        const templateId = staticId(contractClass.id, LineUtils.getLineNumber(contractClass.line));
        const exportInfo = this.exportContract(contractClass, storage, initialBalance);
        return {
            action: ST_ScenarioActionKind.NEW_CONTRACT,
            name: compilation.className,
            id: templateId,
            export: exportInfo,
            line_no: LineUtils.getLineNumber(contractClass.line),
            show,
            accept_unknown_types: false,
        };
    };

    stringifyFlags(contract: ST_Class): string {
        const flags = contract.decorators.Contract?.flags || [];
        const getFlag = ({ name, args }: ST_Flag): string => {
            return '(' + `${name} ${args.join(' ')}`.trim() + ')';
        };
        return flags.map<string>(getFlag).join(' ');
    }

    buildMessages(contract: ST_Class): string {
        /**
         * For each entry_point:
         *  - Template: (name private(bool) has_params(bool) line_no (instructions))
         */
        return Object.values(contract.entry_points)
            .map((ep): string | void => {
                const func = this.getClassMethod(contract, ep.functionRef);
                if (guards.type.isFunction(func.type)) {
                    const notMock = this.capitalizeBoolean(!ep.mock);
                    const isLazy = this.capitalizeBoolean(ep.lazy);
                    const emptyLazy = this.capitalizeBoolean(ep.lazy_no_code);
                    const checkNoIncomingTransfer = (ep.check_no_incoming_transfer === undefined ? 'None' : this.capitalizeBoolean(ep.check_no_incoming_transfer));
                    const hasParams = this.capitalizeBoolean(Object.keys(func.type.inputTypes).length > 0);
                    const epType = ep.type ? this.translators.Type.translateType(ep.type) : 'None';
                    const line = LineUtils.getLineNumber(func.line);
                    const statements = this.translators.Statement.translateStatements(func.statements);
                    const inputTypes = this.translators.Type.translateInputType(func.type);
                    return `(${ep.name} ${notMock} ${isLazy} ${emptyLazy} ${checkNoIncomingTransfer} ${hasParams} ${epType} ${line} (${inputTypes ? `${inputTypes} ` : ''}${statements}))`;
                }
            })
            .filter(nonFalsy)
            .join(' ');
    }

    buildViews(contract: ST_Class): string {
        /**
         * For each offchain view:
         *  - Template: (name has_params(bool) line_no is_pure doc (instructions))
         */
        return Object.values(contract.views)
            .map(({ name, pure, description = '', functionRef, offchainOnly }): string | void => {
                const func = this.getClassMethod(contract, functionRef);
                if (guards.type.isFunction(func.type)) {
                    const isPure = this.capitalizeBoolean(pure);
                    const docString = JSON.stringify(description);
                    const hasParams = this.capitalizeBoolean(Object.keys(func.type.inputTypes).length > 0);
                    const line = LineUtils.getLineNumber(func.line);
                    const inputTypes = this.translators.Type.translateInputType(func.type);
                    const statements = this.translators.Statement.translateStatements(func.statements);
                    return `(${
                        offchainOnly ? 'offchain' : 'onchain'
                    } ${name} ${hasParams} ${line} ${isPure} ${docString} (${
                        inputTypes ? ` ${inputTypes}` : ''
                    }${statements}))`;
                }
            })
            .join(' ');
    }

    buildGlobals(contract: ST_Class): string {
        /**
         * For each global_view:
         *  - Template: (<name> (lambda <id> <argNames> line_no (instructions))
         */
        return Object.values(contract.global_lambdas)
            .map(({ name, functionRef }): string | void => {
                const func = this.getClassMethod(contract, functionRef);
                if (guards.type.isFunction(func.type)) {
                    const expression: ST_Expression = {
                        kind: ST_ExpressionKind.LambdaExpression,
                        lambda: func,
                        line: func.line,
                    };
                    return `(${name} ${this.translators.Expression.translateExpression(expression)})`;
                }
            })
            .filter(nonFalsy)
            .join(' ');
    }

    buildMetadata(contract: ST_Class): string {
        return Object.entries(contract.metadata)
            .map(([name, properties]): string | void => {
                return `(${name} ${MetadataTranslator.translateMetadataObject(
                    properties,
                    LineUtils.getLineNumber(contract.line),
                )})`;
            })
            .join(' ');
    }

    capitalizeBoolean = (bool: boolean): string => (bool ? 'True' : 'False');
}

export default OutputContext;
