import { ST_VariableValueKind } from '../../enums/statement';
import ErrorUtils from '../utils/Error';
import LineUtils from '../utils/Line';
import LiteralTranslator from './literal';
import * as Native from '../../enums/native';
import { ST_BinaryToken, ST_ExpressionKind } from '../../enums/expression';
import { FrontendType } from '../../enums/type';
import PrinterUtils from '../utils/printer';
import { ST_LiteralKind } from '../../enums/literal';
import type { ST_Expression } from '../../@types/expression';
import type { ST_TypeDef } from '../../@types/type';
import { capitalizeBoolean, staticId } from './misc';
import ExpressionBuilder from '../utils/builders/expression';
import TypeBuilder from '../utils/builders/type';
import { TranslatorBase } from './Base';

const set = (elements: string, line: string) => `(${line} set ${elements})`;
const list = (elements: string, line: string) => `(${line} list ${elements})`;
const tuple = (elements: string, line: string) => `(${line} tuple ${elements})`;
const map = (elements: string, line: string) => `(${line} map ${elements})`;
const big_map = (elements: string, line: string) => `(${line} big_map ${elements})`;

const ediv = (left: string, right: string, line: string) => `(${line} ediv ${left} ${right})`;

const record = (literals: string[], line: string) => `(${line} record ${literals.join(' ')})`;

const variant = (name: string, literal: string, line: string) => `(${line} variant "${name}" ${literal})`;

const some = (children: string, line: string) => `(${line} variant "Some" ${children})`;

const defineLocal = (name: string, node: string, line: string): string => `(${line} define_local "${name}" ${node})`;

const getLocal = (name: string, line: string) => `(${line} get_local "${name}")`;

const attr = (left: string, right: string, line: string): string => `(${line} attr ${left} "${right}")`;

const lambdaParams = (id: number, name: string, type: string, line: string) =>
    `(${line} lambda_params ${id} "${name}" ${type})`;

const params = (line: string) => `(${line} params)`;

const never = (children: string, line: string) => `(${line} never ${children})`;

const verify_with_message = (condition: string, message: string, line: string) =>
    `(${line} verify ${condition} ${message})`;

const verify_without_message = (condition: string, line: string) => `(${line} verify ${condition})`;

const failWith = (msg: string, line: string) => `(${line} ${ST_ExpressionKind.FailWith} ${msg})`;

const type_annotation = (expr: string, type: string, line: string) => `(${line} type_annotation ${expr} ${type})`;

const asNat = (expr: string, line: string) => `(${line} open_variant (${line} is_nat ${expr}) "Some" "None")`;

const not = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.Not} ${expr})`;

const negate = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.Negate} ${expr})`;

const toInt = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.ToInt} ${expr})`;

const abs = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.ABS} ${expr})`;

const reverse = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.Reverse} ${expr})`;

const size = (source: string, line: string) => `(${line} ${ST_ExpressionKind.Size} ${source})`;

const contains = (source: string, subject: string, line: string) =>
    `(${line} ${ST_ExpressionKind.Contains} ${source} ${subject})`;

const getItem = (source: string, subject: string, line: string) =>
    `(${line} ${ST_ExpressionKind.GetItem} ${source} ${subject})`;

const getItemDefault = (source: string, subject: string, defaultValue: string, line: string) =>
    `(${line} get_item_default ${source} ${subject} ${defaultValue})`;

const delItem = (source: string, subject: string, line: string) =>
    `(${line} ${ST_ExpressionKind.DelItem} ${source} ${subject})`;

const setItem = (leftExpr: string, rightExpr: string, line: string) =>
    `(${line} ${ST_ExpressionKind.SetItem} ${leftExpr} ${rightExpr})`;

const cons = (leftExpr: string, rightExpr: string, line: string) => `(${line} cons ${rightExpr} ${leftExpr})`;

const unit = (line: string) => `(${line} literal (unit))`;

const none = (line: string) => `(${line} variant "None" (${line} literal (unit)))`;

const iter = (name: string, line: string) => `(${line} iter "${name}")`;

const lessThan = (leftExpr: string, rightExpr: string, line: string) => `(${line} lt ${leftExpr} ${rightExpr})`;

const greaterThan = (leftExpr: string, rightExpr: string, line: string) => `(${line} gt ${leftExpr} ${rightExpr})`;

const lessThanEquals = (leftExpr: string, rightExpr: string, line: string) => `(${line} le ${leftExpr} ${rightExpr})`;

const greaterThanEquals = (leftExpr: string, rightExpr: string, line: string) =>
    `(${line} ge ${leftExpr} ${rightExpr})`;

const eq = (leftExpr: string, rightExpr: string, line: string) => `(${line} eq ${leftExpr} ${rightExpr})`;

const neq = (leftExpr: string, rightExpr: string, line: string) => `(${line} neq ${leftExpr} ${rightExpr})`;

const or = (leftExpr: string, rightExpr: string, line: string) => `(${line} or ${leftExpr} ${rightExpr})`;

const and = (leftExpr: string, rightExpr: string, line: string) => `(${line} and ${leftExpr} ${rightExpr})`;

const variant_arg = (name: string, line: string) => `(${line} variant_arg "${name}")`;

const transfer = (params: string, amount: string, contract: string, line: string) =>
    `(${line} transfer ${params} ${amount} ${contract})`;

const setDelegate = (baker: string, line: string) => `(${line} set_delegate ${baker})`;

const createContractOperation = (contract: string, storage: string, amount: string, baker: string, line: string) =>
    `(${line} create_contract ${contract} ${storage} ${baker} ${amount})`;

const addOperation = (operation: string, line: string) =>
    `(${line} set (${line} operations) (${line} cons ${operation} (${line} operations)))`;

const lambda = (
    id: number,
    withStorage: string,
    withOperations: string,
    params: string,
    statements: string,
    line: string,
) => `(${line} lambda ${id} ${withStorage} ${withOperations} None "${params}" (${statements}))`;

const callLambda = (lambda: string, args: string, line: string) => `(${line} call_lambda ${lambda} ${args})`;

const privateLambda = (name: string, line: string) => `(${line} global "${name}")`;

const pack = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.Pack} ${expr})`;

const unpack = (expr: string, type: string, line: string) => `(${line} ${ST_ExpressionKind.Unpack} ${expr} ${type})`;

const openVariant = (expr: string, name: string, errorMsg = '', line: string) =>
    `(${line} ${ST_ExpressionKind.OpenVariant} ${expr} "${name}" ${errorMsg})`;

const isVariant = (expr: string, name: string, line: string) =>
    `(${line} ${ST_ExpressionKind.IsVariant} ${expr} "${name}")`;

const first = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.PairCAR} ${expr})`;

const second = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.PairCDR} ${expr})`;

const self = (ep: string, line: string) => `(${line} self "${ep}")`;

const contract = (paramType: string, ep: string, address: string, line: string) =>
    `(${line} contract "${ep}" ${paramType} ${address})`;

const mod = (leftExpr: string, rightExpr: string, line: string) =>
    `(${line} ${ST_BinaryToken.Mod} ${leftExpr} ${rightExpr})`;

const accountOfSeed = (seed: string, line: string) => `(${line} account_of_seed "${seed}")`;

const resolve = (expr: string, line: string) => `(${line} resolve ${expr})`;

const votingPower = (keyHash: string, line: string) => `(${line} voting_power ${keyHash})`;

const checkSignature = (pubKey: string, signature: string, contentExpr: string, line: string) =>
    `(${line} ${ST_ExpressionKind.CheckSignature} ${pubKey} ${signature} ${contentExpr})`;

const pairingCheck = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.PairingCheck} ${expr})`;

const implicitAccount = (hashKey: string, line: string) => `(${line} implicit_account ${hashKey})`;

const hashKey = (key: string, line: string) => `(${line} hash_key ${key})`;

const toAddress = (contract: string, line: string) => `(${line} to_address ${contract})`;

const concat = (expr: string, line: string) => `(${line} concat ${expr})`;

const slice = (offset: string, length: string, expr: string, line: string) =>
    `(${line} ${ST_ExpressionKind.Slice} ${offset} ${length} ${expr})`;

const updateSet = (target: string, expr: string, add: string, line: string) =>
    `(${line} ${ST_ExpressionKind.UpdateSet} ${target} ${expr} ${add})`;

const elements = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.GetElements} ${expr})`;

const keys = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.GetKeys} ${expr})`;

const values = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.GetValues} ${expr})`;

const entries = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.GetEntries} ${expr})`;

const addSeconds = (target: string, expr: string, line: string) =>
    `(${line} ${ST_ExpressionKind.AddSeconds} ${target} ${expr})`;

const viewAccessExpr = (name: string, address: string, args: string, type: string, line: string) =>
    `(${line} ${ST_ExpressionKind.ViewAccessExpr} "${name}" ${args} ${address} ${type})`;

const staticViewAccessExpr = (name: string, id: string, args: string, line: string) =>
    `(${line} ${ST_ExpressionKind.StaticViewAccessExpr} "${name}" ${id} ${args})`;

const constant = (hash: string, type: string, line: string) =>
    `(${line} ${ST_ExpressionKind.Constant} "${hash}" ${type})`;

const open_chest = (chest_key: string, chest: string, time: string, line: string) =>
    `(${line} ${ST_ExpressionKind.OpenChest} ${chest_key} ${chest} ${time})`;

// + Test Scenario
const isFailing = (expr: string, line: string) => `(${line} ${ST_ExpressionKind.IsFailing} ${expr})`;
const catchException = (expr: string, type: string, line: string) =>
    `(${line} ${ST_ExpressionKind.CatchException} ${expr} ${type})`;
const scenarioVar = (id: number, line: string) => `(${line} ${ST_ExpressionKind.ScenarioVariable} ${id})`;
const scenarioConstantVar = (id: number, line: string) =>
    `(${line} ${ST_ExpressionKind.ScenarioConstantVariable} ${id})`;
const contractData = (id: string, line: string) => `(${line} contract_data ${id})`;
const contractBalance = (id: string, line: string) => `(${line} contract_balance ${id})`;
export const contractAddress = (id: string, line: string): string => `(${line} contract_address ${id} "")`;
const contractBaker = (id: string, line: string) => `(${line} contract_baker ${id})`;
const contractTyped = (id: string, epName: string, line: string) => `(${line} contract_typed ${id} "${epName}")`;
const makeSignature = (privateKey: string, content: string, line: string) =>
    `(${line} make_signature ${privateKey} ${content} "Raw")`;
// - Test Scenario

export default class ExpressionTranslator extends TranslatorBase {
    translateExpression = (expression: ST_Expression, type?: ST_TypeDef): string => {
        const line = LineUtils.getLineNumber(expression.line);
        switch (expression.kind) {
            case ST_ExpressionKind.BinaryExpr:
                switch (expression.operator) {
                    case ST_BinaryToken.EqualsEquals:
                    case ST_BinaryToken.EqualsEqualsEquals: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return eq(leftExpr, rightExpr, line);
                    }
                    case ST_BinaryToken.ExclamationEquals:
                    case ST_BinaryToken.ExclamationEqualsEquals: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return neq(leftExpr, rightExpr, line);
                    }
                    case ST_BinaryToken.Equals: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return setItem(leftExpr, rightExpr, LineUtils.getLineNumber(expression.line));
                    }
                    case ST_BinaryToken.Minus: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return `(${LineUtils.getLineNumber(expression.line)} sub ${leftExpr} ${rightExpr})`;
                    }
                    case ST_BinaryToken.Plus: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return `(${LineUtils.getLineNumber(expression.line)} add ${leftExpr} ${rightExpr})`;
                    }
                    case ST_BinaryToken.Mul: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return `(${line} mul_homo ${leftExpr} ${rightExpr})`;
                    }
                    case ST_BinaryToken.MulOverloaded: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return `(${line} ${ST_BinaryToken.MulOverloaded} ${leftExpr} ${rightExpr})`;
                    }
                    case ST_BinaryToken.Div: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return `(${line} div ${leftExpr} ${rightExpr})`;
                    }
                    case ST_BinaryToken.Mod: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return mod(leftExpr, rightExpr, line);
                    }
                    //
                    case ST_BinaryToken.LessThan: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return lessThan(leftExpr, rightExpr, line);
                    }
                    case ST_BinaryToken.GreaterThan: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return greaterThan(leftExpr, rightExpr, line);
                    }
                    case ST_BinaryToken.LessThanEquals: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return lessThanEquals(leftExpr, rightExpr, line);
                    }
                    case ST_BinaryToken.GreaterThanEquals: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return greaterThanEquals(leftExpr, rightExpr, line);
                    }
                    //
                    case ST_BinaryToken.Or: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return or(leftExpr, rightExpr, line);
                    }
                    case ST_BinaryToken.And: {
                        const leftExpr = this.translateExpression(expression.left);
                        const rightExpr = this.translateExpression(expression.right);
                        return and(leftExpr, rightExpr, line);
                    }
                    default:
                        return ErrorUtils.failWithInfo({
                            line: expression.line,
                            msg: `Invalid left-hand side in assignment (${expression.operator}).`,
                        });
                }
            // Hash Functions
            case ST_ExpressionKind.Blake2b:
            case ST_ExpressionKind.Sha256:
            case ST_ExpressionKind.Sha512:
            case ST_ExpressionKind.Sha3:
            case ST_ExpressionKind.Keccak:
                const expr = this.translateExpression(expression.expression);
                return `(${line} ${expression.kind} ${expr})`;
            //
            case ST_ExpressionKind.Variant: {
                const kindExpr = expression.action;
                if (
                    kindExpr.kind === ST_ExpressionKind.LiteralExpr &&
                    kindExpr.literal.kind === ST_LiteralKind.String
                ) {
                    const name = kindExpr.literal.value;
                    const expr = this.translateExpression(expression.value);
                    return variant(name, expr, line);
                }
                return ErrorUtils.failWithInfo({
                    msg: 'Variant kind is invalid.',
                    line: expression.line,
                });
            }
            case ST_ExpressionKind.ForIterator: {
                const name = expression.name;
                return iter(name, line);
            }
            case ST_ExpressionKind.Not: {
                const expr = this.translateExpression(expression.expression);
                return not(expr, line);
            }
            case ST_ExpressionKind.UpdateSet: {
                const target = this.translateExpression(expression.target);
                const expr = this.translateExpression(expression.expression);
                const add = expression.add ? 'True' : 'False';
                return updateSet(target, expr, add, line);
            }
            case ST_ExpressionKind.AddSeconds: {
                const target = this.translateExpression(expression.target);
                const expr = this.translateExpression(expression.expression);
                return addSeconds(target, expr, line);
            }
            case ST_ExpressionKind.GetElements: {
                const expr = this.translateExpression(expression.expression);
                return elements(expr, line);
            }
            case ST_ExpressionKind.GetKeys: {
                const expr = this.translateExpression(expression.expression);
                return keys(expr, line);
            }
            case ST_ExpressionKind.GetValues: {
                const expr = this.translateExpression(expression.expression);
                return values(expr, line);
            }
            case ST_ExpressionKind.GetEntries: {
                const expr = this.translateExpression(expression.expression);
                return entries(expr, line);
            }
            case ST_ExpressionKind.Negate: {
                const expr = this.translateExpression(expression.expression);
                return negate(expr, line);
            }
            case ST_ExpressionKind.ToInt: {
                const expr = this.translateExpression(expression.expression);
                return toInt(expr, line);
            }
            case ST_ExpressionKind.ToNat: {
                const expr = this.translateExpression(expression.expression);
                return asNat(expr, line);
            }
            case ST_ExpressionKind.ABS: {
                const expr = this.translateExpression(expression.expression);
                return abs(expr, line);
            }
            case ST_ExpressionKind.Concat: {
                const expr = this.translateExpression(expression.expression);
                return concat(expr, line);
            }
            case ST_ExpressionKind.Slice: {
                const offset = this.translateExpression(expression.offset);
                const length = this.translateExpression(expression.length);
                const expr = this.translateExpression(expression.expression);
                return slice(offset, length, expr, line);
            }
            case ST_ExpressionKind.Reverse: {
                const expr = this.translateExpression(expression.expression);
                return reverse(expr, line);
            }
            case ST_ExpressionKind.Contains: {
                const source = this.translateExpression(expression.source);
                const subject = this.translateExpression(expression.subject);
                return contains(source, subject, line);
            }
            case ST_ExpressionKind.GetItem: {
                const source = this.translateExpression(expression.source);
                const subject = this.translateExpression(expression.subject);
                if (expression.defaultValue) {
                    const defaultValue = this.translateExpression(expression.defaultValue);
                    return getItemDefault(source, subject, defaultValue, line);
                }
                return getItem(source, subject, line);
            }
            case ST_ExpressionKind.DelItem: {
                const source = this.translateExpression(expression.source);
                const subject = this.translateExpression(expression.subject);
                return delItem(source, subject, line);
            }
            case ST_ExpressionKind.Push: {
                const source = this.translateExpression(expression.source);
                const subject = this.translateExpression(expression.subject);
                return setItem(source, cons(source, subject, line), line);
            }
            case ST_ExpressionKind.SetItem: {
                const target = this.translateExpression(expression.target);
                const value = this.translateExpression(expression.value);
                return setItem(target, value, line);
            }
            case ST_ExpressionKind.Size: {
                const source = this.translateExpression(expression.expression);
                return size(source, line);
            }
            case ST_ExpressionKind.GetLocal: {
                return getLocal(expression.name, line);
            }
            case ST_ExpressionKind.AttrAccessExpr: {
                if (!expression.prev) {
                    return ErrorUtils.failWithInfo({
                        msg: 'This should never happen, please report it',
                        line: expression.line,
                    });
                }
                const parent = this.translateExpression(expression.prev);
                return attr(parent, expression.attr, line);
            }
            case ST_ExpressionKind.StorageAccessExpr:
                return `(${line} data)`;
            case ST_ExpressionKind.OperationsAccessExpr:
                return `(${line} operations)`;
            case ST_ExpressionKind.MethodParamAccessExpr: {
                // Spec: (attr (params <line_no>) <field> <line_no>)
                if (expression.singleParam) {
                    return params(line);
                }
                return attr(params(line), expression.attr, line);
            }
            case ST_ExpressionKind.LambdaParamAccessExpr: {
                // Spec: (attr (lambdaParams <line_no>) <field> <line_no>)
                const type = this.translators.Type.translateType(expression.type);
                const param = lambdaParams(expression.id, expression.attr, type, line);
                if (expression.singleParam) {
                    return param;
                }
                return attr(param, expression.attr, line);
            }
            case ST_ExpressionKind.LambdaCallExpr: {
                const lambda = this.translateExpression(expression.lambda);
                const args = this.translateExpression(expression.arguments);
                return callLambda(lambda, args, line);
            }
            case ST_ExpressionKind.ViewAccessExpr: {
                const name = expression.name;
                const type = this.translators.Type.translateType(expression.type);
                const args = this.translateExpression(expression.arguments);
                return viewAccessExpr(name, expression.address, args, type, line);
            }
            case ST_ExpressionKind.StaticViewAccessExpr: {
                const name = expression.name;
                const id = expression.id;
                const args = this.translateExpression(expression.arguments);
                return staticViewAccessExpr(name, id, args, line);
            }
            case ST_ExpressionKind.PrivateLambdaAccessExpr: {
                return privateLambda(expression.name, line);
            }
            case ST_ExpressionKind.PrivateLambdaResult: {
                const expr: ST_Expression = {
                    kind: ST_ExpressionKind.AttrAccessExpr,
                    attr: 'result',
                    type: TypeBuilder.unknown(),
                    prev: {
                        kind: ST_ExpressionKind.MethodPropAccessExpr,
                        attr: expression.name,
                        type: TypeBuilder.unknown(),
                        line: expression.line,
                    },
                    line: expression.line,
                };
                return this.translateExpression(expr);
            }
            case ST_ExpressionKind.MethodPropAccessExpr: {
                return getLocal(expression.attr, line);
            }
            case ST_ExpressionKind.NativePropAccessExpr: {
                switch (expression.prop) {
                    case Native.ST_SpValue.none:
                        return none(line);
                    case Native.ST_SpValue.unit:
                        return unit(line);
                    default:
                        return `(${line} ${expression.prop})`;
                }
            }
            case ST_ExpressionKind.LiteralExpr:
                if (expression.type.type === FrontendType.TContract) {
                    const t = this.translators.Type.translateType(expression.type.inputType);
                    const address = LiteralTranslator.translateLiteral(expression.literal, type || expression.type);
                    const m = LiteralTranslator.translateLiteral(
                        {
                            kind: ST_LiteralKind.String,
                            value: '',
                            line: expression.line,
                        },
                        TypeBuilder.string(),
                    );
                    return openVariant(contract(t, '', address, line), 'Some', m, line);
                }
                return LiteralTranslator.translateLiteral(expression.literal, type || expression.type);
            case ST_ExpressionKind.VariableDeclaration:
                const name = expression.name;
                if (expression.value.kind === ST_VariableValueKind.Literal) {
                    // It is a literal (explicit value)
                    return defineLocal(
                        name,
                        LiteralTranslator.translateLiteral(expression.value.literal, type || expression.type),
                        line,
                    );
                }
                // It is an access expression (lets think of it like a reference for something that will exist at runtime)
                return defineLocal(name, this.translateExpression(expression.value), line);
            case ST_ExpressionKind.Never:
                return never(this.translateExpression(expression.variable), line);
            case ST_ExpressionKind.Verify: {
                const conditionExpr = this.translateExpression(expression.condition);
                if (expression.message) {
                    // A message was provided
                    const message = this.translateExpression(expression.message);
                    return verify_with_message(conditionExpr, message, line);
                }
                return verify_without_message(conditionExpr, line);
            }
            case ST_ExpressionKind.CheckSignature: {
                const publicKeyExpr = this.translateExpression(expression.publicKey);
                const signatureExpr = this.translateExpression(expression.signature);
                const contentExpr = this.translateExpression(expression.content);
                return checkSignature(publicKeyExpr, signatureExpr, contentExpr, line);
            }
            case ST_ExpressionKind.PairingCheck: {
                const expr = this.translateExpression(expression.expression);
                return pairingCheck(expr, line);
            }
            case ST_ExpressionKind.FailWith: {
                const message = this.translateExpression(expression.message);
                return failWith(message, line);
            }
            case ST_ExpressionKind.TypeAnnotation: {
                const expr = this.translateExpression(expression.expression);
                const type = this.translators.Type.translateType(expression.type);
                return type_annotation(expr, type, line);
            }
            case ST_ExpressionKind.ArrayLiteralExpression:
                return this.translateArrayLiteralExpression(expression, type || expression.type);
            case ST_ExpressionKind.ObjectLiteralExpression: {
                return this.translateObjectLiteralExpression(expression, type || expression.type);
            }
            case ST_ExpressionKind.Ediv: {
                const left = this.translateExpression(expression.left);
                const right = this.translateExpression(expression.right);
                return ediv(left, right, line);
            }
            case ST_ExpressionKind.AsExpression: {
                switch (expression.expression.kind) {
                    case ST_ExpressionKind.LiteralExpr: {
                        return this.translateExpression({
                            ...expression.expression,
                            type: expression.type,
                        });
                    }
                    case ST_ExpressionKind.Variant:
                    case ST_ExpressionKind.BinaryExpr:
                    case ST_ExpressionKind.OpenVariant:
                    case ST_ExpressionKind.ObjectLiteralExpression:
                    case ST_ExpressionKind.ArrayLiteralExpression:
                    case ST_ExpressionKind.NativePropAccessExpr:
                    case ST_ExpressionKind.AttrAccessExpr:
                    case ST_ExpressionKind.MethodPropAccessExpr:
                    case ST_ExpressionKind.MethodParamAccessExpr:
                    case ST_ExpressionKind.LambdaParamAccessExpr:
                    case ST_ExpressionKind.VariantAccess:
                    case ST_ExpressionKind.LambdaCallExpr:
                    case ST_ExpressionKind.PrivateLambdaResult:
                        const expr = this.translateExpression(expression.expression, expression.type);
                        switch (expression.type.type) {
                            case FrontendType.TNat:
                                return asNat(expr, line);
                            case FrontendType.TRecord:
                            case FrontendType.TVariant:
                                const type = this.translators.Type.translateType(expression.type);
                                return type_annotation(expr, type, line);
                        }
                        return expr;
                }
                return ErrorUtils.failWithInfo({
                    msg: `Cannot resolve expression (${expression.expression.kind}).`,
                    line: expression.line,
                });
            }
            case ST_ExpressionKind.Some: {
                return some(this.translateExpression(expression.expression), line);
            }
            case ST_ExpressionKind.OpenVariant: {
                const expr = this.translateExpression(expression.source);
                const errorMsg = expression.message ? this.translateExpression(expression.message) : 'None';
                return openVariant(expr, expression.subject, errorMsg, line);
            }
            case ST_ExpressionKind.IsVariant: {
                const expr = this.translateExpression(expression.source);
                return isVariant(expr, expression.subject, line);
            }
            case ST_ExpressionKind.PairCAR: {
                const expr = this.translateExpression(expression.expression);
                return first(expr, line);
            }
            case ST_ExpressionKind.PairCDR: {
                const expr = this.translateExpression(expression.expression);
                return second(expr, line);
            }
            case ST_ExpressionKind.Pack: {
                return pack(this.translateExpression(expression.expression), line);
            }
            case ST_ExpressionKind.Unpack: {
                const t = this.translators.Type.translateType(expression.type);
                const expr = this.translateExpression(expression.expression);
                return unpack(expr, t, line);
            }
            case ST_ExpressionKind.VariantAccess: {
                return variant_arg(expression.name, line);
            }
            case ST_ExpressionKind.Constant: {
                const t = this.translators.Type.translateType(expression.type);
                return constant(expression.hash, t, line);
            }
            case ST_ExpressionKind.Transfer: {
                const params = this.translateExpression(expression.param);
                const amount = this.translateExpression(expression.amount);
                const contract = this.translateExpression(expression.contract);
                return addOperation(transfer(params, amount, contract, line), line);
            }
            case ST_ExpressionKind.SetDelegate: {
                const baker = this.translateExpression(expression.baker);
                return addOperation(setDelegate(baker, line), line);
            }
            case ST_ExpressionKind.CreateContractOperation: {
                // Create contract (create_contract <code> <storage> <baker> <amount> <line>)
                const storage = `(storage ${this.translateExpression(expression.storage)})`;
                const amount = `(amount ${this.translateExpression(expression.amount)})`;
                const baker = `(baker ${
                    !!expression.baker
                        ? this.translateExpression(
                              ExpressionBuilder.some(expression.baker, TypeBuilder.keyHash(), expression.line),
                          )
                        : 'None'
                })`;
                return createContractOperation(`(contract ${expression.contract})`, storage, amount, baker, line);
            }
            case ST_ExpressionKind.CreateContract: {
                // Build "CREATE_CONTRACT" operation
                const contract = this.translateExpression({
                    ...expression,
                    kind: ST_ExpressionKind.CreateContractOperation,
                });
                // Define local variable
                const local = defineLocal(expression.id, contract, line);
                // Get operation from local variable
                const accessOperation = attr(getLocal(expression.id, line), 'operation', line);
                // Add operation
                return `${local} ${addOperation(accessOperation, line)}`;
            }
            case ST_ExpressionKind.CreateContractResult: {
                return attr(getLocal(expression.id, line), 'address', line);
            }
            case ST_ExpressionKind.ImplicitAccount: {
                const keyHash = this.translateExpression(expression.keyHash);
                return implicitAccount(keyHash, line);
            }
            case ST_ExpressionKind.HashKey: {
                const key = this.translateExpression(expression.key);
                return hashKey(key, line);
            }
            case ST_ExpressionKind.OpenChest: {
                const chestKey = this.translateExpression(expression.chestKey);
                const chest = this.translateExpression(expression.chest);
                const time = this.translateExpression(expression.time);
                return open_chest(chestKey, chest, time, line);
            }
            case ST_ExpressionKind.ToAddress: {
                const contract = this.translateExpression(expression.contract);
                return toAddress(contract, line);
            }
            case ST_ExpressionKind.LambdaExpression: {
                const id = expression.lambda.id;
                const vars = Object.keys(expression.lambda.type.inputTypes);
                if (vars.length < 2) {
                    const params = vars?.[0] || '';
                    const withOperations = capitalizeBoolean(expression.lambda.withOperations);
                    const withStorage = expression.lambda.withStorage || 'None';
                    const statements = this.translators.Statement.translateStatements(expression.lambda.statements);
                    return lambda(id, withStorage, withOperations, params, statements, line);
                } else {
                    return ErrorUtils.failWithInfo({
                        line: expression.line,
                        msg: `Lambda must take a single parameter.`,
                    });
                }
            }
            case ST_ExpressionKind.SelfEntryPoint: {
                return self(expression.epName || '', line);
            }
            case ST_ExpressionKind.Contract: {
                const epName = expression.epName || '';
                const address = this.translateExpression(expression.address);
                const paramType = this.translators.Type.translateType(expression.paramType);
                return contract(paramType, epName, address, line);
            }
            case ST_ExpressionKind.VotingPower: {
                const keyHash = this.translateExpression(expression.keyHash);
                return votingPower(keyHash, line);
            }
            case ST_ExpressionKind.InlineFunction:
            case ST_ExpressionKind.ClassAccess: {
                return ErrorUtils.failWith(`${expression.kind} Not expected here.`);
            }
            // + Test Scenario
            case ST_ExpressionKind.ContractStorageAccess: {
                const contractLine = LineUtils.getLineNumber(expression.contract.line);
                const id = staticId(expression.contract.id, contractLine);
                return contractData(id, line);
            }
            case ST_ExpressionKind.ContractBalanceAccess: {
                const contractLine = LineUtils.getLineNumber(expression.contract.line);
                const id = staticId(expression.contract.id, contractLine);
                return contractBalance(id, line);
            }
            case ST_ExpressionKind.ContractAddressAccess: {
                const contractLine = LineUtils.getLineNumber(expression.contract.line);
                const id = staticId(expression.contract.id, contractLine);
                return contractAddress(id, line);
            }
            case ST_ExpressionKind.ContractBakerAccess: {
                const contractLine = LineUtils.getLineNumber(expression.contract.line);
                const id = staticId(expression.contract.id, contractLine);
                return contractBaker(id, line);
            }
            case ST_ExpressionKind.ContractTypedAccess: {
                const contractLine = LineUtils.getLineNumber(expression.contract.line);
                const id = staticId(expression.contract.id, contractLine);
                return contractTyped(id, expression.epName, line);
            }
            case ST_ExpressionKind.ScenarioTestAccount: {
                return `seed:${expression.seed}`;
            }
            case ST_ExpressionKind.ScenarioTestAccountAccess: {
                const testAccount = accountOfSeed(expression.account.seed, line);
                switch (expression.prop) {
                    case '':
                        return testAccount;
                    case 'address':
                        return resolve(attr(testAccount, 'address', line), line);
                    case 'publicKey':
                        return resolve(attr(testAccount, 'public_key', line), line);
                    case 'publicKeyHash':
                        return resolve(attr(testAccount, 'public_key_hash', line), line);
                    case 'secretKey':
                        return resolve(attr(testAccount, 'secret_key', line), line);
                }

                return ErrorUtils.failWithInfo({
                    msg: `Property (${expression.prop}) doesn't exist in test accounts.`,
                    line: expression.line,
                });
            }
            case ST_ExpressionKind.ScenarioMakeSignature: {
                const privateKey = this.translateExpression(expression.privateKey);
                const content = this.translateExpression(expression.content);
                return makeSignature(privateKey, content, line);
            }
            case ST_ExpressionKind.ScenarioVariable: {
                return scenarioVar(expression.id, line);
            }
            case ST_ExpressionKind.ScenarioConstantVariable: {
                return scenarioConstantVar(expression.id, line);
            }
            case ST_ExpressionKind.IsFailing: {
                const expr = this.translateExpression(expression.expression);
                return isFailing(expr, line);
            }
            case ST_ExpressionKind.CatchException: {
                const expr = this.translateExpression(expression.expression);
                const type = this.translators.Type.translateType(expression.type);
                return catchException(expr, type, line);
            }
            case ST_ExpressionKind.ContractOrigination:
            case ST_ExpressionKind.NativeMethodExpr:
            case ST_ExpressionKind.NativeModuleAccessExpr:
            case ST_ExpressionKind.NativePropAccessExpr:
            case ST_ExpressionKind.EntryPointCall:
            case ST_ExpressionKind.ScenarioHtml:
            case ST_ExpressionKind.ScenarioShow:
            case ST_ExpressionKind.ScenarioPrepareConstantValue:
            case ST_ExpressionKind.ScenarioVerify:
                return `@TODO These expressions are translated differently in an earlier step. (${expression.kind})`;
            // - Test Scenario
        }
    };

    translateObjectLiteralExpression = (
        expression: Extract<ST_Expression, { kind: ST_ExpressionKind.ObjectLiteralExpression }>,
        type: ST_TypeDef,
    ): string => {
        switch (type.type) {
            case FrontendType.TRecord:
                return record(
                    Object.entries(expression.properties)
                        .sort(([name1], [name2]) => (name1 > name2 ? 1 : -1))
                        .map(([name, v]) => {
                            let expr = '';
                            switch (v.kind) {
                                case ST_ExpressionKind.ArrayLiteralExpression:
                                    expr = this.translateArrayLiteralExpression(v, type.properties[name]);
                                    break;
                                case ST_ExpressionKind.ObjectLiteralExpression:
                                    expr = this.translateObjectLiteralExpression(v, type.properties[name]);
                                    break;
                                default:
                                    expr = this.translateExpression(v, type.properties[name]);
                            }
                            return `(${name} ${expr})`;
                        }),
                    LineUtils.getLineNumber(expression.line),
                );
            case FrontendType.TVariant:
                const kindExpr = expression.properties['kind'];
                if (
                    kindExpr.kind === ST_ExpressionKind.LiteralExpr &&
                    kindExpr.literal.kind === ST_LiteralKind.String
                ) {
                    const name = kindExpr.literal.value;
                    const expr = this.translateExpression(expression.properties['value']);
                    return variant(name, expr, LineUtils.getLineNumber(expression.line));
                }
        }
        return ErrorUtils.failWithInfo({
            msg: `Cannot resolve value ${PrinterUtils.type.toString(type)}`,
            line: expression.line,
        });
    };

    translateArrayLiteralExpression = (
        expression: Extract<ST_Expression, { kind: ST_ExpressionKind.ArrayLiteralExpression }>,
        type: ST_TypeDef,
    ): string => {
        const line = LineUtils.getLineNumber(expression.line);
        switch (type.type) {
            case FrontendType.TList: {
                const elements = expression.elements.map((el) => this.translateExpression(el)).join(' ');
                return list(elements, line);
            }
            case FrontendType.TSet: {
                const elements = expression.elements.map((el) => this.translateExpression(el)).join(' ');
                return set(elements, line);
            }
            case FrontendType.TTuple: {
                const elements = expression.elements.map((el) => this.translateExpression(el)).join(' ');
                return tuple(elements, line);
            }
            case FrontendType.TBig_map: {
                return big_map(this.translateMapEntry(expression.elements, type), line);
            }
            case FrontendType.TMap: {
                return map(this.translateMapEntry(expression.elements, type), line);
            }
        }
        return ErrorUtils.failWithInfo({
            msg: `Cannot resolve value ${PrinterUtils.type.toString(type)}`,
            line: expression.line,
        });
    };

    translateMapEntry = (
        elements: ST_Expression[],
        type: Extract<ST_TypeDef, { type: FrontendType.TBig_map | FrontendType.TMap }>,
    ): string => {
        return elements
            .map((el) => {
                if (el.kind === ST_ExpressionKind.ArrayLiteralExpression) {
                    const key = this.translateExpression(el.elements[0], type.keyType);
                    const value = this.translateExpression(el.elements[1], type.valueType);
                    return `(${key} ${value})`;
                }
                return ErrorUtils.failWithInfo({
                    msg: `Maps must have arrays as items.`,
                    line: el.line,
                });
            })
            .join(' ');
    };
}
