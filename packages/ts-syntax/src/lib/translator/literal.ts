import { ST_LiteralKind } from '../../enums/literal';
import { BackendType, FrontendType } from '../../enums/type';
import ErrorUtils from '../utils/Error';
import LineUtils from '../utils/Line';
import Type from '../../enums/type';
import type { FileLineInfo } from '../../@types/common';
import type { ST_Literal } from '../../@types/literal';
import type { ST_TypeDef } from '../../@types/type';

const literal = (value: string, line: FileLineInfo) => `(${LineUtils.getLineNumber(line)} literal ${value})`;

const bool = (value: Extract<ST_Literal, { kind: ST_LiteralKind.Boolean }>) =>
    literal(`(bool ${value.value ? 'True' : 'False'})`, value.line);

const numeric = (type: BackendType, value: Extract<ST_Literal, { kind: ST_LiteralKind.Numeric }>) =>
    literal(`(${type} ${value.value})`, value.line);

const string = (type: BackendType, value: Extract<ST_Literal, { kind: ST_LiteralKind.String }>) =>
    literal(`(${type} "${value.value}")`, value.line);

const chain_id_cst = (value: Extract<ST_Literal, { kind: ST_LiteralKind.String }>) =>
    literal(`(chain_id_cst "${value.value}")`, value.line);

const contract = (value: Extract<ST_Literal, { kind: ST_LiteralKind.String }>) =>
    literal(`(address "${value.value}")`, value.line);

const translateLiteral = (value: ST_Literal, type: ST_TypeDef): string => {
    if (Type.backendOfFrontend[type.type]) {
        switch (value.kind) {
            case ST_LiteralKind.Numeric:
                return numeric(Type.backendOfFrontend[type.type], value);
            case ST_LiteralKind.String:
                if (type.type === FrontendType.TChain_id) {
                    return chain_id_cst(value);
                }
                if (type.type === FrontendType.TContract) {
                    return contract(value);
                }
                return string(Type.backendOfFrontend[type.type], value);
            case ST_LiteralKind.Boolean:
                return bool(value);
            case ST_LiteralKind.Unit:
                return literal(`(${Type.backendOfFrontend[type.type]})`, value.line);
        }
    }
    return ErrorUtils.failWithInfo({
        msg: `Unexpected literal (${ST_LiteralKind[value.kind]}) of type (${FrontendType[type.type]}).`,
        line: value.line,
    });
};

const LiteralTranslator = {
    translateLiteral,
};

export default LiteralTranslator;
