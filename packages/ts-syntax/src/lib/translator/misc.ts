export const staticId = (id: number, line: string): string => `(${line} static_id ${id})`;

export const capitalizeBoolean = (bool: boolean): string => (bool ? 'True' : 'False');
