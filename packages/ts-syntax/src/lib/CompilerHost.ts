import {
    createSourceFile as tsCreateSourceFile,
    Extension,
    getDefaultLibFileName,
    isImportDeclaration,
    isStringLiteral,
    ResolvedModule,
    ResolvedModuleFull,
    ScriptKind,
    ScriptTarget,
} from 'typescript';
import type { CompilerHost, CompilerOptions, SourceFile } from 'typescript';
import type { FileInfo } from '../@types/common';
import { ST_FileExtension } from '../enums/import';
import ErrorUtils from './utils/Error';
import LineUtils from './utils/Line';
import FileUtils from './utils/file/FileUtils';

/**
 * @description Load source code from imported files (The compiler host does not support async calls)
 * @param param FileInfo
 * @returns source files
 */
export const fetchImportedFiles = async ({ name, code, baseDir }: FileInfo): Promise<Record<string, string>> => {
    const sourceFile = STCompilerHost.createSourceFile(name, code);
    // Load imported imported files
    const sourceFiles = sourceFile.statements.reduce<Promise<Record<string, string>>>(
        async (prev, statement) => {
            if (isImportDeclaration(statement)) {
                try {
                    if (isStringLiteral(statement.moduleSpecifier)) {
                        let filePath: string = statement.moduleSpecifier.text;

                        if (!filePath.endsWith(`.${ST_FileExtension.TS}`)) {
                            filePath = filePath.concat(`.${ST_FileExtension.TS}`);
                        }

                        // If window is not undefined, it means we are running on a browser
                        const inBrowser = typeof window !== 'undefined';
                        const isURL = filePath.startsWith('http');

                        if (!isURL && inBrowser) {
                            return ErrorUtils.failWithInfo({
                                msg: `Cannot import local files from the browser, you must use an http URL.`,
                                line: LineUtils.getLineAndCharacter(sourceFile, statement),
                                text: statement.getText(sourceFile),
                                fileName: sourceFile.fileName,
                            });
                        }

                        const sourceFiles = await fetchImportedFiles({
                            name: statement.moduleSpecifier.text,
                            code: await FileUtils.getSourceFromFile(filePath, isURL, baseDir),
                            baseDir: baseDir,
                        });
                        return {
                            ...(await prev),
                            ...sourceFiles,
                        };
                    }
                } catch (e: any) {
                    return ErrorUtils.failWithInfo({
                        msg: e?.message,
                        line: LineUtils.getLineAndCharacter(sourceFile, statement),
                        text: statement.getText(sourceFile),
                        fileName: sourceFile.fileName,
                    });
                }
            }
            return prev;
        },
        Promise.resolve({
            [name]: code,
        }),
    );

    return sourceFiles;
};

export class STCompilerHost implements CompilerHost {
    private sourceFiles: {
        [name: string]: SourceFile;
    } = {};

    static createSourceFile = (name: string, code: string) =>
        tsCreateSourceFile(name, code, ScriptTarget.ES2020, true, ScriptKind.TS);

    constructor(files: FileInfo[]) {
        // Instantiate source files
        files.forEach(({ name, code }) => this.addSourceFile(name, code));
    }

    addSourceFile = (name: string, code: string) => {
        this.sourceFiles[name] = STCompilerHost.createSourceFile(name, code);
    };

    getSourceFile = (fileName: string): SourceFile => {
        return this.sourceFiles[fileName] || this.sourceFiles[`./${fileName}`];
    };

    getDefaultLibFileName = (defaultLibOptions: CompilerOptions): string =>
        '/' + getDefaultLibFileName(defaultLibOptions);

    writeFile = (): void => {
        /* do nothing */
    };

    getCurrentDirectory = (): string => '/';

    getDirectories = (_: string): string[] => [];

    fileExists = (fileName: string): boolean => !!this.getSourceFile(fileName);

    readFile = (fileName: string) => this.getSourceFile(fileName)?.getFullText();

    getCanonicalFileName = (fileName: string): string => fileName;

    useCaseSensitiveFileNames = (): boolean => true;

    getNewLine = (): string => '\n';

    getEnvironmentVariable = (): string => '';

    resolveModuleNames = (moduleNames: string[], _containingFile: string): ResolvedModule[] => {
        const resolvedModules: ResolvedModuleFull[] = [];
        for (const moduleName of moduleNames) {
            resolvedModules.push({ resolvedFileName: moduleName, extension: Extension.Ts });
        }
        return resolvedModules;
    };
}

export default CompilerHost;
