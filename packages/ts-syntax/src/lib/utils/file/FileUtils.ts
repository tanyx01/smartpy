import axios from 'axios';
import logger from '../../../services/Logger';

const getSourceFromFile = async (filePath: string, isURL: boolean, baseDir = ''): Promise<string> => {
    let sourceCode = '';
    try {
        if (isURL) {
            // Get URL content
            sourceCode = await fetchFromURL(filePath);
        } else {
            baseDir = !baseDir || baseDir.match(/^.*\/$/) ? baseDir : baseDir + '/';
            sourceCode = await fetchFromDisk(`${baseDir}${filePath}`);
        }
    } catch (e) {
        logger.error(`Could not read file at location ${filePath}.`, e);
    }
    return sourceCode;
};

let fetchFromURL = async (filePath: string): Promise<string> => (await axios.get(filePath)).data;

let fetchFromDisk = async (filePath: string): Promise<string> => {
    const fs = await import('fs');
    return fs.readFileSync(filePath, { encoding: 'utf-8', flag: 'r' });
};

export function setFetchFromURL(newMethod: (filePath: string) => Promise<string>): void {
    fetchFromURL = newMethod;
}

export function setFetchFromDisk(newMethod: (filePath: string) => Promise<string>): void {
    fetchFromDisk = newMethod;
}

const FileUtils = {
    getSourceFromFile,
};

export default FileUtils;
