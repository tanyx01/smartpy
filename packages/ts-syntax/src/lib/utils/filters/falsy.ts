/**
 * @description Returns true if value is falsy
 * @param {unknown} o a value of any type
 * @returns {boolean} true if "o" is falsy, and true otherwise
 */
export const isFalsy = (o: unknown): o is undefined => !o;

/**
 * @description Returns true if value is not falsy
 * @param {unknown} o a value of any type
 * @returns {boolean} true if "o" is not falsy, and false otherwise
 */
export const nonFalsy = <T>(o: T): o is NonNullable<T> => !!o;
