import type { ST_TypeDef } from '../../../@types/type';
import { FrontendType } from '../../../enums/type';

export const hasProperties = (
    typeDef: ST_TypeDef,
): typeDef is Extract<ST_TypeDef, { type: FrontendType.TRecord | FrontendType.TVariant }> =>
    [FrontendType.TRecord, FrontendType.TVariant].includes(typeDef.type);

export const isTuple = (
    typeDef: ST_TypeDef,
): typeDef is Extract<ST_TypeDef, { type: FrontendType.TTuple | FrontendType.TList | FrontendType.TSet }> =>
    [FrontendType.TTuple, FrontendType.TList, FrontendType.TSet].includes(typeDef.type);

export const hasOneArg = (typeDef: ST_TypeDef): typeDef is Extract<ST_TypeDef, { type: FrontendType.TOption }> =>
    [FrontendType.TOption].includes(typeDef.type);

export const isMap = (
    typeDef: ST_TypeDef,
): typeDef is Extract<ST_TypeDef, { type: FrontendType.TMap | FrontendType.TBig_map }> =>
    [FrontendType.TMap, FrontendType.TBig_map].includes(typeDef.type);

export const isUnknown = (t: ST_TypeDef): boolean => t.type === FrontendType.TUnknown;

export const isFunction = (
    t: ST_TypeDef,
): t is Extract<ST_TypeDef, { type: FrontendType.TFunction | FrontendType.TLambda }> =>
    t.type === FrontendType.TFunction || t.type === FrontendType.TLambda;

const type = {
    hasProperties,
    hasOneArg,
    isTuple,
    isMap,
    isUnknown,
    isFunction,
};

export default type;
