import type { FileLineInfo } from '../../../@types/common';
import type { ST_Expression } from '../../../@types/expression';
import { ST_BinaryToken, ST_ExpressionKind } from '../../../enums/expression';
import { pack, binaryExpression } from './expression';

const verifyEqual = (
    leftExpr: ST_Expression,
    rightExpr: ST_Expression,
    line: FileLineInfo,
): Extract<ST_Expression, { kind: ST_ExpressionKind.ScenarioVerify }> => ({
    kind: ST_ExpressionKind.ScenarioVerify,
    condition: binaryExpression(pack(leftExpr), pack(rightExpr), ST_BinaryToken.EqualsEquals, line),
    line,
});

const ScenarioBuilder = {
    verifyEqual,
};

export default ScenarioBuilder;
