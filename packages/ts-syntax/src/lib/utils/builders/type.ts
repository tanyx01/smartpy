import type { FileLineInfo } from '../../../@types/common';
import type { ST_Layout, ST_TypeDef, ST_TypeDefs } from '../../../@types/type';
import { FrontendType, Layout } from '../../../enums/type';

export const mutez = (): ST_TypeDef => ({
    type: FrontendType.TMutez,
});

export const address = (): ST_TypeDef => ({
    type: FrontendType.TAddress,
});

export const bool = (): ST_TypeDef => ({
    type: FrontendType.TBool,
});

export const string = (): ST_TypeDef => ({
    type: FrontendType.TString,
});

export const bytes = (): ST_TypeDef => ({
    type: FrontendType.TBytes,
});

export const timestamp = (): ST_TypeDef => ({
    type: FrontendType.TTimestamp,
});

export const chainId = (): ST_TypeDef => ({
    type: FrontendType.TChain_id,
});

export const nat = (): ST_TypeDef => ({
    type: FrontendType.TNat,
});

export const int = (): ST_TypeDef => ({
    type: FrontendType.TInt,
});

export const key = (): ST_TypeDef => ({
    type: FrontendType.TKey,
});

export const keyHash = (): ST_TypeDef => ({
    type: FrontendType.TKey_hash,
});

export const signature = (): ST_TypeDef => ({
    type: FrontendType.TSignature,
});

export const bls12_381_fr = (): ST_TypeDef => ({
    type: FrontendType.TBls12_381_fr,
});

export const bls12_381_g1 = (): ST_TypeDef => ({
    type: FrontendType.TBls12_381_g1,
});

export const bls12_381_g2 = (): ST_TypeDef => ({
    type: FrontendType.TBls12_381_g2,
});

export const chest = (): ST_TypeDef => ({
    type: FrontendType.TChest,
});

export const chest_key = (): ST_TypeDef => ({
    type: FrontendType.TChest_key,
});

export const map = (keyType: ST_TypeDef, valueType: ST_TypeDef): ST_TypeDef => ({
    type: FrontendType.TMap,
    keyType,
    valueType,
});

export const list = (innerTypes: ST_TypeDef[]): ST_TypeDef => ({
    type: FrontendType.TList,
    innerTypes,
});

export const tuple = (innerTypes: ST_TypeDef[], line?: FileLineInfo): ST_TypeDef => ({
    type: FrontendType.TTuple,
    innerTypes,
    line,
});

export const contract = (inputType: ST_TypeDef): ST_TypeDef => ({
    type: FrontendType.TContract,
    inputType,
});

export const unit = (): ST_TypeDef => ({
    type: FrontendType.TUnit,
});

export const never = (): ST_TypeDef => ({
    type: FrontendType.TNever,
});

export const unknown = (line?: FileLineInfo): ST_TypeDef => ({
    type: FrontendType.TUnknown,
    line,
});

export const record = (info: {
    properties: ST_TypeDefs;
    layout?: ST_Layout | Layout;
    line?: FileLineInfo;
}): ST_TypeDef => ({
    type: FrontendType.TRecord,
    ...info,
});

export const option = (innerType: ST_TypeDef, none?: boolean): ST_TypeDef => ({
    type: FrontendType.TOption,
    none: none,
    innerType,
});

export const operation = (): ST_TypeDef => ({
    type: FrontendType.TOperation,
});

export const variant = (properties: ST_TypeDefs, layout: string[] | Layout): ST_TypeDef => ({
    type: FrontendType.TVariant,
    properties,
    layout,
});

const TypeBuilder = {
    mutez,
    address,
    bool,
    string,
    timestamp,
    chainId,
    nat,
    int,
    key,
    keyHash,
    signature,
    bls12_381_fr,
    bls12_381_g1,
    bls12_381_g2,
    chest,
    chest_key,
    list,
    tuple,
    unit,
    contract,
    never,
    bytes,
    record,
    map,
    unknown,
    option,
    operation,
    variant,
};

export default TypeBuilder;
