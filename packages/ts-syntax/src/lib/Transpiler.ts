import {
    isArrowFunction,
    isCallExpression,
    isClassDeclaration,
    isEnumDeclaration,
    isExpressionStatement,
    isFunctionDeclaration,
    isImportDeclaration,
    isInterfaceDeclaration,
    isStringLiteral,
    isTypeAliasDeclaration,
    isVariableDeclaration,
    isVariableStatement,
    SourceFile,
    Statement,
    Node,
    SyntaxKind,
    isPropertyAccessExpression,
    isQualifiedName,
    NodeFlags,
    isNamespaceImport,
    ImportDeclaration,
    getPreEmitDiagnostics,
    getLineAndCharacterOfPosition,
    flattenDiagnosticMessageText,
    Program,
} from 'typescript';

import { createProgram } from 'typescript';

// Interpreters
import TypeInterpreter from './interpreter/type';
import LiteralInterpreter from './interpreter/literal';
import DecoratorInterpreter from './interpreter/decorator';
import DeclarationInterpreter from './interpreter/declaration';
import StatementInterpreter from './interpreter/statement';
import MetadataInterpreter from './interpreter/metadata';
import InliningInterpreter from './interpreter/inlining';
import ScenarioInterpreter from './interpreter/scenario';

// Translator
import ExpressionTranslator from './translator/expression';
import StatementTranslator from './translator/statement';
import TypeTranslator from './translator/type';

// Utils
import IdentifierUtils from './utils/Identifier';
import FileUtils from './utils/file/FileUtils';
import ModifiersUtils from './utils/Modifiers';
import ErrorUtils from './utils/Error';

// Types
import type { FileInfo } from '../@types/common';
import type { ST_Scenario } from '../@types/scenario';

import { STCompilerHost } from './CompilerHost';
import OutputContext from './OutputContext';
import { CompilerOptions } from '../constants';

import typings from '../ide.generated';
import { ST_ScopeKind } from '../enums/scope';
import { ST_FileExtension } from '../enums/import';
import Logger from '../services/Logger';

enum InterpreterKind {
    Declaration = 'Declaration',
    Decorator = 'Decorator',
    Literal = 'Literal',
    Statement = 'Statement',
    Type = 'Type',
    Metadata = 'Metadata',
    Inlining = 'Inlining',
    Scenario = 'Scenario',
}

enum TranslatorKind {
    Expression = 'Expression',
    Statement = 'Statement',
    Type = 'Type',
}

export type Interpreters = Readonly<{
    [InterpreterKind.Declaration]: DeclarationInterpreter;
    [InterpreterKind.Decorator]: DecoratorInterpreter;
    [InterpreterKind.Literal]: LiteralInterpreter;
    [InterpreterKind.Statement]: StatementInterpreter;
    [InterpreterKind.Type]: TypeInterpreter;
    [InterpreterKind.Metadata]: MetadataInterpreter;
    [InterpreterKind.Inlining]: InliningInterpreter;
    [InterpreterKind.Scenario]: ScenarioInterpreter;
}>;

export type Translators = Readonly<{
    [TranslatorKind.Expression]: ExpressionTranslator;
    [TranslatorKind.Statement]: StatementTranslator;
    [TranslatorKind.Type]: TypeTranslator;
}>;

export class Transpiler {
    program: Program;
    compilerHost: STCompilerHost;
    sourceFile: SourceFile;
    output: OutputContext;

    interpreters: Interpreters;
    translators: Translators;
    constructor(public source: FileInfo, sourceFiles: Record<string, string> = {}) {
        this.compilerHost = new STCompilerHost([
            {
                name: source.name,
                code: source.code,
            },
            {
                name: 'global.ts',
                code: typings,
            },
            {
                name: '/lib.es2020.full.d.ts',
                code: `
                interface SymbolConstructor {
                    /**
                     * A regular expression method that matches the regular expression against a string. Called
                     * by the String.prototype.matchAll method.
                     */
                    readonly matchAll: unique symbol;
                }
                interface SymbolConstructor {
                    /**
                     * A reference to the prototype.
                     */
                    readonly prototype: Symbol;

                    /**
                     * Returns a new unique Symbol value.
                     * @param  description Description of the new Symbol object.
                     */
                    (description?: string | number): symbol;

                    /**
                     * Returns a Symbol object from the global symbol registry matching the given key if found.
                     * Otherwise, returns a new symbol with this key.
                     * @param key key to search for.
                     */
                    for(key: string): symbol;

                    /**
                     * Returns a key from the global symbol registry matching the given Symbol if found.
                     * Otherwise, returns a undefined.
                     * @param sym Symbol to find the key for.
                     */
                    keyFor(sym: symbol): string | undefined;
                }
                declare var Symbol: SymbolConstructor;
                interface Symbol {
                    /**
                     * Expose the [[Description]] internal slot of a symbol directly.
                     */
                    readonly description: string | undefined;
                }
                `,
            },
        ]);
        Object.entries(sourceFiles).forEach(([name, code]) => this.compilerHost.addSourceFile(name, code));

        this.program = createProgram({
            rootNames: [source.name, 'global.ts'],
            options: CompilerOptions,
            host: this.compilerHost,
        });

        const sourceFile = this.program.getSourceFile(source.name);
        if (!sourceFile) {
            throw Error(`Could not find file (${source.name}).`);
        }

        this.sourceFile = sourceFile;

        this.interpreters = Object.freeze({
            [InterpreterKind.Declaration]: new DeclarationInterpreter(this),
            [InterpreterKind.Decorator]: new DecoratorInterpreter(this),
            [InterpreterKind.Literal]: new LiteralInterpreter(this),
            [InterpreterKind.Statement]: new StatementInterpreter(this),
            [InterpreterKind.Type]: new TypeInterpreter(this),
            [InterpreterKind.Metadata]: new MetadataInterpreter(this),
            [InterpreterKind.Inlining]: new InliningInterpreter(this),
            [InterpreterKind.Scenario]: new ScenarioInterpreter(this),
        });

        this.translators = Object.freeze({
            [TranslatorKind.Expression]: new ExpressionTranslator(this),
            [TranslatorKind.Statement]: new StatementTranslator(this),
            [TranslatorKind.Type]: new TypeTranslator(this),
        });

        this.output = new OutputContext(sourceFile, this.translators);
    }

    generateDiagnostics = () => {
        const emitResult = this.program.emit();
        const allDiagnostics = getPreEmitDiagnostics(this.program).concat(emitResult.diagnostics);
        allDiagnostics.forEach((diagnostic) => {
            // Ignore errors related to imports
            if ([2792, 2691].includes(diagnostic.code)) {
                Logger.debug(diagnostic.messageText);
                return;
            }
            if (diagnostic.file && diagnostic.start) {
                const { line, character } = getLineAndCharacterOfPosition(diagnostic.file, diagnostic.start);
                const message = flattenDiagnosticMessageText(diagnostic.messageText, '\n');
                ErrorUtils.failWithInfo({
                    line: {
                        line: line + 1,
                        character: character + 1,
                        fileName: diagnostic.file.fileName,
                    },
                    msg: `${diagnostic.file.fileName} (${line + 1},${character + 1}): ${message}`,
                    fileName: diagnostic.file.fileName,
                    text: diagnostic.source,
                });
            } else {
                ErrorUtils.failWith(flattenDiagnosticMessageText(diagnostic.messageText, '\n'));
            }
        });
    };

    transpile = async (): Promise<ST_Scenario[]> => {
        // 1. Visit import statements
        let children = await this.sourceFile.statements.reduce<Promise<Statement[]>>(async (prev, statement) => {
            if (isImportDeclaration(statement)) {
                try {
                    await this.visitImportDeclaration(statement);
                } catch (e: any) {
                    this.output.emitError(statement, e?.message);
                }
                return await prev;
            }

            return [...(await prev), statement];
        }, Promise.resolve([]));

        // Initialize the ROOT scope
        this.output.enterScope(ST_ScopeKind.Global);

        // 2. Visit type definitions
        children = children.filter((statement) => {
            if (isInterfaceDeclaration(statement)) {
                this.interpreters.Type.visitInterfaceDeclaration(statement);
                return false;
            }
            if (isTypeAliasDeclaration(statement)) {
                this.interpreters.Type.visitTypeAliasDeclaration(statement);
                return false;
            }
            return true;
        });
        // 3. Visit variable statements
        children = children.filter((statement) => {
            if (isEnumDeclaration(statement)) {
                this.output.emitVariableDeclaration(this.interpreters.Statement.extractEnumDeclaration(statement));
                return false;
            }
            if (isVariableStatement(statement)) {
                const modifiers = ModifiersUtils.extractModifiers(statement);
                statement.declarationList.declarations.forEach((st) => {
                    if (isVariableDeclaration(st) && st.initializer && isArrowFunction(st.initializer)) {
                        this.interpreters.Declaration.visitFunctionDeclarationNode(st);
                    } else {
                        this.output.emitVariableDeclaration(
                            this.interpreters.Statement.extractVariableInitializer(st, NodeFlags.Const, modifiers),
                        );
                    }
                });
                return false;
            }
            return true;
        });
        // 4. Visit function declarations
        children = children.filter((statement) => {
            if (isFunctionDeclaration(statement)) {
                this.interpreters.Declaration.visitFunctionDeclarationNode(statement);
                return false;
            }
            return true;
        });
        // 5. Visit class declarations
        children = children.filter((statement) => {
            if (isClassDeclaration(statement)) {
                this.interpreters.Declaration.visitClassDeclarationNode(statement);
                return false;
            }
            return true;
        });

        // 6. Visit expression statements
        children = children.filter((statement) => {
            if (isExpressionStatement(statement) && isCallExpression(statement.expression)) {
                this.interpreters.Scenario.resolveDevExpression(statement.expression);
                return false;
            }
            return true;
        });

        if (children.length > 0) {
            // @TODO - Remove when complete
            Logger.error('Remaining ROOT nodes', children);
        }

        // Terminate ROOT scope
        this.output.exitScope();

        return this.output.getResult();
    };

    /**
     * @description Extract node name.
     * @param {Node} node
     * @returns {string} node name
     */
    extractName = (node: Node): string => {
        const name = IdentifierUtils.extractName(node);
        if (name) {
            return name;
        }
        return this.output.emitError(node, `Unexpected unnamed node (${SyntaxKind[node.kind]}).`);
    };

    /**
     * @description Extract expression namespace.
     * @param {Node} node
     * @returns {string[]} namespace path
     */
    extractNamespace = (expr: Node): string[] => {
        if (expr.kind === SyntaxKind.ThisKeyword) {
            return ['this'];
        }
        if (isQualifiedName(expr)) {
            const name = this.extractName(expr.left);
            return [name, ...this.extractNamespace(expr.right)];
        }
        const name = this.extractName(expr);

        if (isPropertyAccessExpression(expr)) {
            return [...this.extractNamespace(expr.expression), name];
        }

        return [name];
    };

    /**
     * @description Get Imported module name
     * @param {ImportDeclaration} node
     * @returns {string} Module name
     */
    getImportName = (node: ImportDeclaration): string => {
        const namedBindings = node.importClause?.namedBindings;
        if (namedBindings && isNamespaceImport(namedBindings)) {
            // Get imported module
            const moduleName = this.extractName(namedBindings.name);
            if (moduleName) {
                return moduleName;
            }
        }

        return this.output.emitError(node, `You must specify an import clause. (import * as <Name> from "...")`);
    };

    /**
     * @description Visit import declaration and apply transformations
     * @param {ImportDeclaration} node
     * @returns {@Promise<void>}
     */
    public async visitImportDeclaration(node: ImportDeclaration): Promise<void> {
        if (isStringLiteral(node.moduleSpecifier)) {
            let filePath: string = node.moduleSpecifier.text;

            if (!filePath.endsWith(`.${ST_FileExtension.TS}`) && !filePath.endsWith(`.${ST_FileExtension.JSON}`)) {
                filePath = filePath.concat(`.${ST_FileExtension.TS}`);
            }

            const moduleName = this.getImportName(node);

            // If window is not undefined, it means we are running on a browser
            const inBrowser = typeof window !== 'undefined';
            const isURL = filePath.startsWith('http');

            if (!isURL && inBrowser) {
                this.output.emitError(node, `Cannot import local files from the browser, you must use an http URL.`);
            }

            // Fetch source code.
            const sourceCode = await FileUtils.getSourceFromFile(filePath, isURL, this.source.baseDir);
            const [extension] = filePath.split('.').reverse();

            switch (extension) {
                case ST_FileExtension.TS:
                    // Create a new transpiler context to transpile the file being imported.
                    // After transpiling the file, it will be added to the main context as an imported module.
                    const transpiler = new Transpiler(
                        {
                            baseDir: this.source.baseDir,
                            name: filePath,
                            code: sourceCode,
                        },
                        {},
                    );
                    // Transpile TS CODE
                    await transpiler.transpile();
                    return this.output.addModule(moduleName, transpiler.output.exportedModule, node);
                case ST_FileExtension.JSON:
                // @TODO - Interpret JSON file
                default:
                    this.output.emitError(node, `File extension (${extension}) not supported.`);
            }
        }

        this.output.emitError(
            node,
            `Import statements should have the following format (import * as A from "some_url_or_path.<extension>").`,
        );
    }
}

export default Transpiler;
