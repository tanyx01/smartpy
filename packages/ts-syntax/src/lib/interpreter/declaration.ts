import {
    Node,
    ClassDeclaration,
    PropertyDeclaration,
    SyntaxKind,
    isPropertyDeclaration,
    isMethodDeclaration,
    FunctionDeclaration,
    isFunctionDeclaration,
    MethodDeclaration,
    isArrowFunction,
    isBlock,
    ArrowFunction,
    isConstructorDeclaration,
    isParameter,
    VariableDeclaration,
    isVariableDeclaration,
    NodeFlags,
} from 'typescript';

import { InterpreterBase } from './Base';
import guards from '../utils/guard';
import LineUtils from '../utils/Line';
import ModifiersUtils from '../utils/Modifiers';
import { ST_ScopeKind } from '../../enums/scope';
import { ST_StatementKind } from '../../enums/statement';
import { ST_Modifier } from '../../enums/Modifiers';
import { ST_DecoratorKind } from '../../enums/decorator';
import type { ST_Expression } from '../../@types/expression';
import type { ST_Parameter } from '../../@types/parameter';
import type { ST_VariableStatement, ST_ExpressionStatement } from '../../@types/statement';

export default class DeclarationInterpreter extends InterpreterBase {
    public visitClassDeclarationNode(node: ClassDeclaration): void {
        // Initialize class scope
        this.output.enterScope(ST_ScopeKind.Class);

        const className = this.output.emitClassDeclaration(node);
        // Set declaringClass since it is being processed
        this.output.declaringClass = className;

        // Get class decorators
        this.interpreters.Decorator.extractClassDecorators(className, node).map((decorator) =>
            this.output.emitClassDecorator(className, decorator),
        );

        // 1. Visit constructor
        let members = node.members.filter((member) => {
            if (isConstructorDeclaration(member)) {
                this.visitClassMember(member);
                return false;
            }
            return true;
        });
        // 2. Visit property declarations
        members = members.filter((member) => {
            if (
                isPropertyDeclaration(member) &&
                !this.interpreters.Decorator.hasDecorator(member, ST_DecoratorKind.Inline) &&
                !this.interpreters.Decorator.hasDecorator(member, ST_DecoratorKind.PrivateLambda)
            ) {
                this.visitClassMember(member);
                return false;
            }
            return true;
        });
        // 3. Visit @Inlined methods
        members = members.filter((member) => {
            if (isPropertyDeclaration(member) && member.initializer && isArrowFunction(member.initializer)) {
                this.visitFunctionDeclarationNode(member);
                return false;
            }
            if (
                isMethodDeclaration(member) &&
                this.interpreters.Decorator.hasDecorator(member, ST_DecoratorKind.Inline) &&
                member.body
            ) {
                this.visitFunctionDeclarationNode(member);
                return false;
            }
            return true;
        });
        // 4. Visit @PrivateLambda methods
        members = members.filter((member) => {
            if (isPropertyDeclaration(member) && member.initializer && isArrowFunction(member.initializer)) {
                this.visitFunctionDeclarationNode(member);
                return false;
            }
            if (
                isMethodDeclaration(member) &&
                this.interpreters.Decorator.hasDecorator(member, ST_DecoratorKind.PrivateLambda) &&
                member.body
            ) {
                this.visitFunctionDeclarationNode(member);
                return false;
            }
            return true;
        });
        // 5. Visit the remaining methods
        members = members.filter((member) => {
            if (isMethodDeclaration(member)) {
                this.visitFunctionDeclarationNode(member);
                return false;
            }
            return true;
        });

        if (members.length > 0) {
            console.error('Remaining class nodes', members);
        }

        // Removed declaringClass since it is already processed
        delete this.output.declaringClass;
        this.output.exitScope();
    }

    public visitFunctionDeclarationNode = (
        node: FunctionDeclaration | MethodDeclaration | PropertyDeclaration | VariableDeclaration,
    ): void => {
        this.output.enterScope(ST_ScopeKind.Method);
        const functionName = this.transpiler.extractName(node);
        const modifiers = ModifiersUtils.extractModifiers(node);
        const typeDef = this.interpreters.Type.checkType(node);

        // Get function decorators
        const decorators = this.interpreters.Decorator.extractPropertyDecorators(node);
        const isInlined = !!decorators.Inline;

        // Set declaringMethod since a new function is being processed
        this.output.declaringMethod = functionName;

        if (guards.type.isFunction(typeDef)) {
            if (
                (isVariableDeclaration(node) || isPropertyDeclaration(node)) &&
                node.initializer &&
                isArrowFunction(node.initializer)
            ) {
                // Arrow function
                this.output.emitFunctionDeclaration(functionName, typeDef, modifiers, node.initializer, decorators);
            } else if (isFunctionDeclaration(node) || isMethodDeclaration(node)) {
                this.output.emitFunctionDeclaration(functionName, typeDef, modifiers, node, decorators);
            }
        } else {
            this.output.emitError(node, 'Function declaration must have a function type.');
        }

        if (
            (isPropertyDeclaration(node) || isVariableDeclaration(node)) &&
            node.initializer &&
            isArrowFunction(node.initializer)
        ) {
            this.visitFunctionNode(node.initializer, /* inlined */ isInlined);
        } else if (isMethodDeclaration(node) || isFunctionDeclaration(node)) {
            this.visitFunctionNode(node, /* inlined */ isInlined);
        } else {
            return this.output.emitError(node, `Unexpected function declaration (${SyntaxKind[node.kind]}).`);
        }

        // Remove declaringMethod since it is already processed
        delete this.output.declaringMethod;
        this.output.exitScope();
    };

    public visitClassMember = (node: Node): void => {
        if (isPropertyDeclaration(node)) {
            const initializer = node.initializer;
            if (initializer && isArrowFunction(initializer)) {
                this.visitFunctionDeclarationNode(node);
            } else if (this.interpreters.Decorator.hasDecorator(node, ST_DecoratorKind.MetadataBuilder)) {
                const decorators = this.interpreters.Decorator.extractPropertyDecorators(node);
                this.output.emitPropertyDecorators(decorators);
            } else {
                const modifiers = ModifiersUtils.extractModifiers(node);
                const variableStatement = this.interpreters.Statement.extractVariableInitializer(
                    node,
                    NodeFlags.Const,
                    modifiers,
                );
                this.output.emitVariableDeclaration(variableStatement);
            }
        } else if (isConstructorDeclaration(node)) {
            node.parameters.forEach((param, index) => {
                if (isParameter(param) && param.modifiers && param.type) {
                    const modifiers = ModifiersUtils.extractModifiers(param);
                    const name = this.transpiler.extractName(param);
                    const type = this.interpreters.Type.extractTypeDef(param.type);
                    const expression = param.initializer
                        ? this.transpiler.interpreters.Statement.extractExpression(param.initializer, type)
                        : ({} as ST_Expression);
                    const initializer: ST_VariableStatement = {
                        kind: ST_StatementKind.VariableStatement,
                        decorators: {},
                        modifiers,
                        flags: NodeFlags.Const,
                        type,
                        name,
                        expression,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, param),
                    };
                    if (modifiers.includes(ST_Modifier.Public) || modifiers.includes(ST_Modifier.Private)) {
                        // Values with public/private modifiers are declared as properties of the class
                        this.output.emitVariableDeclaration(initializer);
                    }
                    this.output.currentClass.constructorArgs[name] = {
                        index,
                        name,
                        type,
                        initializer: initializer?.expression,
                    };
                }
            });
        } else if (isFunctionDeclaration(node) || isMethodDeclaration(node)) {
            this.visitFunctionDeclarationNode(node);
        } else {
            this.output.emitError(node, `Unexpected class member (${SyntaxKind[node.kind]})`);
        }
    };

    extractParameter = (node: Node): ST_Parameter => {
        const name = this.transpiler.extractName(node);
        const type = this.interpreters.Type.checkType(node);
        return {
            name,
            type,
        };
    };

    /**
     * @description Deference property from anther property reference
     * @param {Node} Identifier Property reference
     * @returns {Extract<ST_Expression, { kind: ST_ExpressionKind.VariableDeclaration }> | void} Property value
     */
    dereferenceProperty = (name: string): ST_VariableStatement | void => {
        if (this.output.declaringClass) {
            // Class scoped types take precedence over ROOT types
            const value = this.output.getClassProperties(this.output.declaringClass)[name];
            if (value) {
                return value;
            }
        }
        return this.output.globalProperties[name];
    };

    visitFunctionNode = (node: FunctionDeclaration | MethodDeclaration | ArrowFunction, inlined: boolean): void => {
        if (node.body && isBlock(node.body)) {
            this.interpreters.Statement.visitBlock(node.body);
        } else if (isArrowFunction(node)) {
            // There is no return statement and the whole expression is returned.
            const expr = this.interpreters.Statement.extractExpression(node.body);
            const line = LineUtils.getLineAndCharacter(this.sourceFile, node);

            // Inlined functions return the raw expression, while not inlined functions return a result expression.
            const statement: ST_ExpressionStatement = inlined
                ? {
                      kind: ST_StatementKind.Expression,
                      expression: expr,
                      line,
                  }
                : {
                      kind: ST_StatementKind.Result,
                      expression: expr,
                      line,
                  };

            this.output.emitFunctionStatement(statement);
        } else {
            this.output.emitError(node, `Cannot transpile (${SyntaxKind[node.kind]})`);
        }
    };
}
