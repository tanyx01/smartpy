import { InterpreterBase } from './Base';
import { StringOfBinaryToken, ST_ExpressionKind } from '../../enums/expression';
import ErrorUtils from '../utils/Error';
import { ST_StatementKind } from '../../enums/statement';
import { isAssignmentBinaryOperator } from '../utils/BinaryOperators';
import type { ST_Expression } from '../../@types/expression';
import type {
    ST_ForOfStatement,
    ST_IfStatement,
    ST_Statement,
    ST_SwitchStatement,
    ST_VariableStatement,
    ST_WhileStatement,
} from '../../@types/statement';
import type { FileLineInfo } from '../../@types/common';
import type { ST_FunctionProperty } from '../../@types/property';

export default class InliningInterpreter extends InterpreterBase {
    expandInlineExpression = (expression: ST_Expression, args: Record<string, ST_Expression>): ST_Expression => {
        switch (expression.kind) {
            case ST_ExpressionKind.MethodParamAccessExpr:
            case ST_ExpressionKind.LambdaParamAccessExpr:
                if (args[expression.attr]) {
                    return args[expression.attr];
                }
                return ErrorUtils.failWithInfo({
                    msg: `Inline function call misses the following argument: ${expression.attr}.`,
                    fileName: this.sourceFile.fileName,
                    line: expression.line,
                });
            case ST_ExpressionKind.ArrayLiteralExpression:
                return {
                    ...expression,
                    kind: ST_ExpressionKind.ArrayLiteralExpression,
                    elements: expression.elements.map((el) => this.expandInlineExpression(el, args)),
                };
            case ST_ExpressionKind.ObjectLiteralExpression:
                return {
                    ...expression,
                    kind: ST_ExpressionKind.ObjectLiteralExpression,
                    properties: Object.entries(expression.properties).reduce(
                        (props, [key, value]) => ({
                            ...props,
                            [key]: this.expandInlineExpression(value, args),
                        }),
                        {},
                    ),
                };
            case ST_ExpressionKind.AttrAccessExpr:
                return {
                    ...expression,
                    prev: expression.prev ? this.expandInlineExpression(expression.prev, args) : undefined,
                };
            case ST_ExpressionKind.Ediv:
            case ST_ExpressionKind.BinaryExpr:
                return {
                    ...expression,
                    left: this.expandInlineExpression(expression.left, args),
                    right: this.expandInlineExpression(expression.right, args),
                };
            case ST_ExpressionKind.MethodPropAccessExpr:
            case ST_ExpressionKind.StorageAccessExpr:
            case ST_ExpressionKind.LiteralExpr:
            case ST_ExpressionKind.NativePropAccessExpr:
            case ST_ExpressionKind.VariantAccess:
            case ST_ExpressionKind.ForIterator:
                return expression;
            case ST_ExpressionKind.PairCAR:
            case ST_ExpressionKind.PairCDR:
            case ST_ExpressionKind.AsExpression:
            case ST_ExpressionKind.ToInt:
            case ST_ExpressionKind.ToNat:
            case ST_ExpressionKind.ABS:
            case ST_ExpressionKind.Not:
            case ST_ExpressionKind.GetElements:
            case ST_ExpressionKind.Size:
                return {
                    ...expression,
                    expression: this.expandInlineExpression(expression.expression, args),
                };
            case ST_ExpressionKind.DelItem:
            case ST_ExpressionKind.Contains:
                return {
                    ...expression,
                    source: this.expandInlineExpression(expression.source, args),
                    subject: this.expandInlineExpression(expression.subject, args),
                };
            case ST_ExpressionKind.SetItem:
                return {
                    ...expression,
                    target: this.expandInlineExpression(expression.target, args),
                    value: this.expandInlineExpression(expression.value, args),
                };
            case ST_ExpressionKind.GetItem:
                return {
                    ...expression,
                    defaultValue: expression.defaultValue
                        ? this.expandInlineExpression(expression.defaultValue, args)
                        : undefined,
                    source: this.expandInlineExpression(expression.source, args),
                    subject: this.expandInlineExpression(expression.subject, args),
                };
            case ST_ExpressionKind.OpenVariant:
                return {
                    ...expression,
                    kind: ST_ExpressionKind.OpenVariant,
                    source: this.expandInlineExpression(expression.source, args),
                };
            case ST_ExpressionKind.Verify:
                return {
                    ...expression,
                    condition: this.expandInlineExpression(expression.condition, args),
                    message: expression.message ? this.expandInlineExpression(expression.message, args) : undefined,
                };
            case ST_ExpressionKind.Transfer:
                return {
                    ...expression,
                    kind: ST_ExpressionKind.Transfer,
                    amount: this.expandInlineExpression(expression.amount, args),
                    contract: this.expandInlineExpression(expression.contract, args),
                    param: this.expandInlineExpression(expression.param, args),
                };
            default:
                return ErrorUtils.failWithInfo({
                    msg: `Inline functions cannot contain (${expression.kind}) expression.`,
                    fileName: this.sourceFile.fileName,
                    line: expression.line,
                });
        }
    };

    expandInlineStatement = (statement: ST_Statement, args: Record<string, ST_Expression>): ST_Statement => {
        switch (statement.kind) {
            case ST_StatementKind.Expression:
                return {
                    ...statement,
                    expression: this.expandInlineExpression(statement.expression, args),
                };
            case ST_StatementKind.SwitchStatement:
                return {
                    ...statement,
                    expression: this.expandInlineExpression(statement.expression, args),
                    cases: Object.entries(statement.cases).reduce((acc, [kind, statements]) => {
                        return {
                            ...acc,
                            [kind]: statements.map((st: ST_Statement) => this.expandInlineStatement(st, args)),
                        };
                    }, {}),
                } as ST_SwitchStatement;
            case ST_StatementKind.IfStatement:
                return {
                    ...statement,
                    expression: this.expandInlineExpression(statement.expression, args),
                    elseStatement: statement.elseStatement.map((st) => this.expandInlineStatement(st, args)),
                    thenStatement: statement.thenStatement.map((st) => this.expandInlineStatement(st, args)),
                } as ST_IfStatement;
            case ST_StatementKind.ForOfStatement:
                return {
                    ...statement,
                    expression: this.expandInlineExpression(statement.expression, args),
                    iterator: this.expandInlineExpression(statement.iterator, args),
                    statements: statement.statements.map((st) => this.expandInlineStatement(st, args)),
                } as ST_ForOfStatement;
            case ST_StatementKind.WhileStatement:
                return {
                    ...statement,
                    condition: this.expandInlineExpression(statement.condition, args),
                    incrementor: statement.incrementor
                        ? this.expandInlineExpression(statement.incrementor, args)
                        : undefined,
                    initializer: statement.initializer
                        ? this.expandInlineStatement(statement.initializer, args)
                        : undefined,
                    statements: statement.statements.map((st) => this.expandInlineStatement(st, args)),
                } as ST_WhileStatement;
            case ST_StatementKind.VariableStatement:
                return {
                    ...statement,
                    expression: this.expandInlineExpression(statement.expression, args),
                } as ST_VariableStatement;
            default:
                return ErrorUtils.failWithInfo({
                    msg: `Inline functions cannot contain (${statement.kind}) statements.`,
                    fileName: this.sourceFile.fileName,
                    line: statement.line,
                });
        }
    };

    visitInlineCall = (args: Record<string, ST_Expression>, func: ST_FunctionProperty, line: FileLineInfo): void => {
        // Inline Expansion
        Object.values(func.statements).forEach((st): void => {
            const statement = this.expandInlineStatement(st, args);
            if (
                statement.kind === ST_StatementKind.Expression &&
                statement.expression.kind === ST_ExpressionKind.BinaryExpr &&
                !isAssignmentBinaryOperator(statement.expression.operator)
            ) {
                return ErrorUtils.failWithInfo({
                    msg: `Binary expression (${
                        StringOfBinaryToken[statement.expression.operator]
                    }) cannot be used as a statement.`,
                    fileName: this.sourceFile.fileName,
                    line,
                });
            }
            this.output.scope.emitStatement(statement);
        });
    };
}
