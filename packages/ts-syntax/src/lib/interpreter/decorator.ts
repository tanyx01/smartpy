import {
    Decorator,
    isCallExpression,
    SyntaxKind,
    isObjectLiteralExpression,
    isPropertyAssignment,
    ClassDeclaration,
    isIdentifier,
    FunctionLikeDeclaration,
    PropertyDeclaration,
    isArrayLiteralExpression,
    isStringLiteral,
    VariableDeclaration,
    Node,
    isPropertyDeclaration,
    ObjectLiteralExpression,
} from 'typescript';

import { InterpreterBase } from './Base';
import {
    ST_ContractDecoratorProp,
    ST_DecoratorKind,
    ST_EntryPointDecoratorProp,
    ST_PrivateLambdaDecoratorProp,
    ST_ViewDecoratorProp,
} from '../../enums/decorator';
import type {
    ST_Class_Decorator,
    ST_Property_Decorators,
    ST_PrivateLambda_Decorator,
    ST_Metadata_Decorator,
    ST_Contract_Decorator,
    ST_View_Decorator,
    ST_EntryPoint_Decorator,
} from '../../@types/decorator';
import type { ST_Flag } from '../../@types/output';

export default class DecoratorInterpreter extends InterpreterBase {
    /**
     * @description Extract class decorators from class declaration.
     * @param {string} className
     * @param {ClassDeclaration} node
     * @returns {STDecorator[]} Class decorators
     */
    extractClassDecorators = (className: string, node: ClassDeclaration): ST_Class_Decorator[] => {
        return (
            node.decorators?.reduce<ST_Class_Decorator[]>((state, decorator) => {
                const decoratorName = this.transpiler.extractName(decorator);

                if (decoratorName === ST_DecoratorKind.Contract) {
                    return [...state, this.extractContractInfo(className, decorator)];
                }

                return this.output.emitError(node, `Unknown decorator (@${decoratorName}).`);
            }, []) || []
        );
    };

    /**
     * @description Extract property decorators.
     * @param {string} className
     * @param {ClassDeclaration} node
     * @returns {STDecorator[]} Property decorators
     */
    extractPropertyDecorators = (
        node: FunctionLikeDeclaration | PropertyDeclaration | VariableDeclaration,
    ): ST_Property_Decorators => {
        const propName = this.transpiler.extractName(node);
        return (
            node.decorators?.reduce<ST_Property_Decorators>((state, decorator) => {
                const decoratorName = this.transpiler.extractName(decorator);

                if (decoratorName === ST_DecoratorKind.EntryPoint) {
                    return {
                        ...state,
                        [ST_DecoratorKind.EntryPoint]: this.extractEntryPointInfo(propName, decorator),
                    };
                }
                if (decoratorName === 'OffChainView') {
                    return {
                        ...state,
                        [ST_DecoratorKind.View]: {
                            ...this.extractViewInfo(propName, decorator),
                            offchainOnly: true,
                        },
                    };
                }
                if (decoratorName === 'OnChainView') {
                    return {
                        ...state,
                        [ST_DecoratorKind.View]: {
                            ...this.extractViewInfo(propName, decorator),
                            offchainOnly: false,
                        },
                    };
                }

                if (
                    decoratorName === ST_DecoratorKind.MetadataBuilder &&
                    isPropertyDeclaration(node) &&
                    node.initializer &&
                    isObjectLiteralExpression(node.initializer)
                ) {
                    return {
                        ...state,
                        [ST_DecoratorKind.MetadataBuilder]: this.extractMetadataInfo(propName, node.initializer),
                    };
                }

                if (decoratorName === ST_DecoratorKind.PrivateLambda) {
                    return {
                        ...state,
                        [ST_DecoratorKind.PrivateLambda]: this.extractPrivateLambdaInfo(propName, decorator),
                    };
                }

                if (decoratorName === ST_DecoratorKind.Inline) {
                    return {
                        ...state,
                        [ST_DecoratorKind.Inline]: { kind: ST_DecoratorKind.Inline, name: propName },
                    };
                }

                return this.output.emitError(node, `Unknown decorator (@${decoratorName}).`);
            }, {}) || {}
        );
    };

    /**
     * @description Extract information from @PrivateLambda decorator
     * @param {string} name
     * @param {Decorator} decorator node
     * @returns {ST_OffChainView_Decorator} @PrivateLambda decorator information
     */
    extractPrivateLambdaInfo = (name: string, decorator: Decorator): ST_PrivateLambda_Decorator => {
        const decoratorInfo: ST_PrivateLambda_Decorator = {
            kind: ST_DecoratorKind.PrivateLambda,
            name: name,
            withStorage: undefined,
            withOperations: false,
            functionRef: name,
        };

        /**
         * If expression is an Identifier, it means the @PrivateLambda doesn't have any argument.
         */
        if (isIdentifier(decorator.expression)) {
            return decoratorInfo;
        }

        if (isCallExpression(decorator.expression)) {
            const callExpression = decorator.expression;
            const args = callExpression.arguments;

            if (args.length > 1) {
                return this.output.emitError(callExpression, '@PrivateLambda decorator can only have one item.');
            }

            args.forEach((arg) => {
                if (isObjectLiteralExpression(arg)) {
                    arg.properties.forEach((prop) => {
                        if (isPropertyAssignment(prop)) {
                            const propName = this.transpiler.extractName(prop);
                            switch (propName) {
                                case ST_PrivateLambdaDecoratorProp.withStorage:
                                    const value = this.interpreters.Literal.extractStringLiteral(prop.initializer);
                                    if (['read-only', 'read-write'].includes(value)) {
                                        decoratorInfo.withStorage = value as 'read-only' | 'read-write';
                                    }
                                    break;
                                case ST_PrivateLambdaDecoratorProp.withOperations:
                                    decoratorInfo.withOperations = this.interpreters.Literal.extractBoolLiteral(
                                        prop.initializer,
                                    );
                                    break;
                                default:
                                    this.output.emitWarning(prop, `Unknown property (${propName}).`);
                            }
                        } else {
                            this.output.emitError(
                                prop,
                                `Expected a property assignment but got (${SyntaxKind[prop.kind]}).`,
                            );
                        }
                    });
                } else {
                    this.output.emitError(arg, `Expected an object but got (${SyntaxKind[arg.kind]}).`);
                }
            });

            return decoratorInfo;
        }
        return this.output.emitError(decorator.expression, `Unexpected (${SyntaxKind[decorator.expression.kind]}).`);
    };

    /**
     * @description Extract information from @MetadataBuilder decorator
     * @param {string} name property name
     * @param {ObjectLiteralExpression} node node
     * @returns {ST_Metadata_Decorator} @MetadataBuilder decorator information
     */
    extractMetadataInfo = (name: string, node: ObjectLiteralExpression): ST_Metadata_Decorator => {
        return {
            kind: ST_DecoratorKind.MetadataBuilder,
            name,
            properties: this.interpreters.Metadata.parseObjectLiteralExpression(node),
        };
    };

    /**
     * @description Extract information from @Contract decorator
     * @param {string} className
     * @param {Decorator} decorator node
     * @returns {ST_Decorator} @Contract decorator information
     */
    extractContractInfo = (className: string, decorator: Decorator): ST_Contract_Decorator => {
        const decoratorInfo: ST_Contract_Decorator = {
            kind: ST_DecoratorKind.Contract,
            name: className,
            flags: [],
        };
        /**
         * If expression is an Identifier, it means the @Contract doesn't have any argument.
         */
        if (isIdentifier(decorator.expression)) {
            return decoratorInfo;
        }

        if (isCallExpression(decorator.expression)) {
            const callExpression = decorator.expression;
            const args = callExpression.arguments;

            if (args.length > 1) {
                return this.output.emitError(callExpression, '@Contract decorator can only have one item.');
            }

            args.forEach((arg) => {
                if (isObjectLiteralExpression(arg)) {
                    arg.properties.forEach((prop) => {
                        if (isPropertyAssignment(prop)) {
                            const propName = this.transpiler.extractName(prop);
                            switch (propName) {
                                case ST_ContractDecoratorProp.flags:
                                    if (isArrayLiteralExpression(prop.initializer)) {
                                        decoratorInfo.flags = prop.initializer.elements.reduce<ST_Flag[]>(
                                            (state, el) => {
                                                if (
                                                    isArrayLiteralExpression(el) &&
                                                    el.elements.every(isStringLiteral)
                                                ) {
                                                    const [name, ...rest] = el.elements;
                                                    state = [
                                                        ...state,
                                                        {
                                                            name: name.text,
                                                            args: rest.map(({ text }) => text),
                                                        },
                                                    ];
                                                }
                                                return state;
                                            },
                                            [],
                                        );
                                    } else {
                                        this.output.emitError(prop, `Expected a list of flags.`);
                                    }
                                    break;
                                default:
                                    this.output.emitWarning(prop, `Unknown property (${propName}).`);
                            }
                        } else {
                            this.output.emitError(
                                prop,
                                `Expected a property assignment but got (${SyntaxKind[prop.kind]}).`,
                            );
                        }
                    });
                } else {
                    this.output.emitError(arg, `Expected an object but got (${SyntaxKind[arg.kind]}).`);
                }
            });

            return decoratorInfo;
        }
        return this.output.emitError(decorator.expression, `Unexpected (${SyntaxKind[decorator.expression.kind]}).`);
    };

    /**
     * @description Extract information from @OffChainView | @OnChainView decorator
     * @param {string} functionName
     * @param {Decorator} decorator node
     * @returns {ST_View_Decorator} decorator information
     */
    extractViewInfo = (functionName: string, decorator: Decorator): ST_View_Decorator => {
        const decoratorInfo: ST_View_Decorator = {
            kind: ST_DecoratorKind.View,
            offchainOnly: false,
            name: functionName,
            pure: false,
            description: '',
            functionRef: functionName,
        };

        /**
         * If expression is an Identifier, it means the View doesn't have any argument.
         */
        if (isIdentifier(decorator.expression)) {
            return decoratorInfo;
        }

        if (isCallExpression(decorator.expression)) {
            const callExpression = decorator.expression;
            const args = callExpression.arguments;

            if (args.length > 1) {
                return this.output.emitError(callExpression, 'Decorator can only have one item.');
            }

            args.forEach((arg) => {
                if (isObjectLiteralExpression(arg)) {
                    arg.properties.forEach((prop) => {
                        if (isPropertyAssignment(prop)) {
                            const propName = this.transpiler.extractName(prop);
                            switch (propName) {
                                case ST_ViewDecoratorProp.name:
                                    decoratorInfo.name = this.interpreters.Literal.extractStringLiteral(
                                        prop.initializer,
                                    );
                                    break;
                                case ST_ViewDecoratorProp.pure:
                                    decoratorInfo.pure = this.interpreters.Literal.extractBoolLiteral(prop.initializer);
                                    break;
                                case ST_ViewDecoratorProp.description:
                                    decoratorInfo.description = this.interpreters.Literal.extractStringLiteral(
                                        prop.initializer,
                                    );
                                    break;
                                default:
                                    this.output.emitWarning(prop, `Unknown property (${propName}).`);
                            }
                        } else {
                            this.output.emitError(
                                prop,
                                `Expected a property assignment but got (${SyntaxKind[prop.kind]}).`,
                            );
                        }
                    });
                } else {
                    this.output.emitError(arg, `Expected an object but got (${SyntaxKind[arg.kind]}).`);
                }
            });

            return decoratorInfo;
        }
        return this.output.emitError(decorator.expression, `Unexpected (${SyntaxKind[decorator.expression.kind]}).`);
    };

    /**
     * @description Extract information from @EntryPoint decorator
     * @param {string} functionName
     * @param {Decorator} decorator node
     * @returns {ST_Decorator} @EntryPoint decorator information
     */
    extractEntryPointInfo = (functionName: string, decorator: Decorator): ST_EntryPoint_Decorator => {
        const decoratorInfo: ST_EntryPoint_Decorator = {
            kind: ST_DecoratorKind.EntryPoint,
            name: functionName,
            mock: false,
            lazy: false,
            lazy_no_code: false,
            check_no_incoming_transfer: undefined,
            functionRef: functionName,
        };
        /**
         * If expression is an Identifier, it means the @EntryPoint doesn't have any argument.
         */
        if (isIdentifier(decorator.expression)) {
            return decoratorInfo;
        }

        if (isCallExpression(decorator.expression)) {
            const callExpression = decorator.expression;
            const args = callExpression.arguments;

            if (args.length > 1) {
                return this.output.emitError(callExpression, '@EntryPoint decorator can only have one item.');
            }

            if (callExpression?.typeArguments?.length) {
                decoratorInfo.type = this.interpreters.Type.extractTypeDef(callExpression.typeArguments[0]);
            }

            args.forEach((arg) => {
                if (isObjectLiteralExpression(arg)) {
                    arg.properties.forEach((prop) => {
                        if (isPropertyAssignment(prop)) {
                            const propName = this.transpiler.extractName(prop);
                            switch (propName) {
                                case ST_EntryPointDecoratorProp.name:
                                    decoratorInfo.name = this.interpreters.Literal.extractStringLiteral(
                                        prop.initializer,
                                    );
                                    break;
                                case ST_EntryPointDecoratorProp.mock:
                                    decoratorInfo.mock = this.interpreters.Literal.extractBoolLiteral(prop.initializer);
                                    break;
                                case ST_EntryPointDecoratorProp.lazy:
                                    decoratorInfo.lazy = this.interpreters.Literal.extractBoolLiteral(prop.initializer);
                                    break;
                                case ST_EntryPointDecoratorProp.lazy_no_code:
                                    decoratorInfo.lazy_no_code = this.interpreters.Literal.extractBoolLiteral(
                                        prop.initializer,
                                    );
                                case ST_EntryPointDecoratorProp.check_no_incoming_transfer:
                                    decoratorInfo.check_no_incoming_transfer = this.interpreters.Literal.extractBoolLiteral(
                                        prop.initializer,
                                    );
                                    break;
                                default:
                                    this.output.emitWarning(prop, `Unknown property (${propName}).`);
                            }
                        } else {
                            this.output.emitError(
                                prop,
                                `Expected a property assignment but got (${SyntaxKind[prop.kind]}).`,
                            );
                        }
                    });
                } else {
                    this.output.emitError(arg, `Expected an object but got (${SyntaxKind[arg.kind]}).`);
                }
            });

            return decoratorInfo;
        }
        return this.output.emitError(decorator.expression, `Unexpected (${SyntaxKind[decorator.expression.kind]}).`);
    };

    hasDecorator = (node: Node, decorator: ST_DecoratorKind): boolean => {
        return !!node.decorators?.some((d) => this.transpiler.extractName(d.expression) === decorator);
    };
}
