# Typescript Syntax for SmartML

## Development

### Building

```shell
node scripts/generate-ide-typings.js && npx rollup -c
```

### Testing

**Run tests**
```shell
npm run ci-test
```

### Update Snapshot

```shell
npx jest --updateSnapshot

# CI mode
CI=true TRACE_ERRORS=true npx jest --no-watch --all -u
```