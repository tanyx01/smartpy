import { FrontendType } from '../../../../src/enums/type';
import Transpiler from '../../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../../ExpectAnyLine';

test(`${FrontendType.TTuple}<${FrontendType.TNat}, ${FrontendType.TNat}>`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type tuple__nat_nat__type = ${FrontendType.TTuple}<[${FrontendType.TNat}, ${FrontendType.TNat}]>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        tuple__nat_nat__type: {
            type: FrontendType.TTuple,
            innerTypes: [
                {
                    line: ExpectAnyLine,
                    type: 'TNat',
                },
                {
                    line: ExpectAnyLine,
                    type: 'TNat',
                },
            ],
            line: ExpectAnyLine,
        },
    });
});
