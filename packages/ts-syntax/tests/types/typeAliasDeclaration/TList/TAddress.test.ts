import { FrontendType } from '../../../../src/enums/type';
import Transpiler from '../../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../../ExpectAnyLine';

test(`${FrontendType.TList}<${FrontendType.TNat}>`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type list__nat__type = ${FrontendType.TList}<${FrontendType.TNat}>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        list__nat__type: {
            type: FrontendType.TList,
            line: ExpectAnyLine,
            innerTypes: [
                {
                    line: ExpectAnyLine,
                    type: FrontendType.TNat,
                },
            ],
        },
    });
});
