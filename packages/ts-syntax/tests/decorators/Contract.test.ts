import { ST_DecoratorKind } from '../../src/enums/decorator';
import Transpiler from '../../src/lib/Transpiler';

test('Contract Decorator', async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `
        @Contract({
            flags: [
                ["initial-cast"],
                ["protocol", "jakarta"]
            ]
        })
        class StoreValue {}


        Dev.compileContract('compile_contract', new StoreValue());
        `,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.result.contracts).toEqual({
        StoreValue: {
            classRef: 'StoreValue',
            name: 'StoreValue',
        },
    });
    expect(transpiler.output.result.scope.classes['StoreValue'].decorators).toEqual({
        Contract: {
            flags: [
                {
                    args: [],
                    name: 'initial-cast',
                },
                {
                    args: ['jakarta'],
                    name: 'protocol',
                },
            ],
            kind: ST_DecoratorKind.Contract,
            name: 'StoreValue',
        },
    });

    const contract = transpiler.output.result.scope.classes['StoreValue'];
    expect(transpiler.output.stringifyFlags(contract)).toMatchSnapshot('(initial-cast) (protocol jakarta)');
});
