import Transpiler from '../../../src/lib/Transpiler';
import { FrontendType } from '../../../src/enums/type';

test(`${FrontendType.TSet}<${FrontendType.TNat}>`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `let set_of_nat: ${FrontendType.TSet}<${FrontendType.TNat}> = [1]`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalProperties).toMatchSnapshot();
});
