import Transpiler from '../../../src/lib/Transpiler';
import { FrontendType } from '../../../src/enums/type';

describe(`${FrontendType.TList}<${FrontendType.TIntOrNat}>`, () => {
    test('Implicit', async () => {
        const sourceCode = {
            name: 'code.ts',
            code: 'let list_of_nat = [1]',
        };
        const transpiler = new Transpiler(sourceCode);
        await transpiler.transpile();

        expect(transpiler.output.globalProperties).toMatchSnapshot();
    });
    test(`${FrontendType.TList}<${FrontendType.TNat}>`, async () => {
        const sourceCode = {
            name: 'code.ts',
            code: `let list_of_nat: ${FrontendType.TList}<${FrontendType.TNat}> = [1]`,
        };
        const transpiler = new Transpiler(sourceCode);
        await transpiler.transpile();

        expect(transpiler.output.globalProperties).toMatchSnapshot();
    });
});
