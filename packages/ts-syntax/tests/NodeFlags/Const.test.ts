import Transpiler from '../../src/lib/Transpiler';

test('Constant values cannot be assigned new values', async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `
        @Contract
        class ConstantValue {
            @EntryPoint
            ep() {
                const constant_value = 1;
                constant_value = 2;
            }
        }
        `,
    };
    const transpiler = new Transpiler(sourceCode);

    try {
        await transpiler.transpile();
        throw new Error('Expected error, constant values cannot be assigned new values');
    } catch (e: any) {
        expect(e.message).toEqual("Cannot assign to 'constant_value' because it is a constant.");
    }
});
