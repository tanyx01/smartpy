const fs = require('fs');

const code = fs.readFileSync('../ts-types/index.d.ts', { encoding: 'utf-8', flag: 'r' });

const template = `export default "${code
    .replace(/(?:\/\/.*)/g, '')
    .replace(/(?:\r\n|\r|\n|\t)/g, '')
    .replace(/(?:\s+)/g, ' ')
    .trim()}";\n`;

fs.writeFileSync('src/ide.generated.ts', template, { encoding: 'utf-8', flag: 'w' });
